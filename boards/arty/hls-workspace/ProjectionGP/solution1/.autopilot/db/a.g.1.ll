; ModuleID = '/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace/ProjectionGP/solution1/.autopilot/db/a.g.1.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@s = global [41 x float] zeroinitializer, align 16 ; [#uses=6 type=[41 x float]*]
@projection_gp.str = internal unnamed_addr constant [14 x i8] c"projection_gp\00" ; [#uses=1 type=[14 x i8]*]
@k = global [40 x float] zeroinitializer, align 16 ; [#uses=4 type=[40 x float]*]
@e = global [41 x float] zeroinitializer, align 16 ; [#uses=4 type=[41 x float]*]
@bvCnt = global i32 0, align 4                    ; [#uses=4 type=i32*]
@basisVectors = global [533 x float] zeroinitializer, align 16 ; [#uses=5 type=[533 x float]*]
@alpha = global [41 x float] zeroinitializer, align 16 ; [#uses=9 type=[41 x float]*]
@Q = global [1681 x float] zeroinitializer, align 16 ; [#uses=12 type=[1681 x float]*]
@C = global [1681 x float] zeroinitializer, align 16 ; [#uses=12 type=[1681 x float]*]
@.str9 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_INNER\00", align 1 ; [#uses=1 type=[17 x i8]*]
@.str8 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_OUTER\00", align 1 ; [#uses=1 type=[17 x i8]*]
@.str7 = private unnamed_addr constant [7 x i8] c"CALC_K\00", align 1 ; [#uses=1 type=[7 x i8]*]
@.str6 = private unnamed_addr constant [20 x i8] c"DELETE_BV_UNSET_C_Q\00", align 1 ; [#uses=1 type=[20 x i8]*]
@.str5 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_INNER\00", align 1 ; [#uses=1 type=[18 x i8]*]
@.str4 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_OUTER\00", align 1 ; [#uses=1 type=[18 x i8]*]
@.str3 = private unnamed_addr constant [16 x i8] c"DELETE_BV_ALPHA\00", align 1 ; [#uses=1 type=[16 x i8]*]
@.str20 = private unnamed_addr constant [11 x i8] c"PREDICTION\00", align 1 ; [#uses=1 type=[11 x i8]*]
@.str2 = private unnamed_addr constant [20 x i8] c"DELETE_BV_SWAP_LOOP\00", align 1 ; [#uses=1 type=[20 x i8]*]
@.str19 = private unnamed_addr constant [11 x i8] c"INIT_INNER\00", align 1 ; [#uses=1 type=[11 x i8]*]
@.str18 = private unnamed_addr constant [11 x i8] c"INIT_OUTER\00", align 1 ; [#uses=1 type=[11 x i8]*]
@.str17 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1 ; [#uses=1 type=[10 x i8]*]
@.str16 = private unnamed_addr constant [8 x i8] c"COPY_BV\00", align 1 ; [#uses=1 type=[8 x i8]*]
@.str15 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_INNER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str14 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_OUTER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str13 = private unnamed_addr constant [15 x i8] c"UPDATE_C_INNER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str12 = private unnamed_addr constant [15 x i8] c"UPDATE_C_OUTER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str11 = private unnamed_addr constant [13 x i8] c"UPDATE_ALPHA\00", align 1 ; [#uses=1 type=[13 x i8]*]
@.str10 = private unnamed_addr constant [7 x i8] c"CALC_S\00", align 1 ; [#uses=1 type=[7 x i8]*]
@.str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=1 type=[1 x i8]*]
@.str = private unnamed_addr constant [12 x i8] c"hls_label_0\00", align 1 ; [#uses=1 type=[12 x i8]*]

; [#uses=1]
define internal fastcc void @train_full_bv_set(float* %pX, float %pY) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pX}, i64 0, metadata !66), !dbg !67 ; [debug line = 122:37] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !68), !dbg !69 ; [debug line = 122:57] [debug variable = pY]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX, i32 13) nounwind, !dbg !70 ; [debug line = 122:62]
  br label %1, !dbg !72                           ; [debug line = 126:35]

; <label>:1                                       ; preds = %2, %0
  %i = phi i32 [ 0, %0 ], [ %i.1, %2 ]            ; [#uses=4 type=i32]
  %m = phi float [ 0x4050B830E0000000, %0 ], [ %m.1, %2 ] ; [#uses=2 type=float]
  %exitcond8 = icmp eq i32 %i, 40, !dbg !72       ; [#uses=1 type=i1] [debug line = 126:35]
  br i1 %exitcond8, label %.preheader10.preheader, label %2, !dbg !72 ; [debug line = 126:35]

.preheader10.preheader:                           ; preds = %1
  %m.0.lcssa = phi float [ %m, %1 ]               ; [#uses=1 type=float]
  br label %.preheader10, !dbg !74                ; [debug line = 137:45]

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([7 x i8]* @.str7, i64 0, i64 0)) nounwind, !dbg !76 ; [debug line = 126:51]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([7 x i8]* @.str7, i64 0, i64 0)) nounwind, !dbg !76 ; [#uses=1 type=i32] [debug line = 126:51]
  %tmp = mul i32 %i, 13, !dbg !78                 ; [#uses=1 type=i32] [debug line = 128:16]
  %tmp.1 = zext i32 %tmp to i64, !dbg !78         ; [#uses=1 type=i64] [debug line = 128:16]
  %basisVectors.addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.1, !dbg !78 ; [#uses=1 type=float*] [debug line = 128:16]
  %tmp.2 = call fastcc float @K(float* %basisVectors.addr, float* %pX), !dbg !78 ; [#uses=3 type=float] [debug line = 128:16]
  %tmp.3 = zext i32 %i to i64, !dbg !78           ; [#uses=2 type=i64] [debug line = 128:16]
  %k.addr = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp.3, !dbg !78 ; [#uses=1 type=float*] [debug line = 128:16]
  store float %tmp.2, float* %k.addr, align 4, !dbg !78 ; [debug line = 128:16]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp.2) nounwind
  %alpha.addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.3, !dbg !79 ; [#uses=1 type=float*] [debug line = 129:9]
  %alpha.load = load float* %alpha.addr, align 4, !dbg !79 ; [#uses=2 type=float] [debug line = 129:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  %tmp.4 = fmul float %tmp.2, %alpha.load, !dbg !79 ; [#uses=1 type=float] [debug line = 129:9]
  %m.1 = fadd float %m, %tmp.4, !dbg !79          ; [#uses=1 type=float] [debug line = 129:9]
  call void @llvm.dbg.value(metadata !{float %m.1}, i64 0, metadata !80), !dbg !79 ; [debug line = 129:9] [debug variable = m]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([7 x i8]* @.str7, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !81 ; [#uses=0 type=i32] [debug line = 130:5]
  %i.1 = add i32 %i, 1, !dbg !82                  ; [#uses=1 type=i32] [debug line = 126:45]
  call void @llvm.dbg.value(metadata !{i32 %i.1}, i64 0, metadata !83), !dbg !82 ; [debug line = 126:45] [debug variable = i]
  br label %1, !dbg !82                           ; [debug line = 126:45]

.preheader10:                                     ; preds = %6, %.preheader10.preheader
  %i1 = phi i32 [ %i.3, %6 ], [ 0, %.preheader10.preheader ] ; [#uses=4 type=i32]
  %exitcond7 = icmp eq i32 %i1, 40, !dbg !74      ; [#uses=1 type=i1] [debug line = 137:45]
  br i1 %exitcond7, label %.preheader9.preheader, label %3, !dbg !74 ; [debug line = 137:45]

.preheader9.preheader:                            ; preds = %.preheader10
  br label %.preheader9, !dbg !84                 ; [debug line = 148:36]

; <label>:3                                       ; preds = %.preheader10
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([17 x i8]* @.str8, i64 0, i64 0)) nounwind, !dbg !86 ; [debug line = 137:61]
  %rbegin1 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([17 x i8]* @.str8, i64 0, i64 0)) nounwind, !dbg !86 ; [#uses=1 type=i32] [debug line = 137:61]
  %tmp.7 = zext i32 %i1 to i64, !dbg !88          ; [#uses=2 type=i64] [debug line = 139:6]
  %s.addr = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.7, !dbg !88 ; [#uses=2 type=float*] [debug line = 139:6]
  store float 0.000000e+00, float* %s.addr, align 4, !dbg !88 ; [debug line = 139:6]
  %e.addr = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp.7, !dbg !89 ; [#uses=2 type=float*] [debug line = 140:6]
  store float 0.000000e+00, float* %e.addr, align 4, !dbg !89 ; [debug line = 140:6]
  %tmp.8 = mul i32 %i1, 41, !dbg !90              ; [#uses=1 type=i32] [debug line = 143:10]
  br label %4, !dbg !93                           ; [debug line = 141:49]

; <label>:4                                       ; preds = %5, %3
  %tmp.9 = phi float [ 0.000000e+00, %3 ], [ %tmp.28, %5 ] ; [#uses=2 type=float]
  %tmp.10 = phi float [ 0.000000e+00, %3 ], [ %tmp.26, %5 ] ; [#uses=2 type=float]
  %j = phi i32 [ 0, %3 ], [ %j.1, %5 ]            ; [#uses=4 type=i32]
  %exitcond6 = icmp eq i32 %j, 40, !dbg !93       ; [#uses=1 type=i1] [debug line = 141:49]
  br i1 %exitcond6, label %6, label %5, !dbg !93  ; [debug line = 141:49]

; <label>:5                                       ; preds = %4
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([17 x i8]* @.str9, i64 0, i64 0)) nounwind, !dbg !94 ; [debug line = 141:65]
  %rbegin3 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([17 x i8]* @.str9, i64 0, i64 0)) nounwind, !dbg !94 ; [#uses=1 type=i32] [debug line = 141:65]
  %tmp.22 = add i32 %j, %tmp.8, !dbg !90          ; [#uses=1 type=i32] [debug line = 143:10]
  %tmp.23 = zext i32 %tmp.22 to i64, !dbg !90     ; [#uses=2 type=i64] [debug line = 143:10]
  %C.addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.23, !dbg !90 ; [#uses=1 type=float*] [debug line = 143:10]
  %C.load = load float* %C.addr, align 4, !dbg !90 ; [#uses=2 type=float] [debug line = 143:10]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load) nounwind
  %tmp.24 = zext i32 %j to i64, !dbg !90          ; [#uses=1 type=i64] [debug line = 143:10]
  %k.addr.2 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp.24, !dbg !90 ; [#uses=1 type=float*] [debug line = 143:10]
  %k.load.1 = load float* %k.addr.2, align 4, !dbg !90 ; [#uses=4 type=float] [debug line = 143:10]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %k.load.1) nounwind
  %tmp.25 = fmul float %C.load, %k.load.1, !dbg !90 ; [#uses=1 type=float] [debug line = 143:10]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp.10) nounwind
  %tmp.26 = fadd float %tmp.10, %tmp.25, !dbg !90 ; [#uses=2 type=float] [debug line = 143:10]
  store float %tmp.26, float* %s.addr, align 4, !dbg !90 ; [debug line = 143:10]
  %Q.addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.23, !dbg !95 ; [#uses=1 type=float*] [debug line = 144:10]
  %Q.load = load float* %Q.addr, align 4, !dbg !95 ; [#uses=2 type=float] [debug line = 144:10]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %k.load.1) nounwind
  %tmp.27 = fmul float %Q.load, %k.load.1, !dbg !95 ; [#uses=1 type=float] [debug line = 144:10]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp.9) nounwind
  %tmp.28 = fadd float %tmp.9, %tmp.27, !dbg !95  ; [#uses=2 type=float] [debug line = 144:10]
  store float %tmp.28, float* %e.addr, align 4, !dbg !95 ; [debug line = 144:10]
  %rend15 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([17 x i8]* @.str9, i64 0, i64 0), i32 %rbegin3) nounwind, !dbg !96 ; [#uses=0 type=i32] [debug line = 145:9]
  %j.1 = add i32 %j, 1, !dbg !97                  ; [#uses=1 type=i32] [debug line = 141:59]
  call void @llvm.dbg.value(metadata !{i32 %j.1}, i64 0, metadata !98), !dbg !97 ; [debug line = 141:59] [debug variable = j]
  br label %4, !dbg !97                           ; [debug line = 141:59]

; <label>:6                                       ; preds = %4
  %rend17 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([17 x i8]* @.str8, i64 0, i64 0), i32 %rbegin1) nounwind, !dbg !99 ; [#uses=0 type=i32] [debug line = 146:5]
  %i.3 = add i32 %i1, 1, !dbg !100                ; [#uses=1 type=i32] [debug line = 137:55]
  call void @llvm.dbg.value(metadata !{i32 %i.3}, i64 0, metadata !101), !dbg !100 ; [debug line = 137:55] [debug variable = i]
  br label %.preheader10, !dbg !100               ; [debug line = 137:55]

.preheader9:                                      ; preds = %7, %.preheader9.preheader
  %i2 = phi i32 [ %i.2, %7 ], [ 0, %.preheader9.preheader ] ; [#uses=3 type=i32]
  %sigma2 = phi float [ %sigma2.1, %7 ], [ 1.000000e+00, %.preheader9.preheader ] ; [#uses=2 type=float]
  %exitcond5 = icmp eq i32 %i2, 40, !dbg !84      ; [#uses=1 type=i1] [debug line = 148:36]
  br i1 %exitcond5, label %8, label %7, !dbg !84  ; [debug line = 148:36]

; <label>:7                                       ; preds = %.preheader9
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([7 x i8]* @.str10, i64 0, i64 0)) nounwind, !dbg !102 ; [debug line = 148:52]
  %rbegin2 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([7 x i8]* @.str10, i64 0, i64 0)) nounwind, !dbg !102 ; [#uses=1 type=i32] [debug line = 148:52]
  %tmp.17 = zext i32 %i2 to i64, !dbg !104        ; [#uses=2 type=i64] [debug line = 150:6]
  %s.addr.1 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.17, !dbg !104 ; [#uses=1 type=float*] [debug line = 150:6]
  %s.load = load float* %s.addr.1, align 4, !dbg !104 ; [#uses=2 type=float] [debug line = 150:6]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %s.load) nounwind
  %k.addr.1 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp.17, !dbg !104 ; [#uses=1 type=float*] [debug line = 150:6]
  %k.load = load float* %k.addr.1, align 4, !dbg !104 ; [#uses=2 type=float] [debug line = 150:6]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %k.load) nounwind
  %tmp.18 = fmul float %s.load, %k.load, !dbg !104 ; [#uses=1 type=float] [debug line = 150:6]
  %sigma2.1 = fadd float %sigma2, %tmp.18, !dbg !104 ; [#uses=1 type=float] [debug line = 150:6]
  call void @llvm.dbg.value(metadata !{float %sigma2.1}, i64 0, metadata !105), !dbg !104 ; [debug line = 150:6] [debug variable = sigma2]
  %rend13 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([7 x i8]* @.str10, i64 0, i64 0), i32 %rbegin2) nounwind, !dbg !106 ; [#uses=0 type=i32] [debug line = 151:5]
  %i.2 = add i32 %i2, 1, !dbg !107                ; [#uses=1 type=i32] [debug line = 148:46]
  call void @llvm.dbg.value(metadata !{i32 %i.2}, i64 0, metadata !108), !dbg !107 ; [debug line = 148:46] [debug variable = i]
  br label %.preheader9, !dbg !107                ; [debug line = 148:46]

; <label>:8                                       ; preds = %.preheader9
  %sigma2.0.lcssa = phi float [ %sigma2, %.preheader9 ] ; [#uses=1 type=float]
  store float 1.000000e+00, float* getelementptr inbounds ([41 x float]* @s, i64 0, i64 40), align 16, !dbg !109 ; [debug line = 152:5]
  %tmp.11 = fsub float %pY, %m.0.lcssa, !dbg !110 ; [#uses=1 type=float] [debug line = 154:40]
  %tmp.12 = fpext float %tmp.11 to double, !dbg !110 ; [#uses=1 type=double] [debug line = 154:40]
  %tmp.13 = fpext float %sigma2.0.lcssa to double, !dbg !110 ; [#uses=1 type=double] [debug line = 154:40]
  %tmp.14 = fadd double %tmp.13, 1.000000e+00, !dbg !110 ; [#uses=2 type=double] [debug line = 154:40]
  %tmp.15 = fdiv double %tmp.12, %tmp.14, !dbg !110 ; [#uses=1 type=double] [debug line = 154:40]
  %q = fptrunc double %tmp.15 to float, !dbg !110 ; [#uses=2 type=float] [debug line = 154:40]
  call void @llvm.dbg.value(metadata !{float %q}, i64 0, metadata !111), !dbg !110 ; [debug line = 154:40] [debug variable = q]
  %tmp.16 = fdiv double -1.000000e+00, %tmp.14, !dbg !112 ; [#uses=1 type=double] [debug line = 155:35]
  %r = fptrunc double %tmp.16 to float, !dbg !112 ; [#uses=1 type=float] [debug line = 155:35]
  call void @llvm.dbg.value(metadata !{float %r}, i64 0, metadata !113), !dbg !112 ; [debug line = 155:35] [debug variable = r]
  br label %9, !dbg !114                          ; [debug line = 158:42]

; <label>:9                                       ; preds = %10, %8
  %gamma = phi float [ 1.000000e+00, %8 ], [ %gamma.1, %10 ] ; [#uses=2 type=float]
  %i3 = phi i32 [ 0, %8 ], [ %i.4, %10 ]          ; [#uses=3 type=i32]
  %exitcond4 = icmp eq i32 %i3, 40, !dbg !114     ; [#uses=1 type=i1] [debug line = 158:42]
  br i1 %exitcond4, label %11, label %10, !dbg !114 ; [debug line = 158:42]

; <label>:10                                      ; preds = %9
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([13 x i8]* @.str11, i64 0, i64 0)) nounwind, !dbg !116 ; [debug line = 158:58]
  %rbegin4 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([13 x i8]* @.str11, i64 0, i64 0)) nounwind, !dbg !116 ; [#uses=1 type=i32] [debug line = 158:58]
  %tmp.31 = zext i32 %i3 to i64, !dbg !118        ; [#uses=4 type=i64] [debug line = 160:6]
  %e.addr.1 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp.31, !dbg !118 ; [#uses=1 type=float*] [debug line = 160:6]
  %e.load = load float* %e.addr.1, align 4, !dbg !118 ; [#uses=2 type=float] [debug line = 160:6]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %e.load) nounwind
  %k.addr.3 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp.31, !dbg !118 ; [#uses=1 type=float*] [debug line = 160:6]
  %k.load.2 = load float* %k.addr.3, align 4, !dbg !118 ; [#uses=2 type=float] [debug line = 160:6]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %k.load.2) nounwind
  %tmp.32 = fmul float %e.load, %k.load.2, !dbg !118 ; [#uses=1 type=float] [debug line = 160:6]
  %gamma.1 = fsub float %gamma, %tmp.32, !dbg !118 ; [#uses=1 type=float] [debug line = 160:6]
  call void @llvm.dbg.value(metadata !{float %gamma.1}, i64 0, metadata !119), !dbg !118 ; [debug line = 160:6] [debug variable = gamma]
  %s.addr.2 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.31, !dbg !120 ; [#uses=1 type=float*] [debug line = 161:6]
  %s.load.1 = load float* %s.addr.2, align 4, !dbg !120 ; [#uses=2 type=float] [debug line = 161:6]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %s.load.1) nounwind
  %tmp.34 = fmul float %s.load.1, %q, !dbg !120   ; [#uses=1 type=float] [debug line = 161:6]
  %alpha.addr.1 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.31, !dbg !120 ; [#uses=2 type=float*] [debug line = 161:6]
  %alpha.load.2 = load float* %alpha.addr.1, align 4, !dbg !120 ; [#uses=2 type=float] [debug line = 161:6]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.2) nounwind
  %tmp.35 = fadd float %alpha.load.2, %tmp.34, !dbg !120 ; [#uses=1 type=float] [debug line = 161:6]
  store float %tmp.35, float* %alpha.addr.1, align 4, !dbg !120 ; [debug line = 161:6]
  %rend19 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([13 x i8]* @.str11, i64 0, i64 0), i32 %rbegin4) nounwind, !dbg !121 ; [#uses=0 type=i32] [debug line = 162:5]
  %i.4 = add i32 %i3, 1, !dbg !122                ; [#uses=1 type=i32] [debug line = 158:52]
  call void @llvm.dbg.value(metadata !{i32 %i.4}, i64 0, metadata !123), !dbg !122 ; [debug line = 158:52] [debug variable = i]
  br label %9, !dbg !122                          ; [debug line = 158:52]

; <label>:11                                      ; preds = %9
  %gamma.0.lcssa = phi float [ %gamma, %9 ]       ; [#uses=1 type=float]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float 1.000000e+00) nounwind
  %alpha.load.1 = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !124 ; [#uses=2 type=float] [debug line = 168:5]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.1) nounwind
  %tmp.30 = fadd float %alpha.load.1, %q, !dbg !124 ; [#uses=1 type=float] [debug line = 168:5]
  store float %tmp.30, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !124 ; [debug line = 168:5]
  br label %12, !dbg !125                         ; [debug line = 171:43]

; <label>:12                                      ; preds = %16, %11
  %i4 = phi i32 [ 0, %11 ], [ %i.5, %16 ]         ; [#uses=4 type=i32]
  %exitcond3 = icmp eq i32 %i4, 41, !dbg !125     ; [#uses=1 type=i1] [debug line = 171:43]
  br i1 %exitcond3, label %.preheader.preheader, label %13, !dbg !125 ; [debug line = 171:43]

.preheader.preheader:                             ; preds = %12
  %tmp.37 = fpext float %gamma.0.lcssa to double, !dbg !127 ; [#uses=1 type=double] [debug line = 187:4]
  br label %.preheader, !dbg !132                 ; [debug line = 180:43]

; <label>:13                                      ; preds = %12
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str12, i64 0, i64 0)) nounwind, !dbg !133 ; [debug line = 171:60]
  %rbegin5 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str12, i64 0, i64 0)) nounwind, !dbg !133 ; [#uses=1 type=i32] [debug line = 171:60]
  %tmp.38 = zext i32 %i4 to i64, !dbg !135        ; [#uses=1 type=i64] [debug line = 176:13]
  %s.addr.3 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.38, !dbg !135 ; [#uses=1 type=float*] [debug line = 176:13]
  %tmp.39 = mul i32 %i4, 41, !dbg !135            ; [#uses=1 type=i32] [debug line = 176:13]
  br label %14, !dbg !138                         ; [debug line = 173:44]

; <label>:14                                      ; preds = %15, %13
  %j5 = phi i32 [ 0, %13 ], [ %j.2, %15 ]         ; [#uses=4 type=i32]
  %exitcond2 = icmp eq i32 %j5, 41, !dbg !138     ; [#uses=1 type=i1] [debug line = 173:44]
  br i1 %exitcond2, label %16, label %15, !dbg !138 ; [debug line = 173:44]

; <label>:15                                      ; preds = %14
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str13, i64 0, i64 0)) nounwind, !dbg !139 ; [debug line = 173:61]
  %rbegin7 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str13, i64 0, i64 0)) nounwind, !dbg !139 ; [#uses=1 type=i32] [debug line = 173:61]
  %s.load.2 = load float* %s.addr.3, align 4, !dbg !135 ; [#uses=2 type=float] [debug line = 176:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %s.load.2) nounwind
  %tmp.44 = zext i32 %j5 to i64, !dbg !135        ; [#uses=1 type=i64] [debug line = 176:13]
  %s.addr.4 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.44, !dbg !135 ; [#uses=1 type=float*] [debug line = 176:13]
  %s.load.3 = load float* %s.addr.4, align 4, !dbg !135 ; [#uses=2 type=float] [debug line = 176:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %s.load.3) nounwind
  %tmp.45 = fmul float %s.load.2, %s.load.3, !dbg !135 ; [#uses=1 type=float] [debug line = 176:13]
  %tmp.46 = fmul float %tmp.45, %r, !dbg !135     ; [#uses=1 type=float] [debug line = 176:13]
  %tmp.47 = add i32 %j5, %tmp.39, !dbg !135       ; [#uses=1 type=i32] [debug line = 176:13]
  %tmp.48 = zext i32 %tmp.47 to i64, !dbg !135    ; [#uses=1 type=i64] [debug line = 176:13]
  %C.addr.1 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.48, !dbg !135 ; [#uses=2 type=float*] [debug line = 176:13]
  %C.load.1 = load float* %C.addr.1, align 4, !dbg !135 ; [#uses=2 type=float] [debug line = 176:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.1) nounwind
  %tmp.49 = fadd float %C.load.1, %tmp.46, !dbg !135 ; [#uses=1 type=float] [debug line = 176:13]
  store float %tmp.49, float* %C.addr.1, align 4, !dbg !135 ; [debug line = 176:13]
  %rend21 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str13, i64 0, i64 0), i32 %rbegin7) nounwind, !dbg !140 ; [#uses=0 type=i32] [debug line = 177:9]
  %j.2 = add i32 %j5, 1, !dbg !141                ; [#uses=1 type=i32] [debug line = 173:55]
  call void @llvm.dbg.value(metadata !{i32 %j.2}, i64 0, metadata !142), !dbg !141 ; [debug line = 173:55] [debug variable = j]
  br label %14, !dbg !141                         ; [debug line = 173:55]

; <label>:16                                      ; preds = %14
  %rend23 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str12, i64 0, i64 0), i32 %rbegin5) nounwind, !dbg !143 ; [#uses=0 type=i32] [debug line = 178:5]
  %i.5 = add i32 %i4, 1, !dbg !144                ; [#uses=1 type=i32] [debug line = 171:54]
  call void @llvm.dbg.value(metadata !{i32 %i.5}, i64 0, metadata !145), !dbg !144 ; [debug line = 171:54] [debug variable = i]
  br label %12, !dbg !144                         ; [debug line = 171:54]

.preheader:                                       ; preds = %22, %.preheader.preheader
  %i6 = phi i32 [ %i.6, %22 ], [ 0, %.preheader.preheader ] ; [#uses=5 type=i32]
  %exitcond1 = icmp eq i32 %i6, 41, !dbg !132     ; [#uses=1 type=i1] [debug line = 180:43]
  br i1 %exitcond1, label %23, label %17, !dbg !132 ; [debug line = 180:43]

; <label>:17                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str14, i64 0, i64 0)) nounwind, !dbg !146 ; [debug line = 180:60]
  %rbegin6 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str14, i64 0, i64 0)) nounwind, !dbg !146 ; [#uses=1 type=i32] [debug line = 180:60]
  %tmp.40 = icmp eq i32 %i6, 40, !dbg !147        ; [#uses=1 type=i1] [debug line = 184:37]
  %tmp.41 = mul i32 %i6, 41, !dbg !127            ; [#uses=1 type=i32] [debug line = 187:4]
  %tmp.42 = zext i32 %i6 to i64, !dbg !147        ; [#uses=1 type=i64] [debug line = 184:37]
  %e.addr.2 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp.42, !dbg !147 ; [#uses=1 type=float*] [debug line = 184:37]
  br label %18, !dbg !148                         ; [debug line = 182:44]

; <label>:18                                      ; preds = %._crit_edge11, %17
  %j7 = phi i32 [ 0, %17 ], [ %j.3, %._crit_edge11 ] ; [#uses=5 type=i32]
  %exitcond = icmp eq i32 %j7, 41, !dbg !148      ; [#uses=1 type=i1] [debug line = 182:44]
  br i1 %exitcond, label %22, label %19, !dbg !148 ; [debug line = 182:44]

; <label>:19                                      ; preds = %18
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str15, i64 0, i64 0)) nounwind, !dbg !149 ; [debug line = 182:61]
  %rbegin8 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str15, i64 0, i64 0)) nounwind, !dbg !149 ; [#uses=1 type=i32] [debug line = 182:61]
  br i1 %tmp.40, label %._crit_edge, label %20, !dbg !147 ; [debug line = 184:37]

; <label>:20                                      ; preds = %19
  %e.load.1 = load float* %e.addr.2, align 4, !dbg !147 ; [#uses=2 type=float] [debug line = 184:37]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %e.load.1) nounwind
  br label %._crit_edge, !dbg !147                ; [debug line = 184:37]

._crit_edge:                                      ; preds = %20, %19
  %ti = phi float [ %e.load.1, %20 ], [ -1.000000e+00, %19 ], !dbg !147 ; [#uses=1 type=float] [debug line = 184:37]
  call void @llvm.dbg.value(metadata !{float %ti}, i64 0, metadata !150), !dbg !147 ; [debug line = 184:37] [debug variable = ti]
  %tmp.52 = icmp eq i32 %j7, 40, !dbg !151        ; [#uses=1 type=i1] [debug line = 185:34]
  br i1 %tmp.52, label %._crit_edge11, label %21, !dbg !151 ; [debug line = 185:34]

; <label>:21                                      ; preds = %._crit_edge
  %tmp.53 = zext i32 %j7 to i64, !dbg !151        ; [#uses=1 type=i64] [debug line = 185:34]
  %e.addr.3 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp.53, !dbg !151 ; [#uses=1 type=float*] [debug line = 185:34]
  %e.load.2 = load float* %e.addr.3, align 4, !dbg !151 ; [#uses=2 type=float] [debug line = 185:34]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %e.load.2) nounwind
  br label %._crit_edge11, !dbg !151              ; [debug line = 185:34]

._crit_edge11:                                    ; preds = %21, %._crit_edge
  %tj = phi float [ %e.load.2, %21 ], [ -1.000000e+00, %._crit_edge ], !dbg !151 ; [#uses=1 type=float] [debug line = 185:34]
  call void @llvm.dbg.value(metadata !{float %tj}, i64 0, metadata !152), !dbg !151 ; [debug line = 185:34] [debug variable = tj]
  %tmp.54 = fmul float %ti, %tj, !dbg !127        ; [#uses=1 type=float] [debug line = 187:4]
  %tmp.55 = fpext float %tmp.54 to double, !dbg !127 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp.56 = fdiv double %tmp.55, %tmp.37, !dbg !127 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp.57 = add i32 %j7, %tmp.41, !dbg !127       ; [#uses=1 type=i32] [debug line = 187:4]
  %tmp.58 = zext i32 %tmp.57 to i64, !dbg !127    ; [#uses=1 type=i64] [debug line = 187:4]
  %Q.addr.1 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.58, !dbg !127 ; [#uses=2 type=float*] [debug line = 187:4]
  %Q.load.1 = load float* %Q.addr.1, align 4, !dbg !127 ; [#uses=2 type=float] [debug line = 187:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.1) nounwind
  %tmp.59 = fpext float %Q.load.1 to double, !dbg !127 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp.60 = fadd double %tmp.59, %tmp.56, !dbg !127 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp.61 = fptrunc double %tmp.60 to float, !dbg !127 ; [#uses=1 type=float] [debug line = 187:4]
  store float %tmp.61, float* %Q.addr.1, align 4, !dbg !127 ; [debug line = 187:4]
  %rend25 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str15, i64 0, i64 0), i32 %rbegin8) nounwind, !dbg !153 ; [#uses=0 type=i32] [debug line = 188:6]
  %j.3 = add i32 %j7, 1, !dbg !154                ; [#uses=1 type=i32] [debug line = 182:55]
  call void @llvm.dbg.value(metadata !{i32 %j.3}, i64 0, metadata !155), !dbg !154 ; [debug line = 182:55] [debug variable = j]
  br label %18, !dbg !154                         ; [debug line = 182:55]

; <label>:22                                      ; preds = %18
  %rend27 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str14, i64 0, i64 0), i32 %rbegin6) nounwind, !dbg !156 ; [#uses=0 type=i32] [debug line = 189:5]
  %i.6 = add i32 %i6, 1, !dbg !157                ; [#uses=1 type=i32] [debug line = 180:54]
  call void @llvm.dbg.value(metadata !{i32 %i.6}, i64 0, metadata !158), !dbg !157 ; [debug line = 180:54] [debug variable = i]
  br label %.preheader, !dbg !157                 ; [debug line = 180:54]

; <label>:23                                      ; preds = %.preheader
  call fastcc void @copyBV(float* %pX), !dbg !159 ; [debug line = 208:5]
  %index = call fastcc i32 @getMinKLApprox(), !dbg !160 ; [#uses=1 type=i32] [debug line = 210:23]
  call void @llvm.dbg.value(metadata !{i32 %index}, i64 0, metadata !161), !dbg !160 ; [debug line = 210:23] [debug variable = index]
  call fastcc void @deleteBV(i32 %index), !dbg !162 ; [debug line = 211:2]
  ret void, !dbg !163                             ; [debug line = 212:1]
}

; [#uses=2]
define internal fastcc void @swapRowAndColumn(float* %pM, i32 %rowA) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pM}, i64 0, metadata !164), !dbg !165 ; [debug line = 27:29] [debug variable = pM]
  call void @llvm.dbg.value(metadata !{i32 %rowA}, i64 0, metadata !166), !dbg !167 ; [debug line = 27:65] [debug variable = rowA]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pM, i32 1681) nounwind, !dbg !168 ; [debug line = 27:92]
  %tmp = mul i32 %rowA, 41, !dbg !170             ; [#uses=1 type=i32] [debug line = 30:40]
  br label %1, !dbg !173                          ; [debug line = 28:28]

; <label>:1                                       ; preds = %2, %0
  %i = phi i32 [ 0, %0 ], [ %i.7, %2 ]            ; [#uses=4 type=i32]
  %exitcond1 = icmp eq i32 %i, 41, !dbg !173      ; [#uses=1 type=i1] [debug line = 28:28]
  br i1 %exitcond1, label %.preheader.preheader, label %2, !dbg !173 ; [debug line = 28:28]

.preheader.preheader:                             ; preds = %1
  br label %.preheader, !dbg !174                 ; [debug line = 35:28]

; <label>:2                                       ; preds = %1
  %tmp.63 = add i32 %i, %tmp, !dbg !170           ; [#uses=1 type=i32] [debug line = 30:40]
  %tmp.64 = zext i32 %tmp.63 to i64, !dbg !170    ; [#uses=1 type=i64] [debug line = 30:40]
  %pM.addr = getelementptr inbounds float* %pM, i64 %tmp.64, !dbg !170 ; [#uses=2 type=float*] [debug line = 30:40]
  %temp = load float* %pM.addr, align 4, !dbg !170 ; [#uses=2 type=float] [debug line = 30:40]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %temp) nounwind
  call void @llvm.dbg.value(metadata !{float %temp}, i64 0, metadata !176), !dbg !170 ; [debug line = 30:40] [debug variable = temp]
  %tmp.65 = add i32 %i, 1640, !dbg !177           ; [#uses=1 type=i32] [debug line = 31:9]
  %tmp.66 = zext i32 %tmp.65 to i64, !dbg !177    ; [#uses=1 type=i64] [debug line = 31:9]
  %pM.addr.1 = getelementptr inbounds float* %pM, i64 %tmp.66, !dbg !177 ; [#uses=2 type=float*] [debug line = 31:9]
  %pM.load = load float* %pM.addr.1, align 4, !dbg !177 ; [#uses=2 type=float] [debug line = 31:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pM.load) nounwind
  store float %pM.load, float* %pM.addr, align 4, !dbg !177 ; [debug line = 31:9]
  store float %temp, float* %pM.addr.1, align 4, !dbg !178 ; [debug line = 32:9]
  %i.7 = add i32 %i, 1, !dbg !179                 ; [#uses=1 type=i32] [debug line = 28:43]
  call void @llvm.dbg.value(metadata !{i32 %i.7}, i64 0, metadata !180), !dbg !179 ; [debug line = 28:43] [debug variable = i]
  br label %1, !dbg !179                          ; [debug line = 28:43]

.preheader:                                       ; preds = %3, %.preheader.preheader
  %i1 = phi i32 [ %i.8, %3 ], [ 0, %.preheader.preheader ] ; [#uses=3 type=i32]
  %exitcond = icmp eq i32 %i1, 41, !dbg !174      ; [#uses=1 type=i1] [debug line = 35:28]
  br i1 %exitcond, label %4, label %3, !dbg !174  ; [debug line = 35:28]

; <label>:3                                       ; preds = %.preheader
  %tmp.68 = mul i32 %i1, 41, !dbg !181            ; [#uses=2 type=i32] [debug line = 37:40]
  %tmp.69 = add i32 %tmp.68, %rowA, !dbg !181     ; [#uses=1 type=i32] [debug line = 37:40]
  %tmp.70 = zext i32 %tmp.69 to i64, !dbg !181    ; [#uses=1 type=i64] [debug line = 37:40]
  %pM.addr.2 = getelementptr inbounds float* %pM, i64 %tmp.70, !dbg !181 ; [#uses=2 type=float*] [debug line = 37:40]
  %temp.1 = load float* %pM.addr.2, align 4, !dbg !181 ; [#uses=2 type=float] [debug line = 37:40]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %temp.1) nounwind
  call void @llvm.dbg.value(metadata !{float %temp.1}, i64 0, metadata !183), !dbg !181 ; [debug line = 37:40] [debug variable = temp]
  %tmp.71 = add i32 %tmp.68, 40, !dbg !184        ; [#uses=1 type=i32] [debug line = 38:9]
  %tmp.72 = zext i32 %tmp.71 to i64, !dbg !184    ; [#uses=1 type=i64] [debug line = 38:9]
  %pM.addr.3 = getelementptr inbounds float* %pM, i64 %tmp.72, !dbg !184 ; [#uses=2 type=float*] [debug line = 38:9]
  %pM.load.2 = load float* %pM.addr.3, align 4, !dbg !184 ; [#uses=2 type=float] [debug line = 38:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pM.load.2) nounwind
  store float %pM.load.2, float* %pM.addr.2, align 4, !dbg !184 ; [debug line = 38:9]
  store float %temp.1, float* %pM.addr.3, align 4, !dbg !185 ; [debug line = 39:9]
  %i.8 = add i32 %i1, 1, !dbg !186                ; [#uses=1 type=i32] [debug line = 35:43]
  call void @llvm.dbg.value(metadata !{i32 %i.8}, i64 0, metadata !187), !dbg !186 ; [debug line = 35:43] [debug variable = i]
  br label %.preheader, !dbg !186                 ; [debug line = 35:43]

; <label>:4                                       ; preds = %.preheader
  ret void, !dbg !188                             ; [debug line = 41:1]
}

; [#uses=1]
define internal fastcc float @"std::exp"(float %__x) nounwind uwtable inlinehint {
  call void @llvm.dbg.value(metadata !{float %__x}, i64 0, metadata !189), !dbg !190 ; [debug line = 216:13] [debug variable = __x]
  %tmp = call float @llvm.exp.f32(float %__x), !dbg !191 ; [#uses=1 type=float] [debug line = 217:12]
  ret float %tmp, !dbg !191                       ; [debug line = 217:12]
}

; [#uses=0]
define float @projection_gp(float* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pReset) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !193
  call void (...)* @_ssdm_op_SpecTopModule([14 x i8]* @projection_gp.str) nounwind
  call void @llvm.dbg.value(metadata !{float* %pX}, i64 0, metadata !199), !dbg !200 ; [debug line = 221:33] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !201), !dbg !202 ; [debug line = 221:53] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{i1 %pPredict}, i64 0, metadata !203), !dbg !204 ; [debug line = 221:68] [debug variable = pPredict]
  call void @llvm.dbg.value(metadata !{i1 %pReset}, i64 0, metadata !205), !dbg !206 ; [debug line = 221:89] [debug variable = pReset]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX, i32 13) nounwind, !dbg !207 ; [debug line = 221:98]
  call void (...)* @_ssdm_op_SpecInterface(float* %pX, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 13, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !209 ; [debug line = 222:1]
  %tmp = fpext float %pY to double, !dbg !210     ; [#uses=1 type=double] [debug line = 223:1]
  call void (...)* @_ssdm_op_SpecInterface(double %tmp, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !210 ; [debug line = 223:1]
  %tmp.74 = zext i1 %pPredict to i32, !dbg !211   ; [#uses=1 type=i32] [debug line = 224:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 %tmp.74, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !211 ; [debug line = 224:1]
  %tmp.75 = zext i1 %pReset to i32, !dbg !212     ; [#uses=1 type=i32] [debug line = 225:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 %tmp.75, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !212 ; [debug line = 225:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 0, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !213 ; [debug line = 226:1]
  br i1 %pReset, label %.preheader3.preheader, label %.loopexit4, !dbg !214 ; [debug line = 228:2]

.preheader3.preheader:                            ; preds = %0
  br label %.preheader3, !dbg !215                ; [debug line = 229:37]

.preheader3:                                      ; preds = %4, %.preheader3.preheader
  %i = phi i32 [ %i.9, %4 ], [ 0, %.preheader3.preheader ] ; [#uses=5 type=i32]
  %exitcond2 = icmp eq i32 %i, 41, !dbg !215      ; [#uses=1 type=i1] [debug line = 229:37]
  br i1 %exitcond2, label %.loopexit4.loopexit, label %1, !dbg !215 ; [debug line = 229:37]

; <label>:1                                       ; preds = %.preheader3
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([11 x i8]* @.str18, i64 0, i64 0)) nounwind, !dbg !218 ; [debug line = 229:58]
  %rbegin6 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([11 x i8]* @.str18, i64 0, i64 0)) nounwind, !dbg !218 ; [#uses=1 type=i32] [debug line = 229:58]
  %tmp.76 = mul i32 %i, 14, !dbg !220             ; [#uses=1 type=i32] [debug line = 234:6]
  %tmp.77 = zext i32 %tmp.76 to i64, !dbg !220    ; [#uses=2 type=i64] [debug line = 234:6]
  %Q.addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.77, !dbg !220 ; [#uses=1 type=float*] [debug line = 234:6]
  %C.addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.77, !dbg !224 ; [#uses=1 type=float*] [debug line = 235:6]
  br label %2, !dbg !225                          ; [debug line = 231:39]

; <label>:2                                       ; preds = %3, %1
  %j = phi i32 [ 0, %1 ], [ %j.4, %3 ]            ; [#uses=3 type=i32]
  %exitcond1 = icmp eq i32 %j, 41, !dbg !225      ; [#uses=1 type=i1] [debug line = 231:39]
  br i1 %exitcond1, label %4, label %3, !dbg !225 ; [debug line = 231:39]

; <label>:3                                       ; preds = %2
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([11 x i8]* @.str19, i64 0, i64 0)) nounwind, !dbg !226 ; [debug line = 231:60]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([11 x i8]* @.str19, i64 0, i64 0)) nounwind, !dbg !226 ; [#uses=1 type=i32] [debug line = 231:60]
  %tmp.81 = icmp eq i32 %i, %j, !dbg !227         ; [#uses=1 type=i1] [debug line = 233:5]
  %storemerge5 = select i1 %tmp.81, float 1.000000e+00, float 0.000000e+00, !dbg !227 ; [#uses=2 type=float] [debug line = 233:5]
  store float %storemerge5, float* %Q.addr, align 8, !dbg !228 ; [debug line = 237:6]
  store float %storemerge5, float* %C.addr, align 8, !dbg !224 ; [debug line = 235:6]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([11 x i8]* @.str19, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !230 ; [#uses=0 type=i32] [debug line = 240:4]
  %j.4 = add i32 %j, 1, !dbg !231                 ; [#uses=1 type=i32] [debug line = 231:54]
  call void @llvm.dbg.value(metadata !{i32 %j.4}, i64 0, metadata !232), !dbg !231 ; [debug line = 231:54] [debug variable = j]
  br label %2, !dbg !231                          ; [debug line = 231:54]

; <label>:4                                       ; preds = %2
  %tmp.79 = zext i32 %i to i64, !dbg !233         ; [#uses=1 type=i64] [debug line = 241:4]
  %alpha.addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.79, !dbg !233 ; [#uses=1 type=float*] [debug line = 241:4]
  store float 0.000000e+00, float* %alpha.addr, align 4, !dbg !233 ; [debug line = 241:4]
  %rend7 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([11 x i8]* @.str18, i64 0, i64 0), i32 %rbegin6) nounwind, !dbg !234 ; [#uses=0 type=i32] [debug line = 242:3]
  %i.9 = add i32 %i, 1, !dbg !235                 ; [#uses=1 type=i32] [debug line = 229:52]
  call void @llvm.dbg.value(metadata !{i32 %i.9}, i64 0, metadata !236), !dbg !235 ; [debug line = 229:52] [debug variable = i]
  br label %.preheader3, !dbg !235                ; [debug line = 229:52]

.loopexit4.loopexit:                              ; preds = %.preheader3
  br label %.loopexit4

.loopexit4:                                       ; preds = %.loopexit4.loopexit, %0
  br i1 %pPredict, label %.preheader.preheader, label %5, !dbg !237 ; [debug line = 245:2]

.preheader.preheader:                             ; preds = %.loopexit4
  br label %.preheader, !dbg !238                 ; [debug line = 255:37]

; <label>:5                                       ; preds = %.loopexit4
  %bvCnt.load = load i32* @bvCnt, align 4, !dbg !241 ; [#uses=1 type=i32] [debug line = 246:3]
  %tmp.78 = icmp eq i32 %bvCnt.load, 40, !dbg !241 ; [#uses=1 type=i1] [debug line = 246:3]
  br i1 %tmp.78, label %6, label %7, !dbg !241    ; [debug line = 246:3]

; <label>:6                                       ; preds = %5
  call fastcc void @train_full_bv_set(float* %pX, float %pY), !dbg !243 ; [debug line = 247:4]
  br label %8, !dbg !245                          ; [debug line = 248:3]

; <label>:7                                       ; preds = %5
  call fastcc void @copyBV(float* %pX), !dbg !246 ; [debug line = 249:4]
  %bvCnt.load.1 = load i32* @bvCnt, align 4, !dbg !248 ; [#uses=1 type=i32] [debug line = 250:4]
  %tmp.83 = add i32 %bvCnt.load.1, 1, !dbg !248   ; [#uses=1 type=i32] [debug line = 250:4]
  store i32 %tmp.83, i32* @bvCnt, align 4, !dbg !248 ; [debug line = 250:4]
  br label %8

; <label>:8                                       ; preds = %7, %6
  br label %.loopexit, !dbg !249                  ; [debug line = 252:3]

.preheader:                                       ; preds = %9, %.preheader.preheader
  %sum = phi float [ %sum.1, %9 ], [ 0x4050B830E0000000, %.preheader.preheader ] ; [#uses=2 type=float]
  %i1 = phi i32 [ %i.10, %9 ], [ 0, %.preheader.preheader ] ; [#uses=4 type=i32]
  %exitcond = icmp eq i32 %i1, 40, !dbg !238      ; [#uses=1 type=i1] [debug line = 255:37]
  br i1 %exitcond, label %.loopexit.loopexit, label %9, !dbg !238 ; [debug line = 255:37]

; <label>:9                                       ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([11 x i8]* @.str20, i64 0, i64 0)) nounwind, !dbg !250 ; [debug line = 255:53]
  %rbegin8 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([11 x i8]* @.str20, i64 0, i64 0)) nounwind, !dbg !250 ; [#uses=1 type=i32] [debug line = 255:53]
  %tmp.84 = mul i32 %i1, 13, !dbg !252            ; [#uses=1 type=i32] [debug line = 257:11]
  %tmp.85 = zext i32 %tmp.84 to i64, !dbg !252    ; [#uses=1 type=i64] [debug line = 257:11]
  %basisVectors.addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.85, !dbg !252 ; [#uses=1 type=float*] [debug line = 257:11]
  %tmp.86 = call fastcc float @K(float* %basisVectors.addr, float* %pX), !dbg !252 ; [#uses=1 type=float] [debug line = 257:11]
  %tmp.87 = zext i32 %i1 to i64, !dbg !252        ; [#uses=1 type=i64] [debug line = 257:11]
  %alpha.addr.2 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.87, !dbg !252 ; [#uses=1 type=float*] [debug line = 257:11]
  %alpha.load = load float* %alpha.addr.2, align 4, !dbg !252 ; [#uses=2 type=float] [debug line = 257:11]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  %tmp.88 = fmul float %tmp.86, %alpha.load, !dbg !252 ; [#uses=1 type=float] [debug line = 257:11]
  %sum.1 = fadd float %sum, %tmp.88, !dbg !252    ; [#uses=1 type=float] [debug line = 257:11]
  call void @llvm.dbg.value(metadata !{float %sum.1}, i64 0, metadata !253), !dbg !252 ; [debug line = 257:11] [debug variable = sum]
  %rend9 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([11 x i8]* @.str20, i64 0, i64 0), i32 %rbegin8) nounwind, !dbg !254 ; [#uses=0 type=i32] [debug line = 258:3]
  %i.10 = add i32 %i1, 1, !dbg !255               ; [#uses=1 type=i32] [debug line = 255:47]
  call void @llvm.dbg.value(metadata !{i32 %i.10}, i64 0, metadata !256), !dbg !255 ; [debug line = 255:47] [debug variable = i]
  br label %.preheader, !dbg !255                 ; [debug line = 255:47]

.loopexit.loopexit:                               ; preds = %.preheader
  %sum.0.lcssa = phi float [ %sum, %.preheader ]  ; [#uses=1 type=float]
  br label %.loopexit

.loopexit:                                        ; preds = %.loopexit.loopexit, %8
  %.0 = phi float [ 0.000000e+00, %8 ], [ %sum.0.lcssa, %.loopexit.loopexit ] ; [#uses=1 type=float]
  ret float %.0, !dbg !257                        ; [debug line = 262:1]
}

; [#uses=1]
declare float @llvm.exp.f32(float) nounwind readonly

; [#uses=68]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=1]
define internal fastcc i32 @getMinKLApprox() nounwind uwtable {
  %alpha.load = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 0), align 16, !dbg !258 ; [#uses=4 type=float] [debug line = 108:77]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  %tmp = fmul float %alpha.load, %alpha.load, !dbg !258 ; [#uses=1 type=float] [debug line = 108:77]
  %Q.load = load float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 0), align 16, !dbg !258 ; [#uses=2 type=float] [debug line = 108:77]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load) nounwind
  %C.load = load float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 0), align 16, !dbg !258 ; [#uses=2 type=float] [debug line = 108:77]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load) nounwind
  %tmp.91 = fadd float %Q.load, %C.load, !dbg !258 ; [#uses=1 type=float] [debug line = 108:77]
  %minScore = fdiv float %tmp, %tmp.91, !dbg !258 ; [#uses=1 type=float] [debug line = 108:77]
  call void @llvm.dbg.value(metadata !{float %minScore}, i64 0, metadata !260), !dbg !258 ; [debug line = 108:77] [debug variable = minScore]
  br label %1, !dbg !261                          ; [debug line = 110:28]

; <label>:1                                       ; preds = %2, %0
  %index.2 = phi i32 [ 1, %0 ], [ %i, %2 ]        ; [#uses=5 type=i32]
  %minScore1 = phi float [ %minScore, %0 ], [ %minScore.1, %2 ] ; [#uses=2 type=float]
  %index = phi i32 [ 0, %0 ], [ %index.1, %2 ]    ; [#uses=2 type=i32]
  %exitcond = icmp eq i32 %index.2, 41, !dbg !261 ; [#uses=1 type=i1] [debug line = 110:28]
  br i1 %exitcond, label %3, label %2, !dbg !261  ; [debug line = 110:28]

; <label>:2                                       ; preds = %1
  %tmp.93 = zext i32 %index.2 to i64, !dbg !263   ; [#uses=1 type=i64] [debug line = 111:79]
  %alpha.addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.93, !dbg !263 ; [#uses=1 type=float*] [debug line = 111:79]
  %alpha.load.3 = load float* %alpha.addr, align 4, !dbg !263 ; [#uses=4 type=float] [debug line = 111:79]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.3) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.3) nounwind
  %tmp.94 = fmul float %alpha.load.3, %alpha.load.3, !dbg !263 ; [#uses=1 type=float] [debug line = 111:79]
  %tmp.95 = mul i32 %index.2, 42, !dbg !263       ; [#uses=1 type=i32] [debug line = 111:79]
  %tmp.96 = zext i32 %tmp.95 to i64, !dbg !263    ; [#uses=2 type=i64] [debug line = 111:79]
  %Q.addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.96, !dbg !263 ; [#uses=1 type=float*] [debug line = 111:79]
  %Q.load.2 = load float* %Q.addr, align 8, !dbg !263 ; [#uses=2 type=float] [debug line = 111:79]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.2) nounwind
  %C.addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.96, !dbg !263 ; [#uses=1 type=float*] [debug line = 111:79]
  %C.load.2 = load float* %C.addr, align 8, !dbg !263 ; [#uses=2 type=float] [debug line = 111:79]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.2) nounwind
  %tmp.97 = fadd float %Q.load.2, %C.load.2, !dbg !263 ; [#uses=1 type=float] [debug line = 111:79]
  %tScore = fdiv float %tmp.94, %tmp.97, !dbg !263 ; [#uses=2 type=float] [debug line = 111:79]
  call void @llvm.dbg.value(metadata !{float %tScore}, i64 0, metadata !265), !dbg !263 ; [debug line = 111:79] [debug variable = tScore]
  %tmp.98 = fcmp olt float %tScore, %minScore1, !dbg !266 ; [#uses=2 type=i1] [debug line = 113:9]
  call void @llvm.dbg.value(metadata !{float %tScore}, i64 0, metadata !260), !dbg !267 ; [debug line = 114:13] [debug variable = minScore]
  call void @llvm.dbg.value(metadata !{i32 %index.2}, i64 0, metadata !269), !dbg !270 ; [debug line = 115:13] [debug variable = index]
  %minScore.1 = select i1 %tmp.98, float %tScore, float %minScore1, !dbg !266 ; [#uses=1 type=float] [debug line = 113:9]
  call void @llvm.dbg.value(metadata !{float %minScore.1}, i64 0, metadata !260), !dbg !266 ; [debug line = 113:9] [debug variable = minScore]
  %index.1 = select i1 %tmp.98, i32 %index.2, i32 %index, !dbg !266 ; [#uses=1 type=i32] [debug line = 113:9]
  call void @llvm.dbg.value(metadata !{i32 %index.1}, i64 0, metadata !269), !dbg !266 ; [debug line = 113:9] [debug variable = index]
  %i = add i32 %index.2, 1, !dbg !271             ; [#uses=1 type=i32] [debug line = 110:43]
  call void @llvm.dbg.value(metadata !{i32 %i}, i64 0, metadata !272), !dbg !271 ; [debug line = 110:43] [debug variable = i]
  br label %1, !dbg !271                          ; [debug line = 110:43]

; <label>:3                                       ; preds = %1
  %index.0.lcssa = phi i32 [ %index, %1 ]         ; [#uses=1 type=i32]
  ret i32 %index.0.lcssa, !dbg !273               ; [debug line = 119:5]
}

; [#uses=1]
define internal fastcc void @deleteBV(i32 %pIndex) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{i32 %pIndex}, i64 0, metadata !274), !dbg !275 ; [debug line = 43:29] [debug variable = pIndex]
  %tmp = zext i32 %pIndex to i64, !dbg !276       ; [#uses=1 type=i64] [debug line = 47:31]
  %alpha.addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp, !dbg !276 ; [#uses=2 type=float*] [debug line = 47:31]
  %temp = load float* %alpha.addr, align 4, !dbg !276 ; [#uses=2 type=float] [debug line = 47:31]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %temp) nounwind
  call void @llvm.dbg.value(metadata !{float %temp}, i64 0, metadata !278), !dbg !276 ; [debug line = 47:31] [debug variable = temp]
  %alpha.load = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !279 ; [#uses=2 type=float] [debug line = 48:5]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  store float %alpha.load, float* %alpha.addr, align 4, !dbg !279 ; [debug line = 48:5]
  store float %temp, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !280 ; [debug line = 49:5]
  call fastcc void @swapRowAndColumn(float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 0), i32 %pIndex), !dbg !281 ; [debug line = 51:5]
  call fastcc void @swapRowAndColumn(float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 0), i32 %pIndex), !dbg !282 ; [debug line = 52:5]
  %tmp.100 = mul i32 %pIndex, 13, !dbg !283       ; [#uses=1 type=i32] [debug line = 56:2]
  br label %1, !dbg !286                          ; [debug line = 54:48]

; <label>:1                                       ; preds = %2, %0
  %i = phi i32 [ 0, %0 ], [ %i.11, %2 ]           ; [#uses=4 type=i32]
  %exitcond4 = icmp eq i32 %i, 13, !dbg !286      ; [#uses=1 type=i1] [debug line = 54:48]
  br i1 %exitcond4, label %3, label %2, !dbg !286 ; [debug line = 54:48]

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([20 x i8]* @.str2, i64 0, i64 0)) nounwind, !dbg !287 ; [debug line = 54:64]
  %rbegin9 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([20 x i8]* @.str2, i64 0, i64 0)) nounwind, !dbg !287 ; [#uses=1 type=i32] [debug line = 54:64]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !288 ; [debug line = 55:1]
  %tmp.103 = add i32 %i, %tmp.100, !dbg !283      ; [#uses=1 type=i32] [debug line = 56:2]
  %tmp.104 = zext i32 %tmp.103 to i64, !dbg !283  ; [#uses=1 type=i64] [debug line = 56:2]
  %basisVectors.addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.104, !dbg !283 ; [#uses=2 type=float*] [debug line = 56:2]
  %temp.3 = load float* %basisVectors.addr, align 4, !dbg !283 ; [#uses=2 type=float] [debug line = 56:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %temp.3) nounwind
  call void @llvm.dbg.value(metadata !{float %temp.3}, i64 0, metadata !278), !dbg !283 ; [debug line = 56:2] [debug variable = temp]
  %tmp.105 = add i32 %i, 520, !dbg !289           ; [#uses=1 type=i32] [debug line = 57:9]
  %tmp.106 = zext i32 %tmp.105 to i64, !dbg !289  ; [#uses=1 type=i64] [debug line = 57:9]
  %basisVectors.addr.1 = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.106, !dbg !289 ; [#uses=2 type=float*] [debug line = 57:9]
  %basisVectors.load = load float* %basisVectors.addr.1, align 4, !dbg !289 ; [#uses=2 type=float] [debug line = 57:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %basisVectors.load) nounwind
  store float %basisVectors.load, float* %basisVectors.addr, align 4, !dbg !289 ; [debug line = 57:9]
  store float %temp.3, float* %basisVectors.addr.1, align 4, !dbg !290 ; [debug line = 58:9]
  %rend10 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([20 x i8]* @.str2, i64 0, i64 0), i32 %rbegin9) nounwind, !dbg !291 ; [#uses=0 type=i32] [debug line = 59:5]
  %i.11 = add i32 %i, 1, !dbg !292                ; [#uses=1 type=i32] [debug line = 54:58]
  call void @llvm.dbg.value(metadata !{i32 %i.11}, i64 0, metadata !293), !dbg !292 ; [debug line = 54:58] [debug variable = i]
  br label %1, !dbg !292                          ; [debug line = 54:58]

; <label>:3                                       ; preds = %1
  %alphaStar = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !294 ; [#uses=2 type=float] [debug line = 62:32]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alphaStar) nounwind
  call void @llvm.dbg.value(metadata !{float %alphaStar}, i64 0, metadata !295), !dbg !294 ; [debug line = 62:32] [debug variable = alphaStar]
  %cStar = load float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 1680), align 16, !dbg !296 ; [#uses=2 type=float] [debug line = 63:24]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %cStar) nounwind
  call void @llvm.dbg.value(metadata !{float %cStar}, i64 0, metadata !297), !dbg !296 ; [debug line = 63:24] [debug variable = cStar]
  %qStar = load float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 1680), align 16, !dbg !298 ; [#uses=3 type=float] [debug line = 81:45]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %qStar) nounwind
  call void @llvm.dbg.value(metadata !{float %qStar}, i64 0, metadata !303), !dbg !304 ; [debug line = 64:24] [debug variable = qStar]
  %tmp.101 = fadd float %cStar, %qStar, !dbg !305 ; [#uses=2 type=float] [debug line = 65:5]
  %temp.2 = fdiv float %alphaStar, %tmp.101, !dbg !305 ; [#uses=1 type=float] [debug line = 65:5]
  call void @llvm.dbg.value(metadata !{float %temp.2}, i64 0, metadata !278), !dbg !305 ; [debug line = 65:5] [debug variable = temp]
  br label %4, !dbg !306                          ; [debug line = 67:44]

; <label>:4                                       ; preds = %5, %3
  %i1 = phi i32 [ 0, %3 ], [ %i.12, %5 ]          ; [#uses=4 type=i32]
  %exitcond3 = icmp eq i32 %i1, 40, !dbg !306     ; [#uses=1 type=i1] [debug line = 67:44]
  br i1 %exitcond3, label %6, label %5, !dbg !306 ; [debug line = 67:44]

; <label>:5                                       ; preds = %4
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([16 x i8]* @.str3, i64 0, i64 0)) nounwind, !dbg !308 ; [debug line = 67:60]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([16 x i8]* @.str3, i64 0, i64 0)) nounwind, !dbg !308 ; [#uses=1 type=i32] [debug line = 67:60]
  %a = add i32 %i1, 1640, !dbg !310               ; [#uses=1 type=i32] [debug line = 69:35]
  call void @llvm.dbg.value(metadata !{i32 %a}, i64 0, metadata !311), !dbg !310 ; [debug line = 69:35] [debug variable = a]
  %tmp.108 = zext i32 %a to i64, !dbg !312        ; [#uses=2 type=i64] [debug line = 70:9]
  %C.addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.108, !dbg !312 ; [#uses=1 type=float*] [debug line = 70:9]
  %C.load = load float* %C.addr, align 4, !dbg !312 ; [#uses=2 type=float] [debug line = 70:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load) nounwind
  %Q.addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.108, !dbg !312 ; [#uses=1 type=float*] [debug line = 70:9]
  %Q.load = load float* %Q.addr, align 4, !dbg !312 ; [#uses=2 type=float] [debug line = 70:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load) nounwind
  %tmp.109 = fadd float %C.load, %Q.load, !dbg !312 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp.110 = fmul float %tmp.109, %temp.2, !dbg !312 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp.111 = zext i32 %i1 to i64, !dbg !312       ; [#uses=1 type=i64] [debug line = 70:9]
  %alpha.addr.3 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.111, !dbg !312 ; [#uses=2 type=float*] [debug line = 70:9]
  %alpha.load.5 = load float* %alpha.addr.3, align 4, !dbg !312 ; [#uses=2 type=float] [debug line = 70:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.5) nounwind
  %tmp.112 = fsub float %alpha.load.5, %tmp.110, !dbg !312 ; [#uses=1 type=float] [debug line = 70:9]
  store float %tmp.112, float* %alpha.addr.3, align 4, !dbg !312 ; [debug line = 70:9]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([16 x i8]* @.str3, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !313 ; [#uses=0 type=i32] [debug line = 71:5]
  %i.12 = add i32 %i1, 1, !dbg !314               ; [#uses=1 type=i32] [debug line = 67:54]
  call void @llvm.dbg.value(metadata !{i32 %i.12}, i64 0, metadata !315), !dbg !314 ; [debug line = 67:54] [debug variable = i]
  br label %4, !dbg !314                          ; [debug line = 67:54]

; <label>:6                                       ; preds = %4
  store float 0.000000e+00, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !316 ; [debug line = 72:5]
  br label %7, !dbg !317                          ; [debug line = 74:45]

; <label>:7                                       ; preds = %11, %6
  %i2 = phi i32 [ 0, %6 ], [ %i.14, %11 ]         ; [#uses=4 type=i32]
  %exitcond2 = icmp eq i32 %i2, 40, !dbg !317     ; [#uses=1 type=i1] [debug line = 74:45]
  br i1 %exitcond2, label %.preheader.preheader, label %8, !dbg !317 ; [debug line = 74:45]

.preheader.preheader:                             ; preds = %7
  br label %.preheader, !dbg !318                 ; [debug line = 94:48]

; <label>:8                                       ; preds = %7
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([18 x i8]* @.str4, i64 0, i64 0)) nounwind, !dbg !320 ; [debug line = 74:61]
  %rbegin7 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([18 x i8]* @.str4, i64 0, i64 0)) nounwind, !dbg !320 ; [#uses=1 type=i32] [debug line = 74:61]
  %a.2 = add i32 %i2, 1640, !dbg !321             ; [#uses=1 type=i32] [debug line = 78:39]
  %tmp.114 = mul i32 %i2, 41, !dbg !322           ; [#uses=1 type=i32] [debug line = 80:38]
  %tmp.115 = zext i32 %a.2 to i64, !dbg !298      ; [#uses=2 type=i64] [debug line = 81:45]
  %Q.addr.2 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.115, !dbg !298 ; [#uses=1 type=float*] [debug line = 81:45]
  %C.addr.2 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.115, !dbg !323 ; [#uses=1 type=float*] [debug line = 84:13]
  br label %9, !dbg !324                          ; [debug line = 76:47]

; <label>:9                                       ; preds = %10, %8
  %j = phi i32 [ 0, %8 ], [ %j.5, %10 ]           ; [#uses=4 type=i32]
  %exitcond1 = icmp eq i32 %j, 40, !dbg !324      ; [#uses=1 type=i1] [debug line = 76:47]
  br i1 %exitcond1, label %11, label %10, !dbg !324 ; [debug line = 76:47]

; <label>:10                                      ; preds = %9
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([18 x i8]* @.str5, i64 0, i64 0)) nounwind, !dbg !325 ; [debug line = 76:63]
  %rbegin5 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([18 x i8]* @.str5, i64 0, i64 0)) nounwind, !dbg !325 ; [#uses=1 type=i32] [debug line = 76:63]
  call void @llvm.dbg.value(metadata !{i32 %a.2}, i64 0, metadata !326), !dbg !321 ; [debug line = 78:39] [debug variable = a]
  %b.2 = add i32 %j, 1640, !dbg !327              ; [#uses=1 type=i32] [debug line = 79:39]
  call void @llvm.dbg.value(metadata !{i32 %b.2}, i64 0, metadata !328), !dbg !327 ; [debug line = 79:39] [debug variable = b]
  %c = add i32 %j, %tmp.114, !dbg !322            ; [#uses=1 type=i32] [debug line = 80:38]
  call void @llvm.dbg.value(metadata !{i32 %c}, i64 0, metadata !329), !dbg !322 ; [debug line = 80:38] [debug variable = c]
  %Q.load.3 = load float* %Q.addr.2, align 4, !dbg !298 ; [#uses=5 type=float] [debug line = 81:45]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.3) nounwind
  %tmp.121 = zext i32 %b.2 to i64, !dbg !298      ; [#uses=2 type=i64] [debug line = 81:45]
  %Q.addr.5 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.121, !dbg !298 ; [#uses=1 type=float*] [debug line = 81:45]
  %Q.load.4 = load float* %Q.addr.5, align 4, !dbg !298 ; [#uses=5 type=float] [debug line = 81:45]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.4) nounwind
  %tmp.122 = fmul float %Q.load.3, %Q.load.4, !dbg !298 ; [#uses=2 type=float] [debug line = 81:45]
  %temp.4 = fdiv float %tmp.122, %qStar, !dbg !298 ; [#uses=2 type=float] [debug line = 81:45]
  call void @llvm.dbg.value(metadata !{float %temp.4}, i64 0, metadata !330), !dbg !298 ; [debug line = 81:45] [debug variable = temp]
  %C.load.3 = load float* %C.addr.2, align 4, !dbg !323 ; [#uses=4 type=float] [debug line = 84:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.3) nounwind
  %C.addr.5 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.121, !dbg !323 ; [#uses=1 type=float*] [debug line = 84:13]
  %C.load.4 = load float* %C.addr.5, align 4, !dbg !323 ; [#uses=4 type=float] [debug line = 84:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.4) nounwind
  %tmp.123 = fmul float %C.load.3, %C.load.4, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.3) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.4) nounwind
  %tmp.124 = fmul float %C.load.3, %Q.load.4, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.125 = fadd float %tmp.123, %tmp.124, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.3) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.4) nounwind
  %tmp.126 = fmul float %Q.load.3, %C.load.4, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.127 = fadd float %tmp.125, %tmp.126, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.3) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.4) nounwind
  %tmp.128 = fadd float %tmp.127, %tmp.122, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.129 = fdiv float %tmp.128, %tmp.101, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.130 = fsub float %temp.4, %tmp.129, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.131 = zext i32 %c to i64, !dbg !323        ; [#uses=2 type=i64] [debug line = 84:13]
  %C.addr.6 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.131, !dbg !323 ; [#uses=2 type=float*] [debug line = 84:13]
  %C.load.5 = load float* %C.addr.6, align 4, !dbg !323 ; [#uses=2 type=float] [debug line = 84:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.5) nounwind
  %tmp.132 = fadd float %C.load.5, %tmp.130, !dbg !323 ; [#uses=1 type=float] [debug line = 84:13]
  store float %tmp.132, float* %C.addr.6, align 4, !dbg !323 ; [debug line = 84:13]
  %Q.addr.6 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.131, !dbg !331 ; [#uses=2 type=float*] [debug line = 90:13]
  %Q.load.5 = load float* %Q.addr.6, align 4, !dbg !331 ; [#uses=2 type=float] [debug line = 90:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.5) nounwind
  %tmp.133 = fsub float %Q.load.5, %temp.4, !dbg !331 ; [#uses=1 type=float] [debug line = 90:13]
  store float %tmp.133, float* %Q.addr.6, align 4, !dbg !331 ; [debug line = 90:13]
  %rend6 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([18 x i8]* @.str5, i64 0, i64 0), i32 %rbegin5) nounwind, !dbg !332 ; [#uses=0 type=i32] [debug line = 91:9]
  %j.5 = add i32 %j, 1, !dbg !333                 ; [#uses=1 type=i32] [debug line = 76:57]
  call void @llvm.dbg.value(metadata !{i32 %j.5}, i64 0, metadata !334), !dbg !333 ; [debug line = 76:57] [debug variable = j]
  br label %9, !dbg !333                          ; [debug line = 76:57]

; <label>:11                                      ; preds = %9
  %rend8 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([18 x i8]* @.str4, i64 0, i64 0), i32 %rbegin7) nounwind, !dbg !335 ; [#uses=0 type=i32] [debug line = 92:5]
  %i.14 = add i32 %i2, 1, !dbg !336               ; [#uses=1 type=i32] [debug line = 74:55]
  call void @llvm.dbg.value(metadata !{i32 %i.14}, i64 0, metadata !337), !dbg !336 ; [debug line = 74:55] [debug variable = i]
  br label %7, !dbg !336                          ; [debug line = 74:55]

.preheader:                                       ; preds = %12, %.preheader.preheader
  %i5 = phi i32 [ %i.13, %12 ], [ 0, %.preheader.preheader ] ; [#uses=4 type=i32]
  %exitcond = icmp eq i32 %i5, 41, !dbg !318      ; [#uses=1 type=i1] [debug line = 94:48]
  br i1 %exitcond, label %13, label %12, !dbg !318 ; [debug line = 94:48]

; <label>:12                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([20 x i8]* @.str6, i64 0, i64 0)) nounwind, !dbg !338 ; [debug line = 94:65]
  %rbegin10 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([20 x i8]* @.str6, i64 0, i64 0)) nounwind, !dbg !338 ; [#uses=1 type=i32] [debug line = 94:65]
  %tmp.116 = mul i32 %i5, 41, !dbg !340           ; [#uses=1 type=i32] [debug line = 96:35]
  %a.1 = add i32 %tmp.116, 40, !dbg !340          ; [#uses=1 type=i32] [debug line = 96:35]
  call void @llvm.dbg.value(metadata !{i32 %a.1}, i64 0, metadata !341), !dbg !340 ; [debug line = 96:35] [debug variable = a]
  %b = add i32 %i5, 1640, !dbg !342               ; [#uses=1 type=i32] [debug line = 97:35]
  call void @llvm.dbg.value(metadata !{i32 %b}, i64 0, metadata !343), !dbg !342 ; [debug line = 97:35] [debug variable = b]
  %tmp.117 = zext i32 %a.1 to i64, !dbg !344      ; [#uses=2 type=i64] [debug line = 99:6]
  %Q.addr.3 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.117, !dbg !344 ; [#uses=1 type=float*] [debug line = 99:6]
  store float 0.000000e+00, float* %Q.addr.3, align 4, !dbg !344 ; [debug line = 99:6]
  %tmp.118 = zext i32 %b to i64, !dbg !345        ; [#uses=2 type=i64] [debug line = 100:9]
  %Q.addr.4 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.118, !dbg !345 ; [#uses=1 type=float*] [debug line = 100:9]
  store float 0.000000e+00, float* %Q.addr.4, align 4, !dbg !345 ; [debug line = 100:9]
  %C.addr.3 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.117, !dbg !346 ; [#uses=1 type=float*] [debug line = 101:9]
  store float 0.000000e+00, float* %C.addr.3, align 4, !dbg !346 ; [debug line = 101:9]
  %C.addr.4 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.118, !dbg !347 ; [#uses=1 type=float*] [debug line = 102:9]
  store float 0.000000e+00, float* %C.addr.4, align 4, !dbg !347 ; [debug line = 102:9]
  %rend12 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([20 x i8]* @.str6, i64 0, i64 0), i32 %rbegin10) nounwind, !dbg !348 ; [#uses=0 type=i32] [debug line = 103:5]
  %i.13 = add i32 %i5, 1, !dbg !349               ; [#uses=1 type=i32] [debug line = 94:59]
  call void @llvm.dbg.value(metadata !{i32 %i.13}, i64 0, metadata !350), !dbg !349 ; [debug line = 94:59] [debug variable = i]
  br label %.preheader, !dbg !349                 ; [debug line = 94:59]

; <label>:13                                      ; preds = %.preheader
  ret void, !dbg !351                             ; [debug line = 104:1]
}

; [#uses=2]
define internal fastcc void @copyBV(float* %pX) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pX}, i64 0, metadata !352), !dbg !353 ; [debug line = 214:25] [debug variable = pX]
  call void @llvm.dbg.value(metadata !2, i64 0, metadata !354), !dbg !355 ; [debug line = 214:46] [debug variable = pIndex]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX, i32 13) nounwind, !dbg !356 ; [debug line = 214:55]
  br label %1, !dbg !358                          ; [debug line = 215:33]

; <label>:1                                       ; preds = %2, %0
  %i = phi i32 [ 0, %0 ], [ %i.15, %2 ]           ; [#uses=4 type=i32]
  %exitcond = icmp eq i32 %i, 13, !dbg !358       ; [#uses=1 type=i1] [debug line = 215:33]
  br i1 %exitcond, label %3, label %2, !dbg !358  ; [debug line = 215:33]

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([8 x i8]* @.str16, i64 0, i64 0)) nounwind, !dbg !360 ; [debug line = 215:49]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([8 x i8]* @.str16, i64 0, i64 0)) nounwind, !dbg !360 ; [#uses=1 type=i32] [debug line = 215:49]
  %tmp = zext i32 %i to i64, !dbg !362            ; [#uses=1 type=i64] [debug line = 217:3]
  %pX.addr = getelementptr inbounds float* %pX, i64 %tmp, !dbg !362 ; [#uses=1 type=float*] [debug line = 217:3]
  %pX.load = load float* %pX.addr, align 4, !dbg !362 ; [#uses=2 type=float] [debug line = 217:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pX.load) nounwind
  %bvCnt.load = load i32* @bvCnt, align 4, !dbg !362 ; [#uses=1 type=i32] [debug line = 217:3]
  %tmp.135 = mul i32 %bvCnt.load, 13, !dbg !362   ; [#uses=1 type=i32] [debug line = 217:3]
  %tmp.136 = add i32 %tmp.135, %i, !dbg !362      ; [#uses=1 type=i32] [debug line = 217:3]
  %tmp.137 = zext i32 %tmp.136 to i64, !dbg !362  ; [#uses=1 type=i64] [debug line = 217:3]
  %basisVectors.addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.137, !dbg !362 ; [#uses=1 type=float*] [debug line = 217:3]
  store float %pX.load, float* %basisVectors.addr, align 4, !dbg !362 ; [debug line = 217:3]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([8 x i8]* @.str16, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !363 ; [#uses=0 type=i32] [debug line = 218:2]
  %i.15 = add i32 %i, 1, !dbg !364                ; [#uses=1 type=i32] [debug line = 215:43]
  call void @llvm.dbg.value(metadata !{i32 %i.15}, i64 0, metadata !365), !dbg !364 ; [debug line = 215:43] [debug variable = i]
  br label %1, !dbg !364                          ; [debug line = 215:43]

; <label>:3                                       ; preds = %1
  ret void, !dbg !366                             ; [debug line = 219:1]
}

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=19]
declare i32 @_ssdm_op_SpecRegionEnd(...)

; [#uses=19]
declare i32 @_ssdm_op_SpecRegionBegin(...)

; [#uses=2]
declare void @_ssdm_op_SpecPipeline(...) nounwind

; [#uses=18]
declare void @_ssdm_op_SpecLoopName(...) nounwind

; [#uses=5]
declare void @_ssdm_op_SpecInterface(...) nounwind

; [#uses=1]
declare void @_ssdm_op_SpecBitsMap(...)

; [#uses=60]
declare void @_ssdm_SpecKeepArrayLoad(...)

; [#uses=6]
declare void @_ssdm_SpecArrayDimSize(...) nounwind

; [#uses=2]
define internal fastcc float @K(float* %pX1, float* %pX2) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pX1}, i64 0, metadata !367), !dbg !368 ; [debug line = 16:21] [debug variable = pX1]
  call void @llvm.dbg.value(metadata !{float* %pX2}, i64 0, metadata !369), !dbg !370 ; [debug line = 16:42] [debug variable = pX2]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX1, i32 13) nounwind, !dbg !371 ; [debug line = 16:52]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX2, i32 13) nounwind, !dbg !373 ; [debug line = 16:83]
  br label %1, !dbg !374                          ; [debug line = 18:28]

; <label>:1                                       ; preds = %2, %0
  %sum = phi float [ 0.000000e+00, %0 ], [ %sum.2, %2 ] ; [#uses=2 type=float]
  %i = phi i32 [ 0, %0 ], [ %i.16, %2 ]           ; [#uses=3 type=i32]
  %exitcond = icmp eq i32 %i, 13, !dbg !374       ; [#uses=1 type=i1] [debug line = 18:28]
  br i1 %exitcond, label %3, label %2, !dbg !374  ; [debug line = 18:28]

; <label>:2                                       ; preds = %1
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([12 x i8]* @.str, i64 0, i64 0)) nounwind, !dbg !376 ; [#uses=1 type=i32] [debug line = 18:44]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !378 ; [debug line = 19:1]
  %tmp.140 = zext i32 %i to i64, !dbg !379        ; [#uses=2 type=i64] [debug line = 20:31]
  %pX1.addr = getelementptr inbounds float* %pX1, i64 %tmp.140, !dbg !379 ; [#uses=1 type=float*] [debug line = 20:31]
  %pX1.load = load float* %pX1.addr, align 4, !dbg !379 ; [#uses=2 type=float] [debug line = 20:31]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pX1.load) nounwind
  %pX2.addr = getelementptr inbounds float* %pX2, i64 %tmp.140, !dbg !379 ; [#uses=1 type=float*] [debug line = 20:31]
  %pX2.load = load float* %pX2.addr, align 4, !dbg !379 ; [#uses=2 type=float] [debug line = 20:31]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pX2.load) nounwind
  %val = fsub float %pX1.load, %pX2.load, !dbg !379 ; [#uses=2 type=float] [debug line = 20:31]
  call void @llvm.dbg.value(metadata !{float %val}, i64 0, metadata !380), !dbg !379 ; [debug line = 20:31] [debug variable = val]
  %tmp.141 = fmul float %val, %val, !dbg !381     ; [#uses=1 type=float] [debug line = 21:9]
  %sum.2 = fadd float %sum, %tmp.141, !dbg !381   ; [#uses=1 type=float] [debug line = 21:9]
  call void @llvm.dbg.value(metadata !{float %sum.2}, i64 0, metadata !382), !dbg !381 ; [debug line = 21:9] [debug variable = sum]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([12 x i8]* @.str, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !383 ; [#uses=0 type=i32] [debug line = 22:5]
  %i.16 = add i32 %i, 1, !dbg !384                ; [#uses=1 type=i32] [debug line = 18:38]
  call void @llvm.dbg.value(metadata !{i32 %i.16}, i64 0, metadata !385), !dbg !384 ; [debug line = 18:38] [debug variable = i]
  br label %1, !dbg !384                          ; [debug line = 18:38]

; <label>:3                                       ; preds = %1
  %sum.0.lcssa = phi float [ %sum, %1 ]           ; [#uses=1 type=float]
  %tmp = fmul float %sum.0.lcssa, -5.000000e-01, !dbg !386 ; [#uses=1 type=float] [debug line = 24:12]
  %tmp.139 = call fastcc float @"std::exp"(float %tmp), !dbg !386 ; [#uses=1 type=float] [debug line = 24:12]
  ret float %tmp.139, !dbg !386                   ; [debug line = 24:12]
}

!llvm.dbg.cu = !{!0}
!hls.encrypted.func = !{}

!0 = metadata !{i32 786449, i32 0, i32 4, metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace/ProjectionGP/solution1/.autopilot/db/ProjectionGP_FULLY_OPTIMIZED.pragma.2.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, metadata !1, metadata !1, metadata !3, metadata !41} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !5, metadata !14, metadata !19, metadata !22, metadata !25, metadata !28, metadata !31, metadata !36}
!5 = metadata !{i32 786478, i32 0, metadata !6, metadata !"K", metadata !"K", metadata !"_Z1KPKfS0_", metadata !6, i32 16, metadata !7, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, float (float*, float*)* @K, null, null, metadata !12, i32 16} ; [ DW_TAG_subprogram ]
!6 = metadata !{i32 786473, metadata !"../../../implementation/hls/Projection_GP/include/ProjectionGP_FULLY_OPTIMIZED.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", null} ; [ DW_TAG_file_type ]
!7 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !8, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!8 = metadata !{metadata !9, metadata !10, metadata !10}
!9 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!10 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !11} ; [ DW_TAG_pointer_type ]
!11 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !9} ; [ DW_TAG_const_type ]
!12 = metadata !{metadata !13}
!13 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!14 = metadata !{i32 786478, i32 0, metadata !6, metadata !"swapRowAndColumn", metadata !"swapRowAndColumn", metadata !"_Z16swapRowAndColumnPfjj", metadata !6, i32 27, metadata !15, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !12, i32 27} ; [ DW_TAG_subprogram ]
!15 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !16, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!16 = metadata !{null, metadata !17, metadata !18, metadata !18}
!17 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !9} ; [ DW_TAG_pointer_type ]
!18 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!19 = metadata !{i32 786478, i32 0, metadata !6, metadata !"deleteBV", metadata !"deleteBV", metadata !"_Z8deleteBVj", metadata !6, i32 43, metadata !20, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32)* @deleteBV, null, null, metadata !12, i32 43} ; [ DW_TAG_subprogram ]
!20 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !21, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!21 = metadata !{null, metadata !18}
!22 = metadata !{i32 786478, i32 0, metadata !6, metadata !"getMinKLApprox", metadata !"getMinKLApprox", metadata !"_Z14getMinKLApproxv", metadata !6, i32 106, metadata !23, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @getMinKLApprox, null, null, metadata !12, i32 106} ; [ DW_TAG_subprogram ]
!23 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !24, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!24 = metadata !{metadata !18}
!25 = metadata !{i32 786478, i32 0, metadata !6, metadata !"train_full_bv_set", metadata !"train_full_bv_set", metadata !"_Z17train_full_bv_setPKff", metadata !6, i32 122, metadata !26, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (float*, float)* @train_full_bv_set, null, null, metadata !12, i32 122} ; [ DW_TAG_subprogram ]
!26 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !27, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!27 = metadata !{null, metadata !10, metadata !11}
!28 = metadata !{i32 786478, i32 0, metadata !6, metadata !"copyBV", metadata !"copyBV", metadata !"_Z6copyBVPKfj", metadata !6, i32 214, metadata !29, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !12, i32 214} ; [ DW_TAG_subprogram ]
!29 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !30, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!30 = metadata !{null, metadata !10, metadata !18}
!31 = metadata !{i32 786478, i32 0, metadata !6, metadata !"projection_gp", metadata !"projection_gp", metadata !"_Z13projection_gpPKffbb", metadata !6, i32 221, metadata !32, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, float (float*, float, i1, i1)* @projection_gp, null, null, metadata !12, i32 221} ; [ DW_TAG_subprogram ]
!32 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !33, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!33 = metadata !{metadata !9, metadata !10, metadata !11, metadata !34, metadata !34}
!34 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !35} ; [ DW_TAG_const_type ]
!35 = metadata !{i32 786468, null, metadata !"bool", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ]
!36 = metadata !{i32 786478, i32 0, metadata !37, metadata !"exp", metadata !"exp", metadata !"_ZSt3expf", metadata !38, i32 216, metadata !39, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, float (float)* @"std::exp", null, null, metadata !12, i32 217} ; [ DW_TAG_subprogram ]
!37 = metadata !{i32 786489, null, metadata !"std", metadata !38, i32 76} ; [ DW_TAG_namespace ]
!38 = metadata !{i32 786473, metadata !"/opt/Xilinx/Vivado_HLS/2015.4/lnx64/tools/gcc/lib/gcc/x86_64-unknown-linux-gnu/4.6.3/../../../../include/c++/4.6.3/cmath", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", null} ; [ DW_TAG_file_type ]
!39 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !40, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!40 = metadata !{metadata !9, metadata !9}
!41 = metadata !{metadata !42}
!42 = metadata !{metadata !43, metadata !47, metadata !48, metadata !52, metadata !56, metadata !57, metadata !58, metadata !62, metadata !63}
!43 = metadata !{i32 786484, i32 0, null, metadata !"C", metadata !"C", metadata !"", metadata !6, i32 7, metadata !44, i32 0, i32 1, [1681 x float]* @C} ; [ DW_TAG_variable ]
!44 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 53792, i64 32, i32 0, i32 0, metadata !9, metadata !45, i32 0, i32 0} ; [ DW_TAG_array_type ]
!45 = metadata !{metadata !46}
!46 = metadata !{i32 786465, i64 0, i64 1680}     ; [ DW_TAG_subrange_type ]
!47 = metadata !{i32 786484, i32 0, null, metadata !"Q", metadata !"Q", metadata !"", metadata !6, i32 8, metadata !44, i32 0, i32 1, [1681 x float]* @Q} ; [ DW_TAG_variable ]
!48 = metadata !{i32 786484, i32 0, null, metadata !"e", metadata !"e", metadata !"", metadata !6, i32 9, metadata !49, i32 0, i32 1, [41 x float]* @e} ; [ DW_TAG_variable ]
!49 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1312, i64 32, i32 0, i32 0, metadata !9, metadata !50, i32 0, i32 0} ; [ DW_TAG_array_type ]
!50 = metadata !{metadata !51}
!51 = metadata !{i32 786465, i64 0, i64 40}       ; [ DW_TAG_subrange_type ]
!52 = metadata !{i32 786484, i32 0, null, metadata !"k", metadata !"k", metadata !"", metadata !6, i32 10, metadata !53, i32 0, i32 1, [40 x float]* @k} ; [ DW_TAG_variable ]
!53 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1280, i64 32, i32 0, i32 0, metadata !9, metadata !54, i32 0, i32 0} ; [ DW_TAG_array_type ]
!54 = metadata !{metadata !55}
!55 = metadata !{i32 786465, i64 0, i64 39}       ; [ DW_TAG_subrange_type ]
!56 = metadata !{i32 786484, i32 0, null, metadata !"s", metadata !"s", metadata !"", metadata !6, i32 11, metadata !49, i32 0, i32 1, [41 x float]* @s} ; [ DW_TAG_variable ]
!57 = metadata !{i32 786484, i32 0, null, metadata !"alpha", metadata !"alpha", metadata !"", metadata !6, i32 12, metadata !49, i32 0, i32 1, [41 x float]* @alpha} ; [ DW_TAG_variable ]
!58 = metadata !{i32 786484, i32 0, null, metadata !"basisVectors", metadata !"basisVectors", metadata !"", metadata !6, i32 13, metadata !59, i32 0, i32 1, [533 x float]* @basisVectors} ; [ DW_TAG_variable ]
!59 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 17056, i64 32, i32 0, i32 0, metadata !9, metadata !60, i32 0, i32 0} ; [ DW_TAG_array_type ]
!60 = metadata !{metadata !61}
!61 = metadata !{i32 786465, i64 0, i64 532}      ; [ DW_TAG_subrange_type ]
!62 = metadata !{i32 786484, i32 0, null, metadata !"bvCnt", metadata !"bvCnt", metadata !"", metadata !6, i32 14, metadata !18, i32 0, i32 1, i32* @bvCnt} ; [ DW_TAG_variable ]
!63 = metadata !{i32 786484, i32 0, null, metadata !"signgam", metadata !"signgam", metadata !"", metadata !64, i32 149, metadata !65, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!64 = metadata !{i32 786473, metadata !"/usr/include/math.h", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", null} ; [ DW_TAG_file_type ]
!65 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!66 = metadata !{i32 786689, metadata !25, metadata !"pX", metadata !6, i32 16777338, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!67 = metadata !{i32 122, i32 37, metadata !25, null}
!68 = metadata !{i32 786689, metadata !25, metadata !"pY", metadata !6, i32 33554554, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!69 = metadata !{i32 122, i32 57, metadata !25, null}
!70 = metadata !{i32 122, i32 62, metadata !71, null}
!71 = metadata !{i32 786443, metadata !25, i32 122, i32 61, metadata !6, i32 23} ; [ DW_TAG_lexical_block ]
!72 = metadata !{i32 126, i32 35, metadata !73, null}
!73 = metadata !{i32 786443, metadata !71, i32 126, i32 12, metadata !6, i32 24} ; [ DW_TAG_lexical_block ]
!74 = metadata !{i32 137, i32 45, metadata !75, null}
!75 = metadata !{i32 786443, metadata !71, i32 137, i32 22, metadata !6, i32 26} ; [ DW_TAG_lexical_block ]
!76 = metadata !{i32 126, i32 51, metadata !77, null}
!77 = metadata !{i32 786443, metadata !73, i32 126, i32 50, metadata !6, i32 25} ; [ DW_TAG_lexical_block ]
!78 = metadata !{i32 128, i32 16, metadata !77, null}
!79 = metadata !{i32 129, i32 9, metadata !77, null}
!80 = metadata !{i32 786688, metadata !71, metadata !"m", metadata !6, i32 123, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!81 = metadata !{i32 130, i32 5, metadata !77, null}
!82 = metadata !{i32 126, i32 45, metadata !73, null}
!83 = metadata !{i32 786688, metadata !73, metadata !"i", metadata !6, i32 126, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!84 = metadata !{i32 148, i32 36, metadata !85, null}
!85 = metadata !{i32 786443, metadata !71, i32 148, i32 13, metadata !6, i32 30} ; [ DW_TAG_lexical_block ]
!86 = metadata !{i32 137, i32 61, metadata !87, null}
!87 = metadata !{i32 786443, metadata !75, i32 137, i32 60, metadata !6, i32 27} ; [ DW_TAG_lexical_block ]
!88 = metadata !{i32 139, i32 6, metadata !87, null}
!89 = metadata !{i32 140, i32 6, metadata !87, null}
!90 = metadata !{i32 143, i32 10, metadata !91, null}
!91 = metadata !{i32 786443, metadata !92, i32 141, i32 64, metadata !6, i32 29} ; [ DW_TAG_lexical_block ]
!92 = metadata !{i32 786443, metadata !87, i32 141, i32 26, metadata !6, i32 28} ; [ DW_TAG_lexical_block ]
!93 = metadata !{i32 141, i32 49, metadata !92, null}
!94 = metadata !{i32 141, i32 65, metadata !91, null}
!95 = metadata !{i32 144, i32 10, metadata !91, null}
!96 = metadata !{i32 145, i32 9, metadata !91, null}
!97 = metadata !{i32 141, i32 59, metadata !92, null}
!98 = metadata !{i32 786688, metadata !92, metadata !"j", metadata !6, i32 141, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!99 = metadata !{i32 146, i32 5, metadata !87, null}
!100 = metadata !{i32 137, i32 55, metadata !75, null}
!101 = metadata !{i32 786688, metadata !75, metadata !"i", metadata !6, i32 137, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!102 = metadata !{i32 148, i32 52, metadata !103, null}
!103 = metadata !{i32 786443, metadata !85, i32 148, i32 51, metadata !6, i32 31} ; [ DW_TAG_lexical_block ]
!104 = metadata !{i32 150, i32 6, metadata !103, null}
!105 = metadata !{i32 786688, metadata !71, metadata !"sigma2", metadata !6, i32 136, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!106 = metadata !{i32 151, i32 5, metadata !103, null}
!107 = metadata !{i32 148, i32 46, metadata !85, null}
!108 = metadata !{i32 786688, metadata !85, metadata !"i", metadata !6, i32 148, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!109 = metadata !{i32 152, i32 5, metadata !71, null}
!110 = metadata !{i32 154, i32 40, metadata !71, null}
!111 = metadata !{i32 786688, metadata !71, metadata !"q", metadata !6, i32 154, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!112 = metadata !{i32 155, i32 35, metadata !71, null}
!113 = metadata !{i32 786688, metadata !71, metadata !"r", metadata !6, i32 155, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!114 = metadata !{i32 158, i32 42, metadata !115, null}
!115 = metadata !{i32 786443, metadata !71, i32 158, i32 19, metadata !6, i32 32} ; [ DW_TAG_lexical_block ]
!116 = metadata !{i32 158, i32 58, metadata !117, null}
!117 = metadata !{i32 786443, metadata !115, i32 158, i32 57, metadata !6, i32 33} ; [ DW_TAG_lexical_block ]
!118 = metadata !{i32 160, i32 6, metadata !117, null}
!119 = metadata !{i32 786688, metadata !71, metadata !"gamma", metadata !6, i32 156, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!120 = metadata !{i32 161, i32 6, metadata !117, null}
!121 = metadata !{i32 162, i32 5, metadata !117, null}
!122 = metadata !{i32 158, i32 52, metadata !115, null}
!123 = metadata !{i32 786688, metadata !115, metadata !"i", metadata !6, i32 158, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!124 = metadata !{i32 168, i32 5, metadata !71, null}
!125 = metadata !{i32 171, i32 43, metadata !126, null}
!126 = metadata !{i32 786443, metadata !71, i32 171, i32 20, metadata !6, i32 34} ; [ DW_TAG_lexical_block ]
!127 = metadata !{i32 187, i32 4, metadata !128, null}
!128 = metadata !{i32 786443, metadata !129, i32 182, i32 60, metadata !6, i32 41} ; [ DW_TAG_lexical_block ]
!129 = metadata !{i32 786443, metadata !130, i32 182, i32 21, metadata !6, i32 40} ; [ DW_TAG_lexical_block ]
!130 = metadata !{i32 786443, metadata !131, i32 180, i32 59, metadata !6, i32 39} ; [ DW_TAG_lexical_block ]
!131 = metadata !{i32 786443, metadata !71, i32 180, i32 20, metadata !6, i32 38} ; [ DW_TAG_lexical_block ]
!132 = metadata !{i32 180, i32 43, metadata !131, null}
!133 = metadata !{i32 171, i32 60, metadata !134, null}
!134 = metadata !{i32 786443, metadata !126, i32 171, i32 59, metadata !6, i32 35} ; [ DW_TAG_lexical_block ]
!135 = metadata !{i32 176, i32 13, metadata !136, null}
!136 = metadata !{i32 786443, metadata !137, i32 173, i32 60, metadata !6, i32 37} ; [ DW_TAG_lexical_block ]
!137 = metadata !{i32 786443, metadata !134, i32 173, i32 21, metadata !6, i32 36} ; [ DW_TAG_lexical_block ]
!138 = metadata !{i32 173, i32 44, metadata !137, null}
!139 = metadata !{i32 173, i32 61, metadata !136, null}
!140 = metadata !{i32 177, i32 9, metadata !136, null}
!141 = metadata !{i32 173, i32 55, metadata !137, null}
!142 = metadata !{i32 786688, metadata !137, metadata !"j", metadata !6, i32 173, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!143 = metadata !{i32 178, i32 5, metadata !134, null}
!144 = metadata !{i32 171, i32 54, metadata !126, null}
!145 = metadata !{i32 786688, metadata !126, metadata !"i", metadata !6, i32 171, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!146 = metadata !{i32 180, i32 60, metadata !130, null}
!147 = metadata !{i32 184, i32 37, metadata !128, null}
!148 = metadata !{i32 182, i32 44, metadata !129, null}
!149 = metadata !{i32 182, i32 61, metadata !128, null}
!150 = metadata !{i32 786688, metadata !128, metadata !"ti", metadata !6, i32 184, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!151 = metadata !{i32 185, i32 34, metadata !128, null}
!152 = metadata !{i32 786688, metadata !128, metadata !"tj", metadata !6, i32 185, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!153 = metadata !{i32 188, i32 6, metadata !128, null}
!154 = metadata !{i32 182, i32 55, metadata !129, null}
!155 = metadata !{i32 786688, metadata !129, metadata !"j", metadata !6, i32 182, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!156 = metadata !{i32 189, i32 5, metadata !130, null}
!157 = metadata !{i32 180, i32 54, metadata !131, null}
!158 = metadata !{i32 786688, metadata !131, metadata !"i", metadata !6, i32 180, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!159 = metadata !{i32 208, i32 5, metadata !71, null}
!160 = metadata !{i32 210, i32 23, metadata !71, null}
!161 = metadata !{i32 786688, metadata !71, metadata !"index", metadata !6, i32 210, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!162 = metadata !{i32 211, i32 2, metadata !71, null}
!163 = metadata !{i32 212, i32 1, metadata !71, null}
!164 = metadata !{i32 786689, metadata !14, metadata !"pM", metadata !6, i32 16777243, metadata !17, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!165 = metadata !{i32 27, i32 29, metadata !14, null}
!166 = metadata !{i32 786689, metadata !14, metadata !"rowA", metadata !6, i32 33554459, metadata !18, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!167 = metadata !{i32 27, i32 65, metadata !14, null}
!168 = metadata !{i32 27, i32 92, metadata !169, null}
!169 = metadata !{i32 786443, metadata !14, i32 27, i32 91, metadata !6, i32 3} ; [ DW_TAG_lexical_block ]
!170 = metadata !{i32 30, i32 40, metadata !171, null}
!171 = metadata !{i32 786443, metadata !172, i32 28, i32 48, metadata !6, i32 5} ; [ DW_TAG_lexical_block ]
!172 = metadata !{i32 786443, metadata !169, i32 28, i32 5, metadata !6, i32 4} ; [ DW_TAG_lexical_block ]
!173 = metadata !{i32 28, i32 28, metadata !172, null}
!174 = metadata !{i32 35, i32 28, metadata !175, null}
!175 = metadata !{i32 786443, metadata !169, i32 35, i32 5, metadata !6, i32 6} ; [ DW_TAG_lexical_block ]
!176 = metadata !{i32 786688, metadata !171, metadata !"temp", metadata !6, i32 30, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!177 = metadata !{i32 31, i32 9, metadata !171, null}
!178 = metadata !{i32 32, i32 9, metadata !171, null}
!179 = metadata !{i32 28, i32 43, metadata !172, null}
!180 = metadata !{i32 786688, metadata !172, metadata !"i", metadata !6, i32 28, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!181 = metadata !{i32 37, i32 40, metadata !182, null}
!182 = metadata !{i32 786443, metadata !175, i32 35, i32 48, metadata !6, i32 7} ; [ DW_TAG_lexical_block ]
!183 = metadata !{i32 786688, metadata !182, metadata !"temp", metadata !6, i32 37, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!184 = metadata !{i32 38, i32 9, metadata !182, null}
!185 = metadata !{i32 39, i32 9, metadata !182, null}
!186 = metadata !{i32 35, i32 43, metadata !175, null}
!187 = metadata !{i32 786688, metadata !175, metadata !"i", metadata !6, i32 35, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!188 = metadata !{i32 41, i32 1, metadata !169, null}
!189 = metadata !{i32 786689, metadata !36, metadata !"__x", metadata !38, i32 16777432, metadata !9, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!190 = metadata !{i32 216, i32 13, metadata !36, null}
!191 = metadata !{i32 217, i32 12, metadata !192, null}
!192 = metadata !{i32 786443, metadata !36, i32 217, i32 3, metadata !38, i32 59} ; [ DW_TAG_lexical_block ]
!193 = metadata !{metadata !194}
!194 = metadata !{i32 0, i32 31, metadata !195}
!195 = metadata !{metadata !196}
!196 = metadata !{metadata !"return", metadata !197, metadata !"float", i32 0, i32 31}
!197 = metadata !{metadata !198}
!198 = metadata !{i32 0, i32 1, i32 0}
!199 = metadata !{i32 786689, metadata !31, metadata !"pX", metadata !6, i32 16777437, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!200 = metadata !{i32 221, i32 33, metadata !31, null}
!201 = metadata !{i32 786689, metadata !31, metadata !"pY", metadata !6, i32 33554653, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!202 = metadata !{i32 221, i32 53, metadata !31, null}
!203 = metadata !{i32 786689, metadata !31, metadata !"pPredict", metadata !6, i32 50331869, metadata !34, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!204 = metadata !{i32 221, i32 68, metadata !31, null}
!205 = metadata !{i32 786689, metadata !31, metadata !"pReset", metadata !6, i32 67109085, metadata !34, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!206 = metadata !{i32 221, i32 89, metadata !31, null}
!207 = metadata !{i32 221, i32 98, metadata !208, null}
!208 = metadata !{i32 786443, metadata !31, i32 221, i32 97, metadata !6, i32 45} ; [ DW_TAG_lexical_block ]
!209 = metadata !{i32 222, i32 1, metadata !208, null}
!210 = metadata !{i32 223, i32 1, metadata !208, null}
!211 = metadata !{i32 224, i32 1, metadata !208, null}
!212 = metadata !{i32 225, i32 1, metadata !208, null}
!213 = metadata !{i32 226, i32 1, metadata !208, null}
!214 = metadata !{i32 228, i32 2, metadata !208, null}
!215 = metadata !{i32 229, i32 37, metadata !216, null}
!216 = metadata !{i32 786443, metadata !217, i32 229, i32 14, metadata !6, i32 47} ; [ DW_TAG_lexical_block ]
!217 = metadata !{i32 786443, metadata !208, i32 228, i32 14, metadata !6, i32 46} ; [ DW_TAG_lexical_block ]
!218 = metadata !{i32 229, i32 58, metadata !219, null}
!219 = metadata !{i32 786443, metadata !216, i32 229, i32 57, metadata !6, i32 48} ; [ DW_TAG_lexical_block ]
!220 = metadata !{i32 234, i32 6, metadata !221, null}
!221 = metadata !{i32 786443, metadata !222, i32 233, i32 17, metadata !6, i32 51} ; [ DW_TAG_lexical_block ]
!222 = metadata !{i32 786443, metadata !223, i32 231, i32 59, metadata !6, i32 50} ; [ DW_TAG_lexical_block ]
!223 = metadata !{i32 786443, metadata !219, i32 231, i32 16, metadata !6, i32 49} ; [ DW_TAG_lexical_block ]
!224 = metadata !{i32 235, i32 6, metadata !221, null}
!225 = metadata !{i32 231, i32 39, metadata !223, null}
!226 = metadata !{i32 231, i32 60, metadata !222, null}
!227 = metadata !{i32 233, i32 5, metadata !222, null}
!228 = metadata !{i32 237, i32 6, metadata !229, null}
!229 = metadata !{i32 786443, metadata !222, i32 236, i32 12, metadata !6, i32 52} ; [ DW_TAG_lexical_block ]
!230 = metadata !{i32 240, i32 4, metadata !222, null}
!231 = metadata !{i32 231, i32 54, metadata !223, null}
!232 = metadata !{i32 786688, metadata !223, metadata !"j", metadata !6, i32 231, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!233 = metadata !{i32 241, i32 4, metadata !219, null}
!234 = metadata !{i32 242, i32 3, metadata !219, null}
!235 = metadata !{i32 229, i32 52, metadata !216, null}
!236 = metadata !{i32 786688, metadata !216, metadata !"i", metadata !6, i32 229, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!237 = metadata !{i32 245, i32 2, metadata !208, null}
!238 = metadata !{i32 255, i32 37, metadata !239, null}
!239 = metadata !{i32 786443, metadata !240, i32 255, i32 14, metadata !6, i32 57} ; [ DW_TAG_lexical_block ]
!240 = metadata !{i32 786443, metadata !208, i32 253, i32 9, metadata !6, i32 56} ; [ DW_TAG_lexical_block ]
!241 = metadata !{i32 246, i32 3, metadata !242, null}
!242 = metadata !{i32 786443, metadata !208, i32 245, i32 17, metadata !6, i32 53} ; [ DW_TAG_lexical_block ]
!243 = metadata !{i32 247, i32 4, metadata !244, null}
!244 = metadata !{i32 786443, metadata !242, i32 246, i32 20, metadata !6, i32 54} ; [ DW_TAG_lexical_block ]
!245 = metadata !{i32 248, i32 3, metadata !244, null}
!246 = metadata !{i32 249, i32 4, metadata !247, null}
!247 = metadata !{i32 786443, metadata !242, i32 248, i32 10, metadata !6, i32 55} ; [ DW_TAG_lexical_block ]
!248 = metadata !{i32 250, i32 4, metadata !247, null}
!249 = metadata !{i32 252, i32 3, metadata !242, null}
!250 = metadata !{i32 255, i32 53, metadata !251, null}
!251 = metadata !{i32 786443, metadata !239, i32 255, i32 52, metadata !6, i32 58} ; [ DW_TAG_lexical_block ]
!252 = metadata !{i32 257, i32 11, metadata !251, null}
!253 = metadata !{i32 786688, metadata !240, metadata !"sum", metadata !6, i32 254, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!254 = metadata !{i32 258, i32 3, metadata !251, null}
!255 = metadata !{i32 255, i32 47, metadata !239, null}
!256 = metadata !{i32 786688, metadata !239, metadata !"i", metadata !6, i32 255, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!257 = metadata !{i32 262, i32 1, metadata !208, null}
!258 = metadata !{i32 108, i32 77, metadata !259, null}
!259 = metadata !{i32 786443, metadata !22, i32 106, i32 31, metadata !6, i32 19} ; [ DW_TAG_lexical_block ]
!260 = metadata !{i32 786688, metadata !259, metadata !"minScore", metadata !6, i32 108, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!261 = metadata !{i32 110, i32 28, metadata !262, null}
!262 = metadata !{i32 786443, metadata !259, i32 110, i32 5, metadata !6, i32 20} ; [ DW_TAG_lexical_block ]
!263 = metadata !{i32 111, i32 79, metadata !264, null}
!264 = metadata !{i32 786443, metadata !262, i32 110, i32 48, metadata !6, i32 21} ; [ DW_TAG_lexical_block ]
!265 = metadata !{i32 786688, metadata !264, metadata !"tScore", metadata !6, i32 111, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!266 = metadata !{i32 113, i32 9, metadata !264, null}
!267 = metadata !{i32 114, i32 13, metadata !268, null}
!268 = metadata !{i32 786443, metadata !264, i32 113, i32 32, metadata !6, i32 22} ; [ DW_TAG_lexical_block ]
!269 = metadata !{i32 786688, metadata !259, metadata !"index", metadata !6, i32 107, metadata !65, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!270 = metadata !{i32 115, i32 13, metadata !268, null}
!271 = metadata !{i32 110, i32 43, metadata !262, null}
!272 = metadata !{i32 786688, metadata !262, metadata !"i", metadata !6, i32 110, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!273 = metadata !{i32 119, i32 5, metadata !259, null}
!274 = metadata !{i32 786689, metadata !19, metadata !"pIndex", metadata !6, i32 16777259, metadata !18, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!275 = metadata !{i32 43, i32 29, metadata !19, null}
!276 = metadata !{i32 47, i32 31, metadata !277, null}
!277 = metadata !{i32 786443, metadata !19, i32 43, i32 37, metadata !6, i32 8} ; [ DW_TAG_lexical_block ]
!278 = metadata !{i32 786688, metadata !277, metadata !"temp", metadata !6, i32 47, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!279 = metadata !{i32 48, i32 5, metadata !277, null}
!280 = metadata !{i32 49, i32 5, metadata !277, null}
!281 = metadata !{i32 51, i32 5, metadata !277, null}
!282 = metadata !{i32 52, i32 5, metadata !277, null}
!283 = metadata !{i32 56, i32 2, metadata !284, null}
!284 = metadata !{i32 786443, metadata !285, i32 54, i32 63, metadata !6, i32 10} ; [ DW_TAG_lexical_block ]
!285 = metadata !{i32 786443, metadata !277, i32 54, i32 25, metadata !6, i32 9} ; [ DW_TAG_lexical_block ]
!286 = metadata !{i32 54, i32 48, metadata !285, null}
!287 = metadata !{i32 54, i32 64, metadata !284, null}
!288 = metadata !{i32 55, i32 1, metadata !284, null}
!289 = metadata !{i32 57, i32 9, metadata !284, null}
!290 = metadata !{i32 58, i32 9, metadata !284, null}
!291 = metadata !{i32 59, i32 5, metadata !284, null}
!292 = metadata !{i32 54, i32 58, metadata !285, null}
!293 = metadata !{i32 786688, metadata !285, metadata !"i", metadata !6, i32 54, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!294 = metadata !{i32 62, i32 32, metadata !277, null}
!295 = metadata !{i32 786688, metadata !277, metadata !"alphaStar", metadata !6, i32 62, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!296 = metadata !{i32 63, i32 24, metadata !277, null}
!297 = metadata !{i32 786688, metadata !277, metadata !"cStar", metadata !6, i32 63, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!298 = metadata !{i32 81, i32 45, metadata !299, null}
!299 = metadata !{i32 786443, metadata !300, i32 76, i32 62, metadata !6, i32 16} ; [ DW_TAG_lexical_block ]
!300 = metadata !{i32 786443, metadata !301, i32 76, i32 24, metadata !6, i32 15} ; [ DW_TAG_lexical_block ]
!301 = metadata !{i32 786443, metadata !302, i32 74, i32 60, metadata !6, i32 14} ; [ DW_TAG_lexical_block ]
!302 = metadata !{i32 786443, metadata !277, i32 74, i32 23, metadata !6, i32 13} ; [ DW_TAG_lexical_block ]
!303 = metadata !{i32 786688, metadata !277, metadata !"qStar", metadata !6, i32 64, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!304 = metadata !{i32 64, i32 24, metadata !277, null}
!305 = metadata !{i32 65, i32 5, metadata !277, null}
!306 = metadata !{i32 67, i32 44, metadata !307, null}
!307 = metadata !{i32 786443, metadata !277, i32 67, i32 21, metadata !6, i32 11} ; [ DW_TAG_lexical_block ]
!308 = metadata !{i32 67, i32 60, metadata !309, null}
!309 = metadata !{i32 786443, metadata !307, i32 67, i32 59, metadata !6, i32 12} ; [ DW_TAG_lexical_block ]
!310 = metadata !{i32 69, i32 35, metadata !309, null}
!311 = metadata !{i32 786688, metadata !309, metadata !"a", metadata !6, i32 69, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!312 = metadata !{i32 70, i32 9, metadata !309, null}
!313 = metadata !{i32 71, i32 5, metadata !309, null}
!314 = metadata !{i32 67, i32 54, metadata !307, null}
!315 = metadata !{i32 786688, metadata !307, metadata !"i", metadata !6, i32 67, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!316 = metadata !{i32 72, i32 5, metadata !277, null}
!317 = metadata !{i32 74, i32 45, metadata !302, null}
!318 = metadata !{i32 94, i32 48, metadata !319, null}
!319 = metadata !{i32 786443, metadata !277, i32 94, i32 25, metadata !6, i32 17} ; [ DW_TAG_lexical_block ]
!320 = metadata !{i32 74, i32 61, metadata !301, null}
!321 = metadata !{i32 78, i32 39, metadata !299, null}
!322 = metadata !{i32 80, i32 38, metadata !299, null}
!323 = metadata !{i32 84, i32 13, metadata !299, null}
!324 = metadata !{i32 76, i32 47, metadata !300, null}
!325 = metadata !{i32 76, i32 63, metadata !299, null}
!326 = metadata !{i32 786688, metadata !299, metadata !"a", metadata !6, i32 78, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!327 = metadata !{i32 79, i32 39, metadata !299, null}
!328 = metadata !{i32 786688, metadata !299, metadata !"b", metadata !6, i32 79, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!329 = metadata !{i32 786688, metadata !299, metadata !"c", metadata !6, i32 80, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!330 = metadata !{i32 786688, metadata !299, metadata !"temp", metadata !6, i32 81, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!331 = metadata !{i32 90, i32 13, metadata !299, null}
!332 = metadata !{i32 91, i32 9, metadata !299, null}
!333 = metadata !{i32 76, i32 57, metadata !300, null}
!334 = metadata !{i32 786688, metadata !300, metadata !"j", metadata !6, i32 76, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!335 = metadata !{i32 92, i32 5, metadata !301, null}
!336 = metadata !{i32 74, i32 55, metadata !302, null}
!337 = metadata !{i32 786688, metadata !302, metadata !"i", metadata !6, i32 74, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!338 = metadata !{i32 94, i32 65, metadata !339, null}
!339 = metadata !{i32 786443, metadata !319, i32 94, i32 64, metadata !6, i32 18} ; [ DW_TAG_lexical_block ]
!340 = metadata !{i32 96, i32 35, metadata !339, null}
!341 = metadata !{i32 786688, metadata !339, metadata !"a", metadata !6, i32 96, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!342 = metadata !{i32 97, i32 35, metadata !339, null}
!343 = metadata !{i32 786688, metadata !339, metadata !"b", metadata !6, i32 97, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!344 = metadata !{i32 99, i32 6, metadata !339, null}
!345 = metadata !{i32 100, i32 9, metadata !339, null}
!346 = metadata !{i32 101, i32 9, metadata !339, null}
!347 = metadata !{i32 102, i32 9, metadata !339, null}
!348 = metadata !{i32 103, i32 5, metadata !339, null}
!349 = metadata !{i32 94, i32 59, metadata !319, null}
!350 = metadata !{i32 786688, metadata !319, metadata !"i", metadata !6, i32 94, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!351 = metadata !{i32 104, i32 1, metadata !277, null}
!352 = metadata !{i32 786689, metadata !28, metadata !"pX", metadata !6, i32 16777430, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!353 = metadata !{i32 214, i32 25, metadata !28, null}
!354 = metadata !{i32 786689, metadata !28, metadata !"pIndex", metadata !6, i32 33554646, metadata !18, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!355 = metadata !{i32 214, i32 46, metadata !28, null}
!356 = metadata !{i32 214, i32 55, metadata !357, null}
!357 = metadata !{i32 786443, metadata !28, i32 214, i32 54, metadata !6, i32 42} ; [ DW_TAG_lexical_block ]
!358 = metadata !{i32 215, i32 33, metadata !359, null}
!359 = metadata !{i32 786443, metadata !357, i32 215, i32 10, metadata !6, i32 43} ; [ DW_TAG_lexical_block ]
!360 = metadata !{i32 215, i32 49, metadata !361, null}
!361 = metadata !{i32 786443, metadata !359, i32 215, i32 48, metadata !6, i32 44} ; [ DW_TAG_lexical_block ]
!362 = metadata !{i32 217, i32 3, metadata !361, null}
!363 = metadata !{i32 218, i32 2, metadata !361, null}
!364 = metadata !{i32 215, i32 43, metadata !359, null}
!365 = metadata !{i32 786688, metadata !359, metadata !"i", metadata !6, i32 215, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!366 = metadata !{i32 219, i32 1, metadata !357, null}
!367 = metadata !{i32 786689, metadata !5, metadata !"pX1", metadata !6, i32 16777232, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!368 = metadata !{i32 16, i32 21, metadata !5, null}
!369 = metadata !{i32 786689, metadata !5, metadata !"pX2", metadata !6, i32 33554448, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!370 = metadata !{i32 16, i32 42, metadata !5, null}
!371 = metadata !{i32 16, i32 52, metadata !372, null}
!372 = metadata !{i32 786443, metadata !5, i32 16, i32 51, metadata !6, i32 0} ; [ DW_TAG_lexical_block ]
!373 = metadata !{i32 16, i32 83, metadata !372, null}
!374 = metadata !{i32 18, i32 28, metadata !375, null}
!375 = metadata !{i32 786443, metadata !372, i32 18, i32 5, metadata !6, i32 1} ; [ DW_TAG_lexical_block ]
!376 = metadata !{i32 18, i32 44, metadata !377, null}
!377 = metadata !{i32 786443, metadata !375, i32 18, i32 43, metadata !6, i32 2} ; [ DW_TAG_lexical_block ]
!378 = metadata !{i32 19, i32 1, metadata !377, null}
!379 = metadata !{i32 20, i32 31, metadata !377, null}
!380 = metadata !{i32 786688, metadata !377, metadata !"val", metadata !6, i32 20, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!381 = metadata !{i32 21, i32 9, metadata !377, null}
!382 = metadata !{i32 786688, metadata !372, metadata !"sum", metadata !6, i32 17, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!383 = metadata !{i32 22, i32 5, metadata !377, null}
!384 = metadata !{i32 18, i32 38, metadata !375, null}
!385 = metadata !{i32 786688, metadata !375, metadata !"i", metadata !6, i32 18, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!386 = metadata !{i32 24, i32 12, metadata !372, null}
