; ModuleID = '/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace/ProjectionGP/solution1/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@s = global [41 x float] zeroinitializer, align 16 ; [#uses=6 type=[41 x float]*]
@projection_gp_str = internal unnamed_addr constant [14 x i8] c"projection_gp\00" ; [#uses=1 type=[14 x i8]*]
@k = global [40 x float] zeroinitializer, align 16 ; [#uses=4 type=[40 x float]*]
@e = global [41 x float] zeroinitializer, align 16 ; [#uses=4 type=[41 x float]*]
@bvCnt = global i32 0, align 4                    ; [#uses=4 type=i32*]
@basisVectors = global [533 x float] zeroinitializer, align 16 ; [#uses=6 type=[533 x float]*]
@alpha = global [41 x float] zeroinitializer, align 16 ; [#uses=9 type=[41 x float]*]
@RAM_1P_str = internal unnamed_addr constant [7 x i8] c"RAM_1P\00" ; [#uses=2 type=[7 x i8]*]
@Q = global [1681 x float] zeroinitializer, align 16 ; [#uses=13 type=[1681 x float]*]
@PREDICTION_L_str = internal unnamed_addr constant [13 x i8] c"PREDICTION_L\00" ; [#uses=1 type=[13 x i8]*]
@C = global [1681 x float] zeroinitializer, align 16 ; [#uses=13 type=[1681 x float]*]
@p_str9 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_INNER\00", align 1 ; [#uses=1 type=[17 x i8]*]
@p_str8 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_OUTER\00", align 1 ; [#uses=3 type=[17 x i8]*]
@p_str7 = private unnamed_addr constant [7 x i8] c"CALC_K\00", align 1 ; [#uses=3 type=[7 x i8]*]
@p_str6 = private unnamed_addr constant [20 x i8] c"DELETE_BV_UNSET_C_Q\00", align 1 ; [#uses=1 type=[20 x i8]*]
@p_str5 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_INNER\00", align 1 ; [#uses=1 type=[18 x i8]*]
@p_str4 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_OUTER\00", align 1 ; [#uses=3 type=[18 x i8]*]
@p_str3 = private unnamed_addr constant [16 x i8] c"DELETE_BV_ALPHA\00", align 1 ; [#uses=1 type=[16 x i8]*]
@p_str20 = internal unnamed_addr constant [1 x i8] zeroinitializer ; [#uses=10 type=[1 x i8]*]
@p_str2 = private unnamed_addr constant [20 x i8] c"DELETE_BV_SWAP_LOOP\00", align 1 ; [#uses=3 type=[20 x i8]*]
@p_str19 = private unnamed_addr constant [11 x i8] c"INIT_INNER\00", align 1 ; [#uses=1 type=[11 x i8]*]
@p_str18 = private unnamed_addr constant [11 x i8] c"INIT_OUTER\00", align 1 ; [#uses=3 type=[11 x i8]*]
@p_str17 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1 ; [#uses=5 type=[10 x i8]*]
@p_str16 = private unnamed_addr constant [8 x i8] c"COPY_BV\00", align 1 ; [#uses=2 type=[8 x i8]*]
@p_str15 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_INNER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@p_str14 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_OUTER\00", align 1 ; [#uses=3 type=[15 x i8]*]
@p_str13 = private unnamed_addr constant [15 x i8] c"UPDATE_C_INNER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@p_str12 = private unnamed_addr constant [15 x i8] c"UPDATE_C_OUTER\00", align 1 ; [#uses=3 type=[15 x i8]*]
@p_str11 = private unnamed_addr constant [13 x i8] c"UPDATE_ALPHA\00", align 1 ; [#uses=1 type=[13 x i8]*]
@p_str10 = private unnamed_addr constant [7 x i8] c"CALC_S\00", align 1 ; [#uses=1 type=[7 x i8]*]
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=23 type=[1 x i8]*]
@p_str = private unnamed_addr constant [12 x i8] c"hls_label_0\00", align 1 ; [#uses=4 type=[12 x i8]*]

; [#uses=1]
define internal fastcc void @projection_gp_train_full_bv_set([13 x float]* nocapture %pX, float %pY) {
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([13 x float]* %pX, [1 x i8]* @p_str20, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str20, i32 -1, [1 x i8]* @p_str20, [1 x i8]* @p_str20, [1 x i8]* @p_str20) ; [#uses=0 type=i32]
  %pY_read = call float @_ssdm_op_Read.ap_auto.float(float %pY) ; [#uses=1 type=float]
  call void @llvm.dbg.value(metadata !{float %pY_read}, i64 0, metadata !50), !dbg !60 ; [debug line = 122:57] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{[13 x float]* %pX}, i64 0, metadata !61), !dbg !65 ; [debug line = 122:37] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !50), !dbg !60 ; [debug line = 122:57] [debug variable = pY]
  br label %1, !dbg !66                           ; [debug line = 126:35]

; <label>:1                                       ; preds = %K.exit, %0
  %i = phi i6 [ 0, %0 ], [ %i_1, %K.exit ]        ; [#uses=3 type=i6]
  %m = phi float [ 0x4050B830E0000000, %0 ], [ %m_1, %K.exit ] ; [#uses=2 type=float]
  %phi_mul = phi i10 [ 0, %0 ], [ %next_mul, %K.exit ] ; [#uses=2 type=i10]
  %next_mul = add i10 %phi_mul, 13                ; [#uses=1 type=i10]
  %exitcond8 = icmp eq i6 %i, -24, !dbg !66       ; [#uses=1 type=i1] [debug line = 126:35]
  %i_1 = add i6 %i, 1, !dbg !69                   ; [#uses=1 type=i6] [debug line = 126:45]
  br i1 %exitcond8, label %.preheader10, label %2, !dbg !66 ; [debug line = 126:35]

; <label>:2                                       ; preds = %1
  %empty_4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str7) nounwind, !dbg !70 ; [debug line = 126:51]
  %tmp_2 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str7), !dbg !70 ; [#uses=1 type=i32] [debug line = 126:51]
  call void @llvm.dbg.value(metadata !{[13 x float]* %pX}, i64 0, metadata !72), !dbg !77 ; [debug line = 16:42@128:16] [debug variable = pX2]
  br label %3, !dbg !78                           ; [debug line = 18:28@128:16]

; <label>:3                                       ; preds = %4, %2
  %sum_i = phi float [ 0.000000e+00, %2 ], [ %sum, %4 ] ; [#uses=2 type=float]
  %i_i = phi i4 [ 0, %2 ], [ %i_7, %4 ]           ; [#uses=4 type=i4]
  %exitcond_i = icmp eq i4 %i_i, -3, !dbg !78     ; [#uses=1 type=i1] [debug line = 18:28@128:16]
  %i_7 = add i4 %i_i, 1, !dbg !81                 ; [#uses=1 type=i4] [debug line = 18:38@128:16]
  br i1 %exitcond_i, label %K.exit, label %4, !dbg !78 ; [debug line = 18:28@128:16]

; <label>:4                                       ; preds = %3
  %empty_5 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) ; [#uses=0 type=i32]
  %tmp_106_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str), !dbg !82 ; [#uses=1 type=i32] [debug line = 18:44@128:16]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind, !dbg !84 ; [debug line = 19:1@128:16]
  %tmp_104_i = zext i4 %i_i to i64, !dbg !85      ; [#uses=1 type=i64] [debug line = 20:31@128:16]
  %tmp_104_i_cast = zext i4 %i_i to i10           ; [#uses=1 type=i10]
  %sum1_i = add i10 %tmp_104_i_cast, %phi_mul     ; [#uses=1 type=i10]
  %sum1_i_cast = zext i10 %sum1_i to i64          ; [#uses=1 type=i64]
  %basisVectors_addr = getelementptr [533 x float]* @basisVectors, i64 0, i64 %sum1_i_cast, !dbg !85 ; [#uses=1 type=float*] [debug line = 20:31@128:16]
  %basisVectors_load = load float* %basisVectors_addr, align 4, !dbg !85 ; [#uses=1 type=float] [debug line = 20:31@128:16]
  %pX_addr = getelementptr [13 x float]* %pX, i64 0, i64 %tmp_104_i, !dbg !85 ; [#uses=1 type=float*] [debug line = 20:31@128:16]
  %pX_load = load float* %pX_addr, align 4, !dbg !85 ; [#uses=1 type=float] [debug line = 20:31@128:16]
  %val = fsub float %basisVectors_load, %pX_load, !dbg !85 ; [#uses=2 type=float] [debug line = 20:31@128:16]
  call void @llvm.dbg.value(metadata !{float %val}, i64 0, metadata !86), !dbg !85 ; [debug line = 20:31@128:16] [debug variable = val]
  %tmp_105_i = fmul float %val, %val, !dbg !87    ; [#uses=1 type=float] [debug line = 21:9@128:16]
  %sum = fadd float %sum_i, %tmp_105_i, !dbg !87  ; [#uses=1 type=float] [debug line = 21:9@128:16]
  call void @llvm.dbg.value(metadata !{float %sum}, i64 0, metadata !88), !dbg !87 ; [debug line = 21:9@128:16] [debug variable = sum]
  %empty_6 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_106_i), !dbg !89 ; [#uses=0 type=i32] [debug line = 22:5@128:16]
  call void @llvm.dbg.value(metadata !{i4 %i_7}, i64 0, metadata !90), !dbg !81 ; [debug line = 18:38@128:16] [debug variable = i]
  br label %3, !dbg !81                           ; [debug line = 18:38@128:16]

K.exit:                                           ; preds = %3
  %p_x_assign = fmul float %sum_i, -5.000000e-01, !dbg !92 ; [#uses=1 type=float] [debug line = 24:12@128:16]
  call void @llvm.dbg.value(metadata !{float %p_x_assign}, i64 0, metadata !93) nounwind, !dbg !99 ; [debug line = 216:13@24:12@128:16] [debug variable = __x]
  %tmp_i_i = call float @llvm.exp.f32(float %p_x_assign) nounwind, !dbg !100 ; [#uses=2 type=float] [debug line = 217:12@24:12@128:16]
  %tmp_3 = zext i6 %i to i64, !dbg !76            ; [#uses=2 type=i64] [debug line = 128:16]
  %k_addr = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp_3, !dbg !76 ; [#uses=1 type=float*] [debug line = 128:16]
  store float %tmp_i_i, float* %k_addr, align 4, !dbg !76 ; [debug line = 128:16]
  %alpha_addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_3, !dbg !102 ; [#uses=1 type=float*] [debug line = 129:9]
  %alpha_load = load float* %alpha_addr, align 4, !dbg !102 ; [#uses=1 type=float] [debug line = 129:9]
  %tmp_4 = fmul float %tmp_i_i, %alpha_load, !dbg !102 ; [#uses=1 type=float] [debug line = 129:9]
  %m_1 = fadd float %m, %tmp_4, !dbg !102         ; [#uses=1 type=float] [debug line = 129:9]
  call void @llvm.dbg.value(metadata !{float %m_1}, i64 0, metadata !103), !dbg !102 ; [debug line = 129:9] [debug variable = m]
  %empty_7 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str7, i32 %tmp_2), !dbg !104 ; [#uses=0 type=i32] [debug line = 130:5]
  call void @llvm.dbg.value(metadata !{i6 %i_1}, i64 0, metadata !105), !dbg !69 ; [debug line = 126:45] [debug variable = i]
  br label %1, !dbg !69                           ; [debug line = 126:45]

.preheader10:                                     ; preds = %8, %1
  %i1 = phi i6 [ %i_3, %8 ], [ 0, %1 ]            ; [#uses=3 type=i6]
  %phi_mul6 = phi i11 [ %next_mul7, %8 ], [ 0, %1 ] ; [#uses=2 type=i11]
  %next_mul7 = add i11 %phi_mul6, 41              ; [#uses=1 type=i11]
  %exitcond7 = icmp eq i6 %i1, -24, !dbg !106     ; [#uses=1 type=i1] [debug line = 137:45]
  %empty_8 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  %i_3 = add i6 %i1, 1, !dbg !108                 ; [#uses=1 type=i6] [debug line = 137:55]
  br i1 %exitcond7, label %.preheader9, label %5, !dbg !106 ; [debug line = 137:45]

; <label>:5                                       ; preds = %.preheader10
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str8) nounwind, !dbg !109 ; [debug line = 137:61]
  %tmp_1 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str8), !dbg !109 ; [#uses=1 type=i32] [debug line = 137:61]
  %tmp_7 = zext i6 %i1 to i64, !dbg !111          ; [#uses=2 type=i64] [debug line = 139:6]
  %s_addr = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_7, !dbg !111 ; [#uses=2 type=float*] [debug line = 139:6]
  store float 0.000000e+00, float* %s_addr, align 4, !dbg !111 ; [debug line = 139:6]
  %e_addr = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp_7, !dbg !112 ; [#uses=1 type=float*] [debug line = 140:6]
  br label %6, !dbg !113                          ; [debug line = 141:49]

; <label>:6                                       ; preds = %7, %5
  %storemerge = phi float [ 0.000000e+00, %5 ], [ %tmp_20, %7 ] ; [#uses=2 type=float]
  %tmp_s = phi float [ 0.000000e+00, %5 ], [ %tmp_18, %7 ] ; [#uses=1 type=float]
  %j = phi i6 [ 0, %5 ], [ %j_1, %7 ]             ; [#uses=4 type=i6]
  store float %storemerge, float* %e_addr, align 4, !dbg !115 ; [debug line = 144:10]
  %j_cast = zext i6 %j to i11, !dbg !113          ; [#uses=1 type=i11] [debug line = 141:49]
  %exitcond6 = icmp eq i6 %j, -24, !dbg !113      ; [#uses=1 type=i1] [debug line = 141:49]
  %empty_9 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  %j_1 = add i6 %j, 1, !dbg !117                  ; [#uses=1 type=i6] [debug line = 141:59]
  br i1 %exitcond6, label %8, label %7, !dbg !113 ; [debug line = 141:49]

; <label>:7                                       ; preds = %6
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str9) nounwind, !dbg !118 ; [debug line = 141:65]
  %tmp_14 = add i11 %j_cast, %phi_mul6, !dbg !119 ; [#uses=1 type=i11] [debug line = 143:10]
  %tmp_15 = zext i11 %tmp_14 to i64, !dbg !119    ; [#uses=2 type=i64] [debug line = 143:10]
  %C_addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_15, !dbg !119 ; [#uses=1 type=float*] [debug line = 143:10]
  %C_load = load float* %C_addr, align 4, !dbg !119 ; [#uses=1 type=float] [debug line = 143:10]
  %tmp_16 = zext i6 %j to i64, !dbg !119          ; [#uses=1 type=i64] [debug line = 143:10]
  %k_addr_2 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp_16, !dbg !119 ; [#uses=1 type=float*] [debug line = 143:10]
  %k_load_1 = load float* %k_addr_2, align 4, !dbg !119 ; [#uses=2 type=float] [debug line = 143:10]
  %tmp_17 = fmul float %C_load, %k_load_1, !dbg !119 ; [#uses=1 type=float] [debug line = 143:10]
  %tmp_18 = fadd float %tmp_s, %tmp_17, !dbg !119 ; [#uses=2 type=float] [debug line = 143:10]
  store float %tmp_18, float* %s_addr, align 4, !dbg !119 ; [debug line = 143:10]
  %Q_addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_15, !dbg !115 ; [#uses=1 type=float*] [debug line = 144:10]
  %Q_load = load float* %Q_addr, align 4, !dbg !115 ; [#uses=1 type=float] [debug line = 144:10]
  %tmp_19 = fmul float %Q_load, %k_load_1, !dbg !115 ; [#uses=1 type=float] [debug line = 144:10]
  %tmp_20 = fadd float %storemerge, %tmp_19, !dbg !115 ; [#uses=1 type=float] [debug line = 144:10]
  call void @llvm.dbg.value(metadata !{i6 %j_1}, i64 0, metadata !120), !dbg !117 ; [debug line = 141:59] [debug variable = j]
  br label %6, !dbg !117                          ; [debug line = 141:59]

; <label>:8                                       ; preds = %6
  %empty_10 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str8, i32 %tmp_1), !dbg !121 ; [#uses=0 type=i32] [debug line = 146:5]
  call void @llvm.dbg.value(metadata !{i6 %i_3}, i64 0, metadata !122), !dbg !108 ; [debug line = 137:55] [debug variable = i]
  br label %.preheader10, !dbg !108               ; [debug line = 137:55]

.preheader9:                                      ; preds = %9, %.preheader10
  %i2 = phi i6 [ %i_2, %9 ], [ 0, %.preheader10 ] ; [#uses=3 type=i6]
  %sigma2 = phi float [ %sigma2_1, %9 ], [ 1.000000e+00, %.preheader10 ] ; [#uses=2 type=float]
  %exitcond5 = icmp eq i6 %i2, -24, !dbg !123     ; [#uses=1 type=i1] [debug line = 148:36]
  %empty_11 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  %i_2 = add i6 %i2, 1, !dbg !125                 ; [#uses=1 type=i6] [debug line = 148:46]
  br i1 %exitcond5, label %10, label %9, !dbg !123 ; [debug line = 148:36]

; <label>:9                                       ; preds = %.preheader9
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str10) nounwind, !dbg !126 ; [debug line = 148:52]
  %tmp_12 = zext i6 %i2 to i64, !dbg !128         ; [#uses=2 type=i64] [debug line = 150:6]
  %s_addr_1 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_12, !dbg !128 ; [#uses=1 type=float*] [debug line = 150:6]
  %s_load = load float* %s_addr_1, align 4, !dbg !128 ; [#uses=1 type=float] [debug line = 150:6]
  %k_addr_1 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp_12, !dbg !128 ; [#uses=1 type=float*] [debug line = 150:6]
  %k_load = load float* %k_addr_1, align 4, !dbg !128 ; [#uses=1 type=float] [debug line = 150:6]
  %tmp_13 = fmul float %s_load, %k_load, !dbg !128 ; [#uses=1 type=float] [debug line = 150:6]
  %sigma2_1 = fadd float %sigma2, %tmp_13, !dbg !128 ; [#uses=1 type=float] [debug line = 150:6]
  call void @llvm.dbg.value(metadata !{float %sigma2_1}, i64 0, metadata !129), !dbg !128 ; [debug line = 150:6] [debug variable = sigma2]
  call void @llvm.dbg.value(metadata !{i6 %i_2}, i64 0, metadata !130), !dbg !125 ; [debug line = 148:46] [debug variable = i]
  br label %.preheader9, !dbg !125                ; [debug line = 148:46]

; <label>:10                                      ; preds = %.preheader9
  store float 1.000000e+00, float* getelementptr inbounds ([41 x float]* @s, i64 0, i64 40), align 16, !dbg !131 ; [debug line = 152:5]
  %tmp_5 = fsub float %pY_read, %m, !dbg !132     ; [#uses=1 type=float] [debug line = 154:40]
  %tmp_6 = fpext float %tmp_5 to double, !dbg !132 ; [#uses=1 type=double] [debug line = 154:40]
  %tmp_8 = fpext float %sigma2 to double, !dbg !132 ; [#uses=1 type=double] [debug line = 154:40]
  %tmp_9 = fadd double %tmp_8, 1.000000e+00, !dbg !132 ; [#uses=2 type=double] [debug line = 154:40]
  %tmp_10 = fdiv double %tmp_6, %tmp_9, !dbg !132 ; [#uses=1 type=double] [debug line = 154:40]
  %q = fptrunc double %tmp_10 to float, !dbg !132 ; [#uses=2 type=float] [debug line = 154:40]
  call void @llvm.dbg.value(metadata !{float %q}, i64 0, metadata !133), !dbg !132 ; [debug line = 154:40] [debug variable = q]
  %tmp_11 = fdiv double -1.000000e+00, %tmp_9, !dbg !134 ; [#uses=1 type=double] [debug line = 155:35]
  %r = fptrunc double %tmp_11 to float, !dbg !134 ; [#uses=1 type=float] [debug line = 155:35]
  call void @llvm.dbg.value(metadata !{float %r}, i64 0, metadata !135), !dbg !134 ; [debug line = 155:35] [debug variable = r]
  br label %11, !dbg !136                         ; [debug line = 158:42]

; <label>:11                                      ; preds = %12, %10
  %gamma = phi float [ 1.000000e+00, %10 ], [ %gamma_1, %12 ] ; [#uses=2 type=float]
  %i3 = phi i6 [ 0, %10 ], [ %i_4, %12 ]          ; [#uses=3 type=i6]
  %exitcond4 = icmp eq i6 %i3, -24, !dbg !136     ; [#uses=1 type=i1] [debug line = 158:42]
  %empty_12 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  %i_4 = add i6 %i3, 1, !dbg !138                 ; [#uses=1 type=i6] [debug line = 158:52]
  br i1 %exitcond4, label %13, label %12, !dbg !136 ; [debug line = 158:42]

; <label>:12                                      ; preds = %11
  call void (...)* @_ssdm_op_SpecLoopName([13 x i8]* @p_str11) nounwind, !dbg !139 ; [debug line = 158:58]
  %tmp_22 = zext i6 %i3 to i64, !dbg !141         ; [#uses=4 type=i64] [debug line = 160:6]
  %e_addr_1 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp_22, !dbg !141 ; [#uses=1 type=float*] [debug line = 160:6]
  %e_load = load float* %e_addr_1, align 4, !dbg !141 ; [#uses=1 type=float] [debug line = 160:6]
  %k_addr_3 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp_22, !dbg !141 ; [#uses=1 type=float*] [debug line = 160:6]
  %k_load_2 = load float* %k_addr_3, align 4, !dbg !141 ; [#uses=1 type=float] [debug line = 160:6]
  %tmp_23 = fmul float %e_load, %k_load_2, !dbg !141 ; [#uses=1 type=float] [debug line = 160:6]
  %gamma_1 = fsub float %gamma, %tmp_23, !dbg !141 ; [#uses=1 type=float] [debug line = 160:6]
  call void @llvm.dbg.value(metadata !{float %gamma_1}, i64 0, metadata !142), !dbg !141 ; [debug line = 160:6] [debug variable = gamma]
  %s_addr_2 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_22, !dbg !143 ; [#uses=1 type=float*] [debug line = 161:6]
  %s_load_1 = load float* %s_addr_2, align 4, !dbg !143 ; [#uses=1 type=float] [debug line = 161:6]
  %tmp_24 = fmul float %s_load_1, %q, !dbg !143   ; [#uses=1 type=float] [debug line = 161:6]
  %alpha_addr_1 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_22, !dbg !143 ; [#uses=2 type=float*] [debug line = 161:6]
  %alpha_load_2 = load float* %alpha_addr_1, align 4, !dbg !143 ; [#uses=1 type=float] [debug line = 161:6]
  %tmp_25 = fadd float %alpha_load_2, %tmp_24, !dbg !143 ; [#uses=1 type=float] [debug line = 161:6]
  store float %tmp_25, float* %alpha_addr_1, align 4, !dbg !143 ; [debug line = 161:6]
  call void @llvm.dbg.value(metadata !{i6 %i_4}, i64 0, metadata !144), !dbg !138 ; [debug line = 158:52] [debug variable = i]
  br label %11, !dbg !138                         ; [debug line = 158:52]

; <label>:13                                      ; preds = %11
  %alpha_load_1 = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !145 ; [#uses=1 type=float] [debug line = 168:5]
  %tmp_21 = fadd float %alpha_load_1, %q, !dbg !145 ; [#uses=1 type=float] [debug line = 168:5]
  store float %tmp_21, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !145 ; [debug line = 168:5]
  br label %14, !dbg !146                         ; [debug line = 171:43]

; <label>:14                                      ; preds = %18, %13
  %i4 = phi i6 [ 0, %13 ], [ %i_5, %18 ]          ; [#uses=3 type=i6]
  %phi_mul8 = phi i11 [ 0, %13 ], [ %next_mul9, %18 ] ; [#uses=2 type=i11]
  %next_mul9 = add i11 %phi_mul8, 41              ; [#uses=1 type=i11]
  %exitcond3 = icmp eq i6 %i4, -23, !dbg !146     ; [#uses=1 type=i1] [debug line = 171:43]
  %empty_13 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  %i_5 = add i6 %i4, 1, !dbg !148                 ; [#uses=1 type=i6] [debug line = 171:54]
  br i1 %exitcond3, label %.preheader.preheader, label %15, !dbg !146 ; [debug line = 171:43]

.preheader.preheader:                             ; preds = %14
  %tmp_26 = fpext float %gamma to double, !dbg !149 ; [#uses=1 type=double] [debug line = 187:4]
  br label %.preheader, !dbg !154                 ; [debug line = 180:43]

; <label>:15                                      ; preds = %14
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str12) nounwind, !dbg !155 ; [debug line = 171:60]
  %tmp_27 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str12), !dbg !155 ; [#uses=1 type=i32] [debug line = 171:60]
  %tmp_28 = zext i6 %i4 to i64, !dbg !157         ; [#uses=1 type=i64] [debug line = 176:13]
  %s_addr_3 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_28, !dbg !157 ; [#uses=1 type=float*] [debug line = 176:13]
  %s_load_2 = load float* %s_addr_3, align 4, !dbg !157 ; [#uses=1 type=float] [debug line = 176:13]
  br label %16, !dbg !160                         ; [debug line = 173:44]

; <label>:16                                      ; preds = %17, %15
  %j5 = phi i6 [ 0, %15 ], [ %j_2, %17 ]          ; [#uses=4 type=i6]
  %j5_cast8 = zext i6 %j5 to i11, !dbg !160       ; [#uses=1 type=i11] [debug line = 173:44]
  %exitcond2 = icmp eq i6 %j5, -23, !dbg !160     ; [#uses=1 type=i1] [debug line = 173:44]
  %empty_14 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  %j_2 = add i6 %j5, 1, !dbg !161                 ; [#uses=1 type=i6] [debug line = 173:55]
  br i1 %exitcond2, label %18, label %17, !dbg !160 ; [debug line = 173:44]

; <label>:17                                      ; preds = %16
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str13) nounwind, !dbg !162 ; [debug line = 173:61]
  %tmp_32 = zext i6 %j5 to i64, !dbg !157         ; [#uses=1 type=i64] [debug line = 176:13]
  %s_addr_4 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_32, !dbg !157 ; [#uses=1 type=float*] [debug line = 176:13]
  %s_load_3 = load float* %s_addr_4, align 4, !dbg !157 ; [#uses=1 type=float] [debug line = 176:13]
  %tmp_33 = fmul float %s_load_2, %s_load_3, !dbg !157 ; [#uses=1 type=float] [debug line = 176:13]
  %tmp_34 = fmul float %tmp_33, %r, !dbg !157     ; [#uses=1 type=float] [debug line = 176:13]
  %tmp_35 = add i11 %j5_cast8, %phi_mul8, !dbg !157 ; [#uses=1 type=i11] [debug line = 176:13]
  %tmp_36 = zext i11 %tmp_35 to i64, !dbg !157    ; [#uses=1 type=i64] [debug line = 176:13]
  %C_addr_1 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_36, !dbg !157 ; [#uses=2 type=float*] [debug line = 176:13]
  %C_load_1 = load float* %C_addr_1, align 4, !dbg !157 ; [#uses=1 type=float] [debug line = 176:13]
  %tmp_37 = fadd float %C_load_1, %tmp_34, !dbg !157 ; [#uses=1 type=float] [debug line = 176:13]
  store float %tmp_37, float* %C_addr_1, align 4, !dbg !157 ; [debug line = 176:13]
  call void @llvm.dbg.value(metadata !{i6 %j_2}, i64 0, metadata !163), !dbg !161 ; [debug line = 173:55] [debug variable = j]
  br label %16, !dbg !161                         ; [debug line = 173:55]

; <label>:18                                      ; preds = %16
  %empty_15 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str12, i32 %tmp_27), !dbg !164 ; [#uses=0 type=i32] [debug line = 178:5]
  call void @llvm.dbg.value(metadata !{i6 %i_5}, i64 0, metadata !165), !dbg !148 ; [debug line = 171:54] [debug variable = i]
  br label %14, !dbg !148                         ; [debug line = 171:54]

.preheader:                                       ; preds = %21, %.preheader.preheader
  %i6 = phi i6 [ %i_6, %21 ], [ 0, %.preheader.preheader ] ; [#uses=4 type=i6]
  %phi_mul1 = phi i11 [ %next_mul1, %21 ], [ 0, %.preheader.preheader ] ; [#uses=2 type=i11]
  %next_mul1 = add i11 %phi_mul1, 41              ; [#uses=1 type=i11]
  %exitcond1 = icmp eq i6 %i6, -23, !dbg !154     ; [#uses=1 type=i1] [debug line = 180:43]
  %empty_16 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  %i_6 = add i6 %i6, 1, !dbg !166                 ; [#uses=1 type=i6] [debug line = 180:54]
  br i1 %exitcond1, label %.preheader11, label %19, !dbg !154 ; [debug line = 180:43]

; <label>:19                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str14) nounwind, !dbg !167 ; [debug line = 180:60]
  %tmp_29 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str14), !dbg !167 ; [#uses=1 type=i32] [debug line = 180:60]
  %tmp_30 = icmp eq i6 %i6, -24, !dbg !168        ; [#uses=1 type=i1] [debug line = 184:37]
  %tmp_31 = zext i6 %i6 to i64, !dbg !168         ; [#uses=1 type=i64] [debug line = 184:37]
  %e_addr_2 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp_31, !dbg !168 ; [#uses=1 type=float*] [debug line = 184:37]
  %e_load_1 = load float* %e_addr_2, align 4, !dbg !168 ; [#uses=1 type=float] [debug line = 184:37]
  %ti = select i1 %tmp_30, float -1.000000e+00, float %e_load_1, !dbg !168 ; [#uses=1 type=float] [debug line = 184:37]
  br label %20, !dbg !169                         ; [debug line = 182:44]

; <label>:20                                      ; preds = %._crit_edge_ifconv, %19
  %j7 = phi i6 [ 0, %19 ], [ %j_3, %._crit_edge_ifconv ] ; [#uses=5 type=i6]
  %j7_cast4 = zext i6 %j7 to i11, !dbg !169       ; [#uses=1 type=i11] [debug line = 182:44]
  %exitcond = icmp eq i6 %j7, -23, !dbg !169      ; [#uses=1 type=i1] [debug line = 182:44]
  %empty_17 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  %j_3 = add i6 %j7, 1, !dbg !170                 ; [#uses=1 type=i6] [debug line = 182:55]
  br i1 %exitcond, label %21, label %._crit_edge_ifconv, !dbg !169 ; [debug line = 182:44]

._crit_edge_ifconv:                               ; preds = %20
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str15) nounwind, !dbg !171 ; [debug line = 182:61]
  call void @llvm.dbg.value(metadata !{float %ti}, i64 0, metadata !172), !dbg !168 ; [debug line = 184:37] [debug variable = ti]
  %tmp_38 = icmp eq i6 %j7, -24, !dbg !173        ; [#uses=1 type=i1] [debug line = 185:34]
  %tmp_39 = zext i6 %j7 to i64, !dbg !173         ; [#uses=1 type=i64] [debug line = 185:34]
  %e_addr_3 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp_39, !dbg !173 ; [#uses=1 type=float*] [debug line = 185:34]
  %e_load_2 = load float* %e_addr_3, align 4, !dbg !173 ; [#uses=1 type=float] [debug line = 185:34]
  %tj = select i1 %tmp_38, float -1.000000e+00, float %e_load_2, !dbg !173 ; [#uses=1 type=float] [debug line = 185:34]
  call void @llvm.dbg.value(metadata !{float %tj}, i64 0, metadata !174), !dbg !173 ; [debug line = 185:34] [debug variable = tj]
  %tmp_40 = fmul float %ti, %tj, !dbg !149        ; [#uses=1 type=float] [debug line = 187:4]
  %tmp_41 = fpext float %tmp_40 to double, !dbg !149 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp_42 = fdiv double %tmp_41, %tmp_26, !dbg !149 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp_43 = add i11 %j7_cast4, %phi_mul1, !dbg !149 ; [#uses=1 type=i11] [debug line = 187:4]
  %tmp_44 = zext i11 %tmp_43 to i64, !dbg !149    ; [#uses=1 type=i64] [debug line = 187:4]
  %Q_addr_1 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_44, !dbg !149 ; [#uses=2 type=float*] [debug line = 187:4]
  %Q_load_1 = load float* %Q_addr_1, align 4, !dbg !149 ; [#uses=1 type=float] [debug line = 187:4]
  %tmp_45 = fpext float %Q_load_1 to double, !dbg !149 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp_46 = fadd double %tmp_45, %tmp_42, !dbg !149 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp_47 = fptrunc double %tmp_46 to float, !dbg !149 ; [#uses=1 type=float] [debug line = 187:4]
  store float %tmp_47, float* %Q_addr_1, align 4, !dbg !149 ; [debug line = 187:4]
  call void @llvm.dbg.value(metadata !{i6 %j_3}, i64 0, metadata !175), !dbg !170 ; [debug line = 182:55] [debug variable = j]
  br label %20, !dbg !170                         ; [debug line = 182:55]

; <label>:21                                      ; preds = %20
  %empty_18 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str14, i32 %tmp_29), !dbg !176 ; [#uses=0 type=i32] [debug line = 189:5]
  call void @llvm.dbg.value(metadata !{i6 %i_6}, i64 0, metadata !177), !dbg !166 ; [debug line = 180:54] [debug variable = i]
  br label %.preheader, !dbg !166                 ; [debug line = 180:54]

.preheader11:                                     ; preds = %22, %.preheader
  %i_i1 = phi i4 [ %i_8, %22 ], [ 0, %.preheader ] ; [#uses=4 type=i4]
  %i_i1_cast2 = zext i4 %i_i1 to i32, !dbg !178   ; [#uses=1 type=i32] [debug line = 215:33@208:5]
  %exitcond_i2 = icmp eq i4 %i_i1, -3, !dbg !178  ; [#uses=1 type=i1] [debug line = 215:33@208:5]
  %empty_19 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) ; [#uses=0 type=i32]
  %i_8 = add i4 %i_i1, 1, !dbg !185               ; [#uses=1 type=i4] [debug line = 215:43@208:5]
  br i1 %exitcond_i2, label %copyBV.exit, label %22, !dbg !178 ; [debug line = 215:33@208:5]

; <label>:22                                      ; preds = %.preheader11
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @p_str16) nounwind, !dbg !186 ; [debug line = 215:49@208:5]
  %tmp_i4 = zext i4 %i_i1 to i64, !dbg !188       ; [#uses=1 type=i64] [debug line = 217:3@208:5]
  %pX_addr_1 = getelementptr [13 x float]* %pX, i64 0, i64 %tmp_i4, !dbg !188 ; [#uses=1 type=float*] [debug line = 217:3@208:5]
  %pX_load_1 = load float* %pX_addr_1, align 4, !dbg !188 ; [#uses=1 type=float] [debug line = 217:3@208:5]
  %bvCnt_load = load i32* @bvCnt, align 4, !dbg !188 ; [#uses=1 type=i32] [debug line = 217:3@208:5]
  %tmp_i = mul i32 %bvCnt_load, 13, !dbg !188     ; [#uses=1 type=i32] [debug line = 217:3@208:5]
  %tmp_102_i = add i32 %tmp_i, %i_i1_cast2, !dbg !188 ; [#uses=1 type=i32] [debug line = 217:3@208:5]
  %tmp_103_i = zext i32 %tmp_102_i to i64, !dbg !188 ; [#uses=1 type=i64] [debug line = 217:3@208:5]
  %basisVectors_addr_1 = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp_103_i, !dbg !188 ; [#uses=1 type=float*] [debug line = 217:3@208:5]
  store float %pX_load_1, float* %basisVectors_addr_1, align 4, !dbg !188 ; [debug line = 217:3@208:5]
  call void @llvm.dbg.value(metadata !{i4 %i_8}, i64 0, metadata !189), !dbg !185 ; [debug line = 215:43@208:5] [debug variable = i]
  br label %.preheader11, !dbg !185               ; [debug line = 215:43@208:5]

copyBV.exit:                                      ; preds = %.preheader11
  %alpha_load_3 = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 0), align 16, !dbg !190 ; [#uses=2 type=float] [debug line = 108:77@210:23]
  %tmp_i5 = fmul float %alpha_load_3, %alpha_load_3, !dbg !190 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  %Q_load_2 = load float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 0), align 16, !dbg !190 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  %C_load_2 = load float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 0), align 16, !dbg !190 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  %tmp_i6 = fadd float %Q_load_2, %C_load_2, !dbg !190 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  %minScore = fdiv float %tmp_i5, %tmp_i6, !dbg !190 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  call void @llvm.dbg.value(metadata !{float %minScore}, i64 0, metadata !196) nounwind, !dbg !190 ; [debug line = 108:77@210:23] [debug variable = minScore]
  br label %23, !dbg !197                         ; [debug line = 110:28@210:23]

; <label>:23                                      ; preds = %24, %copyBV.exit
  %index_1 = phi i6 [ 1, %copyBV.exit ], [ %i_9, %24 ] ; [#uses=5 type=i6]
  %minScore1_i = phi float [ %minScore, %copyBV.exit ], [ %minScore_2, %24 ] ; [#uses=3 type=float]
  %index = phi i32 [ 0, %copyBV.exit ], [ %index_2, %24 ] ; [#uses=2 type=i32]
  %index_1_cast1 = zext i6 %index_1 to i32, !dbg !197 ; [#uses=1 type=i32] [debug line = 110:28@210:23]
  %index_1_cast1_cast = zext i6 %index_1 to i13, !dbg !197 ; [#uses=1 type=i13] [debug line = 110:28@210:23]
  %exitcond_i7 = icmp eq i6 %index_1, -23, !dbg !197 ; [#uses=1 type=i1] [debug line = 110:28@210:23]
  %empty_20 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond_i7, label %getMinKLApprox.exit, label %24, !dbg !197 ; [debug line = 110:28@210:23]

; <label>:24                                      ; preds = %23
  %tmp_68_i = zext i6 %index_1 to i64, !dbg !199  ; [#uses=1 type=i64] [debug line = 111:79@210:23]
  %alpha_addr_2 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_68_i, !dbg !199 ; [#uses=1 type=float*] [debug line = 111:79@210:23]
  %alpha_load_4 = load float* %alpha_addr_2, align 4, !dbg !199 ; [#uses=2 type=float] [debug line = 111:79@210:23]
  %tmp_69_i = fmul float %alpha_load_4, %alpha_load_4, !dbg !199 ; [#uses=1 type=float] [debug line = 111:79@210:23]
  %tmp_70_i = mul i13 42, %index_1_cast1_cast, !dbg !199 ; [#uses=1 type=i13] [debug line = 111:79@210:23]
  %tmp_71_i = zext i13 %tmp_70_i to i64, !dbg !199 ; [#uses=2 type=i64] [debug line = 111:79@210:23]
  %Q_addr_2 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_71_i, !dbg !199 ; [#uses=1 type=float*] [debug line = 111:79@210:23]
  %Q_load_3 = load float* %Q_addr_2, align 8, !dbg !199 ; [#uses=1 type=float] [debug line = 111:79@210:23]
  %C_addr_2 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_71_i, !dbg !199 ; [#uses=1 type=float*] [debug line = 111:79@210:23]
  %C_load_3 = load float* %C_addr_2, align 8, !dbg !199 ; [#uses=1 type=float] [debug line = 111:79@210:23]
  %tmp_72_i = fadd float %Q_load_3, %C_load_3, !dbg !199 ; [#uses=1 type=float] [debug line = 111:79@210:23]
  %tScore = fdiv float %tmp_69_i, %tmp_72_i, !dbg !199 ; [#uses=3 type=float] [debug line = 111:79@210:23]
  call void @llvm.dbg.value(metadata !{float %tScore}, i64 0, metadata !201) nounwind, !dbg !199 ; [debug line = 111:79@210:23] [debug variable = tScore]
  %tScore_to_int = bitcast float %tScore to i32   ; [#uses=2 type=i32]
  %tmp_48 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tScore_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp = trunc i32 %tScore_to_int to i23          ; [#uses=1 type=i23]
  %minScore1_i_to_int = bitcast float %minScore1_i to i32 ; [#uses=2 type=i32]
  %tmp_49 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %minScore1_i_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp_50 = trunc i32 %minScore1_i_to_int to i23  ; [#uses=1 type=i23]
  %notlhs = icmp ne i8 %tmp_48, -1                ; [#uses=1 type=i1]
  %notrhs = icmp eq i23 %tmp, 0                   ; [#uses=1 type=i1]
  %tmp_51 = or i1 %notrhs, %notlhs                ; [#uses=1 type=i1]
  %notlhs1 = icmp ne i8 %tmp_49, -1               ; [#uses=1 type=i1]
  %notrhs1 = icmp eq i23 %tmp_50, 0               ; [#uses=1 type=i1]
  %tmp_52 = or i1 %notrhs1, %notlhs1              ; [#uses=1 type=i1]
  %tmp_53 = and i1 %tmp_51, %tmp_52               ; [#uses=1 type=i1]
  %tmp_54 = fcmp olt float %tScore, %minScore1_i, !dbg !202 ; [#uses=1 type=i1] [debug line = 113:9@210:23]
  %tmp_55 = and i1 %tmp_53, %tmp_54, !dbg !202    ; [#uses=2 type=i1] [debug line = 113:9@210:23]
  call void @llvm.dbg.value(metadata !{float %tScore}, i64 0, metadata !196) nounwind, !dbg !203 ; [debug line = 114:13@210:23] [debug variable = minScore]
  call void @llvm.dbg.value(metadata !{i6 %index_1}, i64 0, metadata !205) nounwind, !dbg !207 ; [debug line = 115:13@210:23] [debug variable = index]
  %minScore_2 = select i1 %tmp_55, float %tScore, float %minScore1_i, !dbg !202 ; [#uses=1 type=float] [debug line = 113:9@210:23]
  call void @llvm.dbg.value(metadata !{float %minScore_2}, i64 0, metadata !196) nounwind, !dbg !202 ; [debug line = 113:9@210:23] [debug variable = minScore]
  %index_2 = select i1 %tmp_55, i32 %index_1_cast1, i32 %index, !dbg !202 ; [#uses=1 type=i32] [debug line = 113:9@210:23]
  call void @llvm.dbg.value(metadata !{i32 %index_2}, i64 0, metadata !205) nounwind, !dbg !202 ; [debug line = 113:9@210:23] [debug variable = index]
  %i_9 = add i6 1, %index_1, !dbg !208            ; [#uses=1 type=i6] [debug line = 110:43@210:23]
  call void @llvm.dbg.value(metadata !{i6 %i_9}, i64 0, metadata !209) nounwind, !dbg !208 ; [debug line = 110:43@210:23] [debug variable = i]
  br label %23, !dbg !208                         ; [debug line = 110:43@210:23]

getMinKLApprox.exit:                              ; preds = %23
  call void @llvm.dbg.value(metadata !{i32 %index}, i64 0, metadata !210), !dbg !195 ; [debug line = 210:23] [debug variable = index]
  call fastcc void @projection_gp_deleteBV(i32 %index), !dbg !211 ; [debug line = 211:2]
  ret void, !dbg !212                             ; [debug line = 212:1]
}

; [#uses=2]
define internal fastcc void @projection_gp_swapRowAndColumn([1681 x float]* nocapture %pM, i32 %rowA) {
  %rowA_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %rowA) ; [#uses=2 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %rowA_read}, i64 0, metadata !213), !dbg !218 ; [debug line = 27:65] [debug variable = rowA]
  call void @llvm.dbg.value(metadata !{[1681 x float]* %pM}, i64 0, metadata !219), !dbg !223 ; [debug line = 27:29] [debug variable = pM]
  call void @llvm.dbg.value(metadata !{i32 %rowA}, i64 0, metadata !213), !dbg !218 ; [debug line = 27:65] [debug variable = rowA]
  %tmp = mul i32 %rowA_read, 41, !dbg !224        ; [#uses=1 type=i32] [debug line = 30:40]
  br label %1, !dbg !228                          ; [debug line = 28:28]

; <label>:1                                       ; preds = %2, %0
  %i = phi i6 [ 0, %0 ], [ %i_7, %2 ]             ; [#uses=4 type=i6]
  %i_cast4_cast = zext i6 %i to i10, !dbg !228    ; [#uses=1 type=i10] [debug line = 28:28]
  %i_cast3 = zext i6 %i to i32, !dbg !228         ; [#uses=1 type=i32] [debug line = 28:28]
  %exitcond1 = icmp eq i6 %i, -23, !dbg !228      ; [#uses=1 type=i1] [debug line = 28:28]
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  %i_7 = add i6 %i, 1, !dbg !229                  ; [#uses=1 type=i6] [debug line = 28:43]
  br i1 %exitcond1, label %.preheader, label %2, !dbg !228 ; [debug line = 28:28]

; <label>:2                                       ; preds = %1
  %tmp_s = add i32 %i_cast3, %tmp, !dbg !224      ; [#uses=1 type=i32] [debug line = 30:40]
  %tmp_50 = zext i32 %tmp_s to i64, !dbg !224     ; [#uses=1 type=i64] [debug line = 30:40]
  %pM_addr = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp_50, !dbg !224 ; [#uses=2 type=float*] [debug line = 30:40]
  %temp = load float* %pM_addr, align 4, !dbg !224 ; [#uses=1 type=float] [debug line = 30:40]
  call void @llvm.dbg.value(metadata !{float %temp}, i64 0, metadata !230), !dbg !224 ; [debug line = 30:40] [debug variable = temp]
  %tmp_51 = add i10 %i_cast4_cast, -408, !dbg !231 ; [#uses=1 type=i10] [debug line = 31:9]
  %tmp_51_cast5 = sext i10 %tmp_51 to i11, !dbg !231 ; [#uses=1 type=i11] [debug line = 31:9]
  %tmp_52 = zext i11 %tmp_51_cast5 to i64, !dbg !231 ; [#uses=1 type=i64] [debug line = 31:9]
  %pM_addr_1 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp_52, !dbg !231 ; [#uses=2 type=float*] [debug line = 31:9]
  %pM_load = load float* %pM_addr_1, align 4, !dbg !231 ; [#uses=1 type=float] [debug line = 31:9]
  store float %pM_load, float* %pM_addr, align 4, !dbg !231 ; [debug line = 31:9]
  store float %temp, float* %pM_addr_1, align 4, !dbg !232 ; [debug line = 32:9]
  call void @llvm.dbg.value(metadata !{i6 %i_7}, i64 0, metadata !233), !dbg !229 ; [debug line = 28:43] [debug variable = i]
  br label %1, !dbg !229                          ; [debug line = 28:43]

.preheader:                                       ; preds = %3, %1
  %i1 = phi i6 [ %i_8, %3 ], [ 0, %1 ]            ; [#uses=2 type=i6]
  %phi_mul = phi i11 [ %next_mul, %3 ], [ 0, %1 ] ; [#uses=3 type=i11]
  %exitcond = icmp eq i6 %i1, -23, !dbg !234      ; [#uses=1 type=i1] [debug line = 35:28]
  %empty_21 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  %i_8 = add i6 %i1, 1, !dbg !236                 ; [#uses=1 type=i6] [debug line = 35:43]
  br i1 %exitcond, label %4, label %3, !dbg !234  ; [debug line = 35:28]

; <label>:3                                       ; preds = %.preheader
  %next_mul = add i11 %phi_mul, 41                ; [#uses=1 type=i11]
  %tmp_53_cast6 = zext i11 %phi_mul to i32, !dbg !237 ; [#uses=1 type=i32] [debug line = 37:40]
  %tmp_54 = add i32 %tmp_53_cast6, %rowA_read, !dbg !237 ; [#uses=1 type=i32] [debug line = 37:40]
  %tmp_55 = zext i32 %tmp_54 to i64, !dbg !237    ; [#uses=1 type=i64] [debug line = 37:40]
  %pM_addr_2 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp_55, !dbg !237 ; [#uses=2 type=float*] [debug line = 37:40]
  %temp_1 = load float* %pM_addr_2, align 4, !dbg !237 ; [#uses=1 type=float] [debug line = 37:40]
  call void @llvm.dbg.value(metadata !{float %temp_1}, i64 0, metadata !239), !dbg !237 ; [debug line = 37:40] [debug variable = temp]
  %tmp_56 = add i11 %phi_mul, 40, !dbg !240       ; [#uses=1 type=i11] [debug line = 38:9]
  %tmp_57 = zext i11 %tmp_56 to i64, !dbg !240    ; [#uses=1 type=i64] [debug line = 38:9]
  %pM_addr_3 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp_57, !dbg !240 ; [#uses=2 type=float*] [debug line = 38:9]
  %pM_load_2 = load float* %pM_addr_3, align 4, !dbg !240 ; [#uses=1 type=float] [debug line = 38:9]
  store float %pM_load_2, float* %pM_addr_2, align 4, !dbg !240 ; [debug line = 38:9]
  store float %temp_1, float* %pM_addr_3, align 4, !dbg !241 ; [debug line = 39:9]
  call void @llvm.dbg.value(metadata !{i6 %i_8}, i64 0, metadata !242), !dbg !236 ; [debug line = 35:43] [debug variable = i]
  br label %.preheader, !dbg !236                 ; [debug line = 35:43]

; <label>:4                                       ; preds = %.preheader
  ret void, !dbg !243                             ; [debug line = 41:1]
}

; [#uses=0]
define float @projection_gp([13 x float]* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pReset) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([13 x float]* %pX) nounwind, !map !244
  call void (...)* @_ssdm_op_SpecBitsMap(float %pY) nounwind, !map !250
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pPredict) nounwind, !map !256
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pReset) nounwind, !map !260
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !264
  call void (...)* @_ssdm_op_SpecTopModule([14 x i8]* @projection_gp_str) nounwind
  %pReset_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pReset) nounwind ; [#uses=1 type=i1]
  call void @llvm.dbg.value(metadata !{i1 %pReset_read}, i64 0, metadata !270), !dbg !276 ; [debug line = 221:89] [debug variable = pReset]
  %pPredict_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pPredict) nounwind ; [#uses=1 type=i1]
  call void @llvm.dbg.value(metadata !{i1 %pPredict_read}, i64 0, metadata !277), !dbg !278 ; [debug line = 221:68] [debug variable = pPredict]
  %pY_read = call float @_ssdm_op_Read.s_axilite.float(float %pY) nounwind ; [#uses=1 type=float]
  call void @llvm.dbg.value(metadata !{float %pY_read}, i64 0, metadata !279), !dbg !280 ; [debug line = 221:53] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{[13 x float]* %pX}, i64 0, metadata !281), !dbg !282 ; [debug line = 221:33] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !279), !dbg !280 ; [debug line = 221:53] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{i1 %pPredict}, i64 0, metadata !277), !dbg !278 ; [debug line = 221:68] [debug variable = pPredict]
  call void @llvm.dbg.value(metadata !{i1 %pReset}, i64 0, metadata !270), !dbg !276 ; [debug line = 221:89] [debug variable = pReset]
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([13 x float]* %pX, [1 x i8]* @p_str20, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str20, i32 -1, [1 x i8]* @p_str20, [1 x i8]* @p_str20, [1 x i8]* @p_str20) nounwind ; [#uses=0 type=i32]
  call void (...)* @_ssdm_op_SpecInterface([13 x float]* %pX, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 13, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(float %pY, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !283 ; [debug line = 223:1]
  call void (...)* @_ssdm_op_SpecInterface(i1 %pPredict, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !285 ; [debug line = 224:1]
  call void (...)* @_ssdm_op_SpecInterface(i1 %pReset, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !286 ; [debug line = 225:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !287 ; [debug line = 226:1]
  br i1 %pReset_read, label %.preheader3, label %.loopexit4, !dbg !288 ; [debug line = 228:2]

.preheader3:                                      ; preds = %4, %0
  %i = phi i6 [ %i_9, %4 ], [ 0, %0 ]             ; [#uses=6 type=i6]
  %exitcond2 = icmp eq i6 %i, -23, !dbg !289      ; [#uses=1 type=i1] [debug line = 229:37]
  %empty_22 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind ; [#uses=0 type=i32]
  %i_9 = add i6 %i, 1, !dbg !292                  ; [#uses=1 type=i6] [debug line = 229:52]
  br i1 %exitcond2, label %.loopexit4, label %1, !dbg !289 ; [debug line = 229:37]

; <label>:1                                       ; preds = %.preheader3
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @p_str18) nounwind, !dbg !293 ; [debug line = 229:58]
  %tmp = call i32 (...)* @_ssdm_op_SpecRegionBegin([11 x i8]* @p_str18) nounwind, !dbg !293 ; [#uses=1 type=i32] [debug line = 229:58]
  %p_shl = call i10 @_ssdm_op_BitConcatenate.i10.i6.i4(i6 %i, i4 0), !dbg !295 ; [#uses=1 type=i10] [debug line = 234:6]
  %p_shl_cast = zext i10 %p_shl to i11, !dbg !295 ; [#uses=1 type=i11] [debug line = 234:6]
  %p_shl1 = call i7 @_ssdm_op_BitConcatenate.i7.i6.i1(i6 %i, i1 false), !dbg !295 ; [#uses=1 type=i7] [debug line = 234:6]
  %p_shl1_cast = zext i7 %p_shl1 to i11, !dbg !295 ; [#uses=1 type=i11] [debug line = 234:6]
  %tmp_s = sub i11 %p_shl_cast, %p_shl1_cast, !dbg !295 ; [#uses=1 type=i11] [debug line = 234:6]
  %tmp_cast = sext i11 %tmp_s to i32, !dbg !295   ; [#uses=1 type=i32] [debug line = 234:6]
  %tmp_58 = zext i32 %tmp_cast to i64, !dbg !295  ; [#uses=2 type=i64] [debug line = 234:6]
  %Q_addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_58, !dbg !295 ; [#uses=1 type=float*] [debug line = 234:6]
  %C_addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_58, !dbg !299 ; [#uses=1 type=float*] [debug line = 235:6]
  br label %2, !dbg !300                          ; [debug line = 231:39]

; <label>:2                                       ; preds = %3, %1
  %j = phi i6 [ 0, %1 ], [ %j_4, %3 ]             ; [#uses=3 type=i6]
  %exitcond1 = icmp eq i6 %j, -23, !dbg !300      ; [#uses=1 type=i1] [debug line = 231:39]
  %empty_23 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind ; [#uses=0 type=i32]
  %j_4 = add i6 %j, 1, !dbg !301                  ; [#uses=1 type=i6] [debug line = 231:54]
  br i1 %exitcond1, label %4, label %3, !dbg !300 ; [debug line = 231:39]

; <label>:3                                       ; preds = %2
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @p_str19) nounwind, !dbg !302 ; [debug line = 231:60]
  %tmp_61 = icmp eq i6 %i, %j, !dbg !303          ; [#uses=1 type=i1] [debug line = 233:5]
  %storemerge5 = select i1 %tmp_61, float 1.000000e+00, float 0.000000e+00, !dbg !303 ; [#uses=2 type=float] [debug line = 233:5]
  store float %storemerge5, float* %Q_addr, align 8, !dbg !304 ; [debug line = 237:6]
  store float %storemerge5, float* %C_addr, align 8, !dbg !299 ; [debug line = 235:6]
  call void @llvm.dbg.value(metadata !{i6 %j_4}, i64 0, metadata !306), !dbg !301 ; [debug line = 231:54] [debug variable = j]
  br label %2, !dbg !301                          ; [debug line = 231:54]

; <label>:4                                       ; preds = %2
  %tmp_60 = zext i6 %i to i64, !dbg !307          ; [#uses=1 type=i64] [debug line = 241:4]
  %alpha_addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_60, !dbg !307 ; [#uses=1 type=float*] [debug line = 241:4]
  store float 0.000000e+00, float* %alpha_addr, align 4, !dbg !307 ; [debug line = 241:4]
  %empty_24 = call i32 (...)* @_ssdm_op_SpecRegionEnd([11 x i8]* @p_str18, i32 %tmp) nounwind, !dbg !308 ; [#uses=0 type=i32] [debug line = 242:3]
  call void @llvm.dbg.value(metadata !{i6 %i_9}, i64 0, metadata !309), !dbg !292 ; [debug line = 229:52] [debug variable = i]
  br label %.preheader3, !dbg !292                ; [debug line = 229:52]

.loopexit4:                                       ; preds = %.preheader3, %0
  br i1 %pPredict_read, label %.preheader.preheader, label %5, !dbg !310 ; [debug line = 245:2]

; <label>:5                                       ; preds = %.loopexit4
  %bvCnt_load = load i32* @bvCnt, align 4, !dbg !311 ; [#uses=2 type=i32] [debug line = 246:3]
  %tmp_59 = icmp eq i32 %bvCnt_load, 40, !dbg !311 ; [#uses=1 type=i1] [debug line = 246:3]
  br i1 %tmp_59, label %6, label %.preheader6, !dbg !311 ; [debug line = 246:3]

; <label>:6                                       ; preds = %5
  call fastcc void @projection_gp_train_full_bv_set([13 x float]* %pX, float %pY_read) nounwind, !dbg !313 ; [debug line = 247:4]
  br label %8, !dbg !315                          ; [debug line = 248:3]

.preheader6:                                      ; preds = %7, %5
  %bvCnt_load_1 = phi i32 [ %bvCnt_load_2, %7 ], [ %bvCnt_load, %5 ] ; [#uses=1 type=i32]
  %i_i = phi i4 [ %i_2, %7 ], [ 0, %5 ]           ; [#uses=4 type=i4]
  %i_i_cast4 = zext i4 %i_i to i32, !dbg !316     ; [#uses=1 type=i32] [debug line = 215:33@249:4]
  %exitcond_i = icmp eq i4 %i_i, -3, !dbg !316    ; [#uses=1 type=i1] [debug line = 215:33@249:4]
  %empty_25 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) nounwind ; [#uses=0 type=i32]
  %i_2 = add i4 %i_i, 1, !dbg !319                ; [#uses=1 type=i4] [debug line = 215:43@249:4]
  br i1 %exitcond_i, label %copyBV.exit, label %7, !dbg !316 ; [debug line = 215:33@249:4]

; <label>:7                                       ; preds = %.preheader6
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @p_str16) nounwind, !dbg !320 ; [debug line = 215:49@249:4]
  %tmp_i = zext i4 %i_i to i64, !dbg !321         ; [#uses=1 type=i64] [debug line = 217:3@249:4]
  %pX_addr = getelementptr [13 x float]* %pX, i64 0, i64 %tmp_i, !dbg !321 ; [#uses=1 type=float*] [debug line = 217:3@249:4]
  %pX_load = load float* %pX_addr, align 4, !dbg !321 ; [#uses=1 type=float] [debug line = 217:3@249:4]
  %bvCnt_load_2 = load i32* @bvCnt, align 4, !dbg !321 ; [#uses=2 type=i32] [debug line = 217:3@249:4]
  %tmp_i_26 = mul i32 %bvCnt_load_2, 13, !dbg !321 ; [#uses=1 type=i32] [debug line = 217:3@249:4]
  %tmp_102_i = add i32 %tmp_i_26, %i_i_cast4, !dbg !321 ; [#uses=1 type=i32] [debug line = 217:3@249:4]
  %tmp_103_i = zext i32 %tmp_102_i to i64, !dbg !321 ; [#uses=1 type=i64] [debug line = 217:3@249:4]
  %basisVectors_addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp_103_i, !dbg !321 ; [#uses=1 type=float*] [debug line = 217:3@249:4]
  store float %pX_load, float* %basisVectors_addr, align 4, !dbg !321 ; [debug line = 217:3@249:4]
  call void @llvm.dbg.value(metadata !{i4 %i_2}, i64 0, metadata !322) nounwind, !dbg !319 ; [debug line = 215:43@249:4] [debug variable = i]
  br label %.preheader6, !dbg !319                ; [debug line = 215:43@249:4]

copyBV.exit:                                      ; preds = %.preheader6
  %tmp_62 = add i32 %bvCnt_load_1, 1, !dbg !323   ; [#uses=1 type=i32] [debug line = 250:4]
  store i32 %tmp_62, i32* @bvCnt, align 4, !dbg !323 ; [debug line = 250:4]
  br label %8

; <label>:8                                       ; preds = %copyBV.exit, %6
  br label %.loopexit, !dbg !324                  ; [debug line = 252:3]

.preheader.preheader:                             ; preds = %K.exit, %.loopexit4
  %indvar_flatten = phi i10 [ %indvar_flatten_next, %K.exit ], [ 0, %.loopexit4 ] ; [#uses=2 type=i10]
  %sum = phi float [ %sum_mid2, %K.exit ], [ 0x4050B830E0000000, %.loopexit4 ] ; [#uses=3 type=float]
  %i1 = phi i6 [ %i1_mid2, %K.exit ], [ 0, %.loopexit4 ] ; [#uses=3 type=i6]
  %sum_i = phi float [ %sum_2, %K.exit ], [ 0.000000e+00, %.loopexit4 ] ; [#uses=2 type=float]
  %i_i2 = phi i4 [ %i_1, %K.exit ], [ 0, %.loopexit4 ] ; [#uses=2 type=i4]
  %p_x_assign_dup = fmul float %sum_i, -5.000000e-01, !dbg !325 ; [#uses=2 type=float] [debug line = 24:12@257:11]
  call void @llvm.dbg.value(metadata !{float %p_x_assign_dup}, i64 0, metadata !330) nounwind, !dbg !331 ; [debug line = 216:13@24:12@257:11] [debug variable = __x]
  %tmp_65_dup = zext i6 %i1 to i64, !dbg !326     ; [#uses=1 type=i64] [debug line = 257:11]
  %alpha_addr_3 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_65_dup, !dbg !326 ; [#uses=1 type=float*] [debug line = 257:11]
  %alpha_load = load float* %alpha_addr_3, align 4, !dbg !326 ; [#uses=2 type=float] [debug line = 257:11]
  call void @llvm.dbg.value(metadata !{float %sum_1}, i64 0, metadata !332), !dbg !326 ; [debug line = 257:11] [debug variable = sum]
  %exitcond_flatten = icmp eq i10 %indvar_flatten, -504 ; [#uses=1 type=i1]
  %indvar_flatten_next = add i10 %indvar_flatten, 1 ; [#uses=1 type=i10]
  br i1 %exitcond_flatten, label %.loopexit.loopexit, label %K.exit

K.exit:                                           ; preds = %.preheader.preheader
  call void @llvm.dbg.value(metadata !{float %p_x_assign_dup}, i64 0, metadata !330) nounwind, !dbg !331 ; [debug line = 216:13@24:12@257:11] [debug variable = __x]
  %tmp_i_i = call float @llvm.exp.f32(float %p_x_assign_dup) nounwind, !dbg !333 ; [#uses=1 type=float] [debug line = 217:12@24:12@257:11]
  %tmp_64 = fmul float %tmp_i_i, %alpha_load, !dbg !326 ; [#uses=1 type=float] [debug line = 257:11]
  %sum_1 = fadd float %sum, %tmp_64, !dbg !326    ; [#uses=1 type=float] [debug line = 257:11]
  call void @llvm.dbg.value(metadata !{float %sum_1}, i64 0, metadata !332), !dbg !326 ; [debug line = 257:11] [debug variable = sum]
  call void @llvm.dbg.value(metadata !{[13 x float]* %pX}, i64 0, metadata !334) nounwind, !dbg !335 ; [debug line = 16:42@257:11] [debug variable = pX2]
  call void (...)* @_ssdm_op_SpecLoopName([13 x i8]* @PREDICTION_L_str)
  %empty_27 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 520, i64 520, i64 520) nounwind ; [#uses=0 type=i32]
  %exitcond_i1 = icmp eq i4 %i_i2, -3, !dbg !336  ; [#uses=4 type=i1] [debug line = 18:28@257:11]
  %sum_i_mid2 = select i1 %exitcond_i1, float 0.000000e+00, float %sum_i ; [#uses=1 type=float]
  %i_i2_mid2 = select i1 %exitcond_i1, i4 0, i4 %i_i2 ; [#uses=3 type=i4]
  %sum_mid2 = select i1 %exitcond_i1, float %sum_1, float %sum ; [#uses=1 type=float]
  %i_s = add i6 %i1, 1, !dbg !337                 ; [#uses=1 type=i6] [debug line = 255:47]
  %i1_mid2 = select i1 %exitcond_i1, i6 %i_s, i6 %i1 ; [#uses=2 type=i6]
  %i1_cast3 = zext i6 %i1_mid2 to i10, !dbg !335  ; [#uses=1 type=i10] [debug line = 16:42@257:11]
  %tmp_63 = mul i10 %i1_cast3, 13, !dbg !335      ; [#uses=1 type=i10] [debug line = 16:42@257:11]
  %tmp_106_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str) nounwind, !dbg !338 ; [#uses=1 type=i32] [debug line = 18:44@257:11]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind, !dbg !339 ; [debug line = 19:1@257:11]
  %tmp_104_i4 = zext i4 %i_i2_mid2 to i64, !dbg !340 ; [#uses=1 type=i64] [debug line = 20:31@257:11]
  %tmp_104_i4_cast = zext i4 %i_i2_mid2 to i10    ; [#uses=1 type=i10]
  %sum1_i = add i10 %tmp_104_i4_cast, %tmp_63     ; [#uses=1 type=i10]
  %sum1_i_cast = zext i10 %sum1_i to i64          ; [#uses=1 type=i64]
  %basisVectors_addr_2 = getelementptr [533 x float]* @basisVectors, i64 0, i64 %sum1_i_cast, !dbg !340 ; [#uses=1 type=float*] [debug line = 20:31@257:11]
  %basisVectors_load = load float* %basisVectors_addr_2, align 4, !dbg !340 ; [#uses=1 type=float] [debug line = 20:31@257:11]
  %pX_addr_2 = getelementptr [13 x float]* %pX, i64 0, i64 %tmp_104_i4, !dbg !340 ; [#uses=1 type=float*] [debug line = 20:31@257:11]
  %pX_load_2 = load float* %pX_addr_2, align 4, !dbg !340 ; [#uses=1 type=float] [debug line = 20:31@257:11]
  %val = fsub float %basisVectors_load, %pX_load_2, !dbg !340 ; [#uses=2 type=float] [debug line = 20:31@257:11]
  call void @llvm.dbg.value(metadata !{float %val}, i64 0, metadata !341) nounwind, !dbg !340 ; [debug line = 20:31@257:11] [debug variable = val]
  %tmp_105_i = fmul float %val, %val, !dbg !342   ; [#uses=1 type=float] [debug line = 21:9@257:11]
  %sum_2 = fadd float %sum_i_mid2, %tmp_105_i, !dbg !342 ; [#uses=1 type=float] [debug line = 21:9@257:11]
  call void @llvm.dbg.value(metadata !{float %sum_2}, i64 0, metadata !343) nounwind, !dbg !342 ; [debug line = 21:9@257:11] [debug variable = sum]
  %empty_28 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_106_i) nounwind, !dbg !344 ; [#uses=0 type=i32] [debug line = 22:5@257:11]
  %i_1 = add i4 %i_i2_mid2, 1, !dbg !345          ; [#uses=1 type=i4] [debug line = 18:38@257:11]
  call void @llvm.dbg.value(metadata !{i4 %i_1}, i64 0, metadata !346) nounwind, !dbg !345 ; [debug line = 18:38@257:11] [debug variable = i]
  br label %.preheader.preheader, !dbg !345       ; [debug line = 18:38@257:11]

.loopexit.loopexit:                               ; preds = %.preheader.preheader
  %tmp_i_i_dup = call float @llvm.exp.f32(float %p_x_assign_dup) nounwind, !dbg !333 ; [#uses=1 type=float] [debug line = 217:12@24:12@257:11]
  %tmp_66_dup = fmul float %tmp_i_i_dup, %alpha_load, !dbg !326 ; [#uses=1 type=float] [debug line = 257:11]
  %sum_1_dup = fadd float %sum, %tmp_66_dup, !dbg !326 ; [#uses=1 type=float] [debug line = 257:11]
  br label %.loopexit

.loopexit:                                        ; preds = %.loopexit.loopexit, %8
  %p_0 = phi float [ 0.000000e+00, %8 ], [ %sum_1_dup, %.loopexit.loopexit ] ; [#uses=1 type=float]
  ret float %p_0, !dbg !347                       ; [debug line = 262:1]
}

; [#uses=1]
declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

; [#uses=3]
declare float @llvm.exp.f32(float) nounwind readonly

; [#uses=78]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=1]
define internal fastcc void @projection_gp_deleteBV(i32 %pIndex) nounwind uwtable {
  %pIndex_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %pIndex) nounwind ; [#uses=4 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %pIndex_read}, i64 0, metadata !348), !dbg !352 ; [debug line = 43:29] [debug variable = pIndex]
  call void @llvm.dbg.value(metadata !{i32 %pIndex}, i64 0, metadata !348), !dbg !352 ; [debug line = 43:29] [debug variable = pIndex]
  %tmp = zext i32 %pIndex_read to i64, !dbg !353  ; [#uses=1 type=i64] [debug line = 47:31]
  %alpha_addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp, !dbg !353 ; [#uses=2 type=float*] [debug line = 47:31]
  %temp = load float* %alpha_addr, align 4, !dbg !353 ; [#uses=1 type=float] [debug line = 47:31]
  call void @llvm.dbg.value(metadata !{float %temp}, i64 0, metadata !355), !dbg !353 ; [debug line = 47:31] [debug variable = temp]
  %alpha_load = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !356 ; [#uses=1 type=float] [debug line = 48:5]
  store float %alpha_load, float* %alpha_addr, align 4, !dbg !356 ; [debug line = 48:5]
  store float %temp, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !357 ; [debug line = 49:5]
  call fastcc void @projection_gp_swapRowAndColumn([1681 x float]* @C, i32 %pIndex_read) nounwind, !dbg !358 ; [debug line = 51:5]
  call fastcc void @projection_gp_swapRowAndColumn([1681 x float]* @Q, i32 %pIndex_read) nounwind, !dbg !359 ; [debug line = 52:5]
  %tmp_s = mul i32 %pIndex_read, 13, !dbg !360    ; [#uses=1 type=i32] [debug line = 56:2]
  br label %1, !dbg !363                          ; [debug line = 54:48]

; <label>:1                                       ; preds = %2, %0
  %i = phi i4 [ 0, %0 ], [ %i_10, %2 ]            ; [#uses=4 type=i4]
  %exitcond4 = icmp eq i4 %i, -3, !dbg !363       ; [#uses=1 type=i1] [debug line = 54:48]
  %i_10 = add i4 %i, 1, !dbg !364                 ; [#uses=1 type=i4] [debug line = 54:58]
  br i1 %exitcond4, label %3, label %2, !dbg !363 ; [debug line = 54:48]

; <label>:2                                       ; preds = %1
  %i_cast9 = zext i4 %i to i10, !dbg !363         ; [#uses=1 type=i10] [debug line = 54:48]
  %i_cast8 = zext i4 %i to i32, !dbg !363         ; [#uses=1 type=i32] [debug line = 54:48]
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) nounwind ; [#uses=0 type=i32]
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @p_str2) nounwind, !dbg !365 ; [debug line = 54:64]
  %tmp_66 = call i32 (...)* @_ssdm_op_SpecRegionBegin([20 x i8]* @p_str2) nounwind, !dbg !365 ; [#uses=1 type=i32] [debug line = 54:64]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind, !dbg !366 ; [debug line = 55:1]
  %tmp_67 = add i32 %i_cast8, %tmp_s, !dbg !360   ; [#uses=1 type=i32] [debug line = 56:2]
  %tmp_68 = zext i32 %tmp_67 to i64, !dbg !360    ; [#uses=1 type=i64] [debug line = 56:2]
  %basisVectors_addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp_68, !dbg !360 ; [#uses=2 type=float*] [debug line = 56:2]
  %temp_3 = load float* %basisVectors_addr, align 4, !dbg !360 ; [#uses=1 type=float] [debug line = 56:2]
  call void @llvm.dbg.value(metadata !{float %temp_3}, i64 0, metadata !355), !dbg !360 ; [debug line = 56:2] [debug variable = temp]
  %tmp_69 = add i10 %i_cast9, -504, !dbg !367     ; [#uses=1 type=i10] [debug line = 57:9]
  %tmp_70 = zext i10 %tmp_69 to i64, !dbg !367    ; [#uses=1 type=i64] [debug line = 57:9]
  %basisVectors_addr_1 = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp_70, !dbg !367 ; [#uses=2 type=float*] [debug line = 57:9]
  %basisVectors_load = load float* %basisVectors_addr_1, align 4, !dbg !367 ; [#uses=1 type=float] [debug line = 57:9]
  store float %basisVectors_load, float* %basisVectors_addr, align 4, !dbg !367 ; [debug line = 57:9]
  store float %temp_3, float* %basisVectors_addr_1, align 4, !dbg !368 ; [debug line = 58:9]
  %empty_29 = call i32 (...)* @_ssdm_op_SpecRegionEnd([20 x i8]* @p_str2, i32 %tmp_66) nounwind, !dbg !369 ; [#uses=0 type=i32] [debug line = 59:5]
  call void @llvm.dbg.value(metadata !{i4 %i_10}, i64 0, metadata !370), !dbg !364 ; [debug line = 54:58] [debug variable = i]
  br label %1, !dbg !364                          ; [debug line = 54:58]

; <label>:3                                       ; preds = %1
  %alphaStar = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !371 ; [#uses=1 type=float] [debug line = 62:32]
  call void @llvm.dbg.value(metadata !{float %alphaStar}, i64 0, metadata !372), !dbg !371 ; [debug line = 62:32] [debug variable = alphaStar]
  %cStar = load float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 1680), align 16, !dbg !373 ; [#uses=1 type=float] [debug line = 63:24]
  call void @llvm.dbg.value(metadata !{float %cStar}, i64 0, metadata !374), !dbg !373 ; [debug line = 63:24] [debug variable = cStar]
  %qStar = load float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 1680), align 16, !dbg !375 ; [#uses=2 type=float] [debug line = 81:45]
  call void @llvm.dbg.value(metadata !{float %qStar}, i64 0, metadata !380), !dbg !381 ; [debug line = 64:24] [debug variable = qStar]
  %tmp_65 = fadd float %cStar, %qStar, !dbg !382  ; [#uses=2 type=float] [debug line = 65:5]
  %temp_2 = fdiv float %alphaStar, %tmp_65, !dbg !382 ; [#uses=1 type=float] [debug line = 65:5]
  call void @llvm.dbg.value(metadata !{float %temp_2}, i64 0, metadata !355), !dbg !382 ; [debug line = 65:5] [debug variable = temp]
  br label %4, !dbg !383                          ; [debug line = 67:44]

; <label>:4                                       ; preds = %5, %3
  %i1 = phi i6 [ 0, %3 ], [ %i_11, %5 ]           ; [#uses=4 type=i6]
  %i1_cast7_cast = zext i6 %i1 to i10, !dbg !383  ; [#uses=1 type=i10] [debug line = 67:44]
  %exitcond3 = icmp eq i6 %i1, -24, !dbg !383     ; [#uses=1 type=i1] [debug line = 67:44]
  %empty_30 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  %i_11 = add i6 %i1, 1, !dbg !385                ; [#uses=1 type=i6] [debug line = 67:54]
  br i1 %exitcond3, label %6, label %5, !dbg !383 ; [debug line = 67:44]

; <label>:5                                       ; preds = %4
  call void (...)* @_ssdm_op_SpecLoopName([16 x i8]* @p_str3) nounwind, !dbg !386 ; [debug line = 67:60]
  %a = add i10 %i1_cast7_cast, -408, !dbg !388    ; [#uses=1 type=i10] [debug line = 69:35]
  %a_cast = sext i10 %a to i11, !dbg !388         ; [#uses=1 type=i11] [debug line = 69:35]
  call void @llvm.dbg.value(metadata !{i10 %a}, i64 0, metadata !389), !dbg !388 ; [debug line = 69:35] [debug variable = a]
  %tmp_71 = zext i11 %a_cast to i64, !dbg !390    ; [#uses=2 type=i64] [debug line = 70:9]
  %C_addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_71, !dbg !390 ; [#uses=1 type=float*] [debug line = 70:9]
  %C_load = load float* %C_addr, align 4, !dbg !390 ; [#uses=1 type=float] [debug line = 70:9]
  %Q_addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_71, !dbg !390 ; [#uses=1 type=float*] [debug line = 70:9]
  %Q_load = load float* %Q_addr, align 4, !dbg !390 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp_73 = fadd float %C_load, %Q_load, !dbg !390 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp_74 = fmul float %tmp_73, %temp_2, !dbg !390 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp_75 = zext i6 %i1 to i64, !dbg !390         ; [#uses=1 type=i64] [debug line = 70:9]
  %alpha_addr_3 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_75, !dbg !390 ; [#uses=2 type=float*] [debug line = 70:9]
  %alpha_load_5 = load float* %alpha_addr_3, align 4, !dbg !390 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp_76 = fsub float %alpha_load_5, %tmp_74, !dbg !390 ; [#uses=1 type=float] [debug line = 70:9]
  store float %tmp_76, float* %alpha_addr_3, align 4, !dbg !390 ; [debug line = 70:9]
  call void @llvm.dbg.value(metadata !{i6 %i_11}, i64 0, metadata !391), !dbg !385 ; [debug line = 67:54] [debug variable = i]
  br label %4, !dbg !385                          ; [debug line = 67:54]

; <label>:6                                       ; preds = %4
  store float 0.000000e+00, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !392 ; [debug line = 72:5]
  br label %7, !dbg !393                          ; [debug line = 74:45]

; <label>:7                                       ; preds = %11, %6
  %i2 = phi i6 [ 0, %6 ], [ %i_12, %11 ]          ; [#uses=3 type=i6]
  %phi_mul = phi i11 [ 0, %6 ], [ %next_mul, %11 ] ; [#uses=2 type=i11]
  %next_mul = add i11 %phi_mul, 41                ; [#uses=1 type=i11]
  %i2_cast4_cast = zext i6 %i2 to i10, !dbg !393  ; [#uses=1 type=i10] [debug line = 74:45]
  %exitcond2 = icmp eq i6 %i2, -24, !dbg !393     ; [#uses=1 type=i1] [debug line = 74:45]
  %empty_31 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  %i_12 = add i6 %i2, 1, !dbg !394                ; [#uses=1 type=i6] [debug line = 74:55]
  br i1 %exitcond2, label %.preheader, label %8, !dbg !393 ; [debug line = 74:45]

; <label>:8                                       ; preds = %7
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @p_str4) nounwind, !dbg !395 ; [debug line = 74:61]
  %tmp_72 = call i32 (...)* @_ssdm_op_SpecRegionBegin([18 x i8]* @p_str4) nounwind, !dbg !395 ; [#uses=1 type=i32] [debug line = 74:61]
  %a_2 = add i10 %i2_cast4_cast, -408, !dbg !396  ; [#uses=1 type=i10] [debug line = 78:39]
  %a_2_cast = sext i10 %a_2 to i11, !dbg !396     ; [#uses=1 type=i11] [debug line = 78:39]
  %tmp_77 = zext i11 %a_2_cast to i64, !dbg !375  ; [#uses=2 type=i64] [debug line = 81:45]
  %Q_addr_2 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_77, !dbg !375 ; [#uses=1 type=float*] [debug line = 81:45]
  %C_addr_2 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_77, !dbg !397 ; [#uses=1 type=float*] [debug line = 84:13]
  br label %9, !dbg !398                          ; [debug line = 76:47]

; <label>:9                                       ; preds = %10, %8
  %j = phi i6 [ 0, %8 ], [ %j_5, %10 ]            ; [#uses=4 type=i6]
  %j_cast3 = zext i6 %j to i11, !dbg !398         ; [#uses=1 type=i11] [debug line = 76:47]
  %j_cast3_cast = zext i6 %j to i10, !dbg !398    ; [#uses=1 type=i10] [debug line = 76:47]
  %exitcond1 = icmp eq i6 %j, -24, !dbg !398      ; [#uses=1 type=i1] [debug line = 76:47]
  %empty_32 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  %j_5 = add i6 %j, 1, !dbg !399                  ; [#uses=1 type=i6] [debug line = 76:57]
  br i1 %exitcond1, label %11, label %10, !dbg !398 ; [debug line = 76:47]

; <label>:10                                      ; preds = %9
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @p_str5) nounwind, !dbg !400 ; [debug line = 76:63]
  call void @llvm.dbg.value(metadata !{i10 %a_2}, i64 0, metadata !401), !dbg !396 ; [debug line = 78:39] [debug variable = a]
  %b_1 = add i10 %j_cast3_cast, -408, !dbg !402   ; [#uses=1 type=i10] [debug line = 79:39]
  %b_1_cast = sext i10 %b_1 to i11, !dbg !402     ; [#uses=1 type=i11] [debug line = 79:39]
  call void @llvm.dbg.value(metadata !{i10 %b_1}, i64 0, metadata !403), !dbg !402 ; [debug line = 79:39] [debug variable = b]
  %c = add i11 %j_cast3, %phi_mul, !dbg !404      ; [#uses=1 type=i11] [debug line = 80:38]
  call void @llvm.dbg.value(metadata !{i11 %c}, i64 0, metadata !405), !dbg !404 ; [debug line = 80:38] [debug variable = c]
  %Q_load_3 = load float* %Q_addr_2, align 4, !dbg !375 ; [#uses=2 type=float] [debug line = 81:45]
  %tmp_80 = zext i11 %b_1_cast to i64, !dbg !375  ; [#uses=2 type=i64] [debug line = 81:45]
  %Q_addr_5 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_80, !dbg !375 ; [#uses=1 type=float*] [debug line = 81:45]
  %Q_load_4 = load float* %Q_addr_5, align 4, !dbg !375 ; [#uses=2 type=float] [debug line = 81:45]
  %tmp_81 = fmul float %Q_load_3, %Q_load_4, !dbg !375 ; [#uses=2 type=float] [debug line = 81:45]
  %temp_4 = fdiv float %tmp_81, %qStar, !dbg !375 ; [#uses=2 type=float] [debug line = 81:45]
  call void @llvm.dbg.value(metadata !{float %temp_4}, i64 0, metadata !406), !dbg !375 ; [debug line = 81:45] [debug variable = temp]
  %C_load_3 = load float* %C_addr_2, align 4, !dbg !397 ; [#uses=2 type=float] [debug line = 84:13]
  %C_addr_5 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_80, !dbg !397 ; [#uses=1 type=float*] [debug line = 84:13]
  %C_load_4 = load float* %C_addr_5, align 4, !dbg !397 ; [#uses=2 type=float] [debug line = 84:13]
  %tmp_82 = fmul float %C_load_3, %C_load_4, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_83 = fmul float %C_load_3, %Q_load_4, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_84 = fadd float %tmp_82, %tmp_83, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_85 = fmul float %Q_load_3, %C_load_4, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_86 = fadd float %tmp_84, %tmp_85, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_87 = fadd float %tmp_86, %tmp_81, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_88 = fdiv float %tmp_87, %tmp_65, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_89 = fsub float %temp_4, %tmp_88, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_90 = zext i11 %c to i64, !dbg !397         ; [#uses=2 type=i64] [debug line = 84:13]
  %C_addr_6 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_90, !dbg !397 ; [#uses=2 type=float*] [debug line = 84:13]
  %C_load_5 = load float* %C_addr_6, align 4, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp_91 = fadd float %C_load_5, %tmp_89, !dbg !397 ; [#uses=1 type=float] [debug line = 84:13]
  store float %tmp_91, float* %C_addr_6, align 4, !dbg !397 ; [debug line = 84:13]
  %Q_addr_6 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_90, !dbg !407 ; [#uses=2 type=float*] [debug line = 90:13]
  %Q_load_5 = load float* %Q_addr_6, align 4, !dbg !407 ; [#uses=1 type=float] [debug line = 90:13]
  %tmp_92 = fsub float %Q_load_5, %temp_4, !dbg !407 ; [#uses=1 type=float] [debug line = 90:13]
  store float %tmp_92, float* %Q_addr_6, align 4, !dbg !407 ; [debug line = 90:13]
  call void @llvm.dbg.value(metadata !{i6 %j_5}, i64 0, metadata !408), !dbg !399 ; [debug line = 76:57] [debug variable = j]
  br label %9, !dbg !399                          ; [debug line = 76:57]

; <label>:11                                      ; preds = %9
  %empty_33 = call i32 (...)* @_ssdm_op_SpecRegionEnd([18 x i8]* @p_str4, i32 %tmp_72) nounwind, !dbg !409 ; [#uses=0 type=i32] [debug line = 92:5]
  call void @llvm.dbg.value(metadata !{i6 %i_12}, i64 0, metadata !410), !dbg !394 ; [debug line = 74:55] [debug variable = i]
  br label %7, !dbg !394                          ; [debug line = 74:55]

.preheader:                                       ; preds = %12, %7
  %i5 = phi i6 [ %i_3, %12 ], [ 0, %7 ]           ; [#uses=3 type=i6]
  %phi_mul6 = phi i11 [ %next_mul7, %12 ], [ 0, %7 ] ; [#uses=2 type=i11]
  %i5_cast2_cast = zext i6 %i5 to i10, !dbg !411  ; [#uses=1 type=i10] [debug line = 94:48]
  %exitcond = icmp eq i6 %i5, -23, !dbg !411      ; [#uses=1 type=i1] [debug line = 94:48]
  %empty_34 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind ; [#uses=0 type=i32]
  %i_3 = add i6 %i5, 1, !dbg !413                 ; [#uses=1 type=i6] [debug line = 94:59]
  br i1 %exitcond, label %13, label %12, !dbg !411 ; [debug line = 94:48]

; <label>:12                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @p_str6) nounwind, !dbg !414 ; [debug line = 94:65]
  %next_mul7 = add i11 %phi_mul6, 41              ; [#uses=1 type=i11]
  %a_1 = add i11 %phi_mul6, 40, !dbg !416         ; [#uses=1 type=i11] [debug line = 96:35]
  call void @llvm.dbg.value(metadata !{i11 %a_1}, i64 0, metadata !417), !dbg !416 ; [debug line = 96:35] [debug variable = a]
  %b = add i10 %i5_cast2_cast, -408, !dbg !418    ; [#uses=1 type=i10] [debug line = 97:35]
  %b_cast = sext i10 %b to i11, !dbg !418         ; [#uses=1 type=i11] [debug line = 97:35]
  call void @llvm.dbg.value(metadata !{i10 %b}, i64 0, metadata !419), !dbg !418 ; [debug line = 97:35] [debug variable = b]
  %tmp_78 = zext i11 %a_1 to i64, !dbg !420       ; [#uses=2 type=i64] [debug line = 99:6]
  %Q_addr_3 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_78, !dbg !420 ; [#uses=1 type=float*] [debug line = 99:6]
  store float 0.000000e+00, float* %Q_addr_3, align 4, !dbg !420 ; [debug line = 99:6]
  %tmp_79 = zext i11 %b_cast to i64, !dbg !421    ; [#uses=2 type=i64] [debug line = 100:9]
  %Q_addr_4 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_79, !dbg !421 ; [#uses=1 type=float*] [debug line = 100:9]
  store float 0.000000e+00, float* %Q_addr_4, align 4, !dbg !421 ; [debug line = 100:9]
  %C_addr_3 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_78, !dbg !422 ; [#uses=1 type=float*] [debug line = 101:9]
  store float 0.000000e+00, float* %C_addr_3, align 4, !dbg !422 ; [debug line = 101:9]
  %C_addr_4 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_79, !dbg !423 ; [#uses=1 type=float*] [debug line = 102:9]
  store float 0.000000e+00, float* %C_addr_4, align 4, !dbg !423 ; [debug line = 102:9]
  call void @llvm.dbg.value(metadata !{i6 %i_3}, i64 0, metadata !424), !dbg !413 ; [debug line = 94:59] [debug variable = i]
  br label %.preheader, !dbg !413                 ; [debug line = 94:59]

; <label>:13                                      ; preds = %.preheader
  ret void, !dbg !425                             ; [debug line = 104:1]
}

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=9]
define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

; [#uses=9]
define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

; [#uses=3]
define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

; [#uses=2]
define weak i32 @_ssdm_op_SpecMemCore(...) {
entry:
  ret i32 0
}

; [#uses=23]
define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

; [#uses=19]
define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

; [#uses=5]
define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

; [#uses=5]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=2]
define weak i1 @_ssdm_op_Read.s_axilite.i1(i1) {
entry:
  ret i1 %0
}

; [#uses=1]
define weak float @_ssdm_op_Read.s_axilite.float(float) {
entry:
  ret float %0
}

; [#uses=2]
define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

; [#uses=1]
define weak float @_ssdm_op_Read.ap_auto.float(float) {
entry:
  ret float %0
}

; [#uses=2]
define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2) ; [#uses=1 type=i32]
  %empty_35 = trunc i32 %empty to i8              ; [#uses=1 type=i8]
  ret i8 %empty_35
}

; [#uses=0]
declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

; [#uses=0]
declare i16 @_ssdm_op_HSub(...)

; [#uses=0]
declare i16 @_ssdm_op_HMul(...)

; [#uses=0]
declare i16 @_ssdm_op_HDiv(...)

; [#uses=0]
declare i16 @_ssdm_op_HAdd(...)

; [#uses=1]
define weak i7 @_ssdm_op_BitConcatenate.i7.i6.i1(i6, i1) nounwind readnone {
entry:
  %empty = zext i6 %0 to i7                       ; [#uses=1 type=i7]
  %empty_36 = zext i1 %1 to i7                    ; [#uses=1 type=i7]
  %empty_37 = shl i7 %empty, 1                    ; [#uses=1 type=i7]
  %empty_38 = or i7 %empty_37, %empty_36          ; [#uses=1 type=i7]
  ret i7 %empty_38
}

; [#uses=1]
define weak i10 @_ssdm_op_BitConcatenate.i10.i6.i4(i6, i4) nounwind readnone {
entry:
  %empty = zext i6 %0 to i10                      ; [#uses=1 type=i10]
  %empty_39 = zext i4 %1 to i10                   ; [#uses=1 type=i10]
  %empty_40 = shl i10 %empty, 4                   ; [#uses=1 type=i10]
  %empty_41 = or i10 %empty_40, %empty_39         ; [#uses=1 type=i10]
  ret i10 %empty_41
}

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0, !7, !14, !19, !26, !33, !38, !45}

!0 = metadata !{metadata !1, [41 x float]* @s}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"s", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 40, i32 1}
!7 = metadata !{metadata !8, [40 x float]* @k}
!8 = metadata !{metadata !9}
!9 = metadata !{i32 0, i32 31, metadata !10}
!10 = metadata !{metadata !11}
!11 = metadata !{metadata !"k", metadata !12, metadata !"float", i32 0, i32 31}
!12 = metadata !{metadata !13}
!13 = metadata !{i32 0, i32 39, i32 1}
!14 = metadata !{metadata !15, [41 x float]* @e}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 31, metadata !17}
!17 = metadata !{metadata !18}
!18 = metadata !{metadata !"e", metadata !5, metadata !"float", i32 0, i32 31}
!19 = metadata !{metadata !20, i32* @bvCnt}
!20 = metadata !{metadata !21}
!21 = metadata !{i32 0, i32 31, metadata !22}
!22 = metadata !{metadata !23}
!23 = metadata !{metadata !"bvCnt", metadata !24, metadata !"unsigned int", i32 0, i32 31}
!24 = metadata !{metadata !25}
!25 = metadata !{i32 0, i32 0, i32 1}
!26 = metadata !{metadata !27, [533 x float]* @basisVectors}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 31, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"basisVectors", metadata !31, metadata !"float", i32 0, i32 31}
!31 = metadata !{metadata !32}
!32 = metadata !{i32 0, i32 532, i32 1}
!33 = metadata !{metadata !34, [41 x float]* @alpha}
!34 = metadata !{metadata !35}
!35 = metadata !{i32 0, i32 31, metadata !36}
!36 = metadata !{metadata !37}
!37 = metadata !{metadata !"alpha", metadata !5, metadata !"float", i32 0, i32 31}
!38 = metadata !{metadata !39, [1681 x float]* @Q}
!39 = metadata !{metadata !40}
!40 = metadata !{i32 0, i32 31, metadata !41}
!41 = metadata !{metadata !42}
!42 = metadata !{metadata !"Q", metadata !43, metadata !"float", i32 0, i32 31}
!43 = metadata !{metadata !44}
!44 = metadata !{i32 0, i32 1680, i32 1}
!45 = metadata !{metadata !46, [1681 x float]* @C}
!46 = metadata !{metadata !47}
!47 = metadata !{i32 0, i32 31, metadata !48}
!48 = metadata !{metadata !49}
!49 = metadata !{metadata !"C", metadata !43, metadata !"float", i32 0, i32 31}
!50 = metadata !{i32 786689, metadata !51, metadata !"pY", metadata !52, i32 33554554, metadata !56, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!51 = metadata !{i32 786478, i32 0, metadata !52, metadata !"train_full_bv_set", metadata !"train_full_bv_set", metadata !"_Z17train_full_bv_setPKff", metadata !52, i32 122, metadata !53, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !58, i32 122} ; [ DW_TAG_subprogram ]
!52 = metadata !{i32 786473, metadata !"../../../implementation/hls/Projection_GP/include/ProjectionGP_FULLY_OPTIMIZED.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", null} ; [ DW_TAG_file_type ]
!53 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !54, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!54 = metadata !{null, metadata !55, metadata !56}
!55 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !56} ; [ DW_TAG_pointer_type ]
!56 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !57} ; [ DW_TAG_const_type ]
!57 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!58 = metadata !{metadata !59}
!59 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!60 = metadata !{i32 122, i32 57, metadata !51, null}
!61 = metadata !{i32 786689, metadata !51, metadata !"pX", null, i32 122, metadata !62, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!62 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !56, metadata !63, i32 0, i32 0} ; [ DW_TAG_array_type ]
!63 = metadata !{metadata !64}
!64 = metadata !{i32 786465, i64 0, i64 12}       ; [ DW_TAG_subrange_type ]
!65 = metadata !{i32 122, i32 37, metadata !51, null}
!66 = metadata !{i32 126, i32 35, metadata !67, null}
!67 = metadata !{i32 786443, metadata !68, i32 126, i32 12, metadata !52, i32 24} ; [ DW_TAG_lexical_block ]
!68 = metadata !{i32 786443, metadata !51, i32 122, i32 61, metadata !52, i32 23} ; [ DW_TAG_lexical_block ]
!69 = metadata !{i32 126, i32 45, metadata !67, null}
!70 = metadata !{i32 126, i32 51, metadata !71, null}
!71 = metadata !{i32 786443, metadata !67, i32 126, i32 50, metadata !52, i32 25} ; [ DW_TAG_lexical_block ]
!72 = metadata !{i32 786689, metadata !73, metadata !"pX2", null, i32 16, metadata !62, i32 0, metadata !76} ; [ DW_TAG_arg_variable ]
!73 = metadata !{i32 786478, i32 0, metadata !52, metadata !"K", metadata !"K", metadata !"_Z1KPKfS0_", metadata !52, i32 16, metadata !74, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !58, i32 16} ; [ DW_TAG_subprogram ]
!74 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !75, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!75 = metadata !{metadata !57, metadata !55, metadata !55}
!76 = metadata !{i32 128, i32 16, metadata !71, null}
!77 = metadata !{i32 16, i32 42, metadata !73, metadata !76}
!78 = metadata !{i32 18, i32 28, metadata !79, metadata !76}
!79 = metadata !{i32 786443, metadata !80, i32 18, i32 5, metadata !52, i32 1} ; [ DW_TAG_lexical_block ]
!80 = metadata !{i32 786443, metadata !73, i32 16, i32 51, metadata !52, i32 0} ; [ DW_TAG_lexical_block ]
!81 = metadata !{i32 18, i32 38, metadata !79, metadata !76}
!82 = metadata !{i32 18, i32 44, metadata !83, metadata !76}
!83 = metadata !{i32 786443, metadata !79, i32 18, i32 43, metadata !52, i32 2} ; [ DW_TAG_lexical_block ]
!84 = metadata !{i32 19, i32 1, metadata !83, metadata !76}
!85 = metadata !{i32 20, i32 31, metadata !83, metadata !76}
!86 = metadata !{i32 786688, metadata !83, metadata !"val", metadata !52, i32 20, metadata !57, i32 0, metadata !76} ; [ DW_TAG_auto_variable ]
!87 = metadata !{i32 21, i32 9, metadata !83, metadata !76}
!88 = metadata !{i32 786688, metadata !80, metadata !"sum", metadata !52, i32 17, metadata !57, i32 0, metadata !76} ; [ DW_TAG_auto_variable ]
!89 = metadata !{i32 22, i32 5, metadata !83, metadata !76}
!90 = metadata !{i32 786688, metadata !79, metadata !"i", metadata !52, i32 18, metadata !91, i32 0, metadata !76} ; [ DW_TAG_auto_variable ]
!91 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!92 = metadata !{i32 24, i32 12, metadata !80, metadata !76}
!93 = metadata !{i32 786689, metadata !94, metadata !"__x", metadata !96, i32 16777432, metadata !57, i32 0, metadata !92} ; [ DW_TAG_arg_variable ]
!94 = metadata !{i32 786478, i32 0, metadata !95, metadata !"exp", metadata !"exp", metadata !"_ZSt3expf", metadata !96, i32 216, metadata !97, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !58, i32 217} ; [ DW_TAG_subprogram ]
!95 = metadata !{i32 786489, null, metadata !"std", metadata !96, i32 76} ; [ DW_TAG_namespace ]
!96 = metadata !{i32 786473, metadata !"/opt/Xilinx/Vivado_HLS/2015.4/lnx64/tools/gcc/lib/gcc/x86_64-unknown-linux-gnu/4.6.3/../../../../include/c++/4.6.3/cmath", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", null} ; [ DW_TAG_file_type ]
!97 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !98, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!98 = metadata !{metadata !57, metadata !57}
!99 = metadata !{i32 216, i32 13, metadata !94, metadata !92}
!100 = metadata !{i32 217, i32 12, metadata !101, metadata !92}
!101 = metadata !{i32 786443, metadata !94, i32 217, i32 3, metadata !96, i32 59} ; [ DW_TAG_lexical_block ]
!102 = metadata !{i32 129, i32 9, metadata !71, null}
!103 = metadata !{i32 786688, metadata !68, metadata !"m", metadata !52, i32 123, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!104 = metadata !{i32 130, i32 5, metadata !71, null}
!105 = metadata !{i32 786688, metadata !67, metadata !"i", metadata !52, i32 126, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!106 = metadata !{i32 137, i32 45, metadata !107, null}
!107 = metadata !{i32 786443, metadata !68, i32 137, i32 22, metadata !52, i32 26} ; [ DW_TAG_lexical_block ]
!108 = metadata !{i32 137, i32 55, metadata !107, null}
!109 = metadata !{i32 137, i32 61, metadata !110, null}
!110 = metadata !{i32 786443, metadata !107, i32 137, i32 60, metadata !52, i32 27} ; [ DW_TAG_lexical_block ]
!111 = metadata !{i32 139, i32 6, metadata !110, null}
!112 = metadata !{i32 140, i32 6, metadata !110, null}
!113 = metadata !{i32 141, i32 49, metadata !114, null}
!114 = metadata !{i32 786443, metadata !110, i32 141, i32 26, metadata !52, i32 28} ; [ DW_TAG_lexical_block ]
!115 = metadata !{i32 144, i32 10, metadata !116, null}
!116 = metadata !{i32 786443, metadata !114, i32 141, i32 64, metadata !52, i32 29} ; [ DW_TAG_lexical_block ]
!117 = metadata !{i32 141, i32 59, metadata !114, null}
!118 = metadata !{i32 141, i32 65, metadata !116, null}
!119 = metadata !{i32 143, i32 10, metadata !116, null}
!120 = metadata !{i32 786688, metadata !114, metadata !"j", metadata !52, i32 141, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!121 = metadata !{i32 146, i32 5, metadata !110, null}
!122 = metadata !{i32 786688, metadata !107, metadata !"i", metadata !52, i32 137, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!123 = metadata !{i32 148, i32 36, metadata !124, null}
!124 = metadata !{i32 786443, metadata !68, i32 148, i32 13, metadata !52, i32 30} ; [ DW_TAG_lexical_block ]
!125 = metadata !{i32 148, i32 46, metadata !124, null}
!126 = metadata !{i32 148, i32 52, metadata !127, null}
!127 = metadata !{i32 786443, metadata !124, i32 148, i32 51, metadata !52, i32 31} ; [ DW_TAG_lexical_block ]
!128 = metadata !{i32 150, i32 6, metadata !127, null}
!129 = metadata !{i32 786688, metadata !68, metadata !"sigma2", metadata !52, i32 136, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!130 = metadata !{i32 786688, metadata !124, metadata !"i", metadata !52, i32 148, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!131 = metadata !{i32 152, i32 5, metadata !68, null}
!132 = metadata !{i32 154, i32 40, metadata !68, null}
!133 = metadata !{i32 786688, metadata !68, metadata !"q", metadata !52, i32 154, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!134 = metadata !{i32 155, i32 35, metadata !68, null}
!135 = metadata !{i32 786688, metadata !68, metadata !"r", metadata !52, i32 155, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!136 = metadata !{i32 158, i32 42, metadata !137, null}
!137 = metadata !{i32 786443, metadata !68, i32 158, i32 19, metadata !52, i32 32} ; [ DW_TAG_lexical_block ]
!138 = metadata !{i32 158, i32 52, metadata !137, null}
!139 = metadata !{i32 158, i32 58, metadata !140, null}
!140 = metadata !{i32 786443, metadata !137, i32 158, i32 57, metadata !52, i32 33} ; [ DW_TAG_lexical_block ]
!141 = metadata !{i32 160, i32 6, metadata !140, null}
!142 = metadata !{i32 786688, metadata !68, metadata !"gamma", metadata !52, i32 156, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!143 = metadata !{i32 161, i32 6, metadata !140, null}
!144 = metadata !{i32 786688, metadata !137, metadata !"i", metadata !52, i32 158, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!145 = metadata !{i32 168, i32 5, metadata !68, null}
!146 = metadata !{i32 171, i32 43, metadata !147, null}
!147 = metadata !{i32 786443, metadata !68, i32 171, i32 20, metadata !52, i32 34} ; [ DW_TAG_lexical_block ]
!148 = metadata !{i32 171, i32 54, metadata !147, null}
!149 = metadata !{i32 187, i32 4, metadata !150, null}
!150 = metadata !{i32 786443, metadata !151, i32 182, i32 60, metadata !52, i32 41} ; [ DW_TAG_lexical_block ]
!151 = metadata !{i32 786443, metadata !152, i32 182, i32 21, metadata !52, i32 40} ; [ DW_TAG_lexical_block ]
!152 = metadata !{i32 786443, metadata !153, i32 180, i32 59, metadata !52, i32 39} ; [ DW_TAG_lexical_block ]
!153 = metadata !{i32 786443, metadata !68, i32 180, i32 20, metadata !52, i32 38} ; [ DW_TAG_lexical_block ]
!154 = metadata !{i32 180, i32 43, metadata !153, null}
!155 = metadata !{i32 171, i32 60, metadata !156, null}
!156 = metadata !{i32 786443, metadata !147, i32 171, i32 59, metadata !52, i32 35} ; [ DW_TAG_lexical_block ]
!157 = metadata !{i32 176, i32 13, metadata !158, null}
!158 = metadata !{i32 786443, metadata !159, i32 173, i32 60, metadata !52, i32 37} ; [ DW_TAG_lexical_block ]
!159 = metadata !{i32 786443, metadata !156, i32 173, i32 21, metadata !52, i32 36} ; [ DW_TAG_lexical_block ]
!160 = metadata !{i32 173, i32 44, metadata !159, null}
!161 = metadata !{i32 173, i32 55, metadata !159, null}
!162 = metadata !{i32 173, i32 61, metadata !158, null}
!163 = metadata !{i32 786688, metadata !159, metadata !"j", metadata !52, i32 173, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!164 = metadata !{i32 178, i32 5, metadata !156, null}
!165 = metadata !{i32 786688, metadata !147, metadata !"i", metadata !52, i32 171, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!166 = metadata !{i32 180, i32 54, metadata !153, null}
!167 = metadata !{i32 180, i32 60, metadata !152, null}
!168 = metadata !{i32 184, i32 37, metadata !150, null}
!169 = metadata !{i32 182, i32 44, metadata !151, null}
!170 = metadata !{i32 182, i32 55, metadata !151, null}
!171 = metadata !{i32 182, i32 61, metadata !150, null}
!172 = metadata !{i32 786688, metadata !150, metadata !"ti", metadata !52, i32 184, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!173 = metadata !{i32 185, i32 34, metadata !150, null}
!174 = metadata !{i32 786688, metadata !150, metadata !"tj", metadata !52, i32 185, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!175 = metadata !{i32 786688, metadata !151, metadata !"j", metadata !52, i32 182, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!176 = metadata !{i32 189, i32 5, metadata !152, null}
!177 = metadata !{i32 786688, metadata !153, metadata !"i", metadata !52, i32 180, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!178 = metadata !{i32 215, i32 33, metadata !179, metadata !184}
!179 = metadata !{i32 786443, metadata !180, i32 215, i32 10, metadata !52, i32 43} ; [ DW_TAG_lexical_block ]
!180 = metadata !{i32 786443, metadata !181, i32 214, i32 54, metadata !52, i32 42} ; [ DW_TAG_lexical_block ]
!181 = metadata !{i32 786478, i32 0, metadata !52, metadata !"copyBV", metadata !"copyBV", metadata !"_Z6copyBVPKfj", metadata !52, i32 214, metadata !182, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !58, i32 214} ; [ DW_TAG_subprogram ]
!182 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !183, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!183 = metadata !{null, metadata !55, metadata !91}
!184 = metadata !{i32 208, i32 5, metadata !68, null}
!185 = metadata !{i32 215, i32 43, metadata !179, metadata !184}
!186 = metadata !{i32 215, i32 49, metadata !187, metadata !184}
!187 = metadata !{i32 786443, metadata !179, i32 215, i32 48, metadata !52, i32 44} ; [ DW_TAG_lexical_block ]
!188 = metadata !{i32 217, i32 3, metadata !187, metadata !184}
!189 = metadata !{i32 786688, metadata !179, metadata !"i", metadata !52, i32 215, metadata !91, i32 0, metadata !184} ; [ DW_TAG_auto_variable ]
!190 = metadata !{i32 108, i32 77, metadata !191, metadata !195}
!191 = metadata !{i32 786443, metadata !192, i32 106, i32 31, metadata !52, i32 19} ; [ DW_TAG_lexical_block ]
!192 = metadata !{i32 786478, i32 0, metadata !52, metadata !"getMinKLApprox", metadata !"getMinKLApprox", metadata !"_Z14getMinKLApproxv", metadata !52, i32 106, metadata !193, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !58, i32 106} ; [ DW_TAG_subprogram ]
!193 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !194, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!194 = metadata !{metadata !91}
!195 = metadata !{i32 210, i32 23, metadata !68, null}
!196 = metadata !{i32 786688, metadata !191, metadata !"minScore", metadata !52, i32 108, metadata !57, i32 0, metadata !195} ; [ DW_TAG_auto_variable ]
!197 = metadata !{i32 110, i32 28, metadata !198, metadata !195}
!198 = metadata !{i32 786443, metadata !191, i32 110, i32 5, metadata !52, i32 20} ; [ DW_TAG_lexical_block ]
!199 = metadata !{i32 111, i32 79, metadata !200, metadata !195}
!200 = metadata !{i32 786443, metadata !198, i32 110, i32 48, metadata !52, i32 21} ; [ DW_TAG_lexical_block ]
!201 = metadata !{i32 786688, metadata !200, metadata !"tScore", metadata !52, i32 111, metadata !57, i32 0, metadata !195} ; [ DW_TAG_auto_variable ]
!202 = metadata !{i32 113, i32 9, metadata !200, metadata !195}
!203 = metadata !{i32 114, i32 13, metadata !204, metadata !195}
!204 = metadata !{i32 786443, metadata !200, i32 113, i32 32, metadata !52, i32 22} ; [ DW_TAG_lexical_block ]
!205 = metadata !{i32 786688, metadata !191, metadata !"index", metadata !52, i32 107, metadata !206, i32 0, metadata !195} ; [ DW_TAG_auto_variable ]
!206 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!207 = metadata !{i32 115, i32 13, metadata !204, metadata !195}
!208 = metadata !{i32 110, i32 43, metadata !198, metadata !195}
!209 = metadata !{i32 786688, metadata !198, metadata !"i", metadata !52, i32 110, metadata !91, i32 0, metadata !195} ; [ DW_TAG_auto_variable ]
!210 = metadata !{i32 786688, metadata !68, metadata !"index", metadata !52, i32 210, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!211 = metadata !{i32 211, i32 2, metadata !68, null}
!212 = metadata !{i32 212, i32 1, metadata !68, null}
!213 = metadata !{i32 786689, metadata !214, metadata !"rowA", metadata !52, i32 33554459, metadata !91, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!214 = metadata !{i32 786478, i32 0, metadata !52, metadata !"swapRowAndColumn", metadata !"swapRowAndColumn", metadata !"_Z16swapRowAndColumnPfjj", metadata !52, i32 27, metadata !215, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !58, i32 27} ; [ DW_TAG_subprogram ]
!215 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !216, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!216 = metadata !{null, metadata !217, metadata !91, metadata !91}
!217 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !57} ; [ DW_TAG_pointer_type ]
!218 = metadata !{i32 27, i32 65, metadata !214, null}
!219 = metadata !{i32 786689, metadata !214, metadata !"pM", null, i32 27, metadata !220, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!220 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 53792, i64 32, i32 0, i32 0, metadata !57, metadata !221, i32 0, i32 0} ; [ DW_TAG_array_type ]
!221 = metadata !{metadata !222}
!222 = metadata !{i32 786465, i64 0, i64 1680}    ; [ DW_TAG_subrange_type ]
!223 = metadata !{i32 27, i32 29, metadata !214, null}
!224 = metadata !{i32 30, i32 40, metadata !225, null}
!225 = metadata !{i32 786443, metadata !226, i32 28, i32 48, metadata !52, i32 5} ; [ DW_TAG_lexical_block ]
!226 = metadata !{i32 786443, metadata !227, i32 28, i32 5, metadata !52, i32 4} ; [ DW_TAG_lexical_block ]
!227 = metadata !{i32 786443, metadata !214, i32 27, i32 91, metadata !52, i32 3} ; [ DW_TAG_lexical_block ]
!228 = metadata !{i32 28, i32 28, metadata !226, null}
!229 = metadata !{i32 28, i32 43, metadata !226, null}
!230 = metadata !{i32 786688, metadata !225, metadata !"temp", metadata !52, i32 30, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!231 = metadata !{i32 31, i32 9, metadata !225, null}
!232 = metadata !{i32 32, i32 9, metadata !225, null}
!233 = metadata !{i32 786688, metadata !226, metadata !"i", metadata !52, i32 28, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!234 = metadata !{i32 35, i32 28, metadata !235, null}
!235 = metadata !{i32 786443, metadata !227, i32 35, i32 5, metadata !52, i32 6} ; [ DW_TAG_lexical_block ]
!236 = metadata !{i32 35, i32 43, metadata !235, null}
!237 = metadata !{i32 37, i32 40, metadata !238, null}
!238 = metadata !{i32 786443, metadata !235, i32 35, i32 48, metadata !52, i32 7} ; [ DW_TAG_lexical_block ]
!239 = metadata !{i32 786688, metadata !238, metadata !"temp", metadata !52, i32 37, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!240 = metadata !{i32 38, i32 9, metadata !238, null}
!241 = metadata !{i32 39, i32 9, metadata !238, null}
!242 = metadata !{i32 786688, metadata !235, metadata !"i", metadata !52, i32 35, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!243 = metadata !{i32 41, i32 1, metadata !227, null}
!244 = metadata !{metadata !245}
!245 = metadata !{i32 0, i32 31, metadata !246}
!246 = metadata !{metadata !247}
!247 = metadata !{metadata !"pX", metadata !248, metadata !"float", i32 0, i32 31}
!248 = metadata !{metadata !249}
!249 = metadata !{i32 0, i32 12, i32 1}
!250 = metadata !{metadata !251}
!251 = metadata !{i32 0, i32 31, metadata !252}
!252 = metadata !{metadata !253}
!253 = metadata !{metadata !"pY", metadata !254, metadata !"float", i32 0, i32 31}
!254 = metadata !{metadata !255}
!255 = metadata !{i32 0, i32 0, i32 0}
!256 = metadata !{metadata !257}
!257 = metadata !{i32 0, i32 0, metadata !258}
!258 = metadata !{metadata !259}
!259 = metadata !{metadata !"pPredict", metadata !254, metadata !"bool", i32 0, i32 0}
!260 = metadata !{metadata !261}
!261 = metadata !{i32 0, i32 0, metadata !262}
!262 = metadata !{metadata !263}
!263 = metadata !{metadata !"pReset", metadata !254, metadata !"bool", i32 0, i32 0}
!264 = metadata !{metadata !265}
!265 = metadata !{i32 0, i32 31, metadata !266}
!266 = metadata !{metadata !267}
!267 = metadata !{metadata !"return", metadata !268, metadata !"float", i32 0, i32 31}
!268 = metadata !{metadata !269}
!269 = metadata !{i32 0, i32 1, i32 0}
!270 = metadata !{i32 786689, metadata !271, metadata !"pReset", metadata !52, i32 67109085, metadata !274, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!271 = metadata !{i32 786478, i32 0, metadata !52, metadata !"projection_gp", metadata !"projection_gp", metadata !"_Z13projection_gpPKffbb", metadata !52, i32 221, metadata !272, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !58, i32 221} ; [ DW_TAG_subprogram ]
!272 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !273, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!273 = metadata !{metadata !57, metadata !55, metadata !56, metadata !274, metadata !274}
!274 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !275} ; [ DW_TAG_const_type ]
!275 = metadata !{i32 786468, null, metadata !"bool", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ]
!276 = metadata !{i32 221, i32 89, metadata !271, null}
!277 = metadata !{i32 786689, metadata !271, metadata !"pPredict", metadata !52, i32 50331869, metadata !274, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!278 = metadata !{i32 221, i32 68, metadata !271, null}
!279 = metadata !{i32 786689, metadata !271, metadata !"pY", metadata !52, i32 33554653, metadata !56, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!280 = metadata !{i32 221, i32 53, metadata !271, null}
!281 = metadata !{i32 786689, metadata !271, metadata !"pX", null, i32 221, metadata !62, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!282 = metadata !{i32 221, i32 33, metadata !271, null}
!283 = metadata !{i32 223, i32 1, metadata !284, null}
!284 = metadata !{i32 786443, metadata !271, i32 221, i32 97, metadata !52, i32 45} ; [ DW_TAG_lexical_block ]
!285 = metadata !{i32 224, i32 1, metadata !284, null}
!286 = metadata !{i32 225, i32 1, metadata !284, null}
!287 = metadata !{i32 226, i32 1, metadata !284, null}
!288 = metadata !{i32 228, i32 2, metadata !284, null}
!289 = metadata !{i32 229, i32 37, metadata !290, null}
!290 = metadata !{i32 786443, metadata !291, i32 229, i32 14, metadata !52, i32 47} ; [ DW_TAG_lexical_block ]
!291 = metadata !{i32 786443, metadata !284, i32 228, i32 14, metadata !52, i32 46} ; [ DW_TAG_lexical_block ]
!292 = metadata !{i32 229, i32 52, metadata !290, null}
!293 = metadata !{i32 229, i32 58, metadata !294, null}
!294 = metadata !{i32 786443, metadata !290, i32 229, i32 57, metadata !52, i32 48} ; [ DW_TAG_lexical_block ]
!295 = metadata !{i32 234, i32 6, metadata !296, null}
!296 = metadata !{i32 786443, metadata !297, i32 233, i32 17, metadata !52, i32 51} ; [ DW_TAG_lexical_block ]
!297 = metadata !{i32 786443, metadata !298, i32 231, i32 59, metadata !52, i32 50} ; [ DW_TAG_lexical_block ]
!298 = metadata !{i32 786443, metadata !294, i32 231, i32 16, metadata !52, i32 49} ; [ DW_TAG_lexical_block ]
!299 = metadata !{i32 235, i32 6, metadata !296, null}
!300 = metadata !{i32 231, i32 39, metadata !298, null}
!301 = metadata !{i32 231, i32 54, metadata !298, null}
!302 = metadata !{i32 231, i32 60, metadata !297, null}
!303 = metadata !{i32 233, i32 5, metadata !297, null}
!304 = metadata !{i32 237, i32 6, metadata !305, null}
!305 = metadata !{i32 786443, metadata !297, i32 236, i32 12, metadata !52, i32 52} ; [ DW_TAG_lexical_block ]
!306 = metadata !{i32 786688, metadata !298, metadata !"j", metadata !52, i32 231, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!307 = metadata !{i32 241, i32 4, metadata !294, null}
!308 = metadata !{i32 242, i32 3, metadata !294, null}
!309 = metadata !{i32 786688, metadata !290, metadata !"i", metadata !52, i32 229, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!310 = metadata !{i32 245, i32 2, metadata !284, null}
!311 = metadata !{i32 246, i32 3, metadata !312, null}
!312 = metadata !{i32 786443, metadata !284, i32 245, i32 17, metadata !52, i32 53} ; [ DW_TAG_lexical_block ]
!313 = metadata !{i32 247, i32 4, metadata !314, null}
!314 = metadata !{i32 786443, metadata !312, i32 246, i32 20, metadata !52, i32 54} ; [ DW_TAG_lexical_block ]
!315 = metadata !{i32 248, i32 3, metadata !314, null}
!316 = metadata !{i32 215, i32 33, metadata !179, metadata !317}
!317 = metadata !{i32 249, i32 4, metadata !318, null}
!318 = metadata !{i32 786443, metadata !312, i32 248, i32 10, metadata !52, i32 55} ; [ DW_TAG_lexical_block ]
!319 = metadata !{i32 215, i32 43, metadata !179, metadata !317}
!320 = metadata !{i32 215, i32 49, metadata !187, metadata !317}
!321 = metadata !{i32 217, i32 3, metadata !187, metadata !317}
!322 = metadata !{i32 786688, metadata !179, metadata !"i", metadata !52, i32 215, metadata !91, i32 0, metadata !317} ; [ DW_TAG_auto_variable ]
!323 = metadata !{i32 250, i32 4, metadata !318, null}
!324 = metadata !{i32 252, i32 3, metadata !312, null}
!325 = metadata !{i32 24, i32 12, metadata !80, metadata !326}
!326 = metadata !{i32 257, i32 11, metadata !327, null}
!327 = metadata !{i32 786443, metadata !328, i32 255, i32 52, metadata !52, i32 58} ; [ DW_TAG_lexical_block ]
!328 = metadata !{i32 786443, metadata !329, i32 255, i32 14, metadata !52, i32 57} ; [ DW_TAG_lexical_block ]
!329 = metadata !{i32 786443, metadata !284, i32 253, i32 9, metadata !52, i32 56} ; [ DW_TAG_lexical_block ]
!330 = metadata !{i32 786689, metadata !94, metadata !"__x", metadata !96, i32 16777432, metadata !57, i32 0, metadata !325} ; [ DW_TAG_arg_variable ]
!331 = metadata !{i32 216, i32 13, metadata !94, metadata !325}
!332 = metadata !{i32 786688, metadata !329, metadata !"sum", metadata !52, i32 254, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!333 = metadata !{i32 217, i32 12, metadata !101, metadata !325}
!334 = metadata !{i32 786689, metadata !73, metadata !"pX2", null, i32 16, metadata !62, i32 0, metadata !326} ; [ DW_TAG_arg_variable ]
!335 = metadata !{i32 16, i32 42, metadata !73, metadata !326}
!336 = metadata !{i32 18, i32 28, metadata !79, metadata !326}
!337 = metadata !{i32 255, i32 47, metadata !328, null}
!338 = metadata !{i32 18, i32 44, metadata !83, metadata !326}
!339 = metadata !{i32 19, i32 1, metadata !83, metadata !326}
!340 = metadata !{i32 20, i32 31, metadata !83, metadata !326}
!341 = metadata !{i32 786688, metadata !83, metadata !"val", metadata !52, i32 20, metadata !57, i32 0, metadata !326} ; [ DW_TAG_auto_variable ]
!342 = metadata !{i32 21, i32 9, metadata !83, metadata !326}
!343 = metadata !{i32 786688, metadata !80, metadata !"sum", metadata !52, i32 17, metadata !57, i32 0, metadata !326} ; [ DW_TAG_auto_variable ]
!344 = metadata !{i32 22, i32 5, metadata !83, metadata !326}
!345 = metadata !{i32 18, i32 38, metadata !79, metadata !326}
!346 = metadata !{i32 786688, metadata !79, metadata !"i", metadata !52, i32 18, metadata !91, i32 0, metadata !326} ; [ DW_TAG_auto_variable ]
!347 = metadata !{i32 262, i32 1, metadata !284, null}
!348 = metadata !{i32 786689, metadata !349, metadata !"pIndex", metadata !52, i32 16777259, metadata !91, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!349 = metadata !{i32 786478, i32 0, metadata !52, metadata !"deleteBV", metadata !"deleteBV", metadata !"_Z8deleteBVj", metadata !52, i32 43, metadata !350, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32)* @projection_gp_deleteBV, null, null, metadata !58, i32 43} ; [ DW_TAG_subprogram ]
!350 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !351, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!351 = metadata !{null, metadata !91}
!352 = metadata !{i32 43, i32 29, metadata !349, null}
!353 = metadata !{i32 47, i32 31, metadata !354, null}
!354 = metadata !{i32 786443, metadata !349, i32 43, i32 37, metadata !52, i32 8} ; [ DW_TAG_lexical_block ]
!355 = metadata !{i32 786688, metadata !354, metadata !"temp", metadata !52, i32 47, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!356 = metadata !{i32 48, i32 5, metadata !354, null}
!357 = metadata !{i32 49, i32 5, metadata !354, null}
!358 = metadata !{i32 51, i32 5, metadata !354, null}
!359 = metadata !{i32 52, i32 5, metadata !354, null}
!360 = metadata !{i32 56, i32 2, metadata !361, null}
!361 = metadata !{i32 786443, metadata !362, i32 54, i32 63, metadata !52, i32 10} ; [ DW_TAG_lexical_block ]
!362 = metadata !{i32 786443, metadata !354, i32 54, i32 25, metadata !52, i32 9} ; [ DW_TAG_lexical_block ]
!363 = metadata !{i32 54, i32 48, metadata !362, null}
!364 = metadata !{i32 54, i32 58, metadata !362, null}
!365 = metadata !{i32 54, i32 64, metadata !361, null}
!366 = metadata !{i32 55, i32 1, metadata !361, null}
!367 = metadata !{i32 57, i32 9, metadata !361, null}
!368 = metadata !{i32 58, i32 9, metadata !361, null}
!369 = metadata !{i32 59, i32 5, metadata !361, null}
!370 = metadata !{i32 786688, metadata !362, metadata !"i", metadata !52, i32 54, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!371 = metadata !{i32 62, i32 32, metadata !354, null}
!372 = metadata !{i32 786688, metadata !354, metadata !"alphaStar", metadata !52, i32 62, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!373 = metadata !{i32 63, i32 24, metadata !354, null}
!374 = metadata !{i32 786688, metadata !354, metadata !"cStar", metadata !52, i32 63, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!375 = metadata !{i32 81, i32 45, metadata !376, null}
!376 = metadata !{i32 786443, metadata !377, i32 76, i32 62, metadata !52, i32 16} ; [ DW_TAG_lexical_block ]
!377 = metadata !{i32 786443, metadata !378, i32 76, i32 24, metadata !52, i32 15} ; [ DW_TAG_lexical_block ]
!378 = metadata !{i32 786443, metadata !379, i32 74, i32 60, metadata !52, i32 14} ; [ DW_TAG_lexical_block ]
!379 = metadata !{i32 786443, metadata !354, i32 74, i32 23, metadata !52, i32 13} ; [ DW_TAG_lexical_block ]
!380 = metadata !{i32 786688, metadata !354, metadata !"qStar", metadata !52, i32 64, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!381 = metadata !{i32 64, i32 24, metadata !354, null}
!382 = metadata !{i32 65, i32 5, metadata !354, null}
!383 = metadata !{i32 67, i32 44, metadata !384, null}
!384 = metadata !{i32 786443, metadata !354, i32 67, i32 21, metadata !52, i32 11} ; [ DW_TAG_lexical_block ]
!385 = metadata !{i32 67, i32 54, metadata !384, null}
!386 = metadata !{i32 67, i32 60, metadata !387, null}
!387 = metadata !{i32 786443, metadata !384, i32 67, i32 59, metadata !52, i32 12} ; [ DW_TAG_lexical_block ]
!388 = metadata !{i32 69, i32 35, metadata !387, null}
!389 = metadata !{i32 786688, metadata !387, metadata !"a", metadata !52, i32 69, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!390 = metadata !{i32 70, i32 9, metadata !387, null}
!391 = metadata !{i32 786688, metadata !384, metadata !"i", metadata !52, i32 67, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!392 = metadata !{i32 72, i32 5, metadata !354, null}
!393 = metadata !{i32 74, i32 45, metadata !379, null}
!394 = metadata !{i32 74, i32 55, metadata !379, null}
!395 = metadata !{i32 74, i32 61, metadata !378, null}
!396 = metadata !{i32 78, i32 39, metadata !376, null}
!397 = metadata !{i32 84, i32 13, metadata !376, null}
!398 = metadata !{i32 76, i32 47, metadata !377, null}
!399 = metadata !{i32 76, i32 57, metadata !377, null}
!400 = metadata !{i32 76, i32 63, metadata !376, null}
!401 = metadata !{i32 786688, metadata !376, metadata !"a", metadata !52, i32 78, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!402 = metadata !{i32 79, i32 39, metadata !376, null}
!403 = metadata !{i32 786688, metadata !376, metadata !"b", metadata !52, i32 79, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!404 = metadata !{i32 80, i32 38, metadata !376, null}
!405 = metadata !{i32 786688, metadata !376, metadata !"c", metadata !52, i32 80, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!406 = metadata !{i32 786688, metadata !376, metadata !"temp", metadata !52, i32 81, metadata !57, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!407 = metadata !{i32 90, i32 13, metadata !376, null}
!408 = metadata !{i32 786688, metadata !377, metadata !"j", metadata !52, i32 76, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!409 = metadata !{i32 92, i32 5, metadata !378, null}
!410 = metadata !{i32 786688, metadata !379, metadata !"i", metadata !52, i32 74, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!411 = metadata !{i32 94, i32 48, metadata !412, null}
!412 = metadata !{i32 786443, metadata !354, i32 94, i32 25, metadata !52, i32 17} ; [ DW_TAG_lexical_block ]
!413 = metadata !{i32 94, i32 59, metadata !412, null}
!414 = metadata !{i32 94, i32 65, metadata !415, null}
!415 = metadata !{i32 786443, metadata !412, i32 94, i32 64, metadata !52, i32 18} ; [ DW_TAG_lexical_block ]
!416 = metadata !{i32 96, i32 35, metadata !415, null}
!417 = metadata !{i32 786688, metadata !415, metadata !"a", metadata !52, i32 96, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!418 = metadata !{i32 97, i32 35, metadata !415, null}
!419 = metadata !{i32 786688, metadata !415, metadata !"b", metadata !52, i32 97, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!420 = metadata !{i32 99, i32 6, metadata !415, null}
!421 = metadata !{i32 100, i32 9, metadata !415, null}
!422 = metadata !{i32 101, i32 9, metadata !415, null}
!423 = metadata !{i32 102, i32 9, metadata !415, null}
!424 = metadata !{i32 786688, metadata !412, metadata !"i", metadata !52, i32 94, metadata !91, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!425 = metadata !{i32 104, i32 1, metadata !354, null}
