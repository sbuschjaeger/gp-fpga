; ModuleID = '/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace/ProjectionGP/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@s = global [41 x float] zeroinitializer, align 16 ; [#uses=6 type=[41 x float]*]
@projection_gp.str = internal unnamed_addr constant [14 x i8] c"projection_gp\00" ; [#uses=1 type=[14 x i8]*]
@k = global [40 x float] zeroinitializer, align 16 ; [#uses=4 type=[40 x float]*]
@e = global [41 x float] zeroinitializer, align 16 ; [#uses=4 type=[41 x float]*]
@bvCnt = global i32 0, align 4                    ; [#uses=4 type=i32*]
@basisVectors = global [533 x float] zeroinitializer, align 16 ; [#uses=6 type=[533 x float]*]
@alpha = global [41 x float] zeroinitializer, align 16 ; [#uses=9 type=[41 x float]*]
@Q = global [1681 x float] zeroinitializer, align 16 ; [#uses=13 type=[1681 x float]*]
@C = global [1681 x float] zeroinitializer, align 16 ; [#uses=13 type=[1681 x float]*]
@.str9 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_INNER\00", align 1 ; [#uses=1 type=[17 x i8]*]
@.str8 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_OUTER\00", align 1 ; [#uses=3 type=[17 x i8]*]
@.str7 = private unnamed_addr constant [7 x i8] c"CALC_K\00", align 1 ; [#uses=3 type=[7 x i8]*]
@.str6 = private unnamed_addr constant [20 x i8] c"DELETE_BV_UNSET_C_Q\00", align 1 ; [#uses=1 type=[20 x i8]*]
@.str5 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_INNER\00", align 1 ; [#uses=1 type=[18 x i8]*]
@.str4 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_OUTER\00", align 1 ; [#uses=3 type=[18 x i8]*]
@.str3 = private unnamed_addr constant [16 x i8] c"DELETE_BV_ALPHA\00", align 1 ; [#uses=1 type=[16 x i8]*]
@.str20 = private unnamed_addr constant [11 x i8] c"PREDICTION\00", align 1 ; [#uses=3 type=[11 x i8]*]
@.str2 = private unnamed_addr constant [20 x i8] c"DELETE_BV_SWAP_LOOP\00", align 1 ; [#uses=3 type=[20 x i8]*]
@.str19 = private unnamed_addr constant [11 x i8] c"INIT_INNER\00", align 1 ; [#uses=1 type=[11 x i8]*]
@.str18 = private unnamed_addr constant [11 x i8] c"INIT_OUTER\00", align 1 ; [#uses=3 type=[11 x i8]*]
@.str17 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1 ; [#uses=5 type=[10 x i8]*]
@.str16 = private unnamed_addr constant [8 x i8] c"COPY_BV\00", align 1 ; [#uses=2 type=[8 x i8]*]
@.str15 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_INNER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str14 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_OUTER\00", align 1 ; [#uses=3 type=[15 x i8]*]
@.str13 = private unnamed_addr constant [15 x i8] c"UPDATE_C_INNER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str12 = private unnamed_addr constant [15 x i8] c"UPDATE_C_OUTER\00", align 1 ; [#uses=3 type=[15 x i8]*]
@.str11 = private unnamed_addr constant [13 x i8] c"UPDATE_ALPHA\00", align 1 ; [#uses=1 type=[13 x i8]*]
@.str10 = private unnamed_addr constant [7 x i8] c"CALC_S\00", align 1 ; [#uses=1 type=[7 x i8]*]
@.str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=23 type=[1 x i8]*]
@.str = private unnamed_addr constant [12 x i8] c"hls_label_0\00", align 1 ; [#uses=4 type=[12 x i8]*]

; [#uses=1]
define internal fastcc void @train_full_bv_set([13 x float]* nocapture %pX, float %pY) {
  call void @llvm.dbg.value(metadata !{[13 x float]* %pX}, i64 0, metadata !79), !dbg !90 ; [debug line = 122:37] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !91), !dbg !92 ; [debug line = 122:57] [debug variable = pY]
  br label %1, !dbg !93                           ; [debug line = 126:35]

; <label>:1                                       ; preds = %K.exit, %0
  %i = phi i6 [ 0, %0 ], [ %i.1, %K.exit ]        ; [#uses=4 type=i6]
  %m = phi float [ 0x4050B830E0000000, %0 ], [ %m.1, %K.exit ] ; [#uses=2 type=float]
  %i.cast = zext i6 %i to i10, !dbg !96           ; [#uses=1 type=i10] [debug line = 16:42@128:16]
  %exitcond8 = icmp eq i6 %i, -24, !dbg !93       ; [#uses=1 type=i1] [debug line = 126:35]
  %2 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  br i1 %exitcond8, label %.preheader10.preheader, label %3, !dbg !93 ; [debug line = 126:35]

.preheader10.preheader:                           ; preds = %1
  %m.lcssa = phi float [ %m, %1 ]                 ; [#uses=1 type=float]
  br label %.preheader10, !dbg !102               ; [debug line = 143:10]

; <label>:3                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @.str7) nounwind, !dbg !107 ; [debug line = 126:51]
  %tmp.2 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @.str7), !dbg !107 ; [#uses=1 type=i32] [debug line = 126:51]
  %tmp = mul i10 %i.cast, 13, !dbg !96            ; [#uses=1 type=i10] [debug line = 16:42@128:16]
  call void @llvm.dbg.value(metadata !{[13 x float]* %pX}, i64 0, metadata !108), !dbg !96 ; [debug line = 16:42@128:16] [debug variable = pX2]
  br label %4, !dbg !109                          ; [debug line = 18:28@128:16]

; <label>:4                                       ; preds = %6, %3
  %sum.i = phi float [ 0.000000e+00, %3 ], [ %sum, %6 ] ; [#uses=2 type=float]
  %i.i = phi i4 [ 0, %3 ], [ %i.7, %6 ]           ; [#uses=4 type=i4]
  %exitcond.i = icmp eq i4 %i.i, -3, !dbg !109    ; [#uses=1 type=i1] [debug line = 18:28@128:16]
  %5 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) ; [#uses=0 type=i32]
  br i1 %exitcond.i, label %K.exit, label %6, !dbg !109 ; [debug line = 18:28@128:16]

; <label>:6                                       ; preds = %4
  %tmp.106.i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @.str), !dbg !112 ; [#uses=1 type=i32] [debug line = 18:44@128:16]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @.str1) nounwind, !dbg !114 ; [debug line = 19:1@128:16]
  %tmp.104.i = zext i4 %i.i to i64, !dbg !115     ; [#uses=1 type=i64] [debug line = 20:31@128:16]
  %tmp.104.i.cast = zext i4 %i.i to i10           ; [#uses=1 type=i10]
  %sum1.i = add i10 %tmp, %tmp.104.i.cast         ; [#uses=1 type=i10]
  %sum1.i.cast = zext i10 %sum1.i to i64          ; [#uses=1 type=i64]
  %basisVectors.addr = getelementptr [533 x float]* @basisVectors, i64 0, i64 %sum1.i.cast, !dbg !115 ; [#uses=1 type=float*] [debug line = 20:31@128:16]
  %basisVectors.load = load float* %basisVectors.addr, align 4, !dbg !115 ; [#uses=1 type=float] [debug line = 20:31@128:16]
  %pX.addr = getelementptr [13 x float]* %pX, i64 0, i64 %tmp.104.i, !dbg !115 ; [#uses=1 type=float*] [debug line = 20:31@128:16]
  %pX.load = load float* %pX.addr, align 4, !dbg !115 ; [#uses=1 type=float] [debug line = 20:31@128:16]
  %val = fsub float %basisVectors.load, %pX.load, !dbg !115 ; [#uses=2 type=float] [debug line = 20:31@128:16]
  call void @llvm.dbg.value(metadata !{float %val}, i64 0, metadata !116), !dbg !115 ; [debug line = 20:31@128:16] [debug variable = val]
  %tmp.105.i = fmul float %val, %val, !dbg !117   ; [#uses=1 type=float] [debug line = 21:9@128:16]
  %sum = fadd float %sum.i, %tmp.105.i, !dbg !117 ; [#uses=1 type=float] [debug line = 21:9@128:16]
  call void @llvm.dbg.value(metadata !{float %sum}, i64 0, metadata !118), !dbg !117 ; [debug line = 21:9@128:16] [debug variable = sum]
  %7 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @.str, i32 %tmp.106.i), !dbg !119 ; [#uses=0 type=i32] [debug line = 22:5@128:16]
  %i.7 = add i4 %i.i, 1, !dbg !120                ; [#uses=1 type=i4] [debug line = 18:38@128:16]
  call void @llvm.dbg.value(metadata !{i4 %i.7}, i64 0, metadata !121), !dbg !120 ; [debug line = 18:38@128:16] [debug variable = i]
  br label %4, !dbg !120                          ; [debug line = 18:38@128:16]

K.exit:                                           ; preds = %4
  %sum.i.lcssa = phi float [ %sum.i, %4 ]         ; [#uses=1 type=float]
  %__x.assign = fmul float %sum.i.lcssa, -5.000000e-01, !dbg !122 ; [#uses=1 type=float] [debug line = 24:12@128:16]
  call void @llvm.dbg.value(metadata !{float %__x.assign}, i64 0, metadata !123) nounwind, !dbg !129 ; [debug line = 216:13@24:12@128:16] [debug variable = __x]
  %tmp.i.i = call float @llvm.exp.f32(float %__x.assign) nounwind, !dbg !130 ; [#uses=2 type=float] [debug line = 217:12@24:12@128:16]
  %tmp.3 = zext i6 %i to i64, !dbg !100           ; [#uses=2 type=i64] [debug line = 128:16]
  %k.addr = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp.3, !dbg !100 ; [#uses=1 type=float*] [debug line = 128:16]
  store float %tmp.i.i, float* %k.addr, align 4, !dbg !100 ; [debug line = 128:16]
  %alpha.addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.3, !dbg !132 ; [#uses=1 type=float*] [debug line = 129:9]
  %alpha.load = load float* %alpha.addr, align 4, !dbg !132 ; [#uses=1 type=float] [debug line = 129:9]
  %tmp.4 = fmul float %tmp.i.i, %alpha.load, !dbg !132 ; [#uses=1 type=float] [debug line = 129:9]
  %m.1 = fadd float %m, %tmp.4, !dbg !132         ; [#uses=1 type=float] [debug line = 129:9]
  call void @llvm.dbg.value(metadata !{float %m.1}, i64 0, metadata !133), !dbg !132 ; [debug line = 129:9] [debug variable = m]
  %8 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @.str7, i32 %tmp.2), !dbg !134 ; [#uses=0 type=i32] [debug line = 130:5]
  %i.1 = add i6 %i, 1, !dbg !135                  ; [#uses=1 type=i6] [debug line = 126:45]
  call void @llvm.dbg.value(metadata !{i6 %i.1}, i64 0, metadata !136), !dbg !135 ; [debug line = 126:45] [debug variable = i]
  br label %1, !dbg !135                          ; [debug line = 126:45]

.preheader10:                                     ; preds = %14, %.preheader10.preheader
  %i1 = phi i6 [ %i.3, %14 ], [ 0, %.preheader10.preheader ] ; [#uses=4 type=i6]
  %i1.cast = zext i6 %i1 to i11, !dbg !102        ; [#uses=1 type=i11] [debug line = 143:10]
  %exitcond7 = icmp eq i6 %i1, -24, !dbg !137     ; [#uses=1 type=i1] [debug line = 137:45]
  %9 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  br i1 %exitcond7, label %.preheader9, label %10, !dbg !137 ; [debug line = 137:45]

; <label>:10                                      ; preds = %.preheader10
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @.str8) nounwind, !dbg !138 ; [debug line = 137:61]
  %tmp.1 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @.str8), !dbg !138 ; [#uses=1 type=i32] [debug line = 137:61]
  %tmp.7 = zext i6 %i1 to i64, !dbg !139          ; [#uses=2 type=i64] [debug line = 139:6]
  %s.addr = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.7, !dbg !139 ; [#uses=2 type=float*] [debug line = 139:6]
  store float 0.000000e+00, float* %s.addr, align 4, !dbg !139 ; [debug line = 139:6]
  %e.addr = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp.7, !dbg !140 ; [#uses=2 type=float*] [debug line = 140:6]
  store float 0.000000e+00, float* %e.addr, align 4, !dbg !140 ; [debug line = 140:6]
  %tmp.8 = mul i11 %i1.cast, 41, !dbg !102        ; [#uses=1 type=i11] [debug line = 143:10]
  br label %11, !dbg !141                         ; [debug line = 141:49]

; <label>:11                                      ; preds = %13, %10
  %tmp.9 = phi float [ 0.000000e+00, %10 ], [ %tmp.22, %13 ] ; [#uses=1 type=float]
  %tmp. = phi float [ 0.000000e+00, %10 ], [ %tmp.20, %13 ] ; [#uses=1 type=float]
  %j = phi i6 [ 0, %10 ], [ %j.1, %13 ]           ; [#uses=4 type=i6]
  %j.cast = zext i6 %j to i11, !dbg !141          ; [#uses=1 type=i11] [debug line = 141:49]
  %exitcond6 = icmp eq i6 %j, -24, !dbg !141      ; [#uses=1 type=i1] [debug line = 141:49]
  %12 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  br i1 %exitcond6, label %14, label %13, !dbg !141 ; [debug line = 141:49]

; <label>:13                                      ; preds = %11
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @.str9) nounwind, !dbg !142 ; [debug line = 141:65]
  %tmp.16 = add i11 %tmp.8, %j.cast, !dbg !102    ; [#uses=1 type=i11] [debug line = 143:10]
  %tmp.17 = zext i11 %tmp.16 to i64, !dbg !102    ; [#uses=2 type=i64] [debug line = 143:10]
  %C.addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.17, !dbg !102 ; [#uses=1 type=float*] [debug line = 143:10]
  %C.load = load float* %C.addr, align 4, !dbg !102 ; [#uses=1 type=float] [debug line = 143:10]
  %tmp.18 = zext i6 %j to i64, !dbg !102          ; [#uses=1 type=i64] [debug line = 143:10]
  %k.addr.2 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp.18, !dbg !102 ; [#uses=1 type=float*] [debug line = 143:10]
  %k.load.1 = load float* %k.addr.2, align 4, !dbg !102 ; [#uses=2 type=float] [debug line = 143:10]
  %tmp.19 = fmul float %C.load, %k.load.1, !dbg !102 ; [#uses=1 type=float] [debug line = 143:10]
  %tmp.20 = fadd float %tmp., %tmp.19, !dbg !102  ; [#uses=2 type=float] [debug line = 143:10]
  store float %tmp.20, float* %s.addr, align 4, !dbg !102 ; [debug line = 143:10]
  %Q.addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.17, !dbg !143 ; [#uses=1 type=float*] [debug line = 144:10]
  %Q.load = load float* %Q.addr, align 4, !dbg !143 ; [#uses=1 type=float] [debug line = 144:10]
  %tmp.21 = fmul float %Q.load, %k.load.1, !dbg !143 ; [#uses=1 type=float] [debug line = 144:10]
  %tmp.22 = fadd float %tmp.9, %tmp.21, !dbg !143 ; [#uses=2 type=float] [debug line = 144:10]
  store float %tmp.22, float* %e.addr, align 4, !dbg !143 ; [debug line = 144:10]
  %j.1 = add i6 %j, 1, !dbg !144                  ; [#uses=1 type=i6] [debug line = 141:59]
  call void @llvm.dbg.value(metadata !{i6 %j.1}, i64 0, metadata !145), !dbg !144 ; [debug line = 141:59] [debug variable = j]
  br label %11, !dbg !144                         ; [debug line = 141:59]

; <label>:14                                      ; preds = %11
  %15 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @.str8, i32 %tmp.1), !dbg !146 ; [#uses=0 type=i32] [debug line = 146:5]
  %i.3 = add i6 %i1, 1, !dbg !147                 ; [#uses=1 type=i6] [debug line = 137:55]
  call void @llvm.dbg.value(metadata !{i6 %i.3}, i64 0, metadata !148), !dbg !147 ; [debug line = 137:55] [debug variable = i]
  br label %.preheader10, !dbg !147               ; [debug line = 137:55]

.preheader9:                                      ; preds = %17, %.preheader10
  %i2 = phi i6 [ %i.2, %17 ], [ 0, %.preheader10 ] ; [#uses=3 type=i6]
  %sigma2 = phi float [ %sigma2.1, %17 ], [ 1.000000e+00, %.preheader10 ] ; [#uses=2 type=float]
  %exitcond5 = icmp eq i6 %i2, -24, !dbg !149     ; [#uses=1 type=i1] [debug line = 148:36]
  %16 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  br i1 %exitcond5, label %18, label %17, !dbg !149 ; [debug line = 148:36]

; <label>:17                                      ; preds = %.preheader9
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @.str10) nounwind, !dbg !151 ; [debug line = 148:52]
  %tmp.14 = zext i6 %i2 to i64, !dbg !153         ; [#uses=2 type=i64] [debug line = 150:6]
  %s.addr.1 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.14, !dbg !153 ; [#uses=1 type=float*] [debug line = 150:6]
  %s.load = load float* %s.addr.1, align 4, !dbg !153 ; [#uses=1 type=float] [debug line = 150:6]
  %k.addr.1 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp.14, !dbg !153 ; [#uses=1 type=float*] [debug line = 150:6]
  %k.load = load float* %k.addr.1, align 4, !dbg !153 ; [#uses=1 type=float] [debug line = 150:6]
  %tmp.15 = fmul float %s.load, %k.load, !dbg !153 ; [#uses=1 type=float] [debug line = 150:6]
  %sigma2.1 = fadd float %sigma2, %tmp.15, !dbg !153 ; [#uses=1 type=float] [debug line = 150:6]
  call void @llvm.dbg.value(metadata !{float %sigma2.1}, i64 0, metadata !154), !dbg !153 ; [debug line = 150:6] [debug variable = sigma2]
  %i.2 = add i6 %i2, 1, !dbg !155                 ; [#uses=1 type=i6] [debug line = 148:46]
  call void @llvm.dbg.value(metadata !{i6 %i.2}, i64 0, metadata !156), !dbg !155 ; [debug line = 148:46] [debug variable = i]
  br label %.preheader9, !dbg !155                ; [debug line = 148:46]

; <label>:18                                      ; preds = %.preheader9
  %sigma2.lcssa = phi float [ %sigma2, %.preheader9 ] ; [#uses=1 type=float]
  store float 1.000000e+00, float* getelementptr inbounds ([41 x float]* @s, i64 0, i64 40), align 16, !dbg !157 ; [debug line = 152:5]
  %tmp.5 = fsub float %pY, %m.lcssa, !dbg !158    ; [#uses=1 type=float] [debug line = 154:40]
  %tmp.6 = fpext float %tmp.5 to double, !dbg !158 ; [#uses=1 type=double] [debug line = 154:40]
  %tmp.10 = fpext float %sigma2.lcssa to double, !dbg !158 ; [#uses=1 type=double] [debug line = 154:40]
  %tmp.11 = fadd double %tmp.10, 1.000000e+00, !dbg !158 ; [#uses=2 type=double] [debug line = 154:40]
  %tmp.12 = fdiv double %tmp.6, %tmp.11, !dbg !158 ; [#uses=1 type=double] [debug line = 154:40]
  %q = fptrunc double %tmp.12 to float, !dbg !158 ; [#uses=2 type=float] [debug line = 154:40]
  call void @llvm.dbg.value(metadata !{float %q}, i64 0, metadata !159), !dbg !158 ; [debug line = 154:40] [debug variable = q]
  %tmp.13 = fdiv double -1.000000e+00, %tmp.11, !dbg !160 ; [#uses=1 type=double] [debug line = 155:35]
  %r = fptrunc double %tmp.13 to float, !dbg !160 ; [#uses=1 type=float] [debug line = 155:35]
  call void @llvm.dbg.value(metadata !{float %r}, i64 0, metadata !161), !dbg !160 ; [debug line = 155:35] [debug variable = r]
  br label %19, !dbg !162                         ; [debug line = 158:42]

; <label>:19                                      ; preds = %21, %18
  %gamma = phi float [ 1.000000e+00, %18 ], [ %gamma.1, %21 ] ; [#uses=2 type=float]
  %i3 = phi i6 [ 0, %18 ], [ %i.4, %21 ]          ; [#uses=3 type=i6]
  %exitcond4 = icmp eq i6 %i3, -24, !dbg !162     ; [#uses=1 type=i1] [debug line = 158:42]
  %20 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) ; [#uses=0 type=i32]
  br i1 %exitcond4, label %22, label %21, !dbg !162 ; [debug line = 158:42]

; <label>:21                                      ; preds = %19
  call void (...)* @_ssdm_op_SpecLoopName([13 x i8]* @.str11) nounwind, !dbg !164 ; [debug line = 158:58]
  %tmp.24 = zext i6 %i3 to i64, !dbg !166         ; [#uses=4 type=i64] [debug line = 160:6]
  %e.addr.1 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp.24, !dbg !166 ; [#uses=1 type=float*] [debug line = 160:6]
  %e.load = load float* %e.addr.1, align 4, !dbg !166 ; [#uses=1 type=float] [debug line = 160:6]
  %k.addr.3 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp.24, !dbg !166 ; [#uses=1 type=float*] [debug line = 160:6]
  %k.load.2 = load float* %k.addr.3, align 4, !dbg !166 ; [#uses=1 type=float] [debug line = 160:6]
  %tmp.25 = fmul float %e.load, %k.load.2, !dbg !166 ; [#uses=1 type=float] [debug line = 160:6]
  %gamma.1 = fsub float %gamma, %tmp.25, !dbg !166 ; [#uses=1 type=float] [debug line = 160:6]
  call void @llvm.dbg.value(metadata !{float %gamma.1}, i64 0, metadata !167), !dbg !166 ; [debug line = 160:6] [debug variable = gamma]
  %s.addr.2 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.24, !dbg !168 ; [#uses=1 type=float*] [debug line = 161:6]
  %s.load.1 = load float* %s.addr.2, align 4, !dbg !168 ; [#uses=1 type=float] [debug line = 161:6]
  %tmp.26 = fmul float %s.load.1, %q, !dbg !168   ; [#uses=1 type=float] [debug line = 161:6]
  %alpha.addr.1 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.24, !dbg !168 ; [#uses=2 type=float*] [debug line = 161:6]
  %alpha.load.2 = load float* %alpha.addr.1, align 4, !dbg !168 ; [#uses=1 type=float] [debug line = 161:6]
  %tmp.27 = fadd float %alpha.load.2, %tmp.26, !dbg !168 ; [#uses=1 type=float] [debug line = 161:6]
  store float %tmp.27, float* %alpha.addr.1, align 4, !dbg !168 ; [debug line = 161:6]
  %i.4 = add i6 %i3, 1, !dbg !169                 ; [#uses=1 type=i6] [debug line = 158:52]
  call void @llvm.dbg.value(metadata !{i6 %i.4}, i64 0, metadata !170), !dbg !169 ; [debug line = 158:52] [debug variable = i]
  br label %19, !dbg !169                         ; [debug line = 158:52]

; <label>:22                                      ; preds = %19
  %gamma.lcssa = phi float [ %gamma, %19 ]        ; [#uses=1 type=float]
  %alpha.load.1 = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !171 ; [#uses=1 type=float] [debug line = 168:5]
  %tmp.23 = fadd float %alpha.load.1, %q, !dbg !171 ; [#uses=1 type=float] [debug line = 168:5]
  store float %tmp.23, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !171 ; [debug line = 168:5]
  br label %23, !dbg !172                         ; [debug line = 171:43]

; <label>:23                                      ; preds = %29, %22
  %i4 = phi i6 [ 0, %22 ], [ %i.5, %29 ]          ; [#uses=4 type=i6]
  %i4.cast = zext i6 %i4 to i11, !dbg !174        ; [#uses=1 type=i11] [debug line = 176:13]
  %exitcond3 = icmp eq i6 %i4, -23, !dbg !172     ; [#uses=1 type=i1] [debug line = 171:43]
  %24 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  br i1 %exitcond3, label %.preheader.preheader, label %25, !dbg !172 ; [debug line = 171:43]

.preheader.preheader:                             ; preds = %23
  %tmp.28 = fpext float %gamma.lcssa to double, !dbg !178 ; [#uses=1 type=double] [debug line = 187:4]
  br label %.preheader, !dbg !183                 ; [debug line = 180:43]

; <label>:25                                      ; preds = %23
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @.str12) nounwind, !dbg !184 ; [debug line = 171:60]
  %tmp.50 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @.str12), !dbg !184 ; [#uses=1 type=i32] [debug line = 171:60]
  %tmp.29 = zext i6 %i4 to i64, !dbg !174         ; [#uses=1 type=i64] [debug line = 176:13]
  %s.addr.3 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.29, !dbg !174 ; [#uses=1 type=float*] [debug line = 176:13]
  %tmp.30 = mul i11 %i4.cast, 41, !dbg !174       ; [#uses=1 type=i11] [debug line = 176:13]
  %s.load.2 = load float* %s.addr.3, align 4, !dbg !174 ; [#uses=1 type=float] [debug line = 176:13]
  br label %26, !dbg !185                         ; [debug line = 173:44]

; <label>:26                                      ; preds = %28, %25
  %j5 = phi i6 [ 0, %25 ], [ %j.2, %28 ]          ; [#uses=4 type=i6]
  %j5.cast8 = zext i6 %j5 to i11, !dbg !185       ; [#uses=1 type=i11] [debug line = 173:44]
  %exitcond2 = icmp eq i6 %j5, -23, !dbg !185     ; [#uses=1 type=i1] [debug line = 173:44]
  %27 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  br i1 %exitcond2, label %29, label %28, !dbg !185 ; [debug line = 173:44]

; <label>:28                                      ; preds = %26
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @.str13) nounwind, !dbg !186 ; [debug line = 173:61]
  %tmp.34 = zext i6 %j5 to i64, !dbg !174         ; [#uses=1 type=i64] [debug line = 176:13]
  %s.addr.4 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp.34, !dbg !174 ; [#uses=1 type=float*] [debug line = 176:13]
  %s.load.3 = load float* %s.addr.4, align 4, !dbg !174 ; [#uses=1 type=float] [debug line = 176:13]
  %tmp.35 = fmul float %s.load.2, %s.load.3, !dbg !174 ; [#uses=1 type=float] [debug line = 176:13]
  %tmp.36 = fmul float %tmp.35, %r, !dbg !174     ; [#uses=1 type=float] [debug line = 176:13]
  %tmp.37 = add i11 %tmp.30, %j5.cast8, !dbg !174 ; [#uses=1 type=i11] [debug line = 176:13]
  %tmp.38 = zext i11 %tmp.37 to i64, !dbg !174    ; [#uses=1 type=i64] [debug line = 176:13]
  %C.addr.1 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.38, !dbg !174 ; [#uses=2 type=float*] [debug line = 176:13]
  %C.load.1 = load float* %C.addr.1, align 4, !dbg !174 ; [#uses=1 type=float] [debug line = 176:13]
  %tmp.39 = fadd float %C.load.1, %tmp.36, !dbg !174 ; [#uses=1 type=float] [debug line = 176:13]
  store float %tmp.39, float* %C.addr.1, align 4, !dbg !174 ; [debug line = 176:13]
  %j.2 = add i6 %j5, 1, !dbg !187                 ; [#uses=1 type=i6] [debug line = 173:55]
  call void @llvm.dbg.value(metadata !{i6 %j.2}, i64 0, metadata !188), !dbg !187 ; [debug line = 173:55] [debug variable = j]
  br label %26, !dbg !187                         ; [debug line = 173:55]

; <label>:29                                      ; preds = %26
  %30 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @.str12, i32 %tmp.50), !dbg !189 ; [#uses=0 type=i32] [debug line = 178:5]
  %i.5 = add i6 %i4, 1, !dbg !190                 ; [#uses=1 type=i6] [debug line = 171:54]
  call void @llvm.dbg.value(metadata !{i6 %i.5}, i64 0, metadata !191), !dbg !190 ; [debug line = 171:54] [debug variable = i]
  br label %23, !dbg !190                         ; [debug line = 171:54]

.preheader:                                       ; preds = %35, %.preheader.preheader
  %i6 = phi i6 [ %i.6, %35 ], [ 0, %.preheader.preheader ] ; [#uses=5 type=i6]
  %i6.cast6 = zext i6 %i6 to i11, !dbg !178       ; [#uses=1 type=i11] [debug line = 187:4]
  %exitcond1 = icmp eq i6 %i6, -23, !dbg !183     ; [#uses=1 type=i1] [debug line = 180:43]
  %31 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  br i1 %exitcond1, label %.preheader11, label %32, !dbg !183 ; [debug line = 180:43]

; <label>:32                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @.str14) nounwind, !dbg !192 ; [debug line = 180:60]
  %tmp.51 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @.str14), !dbg !192 ; [#uses=1 type=i32] [debug line = 180:60]
  %tmp.31 = icmp eq i6 %i6, -24, !dbg !193        ; [#uses=1 type=i1] [debug line = 184:37]
  %tmp.32 = mul i11 %i6.cast6, 41, !dbg !178      ; [#uses=1 type=i11] [debug line = 187:4]
  %tmp.33 = zext i6 %i6 to i64, !dbg !193         ; [#uses=1 type=i64] [debug line = 184:37]
  %e.addr.2 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp.33, !dbg !193 ; [#uses=1 type=float*] [debug line = 184:37]
  %e.load.1 = load float* %e.addr.2, align 4, !dbg !193 ; [#uses=1 type=float] [debug line = 184:37]
  br label %33, !dbg !194                         ; [debug line = 182:44]

; <label>:33                                      ; preds = %._crit_edge_ifconv, %32
  %j7 = phi i6 [ 0, %32 ], [ %j.3, %._crit_edge_ifconv ] ; [#uses=5 type=i6]
  %j7.cast4 = zext i6 %j7 to i11, !dbg !194       ; [#uses=1 type=i11] [debug line = 182:44]
  %exitcond = icmp eq i6 %j7, -23, !dbg !194      ; [#uses=1 type=i1] [debug line = 182:44]
  %34 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  br i1 %exitcond, label %35, label %._crit_edge_ifconv, !dbg !194 ; [debug line = 182:44]

._crit_edge_ifconv:                               ; preds = %33
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @.str15) nounwind, !dbg !195 ; [debug line = 182:61]
  %ti = select i1 %tmp.31, float -1.000000e+00, float %e.load.1, !dbg !193 ; [#uses=1 type=float] [debug line = 184:37]
  call void @llvm.dbg.value(metadata !{float %ti}, i64 0, metadata !196), !dbg !193 ; [debug line = 184:37] [debug variable = ti]
  %tmp.40 = icmp eq i6 %j7, -24, !dbg !197        ; [#uses=1 type=i1] [debug line = 185:34]
  %tmp.41 = zext i6 %j7 to i64, !dbg !197         ; [#uses=1 type=i64] [debug line = 185:34]
  %e.addr.3 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp.41, !dbg !197 ; [#uses=1 type=float*] [debug line = 185:34]
  %e.load.2 = load float* %e.addr.3, align 4, !dbg !197 ; [#uses=1 type=float] [debug line = 185:34]
  %tj = select i1 %tmp.40, float -1.000000e+00, float %e.load.2, !dbg !197 ; [#uses=1 type=float] [debug line = 185:34]
  call void @llvm.dbg.value(metadata !{float %tj}, i64 0, metadata !198), !dbg !197 ; [debug line = 185:34] [debug variable = tj]
  %tmp.42 = fmul float %ti, %tj, !dbg !178        ; [#uses=1 type=float] [debug line = 187:4]
  %tmp.43 = fpext float %tmp.42 to double, !dbg !178 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp.44 = fdiv double %tmp.43, %tmp.28, !dbg !178 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp.45 = add i11 %tmp.32, %j7.cast4, !dbg !178 ; [#uses=1 type=i11] [debug line = 187:4]
  %tmp.46 = zext i11 %tmp.45 to i64, !dbg !178    ; [#uses=1 type=i64] [debug line = 187:4]
  %Q.addr.1 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.46, !dbg !178 ; [#uses=2 type=float*] [debug line = 187:4]
  %Q.load.1 = load float* %Q.addr.1, align 4, !dbg !178 ; [#uses=1 type=float] [debug line = 187:4]
  %tmp.47 = fpext float %Q.load.1 to double, !dbg !178 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp.48 = fadd double %tmp.47, %tmp.44, !dbg !178 ; [#uses=1 type=double] [debug line = 187:4]
  %tmp.49 = fptrunc double %tmp.48 to float, !dbg !178 ; [#uses=1 type=float] [debug line = 187:4]
  store float %tmp.49, float* %Q.addr.1, align 4, !dbg !178 ; [debug line = 187:4]
  %j.3 = add i6 %j7, 1, !dbg !199                 ; [#uses=1 type=i6] [debug line = 182:55]
  call void @llvm.dbg.value(metadata !{i6 %j.3}, i64 0, metadata !200), !dbg !199 ; [debug line = 182:55] [debug variable = j]
  br label %33, !dbg !199                         ; [debug line = 182:55]

; <label>:35                                      ; preds = %33
  %36 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @.str14, i32 %tmp.51), !dbg !201 ; [#uses=0 type=i32] [debug line = 189:5]
  %i.6 = add i6 %i6, 1, !dbg !202                 ; [#uses=1 type=i6] [debug line = 180:54]
  call void @llvm.dbg.value(metadata !{i6 %i.6}, i64 0, metadata !203), !dbg !202 ; [debug line = 180:54] [debug variable = i]
  br label %.preheader, !dbg !202                 ; [debug line = 180:54]

.preheader11:                                     ; preds = %38, %.preheader
  %i.i1 = phi i4 [ %i.8, %38 ], [ 0, %.preheader ] ; [#uses=4 type=i4]
  %i.i1.cast2 = zext i4 %i.i1 to i32, !dbg !204   ; [#uses=1 type=i32] [debug line = 215:33@208:5]
  %exitcond.i2 = icmp eq i4 %i.i1, -3, !dbg !204  ; [#uses=1 type=i1] [debug line = 215:33@208:5]
  %37 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) ; [#uses=0 type=i32]
  br i1 %exitcond.i2, label %copyBV.exit, label %38, !dbg !204 ; [debug line = 215:33@208:5]

; <label>:38                                      ; preds = %.preheader11
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @.str16) nounwind, !dbg !211 ; [debug line = 215:49@208:5]
  %tmp.i4 = zext i4 %i.i1 to i64, !dbg !213       ; [#uses=1 type=i64] [debug line = 217:3@208:5]
  %pX.addr.1 = getelementptr [13 x float]* %pX, i64 0, i64 %tmp.i4, !dbg !213 ; [#uses=1 type=float*] [debug line = 217:3@208:5]
  %pX.load.1 = load float* %pX.addr.1, align 4, !dbg !213 ; [#uses=1 type=float] [debug line = 217:3@208:5]
  %bvCnt.load = load i32* @bvCnt, align 4, !dbg !213 ; [#uses=1 type=i32] [debug line = 217:3@208:5]
  %tmp..i = mul i32 %bvCnt.load, 13, !dbg !213    ; [#uses=1 type=i32] [debug line = 217:3@208:5]
  %tmp.102.i = add i32 %i.i1.cast2, %tmp..i, !dbg !213 ; [#uses=1 type=i32] [debug line = 217:3@208:5]
  %tmp.103.i = zext i32 %tmp.102.i to i64, !dbg !213 ; [#uses=1 type=i64] [debug line = 217:3@208:5]
  %basisVectors.addr.1 = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.103.i, !dbg !213 ; [#uses=1 type=float*] [debug line = 217:3@208:5]
  store float %pX.load.1, float* %basisVectors.addr.1, align 4, !dbg !213 ; [debug line = 217:3@208:5]
  %i.8 = add i4 %i.i1, 1, !dbg !214               ; [#uses=1 type=i4] [debug line = 215:43@208:5]
  call void @llvm.dbg.value(metadata !{i4 %i.8}, i64 0, metadata !215), !dbg !214 ; [debug line = 215:43@208:5] [debug variable = i]
  br label %.preheader11, !dbg !214               ; [debug line = 215:43@208:5]

copyBV.exit:                                      ; preds = %.preheader11
  %alpha.load.3 = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 0), align 16, !dbg !216 ; [#uses=2 type=float] [debug line = 108:77@210:23]
  %tmp.i5 = fmul float %alpha.load.3, %alpha.load.3, !dbg !216 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  %Q.load.2 = load float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 0), align 16, !dbg !216 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  %C.load.2 = load float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 0), align 16, !dbg !216 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  %tmp..i6 = fadd float %Q.load.2, %C.load.2, !dbg !216 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  %minScore = fdiv float %tmp.i5, %tmp..i6, !dbg !216 ; [#uses=1 type=float] [debug line = 108:77@210:23]
  call void @llvm.dbg.value(metadata !{float %minScore}, i64 0, metadata !222) nounwind, !dbg !216 ; [debug line = 108:77@210:23] [debug variable = minScore]
  br label %39, !dbg !223                         ; [debug line = 110:28@210:23]

; <label>:39                                      ; preds = %41, %copyBV.exit
  %index.1 = phi i6 [ 1, %copyBV.exit ], [ %i.9, %41 ] ; [#uses=5 type=i6]
  %minScore1.i = phi float [ %minScore, %copyBV.exit ], [ %minScore.2, %41 ] ; [#uses=3 type=float]
  %index = phi i32 [ 0, %copyBV.exit ], [ %index.2, %41 ] ; [#uses=2 type=i32]
  %index.1.cast1 = zext i6 %index.1 to i32, !dbg !223 ; [#uses=1 type=i32] [debug line = 110:28@210:23]
  %index.1.cast1.cast = zext i6 %index.1 to i13, !dbg !223 ; [#uses=1 type=i13] [debug line = 110:28@210:23]
  %exitcond.i7 = icmp eq i6 %index.1, -23, !dbg !223 ; [#uses=1 type=i1] [debug line = 110:28@210:23]
  %40 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond.i7, label %getMinKLApprox.exit, label %41, !dbg !223 ; [debug line = 110:28@210:23]

; <label>:41                                      ; preds = %39
  %tmp.68.i = zext i6 %index.1 to i64, !dbg !225  ; [#uses=1 type=i64] [debug line = 111:79@210:23]
  %alpha.addr.2 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.68.i, !dbg !225 ; [#uses=1 type=float*] [debug line = 111:79@210:23]
  %alpha.load.4 = load float* %alpha.addr.2, align 4, !dbg !225 ; [#uses=2 type=float] [debug line = 111:79@210:23]
  %tmp.69.i = fmul float %alpha.load.4, %alpha.load.4, !dbg !225 ; [#uses=1 type=float] [debug line = 111:79@210:23]
  %tmp.70.i = mul i13 %index.1.cast1.cast, 42, !dbg !225 ; [#uses=1 type=i13] [debug line = 111:79@210:23]
  %tmp.71.i = zext i13 %tmp.70.i to i64, !dbg !225 ; [#uses=2 type=i64] [debug line = 111:79@210:23]
  %Q.addr.2 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.71.i, !dbg !225 ; [#uses=1 type=float*] [debug line = 111:79@210:23]
  %Q.load.3 = load float* %Q.addr.2, align 8, !dbg !225 ; [#uses=1 type=float] [debug line = 111:79@210:23]
  %C.addr.2 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.71.i, !dbg !225 ; [#uses=1 type=float*] [debug line = 111:79@210:23]
  %C.load.3 = load float* %C.addr.2, align 8, !dbg !225 ; [#uses=1 type=float] [debug line = 111:79@210:23]
  %tmp.72.i = fadd float %Q.load.3, %C.load.3, !dbg !225 ; [#uses=1 type=float] [debug line = 111:79@210:23]
  %tScore = fdiv float %tmp.69.i, %tmp.72.i, !dbg !225 ; [#uses=3 type=float] [debug line = 111:79@210:23]
  call void @llvm.dbg.value(metadata !{float %tScore}, i64 0, metadata !227) nounwind, !dbg !225 ; [debug line = 111:79@210:23] [debug variable = tScore]
  %tScore_to_int = bitcast float %tScore to i32   ; [#uses=2 type=i32]
  %tmp.52 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tScore_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp.53 = trunc i32 %tScore_to_int to i23       ; [#uses=1 type=i23]
  %minScore1.i_to_int = bitcast float %minScore1.i to i32 ; [#uses=2 type=i32]
  %tmp.54 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %minScore1.i_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp.55 = trunc i32 %minScore1.i_to_int to i23  ; [#uses=1 type=i23]
  %notlhs = icmp ne i8 %tmp.52, -1                ; [#uses=1 type=i1]
  %notrhs = icmp eq i23 %tmp.53, 0                ; [#uses=1 type=i1]
  %tmp.56 = or i1 %notrhs, %notlhs                ; [#uses=1 type=i1]
  %notlhs1 = icmp ne i8 %tmp.54, -1               ; [#uses=1 type=i1]
  %notrhs1 = icmp eq i23 %tmp.55, 0               ; [#uses=1 type=i1]
  %tmp.57 = or i1 %notrhs1, %notlhs1              ; [#uses=1 type=i1]
  %tmp.58 = and i1 %tmp.56, %tmp.57               ; [#uses=1 type=i1]
  %tmp.59 = fcmp olt float %tScore, %minScore1.i, !dbg !228 ; [#uses=1 type=i1] [debug line = 113:9@210:23]
  %tmp.60 = and i1 %tmp.58, %tmp.59, !dbg !228    ; [#uses=2 type=i1] [debug line = 113:9@210:23]
  call void @llvm.dbg.value(metadata !{float %tScore}, i64 0, metadata !222) nounwind, !dbg !229 ; [debug line = 114:13@210:23] [debug variable = minScore]
  call void @llvm.dbg.value(metadata !{i6 %index.1}, i64 0, metadata !231) nounwind, !dbg !232 ; [debug line = 115:13@210:23] [debug variable = index]
  %minScore.2 = select i1 %tmp.60, float %tScore, float %minScore1.i, !dbg !228 ; [#uses=1 type=float] [debug line = 113:9@210:23]
  call void @llvm.dbg.value(metadata !{float %minScore.2}, i64 0, metadata !222) nounwind, !dbg !228 ; [debug line = 113:9@210:23] [debug variable = minScore]
  %index.2 = select i1 %tmp.60, i32 %index.1.cast1, i32 %index, !dbg !228 ; [#uses=1 type=i32] [debug line = 113:9@210:23]
  call void @llvm.dbg.value(metadata !{i32 %index.2}, i64 0, metadata !231) nounwind, !dbg !228 ; [debug line = 113:9@210:23] [debug variable = index]
  %i.9 = add i6 %index.1, 1, !dbg !233            ; [#uses=1 type=i6] [debug line = 110:43@210:23]
  call void @llvm.dbg.value(metadata !{i6 %i.9}, i64 0, metadata !234) nounwind, !dbg !233 ; [debug line = 110:43@210:23] [debug variable = i]
  br label %39, !dbg !233                         ; [debug line = 110:43@210:23]

getMinKLApprox.exit:                              ; preds = %39
  %index.lcssa = phi i32 [ %index, %39 ]          ; [#uses=1 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %index}, i64 0, metadata !235), !dbg !221 ; [debug line = 210:23] [debug variable = index]
  call fastcc void @deleteBV(i32 %index.lcssa), !dbg !236 ; [debug line = 211:2]
  ret void, !dbg !237                             ; [debug line = 212:1]
}

; [#uses=2]
define internal fastcc void @swapRowAndColumn([1681 x float]* nocapture %pM, i32 %rowA) {
  call void @llvm.dbg.value(metadata !{[1681 x float]* %pM}, i64 0, metadata !238), !dbg !243 ; [debug line = 27:29] [debug variable = pM]
  call void @llvm.dbg.value(metadata !{i32 %rowA}, i64 0, metadata !244), !dbg !245 ; [debug line = 27:65] [debug variable = rowA]
  %tmp = mul i32 %rowA, 41, !dbg !246             ; [#uses=1 type=i32] [debug line = 30:40]
  br label %1, !dbg !250                          ; [debug line = 28:28]

; <label>:1                                       ; preds = %3, %0
  %i = phi i6 [ 0, %0 ], [ %i.7, %3 ]             ; [#uses=4 type=i6]
  %i.cast4.cast = zext i6 %i to i10, !dbg !250    ; [#uses=1 type=i10] [debug line = 28:28]
  %i.cast3 = zext i6 %i to i32, !dbg !250         ; [#uses=1 type=i32] [debug line = 28:28]
  %exitcond1 = icmp eq i6 %i, -23, !dbg !250      ; [#uses=1 type=i1] [debug line = 28:28]
  %2 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  br i1 %exitcond1, label %.preheader, label %3, !dbg !250 ; [debug line = 28:28]

; <label>:3                                       ; preds = %1
  %tmp. = add i32 %tmp, %i.cast3, !dbg !246       ; [#uses=1 type=i32] [debug line = 30:40]
  %tmp.50 = zext i32 %tmp. to i64, !dbg !246      ; [#uses=1 type=i64] [debug line = 30:40]
  %pM.addr = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp.50, !dbg !246 ; [#uses=2 type=float*] [debug line = 30:40]
  %temp = load float* %pM.addr, align 4, !dbg !246 ; [#uses=1 type=float] [debug line = 30:40]
  call void @llvm.dbg.value(metadata !{float %temp}, i64 0, metadata !251), !dbg !246 ; [debug line = 30:40] [debug variable = temp]
  %tmp.51 = add i10 %i.cast4.cast, -408, !dbg !252 ; [#uses=1 type=i10] [debug line = 31:9]
  %tmp.51.cast5 = sext i10 %tmp.51 to i11, !dbg !252 ; [#uses=1 type=i11] [debug line = 31:9]
  %tmp.52 = zext i11 %tmp.51.cast5 to i64, !dbg !252 ; [#uses=1 type=i64] [debug line = 31:9]
  %pM.addr.1 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp.52, !dbg !252 ; [#uses=2 type=float*] [debug line = 31:9]
  %pM.load = load float* %pM.addr.1, align 4, !dbg !252 ; [#uses=1 type=float] [debug line = 31:9]
  store float %pM.load, float* %pM.addr, align 4, !dbg !252 ; [debug line = 31:9]
  store float %temp, float* %pM.addr.1, align 4, !dbg !253 ; [debug line = 32:9]
  %i.7 = add i6 %i, 1, !dbg !254                  ; [#uses=1 type=i6] [debug line = 28:43]
  call void @llvm.dbg.value(metadata !{i6 %i.7}, i64 0, metadata !255), !dbg !254 ; [debug line = 28:43] [debug variable = i]
  br label %1, !dbg !254                          ; [debug line = 28:43]

.preheader:                                       ; preds = %5, %1
  %i1 = phi i6 [ %i.8, %5 ], [ 0, %1 ]            ; [#uses=3 type=i6]
  %i1.cast2 = zext i6 %i1 to i13, !dbg !256       ; [#uses=1 type=i13] [debug line = 35:28]
  %exitcond = icmp eq i6 %i1, -23, !dbg !256      ; [#uses=1 type=i1] [debug line = 35:28]
  %4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) ; [#uses=0 type=i32]
  br i1 %exitcond, label %6, label %5, !dbg !256  ; [debug line = 35:28]

; <label>:5                                       ; preds = %.preheader
  %tmp.53 = mul i13 %i1.cast2, 41, !dbg !258      ; [#uses=2 type=i13] [debug line = 37:40]
  %tmp.53.cast1 = trunc i13 %tmp.53 to i11, !dbg !258 ; [#uses=1 type=i11] [debug line = 37:40]
  %tmp.53.cast6 = zext i13 %tmp.53 to i32, !dbg !258 ; [#uses=1 type=i32] [debug line = 37:40]
  %tmp.54 = add i32 %tmp.53.cast6, %rowA, !dbg !258 ; [#uses=1 type=i32] [debug line = 37:40]
  %tmp.55 = zext i32 %tmp.54 to i64, !dbg !258    ; [#uses=1 type=i64] [debug line = 37:40]
  %pM.addr.2 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp.55, !dbg !258 ; [#uses=2 type=float*] [debug line = 37:40]
  %temp.1 = load float* %pM.addr.2, align 4, !dbg !258 ; [#uses=1 type=float] [debug line = 37:40]
  call void @llvm.dbg.value(metadata !{float %temp.1}, i64 0, metadata !260), !dbg !258 ; [debug line = 37:40] [debug variable = temp]
  %tmp.56 = add i11 %tmp.53.cast1, 40, !dbg !261  ; [#uses=1 type=i11] [debug line = 38:9]
  %tmp.57 = zext i11 %tmp.56 to i64, !dbg !261    ; [#uses=1 type=i64] [debug line = 38:9]
  %pM.addr.3 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp.57, !dbg !261 ; [#uses=2 type=float*] [debug line = 38:9]
  %pM.load.2 = load float* %pM.addr.3, align 4, !dbg !261 ; [#uses=1 type=float] [debug line = 38:9]
  store float %pM.load.2, float* %pM.addr.2, align 4, !dbg !261 ; [debug line = 38:9]
  store float %temp.1, float* %pM.addr.3, align 4, !dbg !262 ; [debug line = 39:9]
  %i.8 = add i6 %i1, 1, !dbg !263                 ; [#uses=1 type=i6] [debug line = 35:43]
  call void @llvm.dbg.value(metadata !{i6 %i.8}, i64 0, metadata !264), !dbg !263 ; [debug line = 35:43] [debug variable = i]
  br label %.preheader, !dbg !263                 ; [debug line = 35:43]

; <label>:6                                       ; preds = %.preheader
  ret void, !dbg !265                             ; [debug line = 41:1]
}

; [#uses=0]
define float @projection_gp([13 x float]* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pReset) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([13 x float]* %pX) nounwind, !map !266
  call void (...)* @_ssdm_op_SpecBitsMap(float %pY) nounwind, !map !272
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pPredict) nounwind, !map !278
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pReset) nounwind, !map !282
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !286
  call void (...)* @_ssdm_op_SpecTopModule([14 x i8]* @projection_gp.str) nounwind
  call void @llvm.dbg.value(metadata !{[13 x float]* %pX}, i64 0, metadata !292), !dbg !298 ; [debug line = 221:33] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !299), !dbg !300 ; [debug line = 221:53] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{i1 %pPredict}, i64 0, metadata !301), !dbg !302 ; [debug line = 221:68] [debug variable = pPredict]
  call void @llvm.dbg.value(metadata !{i1 %pReset}, i64 0, metadata !303), !dbg !304 ; [debug line = 221:89] [debug variable = pReset]
  call void (...)* @_ssdm_op_SpecInterface([13 x float]* %pX, [10 x i8]* @.str17, i32 0, i32 0, i32 0, i32 13, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(float %pY, [10 x i8]* @.str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !305 ; [debug line = 223:1]
  call void (...)* @_ssdm_op_SpecInterface(i1 %pPredict, [10 x i8]* @.str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !307 ; [debug line = 224:1]
  call void (...)* @_ssdm_op_SpecInterface(i1 %pReset, [10 x i8]* @.str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !308 ; [debug line = 225:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @.str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !309 ; [debug line = 226:1]
  br i1 %pReset, label %.preheader3, label %.loopexit4, !dbg !310 ; [debug line = 228:2]

.preheader3:                                      ; preds = %6, %0
  %i = phi i6 [ %i.9, %6 ], [ 0, %0 ]             ; [#uses=6 type=i6]
  %i.cast5 = zext i6 %i to i10, !dbg !311         ; [#uses=1 type=i10] [debug line = 229:37]
  %i.cast = zext i6 %i to i7, !dbg !311           ; [#uses=1 type=i7] [debug line = 229:37]
  %exitcond2 = icmp eq i6 %i, -23, !dbg !311      ; [#uses=1 type=i1] [debug line = 229:37]
  %1 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond2, label %.loopexit4, label %2, !dbg !311 ; [debug line = 229:37]

; <label>:2                                       ; preds = %.preheader3
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @.str18) nounwind, !dbg !314 ; [debug line = 229:58]
  %tmp = call i32 (...)* @_ssdm_op_SpecRegionBegin([11 x i8]* @.str18) nounwind, !dbg !314 ; [#uses=1 type=i32] [debug line = 229:58]
  %_shl = shl nuw i10 %i.cast5, 4, !dbg !316      ; [#uses=1 type=i10] [debug line = 234:6]
  %_shl.cast = zext i10 %_shl to i11, !dbg !316   ; [#uses=1 type=i11] [debug line = 234:6]
  %_shl1 = shl nuw i7 %i.cast, 1, !dbg !316       ; [#uses=1 type=i7] [debug line = 234:6]
  %_shl1.cast = zext i7 %_shl1 to i11, !dbg !316  ; [#uses=1 type=i11] [debug line = 234:6]
  %tmp. = sub i11 %_shl.cast, %_shl1.cast, !dbg !316 ; [#uses=1 type=i11] [debug line = 234:6]
  %tmp..cast = sext i11 %tmp. to i32, !dbg !316   ; [#uses=1 type=i32] [debug line = 234:6]
  %tmp.58 = zext i32 %tmp..cast to i64, !dbg !316 ; [#uses=2 type=i64] [debug line = 234:6]
  %Q.addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.58, !dbg !316 ; [#uses=1 type=float*] [debug line = 234:6]
  %C.addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.58, !dbg !320 ; [#uses=1 type=float*] [debug line = 235:6]
  br label %3, !dbg !321                          ; [debug line = 231:39]

; <label>:3                                       ; preds = %5, %2
  %j = phi i6 [ 0, %2 ], [ %j.4, %5 ]             ; [#uses=3 type=i6]
  %exitcond1 = icmp eq i6 %j, -23, !dbg !321      ; [#uses=1 type=i1] [debug line = 231:39]
  %4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond1, label %6, label %5, !dbg !321 ; [debug line = 231:39]

; <label>:5                                       ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @.str19) nounwind, !dbg !322 ; [debug line = 231:60]
  %tmp.61 = icmp eq i6 %i, %j, !dbg !323          ; [#uses=1 type=i1] [debug line = 233:5]
  %storemerge5 = select i1 %tmp.61, float 1.000000e+00, float 0.000000e+00, !dbg !323 ; [#uses=2 type=float] [debug line = 233:5]
  store float %storemerge5, float* %Q.addr, align 8, !dbg !324 ; [debug line = 237:6]
  store float %storemerge5, float* %C.addr, align 8, !dbg !320 ; [debug line = 235:6]
  %j.4 = add i6 %j, 1, !dbg !326                  ; [#uses=1 type=i6] [debug line = 231:54]
  call void @llvm.dbg.value(metadata !{i6 %j.4}, i64 0, metadata !327), !dbg !326 ; [debug line = 231:54] [debug variable = j]
  br label %3, !dbg !326                          ; [debug line = 231:54]

; <label>:6                                       ; preds = %3
  %tmp.60 = zext i6 %i to i64, !dbg !328          ; [#uses=1 type=i64] [debug line = 241:4]
  %alpha.addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.60, !dbg !328 ; [#uses=1 type=float*] [debug line = 241:4]
  store float 0.000000e+00, float* %alpha.addr, align 4, !dbg !328 ; [debug line = 241:4]
  %7 = call i32 (...)* @_ssdm_op_SpecRegionEnd([11 x i8]* @.str18, i32 %tmp) nounwind, !dbg !329 ; [#uses=0 type=i32] [debug line = 242:3]
  %i.9 = add i6 %i, 1, !dbg !330                  ; [#uses=1 type=i6] [debug line = 229:52]
  call void @llvm.dbg.value(metadata !{i6 %i.9}, i64 0, metadata !331), !dbg !330 ; [debug line = 229:52] [debug variable = i]
  br label %.preheader3, !dbg !330                ; [debug line = 229:52]

.loopexit4:                                       ; preds = %.preheader3, %0
  br i1 %pPredict, label %.preheader, label %8, !dbg !332 ; [debug line = 245:2]

; <label>:8                                       ; preds = %.loopexit4
  %bvCnt.load = load i32* @bvCnt, align 4, !dbg !333 ; [#uses=2 type=i32] [debug line = 246:3]
  %tmp.59 = icmp eq i32 %bvCnt.load, 40, !dbg !333 ; [#uses=1 type=i1] [debug line = 246:3]
  br i1 %tmp.59, label %9, label %.preheader6, !dbg !333 ; [debug line = 246:3]

; <label>:9                                       ; preds = %8
  call fastcc void @train_full_bv_set([13 x float]* %pX, float %pY) nounwind, !dbg !335 ; [debug line = 247:4]
  br label %12, !dbg !337                         ; [debug line = 248:3]

.preheader6:                                      ; preds = %11, %8
  %bvCnt.load.1 = phi i32 [ %bvCnt.load.2, %11 ], [ %bvCnt.load, %8 ] ; [#uses=1 type=i32]
  %i.i = phi i4 [ %i.10, %11 ], [ 0, %8 ]         ; [#uses=4 type=i4]
  %i.i.cast4 = zext i4 %i.i to i32, !dbg !338     ; [#uses=1 type=i32] [debug line = 215:33@249:4]
  %exitcond.i = icmp eq i4 %i.i, -3, !dbg !338    ; [#uses=1 type=i1] [debug line = 215:33@249:4]
  %10 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond.i, label %copyBV.exit, label %11, !dbg !338 ; [debug line = 215:33@249:4]

; <label>:11                                      ; preds = %.preheader6
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @.str16) nounwind, !dbg !341 ; [debug line = 215:49@249:4]
  %tmp.i = zext i4 %i.i to i64, !dbg !342         ; [#uses=1 type=i64] [debug line = 217:3@249:4]
  %pX.addr = getelementptr [13 x float]* %pX, i64 0, i64 %tmp.i, !dbg !342 ; [#uses=1 type=float*] [debug line = 217:3@249:4]
  %pX.load = load float* %pX.addr, align 4, !dbg !342 ; [#uses=1 type=float] [debug line = 217:3@249:4]
  %bvCnt.load.2 = load i32* @bvCnt, align 4, !dbg !342 ; [#uses=2 type=i32] [debug line = 217:3@249:4]
  %tmp..i = mul i32 %bvCnt.load.2, 13, !dbg !342  ; [#uses=1 type=i32] [debug line = 217:3@249:4]
  %tmp.102.i = add i32 %i.i.cast4, %tmp..i, !dbg !342 ; [#uses=1 type=i32] [debug line = 217:3@249:4]
  %tmp.103.i = zext i32 %tmp.102.i to i64, !dbg !342 ; [#uses=1 type=i64] [debug line = 217:3@249:4]
  %basisVectors.addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.103.i, !dbg !342 ; [#uses=1 type=float*] [debug line = 217:3@249:4]
  store float %pX.load, float* %basisVectors.addr, align 4, !dbg !342 ; [debug line = 217:3@249:4]
  %i.10 = add i4 %i.i, 1, !dbg !343               ; [#uses=1 type=i4] [debug line = 215:43@249:4]
  call void @llvm.dbg.value(metadata !{i4 %i.10}, i64 0, metadata !344) nounwind, !dbg !343 ; [debug line = 215:43@249:4] [debug variable = i]
  br label %.preheader6, !dbg !343                ; [debug line = 215:43@249:4]

copyBV.exit:                                      ; preds = %.preheader6
  %bvCnt.load.1.lcssa = phi i32 [ %bvCnt.load.1, %.preheader6 ] ; [#uses=1 type=i32]
  %tmp.62 = add i32 %bvCnt.load.1.lcssa, 1, !dbg !345 ; [#uses=1 type=i32] [debug line = 250:4]
  store i32 %tmp.62, i32* @bvCnt, align 4, !dbg !345 ; [debug line = 250:4]
  br label %12

; <label>:12                                      ; preds = %copyBV.exit, %9
  br label %.loopexit, !dbg !346                  ; [debug line = 252:3]

.preheader:                                       ; preds = %K.exit, %.loopexit4
  %sum = phi float [ %sum.1, %K.exit ], [ 0x4050B830E0000000, %.loopexit4 ] ; [#uses=2 type=float]
  %i1 = phi i6 [ %i.11, %K.exit ], [ 0, %.loopexit4 ] ; [#uses=4 type=i6]
  %i1.cast3 = zext i6 %i1 to i10, !dbg !347       ; [#uses=1 type=i10] [debug line = 16:42@257:11]
  %exitcond = icmp eq i6 %i1, -24, !dbg !352      ; [#uses=1 type=i1] [debug line = 255:37]
  %13 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond, label %.loopexit, label %14, !dbg !352 ; [debug line = 255:37]

; <label>:14                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @.str20) nounwind, !dbg !353 ; [debug line = 255:53]
  %tmp.64 = call i32 (...)* @_ssdm_op_SpecRegionBegin([11 x i8]* @.str20) nounwind, !dbg !353 ; [#uses=1 type=i32] [debug line = 255:53]
  %tmp.63 = mul i10 %i1.cast3, 13, !dbg !347      ; [#uses=1 type=i10] [debug line = 16:42@257:11]
  call void @llvm.dbg.value(metadata !{[13 x float]* %pX}, i64 0, metadata !354) nounwind, !dbg !347 ; [debug line = 16:42@257:11] [debug variable = pX2]
  br label %15, !dbg !355                         ; [debug line = 18:28@257:11]

; <label>:15                                      ; preds = %17, %14
  %sum.i = phi float [ 0.000000e+00, %14 ], [ %sum.2, %17 ] ; [#uses=2 type=float]
  %i.i2 = phi i4 [ 0, %14 ], [ %i.12, %17 ]       ; [#uses=4 type=i4]
  %exitcond.i3 = icmp eq i4 %i.i2, -3, !dbg !355  ; [#uses=1 type=i1] [debug line = 18:28@257:11]
  %16 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond.i3, label %K.exit, label %17, !dbg !355 ; [debug line = 18:28@257:11]

; <label>:17                                      ; preds = %15
  %tmp.106.i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @.str) nounwind, !dbg !356 ; [#uses=1 type=i32] [debug line = 18:44@257:11]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @.str1) nounwind, !dbg !357 ; [debug line = 19:1@257:11]
  %tmp.104.i4 = zext i4 %i.i2 to i64, !dbg !358   ; [#uses=1 type=i64] [debug line = 20:31@257:11]
  %tmp.104.i4.cast = zext i4 %i.i2 to i10         ; [#uses=1 type=i10]
  %sum1.i = add i10 %tmp.63, %tmp.104.i4.cast     ; [#uses=1 type=i10]
  %sum1.i.cast = zext i10 %sum1.i to i64          ; [#uses=1 type=i64]
  %basisVectors.addr.2 = getelementptr [533 x float]* @basisVectors, i64 0, i64 %sum1.i.cast, !dbg !358 ; [#uses=1 type=float*] [debug line = 20:31@257:11]
  %basisVectors.load = load float* %basisVectors.addr.2, align 4, !dbg !358 ; [#uses=1 type=float] [debug line = 20:31@257:11]
  %pX.addr.2 = getelementptr [13 x float]* %pX, i64 0, i64 %tmp.104.i4, !dbg !358 ; [#uses=1 type=float*] [debug line = 20:31@257:11]
  %pX.load.2 = load float* %pX.addr.2, align 4, !dbg !358 ; [#uses=1 type=float] [debug line = 20:31@257:11]
  %val = fsub float %basisVectors.load, %pX.load.2, !dbg !358 ; [#uses=2 type=float] [debug line = 20:31@257:11]
  call void @llvm.dbg.value(metadata !{float %val}, i64 0, metadata !359) nounwind, !dbg !358 ; [debug line = 20:31@257:11] [debug variable = val]
  %tmp.105.i = fmul float %val, %val, !dbg !360   ; [#uses=1 type=float] [debug line = 21:9@257:11]
  %sum.2 = fadd float %sum.i, %tmp.105.i, !dbg !360 ; [#uses=1 type=float] [debug line = 21:9@257:11]
  call void @llvm.dbg.value(metadata !{float %sum.2}, i64 0, metadata !361) nounwind, !dbg !360 ; [debug line = 21:9@257:11] [debug variable = sum]
  %18 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @.str, i32 %tmp.106.i) nounwind, !dbg !362 ; [#uses=0 type=i32] [debug line = 22:5@257:11]
  %i.12 = add i4 %i.i2, 1, !dbg !363              ; [#uses=1 type=i4] [debug line = 18:38@257:11]
  call void @llvm.dbg.value(metadata !{i4 %i.12}, i64 0, metadata !364) nounwind, !dbg !363 ; [debug line = 18:38@257:11] [debug variable = i]
  br label %15, !dbg !363                         ; [debug line = 18:38@257:11]

K.exit:                                           ; preds = %15
  %sum.i.lcssa = phi float [ %sum.i, %15 ]        ; [#uses=1 type=float]
  %__x.assign = fmul float %sum.i.lcssa, -5.000000e-01, !dbg !365 ; [#uses=1 type=float] [debug line = 24:12@257:11]
  call void @llvm.dbg.value(metadata !{float %__x.assign}, i64 0, metadata !366) nounwind, !dbg !367 ; [debug line = 216:13@24:12@257:11] [debug variable = __x]
  %tmp.i.i = call float @llvm.exp.f32(float %__x.assign) nounwind, !dbg !368 ; [#uses=1 type=float] [debug line = 217:12@24:12@257:11]
  %tmp.65 = zext i6 %i1 to i64, !dbg !348         ; [#uses=1 type=i64] [debug line = 257:11]
  %alpha.addr.2 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.65, !dbg !348 ; [#uses=1 type=float*] [debug line = 257:11]
  %alpha.load = load float* %alpha.addr.2, align 4, !dbg !348 ; [#uses=1 type=float] [debug line = 257:11]
  %tmp.66 = fmul float %tmp.i.i, %alpha.load, !dbg !348 ; [#uses=1 type=float] [debug line = 257:11]
  %sum.1 = fadd float %sum, %tmp.66, !dbg !348    ; [#uses=1 type=float] [debug line = 257:11]
  call void @llvm.dbg.value(metadata !{float %sum.1}, i64 0, metadata !369), !dbg !348 ; [debug line = 257:11] [debug variable = sum]
  %19 = call i32 (...)* @_ssdm_op_SpecRegionEnd([11 x i8]* @.str20, i32 %tmp.64) nounwind, !dbg !370 ; [#uses=0 type=i32] [debug line = 258:3]
  %i.11 = add i6 %i1, 1, !dbg !371                ; [#uses=1 type=i6] [debug line = 255:47]
  call void @llvm.dbg.value(metadata !{i6 %i.11}, i64 0, metadata !372), !dbg !371 ; [debug line = 255:47] [debug variable = i]
  br label %.preheader, !dbg !371                 ; [debug line = 255:47]

.loopexit:                                        ; preds = %.preheader, %12
  %.0 = phi float [ 0.000000e+00, %12 ], [ %sum, %.preheader ] ; [#uses=1 type=float]
  ret float %.0, !dbg !373                        ; [debug line = 262:1]
}

; [#uses=2]
declare float @llvm.exp.f32(float) nounwind readonly

; [#uses=71]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=1]
define internal fastcc void @deleteBV(i32 %pIndex) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{i32 %pIndex}, i64 0, metadata !374), !dbg !378 ; [debug line = 43:29] [debug variable = pIndex]
  %tmp = zext i32 %pIndex to i64, !dbg !379       ; [#uses=1 type=i64] [debug line = 47:31]
  %alpha.addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp, !dbg !379 ; [#uses=2 type=float*] [debug line = 47:31]
  %temp = load float* %alpha.addr, align 4, !dbg !379 ; [#uses=1 type=float] [debug line = 47:31]
  call void @llvm.dbg.value(metadata !{float %temp}, i64 0, metadata !381), !dbg !379 ; [debug line = 47:31] [debug variable = temp]
  %alpha.load = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !382 ; [#uses=1 type=float] [debug line = 48:5]
  store float %alpha.load, float* %alpha.addr, align 4, !dbg !382 ; [debug line = 48:5]
  store float %temp, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !383 ; [debug line = 49:5]
  call fastcc void @swapRowAndColumn([1681 x float]* @C, i32 %pIndex) nounwind, !dbg !384 ; [debug line = 51:5]
  call fastcc void @swapRowAndColumn([1681 x float]* @Q, i32 %pIndex) nounwind, !dbg !385 ; [debug line = 52:5]
  %tmp. = mul i32 %pIndex, 13, !dbg !386          ; [#uses=1 type=i32] [debug line = 56:2]
  br label %1, !dbg !389                          ; [debug line = 54:48]

; <label>:1                                       ; preds = %3, %0
  %i = phi i4 [ 0, %0 ], [ %i.13, %3 ]            ; [#uses=4 type=i4]
  %i.cast9 = zext i4 %i to i10, !dbg !389         ; [#uses=1 type=i10] [debug line = 54:48]
  %i.cast8 = zext i4 %i to i32, !dbg !389         ; [#uses=1 type=i32] [debug line = 54:48]
  %exitcond4 = icmp eq i4 %i, -3, !dbg !389       ; [#uses=1 type=i1] [debug line = 54:48]
  %2 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond4, label %5, label %3, !dbg !389 ; [debug line = 54:48]

; <label>:3                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @.str2) nounwind, !dbg !390 ; [debug line = 54:64]
  %tmp.68 = call i32 (...)* @_ssdm_op_SpecRegionBegin([20 x i8]* @.str2) nounwind, !dbg !390 ; [#uses=1 type=i32] [debug line = 54:64]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @.str1) nounwind, !dbg !391 ; [debug line = 55:1]
  %tmp.69 = add i32 %tmp., %i.cast8, !dbg !386    ; [#uses=1 type=i32] [debug line = 56:2]
  %tmp.70 = zext i32 %tmp.69 to i64, !dbg !386    ; [#uses=1 type=i64] [debug line = 56:2]
  %basisVectors.addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.70, !dbg !386 ; [#uses=2 type=float*] [debug line = 56:2]
  %temp.3 = load float* %basisVectors.addr, align 4, !dbg !386 ; [#uses=1 type=float] [debug line = 56:2]
  call void @llvm.dbg.value(metadata !{float %temp.3}, i64 0, metadata !381), !dbg !386 ; [debug line = 56:2] [debug variable = temp]
  %tmp.71 = add i10 %i.cast9, -504, !dbg !392     ; [#uses=1 type=i10] [debug line = 57:9]
  %tmp.73 = zext i10 %tmp.71 to i64, !dbg !392    ; [#uses=1 type=i64] [debug line = 57:9]
  %basisVectors.addr.1 = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp.73, !dbg !392 ; [#uses=2 type=float*] [debug line = 57:9]
  %basisVectors.load = load float* %basisVectors.addr.1, align 4, !dbg !392 ; [#uses=1 type=float] [debug line = 57:9]
  store float %basisVectors.load, float* %basisVectors.addr, align 4, !dbg !392 ; [debug line = 57:9]
  store float %temp.3, float* %basisVectors.addr.1, align 4, !dbg !393 ; [debug line = 58:9]
  %4 = call i32 (...)* @_ssdm_op_SpecRegionEnd([20 x i8]* @.str2, i32 %tmp.68) nounwind, !dbg !394 ; [#uses=0 type=i32] [debug line = 59:5]
  %i.13 = add i4 %i, 1, !dbg !395                 ; [#uses=1 type=i4] [debug line = 54:58]
  call void @llvm.dbg.value(metadata !{i4 %i.13}, i64 0, metadata !396), !dbg !395 ; [debug line = 54:58] [debug variable = i]
  br label %1, !dbg !395                          ; [debug line = 54:58]

; <label>:5                                       ; preds = %1
  %alphaStar = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !397 ; [#uses=1 type=float] [debug line = 62:32]
  call void @llvm.dbg.value(metadata !{float %alphaStar}, i64 0, metadata !398), !dbg !397 ; [debug line = 62:32] [debug variable = alphaStar]
  %cStar = load float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 1680), align 16, !dbg !399 ; [#uses=1 type=float] [debug line = 63:24]
  call void @llvm.dbg.value(metadata !{float %cStar}, i64 0, metadata !400), !dbg !399 ; [debug line = 63:24] [debug variable = cStar]
  %qStar = load float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 1680), align 16, !dbg !401 ; [#uses=2 type=float] [debug line = 81:45]
  call void @llvm.dbg.value(metadata !{float %qStar}, i64 0, metadata !406), !dbg !407 ; [debug line = 64:24] [debug variable = qStar]
  %tmp.67 = fadd float %cStar, %qStar, !dbg !408  ; [#uses=2 type=float] [debug line = 65:5]
  %temp.2 = fdiv float %alphaStar, %tmp.67, !dbg !408 ; [#uses=1 type=float] [debug line = 65:5]
  call void @llvm.dbg.value(metadata !{float %temp.2}, i64 0, metadata !381), !dbg !408 ; [debug line = 65:5] [debug variable = temp]
  br label %6, !dbg !409                          ; [debug line = 67:44]

; <label>:6                                       ; preds = %8, %5
  %i1 = phi i6 [ 0, %5 ], [ %i.14, %8 ]           ; [#uses=4 type=i6]
  %i1.cast7.cast = zext i6 %i1 to i10, !dbg !409  ; [#uses=1 type=i10] [debug line = 67:44]
  %exitcond3 = icmp eq i6 %i1, -24, !dbg !409     ; [#uses=1 type=i1] [debug line = 67:44]
  %7 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond3, label %9, label %8, !dbg !409 ; [debug line = 67:44]

; <label>:8                                       ; preds = %6
  call void (...)* @_ssdm_op_SpecLoopName([16 x i8]* @.str3) nounwind, !dbg !411 ; [debug line = 67:60]
  %a = add i10 %i1.cast7.cast, -408, !dbg !413    ; [#uses=1 type=i10] [debug line = 69:35]
  %a.cast = sext i10 %a to i11, !dbg !413         ; [#uses=1 type=i11] [debug line = 69:35]
  call void @llvm.dbg.value(metadata !{i10 %a}, i64 0, metadata !414), !dbg !413 ; [debug line = 69:35] [debug variable = a]
  %tmp.74 = zext i11 %a.cast to i64, !dbg !415    ; [#uses=2 type=i64] [debug line = 70:9]
  %C.addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.74, !dbg !415 ; [#uses=1 type=float*] [debug line = 70:9]
  %C.load = load float* %C.addr, align 4, !dbg !415 ; [#uses=1 type=float] [debug line = 70:9]
  %Q.addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.74, !dbg !415 ; [#uses=1 type=float*] [debug line = 70:9]
  %Q.load = load float* %Q.addr, align 4, !dbg !415 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp.75 = fadd float %C.load, %Q.load, !dbg !415 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp.76 = fmul float %tmp.75, %temp.2, !dbg !415 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp.77 = zext i6 %i1 to i64, !dbg !415         ; [#uses=1 type=i64] [debug line = 70:9]
  %alpha.addr.3 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp.77, !dbg !415 ; [#uses=2 type=float*] [debug line = 70:9]
  %alpha.load.5 = load float* %alpha.addr.3, align 4, !dbg !415 ; [#uses=1 type=float] [debug line = 70:9]
  %tmp.78 = fsub float %alpha.load.5, %tmp.76, !dbg !415 ; [#uses=1 type=float] [debug line = 70:9]
  store float %tmp.78, float* %alpha.addr.3, align 4, !dbg !415 ; [debug line = 70:9]
  %i.14 = add i6 %i1, 1, !dbg !416                ; [#uses=1 type=i6] [debug line = 67:54]
  call void @llvm.dbg.value(metadata !{i6 %i.14}, i64 0, metadata !417), !dbg !416 ; [debug line = 67:54] [debug variable = i]
  br label %6, !dbg !416                          ; [debug line = 67:54]

; <label>:9                                       ; preds = %6
  store float 0.000000e+00, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16, !dbg !418 ; [debug line = 72:5]
  br label %10, !dbg !419                         ; [debug line = 74:45]

; <label>:10                                      ; preds = %16, %9
  %i2 = phi i6 [ 0, %9 ], [ %i.16, %16 ]          ; [#uses=4 type=i6]
  %i2.cast5 = zext i6 %i2 to i11, !dbg !420       ; [#uses=1 type=i11] [debug line = 80:38]
  %i2.cast4.cast = zext i6 %i2 to i10, !dbg !419  ; [#uses=1 type=i10] [debug line = 74:45]
  %exitcond2 = icmp eq i6 %i2, -24, !dbg !419     ; [#uses=1 type=i1] [debug line = 74:45]
  %11 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond2, label %.preheader, label %12, !dbg !419 ; [debug line = 74:45]

; <label>:12                                      ; preds = %10
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @.str4) nounwind, !dbg !421 ; [debug line = 74:61]
  %tmp.72 = call i32 (...)* @_ssdm_op_SpecRegionBegin([18 x i8]* @.str4) nounwind, !dbg !421 ; [#uses=1 type=i32] [debug line = 74:61]
  %a.2 = add i10 %i2.cast4.cast, -408, !dbg !422  ; [#uses=1 type=i10] [debug line = 78:39]
  %a.2.cast = sext i10 %a.2 to i11, !dbg !422     ; [#uses=1 type=i11] [debug line = 78:39]
  %tmp.79 = mul i11 %i2.cast5, 41, !dbg !420      ; [#uses=1 type=i11] [debug line = 80:38]
  %tmp.80 = zext i11 %a.2.cast to i64, !dbg !401  ; [#uses=2 type=i64] [debug line = 81:45]
  %Q.addr.2 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.80, !dbg !401 ; [#uses=1 type=float*] [debug line = 81:45]
  %C.addr.2 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.80, !dbg !423 ; [#uses=1 type=float*] [debug line = 84:13]
  br label %13, !dbg !424                         ; [debug line = 76:47]

; <label>:13                                      ; preds = %15, %12
  %j = phi i6 [ 0, %12 ], [ %j.5, %15 ]           ; [#uses=4 type=i6]
  %j.cast3 = zext i6 %j to i11, !dbg !424         ; [#uses=1 type=i11] [debug line = 76:47]
  %j.cast3.cast = zext i6 %j to i10, !dbg !424    ; [#uses=1 type=i10] [debug line = 76:47]
  %exitcond1 = icmp eq i6 %j, -24, !dbg !424      ; [#uses=1 type=i1] [debug line = 76:47]
  %14 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond1, label %16, label %15, !dbg !424 ; [debug line = 76:47]

; <label>:15                                      ; preds = %13
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @.str5) nounwind, !dbg !425 ; [debug line = 76:63]
  call void @llvm.dbg.value(metadata !{i10 %a.2}, i64 0, metadata !426), !dbg !422 ; [debug line = 78:39] [debug variable = a]
  %b.1 = add i10 %j.cast3.cast, -408, !dbg !427   ; [#uses=1 type=i10] [debug line = 79:39]
  %b.1.cast = sext i10 %b.1 to i11, !dbg !427     ; [#uses=1 type=i11] [debug line = 79:39]
  call void @llvm.dbg.value(metadata !{i10 %b.1}, i64 0, metadata !428), !dbg !427 ; [debug line = 79:39] [debug variable = b]
  %c = add i11 %tmp.79, %j.cast3, !dbg !420       ; [#uses=1 type=i11] [debug line = 80:38]
  call void @llvm.dbg.value(metadata !{i11 %c}, i64 0, metadata !429), !dbg !420 ; [debug line = 80:38] [debug variable = c]
  %Q.load.3 = load float* %Q.addr.2, align 4, !dbg !401 ; [#uses=2 type=float] [debug line = 81:45]
  %tmp.84 = zext i11 %b.1.cast to i64, !dbg !401  ; [#uses=2 type=i64] [debug line = 81:45]
  %Q.addr.5 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.84, !dbg !401 ; [#uses=1 type=float*] [debug line = 81:45]
  %Q.load.4 = load float* %Q.addr.5, align 4, !dbg !401 ; [#uses=2 type=float] [debug line = 81:45]
  %tmp.85 = fmul float %Q.load.3, %Q.load.4, !dbg !401 ; [#uses=2 type=float] [debug line = 81:45]
  %temp.4 = fdiv float %tmp.85, %qStar, !dbg !401 ; [#uses=2 type=float] [debug line = 81:45]
  call void @llvm.dbg.value(metadata !{float %temp.4}, i64 0, metadata !430), !dbg !401 ; [debug line = 81:45] [debug variable = temp]
  %C.load.3 = load float* %C.addr.2, align 4, !dbg !423 ; [#uses=2 type=float] [debug line = 84:13]
  %C.addr.5 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.84, !dbg !423 ; [#uses=1 type=float*] [debug line = 84:13]
  %C.load.4 = load float* %C.addr.5, align 4, !dbg !423 ; [#uses=2 type=float] [debug line = 84:13]
  %tmp.86 = fmul float %C.load.3, %C.load.4, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.87 = fmul float %C.load.3, %Q.load.4, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.88 = fadd float %tmp.86, %tmp.87, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.89 = fmul float %Q.load.3, %C.load.4, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.90 = fadd float %tmp.88, %tmp.89, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.91 = fadd float %tmp.90, %tmp.85, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.92 = fdiv float %tmp.91, %tmp.67, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.93 = fsub float %temp.4, %tmp.92, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.94 = zext i11 %c to i64, !dbg !423         ; [#uses=2 type=i64] [debug line = 84:13]
  %C.addr.6 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.94, !dbg !423 ; [#uses=2 type=float*] [debug line = 84:13]
  %C.load.5 = load float* %C.addr.6, align 4, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  %tmp.95 = fadd float %C.load.5, %tmp.93, !dbg !423 ; [#uses=1 type=float] [debug line = 84:13]
  store float %tmp.95, float* %C.addr.6, align 4, !dbg !423 ; [debug line = 84:13]
  %Q.addr.6 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.94, !dbg !431 ; [#uses=2 type=float*] [debug line = 90:13]
  %Q.load.5 = load float* %Q.addr.6, align 4, !dbg !431 ; [#uses=1 type=float] [debug line = 90:13]
  %tmp.96 = fsub float %Q.load.5, %temp.4, !dbg !431 ; [#uses=1 type=float] [debug line = 90:13]
  store float %tmp.96, float* %Q.addr.6, align 4, !dbg !431 ; [debug line = 90:13]
  %j.5 = add i6 %j, 1, !dbg !432                  ; [#uses=1 type=i6] [debug line = 76:57]
  call void @llvm.dbg.value(metadata !{i6 %j.5}, i64 0, metadata !433), !dbg !432 ; [debug line = 76:57] [debug variable = j]
  br label %13, !dbg !432                         ; [debug line = 76:57]

; <label>:16                                      ; preds = %13
  %17 = call i32 (...)* @_ssdm_op_SpecRegionEnd([18 x i8]* @.str4, i32 %tmp.72) nounwind, !dbg !434 ; [#uses=0 type=i32] [debug line = 92:5]
  %i.16 = add i6 %i2, 1, !dbg !435                ; [#uses=1 type=i6] [debug line = 74:55]
  call void @llvm.dbg.value(metadata !{i6 %i.16}, i64 0, metadata !436), !dbg !435 ; [debug line = 74:55] [debug variable = i]
  br label %10, !dbg !435                         ; [debug line = 74:55]

.preheader:                                       ; preds = %19, %10
  %i5 = phi i6 [ %i.15, %19 ], [ 0, %10 ]         ; [#uses=4 type=i6]
  %i5.cast2.cast = zext i6 %i5 to i10, !dbg !437  ; [#uses=1 type=i10] [debug line = 94:48]
  %i5.cast1 = zext i6 %i5 to i11, !dbg !439       ; [#uses=1 type=i11] [debug line = 96:35]
  %exitcond = icmp eq i6 %i5, -23, !dbg !437      ; [#uses=1 type=i1] [debug line = 94:48]
  %18 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond, label %20, label %19, !dbg !437 ; [debug line = 94:48]

; <label>:19                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @.str6) nounwind, !dbg !441 ; [debug line = 94:65]
  %tmp.81 = mul i11 %i5.cast1, 41, !dbg !439      ; [#uses=1 type=i11] [debug line = 96:35]
  %a.1 = add i11 %tmp.81, 40, !dbg !439           ; [#uses=1 type=i11] [debug line = 96:35]
  call void @llvm.dbg.value(metadata !{i11 %a.1}, i64 0, metadata !442), !dbg !439 ; [debug line = 96:35] [debug variable = a]
  %b = add i10 %i5.cast2.cast, -408, !dbg !443    ; [#uses=1 type=i10] [debug line = 97:35]
  %b.cast = sext i10 %b to i11, !dbg !443         ; [#uses=1 type=i11] [debug line = 97:35]
  call void @llvm.dbg.value(metadata !{i10 %b}, i64 0, metadata !444), !dbg !443 ; [debug line = 97:35] [debug variable = b]
  %tmp.82 = zext i11 %a.1 to i64, !dbg !445       ; [#uses=2 type=i64] [debug line = 99:6]
  %Q.addr.3 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.82, !dbg !445 ; [#uses=1 type=float*] [debug line = 99:6]
  store float 0.000000e+00, float* %Q.addr.3, align 4, !dbg !445 ; [debug line = 99:6]
  %tmp.83 = zext i11 %b.cast to i64, !dbg !446    ; [#uses=2 type=i64] [debug line = 100:9]
  %Q.addr.4 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp.83, !dbg !446 ; [#uses=1 type=float*] [debug line = 100:9]
  store float 0.000000e+00, float* %Q.addr.4, align 4, !dbg !446 ; [debug line = 100:9]
  %C.addr.3 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.82, !dbg !447 ; [#uses=1 type=float*] [debug line = 101:9]
  store float 0.000000e+00, float* %C.addr.3, align 4, !dbg !447 ; [debug line = 101:9]
  %C.addr.4 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp.83, !dbg !448 ; [#uses=1 type=float*] [debug line = 102:9]
  store float 0.000000e+00, float* %C.addr.4, align 4, !dbg !448 ; [debug line = 102:9]
  %i.15 = add i6 %i5, 1, !dbg !449                ; [#uses=1 type=i6] [debug line = 94:59]
  call void @llvm.dbg.value(metadata !{i6 %i.15}, i64 0, metadata !450), !dbg !449 ; [debug line = 94:59] [debug variable = i]
  br label %.preheader, !dbg !449                 ; [debug line = 94:59]

; <label>:20                                      ; preds = %.preheader
  ret void, !dbg !451                             ; [debug line = 104:1]
}

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=10]
declare i32 @_ssdm_op_SpecRegionEnd(...)

; [#uses=10]
declare i32 @_ssdm_op_SpecRegionBegin(...)

; [#uses=3]
declare void @_ssdm_op_SpecPipeline(...) nounwind

; [#uses=24]
declare i32 @_ssdm_op_SpecLoopTripCount(...)

; [#uses=19]
declare void @_ssdm_op_SpecLoopName(...) nounwind

; [#uses=5]
declare void @_ssdm_op_SpecInterface(...) nounwind

; [#uses=5]
declare void @_ssdm_op_SpecBitsMap(...)

; [#uses=2]
declare i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0, !7, !14, !19, !26, !33, !38, !45}
!llvm.dbg.cu = !{!50}

!0 = metadata !{metadata !1, [41 x float]* @s}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"s", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 40, i32 1}
!7 = metadata !{metadata !8, [40 x float]* @k}
!8 = metadata !{metadata !9}
!9 = metadata !{i32 0, i32 31, metadata !10}
!10 = metadata !{metadata !11}
!11 = metadata !{metadata !"k", metadata !12, metadata !"float", i32 0, i32 31}
!12 = metadata !{metadata !13}
!13 = metadata !{i32 0, i32 39, i32 1}
!14 = metadata !{metadata !15, [41 x float]* @e}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 31, metadata !17}
!17 = metadata !{metadata !18}
!18 = metadata !{metadata !"e", metadata !5, metadata !"float", i32 0, i32 31}
!19 = metadata !{metadata !20, i32* @bvCnt}
!20 = metadata !{metadata !21}
!21 = metadata !{i32 0, i32 31, metadata !22}
!22 = metadata !{metadata !23}
!23 = metadata !{metadata !"bvCnt", metadata !24, metadata !"unsigned int", i32 0, i32 31}
!24 = metadata !{metadata !25}
!25 = metadata !{i32 0, i32 0, i32 1}
!26 = metadata !{metadata !27, [533 x float]* @basisVectors}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 31, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"basisVectors", metadata !31, metadata !"float", i32 0, i32 31}
!31 = metadata !{metadata !32}
!32 = metadata !{i32 0, i32 532, i32 1}
!33 = metadata !{metadata !34, [41 x float]* @alpha}
!34 = metadata !{metadata !35}
!35 = metadata !{i32 0, i32 31, metadata !36}
!36 = metadata !{metadata !37}
!37 = metadata !{metadata !"alpha", metadata !5, metadata !"float", i32 0, i32 31}
!38 = metadata !{metadata !39, [1681 x float]* @Q}
!39 = metadata !{metadata !40}
!40 = metadata !{i32 0, i32 31, metadata !41}
!41 = metadata !{metadata !42}
!42 = metadata !{metadata !"Q", metadata !43, metadata !"float", i32 0, i32 31}
!43 = metadata !{metadata !44}
!44 = metadata !{i32 0, i32 1680, i32 1}
!45 = metadata !{metadata !46, [1681 x float]* @C}
!46 = metadata !{metadata !47}
!47 = metadata !{i32 0, i32 31, metadata !48}
!48 = metadata !{metadata !49}
!49 = metadata !{metadata !"C", metadata !43, metadata !"float", i32 0, i32 31}
!50 = metadata !{i32 786449, i32 0, i32 4, metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace/ProjectionGP/solution1/.autopilot/db/ProjectionGP_FULLY_OPTIMIZED.pragma.2.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, null, null, null, metadata !51} ; [ DW_TAG_compile_unit ]
!51 = metadata !{metadata !52}
!52 = metadata !{metadata !53, metadata !59, metadata !63, metadata !67, metadata !71, metadata !72, metadata !73, metadata !75, metadata !76}
!53 = metadata !{i32 786484, i32 0, null, metadata !"Q", metadata !"Q", metadata !"", metadata !54, i32 8, metadata !55, i32 0, i32 1, [1681 x float]* @Q} ; [ DW_TAG_variable ]
!54 = metadata !{i32 786473, metadata !"../../../implementation/hls/Projection_GP/include/ProjectionGP_FULLY_OPTIMIZED.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", null} ; [ DW_TAG_file_type ]
!55 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 53792, i64 32, i32 0, i32 0, metadata !56, metadata !57, i32 0, i32 0} ; [ DW_TAG_array_type ]
!56 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!57 = metadata !{metadata !58}
!58 = metadata !{i32 786465, i64 0, i64 1680}     ; [ DW_TAG_subrange_type ]
!59 = metadata !{i32 786484, i32 0, null, metadata !"k", metadata !"k", metadata !"", metadata !54, i32 10, metadata !60, i32 0, i32 1, [40 x float]* @k} ; [ DW_TAG_variable ]
!60 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1280, i64 32, i32 0, i32 0, metadata !56, metadata !61, i32 0, i32 0} ; [ DW_TAG_array_type ]
!61 = metadata !{metadata !62}
!62 = metadata !{i32 786465, i64 0, i64 39}       ; [ DW_TAG_subrange_type ]
!63 = metadata !{i32 786484, i32 0, null, metadata !"alpha", metadata !"alpha", metadata !"", metadata !54, i32 12, metadata !64, i32 0, i32 1, [41 x float]* @alpha} ; [ DW_TAG_variable ]
!64 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1312, i64 32, i32 0, i32 0, metadata !56, metadata !65, i32 0, i32 0} ; [ DW_TAG_array_type ]
!65 = metadata !{metadata !66}
!66 = metadata !{i32 786465, i64 0, i64 40}       ; [ DW_TAG_subrange_type ]
!67 = metadata !{i32 786484, i32 0, null, metadata !"basisVectors", metadata !"basisVectors", metadata !"", metadata !54, i32 13, metadata !68, i32 0, i32 1, [533 x float]* @basisVectors} ; [ DW_TAG_variable ]
!68 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 17056, i64 32, i32 0, i32 0, metadata !56, metadata !69, i32 0, i32 0} ; [ DW_TAG_array_type ]
!69 = metadata !{metadata !70}
!70 = metadata !{i32 786465, i64 0, i64 532}      ; [ DW_TAG_subrange_type ]
!71 = metadata !{i32 786484, i32 0, null, metadata !"C", metadata !"C", metadata !"", metadata !54, i32 7, metadata !55, i32 0, i32 1, [1681 x float]* @C} ; [ DW_TAG_variable ]
!72 = metadata !{i32 786484, i32 0, null, metadata !"e", metadata !"e", metadata !"", metadata !54, i32 9, metadata !64, i32 0, i32 1, [41 x float]* @e} ; [ DW_TAG_variable ]
!73 = metadata !{i32 786484, i32 0, null, metadata !"bvCnt", metadata !"bvCnt", metadata !"", metadata !54, i32 14, metadata !74, i32 0, i32 1, i32* @bvCnt} ; [ DW_TAG_variable ]
!74 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!75 = metadata !{i32 786484, i32 0, null, metadata !"s", metadata !"s", metadata !"", metadata !54, i32 11, metadata !64, i32 0, i32 1, [41 x float]* @s} ; [ DW_TAG_variable ]
!76 = metadata !{i32 786484, i32 0, null, metadata !"signgam", metadata !"signgam", metadata !"", metadata !77, i32 149, metadata !78, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!77 = metadata !{i32 786473, metadata !"/usr/include/math.h", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", null} ; [ DW_TAG_file_type ]
!78 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!79 = metadata !{i32 786689, metadata !80, metadata !"pX", null, i32 122, metadata !87, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!80 = metadata !{i32 786478, i32 0, metadata !54, metadata !"train_full_bv_set", metadata !"train_full_bv_set", metadata !"_Z17train_full_bv_setPKff", metadata !54, i32 122, metadata !81, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !85, i32 122} ; [ DW_TAG_subprogram ]
!81 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !82, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!82 = metadata !{null, metadata !83, metadata !84}
!83 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !84} ; [ DW_TAG_pointer_type ]
!84 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_const_type ]
!85 = metadata !{metadata !86}
!86 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!87 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !84, metadata !88, i32 0, i32 0} ; [ DW_TAG_array_type ]
!88 = metadata !{metadata !89}
!89 = metadata !{i32 786465, i64 0, i64 12}       ; [ DW_TAG_subrange_type ]
!90 = metadata !{i32 122, i32 37, metadata !80, null}
!91 = metadata !{i32 786689, metadata !80, metadata !"pY", metadata !54, i32 33554554, metadata !84, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!92 = metadata !{i32 122, i32 57, metadata !80, null}
!93 = metadata !{i32 126, i32 35, metadata !94, null}
!94 = metadata !{i32 786443, metadata !95, i32 126, i32 12, metadata !54, i32 24} ; [ DW_TAG_lexical_block ]
!95 = metadata !{i32 786443, metadata !80, i32 122, i32 61, metadata !54, i32 23} ; [ DW_TAG_lexical_block ]
!96 = metadata !{i32 16, i32 42, metadata !97, metadata !100}
!97 = metadata !{i32 786478, i32 0, metadata !54, metadata !"K", metadata !"K", metadata !"_Z1KPKfS0_", metadata !54, i32 16, metadata !98, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !85, i32 16} ; [ DW_TAG_subprogram ]
!98 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !99, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!99 = metadata !{metadata !56, metadata !83, metadata !83}
!100 = metadata !{i32 128, i32 16, metadata !101, null}
!101 = metadata !{i32 786443, metadata !94, i32 126, i32 50, metadata !54, i32 25} ; [ DW_TAG_lexical_block ]
!102 = metadata !{i32 143, i32 10, metadata !103, null}
!103 = metadata !{i32 786443, metadata !104, i32 141, i32 64, metadata !54, i32 29} ; [ DW_TAG_lexical_block ]
!104 = metadata !{i32 786443, metadata !105, i32 141, i32 26, metadata !54, i32 28} ; [ DW_TAG_lexical_block ]
!105 = metadata !{i32 786443, metadata !106, i32 137, i32 60, metadata !54, i32 27} ; [ DW_TAG_lexical_block ]
!106 = metadata !{i32 786443, metadata !95, i32 137, i32 22, metadata !54, i32 26} ; [ DW_TAG_lexical_block ]
!107 = metadata !{i32 126, i32 51, metadata !101, null}
!108 = metadata !{i32 786689, metadata !97, metadata !"pX2", null, i32 16, metadata !87, i32 0, metadata !100} ; [ DW_TAG_arg_variable ]
!109 = metadata !{i32 18, i32 28, metadata !110, metadata !100}
!110 = metadata !{i32 786443, metadata !111, i32 18, i32 5, metadata !54, i32 1} ; [ DW_TAG_lexical_block ]
!111 = metadata !{i32 786443, metadata !97, i32 16, i32 51, metadata !54, i32 0} ; [ DW_TAG_lexical_block ]
!112 = metadata !{i32 18, i32 44, metadata !113, metadata !100}
!113 = metadata !{i32 786443, metadata !110, i32 18, i32 43, metadata !54, i32 2} ; [ DW_TAG_lexical_block ]
!114 = metadata !{i32 19, i32 1, metadata !113, metadata !100}
!115 = metadata !{i32 20, i32 31, metadata !113, metadata !100}
!116 = metadata !{i32 786688, metadata !113, metadata !"val", metadata !54, i32 20, metadata !56, i32 0, metadata !100} ; [ DW_TAG_auto_variable ]
!117 = metadata !{i32 21, i32 9, metadata !113, metadata !100}
!118 = metadata !{i32 786688, metadata !111, metadata !"sum", metadata !54, i32 17, metadata !56, i32 0, metadata !100} ; [ DW_TAG_auto_variable ]
!119 = metadata !{i32 22, i32 5, metadata !113, metadata !100}
!120 = metadata !{i32 18, i32 38, metadata !110, metadata !100}
!121 = metadata !{i32 786688, metadata !110, metadata !"i", metadata !54, i32 18, metadata !74, i32 0, metadata !100} ; [ DW_TAG_auto_variable ]
!122 = metadata !{i32 24, i32 12, metadata !111, metadata !100}
!123 = metadata !{i32 786689, metadata !124, metadata !"__x", metadata !126, i32 16777432, metadata !56, i32 0, metadata !122} ; [ DW_TAG_arg_variable ]
!124 = metadata !{i32 786478, i32 0, metadata !125, metadata !"exp", metadata !"exp", metadata !"_ZSt3expf", metadata !126, i32 216, metadata !127, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !85, i32 217} ; [ DW_TAG_subprogram ]
!125 = metadata !{i32 786489, null, metadata !"std", metadata !126, i32 76} ; [ DW_TAG_namespace ]
!126 = metadata !{i32 786473, metadata !"/opt/Xilinx/Vivado_HLS/2015.4/lnx64/tools/gcc/lib/gcc/x86_64-unknown-linux-gnu/4.6.3/../../../../include/c++/4.6.3/cmath", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace", null} ; [ DW_TAG_file_type ]
!127 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !128, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!128 = metadata !{metadata !56, metadata !56}
!129 = metadata !{i32 216, i32 13, metadata !124, metadata !122}
!130 = metadata !{i32 217, i32 12, metadata !131, metadata !122}
!131 = metadata !{i32 786443, metadata !124, i32 217, i32 3, metadata !126, i32 59} ; [ DW_TAG_lexical_block ]
!132 = metadata !{i32 129, i32 9, metadata !101, null}
!133 = metadata !{i32 786688, metadata !95, metadata !"m", metadata !54, i32 123, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!134 = metadata !{i32 130, i32 5, metadata !101, null}
!135 = metadata !{i32 126, i32 45, metadata !94, null}
!136 = metadata !{i32 786688, metadata !94, metadata !"i", metadata !54, i32 126, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!137 = metadata !{i32 137, i32 45, metadata !106, null}
!138 = metadata !{i32 137, i32 61, metadata !105, null}
!139 = metadata !{i32 139, i32 6, metadata !105, null}
!140 = metadata !{i32 140, i32 6, metadata !105, null}
!141 = metadata !{i32 141, i32 49, metadata !104, null}
!142 = metadata !{i32 141, i32 65, metadata !103, null}
!143 = metadata !{i32 144, i32 10, metadata !103, null}
!144 = metadata !{i32 141, i32 59, metadata !104, null}
!145 = metadata !{i32 786688, metadata !104, metadata !"j", metadata !54, i32 141, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!146 = metadata !{i32 146, i32 5, metadata !105, null}
!147 = metadata !{i32 137, i32 55, metadata !106, null}
!148 = metadata !{i32 786688, metadata !106, metadata !"i", metadata !54, i32 137, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!149 = metadata !{i32 148, i32 36, metadata !150, null}
!150 = metadata !{i32 786443, metadata !95, i32 148, i32 13, metadata !54, i32 30} ; [ DW_TAG_lexical_block ]
!151 = metadata !{i32 148, i32 52, metadata !152, null}
!152 = metadata !{i32 786443, metadata !150, i32 148, i32 51, metadata !54, i32 31} ; [ DW_TAG_lexical_block ]
!153 = metadata !{i32 150, i32 6, metadata !152, null}
!154 = metadata !{i32 786688, metadata !95, metadata !"sigma2", metadata !54, i32 136, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!155 = metadata !{i32 148, i32 46, metadata !150, null}
!156 = metadata !{i32 786688, metadata !150, metadata !"i", metadata !54, i32 148, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!157 = metadata !{i32 152, i32 5, metadata !95, null}
!158 = metadata !{i32 154, i32 40, metadata !95, null}
!159 = metadata !{i32 786688, metadata !95, metadata !"q", metadata !54, i32 154, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!160 = metadata !{i32 155, i32 35, metadata !95, null}
!161 = metadata !{i32 786688, metadata !95, metadata !"r", metadata !54, i32 155, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!162 = metadata !{i32 158, i32 42, metadata !163, null}
!163 = metadata !{i32 786443, metadata !95, i32 158, i32 19, metadata !54, i32 32} ; [ DW_TAG_lexical_block ]
!164 = metadata !{i32 158, i32 58, metadata !165, null}
!165 = metadata !{i32 786443, metadata !163, i32 158, i32 57, metadata !54, i32 33} ; [ DW_TAG_lexical_block ]
!166 = metadata !{i32 160, i32 6, metadata !165, null}
!167 = metadata !{i32 786688, metadata !95, metadata !"gamma", metadata !54, i32 156, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!168 = metadata !{i32 161, i32 6, metadata !165, null}
!169 = metadata !{i32 158, i32 52, metadata !163, null}
!170 = metadata !{i32 786688, metadata !163, metadata !"i", metadata !54, i32 158, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!171 = metadata !{i32 168, i32 5, metadata !95, null}
!172 = metadata !{i32 171, i32 43, metadata !173, null}
!173 = metadata !{i32 786443, metadata !95, i32 171, i32 20, metadata !54, i32 34} ; [ DW_TAG_lexical_block ]
!174 = metadata !{i32 176, i32 13, metadata !175, null}
!175 = metadata !{i32 786443, metadata !176, i32 173, i32 60, metadata !54, i32 37} ; [ DW_TAG_lexical_block ]
!176 = metadata !{i32 786443, metadata !177, i32 173, i32 21, metadata !54, i32 36} ; [ DW_TAG_lexical_block ]
!177 = metadata !{i32 786443, metadata !173, i32 171, i32 59, metadata !54, i32 35} ; [ DW_TAG_lexical_block ]
!178 = metadata !{i32 187, i32 4, metadata !179, null}
!179 = metadata !{i32 786443, metadata !180, i32 182, i32 60, metadata !54, i32 41} ; [ DW_TAG_lexical_block ]
!180 = metadata !{i32 786443, metadata !181, i32 182, i32 21, metadata !54, i32 40} ; [ DW_TAG_lexical_block ]
!181 = metadata !{i32 786443, metadata !182, i32 180, i32 59, metadata !54, i32 39} ; [ DW_TAG_lexical_block ]
!182 = metadata !{i32 786443, metadata !95, i32 180, i32 20, metadata !54, i32 38} ; [ DW_TAG_lexical_block ]
!183 = metadata !{i32 180, i32 43, metadata !182, null}
!184 = metadata !{i32 171, i32 60, metadata !177, null}
!185 = metadata !{i32 173, i32 44, metadata !176, null}
!186 = metadata !{i32 173, i32 61, metadata !175, null}
!187 = metadata !{i32 173, i32 55, metadata !176, null}
!188 = metadata !{i32 786688, metadata !176, metadata !"j", metadata !54, i32 173, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!189 = metadata !{i32 178, i32 5, metadata !177, null}
!190 = metadata !{i32 171, i32 54, metadata !173, null}
!191 = metadata !{i32 786688, metadata !173, metadata !"i", metadata !54, i32 171, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!192 = metadata !{i32 180, i32 60, metadata !181, null}
!193 = metadata !{i32 184, i32 37, metadata !179, null}
!194 = metadata !{i32 182, i32 44, metadata !180, null}
!195 = metadata !{i32 182, i32 61, metadata !179, null}
!196 = metadata !{i32 786688, metadata !179, metadata !"ti", metadata !54, i32 184, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!197 = metadata !{i32 185, i32 34, metadata !179, null}
!198 = metadata !{i32 786688, metadata !179, metadata !"tj", metadata !54, i32 185, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!199 = metadata !{i32 182, i32 55, metadata !180, null}
!200 = metadata !{i32 786688, metadata !180, metadata !"j", metadata !54, i32 182, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!201 = metadata !{i32 189, i32 5, metadata !181, null}
!202 = metadata !{i32 180, i32 54, metadata !182, null}
!203 = metadata !{i32 786688, metadata !182, metadata !"i", metadata !54, i32 180, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!204 = metadata !{i32 215, i32 33, metadata !205, metadata !210}
!205 = metadata !{i32 786443, metadata !206, i32 215, i32 10, metadata !54, i32 43} ; [ DW_TAG_lexical_block ]
!206 = metadata !{i32 786443, metadata !207, i32 214, i32 54, metadata !54, i32 42} ; [ DW_TAG_lexical_block ]
!207 = metadata !{i32 786478, i32 0, metadata !54, metadata !"copyBV", metadata !"copyBV", metadata !"_Z6copyBVPKfj", metadata !54, i32 214, metadata !208, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !85, i32 214} ; [ DW_TAG_subprogram ]
!208 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !209, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!209 = metadata !{null, metadata !83, metadata !74}
!210 = metadata !{i32 208, i32 5, metadata !95, null}
!211 = metadata !{i32 215, i32 49, metadata !212, metadata !210}
!212 = metadata !{i32 786443, metadata !205, i32 215, i32 48, metadata !54, i32 44} ; [ DW_TAG_lexical_block ]
!213 = metadata !{i32 217, i32 3, metadata !212, metadata !210}
!214 = metadata !{i32 215, i32 43, metadata !205, metadata !210}
!215 = metadata !{i32 786688, metadata !205, metadata !"i", metadata !54, i32 215, metadata !74, i32 0, metadata !210} ; [ DW_TAG_auto_variable ]
!216 = metadata !{i32 108, i32 77, metadata !217, metadata !221}
!217 = metadata !{i32 786443, metadata !218, i32 106, i32 31, metadata !54, i32 19} ; [ DW_TAG_lexical_block ]
!218 = metadata !{i32 786478, i32 0, metadata !54, metadata !"getMinKLApprox", metadata !"getMinKLApprox", metadata !"_Z14getMinKLApproxv", metadata !54, i32 106, metadata !219, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !85, i32 106} ; [ DW_TAG_subprogram ]
!219 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !220, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!220 = metadata !{metadata !74}
!221 = metadata !{i32 210, i32 23, metadata !95, null}
!222 = metadata !{i32 786688, metadata !217, metadata !"minScore", metadata !54, i32 108, metadata !56, i32 0, metadata !221} ; [ DW_TAG_auto_variable ]
!223 = metadata !{i32 110, i32 28, metadata !224, metadata !221}
!224 = metadata !{i32 786443, metadata !217, i32 110, i32 5, metadata !54, i32 20} ; [ DW_TAG_lexical_block ]
!225 = metadata !{i32 111, i32 79, metadata !226, metadata !221}
!226 = metadata !{i32 786443, metadata !224, i32 110, i32 48, metadata !54, i32 21} ; [ DW_TAG_lexical_block ]
!227 = metadata !{i32 786688, metadata !226, metadata !"tScore", metadata !54, i32 111, metadata !56, i32 0, metadata !221} ; [ DW_TAG_auto_variable ]
!228 = metadata !{i32 113, i32 9, metadata !226, metadata !221}
!229 = metadata !{i32 114, i32 13, metadata !230, metadata !221}
!230 = metadata !{i32 786443, metadata !226, i32 113, i32 32, metadata !54, i32 22} ; [ DW_TAG_lexical_block ]
!231 = metadata !{i32 786688, metadata !217, metadata !"index", metadata !54, i32 107, metadata !78, i32 0, metadata !221} ; [ DW_TAG_auto_variable ]
!232 = metadata !{i32 115, i32 13, metadata !230, metadata !221}
!233 = metadata !{i32 110, i32 43, metadata !224, metadata !221}
!234 = metadata !{i32 786688, metadata !224, metadata !"i", metadata !54, i32 110, metadata !74, i32 0, metadata !221} ; [ DW_TAG_auto_variable ]
!235 = metadata !{i32 786688, metadata !95, metadata !"index", metadata !54, i32 210, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!236 = metadata !{i32 211, i32 2, metadata !95, null}
!237 = metadata !{i32 212, i32 1, metadata !95, null}
!238 = metadata !{i32 786689, metadata !239, metadata !"pM", null, i32 27, metadata !55, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!239 = metadata !{i32 786478, i32 0, metadata !54, metadata !"swapRowAndColumn", metadata !"swapRowAndColumn", metadata !"_Z16swapRowAndColumnPfjj", metadata !54, i32 27, metadata !240, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !85, i32 27} ; [ DW_TAG_subprogram ]
!240 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !241, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!241 = metadata !{null, metadata !242, metadata !74, metadata !74}
!242 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !56} ; [ DW_TAG_pointer_type ]
!243 = metadata !{i32 27, i32 29, metadata !239, null}
!244 = metadata !{i32 786689, metadata !239, metadata !"rowA", metadata !54, i32 33554459, metadata !74, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!245 = metadata !{i32 27, i32 65, metadata !239, null}
!246 = metadata !{i32 30, i32 40, metadata !247, null}
!247 = metadata !{i32 786443, metadata !248, i32 28, i32 48, metadata !54, i32 5} ; [ DW_TAG_lexical_block ]
!248 = metadata !{i32 786443, metadata !249, i32 28, i32 5, metadata !54, i32 4} ; [ DW_TAG_lexical_block ]
!249 = metadata !{i32 786443, metadata !239, i32 27, i32 91, metadata !54, i32 3} ; [ DW_TAG_lexical_block ]
!250 = metadata !{i32 28, i32 28, metadata !248, null}
!251 = metadata !{i32 786688, metadata !247, metadata !"temp", metadata !54, i32 30, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!252 = metadata !{i32 31, i32 9, metadata !247, null}
!253 = metadata !{i32 32, i32 9, metadata !247, null}
!254 = metadata !{i32 28, i32 43, metadata !248, null}
!255 = metadata !{i32 786688, metadata !248, metadata !"i", metadata !54, i32 28, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!256 = metadata !{i32 35, i32 28, metadata !257, null}
!257 = metadata !{i32 786443, metadata !249, i32 35, i32 5, metadata !54, i32 6} ; [ DW_TAG_lexical_block ]
!258 = metadata !{i32 37, i32 40, metadata !259, null}
!259 = metadata !{i32 786443, metadata !257, i32 35, i32 48, metadata !54, i32 7} ; [ DW_TAG_lexical_block ]
!260 = metadata !{i32 786688, metadata !259, metadata !"temp", metadata !54, i32 37, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!261 = metadata !{i32 38, i32 9, metadata !259, null}
!262 = metadata !{i32 39, i32 9, metadata !259, null}
!263 = metadata !{i32 35, i32 43, metadata !257, null}
!264 = metadata !{i32 786688, metadata !257, metadata !"i", metadata !54, i32 35, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!265 = metadata !{i32 41, i32 1, metadata !249, null}
!266 = metadata !{metadata !267}
!267 = metadata !{i32 0, i32 31, metadata !268}
!268 = metadata !{metadata !269}
!269 = metadata !{metadata !"pX", metadata !270, metadata !"float", i32 0, i32 31}
!270 = metadata !{metadata !271}
!271 = metadata !{i32 0, i32 12, i32 1}
!272 = metadata !{metadata !273}
!273 = metadata !{i32 0, i32 31, metadata !274}
!274 = metadata !{metadata !275}
!275 = metadata !{metadata !"pY", metadata !276, metadata !"float", i32 0, i32 31}
!276 = metadata !{metadata !277}
!277 = metadata !{i32 0, i32 0, i32 0}
!278 = metadata !{metadata !279}
!279 = metadata !{i32 0, i32 0, metadata !280}
!280 = metadata !{metadata !281}
!281 = metadata !{metadata !"pPredict", metadata !276, metadata !"bool", i32 0, i32 0}
!282 = metadata !{metadata !283}
!283 = metadata !{i32 0, i32 0, metadata !284}
!284 = metadata !{metadata !285}
!285 = metadata !{metadata !"pReset", metadata !276, metadata !"bool", i32 0, i32 0}
!286 = metadata !{metadata !287}
!287 = metadata !{i32 0, i32 31, metadata !288}
!288 = metadata !{metadata !289}
!289 = metadata !{metadata !"return", metadata !290, metadata !"float", i32 0, i32 31}
!290 = metadata !{metadata !291}
!291 = metadata !{i32 0, i32 1, i32 0}
!292 = metadata !{i32 786689, metadata !293, metadata !"pX", null, i32 221, metadata !87, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!293 = metadata !{i32 786478, i32 0, metadata !54, metadata !"projection_gp", metadata !"projection_gp", metadata !"_Z13projection_gpPKffbb", metadata !54, i32 221, metadata !294, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !85, i32 221} ; [ DW_TAG_subprogram ]
!294 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !295, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!295 = metadata !{metadata !56, metadata !83, metadata !84, metadata !296, metadata !296}
!296 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !297} ; [ DW_TAG_const_type ]
!297 = metadata !{i32 786468, null, metadata !"bool", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ]
!298 = metadata !{i32 221, i32 33, metadata !293, null}
!299 = metadata !{i32 786689, metadata !293, metadata !"pY", metadata !54, i32 33554653, metadata !84, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!300 = metadata !{i32 221, i32 53, metadata !293, null}
!301 = metadata !{i32 786689, metadata !293, metadata !"pPredict", metadata !54, i32 50331869, metadata !296, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!302 = metadata !{i32 221, i32 68, metadata !293, null}
!303 = metadata !{i32 786689, metadata !293, metadata !"pReset", metadata !54, i32 67109085, metadata !296, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!304 = metadata !{i32 221, i32 89, metadata !293, null}
!305 = metadata !{i32 223, i32 1, metadata !306, null}
!306 = metadata !{i32 786443, metadata !293, i32 221, i32 97, metadata !54, i32 45} ; [ DW_TAG_lexical_block ]
!307 = metadata !{i32 224, i32 1, metadata !306, null}
!308 = metadata !{i32 225, i32 1, metadata !306, null}
!309 = metadata !{i32 226, i32 1, metadata !306, null}
!310 = metadata !{i32 228, i32 2, metadata !306, null}
!311 = metadata !{i32 229, i32 37, metadata !312, null}
!312 = metadata !{i32 786443, metadata !313, i32 229, i32 14, metadata !54, i32 47} ; [ DW_TAG_lexical_block ]
!313 = metadata !{i32 786443, metadata !306, i32 228, i32 14, metadata !54, i32 46} ; [ DW_TAG_lexical_block ]
!314 = metadata !{i32 229, i32 58, metadata !315, null}
!315 = metadata !{i32 786443, metadata !312, i32 229, i32 57, metadata !54, i32 48} ; [ DW_TAG_lexical_block ]
!316 = metadata !{i32 234, i32 6, metadata !317, null}
!317 = metadata !{i32 786443, metadata !318, i32 233, i32 17, metadata !54, i32 51} ; [ DW_TAG_lexical_block ]
!318 = metadata !{i32 786443, metadata !319, i32 231, i32 59, metadata !54, i32 50} ; [ DW_TAG_lexical_block ]
!319 = metadata !{i32 786443, metadata !315, i32 231, i32 16, metadata !54, i32 49} ; [ DW_TAG_lexical_block ]
!320 = metadata !{i32 235, i32 6, metadata !317, null}
!321 = metadata !{i32 231, i32 39, metadata !319, null}
!322 = metadata !{i32 231, i32 60, metadata !318, null}
!323 = metadata !{i32 233, i32 5, metadata !318, null}
!324 = metadata !{i32 237, i32 6, metadata !325, null}
!325 = metadata !{i32 786443, metadata !318, i32 236, i32 12, metadata !54, i32 52} ; [ DW_TAG_lexical_block ]
!326 = metadata !{i32 231, i32 54, metadata !319, null}
!327 = metadata !{i32 786688, metadata !319, metadata !"j", metadata !54, i32 231, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!328 = metadata !{i32 241, i32 4, metadata !315, null}
!329 = metadata !{i32 242, i32 3, metadata !315, null}
!330 = metadata !{i32 229, i32 52, metadata !312, null}
!331 = metadata !{i32 786688, metadata !312, metadata !"i", metadata !54, i32 229, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!332 = metadata !{i32 245, i32 2, metadata !306, null}
!333 = metadata !{i32 246, i32 3, metadata !334, null}
!334 = metadata !{i32 786443, metadata !306, i32 245, i32 17, metadata !54, i32 53} ; [ DW_TAG_lexical_block ]
!335 = metadata !{i32 247, i32 4, metadata !336, null}
!336 = metadata !{i32 786443, metadata !334, i32 246, i32 20, metadata !54, i32 54} ; [ DW_TAG_lexical_block ]
!337 = metadata !{i32 248, i32 3, metadata !336, null}
!338 = metadata !{i32 215, i32 33, metadata !205, metadata !339}
!339 = metadata !{i32 249, i32 4, metadata !340, null}
!340 = metadata !{i32 786443, metadata !334, i32 248, i32 10, metadata !54, i32 55} ; [ DW_TAG_lexical_block ]
!341 = metadata !{i32 215, i32 49, metadata !212, metadata !339}
!342 = metadata !{i32 217, i32 3, metadata !212, metadata !339}
!343 = metadata !{i32 215, i32 43, metadata !205, metadata !339}
!344 = metadata !{i32 786688, metadata !205, metadata !"i", metadata !54, i32 215, metadata !74, i32 0, metadata !339} ; [ DW_TAG_auto_variable ]
!345 = metadata !{i32 250, i32 4, metadata !340, null}
!346 = metadata !{i32 252, i32 3, metadata !334, null}
!347 = metadata !{i32 16, i32 42, metadata !97, metadata !348}
!348 = metadata !{i32 257, i32 11, metadata !349, null}
!349 = metadata !{i32 786443, metadata !350, i32 255, i32 52, metadata !54, i32 58} ; [ DW_TAG_lexical_block ]
!350 = metadata !{i32 786443, metadata !351, i32 255, i32 14, metadata !54, i32 57} ; [ DW_TAG_lexical_block ]
!351 = metadata !{i32 786443, metadata !306, i32 253, i32 9, metadata !54, i32 56} ; [ DW_TAG_lexical_block ]
!352 = metadata !{i32 255, i32 37, metadata !350, null}
!353 = metadata !{i32 255, i32 53, metadata !349, null}
!354 = metadata !{i32 786689, metadata !97, metadata !"pX2", null, i32 16, metadata !87, i32 0, metadata !348} ; [ DW_TAG_arg_variable ]
!355 = metadata !{i32 18, i32 28, metadata !110, metadata !348}
!356 = metadata !{i32 18, i32 44, metadata !113, metadata !348}
!357 = metadata !{i32 19, i32 1, metadata !113, metadata !348}
!358 = metadata !{i32 20, i32 31, metadata !113, metadata !348}
!359 = metadata !{i32 786688, metadata !113, metadata !"val", metadata !54, i32 20, metadata !56, i32 0, metadata !348} ; [ DW_TAG_auto_variable ]
!360 = metadata !{i32 21, i32 9, metadata !113, metadata !348}
!361 = metadata !{i32 786688, metadata !111, metadata !"sum", metadata !54, i32 17, metadata !56, i32 0, metadata !348} ; [ DW_TAG_auto_variable ]
!362 = metadata !{i32 22, i32 5, metadata !113, metadata !348}
!363 = metadata !{i32 18, i32 38, metadata !110, metadata !348}
!364 = metadata !{i32 786688, metadata !110, metadata !"i", metadata !54, i32 18, metadata !74, i32 0, metadata !348} ; [ DW_TAG_auto_variable ]
!365 = metadata !{i32 24, i32 12, metadata !111, metadata !348}
!366 = metadata !{i32 786689, metadata !124, metadata !"__x", metadata !126, i32 16777432, metadata !56, i32 0, metadata !365} ; [ DW_TAG_arg_variable ]
!367 = metadata !{i32 216, i32 13, metadata !124, metadata !365}
!368 = metadata !{i32 217, i32 12, metadata !131, metadata !365}
!369 = metadata !{i32 786688, metadata !351, metadata !"sum", metadata !54, i32 254, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!370 = metadata !{i32 258, i32 3, metadata !349, null}
!371 = metadata !{i32 255, i32 47, metadata !350, null}
!372 = metadata !{i32 786688, metadata !350, metadata !"i", metadata !54, i32 255, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!373 = metadata !{i32 262, i32 1, metadata !306, null}
!374 = metadata !{i32 786689, metadata !375, metadata !"pIndex", metadata !54, i32 16777259, metadata !74, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!375 = metadata !{i32 786478, i32 0, metadata !54, metadata !"deleteBV", metadata !"deleteBV", metadata !"_Z8deleteBVj", metadata !54, i32 43, metadata !376, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32)* @deleteBV, null, null, metadata !85, i32 43} ; [ DW_TAG_subprogram ]
!376 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !377, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!377 = metadata !{null, metadata !74}
!378 = metadata !{i32 43, i32 29, metadata !375, null}
!379 = metadata !{i32 47, i32 31, metadata !380, null}
!380 = metadata !{i32 786443, metadata !375, i32 43, i32 37, metadata !54, i32 8} ; [ DW_TAG_lexical_block ]
!381 = metadata !{i32 786688, metadata !380, metadata !"temp", metadata !54, i32 47, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!382 = metadata !{i32 48, i32 5, metadata !380, null}
!383 = metadata !{i32 49, i32 5, metadata !380, null}
!384 = metadata !{i32 51, i32 5, metadata !380, null}
!385 = metadata !{i32 52, i32 5, metadata !380, null}
!386 = metadata !{i32 56, i32 2, metadata !387, null}
!387 = metadata !{i32 786443, metadata !388, i32 54, i32 63, metadata !54, i32 10} ; [ DW_TAG_lexical_block ]
!388 = metadata !{i32 786443, metadata !380, i32 54, i32 25, metadata !54, i32 9} ; [ DW_TAG_lexical_block ]
!389 = metadata !{i32 54, i32 48, metadata !388, null}
!390 = metadata !{i32 54, i32 64, metadata !387, null}
!391 = metadata !{i32 55, i32 1, metadata !387, null}
!392 = metadata !{i32 57, i32 9, metadata !387, null}
!393 = metadata !{i32 58, i32 9, metadata !387, null}
!394 = metadata !{i32 59, i32 5, metadata !387, null}
!395 = metadata !{i32 54, i32 58, metadata !388, null}
!396 = metadata !{i32 786688, metadata !388, metadata !"i", metadata !54, i32 54, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!397 = metadata !{i32 62, i32 32, metadata !380, null}
!398 = metadata !{i32 786688, metadata !380, metadata !"alphaStar", metadata !54, i32 62, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!399 = metadata !{i32 63, i32 24, metadata !380, null}
!400 = metadata !{i32 786688, metadata !380, metadata !"cStar", metadata !54, i32 63, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!401 = metadata !{i32 81, i32 45, metadata !402, null}
!402 = metadata !{i32 786443, metadata !403, i32 76, i32 62, metadata !54, i32 16} ; [ DW_TAG_lexical_block ]
!403 = metadata !{i32 786443, metadata !404, i32 76, i32 24, metadata !54, i32 15} ; [ DW_TAG_lexical_block ]
!404 = metadata !{i32 786443, metadata !405, i32 74, i32 60, metadata !54, i32 14} ; [ DW_TAG_lexical_block ]
!405 = metadata !{i32 786443, metadata !380, i32 74, i32 23, metadata !54, i32 13} ; [ DW_TAG_lexical_block ]
!406 = metadata !{i32 786688, metadata !380, metadata !"qStar", metadata !54, i32 64, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!407 = metadata !{i32 64, i32 24, metadata !380, null}
!408 = metadata !{i32 65, i32 5, metadata !380, null}
!409 = metadata !{i32 67, i32 44, metadata !410, null}
!410 = metadata !{i32 786443, metadata !380, i32 67, i32 21, metadata !54, i32 11} ; [ DW_TAG_lexical_block ]
!411 = metadata !{i32 67, i32 60, metadata !412, null}
!412 = metadata !{i32 786443, metadata !410, i32 67, i32 59, metadata !54, i32 12} ; [ DW_TAG_lexical_block ]
!413 = metadata !{i32 69, i32 35, metadata !412, null}
!414 = metadata !{i32 786688, metadata !412, metadata !"a", metadata !54, i32 69, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!415 = metadata !{i32 70, i32 9, metadata !412, null}
!416 = metadata !{i32 67, i32 54, metadata !410, null}
!417 = metadata !{i32 786688, metadata !410, metadata !"i", metadata !54, i32 67, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!418 = metadata !{i32 72, i32 5, metadata !380, null}
!419 = metadata !{i32 74, i32 45, metadata !405, null}
!420 = metadata !{i32 80, i32 38, metadata !402, null}
!421 = metadata !{i32 74, i32 61, metadata !404, null}
!422 = metadata !{i32 78, i32 39, metadata !402, null}
!423 = metadata !{i32 84, i32 13, metadata !402, null}
!424 = metadata !{i32 76, i32 47, metadata !403, null}
!425 = metadata !{i32 76, i32 63, metadata !402, null}
!426 = metadata !{i32 786688, metadata !402, metadata !"a", metadata !54, i32 78, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!427 = metadata !{i32 79, i32 39, metadata !402, null}
!428 = metadata !{i32 786688, metadata !402, metadata !"b", metadata !54, i32 79, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!429 = metadata !{i32 786688, metadata !402, metadata !"c", metadata !54, i32 80, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!430 = metadata !{i32 786688, metadata !402, metadata !"temp", metadata !54, i32 81, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!431 = metadata !{i32 90, i32 13, metadata !402, null}
!432 = metadata !{i32 76, i32 57, metadata !403, null}
!433 = metadata !{i32 786688, metadata !403, metadata !"j", metadata !54, i32 76, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!434 = metadata !{i32 92, i32 5, metadata !404, null}
!435 = metadata !{i32 74, i32 55, metadata !405, null}
!436 = metadata !{i32 786688, metadata !405, metadata !"i", metadata !54, i32 74, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!437 = metadata !{i32 94, i32 48, metadata !438, null}
!438 = metadata !{i32 786443, metadata !380, i32 94, i32 25, metadata !54, i32 17} ; [ DW_TAG_lexical_block ]
!439 = metadata !{i32 96, i32 35, metadata !440, null}
!440 = metadata !{i32 786443, metadata !438, i32 94, i32 64, metadata !54, i32 18} ; [ DW_TAG_lexical_block ]
!441 = metadata !{i32 94, i32 65, metadata !440, null}
!442 = metadata !{i32 786688, metadata !440, metadata !"a", metadata !54, i32 96, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!443 = metadata !{i32 97, i32 35, metadata !440, null}
!444 = metadata !{i32 786688, metadata !440, metadata !"b", metadata !54, i32 97, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!445 = metadata !{i32 99, i32 6, metadata !440, null}
!446 = metadata !{i32 100, i32 9, metadata !440, null}
!447 = metadata !{i32 101, i32 9, metadata !440, null}
!448 = metadata !{i32 102, i32 9, metadata !440, null}
!449 = metadata !{i32 94, i32 59, metadata !438, null}
!450 = metadata !{i32 786688, metadata !438, metadata !"i", metadata !54, i32 94, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!451 = metadata !{i32 104, i32 1, metadata !380, null}
