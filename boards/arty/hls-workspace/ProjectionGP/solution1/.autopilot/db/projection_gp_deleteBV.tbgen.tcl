set moduleName projection_gp_deleteBV
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {projection_gp_deleteBV}
set C_modelType { void 0 }
set C_modelArgList { 
	{ pIndex int 32 regular  }
	{ alpha float 32 regular {array 41 { 2 3 } 1 1 } {global 2}  }
	{ C float 32 regular {array 1681 { 2 2 } 1 1 } {global 2}  }
	{ Q float 32 regular {array 1681 { 2 2 } 1 1 } {global 2}  }
	{ basisVectors float 32 regular {array 533 { 2 2 } 1 1 } {global 2}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "pIndex", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "alpha", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "alpha","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 40,"step" : 1}]}]}], "extern" : 0} , 
 	{ "Name" : "C", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "C","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1680,"step" : 1}]}]}], "extern" : 0} , 
 	{ "Name" : "Q", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "Q","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1680,"step" : 1}]}]}], "extern" : 0} , 
 	{ "Name" : "basisVectors", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "basisVectors","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 532,"step" : 1}]}]}], "extern" : 0} ]}
# RTL Port declarations: 
set portNum 42
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ pIndex sc_in sc_lv 32 signal 0 } 
	{ alpha_address0 sc_out sc_lv 6 signal 1 } 
	{ alpha_ce0 sc_out sc_logic 1 signal 1 } 
	{ alpha_we0 sc_out sc_logic 1 signal 1 } 
	{ alpha_d0 sc_out sc_lv 32 signal 1 } 
	{ alpha_q0 sc_in sc_lv 32 signal 1 } 
	{ C_address0 sc_out sc_lv 11 signal 2 } 
	{ C_ce0 sc_out sc_logic 1 signal 2 } 
	{ C_we0 sc_out sc_logic 1 signal 2 } 
	{ C_d0 sc_out sc_lv 32 signal 2 } 
	{ C_q0 sc_in sc_lv 32 signal 2 } 
	{ C_address1 sc_out sc_lv 11 signal 2 } 
	{ C_ce1 sc_out sc_logic 1 signal 2 } 
	{ C_we1 sc_out sc_logic 1 signal 2 } 
	{ C_d1 sc_out sc_lv 32 signal 2 } 
	{ C_q1 sc_in sc_lv 32 signal 2 } 
	{ Q_address0 sc_out sc_lv 11 signal 3 } 
	{ Q_ce0 sc_out sc_logic 1 signal 3 } 
	{ Q_we0 sc_out sc_logic 1 signal 3 } 
	{ Q_d0 sc_out sc_lv 32 signal 3 } 
	{ Q_q0 sc_in sc_lv 32 signal 3 } 
	{ Q_address1 sc_out sc_lv 11 signal 3 } 
	{ Q_ce1 sc_out sc_logic 1 signal 3 } 
	{ Q_we1 sc_out sc_logic 1 signal 3 } 
	{ Q_d1 sc_out sc_lv 32 signal 3 } 
	{ Q_q1 sc_in sc_lv 32 signal 3 } 
	{ basisVectors_address0 sc_out sc_lv 10 signal 4 } 
	{ basisVectors_ce0 sc_out sc_logic 1 signal 4 } 
	{ basisVectors_we0 sc_out sc_logic 1 signal 4 } 
	{ basisVectors_d0 sc_out sc_lv 32 signal 4 } 
	{ basisVectors_q0 sc_in sc_lv 32 signal 4 } 
	{ basisVectors_address1 sc_out sc_lv 10 signal 4 } 
	{ basisVectors_ce1 sc_out sc_logic 1 signal 4 } 
	{ basisVectors_we1 sc_out sc_logic 1 signal 4 } 
	{ basisVectors_d1 sc_out sc_lv 32 signal 4 } 
	{ basisVectors_q1 sc_in sc_lv 32 signal 4 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "pIndex", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "pIndex", "role": "default" }} , 
 	{ "name": "alpha_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "alpha", "role": "address0" }} , 
 	{ "name": "alpha_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "alpha", "role": "ce0" }} , 
 	{ "name": "alpha_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "alpha", "role": "we0" }} , 
 	{ "name": "alpha_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "alpha", "role": "d0" }} , 
 	{ "name": "alpha_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "alpha", "role": "q0" }} , 
 	{ "name": "C_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "C", "role": "address0" }} , 
 	{ "name": "C_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "C", "role": "ce0" }} , 
 	{ "name": "C_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "C", "role": "we0" }} , 
 	{ "name": "C_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "C", "role": "d0" }} , 
 	{ "name": "C_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "C", "role": "q0" }} , 
 	{ "name": "C_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "C", "role": "address1" }} , 
 	{ "name": "C_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "C", "role": "ce1" }} , 
 	{ "name": "C_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "C", "role": "we1" }} , 
 	{ "name": "C_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "C", "role": "d1" }} , 
 	{ "name": "C_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "C", "role": "q1" }} , 
 	{ "name": "Q_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "Q", "role": "address0" }} , 
 	{ "name": "Q_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "Q", "role": "ce0" }} , 
 	{ "name": "Q_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "Q", "role": "we0" }} , 
 	{ "name": "Q_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "Q", "role": "d0" }} , 
 	{ "name": "Q_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "Q", "role": "q0" }} , 
 	{ "name": "Q_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "Q", "role": "address1" }} , 
 	{ "name": "Q_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "Q", "role": "ce1" }} , 
 	{ "name": "Q_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "Q", "role": "we1" }} , 
 	{ "name": "Q_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "Q", "role": "d1" }} , 
 	{ "name": "Q_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "Q", "role": "q1" }} , 
 	{ "name": "basisVectors_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "basisVectors", "role": "address0" }} , 
 	{ "name": "basisVectors_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "basisVectors", "role": "ce0" }} , 
 	{ "name": "basisVectors_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "basisVectors", "role": "we0" }} , 
 	{ "name": "basisVectors_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "basisVectors", "role": "d0" }} , 
 	{ "name": "basisVectors_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "basisVectors", "role": "q0" }} , 
 	{ "name": "basisVectors_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "basisVectors", "role": "address1" }} , 
 	{ "name": "basisVectors_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "basisVectors", "role": "ce1" }} , 
 	{ "name": "basisVectors_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "basisVectors", "role": "we1" }} , 
 	{ "name": "basisVectors_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "basisVectors", "role": "d1" }} , 
 	{ "name": "basisVectors_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "basisVectors", "role": "q1" }}  ]}
set Spec2ImplPortList { 
	pIndex { ap_none {  { pIndex in_data 0 32 } } }
	alpha { ap_memory {  { alpha_address0 mem_address 1 6 }  { alpha_ce0 mem_ce 1 1 }  { alpha_we0 mem_we 1 1 }  { alpha_d0 mem_din 1 32 }  { alpha_q0 mem_dout 0 32 } } }
	C { ap_memory {  { C_address0 mem_address 1 11 }  { C_ce0 mem_ce 1 1 }  { C_we0 mem_we 1 1 }  { C_d0 mem_din 1 32 }  { C_q0 mem_dout 0 32 }  { C_address1 mem_address 1 11 }  { C_ce1 mem_ce 1 1 }  { C_we1 mem_we 1 1 }  { C_d1 mem_din 1 32 }  { C_q1 mem_dout 0 32 } } }
	Q { ap_memory {  { Q_address0 mem_address 1 11 }  { Q_ce0 mem_ce 1 1 }  { Q_we0 mem_we 1 1 }  { Q_d0 mem_din 1 32 }  { Q_q0 mem_dout 0 32 }  { Q_address1 mem_address 1 11 }  { Q_ce1 mem_ce 1 1 }  { Q_we1 mem_we 1 1 }  { Q_d1 mem_din 1 32 }  { Q_q1 mem_dout 0 32 } } }
	basisVectors { ap_memory {  { basisVectors_address0 mem_address 1 10 }  { basisVectors_ce0 mem_ce 1 1 }  { basisVectors_we0 mem_we 1 1 }  { basisVectors_d0 mem_din 1 32 }  { basisVectors_q0 mem_dout 0 32 }  { basisVectors_address1 mem_address 1 10 }  { basisVectors_ce1 mem_ce 1 1 }  { basisVectors_we1 mem_we 1 1 }  { basisVectors_d1 mem_din 1 32 }  { basisVectors_q1 mem_dout 0 32 } } }
}
