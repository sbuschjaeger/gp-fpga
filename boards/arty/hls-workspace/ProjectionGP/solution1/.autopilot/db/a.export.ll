; ModuleID = '/home/buschjae/projects/masterarbeit/GP-FPGA/boards/arty/hls-workspace/ProjectionGP/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@s = global [41 x float] zeroinitializer, align 16
@projection_gp_str = internal unnamed_addr constant [14 x i8] c"projection_gp\00"
@k = global [40 x float] zeroinitializer, align 16
@e = global [41 x float] zeroinitializer, align 16
@bvCnt = global i32 0, align 4
@basisVectors = global [533 x float] zeroinitializer, align 16
@alpha = global [41 x float] zeroinitializer, align 16
@RAM_1P_str = internal unnamed_addr constant [7 x i8] c"RAM_1P\00"
@Q = global [1681 x float] zeroinitializer, align 16
@PREDICTION_L_str = internal unnamed_addr constant [13 x i8] c"PREDICTION_L\00"
@C = global [1681 x float] zeroinitializer, align 16
@p_str9 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_INNER\00", align 1
@p_str8 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_OUTER\00", align 1
@p_str7 = private unnamed_addr constant [7 x i8] c"CALC_K\00", align 1
@p_str6 = private unnamed_addr constant [20 x i8] c"DELETE_BV_UNSET_C_Q\00", align 1
@p_str5 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_INNER\00", align 1
@p_str4 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_OUTER\00", align 1
@p_str3 = private unnamed_addr constant [16 x i8] c"DELETE_BV_ALPHA\00", align 1
@p_str20 = internal unnamed_addr constant [1 x i8] zeroinitializer
@p_str2 = private unnamed_addr constant [20 x i8] c"DELETE_BV_SWAP_LOOP\00", align 1
@p_str19 = private unnamed_addr constant [11 x i8] c"INIT_INNER\00", align 1
@p_str18 = private unnamed_addr constant [11 x i8] c"INIT_OUTER\00", align 1
@p_str17 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1
@p_str16 = private unnamed_addr constant [8 x i8] c"COPY_BV\00", align 1
@p_str15 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_INNER\00", align 1
@p_str14 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_OUTER\00", align 1
@p_str13 = private unnamed_addr constant [15 x i8] c"UPDATE_C_INNER\00", align 1
@p_str12 = private unnamed_addr constant [15 x i8] c"UPDATE_C_OUTER\00", align 1
@p_str11 = private unnamed_addr constant [13 x i8] c"UPDATE_ALPHA\00", align 1
@p_str10 = private unnamed_addr constant [7 x i8] c"CALC_S\00", align 1
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str = private unnamed_addr constant [12 x i8] c"hls_label_0\00", align 1

define internal fastcc void @projection_gp_train_full_bv_set([13 x float]* nocapture %pX, float %pY) {
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([13 x float]* %pX, [1 x i8]* @p_str20, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str20, i32 -1, [1 x i8]* @p_str20, [1 x i8]* @p_str20, [1 x i8]* @p_str20)
  %pY_read = call float @_ssdm_op_Read.ap_auto.float(float %pY)
  br label %1

; <label>:1                                       ; preds = %K.exit, %0
  %i = phi i6 [ 0, %0 ], [ %i_1, %K.exit ]
  %m = phi float [ 0x4050B830E0000000, %0 ], [ %m_1, %K.exit ]
  %phi_mul = phi i10 [ 0, %0 ], [ %next_mul, %K.exit ]
  %next_mul = add i10 %phi_mul, 13
  %exitcond8 = icmp eq i6 %i, -24
  %i_1 = add i6 %i, 1
  br i1 %exitcond8, label %.preheader10, label %2

; <label>:2                                       ; preds = %1
  %empty_4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40)
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str7) nounwind
  %tmp_2 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str7)
  br label %3

; <label>:3                                       ; preds = %4, %2
  %sum_i = phi float [ 0.000000e+00, %2 ], [ %sum, %4 ]
  %i_i = phi i4 [ 0, %2 ], [ %i_7, %4 ]
  %exitcond_i = icmp eq i4 %i_i, -3
  %i_7 = add i4 %i_i, 1
  br i1 %exitcond_i, label %K.exit, label %4

; <label>:4                                       ; preds = %3
  %empty_5 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13)
  %tmp_106_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_104_i = zext i4 %i_i to i64
  %tmp_104_i_cast = zext i4 %i_i to i10
  %sum1_i = add i10 %tmp_104_i_cast, %phi_mul
  %sum1_i_cast = zext i10 %sum1_i to i64
  %basisVectors_addr = getelementptr [533 x float]* @basisVectors, i64 0, i64 %sum1_i_cast
  %basisVectors_load = load float* %basisVectors_addr, align 4
  %pX_addr = getelementptr [13 x float]* %pX, i64 0, i64 %tmp_104_i
  %pX_load = load float* %pX_addr, align 4
  %val = fsub float %basisVectors_load, %pX_load
  %tmp_105_i = fmul float %val, %val
  %sum = fadd float %sum_i, %tmp_105_i
  %empty_6 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_106_i)
  br label %3

K.exit:                                           ; preds = %3
  %p_x_assign = fmul float %sum_i, -5.000000e-01
  %tmp_i_i = call float @llvm.exp.f32(float %p_x_assign) nounwind
  %tmp_3 = zext i6 %i to i64
  %k_addr = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp_3
  store float %tmp_i_i, float* %k_addr, align 4
  %alpha_addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_3
  %alpha_load = load float* %alpha_addr, align 4
  %tmp_4 = fmul float %tmp_i_i, %alpha_load
  %m_1 = fadd float %m, %tmp_4
  %empty_7 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str7, i32 %tmp_2)
  br label %1

.preheader10:                                     ; preds = %1, %8
  %i1 = phi i6 [ %i_3, %8 ], [ 0, %1 ]
  %phi_mul6 = phi i11 [ %next_mul7, %8 ], [ 0, %1 ]
  %next_mul7 = add i11 %phi_mul6, 41
  %exitcond7 = icmp eq i6 %i1, -24
  %empty_8 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40)
  %i_3 = add i6 %i1, 1
  br i1 %exitcond7, label %.preheader9, label %5

; <label>:5                                       ; preds = %.preheader10
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str8) nounwind
  %tmp_1 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str8)
  %tmp_7 = zext i6 %i1 to i64
  %s_addr = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_7
  store float 0.000000e+00, float* %s_addr, align 4
  %e_addr = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp_7
  br label %6

; <label>:6                                       ; preds = %7, %5
  %storemerge = phi float [ 0.000000e+00, %5 ], [ %tmp_20, %7 ]
  %tmp_s = phi float [ 0.000000e+00, %5 ], [ %tmp_18, %7 ]
  %j = phi i6 [ 0, %5 ], [ %j_1, %7 ]
  store float %storemerge, float* %e_addr, align 4
  %j_cast = zext i6 %j to i11
  %exitcond6 = icmp eq i6 %j, -24
  %empty_9 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40)
  %j_1 = add i6 %j, 1
  br i1 %exitcond6, label %8, label %7

; <label>:7                                       ; preds = %6
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str9) nounwind
  %tmp_14 = add i11 %j_cast, %phi_mul6
  %tmp_15 = zext i11 %tmp_14 to i64
  %C_addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_15
  %C_load = load float* %C_addr, align 4
  %tmp_16 = zext i6 %j to i64
  %k_addr_2 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp_16
  %k_load_1 = load float* %k_addr_2, align 4
  %tmp_17 = fmul float %C_load, %k_load_1
  %tmp_18 = fadd float %tmp_s, %tmp_17
  store float %tmp_18, float* %s_addr, align 4
  %Q_addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_15
  %Q_load = load float* %Q_addr, align 4
  %tmp_19 = fmul float %Q_load, %k_load_1
  %tmp_20 = fadd float %storemerge, %tmp_19
  br label %6

; <label>:8                                       ; preds = %6
  %empty_10 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str8, i32 %tmp_1)
  br label %.preheader10

.preheader9:                                      ; preds = %.preheader10, %9
  %i2 = phi i6 [ %i_2, %9 ], [ 0, %.preheader10 ]
  %sigma2 = phi float [ %sigma2_1, %9 ], [ 1.000000e+00, %.preheader10 ]
  %exitcond5 = icmp eq i6 %i2, -24
  %empty_11 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40)
  %i_2 = add i6 %i2, 1
  br i1 %exitcond5, label %10, label %9

; <label>:9                                       ; preds = %.preheader9
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str10) nounwind
  %tmp_12 = zext i6 %i2 to i64
  %s_addr_1 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_12
  %s_load = load float* %s_addr_1, align 4
  %k_addr_1 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp_12
  %k_load = load float* %k_addr_1, align 4
  %tmp_13 = fmul float %s_load, %k_load
  %sigma2_1 = fadd float %sigma2, %tmp_13
  br label %.preheader9

; <label>:10                                      ; preds = %.preheader9
  store float 1.000000e+00, float* getelementptr inbounds ([41 x float]* @s, i64 0, i64 40), align 16
  %tmp_5 = fsub float %pY_read, %m
  %tmp_6 = fpext float %tmp_5 to double
  %tmp_8 = fpext float %sigma2 to double
  %tmp_9 = fadd double %tmp_8, 1.000000e+00
  %tmp_10 = fdiv double %tmp_6, %tmp_9
  %q = fptrunc double %tmp_10 to float
  %tmp_11 = fdiv double -1.000000e+00, %tmp_9
  %r = fptrunc double %tmp_11 to float
  br label %11

; <label>:11                                      ; preds = %12, %10
  %gamma = phi float [ 1.000000e+00, %10 ], [ %gamma_1, %12 ]
  %i3 = phi i6 [ 0, %10 ], [ %i_4, %12 ]
  %exitcond4 = icmp eq i6 %i3, -24
  %empty_12 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40)
  %i_4 = add i6 %i3, 1
  br i1 %exitcond4, label %13, label %12

; <label>:12                                      ; preds = %11
  call void (...)* @_ssdm_op_SpecLoopName([13 x i8]* @p_str11) nounwind
  %tmp_22 = zext i6 %i3 to i64
  %e_addr_1 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp_22
  %e_load = load float* %e_addr_1, align 4
  %k_addr_3 = getelementptr inbounds [40 x float]* @k, i64 0, i64 %tmp_22
  %k_load_2 = load float* %k_addr_3, align 4
  %tmp_23 = fmul float %e_load, %k_load_2
  %gamma_1 = fsub float %gamma, %tmp_23
  %s_addr_2 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_22
  %s_load_1 = load float* %s_addr_2, align 4
  %tmp_24 = fmul float %s_load_1, %q
  %alpha_addr_1 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_22
  %alpha_load_2 = load float* %alpha_addr_1, align 4
  %tmp_25 = fadd float %alpha_load_2, %tmp_24
  store float %tmp_25, float* %alpha_addr_1, align 4
  br label %11

; <label>:13                                      ; preds = %11
  %alpha_load_1 = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16
  %tmp_21 = fadd float %alpha_load_1, %q
  store float %tmp_21, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16
  br label %14

; <label>:14                                      ; preds = %18, %13
  %i4 = phi i6 [ 0, %13 ], [ %i_5, %18 ]
  %phi_mul8 = phi i11 [ 0, %13 ], [ %next_mul9, %18 ]
  %next_mul9 = add i11 %phi_mul8, 41
  %exitcond3 = icmp eq i6 %i4, -23
  %empty_13 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41)
  %i_5 = add i6 %i4, 1
  br i1 %exitcond3, label %.preheader.preheader, label %15

.preheader.preheader:                             ; preds = %14
  %tmp_26 = fpext float %gamma to double
  br label %.preheader

; <label>:15                                      ; preds = %14
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str12) nounwind
  %tmp_27 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str12)
  %tmp_28 = zext i6 %i4 to i64
  %s_addr_3 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_28
  %s_load_2 = load float* %s_addr_3, align 4
  br label %16

; <label>:16                                      ; preds = %17, %15
  %j5 = phi i6 [ 0, %15 ], [ %j_2, %17 ]
  %j5_cast8 = zext i6 %j5 to i11
  %exitcond2 = icmp eq i6 %j5, -23
  %empty_14 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41)
  %j_2 = add i6 %j5, 1
  br i1 %exitcond2, label %18, label %17

; <label>:17                                      ; preds = %16
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str13) nounwind
  %tmp_32 = zext i6 %j5 to i64
  %s_addr_4 = getelementptr inbounds [41 x float]* @s, i64 0, i64 %tmp_32
  %s_load_3 = load float* %s_addr_4, align 4
  %tmp_33 = fmul float %s_load_2, %s_load_3
  %tmp_34 = fmul float %tmp_33, %r
  %tmp_35 = add i11 %j5_cast8, %phi_mul8
  %tmp_36 = zext i11 %tmp_35 to i64
  %C_addr_1 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_36
  %C_load_1 = load float* %C_addr_1, align 4
  %tmp_37 = fadd float %C_load_1, %tmp_34
  store float %tmp_37, float* %C_addr_1, align 4
  br label %16

; <label>:18                                      ; preds = %16
  %empty_15 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str12, i32 %tmp_27)
  br label %14

.preheader:                                       ; preds = %21, %.preheader.preheader
  %i6 = phi i6 [ %i_6, %21 ], [ 0, %.preheader.preheader ]
  %phi_mul1 = phi i11 [ %next_mul1, %21 ], [ 0, %.preheader.preheader ]
  %next_mul1 = add i11 %phi_mul1, 41
  %exitcond1 = icmp eq i6 %i6, -23
  %empty_16 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41)
  %i_6 = add i6 %i6, 1
  br i1 %exitcond1, label %.preheader11, label %19

; <label>:19                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str14) nounwind
  %tmp_29 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str14)
  %tmp_30 = icmp eq i6 %i6, -24
  %tmp_31 = zext i6 %i6 to i64
  %e_addr_2 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp_31
  %e_load_1 = load float* %e_addr_2, align 4
  %ti = select i1 %tmp_30, float -1.000000e+00, float %e_load_1
  br label %20

; <label>:20                                      ; preds = %._crit_edge_ifconv, %19
  %j7 = phi i6 [ 0, %19 ], [ %j_3, %._crit_edge_ifconv ]
  %j7_cast4 = zext i6 %j7 to i11
  %exitcond = icmp eq i6 %j7, -23
  %empty_17 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41)
  %j_3 = add i6 %j7, 1
  br i1 %exitcond, label %21, label %._crit_edge_ifconv

._crit_edge_ifconv:                               ; preds = %20
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str15) nounwind
  %tmp_38 = icmp eq i6 %j7, -24
  %tmp_39 = zext i6 %j7 to i64
  %e_addr_3 = getelementptr inbounds [41 x float]* @e, i64 0, i64 %tmp_39
  %e_load_2 = load float* %e_addr_3, align 4
  %tj = select i1 %tmp_38, float -1.000000e+00, float %e_load_2
  %tmp_40 = fmul float %ti, %tj
  %tmp_41 = fpext float %tmp_40 to double
  %tmp_42 = fdiv double %tmp_41, %tmp_26
  %tmp_43 = add i11 %j7_cast4, %phi_mul1
  %tmp_44 = zext i11 %tmp_43 to i64
  %Q_addr_1 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_44
  %Q_load_1 = load float* %Q_addr_1, align 4
  %tmp_45 = fpext float %Q_load_1 to double
  %tmp_46 = fadd double %tmp_45, %tmp_42
  %tmp_47 = fptrunc double %tmp_46 to float
  store float %tmp_47, float* %Q_addr_1, align 4
  br label %20

; <label>:21                                      ; preds = %20
  %empty_18 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str14, i32 %tmp_29)
  br label %.preheader

.preheader11:                                     ; preds = %.preheader, %22
  %i_i1 = phi i4 [ %i_8, %22 ], [ 0, %.preheader ]
  %i_i1_cast2 = zext i4 %i_i1 to i32
  %exitcond_i2 = icmp eq i4 %i_i1, -3
  %empty_19 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13)
  %i_8 = add i4 %i_i1, 1
  br i1 %exitcond_i2, label %copyBV.exit, label %22

; <label>:22                                      ; preds = %.preheader11
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @p_str16) nounwind
  %tmp_i4 = zext i4 %i_i1 to i64
  %pX_addr_1 = getelementptr [13 x float]* %pX, i64 0, i64 %tmp_i4
  %pX_load_1 = load float* %pX_addr_1, align 4
  %bvCnt_load = load i32* @bvCnt, align 4
  %tmp_i = mul i32 %bvCnt_load, 13
  %tmp_102_i = add i32 %tmp_i, %i_i1_cast2
  %tmp_103_i = zext i32 %tmp_102_i to i64
  %basisVectors_addr_1 = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp_103_i
  store float %pX_load_1, float* %basisVectors_addr_1, align 4
  br label %.preheader11

copyBV.exit:                                      ; preds = %.preheader11
  %alpha_load_3 = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_i5 = fmul float %alpha_load_3, %alpha_load_3
  %Q_load_2 = load float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 0), align 16
  %C_load_2 = load float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 0), align 16
  %tmp_i6 = fadd float %Q_load_2, %C_load_2
  %minScore = fdiv float %tmp_i5, %tmp_i6
  br label %23

; <label>:23                                      ; preds = %24, %copyBV.exit
  %index_1 = phi i6 [ 1, %copyBV.exit ], [ %i_9, %24 ]
  %minScore1_i = phi float [ %minScore, %copyBV.exit ], [ %minScore_2, %24 ]
  %index = phi i32 [ 0, %copyBV.exit ], [ %index_2, %24 ]
  %index_1_cast1 = zext i6 %index_1 to i32
  %index_1_cast1_cast = zext i6 %index_1 to i13
  %exitcond_i7 = icmp eq i6 %index_1, -23
  %empty_20 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind
  br i1 %exitcond_i7, label %getMinKLApprox.exit, label %24

; <label>:24                                      ; preds = %23
  %tmp_68_i = zext i6 %index_1 to i64
  %alpha_addr_2 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_68_i
  %alpha_load_4 = load float* %alpha_addr_2, align 4
  %tmp_69_i = fmul float %alpha_load_4, %alpha_load_4
  %tmp_70_i = mul i13 42, %index_1_cast1_cast
  %tmp_71_i = zext i13 %tmp_70_i to i64
  %Q_addr_2 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_71_i
  %Q_load_3 = load float* %Q_addr_2, align 8
  %C_addr_2 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_71_i
  %C_load_3 = load float* %C_addr_2, align 8
  %tmp_72_i = fadd float %Q_load_3, %C_load_3
  %tScore = fdiv float %tmp_69_i, %tmp_72_i
  %tScore_to_int = bitcast float %tScore to i32
  %tmp_48 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tScore_to_int, i32 23, i32 30)
  %tmp = trunc i32 %tScore_to_int to i23
  %minScore1_i_to_int = bitcast float %minScore1_i to i32
  %tmp_49 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %minScore1_i_to_int, i32 23, i32 30)
  %tmp_50 = trunc i32 %minScore1_i_to_int to i23
  %notlhs = icmp ne i8 %tmp_48, -1
  %notrhs = icmp eq i23 %tmp, 0
  %tmp_51 = or i1 %notrhs, %notlhs
  %notlhs1 = icmp ne i8 %tmp_49, -1
  %notrhs1 = icmp eq i23 %tmp_50, 0
  %tmp_52 = or i1 %notrhs1, %notlhs1
  %tmp_53 = and i1 %tmp_51, %tmp_52
  %tmp_54 = fcmp olt float %tScore, %minScore1_i
  %tmp_55 = and i1 %tmp_53, %tmp_54
  %minScore_2 = select i1 %tmp_55, float %tScore, float %minScore1_i
  %index_2 = select i1 %tmp_55, i32 %index_1_cast1, i32 %index
  %i_9 = add i6 1, %index_1
  br label %23

getMinKLApprox.exit:                              ; preds = %23
  call fastcc void @projection_gp_deleteBV(i32 %index)
  ret void
}

define internal fastcc void @projection_gp_swapRowAndColumn([1681 x float]* nocapture %pM, i32 %rowA) {
  %rowA_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %rowA)
  %tmp = mul i32 %rowA_read, 41
  br label %1

; <label>:1                                       ; preds = %2, %0
  %i = phi i6 [ 0, %0 ], [ %i_7, %2 ]
  %i_cast4_cast = zext i6 %i to i10
  %i_cast3 = zext i6 %i to i32
  %exitcond1 = icmp eq i6 %i, -23
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41)
  %i_7 = add i6 %i, 1
  br i1 %exitcond1, label %.preheader, label %2

; <label>:2                                       ; preds = %1
  %tmp_s = add i32 %i_cast3, %tmp
  %tmp_50 = zext i32 %tmp_s to i64
  %pM_addr = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp_50
  %temp = load float* %pM_addr, align 4
  %tmp_51 = add i10 %i_cast4_cast, -408
  %tmp_51_cast5 = sext i10 %tmp_51 to i11
  %tmp_52 = zext i11 %tmp_51_cast5 to i64
  %pM_addr_1 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp_52
  %pM_load = load float* %pM_addr_1, align 4
  store float %pM_load, float* %pM_addr, align 4
  store float %temp, float* %pM_addr_1, align 4
  br label %1

.preheader:                                       ; preds = %1, %3
  %i1 = phi i6 [ %i_8, %3 ], [ 0, %1 ]
  %phi_mul = phi i11 [ %next_mul, %3 ], [ 0, %1 ]
  %exitcond = icmp eq i6 %i1, -23
  %empty_21 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41)
  %i_8 = add i6 %i1, 1
  br i1 %exitcond, label %4, label %3

; <label>:3                                       ; preds = %.preheader
  %next_mul = add i11 %phi_mul, 41
  %tmp_53_cast6 = zext i11 %phi_mul to i32
  %tmp_54 = add i32 %tmp_53_cast6, %rowA_read
  %tmp_55 = zext i32 %tmp_54 to i64
  %pM_addr_2 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp_55
  %temp_1 = load float* %pM_addr_2, align 4
  %tmp_56 = add i11 %phi_mul, 40
  %tmp_57 = zext i11 %tmp_56 to i64
  %pM_addr_3 = getelementptr [1681 x float]* %pM, i64 0, i64 %tmp_57
  %pM_load_2 = load float* %pM_addr_3, align 4
  store float %pM_load_2, float* %pM_addr_2, align 4
  store float %temp_1, float* %pM_addr_3, align 4
  br label %.preheader

; <label>:4                                       ; preds = %.preheader
  ret void
}

define float @projection_gp([13 x float]* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pReset) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([13 x float]* %pX) nounwind, !map !50
  call void (...)* @_ssdm_op_SpecBitsMap(float %pY) nounwind, !map !56
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pPredict) nounwind, !map !62
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pReset) nounwind, !map !66
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !70
  call void (...)* @_ssdm_op_SpecTopModule([14 x i8]* @projection_gp_str) nounwind
  %pReset_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pReset) nounwind
  %pPredict_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pPredict) nounwind
  %pY_read = call float @_ssdm_op_Read.s_axilite.float(float %pY) nounwind
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([13 x float]* %pX, [1 x i8]* @p_str20, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str20, i32 -1, [1 x i8]* @p_str20, [1 x i8]* @p_str20, [1 x i8]* @p_str20) nounwind
  call void (...)* @_ssdm_op_SpecInterface([13 x float]* %pX, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 13, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(float %pY, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i1 %pPredict, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i1 %pReset, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  br i1 %pReset_read, label %.preheader3, label %.loopexit4

.preheader3:                                      ; preds = %0, %4
  %i = phi i6 [ %i_9, %4 ], [ 0, %0 ]
  %exitcond2 = icmp eq i6 %i, -23
  %empty_22 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind
  %i_9 = add i6 %i, 1
  br i1 %exitcond2, label %.loopexit4, label %1

; <label>:1                                       ; preds = %.preheader3
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @p_str18) nounwind
  %tmp = call i32 (...)* @_ssdm_op_SpecRegionBegin([11 x i8]* @p_str18) nounwind
  %p_shl = call i10 @_ssdm_op_BitConcatenate.i10.i6.i4(i6 %i, i4 0)
  %p_shl_cast = zext i10 %p_shl to i11
  %p_shl1 = call i7 @_ssdm_op_BitConcatenate.i7.i6.i1(i6 %i, i1 false)
  %p_shl1_cast = zext i7 %p_shl1 to i11
  %tmp_s = sub i11 %p_shl_cast, %p_shl1_cast
  %tmp_cast = sext i11 %tmp_s to i32
  %tmp_58 = zext i32 %tmp_cast to i64
  %Q_addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_58
  %C_addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_58
  br label %2

; <label>:2                                       ; preds = %3, %1
  %j = phi i6 [ 0, %1 ], [ %j_4, %3 ]
  %exitcond1 = icmp eq i6 %j, -23
  %empty_23 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind
  %j_4 = add i6 %j, 1
  br i1 %exitcond1, label %4, label %3

; <label>:3                                       ; preds = %2
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @p_str19) nounwind
  %tmp_61 = icmp eq i6 %i, %j
  %storemerge5 = select i1 %tmp_61, float 1.000000e+00, float 0.000000e+00
  store float %storemerge5, float* %Q_addr, align 8
  store float %storemerge5, float* %C_addr, align 8
  br label %2

; <label>:4                                       ; preds = %2
  %tmp_60 = zext i6 %i to i64
  %alpha_addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_60
  store float 0.000000e+00, float* %alpha_addr, align 4
  %empty_24 = call i32 (...)* @_ssdm_op_SpecRegionEnd([11 x i8]* @p_str18, i32 %tmp) nounwind
  br label %.preheader3

.loopexit4:                                       ; preds = %.preheader3, %0
  br i1 %pPredict_read, label %.preheader.preheader, label %5

; <label>:5                                       ; preds = %.loopexit4
  %bvCnt_load = load i32* @bvCnt, align 4
  %tmp_59 = icmp eq i32 %bvCnt_load, 40
  br i1 %tmp_59, label %6, label %.preheader6

; <label>:6                                       ; preds = %5
  call fastcc void @projection_gp_train_full_bv_set([13 x float]* %pX, float %pY_read) nounwind
  br label %8

.preheader6:                                      ; preds = %5, %7
  %bvCnt_load_1 = phi i32 [ %bvCnt_load_2, %7 ], [ %bvCnt_load, %5 ]
  %i_i = phi i4 [ %i_2, %7 ], [ 0, %5 ]
  %i_i_cast4 = zext i4 %i_i to i32
  %exitcond_i = icmp eq i4 %i_i, -3
  %empty_25 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) nounwind
  %i_2 = add i4 %i_i, 1
  br i1 %exitcond_i, label %copyBV.exit, label %7

; <label>:7                                       ; preds = %.preheader6
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @p_str16) nounwind
  %tmp_i = zext i4 %i_i to i64
  %pX_addr = getelementptr [13 x float]* %pX, i64 0, i64 %tmp_i
  %pX_load = load float* %pX_addr, align 4
  %bvCnt_load_2 = load i32* @bvCnt, align 4
  %tmp_i_26 = mul i32 %bvCnt_load_2, 13
  %tmp_102_i = add i32 %tmp_i_26, %i_i_cast4
  %tmp_103_i = zext i32 %tmp_102_i to i64
  %basisVectors_addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp_103_i
  store float %pX_load, float* %basisVectors_addr, align 4
  br label %.preheader6

copyBV.exit:                                      ; preds = %.preheader6
  %tmp_62 = add i32 %bvCnt_load_1, 1
  store i32 %tmp_62, i32* @bvCnt, align 4
  br label %8

; <label>:8                                       ; preds = %copyBV.exit, %6
  br label %.loopexit

.preheader.preheader:                             ; preds = %.loopexit4, %K.exit
  %indvar_flatten = phi i10 [ %indvar_flatten_next, %K.exit ], [ 0, %.loopexit4 ]
  %sum = phi float [ %sum_mid2, %K.exit ], [ 0x4050B830E0000000, %.loopexit4 ]
  %i1 = phi i6 [ %i1_mid2, %K.exit ], [ 0, %.loopexit4 ]
  %sum_i = phi float [ %sum_2, %K.exit ], [ 0.000000e+00, %.loopexit4 ]
  %i_i2 = phi i4 [ %i_1, %K.exit ], [ 0, %.loopexit4 ]
  %p_x_assign_dup = fmul float %sum_i, -5.000000e-01
  %tmp_65_dup = zext i6 %i1 to i64
  %alpha_addr_3 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_65_dup
  %alpha_load = load float* %alpha_addr_3, align 4
  %exitcond_flatten = icmp eq i10 %indvar_flatten, -504
  %indvar_flatten_next = add i10 %indvar_flatten, 1
  br i1 %exitcond_flatten, label %.loopexit.loopexit, label %K.exit

K.exit:                                           ; preds = %.preheader.preheader
  %tmp_i_i = call float @llvm.exp.f32(float %p_x_assign_dup) nounwind
  %tmp_64 = fmul float %tmp_i_i, %alpha_load
  %sum_1 = fadd float %sum, %tmp_64
  call void (...)* @_ssdm_op_SpecLoopName([13 x i8]* @PREDICTION_L_str)
  %empty_27 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 520, i64 520, i64 520) nounwind
  %exitcond_i1 = icmp eq i4 %i_i2, -3
  %sum_i_mid2 = select i1 %exitcond_i1, float 0.000000e+00, float %sum_i
  %i_i2_mid2 = select i1 %exitcond_i1, i4 0, i4 %i_i2
  %sum_mid2 = select i1 %exitcond_i1, float %sum_1, float %sum
  %i_s = add i6 %i1, 1
  %i1_mid2 = select i1 %exitcond_i1, i6 %i_s, i6 %i1
  %i1_cast3 = zext i6 %i1_mid2 to i10
  %tmp_63 = mul i10 %i1_cast3, 13
  %tmp_106_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_104_i4 = zext i4 %i_i2_mid2 to i64
  %tmp_104_i4_cast = zext i4 %i_i2_mid2 to i10
  %sum1_i = add i10 %tmp_104_i4_cast, %tmp_63
  %sum1_i_cast = zext i10 %sum1_i to i64
  %basisVectors_addr_2 = getelementptr [533 x float]* @basisVectors, i64 0, i64 %sum1_i_cast
  %basisVectors_load = load float* %basisVectors_addr_2, align 4
  %pX_addr_2 = getelementptr [13 x float]* %pX, i64 0, i64 %tmp_104_i4
  %pX_load_2 = load float* %pX_addr_2, align 4
  %val = fsub float %basisVectors_load, %pX_load_2
  %tmp_105_i = fmul float %val, %val
  %sum_2 = fadd float %sum_i_mid2, %tmp_105_i
  %empty_28 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_106_i) nounwind
  %i_1 = add i4 %i_i2_mid2, 1
  br label %.preheader.preheader

.loopexit.loopexit:                               ; preds = %.preheader.preheader
  %tmp_i_i_dup = call float @llvm.exp.f32(float %p_x_assign_dup) nounwind
  %tmp_66_dup = fmul float %tmp_i_i_dup, %alpha_load
  %sum_1_dup = fadd float %sum, %tmp_66_dup
  br label %.loopexit

.loopexit:                                        ; preds = %.loopexit.loopexit, %8
  %p_0 = phi float [ 0.000000e+00, %8 ], [ %sum_1_dup, %.loopexit.loopexit ]
  ret float %p_0
}

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare float @llvm.exp.f32(float) nounwind readonly

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define internal fastcc void @projection_gp_deleteBV(i32 %pIndex) nounwind uwtable {
  %pIndex_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %pIndex) nounwind
  %tmp = zext i32 %pIndex_read to i64
  %alpha_addr = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp
  %temp = load float* %alpha_addr, align 4
  %alpha_load = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16
  store float %alpha_load, float* %alpha_addr, align 4
  store float %temp, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16
  call fastcc void @projection_gp_swapRowAndColumn([1681 x float]* @C, i32 %pIndex_read) nounwind
  call fastcc void @projection_gp_swapRowAndColumn([1681 x float]* @Q, i32 %pIndex_read) nounwind
  %tmp_s = mul i32 %pIndex_read, 13
  br label %1

; <label>:1                                       ; preds = %2, %0
  %i = phi i4 [ 0, %0 ], [ %i_10, %2 ]
  %exitcond4 = icmp eq i4 %i, -3
  %i_10 = add i4 %i, 1
  br i1 %exitcond4, label %3, label %2

; <label>:2                                       ; preds = %1
  %i_cast9 = zext i4 %i to i10
  %i_cast8 = zext i4 %i to i32
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 13, i64 13, i64 13) nounwind
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @p_str2) nounwind
  %tmp_66 = call i32 (...)* @_ssdm_op_SpecRegionBegin([20 x i8]* @p_str2) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_67 = add i32 %i_cast8, %tmp_s
  %tmp_68 = zext i32 %tmp_67 to i64
  %basisVectors_addr = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp_68
  %temp_3 = load float* %basisVectors_addr, align 4
  %tmp_69 = add i10 %i_cast9, -504
  %tmp_70 = zext i10 %tmp_69 to i64
  %basisVectors_addr_1 = getelementptr inbounds [533 x float]* @basisVectors, i64 0, i64 %tmp_70
  %basisVectors_load = load float* %basisVectors_addr_1, align 4
  store float %basisVectors_load, float* %basisVectors_addr, align 4
  store float %temp_3, float* %basisVectors_addr_1, align 4
  %empty_29 = call i32 (...)* @_ssdm_op_SpecRegionEnd([20 x i8]* @p_str2, i32 %tmp_66) nounwind
  br label %1

; <label>:3                                       ; preds = %1
  %alphaStar = load float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16
  %cStar = load float* getelementptr inbounds ([1681 x float]* @C, i64 0, i64 1680), align 16
  %qStar = load float* getelementptr inbounds ([1681 x float]* @Q, i64 0, i64 1680), align 16
  %tmp_65 = fadd float %cStar, %qStar
  %temp_2 = fdiv float %alphaStar, %tmp_65
  br label %4

; <label>:4                                       ; preds = %5, %3
  %i1 = phi i6 [ 0, %3 ], [ %i_11, %5 ]
  %i1_cast7_cast = zext i6 %i1 to i10
  %exitcond3 = icmp eq i6 %i1, -24
  %empty_30 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind
  %i_11 = add i6 %i1, 1
  br i1 %exitcond3, label %6, label %5

; <label>:5                                       ; preds = %4
  call void (...)* @_ssdm_op_SpecLoopName([16 x i8]* @p_str3) nounwind
  %a = add i10 %i1_cast7_cast, -408
  %a_cast = sext i10 %a to i11
  %tmp_71 = zext i11 %a_cast to i64
  %C_addr = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_71
  %C_load = load float* %C_addr, align 4
  %Q_addr = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_71
  %Q_load = load float* %Q_addr, align 4
  %tmp_73 = fadd float %C_load, %Q_load
  %tmp_74 = fmul float %tmp_73, %temp_2
  %tmp_75 = zext i6 %i1 to i64
  %alpha_addr_3 = getelementptr inbounds [41 x float]* @alpha, i64 0, i64 %tmp_75
  %alpha_load_5 = load float* %alpha_addr_3, align 4
  %tmp_76 = fsub float %alpha_load_5, %tmp_74
  store float %tmp_76, float* %alpha_addr_3, align 4
  br label %4

; <label>:6                                       ; preds = %4
  store float 0.000000e+00, float* getelementptr inbounds ([41 x float]* @alpha, i64 0, i64 40), align 16
  br label %7

; <label>:7                                       ; preds = %11, %6
  %i2 = phi i6 [ 0, %6 ], [ %i_12, %11 ]
  %phi_mul = phi i11 [ 0, %6 ], [ %next_mul, %11 ]
  %next_mul = add i11 %phi_mul, 41
  %i2_cast4_cast = zext i6 %i2 to i10
  %exitcond2 = icmp eq i6 %i2, -24
  %empty_31 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind
  %i_12 = add i6 %i2, 1
  br i1 %exitcond2, label %.preheader, label %8

; <label>:8                                       ; preds = %7
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @p_str4) nounwind
  %tmp_72 = call i32 (...)* @_ssdm_op_SpecRegionBegin([18 x i8]* @p_str4) nounwind
  %a_2 = add i10 %i2_cast4_cast, -408
  %a_2_cast = sext i10 %a_2 to i11
  %tmp_77 = zext i11 %a_2_cast to i64
  %Q_addr_2 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_77
  %C_addr_2 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_77
  br label %9

; <label>:9                                       ; preds = %10, %8
  %j = phi i6 [ 0, %8 ], [ %j_5, %10 ]
  %j_cast3 = zext i6 %j to i11
  %j_cast3_cast = zext i6 %j to i10
  %exitcond1 = icmp eq i6 %j, -24
  %empty_32 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 40, i64 40, i64 40) nounwind
  %j_5 = add i6 %j, 1
  br i1 %exitcond1, label %11, label %10

; <label>:10                                      ; preds = %9
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @p_str5) nounwind
  %b_1 = add i10 %j_cast3_cast, -408
  %b_1_cast = sext i10 %b_1 to i11
  %c = add i11 %j_cast3, %phi_mul
  %Q_load_3 = load float* %Q_addr_2, align 4
  %tmp_80 = zext i11 %b_1_cast to i64
  %Q_addr_5 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_80
  %Q_load_4 = load float* %Q_addr_5, align 4
  %tmp_81 = fmul float %Q_load_3, %Q_load_4
  %temp_4 = fdiv float %tmp_81, %qStar
  %C_load_3 = load float* %C_addr_2, align 4
  %C_addr_5 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_80
  %C_load_4 = load float* %C_addr_5, align 4
  %tmp_82 = fmul float %C_load_3, %C_load_4
  %tmp_83 = fmul float %C_load_3, %Q_load_4
  %tmp_84 = fadd float %tmp_82, %tmp_83
  %tmp_85 = fmul float %Q_load_3, %C_load_4
  %tmp_86 = fadd float %tmp_84, %tmp_85
  %tmp_87 = fadd float %tmp_86, %tmp_81
  %tmp_88 = fdiv float %tmp_87, %tmp_65
  %tmp_89 = fsub float %temp_4, %tmp_88
  %tmp_90 = zext i11 %c to i64
  %C_addr_6 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_90
  %C_load_5 = load float* %C_addr_6, align 4
  %tmp_91 = fadd float %C_load_5, %tmp_89
  store float %tmp_91, float* %C_addr_6, align 4
  %Q_addr_6 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_90
  %Q_load_5 = load float* %Q_addr_6, align 4
  %tmp_92 = fsub float %Q_load_5, %temp_4
  store float %tmp_92, float* %Q_addr_6, align 4
  br label %9

; <label>:11                                      ; preds = %9
  %empty_33 = call i32 (...)* @_ssdm_op_SpecRegionEnd([18 x i8]* @p_str4, i32 %tmp_72) nounwind
  br label %7

.preheader:                                       ; preds = %7, %12
  %i5 = phi i6 [ %i_3, %12 ], [ 0, %7 ]
  %phi_mul6 = phi i11 [ %next_mul7, %12 ], [ 0, %7 ]
  %i5_cast2_cast = zext i6 %i5 to i10
  %exitcond = icmp eq i6 %i5, -23
  %empty_34 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 41, i64 41, i64 41) nounwind
  %i_3 = add i6 %i5, 1
  br i1 %exitcond, label %13, label %12

; <label>:12                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @p_str6) nounwind
  %next_mul7 = add i11 %phi_mul6, 41
  %a_1 = add i11 %phi_mul6, 40
  %b = add i10 %i5_cast2_cast, -408
  %b_cast = sext i10 %b to i11
  %tmp_78 = zext i11 %a_1 to i64
  %Q_addr_3 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_78
  store float 0.000000e+00, float* %Q_addr_3, align 4
  %tmp_79 = zext i11 %b_cast to i64
  %Q_addr_4 = getelementptr inbounds [1681 x float]* @Q, i64 0, i64 %tmp_79
  store float 0.000000e+00, float* %Q_addr_4, align 4
  %C_addr_3 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_78
  store float 0.000000e+00, float* %C_addr_3, align 4
  %C_addr_4 = getelementptr inbounds [1681 x float]* @C, i64 0, i64 %tmp_79
  store float 0.000000e+00, float* %C_addr_4, align 4
  br label %.preheader

; <label>:13                                      ; preds = %.preheader
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecMemCore(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i1 @_ssdm_op_Read.s_axilite.i1(i1) {
entry:
  ret i1 %0
}

define weak float @_ssdm_op_Read.s_axilite.float(float) {
entry:
  ret float %0
}

define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

define weak float @_ssdm_op_Read.ap_auto.float(float) {
entry:
  ret float %0
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_35 = trunc i32 %empty to i8
  ret i8 %empty_35
}

declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i7 @_ssdm_op_BitConcatenate.i7.i6.i1(i6, i1) nounwind readnone {
entry:
  %empty = zext i6 %0 to i7
  %empty_36 = zext i1 %1 to i7
  %empty_37 = shl i7 %empty, 1
  %empty_38 = or i7 %empty_37, %empty_36
  ret i7 %empty_38
}

define weak i10 @_ssdm_op_BitConcatenate.i10.i6.i4(i6, i4) nounwind readnone {
entry:
  %empty = zext i6 %0 to i10
  %empty_39 = zext i4 %1 to i10
  %empty_40 = shl i10 %empty, 4
  %empty_41 = or i10 %empty_40, %empty_39
  ret i10 %empty_41
}

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0, !7, !14, !19, !26, !33, !38, !45}

!0 = metadata !{metadata !1, [41 x float]* @s}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"s", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 40, i32 1}
!7 = metadata !{metadata !8, [40 x float]* @k}
!8 = metadata !{metadata !9}
!9 = metadata !{i32 0, i32 31, metadata !10}
!10 = metadata !{metadata !11}
!11 = metadata !{metadata !"k", metadata !12, metadata !"float", i32 0, i32 31}
!12 = metadata !{metadata !13}
!13 = metadata !{i32 0, i32 39, i32 1}
!14 = metadata !{metadata !15, [41 x float]* @e}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 31, metadata !17}
!17 = metadata !{metadata !18}
!18 = metadata !{metadata !"e", metadata !5, metadata !"float", i32 0, i32 31}
!19 = metadata !{metadata !20, i32* @bvCnt}
!20 = metadata !{metadata !21}
!21 = metadata !{i32 0, i32 31, metadata !22}
!22 = metadata !{metadata !23}
!23 = metadata !{metadata !"bvCnt", metadata !24, metadata !"unsigned int", i32 0, i32 31}
!24 = metadata !{metadata !25}
!25 = metadata !{i32 0, i32 0, i32 1}
!26 = metadata !{metadata !27, [533 x float]* @basisVectors}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 31, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"basisVectors", metadata !31, metadata !"float", i32 0, i32 31}
!31 = metadata !{metadata !32}
!32 = metadata !{i32 0, i32 532, i32 1}
!33 = metadata !{metadata !34, [41 x float]* @alpha}
!34 = metadata !{metadata !35}
!35 = metadata !{i32 0, i32 31, metadata !36}
!36 = metadata !{metadata !37}
!37 = metadata !{metadata !"alpha", metadata !5, metadata !"float", i32 0, i32 31}
!38 = metadata !{metadata !39, [1681 x float]* @Q}
!39 = metadata !{metadata !40}
!40 = metadata !{i32 0, i32 31, metadata !41}
!41 = metadata !{metadata !42}
!42 = metadata !{metadata !"Q", metadata !43, metadata !"float", i32 0, i32 31}
!43 = metadata !{metadata !44}
!44 = metadata !{i32 0, i32 1680, i32 1}
!45 = metadata !{metadata !46, [1681 x float]* @C}
!46 = metadata !{metadata !47}
!47 = metadata !{i32 0, i32 31, metadata !48}
!48 = metadata !{metadata !49}
!49 = metadata !{metadata !"C", metadata !43, metadata !"float", i32 0, i32 31}
!50 = metadata !{metadata !51}
!51 = metadata !{i32 0, i32 31, metadata !52}
!52 = metadata !{metadata !53}
!53 = metadata !{metadata !"pX", metadata !54, metadata !"float", i32 0, i32 31}
!54 = metadata !{metadata !55}
!55 = metadata !{i32 0, i32 12, i32 1}
!56 = metadata !{metadata !57}
!57 = metadata !{i32 0, i32 31, metadata !58}
!58 = metadata !{metadata !59}
!59 = metadata !{metadata !"pY", metadata !60, metadata !"float", i32 0, i32 31}
!60 = metadata !{metadata !61}
!61 = metadata !{i32 0, i32 0, i32 0}
!62 = metadata !{metadata !63}
!63 = metadata !{i32 0, i32 0, metadata !64}
!64 = metadata !{metadata !65}
!65 = metadata !{metadata !"pPredict", metadata !60, metadata !"bool", i32 0, i32 0}
!66 = metadata !{metadata !67}
!67 = metadata !{i32 0, i32 0, metadata !68}
!68 = metadata !{metadata !69}
!69 = metadata !{metadata !"pReset", metadata !60, metadata !"bool", i32 0, i32 0}
!70 = metadata !{metadata !71}
!71 = metadata !{i32 0, i32 31, metadata !72}
!72 = metadata !{metadata !73}
!73 = metadata !{metadata !"return", metadata !74, metadata !"float", i32 0, i32 31}
!74 = metadata !{metadata !75}
!75 = metadata !{i32 0, i32 1, i32 0}
