############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project ProjectionGP
set_top projection_gp
add_files ../../../implementation/hls/Projection_GP/include/ProjectionGP_FULLY_OPTIMIZED.cpp
add_files ../../../implementation/hls/Projection_GP/include/ProjectionGP_FULLY_OPTIMIZED.h
add_files ../../../implementation/hls/Projection_GP/include/ProjectionGP_config.h
add_files -tb ../../../implementation/hls/Projection_GP/test/testHLSProjectionGP.cpp
open_solution "solution1"
set_part {xc7a35ticsg324-1l}
create_clock -period 5 -name default
#source "./ProjectionGP/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
