// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#include "projection_gp_swapRowAndColumn.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

const sc_logic projection_gp_swapRowAndColumn::ap_const_logic_1 = sc_dt::Log_1;
const sc_logic projection_gp_swapRowAndColumn::ap_const_logic_0 = sc_dt::Log_0;
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st1_fsm_0 = "1";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st2_fsm_1 = "10";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st3_fsm_2 = "100";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st4_fsm_3 = "1000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st5_fsm_4 = "10000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st6_fsm_5 = "100000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st7_fsm_6 = "1000000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st8_fsm_7 = "10000000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st9_fsm_8 = "100000000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st10_fsm_9 = "1000000000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st11_fsm_10 = "10000000000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st12_fsm_11 = "100000000000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st13_fsm_12 = "1000000000000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st14_fsm_13 = "10000000000000";
const sc_lv<15> projection_gp_swapRowAndColumn::ap_ST_st15_fsm_14 = "100000000000000";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_0 = "00000000000000000000000000000000";
const sc_lv<1> projection_gp_swapRowAndColumn::ap_const_lv1_1 = "1";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_7 = "111";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_8 = "1000";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_C = "1100";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_D = "1101";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_4 = "100";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_5 = "101";
const sc_lv<1> projection_gp_swapRowAndColumn::ap_const_lv1_0 = "0";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_6 = "110";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_A = "1010";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_B = "1011";
const sc_lv<6> projection_gp_swapRowAndColumn::ap_const_lv6_0 = "000000";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_9 = "1001";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_E = "1110";
const sc_lv<11> projection_gp_swapRowAndColumn::ap_const_lv11_0 = "00000000000";
const sc_lv<32> projection_gp_swapRowAndColumn::ap_const_lv32_29 = "101001";
const sc_lv<6> projection_gp_swapRowAndColumn::ap_const_lv6_29 = "101001";
const sc_lv<6> projection_gp_swapRowAndColumn::ap_const_lv6_1 = "1";
const sc_lv<10> projection_gp_swapRowAndColumn::ap_const_lv10_268 = "1001101000";
const sc_lv<11> projection_gp_swapRowAndColumn::ap_const_lv11_29 = "101001";
const sc_lv<11> projection_gp_swapRowAndColumn::ap_const_lv11_28 = "101000";

projection_gp_swapRowAndColumn::projection_gp_swapRowAndColumn(sc_module_name name) : sc_module(name), mVcdFile(0) {
    projection_gp_mul_32s_7ns_32_5_U1 = new projection_gp_mul_32s_7ns_32_5<1,5,32,7,32>("projection_gp_mul_32s_7ns_32_5_U1");
    projection_gp_mul_32s_7ns_32_5_U1->clk(ap_clk);
    projection_gp_mul_32s_7ns_32_5_U1->reset(ap_rst);
    projection_gp_mul_32s_7ns_32_5_U1->din0(rowA);
    projection_gp_mul_32s_7ns_32_5_U1->din1(grp_fu_106_p1);
    projection_gp_mul_32s_7ns_32_5_U1->ce(grp_fu_106_ce);
    projection_gp_mul_32s_7ns_32_5_U1->dout(grp_fu_106_p2);

    SC_METHOD(thread_ap_clk_no_reset_);
    dont_initialize();
    sensitive << ( ap_clk.pos() );

    SC_METHOD(thread_ap_done);
    sensitive << ( ap_start );
    sensitive << ( ap_sig_cseq_ST_st1_fsm_0 );
    sensitive << ( ap_sig_cseq_ST_st11_fsm_10 );
    sensitive << ( exitcond_fu_156_p2 );

    SC_METHOD(thread_ap_idle);
    sensitive << ( ap_start );
    sensitive << ( ap_sig_cseq_ST_st1_fsm_0 );

    SC_METHOD(thread_ap_ready);
    sensitive << ( ap_sig_cseq_ST_st11_fsm_10 );
    sensitive << ( exitcond_fu_156_p2 );

    SC_METHOD(thread_ap_sig_bdd_100);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_117);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_126);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_144);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_154);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_163);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_33);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_53);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_60);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_68);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_76);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_bdd_91);
    sensitive << ( ap_CS_fsm );

    SC_METHOD(thread_ap_sig_cseq_ST_st10_fsm_9);
    sensitive << ( ap_sig_bdd_154 );

    SC_METHOD(thread_ap_sig_cseq_ST_st11_fsm_10);
    sensitive << ( ap_sig_bdd_126 );

    SC_METHOD(thread_ap_sig_cseq_ST_st12_fsm_11);
    sensitive << ( ap_sig_bdd_144 );

    SC_METHOD(thread_ap_sig_cseq_ST_st13_fsm_12);
    sensitive << ( ap_sig_bdd_68 );

    SC_METHOD(thread_ap_sig_cseq_ST_st14_fsm_13);
    sensitive << ( ap_sig_bdd_76 );

    SC_METHOD(thread_ap_sig_cseq_ST_st15_fsm_14);
    sensitive << ( ap_sig_bdd_163 );

    SC_METHOD(thread_ap_sig_cseq_ST_st1_fsm_0);
    sensitive << ( ap_sig_bdd_33 );

    SC_METHOD(thread_ap_sig_cseq_ST_st5_fsm_4);
    sensitive << ( ap_sig_bdd_91 );

    SC_METHOD(thread_ap_sig_cseq_ST_st6_fsm_5);
    sensitive << ( ap_sig_bdd_100 );

    SC_METHOD(thread_ap_sig_cseq_ST_st7_fsm_6);
    sensitive << ( ap_sig_bdd_117 );

    SC_METHOD(thread_ap_sig_cseq_ST_st8_fsm_7);
    sensitive << ( ap_sig_bdd_53 );

    SC_METHOD(thread_ap_sig_cseq_ST_st9_fsm_8);
    sensitive << ( ap_sig_bdd_60 );

    SC_METHOD(thread_exitcond1_fu_120_p2);
    sensitive << ( ap_sig_cseq_ST_st6_fsm_5 );
    sensitive << ( i_reg_68 );

    SC_METHOD(thread_exitcond_fu_156_p2);
    sensitive << ( ap_sig_cseq_ST_st11_fsm_10 );
    sensitive << ( i1_reg_79 );

    SC_METHOD(thread_grp_fu_106_ce);

    SC_METHOD(thread_grp_fu_106_p1);
    sensitive << ( ap_sig_cseq_ST_st1_fsm_0 );

    SC_METHOD(thread_i_7_fu_126_p2);
    sensitive << ( i_reg_68 );

    SC_METHOD(thread_i_8_fu_162_p2);
    sensitive << ( i1_reg_79 );

    SC_METHOD(thread_i_cast3_fu_116_p1);
    sensitive << ( i_reg_68 );

    SC_METHOD(thread_i_cast4_cast_fu_112_p1);
    sensitive << ( i_reg_68 );

    SC_METHOD(thread_next_mul_fu_168_p2);
    sensitive << ( phi_mul_reg_90 );

    SC_METHOD(thread_pM_address0);
    sensitive << ( ap_sig_cseq_ST_st8_fsm_7 );
    sensitive << ( ap_sig_cseq_ST_st9_fsm_8 );
    sensitive << ( ap_sig_cseq_ST_st13_fsm_12 );
    sensitive << ( ap_sig_cseq_ST_st14_fsm_13 );
    sensitive << ( pM_addr_reg_217 );
    sensitive << ( pM_addr_1_reg_227 );
    sensitive << ( ap_sig_cseq_ST_st7_fsm_6 );
    sensitive << ( pM_addr_2_reg_245 );
    sensitive << ( pM_addr_3_reg_255 );
    sensitive << ( ap_sig_cseq_ST_st12_fsm_11 );
    sensitive << ( ap_sig_cseq_ST_st10_fsm_9 );
    sensitive << ( ap_sig_cseq_ST_st15_fsm_14 );
    sensitive << ( tmp_52_fu_151_p1 );
    sensitive << ( tmp_57_fu_194_p1 );

    SC_METHOD(thread_pM_ce0);
    sensitive << ( ap_sig_cseq_ST_st8_fsm_7 );
    sensitive << ( ap_sig_cseq_ST_st9_fsm_8 );
    sensitive << ( ap_sig_cseq_ST_st13_fsm_12 );
    sensitive << ( ap_sig_cseq_ST_st14_fsm_13 );
    sensitive << ( ap_sig_cseq_ST_st7_fsm_6 );
    sensitive << ( ap_sig_cseq_ST_st12_fsm_11 );
    sensitive << ( ap_sig_cseq_ST_st10_fsm_9 );
    sensitive << ( ap_sig_cseq_ST_st15_fsm_14 );

    SC_METHOD(thread_pM_d0);
    sensitive << ( reg_101 );
    sensitive << ( ap_sig_cseq_ST_st9_fsm_8 );
    sensitive << ( ap_sig_cseq_ST_st14_fsm_13 );
    sensitive << ( ap_sig_cseq_ST_st10_fsm_9 );
    sensitive << ( ap_sig_cseq_ST_st15_fsm_14 );

    SC_METHOD(thread_pM_we0);
    sensitive << ( ap_sig_cseq_ST_st9_fsm_8 );
    sensitive << ( ap_sig_cseq_ST_st14_fsm_13 );
    sensitive << ( ap_sig_cseq_ST_st10_fsm_9 );
    sensitive << ( ap_sig_cseq_ST_st15_fsm_14 );

    SC_METHOD(thread_tmp_50_fu_137_p1);
    sensitive << ( tmp_s_fu_132_p2 );

    SC_METHOD(thread_tmp_51_cast5_fu_148_p1);
    sensitive << ( tmp_51_reg_222 );

    SC_METHOD(thread_tmp_51_fu_142_p2);
    sensitive << ( i_cast4_cast_fu_112_p1 );

    SC_METHOD(thread_tmp_52_fu_151_p1);
    sensitive << ( tmp_51_cast5_fu_148_p1 );

    SC_METHOD(thread_tmp_53_cast6_fu_174_p1);
    sensitive << ( phi_mul_reg_90 );

    SC_METHOD(thread_tmp_54_fu_178_p1);
    sensitive << ( rowA );
    sensitive << ( ap_sig_cseq_ST_st11_fsm_10 );

    SC_METHOD(thread_tmp_54_fu_178_p2);
    sensitive << ( tmp_53_cast6_fu_174_p1 );
    sensitive << ( tmp_54_fu_178_p1 );

    SC_METHOD(thread_tmp_55_fu_183_p1);
    sensitive << ( tmp_54_fu_178_p2 );

    SC_METHOD(thread_tmp_56_fu_188_p2);
    sensitive << ( phi_mul_reg_90 );

    SC_METHOD(thread_tmp_57_fu_194_p1);
    sensitive << ( tmp_56_reg_250 );

    SC_METHOD(thread_tmp_s_fu_132_p2);
    sensitive << ( tmp_reg_204 );
    sensitive << ( i_cast3_fu_116_p1 );

    SC_METHOD(thread_ap_NS_fsm);
    sensitive << ( ap_start );
    sensitive << ( ap_CS_fsm );
    sensitive << ( exitcond1_fu_120_p2 );
    sensitive << ( exitcond_fu_156_p2 );

    ap_CS_fsm = "000000000000001";
    static int apTFileNum = 0;
    stringstream apTFilenSS;
    apTFilenSS << "projection_gp_swapRowAndColumn_sc_trace_" << apTFileNum ++;
    string apTFn = apTFilenSS.str();
    mVcdFile = sc_create_vcd_trace_file(apTFn.c_str());
    mVcdFile->set_time_unit(1, SC_PS);
    if (1) {
#ifdef __HLS_TRACE_LEVEL_PORT_HIER__
    sc_trace(mVcdFile, ap_clk, "(port)ap_clk");
    sc_trace(mVcdFile, ap_rst, "(port)ap_rst");
    sc_trace(mVcdFile, ap_start, "(port)ap_start");
    sc_trace(mVcdFile, ap_done, "(port)ap_done");
    sc_trace(mVcdFile, ap_idle, "(port)ap_idle");
    sc_trace(mVcdFile, ap_ready, "(port)ap_ready");
    sc_trace(mVcdFile, pM_address0, "(port)pM_address0");
    sc_trace(mVcdFile, pM_ce0, "(port)pM_ce0");
    sc_trace(mVcdFile, pM_we0, "(port)pM_we0");
    sc_trace(mVcdFile, pM_d0, "(port)pM_d0");
    sc_trace(mVcdFile, pM_q0, "(port)pM_q0");
    sc_trace(mVcdFile, rowA, "(port)rowA");
#endif
#ifdef __HLS_TRACE_LEVEL_INT__
    sc_trace(mVcdFile, ap_CS_fsm, "ap_CS_fsm");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st1_fsm_0, "ap_sig_cseq_ST_st1_fsm_0");
    sc_trace(mVcdFile, ap_sig_bdd_33, "ap_sig_bdd_33");
    sc_trace(mVcdFile, reg_101, "reg_101");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st8_fsm_7, "ap_sig_cseq_ST_st8_fsm_7");
    sc_trace(mVcdFile, ap_sig_bdd_53, "ap_sig_bdd_53");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st9_fsm_8, "ap_sig_cseq_ST_st9_fsm_8");
    sc_trace(mVcdFile, ap_sig_bdd_60, "ap_sig_bdd_60");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st13_fsm_12, "ap_sig_cseq_ST_st13_fsm_12");
    sc_trace(mVcdFile, ap_sig_bdd_68, "ap_sig_bdd_68");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st14_fsm_13, "ap_sig_cseq_ST_st14_fsm_13");
    sc_trace(mVcdFile, ap_sig_bdd_76, "ap_sig_bdd_76");
    sc_trace(mVcdFile, grp_fu_106_p2, "grp_fu_106_p2");
    sc_trace(mVcdFile, tmp_reg_204, "tmp_reg_204");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st5_fsm_4, "ap_sig_cseq_ST_st5_fsm_4");
    sc_trace(mVcdFile, ap_sig_bdd_91, "ap_sig_bdd_91");
    sc_trace(mVcdFile, i_7_fu_126_p2, "i_7_fu_126_p2");
    sc_trace(mVcdFile, i_7_reg_212, "i_7_reg_212");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st6_fsm_5, "ap_sig_cseq_ST_st6_fsm_5");
    sc_trace(mVcdFile, ap_sig_bdd_100, "ap_sig_bdd_100");
    sc_trace(mVcdFile, pM_addr_reg_217, "pM_addr_reg_217");
    sc_trace(mVcdFile, exitcond1_fu_120_p2, "exitcond1_fu_120_p2");
    sc_trace(mVcdFile, tmp_51_fu_142_p2, "tmp_51_fu_142_p2");
    sc_trace(mVcdFile, tmp_51_reg_222, "tmp_51_reg_222");
    sc_trace(mVcdFile, pM_addr_1_reg_227, "pM_addr_1_reg_227");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st7_fsm_6, "ap_sig_cseq_ST_st7_fsm_6");
    sc_trace(mVcdFile, ap_sig_bdd_117, "ap_sig_bdd_117");
    sc_trace(mVcdFile, i_8_fu_162_p2, "i_8_fu_162_p2");
    sc_trace(mVcdFile, i_8_reg_235, "i_8_reg_235");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st11_fsm_10, "ap_sig_cseq_ST_st11_fsm_10");
    sc_trace(mVcdFile, ap_sig_bdd_126, "ap_sig_bdd_126");
    sc_trace(mVcdFile, next_mul_fu_168_p2, "next_mul_fu_168_p2");
    sc_trace(mVcdFile, next_mul_reg_240, "next_mul_reg_240");
    sc_trace(mVcdFile, exitcond_fu_156_p2, "exitcond_fu_156_p2");
    sc_trace(mVcdFile, pM_addr_2_reg_245, "pM_addr_2_reg_245");
    sc_trace(mVcdFile, tmp_56_fu_188_p2, "tmp_56_fu_188_p2");
    sc_trace(mVcdFile, tmp_56_reg_250, "tmp_56_reg_250");
    sc_trace(mVcdFile, pM_addr_3_reg_255, "pM_addr_3_reg_255");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st12_fsm_11, "ap_sig_cseq_ST_st12_fsm_11");
    sc_trace(mVcdFile, ap_sig_bdd_144, "ap_sig_bdd_144");
    sc_trace(mVcdFile, i_reg_68, "i_reg_68");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st10_fsm_9, "ap_sig_cseq_ST_st10_fsm_9");
    sc_trace(mVcdFile, ap_sig_bdd_154, "ap_sig_bdd_154");
    sc_trace(mVcdFile, i1_reg_79, "i1_reg_79");
    sc_trace(mVcdFile, ap_sig_cseq_ST_st15_fsm_14, "ap_sig_cseq_ST_st15_fsm_14");
    sc_trace(mVcdFile, ap_sig_bdd_163, "ap_sig_bdd_163");
    sc_trace(mVcdFile, phi_mul_reg_90, "phi_mul_reg_90");
    sc_trace(mVcdFile, tmp_50_fu_137_p1, "tmp_50_fu_137_p1");
    sc_trace(mVcdFile, tmp_52_fu_151_p1, "tmp_52_fu_151_p1");
    sc_trace(mVcdFile, tmp_55_fu_183_p1, "tmp_55_fu_183_p1");
    sc_trace(mVcdFile, tmp_57_fu_194_p1, "tmp_57_fu_194_p1");
    sc_trace(mVcdFile, grp_fu_106_p1, "grp_fu_106_p1");
    sc_trace(mVcdFile, i_cast3_fu_116_p1, "i_cast3_fu_116_p1");
    sc_trace(mVcdFile, tmp_s_fu_132_p2, "tmp_s_fu_132_p2");
    sc_trace(mVcdFile, i_cast4_cast_fu_112_p1, "i_cast4_cast_fu_112_p1");
    sc_trace(mVcdFile, tmp_51_cast5_fu_148_p1, "tmp_51_cast5_fu_148_p1");
    sc_trace(mVcdFile, tmp_53_cast6_fu_174_p1, "tmp_53_cast6_fu_174_p1");
    sc_trace(mVcdFile, tmp_54_fu_178_p1, "tmp_54_fu_178_p1");
    sc_trace(mVcdFile, tmp_54_fu_178_p2, "tmp_54_fu_178_p2");
    sc_trace(mVcdFile, grp_fu_106_ce, "grp_fu_106_ce");
    sc_trace(mVcdFile, ap_NS_fsm, "ap_NS_fsm");
#endif

    }
}

projection_gp_swapRowAndColumn::~projection_gp_swapRowAndColumn() {
    if (mVcdFile) 
        sc_close_vcd_trace_file(mVcdFile);

    delete projection_gp_mul_32s_7ns_32_5_U1;
}

void projection_gp_swapRowAndColumn::thread_ap_clk_no_reset_() {
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_CS_fsm = ap_ST_st1_fsm_0;
    } else {
        ap_CS_fsm = ap_NS_fsm.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st6_fsm_5.read()) && 
         !esl_seteq<1,1,1>(exitcond1_fu_120_p2.read(), ap_const_lv1_0))) {
        i1_reg_79 = ap_const_lv6_0;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st15_fsm_14.read())) {
        i1_reg_79 = i_8_reg_235.read();
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_9.read())) {
        i_reg_68 = i_7_reg_212.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st5_fsm_4.read())) {
        i_reg_68 = ap_const_lv6_0;
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st6_fsm_5.read()) && 
         !esl_seteq<1,1,1>(exitcond1_fu_120_p2.read(), ap_const_lv1_0))) {
        phi_mul_reg_90 = ap_const_lv11_0;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st15_fsm_14.read())) {
        phi_mul_reg_90 = next_mul_reg_240.read();
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st6_fsm_5.read())) {
        i_7_reg_212 = i_7_fu_126_p2.read();
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_10.read())) {
        i_8_reg_235 = i_8_fu_162_p2.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_10.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_fu_156_p2.read()))) {
        next_mul_reg_240 = next_mul_fu_168_p2.read();
        pM_addr_2_reg_245 =  (sc_lv<11>) (tmp_55_fu_183_p1.read());
        tmp_56_reg_250 = tmp_56_fu_188_p2.read();
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st7_fsm_6.read())) {
        pM_addr_1_reg_227 =  (sc_lv<11>) (tmp_52_fu_151_p1.read());
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st12_fsm_11.read())) {
        pM_addr_3_reg_255 =  (sc_lv<11>) (tmp_57_fu_194_p1.read());
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st6_fsm_5.read()) && esl_seteq<1,1,1>(exitcond1_fu_120_p2.read(), ap_const_lv1_0))) {
        pM_addr_reg_217 =  (sc_lv<11>) (tmp_50_fu_137_p1.read());
        tmp_51_reg_222 = tmp_51_fu_142_p2.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st8_fsm_7.read()) || esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st9_fsm_8.read()) || esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st13_fsm_12.read()) || esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read()))) {
        reg_101 = pM_q0.read();
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st5_fsm_4.read())) {
        tmp_reg_204 = grp_fu_106_p2.read();
    }
}

void projection_gp_swapRowAndColumn::thread_ap_done() {
    if (((!esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_10.read()) && 
          !esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_fu_156_p2.read())))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_idle() {
    if ((!esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_10.read()) && 
         !esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_fu_156_p2.read()))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_100() {
    ap_sig_bdd_100 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(5, 5));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_117() {
    ap_sig_bdd_117 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(6, 6));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_126() {
    ap_sig_bdd_126 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(10, 10));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_144() {
    ap_sig_bdd_144 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(11, 11));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_154() {
    ap_sig_bdd_154 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(9, 9));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_163() {
    ap_sig_bdd_163 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(14, 14));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_33() {
    ap_sig_bdd_33 = esl_seteq<1,1,1>(ap_CS_fsm.read().range(0, 0), ap_const_lv1_1);
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_53() {
    ap_sig_bdd_53 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(7, 7));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_60() {
    ap_sig_bdd_60 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(8, 8));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_68() {
    ap_sig_bdd_68 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(12, 12));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_76() {
    ap_sig_bdd_76 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(13, 13));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_bdd_91() {
    ap_sig_bdd_91 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(4, 4));
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st10_fsm_9() {
    if (ap_sig_bdd_154.read()) {
        ap_sig_cseq_ST_st10_fsm_9 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st10_fsm_9 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st11_fsm_10() {
    if (ap_sig_bdd_126.read()) {
        ap_sig_cseq_ST_st11_fsm_10 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st11_fsm_10 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st12_fsm_11() {
    if (ap_sig_bdd_144.read()) {
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st13_fsm_12() {
    if (ap_sig_bdd_68.read()) {
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st14_fsm_13() {
    if (ap_sig_bdd_76.read()) {
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st15_fsm_14() {
    if (ap_sig_bdd_163.read()) {
        ap_sig_cseq_ST_st15_fsm_14 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st15_fsm_14 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st1_fsm_0() {
    if (ap_sig_bdd_33.read()) {
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st5_fsm_4() {
    if (ap_sig_bdd_91.read()) {
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st6_fsm_5() {
    if (ap_sig_bdd_100.read()) {
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st7_fsm_6() {
    if (ap_sig_bdd_117.read()) {
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st8_fsm_7() {
    if (ap_sig_bdd_53.read()) {
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_ap_sig_cseq_ST_st9_fsm_8() {
    if (ap_sig_bdd_60.read()) {
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_exitcond1_fu_120_p2() {
    exitcond1_fu_120_p2 = (!i_reg_68.read().is_01() || !ap_const_lv6_29.is_01())? sc_lv<1>(): sc_lv<1>(i_reg_68.read() == ap_const_lv6_29);
}

void projection_gp_swapRowAndColumn::thread_exitcond_fu_156_p2() {
    exitcond_fu_156_p2 = (!i1_reg_79.read().is_01() || !ap_const_lv6_29.is_01())? sc_lv<1>(): sc_lv<1>(i1_reg_79.read() == ap_const_lv6_29);
}

void projection_gp_swapRowAndColumn::thread_grp_fu_106_ce() {
    grp_fu_106_ce = ap_const_logic_1;
}

void projection_gp_swapRowAndColumn::thread_grp_fu_106_p1() {
    grp_fu_106_p1 =  (sc_lv<7>) (ap_const_lv32_29);
}

void projection_gp_swapRowAndColumn::thread_i_7_fu_126_p2() {
    i_7_fu_126_p2 = (!i_reg_68.read().is_01() || !ap_const_lv6_1.is_01())? sc_lv<6>(): (sc_biguint<6>(i_reg_68.read()) + sc_biguint<6>(ap_const_lv6_1));
}

void projection_gp_swapRowAndColumn::thread_i_8_fu_162_p2() {
    i_8_fu_162_p2 = (!i1_reg_79.read().is_01() || !ap_const_lv6_1.is_01())? sc_lv<6>(): (sc_biguint<6>(i1_reg_79.read()) + sc_biguint<6>(ap_const_lv6_1));
}

void projection_gp_swapRowAndColumn::thread_i_cast3_fu_116_p1() {
    i_cast3_fu_116_p1 = esl_zext<32,6>(i_reg_68.read());
}

void projection_gp_swapRowAndColumn::thread_i_cast4_cast_fu_112_p1() {
    i_cast4_cast_fu_112_p1 = esl_zext<10,6>(i_reg_68.read());
}

void projection_gp_swapRowAndColumn::thread_next_mul_fu_168_p2() {
    next_mul_fu_168_p2 = (!phi_mul_reg_90.read().is_01() || !ap_const_lv11_29.is_01())? sc_lv<11>(): (sc_biguint<11>(phi_mul_reg_90.read()) + sc_biguint<11>(ap_const_lv11_29));
}

void projection_gp_swapRowAndColumn::thread_pM_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st15_fsm_14.read())) {
        pM_address0 = pM_addr_3_reg_255.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_9.read())) {
        pM_address0 = pM_addr_1_reg_227.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st13_fsm_12.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read()))) {
        pM_address0 = pM_addr_2_reg_245.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st12_fsm_11.read())) {
        pM_address0 =  (sc_lv<11>) (tmp_57_fu_194_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st8_fsm_7.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st9_fsm_8.read()))) {
        pM_address0 = pM_addr_reg_217.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st7_fsm_6.read())) {
        pM_address0 =  (sc_lv<11>) (tmp_52_fu_151_p1.read());
    } else {
        pM_address0 = "XXXXXXXXXXX";
    }
}

void projection_gp_swapRowAndColumn::thread_pM_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st8_fsm_7.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st9_fsm_8.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st13_fsm_12.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st7_fsm_6.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st12_fsm_11.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_9.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st15_fsm_14.read()))) {
        pM_ce0 = ap_const_logic_1;
    } else {
        pM_ce0 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_pM_d0() {
    pM_d0 = reg_101.read();
}

void projection_gp_swapRowAndColumn::thread_pM_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st9_fsm_8.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_9.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st15_fsm_14.read()))) {
        pM_we0 = ap_const_logic_1;
    } else {
        pM_we0 = ap_const_logic_0;
    }
}

void projection_gp_swapRowAndColumn::thread_tmp_50_fu_137_p1() {
    tmp_50_fu_137_p1 = esl_zext<64,32>(tmp_s_fu_132_p2.read());
}

void projection_gp_swapRowAndColumn::thread_tmp_51_cast5_fu_148_p1() {
    tmp_51_cast5_fu_148_p1 = esl_sext<11,10>(tmp_51_reg_222.read());
}

void projection_gp_swapRowAndColumn::thread_tmp_51_fu_142_p2() {
    tmp_51_fu_142_p2 = (!i_cast4_cast_fu_112_p1.read().is_01() || !ap_const_lv10_268.is_01())? sc_lv<10>(): (sc_biguint<10>(i_cast4_cast_fu_112_p1.read()) + sc_bigint<10>(ap_const_lv10_268));
}

void projection_gp_swapRowAndColumn::thread_tmp_52_fu_151_p1() {
    tmp_52_fu_151_p1 = esl_zext<64,11>(tmp_51_cast5_fu_148_p1.read());
}

void projection_gp_swapRowAndColumn::thread_tmp_53_cast6_fu_174_p1() {
    tmp_53_cast6_fu_174_p1 = esl_zext<32,11>(phi_mul_reg_90.read());
}

void projection_gp_swapRowAndColumn::thread_tmp_54_fu_178_p1() {
    tmp_54_fu_178_p1 = rowA.read();
}

void projection_gp_swapRowAndColumn::thread_tmp_54_fu_178_p2() {
    tmp_54_fu_178_p2 = (!tmp_53_cast6_fu_174_p1.read().is_01() || !tmp_54_fu_178_p1.read().is_01())? sc_lv<32>(): (sc_biguint<32>(tmp_53_cast6_fu_174_p1.read()) + sc_bigint<32>(tmp_54_fu_178_p1.read()));
}

void projection_gp_swapRowAndColumn::thread_tmp_55_fu_183_p1() {
    tmp_55_fu_183_p1 = esl_zext<64,32>(tmp_54_fu_178_p2.read());
}

void projection_gp_swapRowAndColumn::thread_tmp_56_fu_188_p2() {
    tmp_56_fu_188_p2 = (!phi_mul_reg_90.read().is_01() || !ap_const_lv11_28.is_01())? sc_lv<11>(): (sc_biguint<11>(phi_mul_reg_90.read()) + sc_biguint<11>(ap_const_lv11_28));
}

void projection_gp_swapRowAndColumn::thread_tmp_57_fu_194_p1() {
    tmp_57_fu_194_p1 = esl_zext<64,11>(tmp_56_reg_250.read());
}

void projection_gp_swapRowAndColumn::thread_tmp_s_fu_132_p2() {
    tmp_s_fu_132_p2 = (!i_cast3_fu_116_p1.read().is_01() || !tmp_reg_204.read().is_01())? sc_lv<32>(): (sc_biguint<32>(i_cast3_fu_116_p1.read()) + sc_biguint<32>(tmp_reg_204.read()));
}

void projection_gp_swapRowAndColumn::thread_ap_NS_fsm() {
    switch (ap_CS_fsm.read().to_uint64()) {
        case 1 : 
            if (!esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_0)) {
                ap_NS_fsm = ap_ST_st2_fsm_1;
            } else {
                ap_NS_fsm = ap_ST_st1_fsm_0;
            }
            break;
        case 2 : 
            ap_NS_fsm = ap_ST_st3_fsm_2;
            break;
        case 4 : 
            ap_NS_fsm = ap_ST_st4_fsm_3;
            break;
        case 8 : 
            ap_NS_fsm = ap_ST_st5_fsm_4;
            break;
        case 16 : 
            ap_NS_fsm = ap_ST_st6_fsm_5;
            break;
        case 32 : 
            if (!esl_seteq<1,1,1>(exitcond1_fu_120_p2.read(), ap_const_lv1_0)) {
                ap_NS_fsm = ap_ST_st11_fsm_10;
            } else {
                ap_NS_fsm = ap_ST_st7_fsm_6;
            }
            break;
        case 64 : 
            ap_NS_fsm = ap_ST_st8_fsm_7;
            break;
        case 128 : 
            ap_NS_fsm = ap_ST_st9_fsm_8;
            break;
        case 256 : 
            ap_NS_fsm = ap_ST_st10_fsm_9;
            break;
        case 512 : 
            ap_NS_fsm = ap_ST_st6_fsm_5;
            break;
        case 1024 : 
            if (!esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_fu_156_p2.read())) {
                ap_NS_fsm = ap_ST_st1_fsm_0;
            } else {
                ap_NS_fsm = ap_ST_st12_fsm_11;
            }
            break;
        case 2048 : 
            ap_NS_fsm = ap_ST_st13_fsm_12;
            break;
        case 4096 : 
            ap_NS_fsm = ap_ST_st14_fsm_13;
            break;
        case 8192 : 
            ap_NS_fsm = ap_ST_st15_fsm_14;
            break;
        case 16384 : 
            ap_NS_fsm = ap_ST_st11_fsm_10;
            break;
        default : 
            ap_NS_fsm =  (sc_lv<15>) ("XXXXXXXXXXXXXXX");
            break;
    }
}

}

