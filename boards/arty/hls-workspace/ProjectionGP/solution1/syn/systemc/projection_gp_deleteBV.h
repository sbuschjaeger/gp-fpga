// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _projection_gp_deleteBV_HH_
#define _projection_gp_deleteBV_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "projection_gp_swapRowAndColumn.h"
#include "projection_gp_faddfsub_32ns_32ns_32_9_full_dsp.h"
#include "projection_gp_fsub_32ns_32ns_32_9_full_dsp.h"
#include "projection_gp_fmul_32ns_32ns_32_5_max_dsp.h"
#include "projection_gp_fdiv_32ns_32ns_32_30.h"
#include "projection_gp_mul_32s_5ns_32_5.h"

namespace ap_rtl {

struct projection_gp_deleteBV : public sc_module {
    // Port declarations 42
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<32> > pIndex;
    sc_out< sc_lv<6> > alpha_address0;
    sc_out< sc_logic > alpha_ce0;
    sc_out< sc_logic > alpha_we0;
    sc_out< sc_lv<32> > alpha_d0;
    sc_in< sc_lv<32> > alpha_q0;
    sc_out< sc_lv<11> > C_address0;
    sc_out< sc_logic > C_ce0;
    sc_out< sc_logic > C_we0;
    sc_out< sc_lv<32> > C_d0;
    sc_in< sc_lv<32> > C_q0;
    sc_out< sc_lv<11> > C_address1;
    sc_out< sc_logic > C_ce1;
    sc_out< sc_logic > C_we1;
    sc_out< sc_lv<32> > C_d1;
    sc_in< sc_lv<32> > C_q1;
    sc_out< sc_lv<11> > Q_address0;
    sc_out< sc_logic > Q_ce0;
    sc_out< sc_logic > Q_we0;
    sc_out< sc_lv<32> > Q_d0;
    sc_in< sc_lv<32> > Q_q0;
    sc_out< sc_lv<11> > Q_address1;
    sc_out< sc_logic > Q_ce1;
    sc_out< sc_logic > Q_we1;
    sc_out< sc_lv<32> > Q_d1;
    sc_in< sc_lv<32> > Q_q1;
    sc_out< sc_lv<10> > basisVectors_address0;
    sc_out< sc_logic > basisVectors_ce0;
    sc_out< sc_logic > basisVectors_we0;
    sc_out< sc_lv<32> > basisVectors_d0;
    sc_in< sc_lv<32> > basisVectors_q0;
    sc_out< sc_lv<10> > basisVectors_address1;
    sc_out< sc_logic > basisVectors_ce1;
    sc_out< sc_logic > basisVectors_we1;
    sc_out< sc_lv<32> > basisVectors_d1;
    sc_in< sc_lv<32> > basisVectors_q1;


    // Module declarations
    projection_gp_deleteBV(sc_module_name name);
    SC_HAS_PROCESS(projection_gp_deleteBV);

    ~projection_gp_deleteBV();

    sc_trace_file* mVcdFile;

    projection_gp_swapRowAndColumn* grp_projection_gp_swapRowAndColumn_fu_324;
    projection_gp_swapRowAndColumn* grp_projection_gp_swapRowAndColumn_fu_332;
    projection_gp_faddfsub_32ns_32ns_32_9_full_dsp<1,9,32,32,32>* projection_gp_faddfsub_32ns_32ns_32_9_full_dsp_U4;
    projection_gp_fsub_32ns_32ns_32_9_full_dsp<1,9,32,32,32>* projection_gp_fsub_32ns_32ns_32_9_full_dsp_U5;
    projection_gp_fmul_32ns_32ns_32_5_max_dsp<1,5,32,32,32>* projection_gp_fmul_32ns_32ns_32_5_max_dsp_U6;
    projection_gp_fmul_32ns_32ns_32_5_max_dsp<1,5,32,32,32>* projection_gp_fmul_32ns_32ns_32_5_max_dsp_U7;
    projection_gp_fmul_32ns_32ns_32_5_max_dsp<1,5,32,32,32>* projection_gp_fmul_32ns_32ns_32_5_max_dsp_U8;
    projection_gp_fmul_32ns_32ns_32_5_max_dsp<1,5,32,32,32>* projection_gp_fmul_32ns_32ns_32_5_max_dsp_U9;
    projection_gp_fdiv_32ns_32ns_32_30<1,30,32,32,32>* projection_gp_fdiv_32ns_32ns_32_30_U10;
    projection_gp_mul_32s_5ns_32_5<1,5,32,5,32>* projection_gp_mul_32s_5ns_32_5_U11;
    sc_signal< sc_lv<163> > ap_CS_fsm;
    sc_signal< sc_logic > ap_sig_cseq_ST_st1_fsm_0;
    sc_signal< bool > ap_sig_bdd_181;
    sc_signal< sc_lv<4> > i_reg_244;
    sc_signal< sc_lv<32> > reg_368;
    sc_signal< sc_logic > ap_sig_cseq_ST_st2_fsm_1;
    sc_signal< bool > ap_sig_bdd_242;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_324_ap_done;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_332_ap_done;
    sc_signal< sc_logic > ap_sig_cseq_ST_st3_fsm_2;
    sc_signal< bool > ap_sig_bdd_256;
    sc_signal< sc_logic > ap_sig_cseq_ST_st21_fsm_18;
    sc_signal< bool > ap_sig_bdd_264;
    sc_signal< sc_logic > ap_sig_cseq_ST_st68_fsm_65;
    sc_signal< bool > ap_sig_bdd_272;
    sc_signal< sc_lv<32> > reg_375;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg2_fsm_7;
    sc_signal< bool > ap_sig_bdd_281;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it0;
    sc_signal< sc_lv<1> > exitcond4_reg_658;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg0_fsm_5;
    sc_signal< bool > ap_sig_bdd_295;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it1;
    sc_signal< sc_lv<32> > reg_382;
    sc_signal< sc_logic > ap_sig_cseq_ST_st12_fsm_9;
    sc_signal< bool > ap_sig_bdd_307;
    sc_signal< sc_logic > ap_sig_cseq_ST_st54_fsm_51;
    sc_signal< bool > ap_sig_bdd_314;
    sc_signal< sc_logic > ap_sig_cseq_ST_st82_fsm_79;
    sc_signal< bool > ap_sig_bdd_322;
    sc_signal< sc_logic > ap_sig_cseq_ST_st153_fsm_150;
    sc_signal< bool > ap_sig_bdd_330;
    sc_signal< sc_lv<32> > grp_fu_364_p2;
    sc_signal< sc_lv<32> > reg_390;
    sc_signal< sc_logic > ap_sig_cseq_ST_st51_fsm_48;
    sc_signal< bool > ap_sig_bdd_339;
    sc_signal< sc_logic > ap_sig_cseq_ST_st135_fsm_132;
    sc_signal< bool > ap_sig_bdd_346;
    sc_signal< sc_lv<32> > reg_397;
    sc_signal< sc_logic > ap_sig_cseq_ST_st144_fsm_141;
    sc_signal< bool > ap_sig_bdd_356;
    sc_signal< sc_lv<32> > grp_fu_340_p2;
    sc_signal< sc_lv<32> > reg_406;
    sc_signal< sc_logic > ap_sig_cseq_ST_st63_fsm_60;
    sc_signal< bool > ap_sig_bdd_365;
    sc_signal< sc_logic > ap_sig_cseq_ST_st77_fsm_74;
    sc_signal< bool > ap_sig_bdd_372;
    sc_signal< sc_logic > ap_sig_cseq_ST_st96_fsm_93;
    sc_signal< bool > ap_sig_bdd_380;
    sc_signal< sc_logic > ap_sig_cseq_ST_st105_fsm_102;
    sc_signal< bool > ap_sig_bdd_388;
    sc_signal< sc_logic > ap_sig_cseq_ST_st114_fsm_111;
    sc_signal< bool > ap_sig_bdd_396;
    sc_signal< sc_logic > ap_sig_cseq_ST_st162_fsm_159;
    sc_signal< bool > ap_sig_bdd_405;
    sc_signal< sc_lv<32> > grp_fu_348_p2;
    sc_signal< sc_lv<32> > reg_416;
    sc_signal< sc_logic > ap_sig_cseq_ST_st87_fsm_84;
    sc_signal< bool > ap_sig_bdd_415;
    sc_signal< sc_lv<6> > alpha_addr_reg_648;
    sc_signal< sc_lv<32> > grp_fu_427_p2;
    sc_signal< sc_lv<32> > tmp_s_reg_653;
    sc_signal< sc_logic > ap_sig_cseq_ST_st5_fsm_4;
    sc_signal< bool > ap_sig_bdd_432;
    sc_signal< sc_lv<1> > exitcond4_fu_433_p2;
    sc_signal< sc_lv<1> > ap_reg_ppstg_exitcond4_reg_658_pp0_it1;
    sc_signal< sc_lv<4> > i_10_fu_439_p2;
    sc_signal< sc_lv<4> > i_10_reg_662;
    sc_signal< sc_lv<10> > tmp_69_fu_449_p2;
    sc_signal< sc_lv<10> > tmp_69_reg_667;
    sc_signal< sc_lv<10> > basisVectors_addr_reg_672;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg1_fsm_6;
    sc_signal< bool > ap_sig_bdd_451;
    sc_signal< sc_lv<10> > basisVectors_addr_1_reg_678;
    sc_signal< sc_lv<32> > qStar_reg_684;
    sc_signal< sc_lv<32> > tmp_65_reg_690;
    sc_signal< sc_lv<6> > i_11_fu_483_p2;
    sc_signal< sc_lv<6> > i_11_reg_698;
    sc_signal< sc_logic > ap_sig_cseq_ST_st52_fsm_49;
    sc_signal< bool > ap_sig_bdd_465;
    sc_signal< sc_lv<10> > a_fu_489_p2;
    sc_signal< sc_lv<10> > a_reg_703;
    sc_signal< sc_lv<1> > exitcond3_fu_477_p2;
    sc_signal< sc_logic > ap_sig_cseq_ST_st53_fsm_50;
    sc_signal< bool > ap_sig_bdd_479;
    sc_signal< sc_lv<6> > alpha_addr_3_reg_718;
    sc_signal< sc_logic > ap_sig_cseq_ST_st67_fsm_64;
    sc_signal< bool > ap_sig_bdd_490;
    sc_signal< sc_lv<11> > next_mul_fu_509_p2;
    sc_signal< sc_lv<11> > next_mul_reg_723;
    sc_signal< sc_logic > ap_sig_cseq_ST_st79_fsm_76;
    sc_signal< bool > ap_sig_bdd_499;
    sc_signal< sc_lv<6> > i_12_fu_525_p2;
    sc_signal< sc_lv<6> > i_12_reg_731;
    sc_signal< sc_lv<11> > Q_addr_2_reg_736;
    sc_signal< sc_lv<1> > exitcond2_fu_519_p2;
    sc_signal< sc_lv<11> > C_addr_2_reg_741;
    sc_signal< sc_lv<6> > j_5_fu_561_p2;
    sc_signal< sc_lv<6> > j_5_reg_749;
    sc_signal< sc_logic > ap_sig_cseq_ST_st80_fsm_77;
    sc_signal< bool > ap_sig_bdd_517;
    sc_signal< sc_lv<10> > b_1_fu_567_p2;
    sc_signal< sc_lv<10> > b_1_reg_754;
    sc_signal< sc_lv<1> > exitcond1_fu_555_p2;
    sc_signal< sc_lv<11> > c_fu_573_p2;
    sc_signal< sc_lv<11> > c_reg_759;
    sc_signal< sc_logic > ap_sig_cseq_ST_st81_fsm_78;
    sc_signal< bool > ap_sig_bdd_533;
    sc_signal< sc_lv<32> > Q_load_4_reg_774;
    sc_signal< sc_lv<32> > C_load_4_reg_780;
    sc_signal< sc_lv<32> > grp_fu_352_p2;
    sc_signal< sc_lv<32> > tmp_82_reg_786;
    sc_signal< sc_lv<32> > grp_fu_356_p2;
    sc_signal< sc_lv<32> > tmp_83_reg_791;
    sc_signal< sc_lv<32> > grp_fu_360_p2;
    sc_signal< sc_lv<32> > tmp_85_reg_796;
    sc_signal< sc_lv<11> > C_addr_6_reg_801;
    sc_signal< sc_logic > ap_sig_cseq_ST_st143_fsm_140;
    sc_signal< bool > ap_sig_bdd_552;
    sc_signal< sc_lv<11> > Q_addr_6_reg_806;
    sc_signal< sc_lv<32> > tmp_88_reg_811;
    sc_signal< sc_lv<32> > grp_fu_344_p2;
    sc_signal< sc_lv<32> > tmp_92_reg_816;
    sc_signal< sc_lv<6> > i_3_fu_603_p2;
    sc_signal< sc_lv<6> > i_3_reg_824;
    sc_signal< sc_logic > ap_sig_cseq_ST_st164_fsm_161;
    sc_signal< bool > ap_sig_bdd_566;
    sc_signal< sc_lv<11> > next_mul7_fu_609_p2;
    sc_signal< sc_lv<11> > next_mul7_reg_829;
    sc_signal< sc_lv<1> > exitcond_fu_597_p2;
    sc_signal< sc_lv<11> > a_1_fu_615_p2;
    sc_signal< sc_lv<11> > a_1_reg_834;
    sc_signal< sc_lv<10> > b_fu_621_p2;
    sc_signal< sc_lv<10> > b_reg_839;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_324_ap_start;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_324_ap_idle;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_324_ap_ready;
    sc_signal< sc_lv<11> > grp_projection_gp_swapRowAndColumn_fu_324_pM_address0;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_324_pM_ce0;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_324_pM_we0;
    sc_signal< sc_lv<32> > grp_projection_gp_swapRowAndColumn_fu_324_pM_d0;
    sc_signal< sc_lv<32> > grp_projection_gp_swapRowAndColumn_fu_324_pM_q0;
    sc_signal< sc_lv<32> > grp_projection_gp_swapRowAndColumn_fu_324_rowA;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_332_ap_start;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_332_ap_idle;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_332_ap_ready;
    sc_signal< sc_lv<11> > grp_projection_gp_swapRowAndColumn_fu_332_pM_address0;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_332_pM_ce0;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_332_pM_we0;
    sc_signal< sc_lv<32> > grp_projection_gp_swapRowAndColumn_fu_332_pM_d0;
    sc_signal< sc_lv<32> > grp_projection_gp_swapRowAndColumn_fu_332_pM_q0;
    sc_signal< sc_lv<32> > grp_projection_gp_swapRowAndColumn_fu_332_rowA;
    sc_signal< sc_lv<4> > i_phi_fu_248_p4;
    sc_signal< sc_lv<6> > i1_reg_256;
    sc_signal< sc_logic > ap_sig_cseq_ST_st78_fsm_75;
    sc_signal< bool > ap_sig_bdd_614;
    sc_signal< sc_lv<6> > i2_reg_268;
    sc_signal< sc_lv<11> > phi_mul_reg_279;
    sc_signal< sc_lv<6> > j_reg_291;
    sc_signal< sc_logic > ap_sig_cseq_ST_st163_fsm_160;
    sc_signal< bool > ap_sig_bdd_632;
    sc_signal< sc_lv<6> > i5_reg_302;
    sc_signal< sc_logic > ap_sig_cseq_ST_st165_fsm_162;
    sc_signal< bool > ap_sig_bdd_641;
    sc_signal< sc_lv<11> > phi_mul6_reg_313;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_324_ap_start_ap_start_reg;
    sc_signal< sc_logic > grp_projection_gp_swapRowAndColumn_fu_332_ap_start_ap_start_reg;
    sc_signal< sc_lv<64> > tmp_fu_422_p1;
    sc_signal< sc_lv<64> > tmp_68_fu_464_p1;
    sc_signal< sc_lv<64> > tmp_70_fu_469_p1;
    sc_signal< sc_lv<64> > tmp_71_fu_498_p1;
    sc_signal< sc_lv<64> > tmp_75_fu_504_p1;
    sc_signal< sc_lv<64> > tmp_77_fu_541_p1;
    sc_signal< sc_lv<64> > tmp_80_fu_582_p1;
    sc_signal< sc_lv<64> > tmp_90_fu_588_p1;
    sc_signal< sc_lv<64> > tmp_78_fu_630_p1;
    sc_signal< sc_lv<64> > tmp_79_fu_635_p1;
    sc_signal< sc_logic > ap_sig_cseq_ST_st4_fsm_3;
    sc_signal< bool > ap_sig_bdd_674;
    sc_signal< sc_logic > ap_sig_cseq_ST_st20_fsm_17;
    sc_signal< bool > ap_sig_bdd_682;
    sc_signal< sc_logic > ap_sig_cseq_ST_st11_fsm_8;
    sc_signal< bool > ap_sig_bdd_708;
    sc_signal< sc_logic > ap_sig_cseq_ST_st152_fsm_149;
    sc_signal< bool > ap_sig_bdd_720;
    sc_signal< sc_logic > ap_sig_cseq_ST_st154_fsm_151;
    sc_signal< bool > ap_sig_bdd_733;
    sc_signal< sc_lv<32> > grp_fu_340_p0;
    sc_signal< sc_lv<32> > grp_fu_340_p1;
    sc_signal< sc_logic > ap_sig_cseq_ST_st13_fsm_10;
    sc_signal< bool > ap_sig_bdd_745;
    sc_signal< sc_logic > ap_sig_cseq_ST_st55_fsm_52;
    sc_signal< bool > ap_sig_bdd_752;
    sc_signal< sc_logic > ap_sig_cseq_ST_st69_fsm_66;
    sc_signal< bool > ap_sig_bdd_760;
    sc_signal< sc_logic > ap_sig_cseq_ST_st88_fsm_85;
    sc_signal< bool > ap_sig_bdd_767;
    sc_signal< sc_logic > ap_sig_cseq_ST_st97_fsm_94;
    sc_signal< bool > ap_sig_bdd_774;
    sc_signal< sc_logic > ap_sig_cseq_ST_st106_fsm_103;
    sc_signal< bool > ap_sig_bdd_781;
    sc_signal< sc_logic > ap_sig_cseq_ST_st145_fsm_142;
    sc_signal< bool > ap_sig_bdd_789;
    sc_signal< sc_lv<32> > grp_fu_348_p0;
    sc_signal< sc_lv<32> > grp_fu_348_p1;
    sc_signal< sc_logic > ap_sig_cseq_ST_st64_fsm_61;
    sc_signal< bool > ap_sig_bdd_802;
    sc_signal< sc_logic > ap_sig_cseq_ST_st83_fsm_80;
    sc_signal< bool > ap_sig_bdd_809;
    sc_signal< sc_lv<32> > grp_fu_364_p0;
    sc_signal< sc_lv<32> > grp_fu_364_p1;
    sc_signal< sc_logic > ap_sig_cseq_ST_st22_fsm_19;
    sc_signal< bool > ap_sig_bdd_824;
    sc_signal< sc_logic > ap_sig_cseq_ST_st115_fsm_112;
    sc_signal< bool > ap_sig_bdd_831;
    sc_signal< sc_lv<32> > tmp_fu_422_p0;
    sc_signal< sc_lv<5> > grp_fu_427_p1;
    sc_signal< sc_lv<10> > i_cast9_fu_445_p1;
    sc_signal< sc_lv<32> > i_cast8_fu_455_p1;
    sc_signal< sc_lv<32> > tmp_67_fu_459_p2;
    sc_signal< sc_lv<10> > i1_cast7_cast_fu_473_p1;
    sc_signal< sc_lv<11> > a_cast_fu_495_p1;
    sc_signal< sc_lv<10> > i2_cast4_cast_fu_515_p1;
    sc_signal< sc_lv<10> > a_2_fu_531_p2;
    sc_signal< sc_lv<11> > a_2_cast_fu_537_p1;
    sc_signal< sc_lv<10> > j_cast3_cast_fu_551_p1;
    sc_signal< sc_lv<11> > j_cast3_fu_547_p1;
    sc_signal< sc_lv<11> > b_1_cast_fu_579_p1;
    sc_signal< sc_lv<10> > i5_cast2_cast_fu_593_p1;
    sc_signal< sc_lv<11> > b_cast_fu_627_p1;
    sc_signal< sc_lv<2> > grp_fu_340_opcode;
    sc_signal< sc_logic > grp_fu_340_ce;
    sc_signal< sc_logic > grp_fu_344_ce;
    sc_signal< sc_logic > grp_fu_348_ce;
    sc_signal< sc_logic > grp_fu_352_ce;
    sc_signal< sc_logic > grp_fu_356_ce;
    sc_signal< sc_logic > grp_fu_360_ce;
    sc_signal< sc_logic > grp_fu_364_ce;
    sc_signal< sc_logic > grp_fu_427_ce;
    sc_signal< sc_lv<163> > ap_NS_fsm;
    sc_signal< bool > ap_sig_bdd_286;
    sc_signal< bool > ap_sig_bdd_300;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<163> ap_ST_st1_fsm_0;
    static const sc_lv<163> ap_ST_st2_fsm_1;
    static const sc_lv<163> ap_ST_st3_fsm_2;
    static const sc_lv<163> ap_ST_st4_fsm_3;
    static const sc_lv<163> ap_ST_st5_fsm_4;
    static const sc_lv<163> ap_ST_pp0_stg0_fsm_5;
    static const sc_lv<163> ap_ST_pp0_stg1_fsm_6;
    static const sc_lv<163> ap_ST_pp0_stg2_fsm_7;
    static const sc_lv<163> ap_ST_st11_fsm_8;
    static const sc_lv<163> ap_ST_st12_fsm_9;
    static const sc_lv<163> ap_ST_st13_fsm_10;
    static const sc_lv<163> ap_ST_st14_fsm_11;
    static const sc_lv<163> ap_ST_st15_fsm_12;
    static const sc_lv<163> ap_ST_st16_fsm_13;
    static const sc_lv<163> ap_ST_st17_fsm_14;
    static const sc_lv<163> ap_ST_st18_fsm_15;
    static const sc_lv<163> ap_ST_st19_fsm_16;
    static const sc_lv<163> ap_ST_st20_fsm_17;
    static const sc_lv<163> ap_ST_st21_fsm_18;
    static const sc_lv<163> ap_ST_st22_fsm_19;
    static const sc_lv<163> ap_ST_st23_fsm_20;
    static const sc_lv<163> ap_ST_st24_fsm_21;
    static const sc_lv<163> ap_ST_st25_fsm_22;
    static const sc_lv<163> ap_ST_st26_fsm_23;
    static const sc_lv<163> ap_ST_st27_fsm_24;
    static const sc_lv<163> ap_ST_st28_fsm_25;
    static const sc_lv<163> ap_ST_st29_fsm_26;
    static const sc_lv<163> ap_ST_st30_fsm_27;
    static const sc_lv<163> ap_ST_st31_fsm_28;
    static const sc_lv<163> ap_ST_st32_fsm_29;
    static const sc_lv<163> ap_ST_st33_fsm_30;
    static const sc_lv<163> ap_ST_st34_fsm_31;
    static const sc_lv<163> ap_ST_st35_fsm_32;
    static const sc_lv<163> ap_ST_st36_fsm_33;
    static const sc_lv<163> ap_ST_st37_fsm_34;
    static const sc_lv<163> ap_ST_st38_fsm_35;
    static const sc_lv<163> ap_ST_st39_fsm_36;
    static const sc_lv<163> ap_ST_st40_fsm_37;
    static const sc_lv<163> ap_ST_st41_fsm_38;
    static const sc_lv<163> ap_ST_st42_fsm_39;
    static const sc_lv<163> ap_ST_st43_fsm_40;
    static const sc_lv<163> ap_ST_st44_fsm_41;
    static const sc_lv<163> ap_ST_st45_fsm_42;
    static const sc_lv<163> ap_ST_st46_fsm_43;
    static const sc_lv<163> ap_ST_st47_fsm_44;
    static const sc_lv<163> ap_ST_st48_fsm_45;
    static const sc_lv<163> ap_ST_st49_fsm_46;
    static const sc_lv<163> ap_ST_st50_fsm_47;
    static const sc_lv<163> ap_ST_st51_fsm_48;
    static const sc_lv<163> ap_ST_st52_fsm_49;
    static const sc_lv<163> ap_ST_st53_fsm_50;
    static const sc_lv<163> ap_ST_st54_fsm_51;
    static const sc_lv<163> ap_ST_st55_fsm_52;
    static const sc_lv<163> ap_ST_st56_fsm_53;
    static const sc_lv<163> ap_ST_st57_fsm_54;
    static const sc_lv<163> ap_ST_st58_fsm_55;
    static const sc_lv<163> ap_ST_st59_fsm_56;
    static const sc_lv<163> ap_ST_st60_fsm_57;
    static const sc_lv<163> ap_ST_st61_fsm_58;
    static const sc_lv<163> ap_ST_st62_fsm_59;
    static const sc_lv<163> ap_ST_st63_fsm_60;
    static const sc_lv<163> ap_ST_st64_fsm_61;
    static const sc_lv<163> ap_ST_st65_fsm_62;
    static const sc_lv<163> ap_ST_st66_fsm_63;
    static const sc_lv<163> ap_ST_st67_fsm_64;
    static const sc_lv<163> ap_ST_st68_fsm_65;
    static const sc_lv<163> ap_ST_st69_fsm_66;
    static const sc_lv<163> ap_ST_st70_fsm_67;
    static const sc_lv<163> ap_ST_st71_fsm_68;
    static const sc_lv<163> ap_ST_st72_fsm_69;
    static const sc_lv<163> ap_ST_st73_fsm_70;
    static const sc_lv<163> ap_ST_st74_fsm_71;
    static const sc_lv<163> ap_ST_st75_fsm_72;
    static const sc_lv<163> ap_ST_st76_fsm_73;
    static const sc_lv<163> ap_ST_st77_fsm_74;
    static const sc_lv<163> ap_ST_st78_fsm_75;
    static const sc_lv<163> ap_ST_st79_fsm_76;
    static const sc_lv<163> ap_ST_st80_fsm_77;
    static const sc_lv<163> ap_ST_st81_fsm_78;
    static const sc_lv<163> ap_ST_st82_fsm_79;
    static const sc_lv<163> ap_ST_st83_fsm_80;
    static const sc_lv<163> ap_ST_st84_fsm_81;
    static const sc_lv<163> ap_ST_st85_fsm_82;
    static const sc_lv<163> ap_ST_st86_fsm_83;
    static const sc_lv<163> ap_ST_st87_fsm_84;
    static const sc_lv<163> ap_ST_st88_fsm_85;
    static const sc_lv<163> ap_ST_st89_fsm_86;
    static const sc_lv<163> ap_ST_st90_fsm_87;
    static const sc_lv<163> ap_ST_st91_fsm_88;
    static const sc_lv<163> ap_ST_st92_fsm_89;
    static const sc_lv<163> ap_ST_st93_fsm_90;
    static const sc_lv<163> ap_ST_st94_fsm_91;
    static const sc_lv<163> ap_ST_st95_fsm_92;
    static const sc_lv<163> ap_ST_st96_fsm_93;
    static const sc_lv<163> ap_ST_st97_fsm_94;
    static const sc_lv<163> ap_ST_st98_fsm_95;
    static const sc_lv<163> ap_ST_st99_fsm_96;
    static const sc_lv<163> ap_ST_st100_fsm_97;
    static const sc_lv<163> ap_ST_st101_fsm_98;
    static const sc_lv<163> ap_ST_st102_fsm_99;
    static const sc_lv<163> ap_ST_st103_fsm_100;
    static const sc_lv<163> ap_ST_st104_fsm_101;
    static const sc_lv<163> ap_ST_st105_fsm_102;
    static const sc_lv<163> ap_ST_st106_fsm_103;
    static const sc_lv<163> ap_ST_st107_fsm_104;
    static const sc_lv<163> ap_ST_st108_fsm_105;
    static const sc_lv<163> ap_ST_st109_fsm_106;
    static const sc_lv<163> ap_ST_st110_fsm_107;
    static const sc_lv<163> ap_ST_st111_fsm_108;
    static const sc_lv<163> ap_ST_st112_fsm_109;
    static const sc_lv<163> ap_ST_st113_fsm_110;
    static const sc_lv<163> ap_ST_st114_fsm_111;
    static const sc_lv<163> ap_ST_st115_fsm_112;
    static const sc_lv<163> ap_ST_st116_fsm_113;
    static const sc_lv<163> ap_ST_st117_fsm_114;
    static const sc_lv<163> ap_ST_st118_fsm_115;
    static const sc_lv<163> ap_ST_st119_fsm_116;
    static const sc_lv<163> ap_ST_st120_fsm_117;
    static const sc_lv<163> ap_ST_st121_fsm_118;
    static const sc_lv<163> ap_ST_st122_fsm_119;
    static const sc_lv<163> ap_ST_st123_fsm_120;
    static const sc_lv<163> ap_ST_st124_fsm_121;
    static const sc_lv<163> ap_ST_st125_fsm_122;
    static const sc_lv<163> ap_ST_st126_fsm_123;
    static const sc_lv<163> ap_ST_st127_fsm_124;
    static const sc_lv<163> ap_ST_st128_fsm_125;
    static const sc_lv<163> ap_ST_st129_fsm_126;
    static const sc_lv<163> ap_ST_st130_fsm_127;
    static const sc_lv<163> ap_ST_st131_fsm_128;
    static const sc_lv<163> ap_ST_st132_fsm_129;
    static const sc_lv<163> ap_ST_st133_fsm_130;
    static const sc_lv<163> ap_ST_st134_fsm_131;
    static const sc_lv<163> ap_ST_st135_fsm_132;
    static const sc_lv<163> ap_ST_st136_fsm_133;
    static const sc_lv<163> ap_ST_st137_fsm_134;
    static const sc_lv<163> ap_ST_st138_fsm_135;
    static const sc_lv<163> ap_ST_st139_fsm_136;
    static const sc_lv<163> ap_ST_st140_fsm_137;
    static const sc_lv<163> ap_ST_st141_fsm_138;
    static const sc_lv<163> ap_ST_st142_fsm_139;
    static const sc_lv<163> ap_ST_st143_fsm_140;
    static const sc_lv<163> ap_ST_st144_fsm_141;
    static const sc_lv<163> ap_ST_st145_fsm_142;
    static const sc_lv<163> ap_ST_st146_fsm_143;
    static const sc_lv<163> ap_ST_st147_fsm_144;
    static const sc_lv<163> ap_ST_st148_fsm_145;
    static const sc_lv<163> ap_ST_st149_fsm_146;
    static const sc_lv<163> ap_ST_st150_fsm_147;
    static const sc_lv<163> ap_ST_st151_fsm_148;
    static const sc_lv<163> ap_ST_st152_fsm_149;
    static const sc_lv<163> ap_ST_st153_fsm_150;
    static const sc_lv<163> ap_ST_st154_fsm_151;
    static const sc_lv<163> ap_ST_st155_fsm_152;
    static const sc_lv<163> ap_ST_st156_fsm_153;
    static const sc_lv<163> ap_ST_st157_fsm_154;
    static const sc_lv<163> ap_ST_st158_fsm_155;
    static const sc_lv<163> ap_ST_st159_fsm_156;
    static const sc_lv<163> ap_ST_st160_fsm_157;
    static const sc_lv<163> ap_ST_st161_fsm_158;
    static const sc_lv<163> ap_ST_st162_fsm_159;
    static const sc_lv<163> ap_ST_st163_fsm_160;
    static const sc_lv<163> ap_ST_st164_fsm_161;
    static const sc_lv<163> ap_ST_st165_fsm_162;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<32> ap_const_lv32_12;
    static const sc_lv<32> ap_const_lv32_41;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_5;
    static const sc_lv<32> ap_const_lv32_9;
    static const sc_lv<32> ap_const_lv32_33;
    static const sc_lv<32> ap_const_lv32_4F;
    static const sc_lv<32> ap_const_lv32_96;
    static const sc_lv<32> ap_const_lv32_30;
    static const sc_lv<32> ap_const_lv32_84;
    static const sc_lv<32> ap_const_lv32_8D;
    static const sc_lv<32> ap_const_lv32_3C;
    static const sc_lv<32> ap_const_lv32_4A;
    static const sc_lv<32> ap_const_lv32_5D;
    static const sc_lv<32> ap_const_lv32_66;
    static const sc_lv<32> ap_const_lv32_6F;
    static const sc_lv<32> ap_const_lv32_9F;
    static const sc_lv<32> ap_const_lv32_54;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_6;
    static const sc_lv<32> ap_const_lv32_31;
    static const sc_lv<32> ap_const_lv32_32;
    static const sc_lv<32> ap_const_lv32_40;
    static const sc_lv<32> ap_const_lv32_4C;
    static const sc_lv<32> ap_const_lv32_4D;
    static const sc_lv<32> ap_const_lv32_4E;
    static const sc_lv<32> ap_const_lv32_8C;
    static const sc_lv<32> ap_const_lv32_A1;
    static const sc_lv<4> ap_const_lv4_0;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<32> ap_const_lv32_4B;
    static const sc_lv<11> ap_const_lv11_0;
    static const sc_lv<32> ap_const_lv32_A0;
    static const sc_lv<32> ap_const_lv32_A2;
    static const sc_lv<6> ap_const_lv6_28;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_11;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<11> ap_const_lv11_690;
    static const sc_lv<32> ap_const_lv32_95;
    static const sc_lv<32> ap_const_lv32_97;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<32> ap_const_lv32_34;
    static const sc_lv<32> ap_const_lv32_42;
    static const sc_lv<32> ap_const_lv32_55;
    static const sc_lv<32> ap_const_lv32_5E;
    static const sc_lv<32> ap_const_lv32_67;
    static const sc_lv<32> ap_const_lv32_8E;
    static const sc_lv<32> ap_const_lv32_3D;
    static const sc_lv<32> ap_const_lv32_50;
    static const sc_lv<32> ap_const_lv32_13;
    static const sc_lv<32> ap_const_lv32_70;
    static const sc_lv<32> ap_const_lv32_D;
    static const sc_lv<4> ap_const_lv4_D;
    static const sc_lv<4> ap_const_lv4_1;
    static const sc_lv<10> ap_const_lv10_208;
    static const sc_lv<6> ap_const_lv6_1;
    static const sc_lv<10> ap_const_lv10_268;
    static const sc_lv<11> ap_const_lv11_29;
    static const sc_lv<6> ap_const_lv6_29;
    static const sc_lv<11> ap_const_lv11_28;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<2> ap_const_lv2_1;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_C_address0();
    void thread_C_address1();
    void thread_C_ce0();
    void thread_C_ce1();
    void thread_C_d0();
    void thread_C_d1();
    void thread_C_we0();
    void thread_C_we1();
    void thread_Q_address0();
    void thread_Q_address1();
    void thread_Q_ce0();
    void thread_Q_ce1();
    void thread_Q_d0();
    void thread_Q_d1();
    void thread_Q_we0();
    void thread_Q_we1();
    void thread_a_1_fu_615_p2();
    void thread_a_2_cast_fu_537_p1();
    void thread_a_2_fu_531_p2();
    void thread_a_cast_fu_495_p1();
    void thread_a_fu_489_p2();
    void thread_alpha_address0();
    void thread_alpha_ce0();
    void thread_alpha_d0();
    void thread_alpha_we0();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_sig_bdd_181();
    void thread_ap_sig_bdd_242();
    void thread_ap_sig_bdd_256();
    void thread_ap_sig_bdd_264();
    void thread_ap_sig_bdd_272();
    void thread_ap_sig_bdd_281();
    void thread_ap_sig_bdd_286();
    void thread_ap_sig_bdd_295();
    void thread_ap_sig_bdd_300();
    void thread_ap_sig_bdd_307();
    void thread_ap_sig_bdd_314();
    void thread_ap_sig_bdd_322();
    void thread_ap_sig_bdd_330();
    void thread_ap_sig_bdd_339();
    void thread_ap_sig_bdd_346();
    void thread_ap_sig_bdd_356();
    void thread_ap_sig_bdd_365();
    void thread_ap_sig_bdd_372();
    void thread_ap_sig_bdd_380();
    void thread_ap_sig_bdd_388();
    void thread_ap_sig_bdd_396();
    void thread_ap_sig_bdd_405();
    void thread_ap_sig_bdd_415();
    void thread_ap_sig_bdd_432();
    void thread_ap_sig_bdd_451();
    void thread_ap_sig_bdd_465();
    void thread_ap_sig_bdd_479();
    void thread_ap_sig_bdd_490();
    void thread_ap_sig_bdd_499();
    void thread_ap_sig_bdd_517();
    void thread_ap_sig_bdd_533();
    void thread_ap_sig_bdd_552();
    void thread_ap_sig_bdd_566();
    void thread_ap_sig_bdd_614();
    void thread_ap_sig_bdd_632();
    void thread_ap_sig_bdd_641();
    void thread_ap_sig_bdd_674();
    void thread_ap_sig_bdd_682();
    void thread_ap_sig_bdd_708();
    void thread_ap_sig_bdd_720();
    void thread_ap_sig_bdd_733();
    void thread_ap_sig_bdd_745();
    void thread_ap_sig_bdd_752();
    void thread_ap_sig_bdd_760();
    void thread_ap_sig_bdd_767();
    void thread_ap_sig_bdd_774();
    void thread_ap_sig_bdd_781();
    void thread_ap_sig_bdd_789();
    void thread_ap_sig_bdd_802();
    void thread_ap_sig_bdd_809();
    void thread_ap_sig_bdd_824();
    void thread_ap_sig_bdd_831();
    void thread_ap_sig_cseq_ST_pp0_stg0_fsm_5();
    void thread_ap_sig_cseq_ST_pp0_stg1_fsm_6();
    void thread_ap_sig_cseq_ST_pp0_stg2_fsm_7();
    void thread_ap_sig_cseq_ST_st105_fsm_102();
    void thread_ap_sig_cseq_ST_st106_fsm_103();
    void thread_ap_sig_cseq_ST_st114_fsm_111();
    void thread_ap_sig_cseq_ST_st115_fsm_112();
    void thread_ap_sig_cseq_ST_st11_fsm_8();
    void thread_ap_sig_cseq_ST_st12_fsm_9();
    void thread_ap_sig_cseq_ST_st135_fsm_132();
    void thread_ap_sig_cseq_ST_st13_fsm_10();
    void thread_ap_sig_cseq_ST_st143_fsm_140();
    void thread_ap_sig_cseq_ST_st144_fsm_141();
    void thread_ap_sig_cseq_ST_st145_fsm_142();
    void thread_ap_sig_cseq_ST_st152_fsm_149();
    void thread_ap_sig_cseq_ST_st153_fsm_150();
    void thread_ap_sig_cseq_ST_st154_fsm_151();
    void thread_ap_sig_cseq_ST_st162_fsm_159();
    void thread_ap_sig_cseq_ST_st163_fsm_160();
    void thread_ap_sig_cseq_ST_st164_fsm_161();
    void thread_ap_sig_cseq_ST_st165_fsm_162();
    void thread_ap_sig_cseq_ST_st1_fsm_0();
    void thread_ap_sig_cseq_ST_st20_fsm_17();
    void thread_ap_sig_cseq_ST_st21_fsm_18();
    void thread_ap_sig_cseq_ST_st22_fsm_19();
    void thread_ap_sig_cseq_ST_st2_fsm_1();
    void thread_ap_sig_cseq_ST_st3_fsm_2();
    void thread_ap_sig_cseq_ST_st4_fsm_3();
    void thread_ap_sig_cseq_ST_st51_fsm_48();
    void thread_ap_sig_cseq_ST_st52_fsm_49();
    void thread_ap_sig_cseq_ST_st53_fsm_50();
    void thread_ap_sig_cseq_ST_st54_fsm_51();
    void thread_ap_sig_cseq_ST_st55_fsm_52();
    void thread_ap_sig_cseq_ST_st5_fsm_4();
    void thread_ap_sig_cseq_ST_st63_fsm_60();
    void thread_ap_sig_cseq_ST_st64_fsm_61();
    void thread_ap_sig_cseq_ST_st67_fsm_64();
    void thread_ap_sig_cseq_ST_st68_fsm_65();
    void thread_ap_sig_cseq_ST_st69_fsm_66();
    void thread_ap_sig_cseq_ST_st77_fsm_74();
    void thread_ap_sig_cseq_ST_st78_fsm_75();
    void thread_ap_sig_cseq_ST_st79_fsm_76();
    void thread_ap_sig_cseq_ST_st80_fsm_77();
    void thread_ap_sig_cseq_ST_st81_fsm_78();
    void thread_ap_sig_cseq_ST_st82_fsm_79();
    void thread_ap_sig_cseq_ST_st83_fsm_80();
    void thread_ap_sig_cseq_ST_st87_fsm_84();
    void thread_ap_sig_cseq_ST_st88_fsm_85();
    void thread_ap_sig_cseq_ST_st96_fsm_93();
    void thread_ap_sig_cseq_ST_st97_fsm_94();
    void thread_b_1_cast_fu_579_p1();
    void thread_b_1_fu_567_p2();
    void thread_b_cast_fu_627_p1();
    void thread_b_fu_621_p2();
    void thread_basisVectors_address0();
    void thread_basisVectors_address1();
    void thread_basisVectors_ce0();
    void thread_basisVectors_ce1();
    void thread_basisVectors_d0();
    void thread_basisVectors_d1();
    void thread_basisVectors_we0();
    void thread_basisVectors_we1();
    void thread_c_fu_573_p2();
    void thread_exitcond1_fu_555_p2();
    void thread_exitcond2_fu_519_p2();
    void thread_exitcond3_fu_477_p2();
    void thread_exitcond4_fu_433_p2();
    void thread_exitcond_fu_597_p2();
    void thread_grp_fu_340_ce();
    void thread_grp_fu_340_opcode();
    void thread_grp_fu_340_p0();
    void thread_grp_fu_340_p1();
    void thread_grp_fu_344_ce();
    void thread_grp_fu_348_ce();
    void thread_grp_fu_348_p0();
    void thread_grp_fu_348_p1();
    void thread_grp_fu_352_ce();
    void thread_grp_fu_356_ce();
    void thread_grp_fu_360_ce();
    void thread_grp_fu_364_ce();
    void thread_grp_fu_364_p0();
    void thread_grp_fu_364_p1();
    void thread_grp_fu_427_ce();
    void thread_grp_fu_427_p1();
    void thread_grp_projection_gp_swapRowAndColumn_fu_324_ap_start();
    void thread_grp_projection_gp_swapRowAndColumn_fu_324_pM_q0();
    void thread_grp_projection_gp_swapRowAndColumn_fu_324_rowA();
    void thread_grp_projection_gp_swapRowAndColumn_fu_332_ap_start();
    void thread_grp_projection_gp_swapRowAndColumn_fu_332_pM_q0();
    void thread_grp_projection_gp_swapRowAndColumn_fu_332_rowA();
    void thread_i1_cast7_cast_fu_473_p1();
    void thread_i2_cast4_cast_fu_515_p1();
    void thread_i5_cast2_cast_fu_593_p1();
    void thread_i_10_fu_439_p2();
    void thread_i_11_fu_483_p2();
    void thread_i_12_fu_525_p2();
    void thread_i_3_fu_603_p2();
    void thread_i_cast8_fu_455_p1();
    void thread_i_cast9_fu_445_p1();
    void thread_i_phi_fu_248_p4();
    void thread_j_5_fu_561_p2();
    void thread_j_cast3_cast_fu_551_p1();
    void thread_j_cast3_fu_547_p1();
    void thread_next_mul7_fu_609_p2();
    void thread_next_mul_fu_509_p2();
    void thread_tmp_67_fu_459_p2();
    void thread_tmp_68_fu_464_p1();
    void thread_tmp_69_fu_449_p2();
    void thread_tmp_70_fu_469_p1();
    void thread_tmp_71_fu_498_p1();
    void thread_tmp_75_fu_504_p1();
    void thread_tmp_77_fu_541_p1();
    void thread_tmp_78_fu_630_p1();
    void thread_tmp_79_fu_635_p1();
    void thread_tmp_80_fu_582_p1();
    void thread_tmp_90_fu_588_p1();
    void thread_tmp_fu_422_p0();
    void thread_tmp_fu_422_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
