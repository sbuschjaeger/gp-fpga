// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _projection_gp_swapRowAndColumn_HH_
#define _projection_gp_swapRowAndColumn_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "projection_gp_mul_32s_7ns_32_5.h"

namespace ap_rtl {

struct projection_gp_swapRowAndColumn : public sc_module {
    // Port declarations 12
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_out< sc_lv<11> > pM_address0;
    sc_out< sc_logic > pM_ce0;
    sc_out< sc_logic > pM_we0;
    sc_out< sc_lv<32> > pM_d0;
    sc_in< sc_lv<32> > pM_q0;
    sc_in< sc_lv<32> > rowA;


    // Module declarations
    projection_gp_swapRowAndColumn(sc_module_name name);
    SC_HAS_PROCESS(projection_gp_swapRowAndColumn);

    ~projection_gp_swapRowAndColumn();

    sc_trace_file* mVcdFile;

    projection_gp_mul_32s_7ns_32_5<1,5,32,7,32>* projection_gp_mul_32s_7ns_32_5_U1;
    sc_signal< sc_lv<15> > ap_CS_fsm;
    sc_signal< sc_logic > ap_sig_cseq_ST_st1_fsm_0;
    sc_signal< bool > ap_sig_bdd_33;
    sc_signal< sc_lv<32> > reg_101;
    sc_signal< sc_logic > ap_sig_cseq_ST_st8_fsm_7;
    sc_signal< bool > ap_sig_bdd_53;
    sc_signal< sc_logic > ap_sig_cseq_ST_st9_fsm_8;
    sc_signal< bool > ap_sig_bdd_60;
    sc_signal< sc_logic > ap_sig_cseq_ST_st13_fsm_12;
    sc_signal< bool > ap_sig_bdd_68;
    sc_signal< sc_logic > ap_sig_cseq_ST_st14_fsm_13;
    sc_signal< bool > ap_sig_bdd_76;
    sc_signal< sc_lv<32> > grp_fu_106_p2;
    sc_signal< sc_lv<32> > tmp_reg_204;
    sc_signal< sc_logic > ap_sig_cseq_ST_st5_fsm_4;
    sc_signal< bool > ap_sig_bdd_91;
    sc_signal< sc_lv<6> > i_7_fu_126_p2;
    sc_signal< sc_lv<6> > i_7_reg_212;
    sc_signal< sc_logic > ap_sig_cseq_ST_st6_fsm_5;
    sc_signal< bool > ap_sig_bdd_100;
    sc_signal< sc_lv<11> > pM_addr_reg_217;
    sc_signal< sc_lv<1> > exitcond1_fu_120_p2;
    sc_signal< sc_lv<10> > tmp_51_fu_142_p2;
    sc_signal< sc_lv<10> > tmp_51_reg_222;
    sc_signal< sc_lv<11> > pM_addr_1_reg_227;
    sc_signal< sc_logic > ap_sig_cseq_ST_st7_fsm_6;
    sc_signal< bool > ap_sig_bdd_117;
    sc_signal< sc_lv<6> > i_8_fu_162_p2;
    sc_signal< sc_lv<6> > i_8_reg_235;
    sc_signal< sc_logic > ap_sig_cseq_ST_st11_fsm_10;
    sc_signal< bool > ap_sig_bdd_126;
    sc_signal< sc_lv<11> > next_mul_fu_168_p2;
    sc_signal< sc_lv<11> > next_mul_reg_240;
    sc_signal< sc_lv<1> > exitcond_fu_156_p2;
    sc_signal< sc_lv<11> > pM_addr_2_reg_245;
    sc_signal< sc_lv<11> > tmp_56_fu_188_p2;
    sc_signal< sc_lv<11> > tmp_56_reg_250;
    sc_signal< sc_lv<11> > pM_addr_3_reg_255;
    sc_signal< sc_logic > ap_sig_cseq_ST_st12_fsm_11;
    sc_signal< bool > ap_sig_bdd_144;
    sc_signal< sc_lv<6> > i_reg_68;
    sc_signal< sc_logic > ap_sig_cseq_ST_st10_fsm_9;
    sc_signal< bool > ap_sig_bdd_154;
    sc_signal< sc_lv<6> > i1_reg_79;
    sc_signal< sc_logic > ap_sig_cseq_ST_st15_fsm_14;
    sc_signal< bool > ap_sig_bdd_163;
    sc_signal< sc_lv<11> > phi_mul_reg_90;
    sc_signal< sc_lv<64> > tmp_50_fu_137_p1;
    sc_signal< sc_lv<64> > tmp_52_fu_151_p1;
    sc_signal< sc_lv<64> > tmp_55_fu_183_p1;
    sc_signal< sc_lv<64> > tmp_57_fu_194_p1;
    sc_signal< sc_lv<7> > grp_fu_106_p1;
    sc_signal< sc_lv<32> > i_cast3_fu_116_p1;
    sc_signal< sc_lv<32> > tmp_s_fu_132_p2;
    sc_signal< sc_lv<10> > i_cast4_cast_fu_112_p1;
    sc_signal< sc_lv<11> > tmp_51_cast5_fu_148_p1;
    sc_signal< sc_lv<32> > tmp_53_cast6_fu_174_p1;
    sc_signal< sc_lv<32> > tmp_54_fu_178_p1;
    sc_signal< sc_lv<32> > tmp_54_fu_178_p2;
    sc_signal< sc_logic > grp_fu_106_ce;
    sc_signal< sc_lv<15> > ap_NS_fsm;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<15> ap_ST_st1_fsm_0;
    static const sc_lv<15> ap_ST_st2_fsm_1;
    static const sc_lv<15> ap_ST_st3_fsm_2;
    static const sc_lv<15> ap_ST_st4_fsm_3;
    static const sc_lv<15> ap_ST_st5_fsm_4;
    static const sc_lv<15> ap_ST_st6_fsm_5;
    static const sc_lv<15> ap_ST_st7_fsm_6;
    static const sc_lv<15> ap_ST_st8_fsm_7;
    static const sc_lv<15> ap_ST_st9_fsm_8;
    static const sc_lv<15> ap_ST_st10_fsm_9;
    static const sc_lv<15> ap_ST_st11_fsm_10;
    static const sc_lv<15> ap_ST_st12_fsm_11;
    static const sc_lv<15> ap_ST_st13_fsm_12;
    static const sc_lv<15> ap_ST_st14_fsm_13;
    static const sc_lv<15> ap_ST_st15_fsm_14;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_C;
    static const sc_lv<32> ap_const_lv32_D;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_5;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_6;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<32> ap_const_lv32_B;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<32> ap_const_lv32_9;
    static const sc_lv<32> ap_const_lv32_E;
    static const sc_lv<11> ap_const_lv11_0;
    static const sc_lv<32> ap_const_lv32_29;
    static const sc_lv<6> ap_const_lv6_29;
    static const sc_lv<6> ap_const_lv6_1;
    static const sc_lv<10> ap_const_lv10_268;
    static const sc_lv<11> ap_const_lv11_29;
    static const sc_lv<11> ap_const_lv11_28;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_sig_bdd_100();
    void thread_ap_sig_bdd_117();
    void thread_ap_sig_bdd_126();
    void thread_ap_sig_bdd_144();
    void thread_ap_sig_bdd_154();
    void thread_ap_sig_bdd_163();
    void thread_ap_sig_bdd_33();
    void thread_ap_sig_bdd_53();
    void thread_ap_sig_bdd_60();
    void thread_ap_sig_bdd_68();
    void thread_ap_sig_bdd_76();
    void thread_ap_sig_bdd_91();
    void thread_ap_sig_cseq_ST_st10_fsm_9();
    void thread_ap_sig_cseq_ST_st11_fsm_10();
    void thread_ap_sig_cseq_ST_st12_fsm_11();
    void thread_ap_sig_cseq_ST_st13_fsm_12();
    void thread_ap_sig_cseq_ST_st14_fsm_13();
    void thread_ap_sig_cseq_ST_st15_fsm_14();
    void thread_ap_sig_cseq_ST_st1_fsm_0();
    void thread_ap_sig_cseq_ST_st5_fsm_4();
    void thread_ap_sig_cseq_ST_st6_fsm_5();
    void thread_ap_sig_cseq_ST_st7_fsm_6();
    void thread_ap_sig_cseq_ST_st8_fsm_7();
    void thread_ap_sig_cseq_ST_st9_fsm_8();
    void thread_exitcond1_fu_120_p2();
    void thread_exitcond_fu_156_p2();
    void thread_grp_fu_106_ce();
    void thread_grp_fu_106_p1();
    void thread_i_7_fu_126_p2();
    void thread_i_8_fu_162_p2();
    void thread_i_cast3_fu_116_p1();
    void thread_i_cast4_cast_fu_112_p1();
    void thread_next_mul_fu_168_p2();
    void thread_pM_address0();
    void thread_pM_ce0();
    void thread_pM_d0();
    void thread_pM_we0();
    void thread_tmp_50_fu_137_p1();
    void thread_tmp_51_cast5_fu_148_p1();
    void thread_tmp_51_fu_142_p2();
    void thread_tmp_52_fu_151_p1();
    void thread_tmp_53_cast6_fu_174_p1();
    void thread_tmp_54_fu_178_p1();
    void thread_tmp_54_fu_178_p2();
    void thread_tmp_55_fu_183_p1();
    void thread_tmp_56_fu_188_p2();
    void thread_tmp_57_fu_194_p1();
    void thread_tmp_s_fu_132_p2();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
