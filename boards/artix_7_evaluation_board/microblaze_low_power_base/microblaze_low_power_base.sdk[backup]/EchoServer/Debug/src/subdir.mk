################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/echo.c \
../src/main.c \
../src/platform.c \
../src/platform_mb.c 

OBJS += \
./src/echo.o \
./src/main.o \
./src/platform.o \
./src/platform_mb.o 

C_DEPS += \
./src/echo.d \
./src/main.d \
./src/platform.d \
./src/platform_mb.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MicroBlaze gcc compiler'
	mb-gcc -Wall -Os -g -c -fmessage-length=0 -MT"$@" -I../../standalone_bsp_0/microblaze_0/include -mno-xl-reorder -mlittle-endian -mcpu=v9.5 -mxl-soft-mul -Wl,--no-relax -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


