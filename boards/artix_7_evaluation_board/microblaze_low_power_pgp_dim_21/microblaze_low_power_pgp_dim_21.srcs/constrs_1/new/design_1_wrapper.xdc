set_operating_conditions -process maximum
set_load 0.000 [all_outputs]


set_operating_conditions -process typical
set_load 0.000 [all_outputs]
set_operating_conditions -process maximum
set_property LOAD 0 [get_ports mdio_mdc_mdc]
set_property LOAD 0 [get_ports rs232_uart_txd]
set_property LOAD 0 [get_ports rgmii_txc]
set_property LOAD 0 [get_ports {rgmii_td[0]}]
set_property LOAD 0 [get_ports {rgmii_td[1]}]
set_property LOAD 0 [get_ports {rgmii_td[2]}]
set_property LOAD 0 [get_ports {rgmii_td[3]}]
set_property LOAD 0 [get_ports phy_reset_out]
set_property LOAD 0 [get_ports mdio_mdc_mdio_io]
set_property LOAD 0 [get_ports rgmii_tx_ctl]
set_load 0.000 [all_outputs]
