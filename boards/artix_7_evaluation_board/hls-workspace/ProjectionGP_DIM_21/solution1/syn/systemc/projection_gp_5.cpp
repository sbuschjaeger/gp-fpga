#include "projection_gp.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void projection_gp::thread_ap_sig_cseq_ST_st857_fsm_851() {
    if (ap_sig_bdd_1909.read()) {
        ap_sig_cseq_ST_st857_fsm_851 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st857_fsm_851 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st858_fsm_852() {
    if (ap_sig_bdd_5887.read()) {
        ap_sig_cseq_ST_st858_fsm_852 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st858_fsm_852 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st862_fsm_856() {
    if (ap_sig_bdd_2717.read()) {
        ap_sig_cseq_ST_st862_fsm_856 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st862_fsm_856 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st863_fsm_857() {
    if (ap_sig_bdd_5086.read()) {
        ap_sig_cseq_ST_st863_fsm_857 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st863_fsm_857 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st865_fsm_859() {
    if (ap_sig_bdd_3881.read()) {
        ap_sig_cseq_ST_st865_fsm_859 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st865_fsm_859 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st866_fsm_860() {
    if (ap_sig_bdd_1918.read()) {
        ap_sig_cseq_ST_st866_fsm_860 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st866_fsm_860 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st867_fsm_861() {
    if (ap_sig_bdd_5895.read()) {
        ap_sig_cseq_ST_st867_fsm_861 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st867_fsm_861 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st871_fsm_865() {
    if (ap_sig_bdd_2725.read()) {
        ap_sig_cseq_ST_st871_fsm_865 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st871_fsm_865 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st872_fsm_866() {
    if (ap_sig_bdd_5094.read()) {
        ap_sig_cseq_ST_st872_fsm_866 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st872_fsm_866 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st874_fsm_868() {
    if (ap_sig_bdd_3889.read()) {
        ap_sig_cseq_ST_st874_fsm_868 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st874_fsm_868 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st875_fsm_869() {
    if (ap_sig_bdd_1927.read()) {
        ap_sig_cseq_ST_st875_fsm_869 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st875_fsm_869 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st876_fsm_870() {
    if (ap_sig_bdd_5903.read()) {
        ap_sig_cseq_ST_st876_fsm_870 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st876_fsm_870 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st880_fsm_874() {
    if (ap_sig_bdd_2733.read()) {
        ap_sig_cseq_ST_st880_fsm_874 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st880_fsm_874 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st881_fsm_875() {
    if (ap_sig_bdd_5102.read()) {
        ap_sig_cseq_ST_st881_fsm_875 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st881_fsm_875 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st883_fsm_877() {
    if (ap_sig_bdd_3897.read()) {
        ap_sig_cseq_ST_st883_fsm_877 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st883_fsm_877 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st884_fsm_878() {
    if (ap_sig_bdd_1936.read()) {
        ap_sig_cseq_ST_st884_fsm_878 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st884_fsm_878 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st885_fsm_879() {
    if (ap_sig_bdd_5911.read()) {
        ap_sig_cseq_ST_st885_fsm_879 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st885_fsm_879 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st889_fsm_883() {
    if (ap_sig_bdd_2741.read()) {
        ap_sig_cseq_ST_st889_fsm_883 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st889_fsm_883 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st88_fsm_82() {
    if (ap_sig_bdd_2029.read()) {
        ap_sig_cseq_ST_st88_fsm_82 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st88_fsm_82 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st890_fsm_884() {
    if (ap_sig_bdd_5110.read()) {
        ap_sig_cseq_ST_st890_fsm_884 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st890_fsm_884 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st892_fsm_886() {
    if (ap_sig_bdd_3905.read()) {
        ap_sig_cseq_ST_st892_fsm_886 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st892_fsm_886 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st893_fsm_887() {
    if (ap_sig_bdd_1945.read()) {
        ap_sig_cseq_ST_st893_fsm_887 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st893_fsm_887 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st894_fsm_888() {
    if (ap_sig_bdd_5919.read()) {
        ap_sig_cseq_ST_st894_fsm_888 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st894_fsm_888 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st898_fsm_892() {
    if (ap_sig_bdd_2749.read()) {
        ap_sig_cseq_ST_st898_fsm_892 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st898_fsm_892 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st899_fsm_893() {
    if (ap_sig_bdd_5118.read()) {
        ap_sig_cseq_ST_st899_fsm_893 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st899_fsm_893 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st89_fsm_83() {
    if (ap_sig_bdd_4398.read()) {
        ap_sig_cseq_ST_st89_fsm_83 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st89_fsm_83 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st901_fsm_895() {
    if (ap_sig_bdd_3913.read()) {
        ap_sig_cseq_ST_st901_fsm_895 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st901_fsm_895 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st902_fsm_896() {
    if (ap_sig_bdd_1954.read()) {
        ap_sig_cseq_ST_st902_fsm_896 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st902_fsm_896 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st903_fsm_897() {
    if (ap_sig_bdd_5927.read()) {
        ap_sig_cseq_ST_st903_fsm_897 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st903_fsm_897 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st907_fsm_901() {
    if (ap_sig_bdd_2757.read()) {
        ap_sig_cseq_ST_st907_fsm_901 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st907_fsm_901 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st908_fsm_902() {
    if (ap_sig_bdd_5126.read()) {
        ap_sig_cseq_ST_st908_fsm_902 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st908_fsm_902 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st916_fsm_910() {
    if (ap_sig_bdd_2865.read()) {
        ap_sig_cseq_ST_st916_fsm_910 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st916_fsm_910 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st917_fsm_911() {
    if (ap_sig_bdd_3022.read()) {
        ap_sig_cseq_ST_st917_fsm_911 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st917_fsm_911 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st91_fsm_85() {
    if (ap_sig_bdd_3193.read()) {
        ap_sig_cseq_ST_st91_fsm_85 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st91_fsm_85 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st92_fsm_86() {
    if (ap_sig_bdd_1144.read()) {
        ap_sig_cseq_ST_st92_fsm_86 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st92_fsm_86 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st93_fsm_87() {
    if (ap_sig_bdd_5207.read()) {
        ap_sig_cseq_ST_st93_fsm_87 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st93_fsm_87 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st97_fsm_91() {
    if (ap_sig_bdd_2037.read()) {
        ap_sig_cseq_ST_st97_fsm_91 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st97_fsm_91 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st98_fsm_92() {
    if (ap_sig_bdd_4406.read()) {
        ap_sig_cseq_ST_st98_fsm_92 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st98_fsm_92 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st9_fsm_3() {
    if (ap_sig_bdd_3015.read()) {
        ap_sig_cseq_ST_st9_fsm_3 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st9_fsm_3 = ap_const_logic_0;
    }
}

void projection_gp::thread_basisVectors_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it5.read())) {
        basisVectors_address0 =  (sc_lv<11>) (tmp_63_i_fu_964_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_5.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st20_fsm_14.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st29_fsm_23.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st38_fsm_32.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st47_fsm_41.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st56_fsm_50.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st65_fsm_59.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st74_fsm_68.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st83_fsm_77.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st92_fsm_86.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st101_fsm_95.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st110_fsm_104.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st119_fsm_113.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st128_fsm_122.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st137_fsm_131.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st146_fsm_140.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st155_fsm_149.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st164_fsm_158.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st173_fsm_167.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st182_fsm_176.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st191_fsm_185.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st200_fsm_194.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st209_fsm_203.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st218_fsm_212.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st227_fsm_221.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st236_fsm_230.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st245_fsm_239.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st254_fsm_248.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st263_fsm_257.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st272_fsm_266.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st281_fsm_275.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st290_fsm_284.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st299_fsm_293.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st308_fsm_302.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st317_fsm_311.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st326_fsm_320.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st335_fsm_329.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st344_fsm_338.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st353_fsm_347.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st362_fsm_356.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st371_fsm_365.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st380_fsm_374.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st389_fsm_383.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st398_fsm_392.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st407_fsm_401.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st416_fsm_410.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st425_fsm_419.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st434_fsm_428.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st443_fsm_437.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st452_fsm_446.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st461_fsm_455.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st470_fsm_464.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st479_fsm_473.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st488_fsm_482.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st497_fsm_491.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st506_fsm_500.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st515_fsm_509.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st524_fsm_518.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st533_fsm_527.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st542_fsm_536.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st551_fsm_545.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st560_fsm_554.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st569_fsm_563.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st578_fsm_572.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st587_fsm_581.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st596_fsm_590.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st605_fsm_599.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st614_fsm_608.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st623_fsm_617.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st632_fsm_626.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st641_fsm_635.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st650_fsm_644.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st659_fsm_653.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st668_fsm_662.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st677_fsm_671.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st686_fsm_680.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st695_fsm_689.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st704_fsm_698.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st713_fsm_707.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st722_fsm_716.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st731_fsm_725.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st740_fsm_734.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st749_fsm_743.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st758_fsm_752.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st767_fsm_761.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st776_fsm_770.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st785_fsm_779.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st794_fsm_788.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st803_fsm_797.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st812_fsm_806.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st821_fsm_815.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st830_fsm_824.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st839_fsm_833.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st848_fsm_842.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st857_fsm_851.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st866_fsm_860.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st875_fsm_869.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st884_fsm_878.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st893_fsm_887.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st902_fsm_896.read()))) {
        basisVectors_address0 = grp_projection_gp_K_fu_730_pX1_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read())) {
        basisVectors_address0 = grp_projection_gp_train_full_bv_set_fu_707_basisVectors_address0.read();
    } else {
        basisVectors_address0 =  (sc_lv<11>) ("XXXXXXXXXXX");
    }
}

void projection_gp::thread_basisVectors_address1() {
    basisVectors_address1 = grp_projection_gp_train_full_bv_set_fu_707_basisVectors_address1.read();
}

void projection_gp::thread_basisVectors_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it5.read())) {
        basisVectors_ce0 = ap_const_logic_1;
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_5.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st20_fsm_14.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st29_fsm_23.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st38_fsm_32.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st47_fsm_41.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st56_fsm_50.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st65_fsm_59.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st74_fsm_68.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st83_fsm_77.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st92_fsm_86.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st101_fsm_95.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st110_fsm_104.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st119_fsm_113.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st128_fsm_122.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st137_fsm_131.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st146_fsm_140.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st155_fsm_149.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st164_fsm_158.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st173_fsm_167.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st182_fsm_176.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st191_fsm_185.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st200_fsm_194.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st209_fsm_203.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st218_fsm_212.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st227_fsm_221.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st236_fsm_230.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st245_fsm_239.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st254_fsm_248.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st263_fsm_257.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st272_fsm_266.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st281_fsm_275.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st290_fsm_284.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st299_fsm_293.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st308_fsm_302.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st317_fsm_311.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st326_fsm_320.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st335_fsm_329.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st344_fsm_338.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st353_fsm_347.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st362_fsm_356.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st371_fsm_365.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st380_fsm_374.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st389_fsm_383.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st398_fsm_392.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st407_fsm_401.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st416_fsm_410.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st425_fsm_419.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st434_fsm_428.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st443_fsm_437.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st452_fsm_446.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st461_fsm_455.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st470_fsm_464.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st479_fsm_473.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st488_fsm_482.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st497_fsm_491.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st506_fsm_500.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st515_fsm_509.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st524_fsm_518.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st533_fsm_527.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st542_fsm_536.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st551_fsm_545.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st560_fsm_554.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st569_fsm_563.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st578_fsm_572.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st587_fsm_581.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st596_fsm_590.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st605_fsm_599.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st614_fsm_608.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st623_fsm_617.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st632_fsm_626.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st641_fsm_635.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st650_fsm_644.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st659_fsm_653.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st668_fsm_662.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st677_fsm_671.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st686_fsm_680.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st695_fsm_689.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st704_fsm_698.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st713_fsm_707.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st722_fsm_716.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st731_fsm_725.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st740_fsm_734.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st749_fsm_743.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st758_fsm_752.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st767_fsm_761.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st776_fsm_770.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st785_fsm_779.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st794_fsm_788.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st803_fsm_797.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st812_fsm_806.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st821_fsm_815.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st830_fsm_824.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st839_fsm_833.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st848_fsm_842.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st857_fsm_851.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st866_fsm_860.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st875_fsm_869.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st884_fsm_878.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st893_fsm_887.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st902_fsm_896.read()))) {
        basisVectors_ce0 = grp_projection_gp_K_fu_730_pX1_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read())) {
        basisVectors_ce0 = grp_projection_gp_train_full_bv_set_fu_707_basisVectors_ce0.read();
    } else {
        basisVectors_ce0 = ap_const_logic_0;
    }
}

void projection_gp::thread_basisVectors_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read())) {
        basisVectors_ce1 = grp_projection_gp_train_full_bv_set_fu_707_basisVectors_ce1.read();
    } else {
        basisVectors_ce1 = ap_const_logic_0;
    }
}

void projection_gp::thread_basisVectors_d0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it5.read())) {
        basisVectors_d0 = pX_load_reg_1031.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read())) {
        basisVectors_d0 = grp_projection_gp_train_full_bv_set_fu_707_basisVectors_d0.read();
    } else {
        basisVectors_d0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_basisVectors_d1() {
    basisVectors_d1 = grp_projection_gp_train_full_bv_set_fu_707_basisVectors_d1.read();
}

void projection_gp::thread_basisVectors_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it5.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, ap_reg_ppstg_exitcond_i_reg_1011_pp0_it4.read())))) {
        basisVectors_we0 = ap_const_logic_1;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read())) {
        basisVectors_we0 = grp_projection_gp_train_full_bv_set_fu_707_basisVectors_we0.read();
    } else {
        basisVectors_we0 = ap_const_logic_0;
    }
}

void projection_gp::thread_basisVectors_we1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read())) {
        basisVectors_we1 = grp_projection_gp_train_full_bv_set_fu_707_basisVectors_we1.read();
    } else {
        basisVectors_we1 = ap_const_logic_0;
    }
}

void projection_gp::thread_exitcond1_fu_874_p2() {
    exitcond1_fu_874_p2 = (!i_reg_663.read().is_01() || !ap_const_lv7_65.is_01())? sc_lv<1>(): sc_lv<1>(i_reg_663.read() == ap_const_lv7_65);
}

void projection_gp::thread_exitcond_i_fu_931_p2() {
    exitcond_i_fu_931_p2 = (!i_i_phi_fu_688_p4.read().is_01() || !ap_const_lv4_D.is_01())? sc_lv<1>(): sc_lv<1>(i_i_phi_fu_688_p4.read() == ap_const_lv4_D);
}

void projection_gp::thread_grp_fu_839_ce() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_2.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_5.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st16_fsm_10.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st2_fsm_1.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st9_fsm_3.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st917_fsm_911.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st12_fsm_6.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st20_fsm_14.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st29_fsm_23.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st38_fsm_32.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st47_fsm_41.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st56_fsm_50.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st65_fsm_59.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st74_fsm_68.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st83_fsm_77.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st92_fsm_86.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st101_fsm_95.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st110_fsm_104.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st119_fsm_113.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st128_fsm_122.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st137_fsm_131.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st146_fsm_140.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st155_fsm_149.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st164_fsm_158.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st173_fsm_167.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st182_fsm_176.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st191_fsm_185.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st200_fsm_194.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st209_fsm_203.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st218_fsm_212.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st227_fsm_221.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st236_fsm_230.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st245_fsm_239.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st254_fsm_248.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st263_fsm_257.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st272_fsm_266.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st281_fsm_275.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st290_fsm_284.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st299_fsm_293.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st308_fsm_302.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st317_fsm_311.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st326_fsm_320.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st335_fsm_329.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st344_fsm_338.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st353_fsm_347.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st362_fsm_356.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st371_fsm_365.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st380_fsm_374.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st389_fsm_383.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st398_fsm_392.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st407_fsm_401.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st416_fsm_410.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st425_fsm_419.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st434_fsm_428.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st443_fsm_437.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st452_fsm_446.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st461_fsm_455.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st470_fsm_464.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st479_fsm_473.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st488_fsm_482.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st497_fsm_491.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st506_fsm_500.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st515_fsm_509.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st524_fsm_518.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st533_fsm_527.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st542_fsm_536.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st551_fsm_545.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st560_fsm_554.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st569_fsm_563.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st578_fsm_572.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st587_fsm_581.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st596_fsm_590.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st605_fsm_599.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st614_fsm_608.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st623_fsm_617.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st632_fsm_626.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st641_fsm_635.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st650_fsm_644.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st659_fsm_653.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st668_fsm_662.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st677_fsm_671.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st686_fsm_680.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st695_fsm_689.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st704_fsm_698.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st713_fsm_707.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st722_fsm_716.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st731_fsm_725.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st740_fsm_734.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st749_fsm_743.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st758_fsm_752.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st767_fsm_761.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st776_fsm_770.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st785_fsm_779.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st794_fsm_788.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st803_fsm_797.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st812_fsm_806.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st821_fsm_815.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st830_fsm_824.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st839_fsm_833.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st848_fsm_842.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st857_fsm_851.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st866_fsm_860.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st875_fsm_869.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st884_fsm_878.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st893_fsm_887.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_730_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st902_fsm_896.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st13_fsm_7.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_8.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st15_fsm_9.read()))) {
        grp_fu_839_ce = ap_const_logic_0;
    } else {
        grp_fu_839_ce = ap_const_logic_1;
    }
}

void projection_gp::thread_grp_fu_839_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st26_fsm_20.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st35_fsm_29.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st44_fsm_38.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st53_fsm_47.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st62_fsm_56.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st71_fsm_65.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st80_fsm_74.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st89_fsm_83.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st98_fsm_92.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st107_fsm_101.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st116_fsm_110.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st125_fsm_119.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st134_fsm_128.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st143_fsm_137.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st152_fsm_146.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st161_fsm_155.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st170_fsm_164.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st179_fsm_173.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st188_fsm_182.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st197_fsm_191.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st206_fsm_200.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st215_fsm_209.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st224_fsm_218.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st233_fsm_227.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st242_fsm_236.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st251_fsm_245.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st260_fsm_254.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st269_fsm_263.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st278_fsm_272.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st287_fsm_281.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st296_fsm_290.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st305_fsm_299.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st314_fsm_308.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st323_fsm_317.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st332_fsm_326.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st341_fsm_335.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st350_fsm_344.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st359_fsm_353.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st368_fsm_362.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st377_fsm_371.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st386_fsm_380.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st395_fsm_389.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st404_fsm_398.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st413_fsm_407.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st422_fsm_416.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st431_fsm_425.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st440_fsm_434.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st449_fsm_443.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st458_fsm_452.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st467_fsm_461.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st476_fsm_470.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st485_fsm_479.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st494_fsm_488.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st503_fsm_497.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st512_fsm_506.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st521_fsm_515.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st530_fsm_524.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st539_fsm_533.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st548_fsm_542.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st557_fsm_551.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st566_fsm_560.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st575_fsm_569.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st584_fsm_578.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st593_fsm_587.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st602_fsm_596.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st611_fsm_605.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st620_fsm_614.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st629_fsm_623.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st638_fsm_632.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st647_fsm_641.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st656_fsm_650.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st665_fsm_659.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st674_fsm_668.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st683_fsm_677.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st692_fsm_686.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st701_fsm_695.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st710_fsm_704.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st719_fsm_713.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st728_fsm_722.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st737_fsm_731.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st746_fsm_740.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st755_fsm_749.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st764_fsm_758.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st773_fsm_767.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st782_fsm_776.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st791_fsm_785.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st800_fsm_794.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st809_fsm_803.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st818_fsm_812.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st827_fsm_821.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st836_fsm_830.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st845_fsm_839.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st854_fsm_848.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st863_fsm_857.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st872_fsm_866.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st881_fsm_875.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st890_fsm_884.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st899_fsm_893.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st908_fsm_902.read()))) {
        grp_fu_839_p0 = reg_868.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st17_fsm_11.read())) {
        grp_fu_839_p0 = reg_862.read();
    } else {
        grp_fu_839_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_grp_fu_839_p1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st26_fsm_20.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st35_fsm_29.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st44_fsm_38.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st53_fsm_47.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st62_fsm_56.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st71_fsm_65.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st80_fsm_74.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st89_fsm_83.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st98_fsm_92.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st107_fsm_101.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st116_fsm_110.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st125_fsm_119.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st134_fsm_128.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st143_fsm_137.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st152_fsm_146.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st161_fsm_155.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st170_fsm_164.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st179_fsm_173.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st188_fsm_182.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st197_fsm_191.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st206_fsm_200.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st215_fsm_209.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st224_fsm_218.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st233_fsm_227.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st242_fsm_236.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st251_fsm_245.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st260_fsm_254.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st269_fsm_263.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st278_fsm_272.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st287_fsm_281.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st296_fsm_290.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st305_fsm_299.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st314_fsm_308.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st323_fsm_317.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st332_fsm_326.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st341_fsm_335.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st350_fsm_344.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st359_fsm_353.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st368_fsm_362.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st377_fsm_371.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st386_fsm_380.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st395_fsm_389.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st404_fsm_398.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st413_fsm_407.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st422_fsm_416.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st431_fsm_425.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st440_fsm_434.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st449_fsm_443.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st458_fsm_452.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st467_fsm_461.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st476_fsm_470.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st485_fsm_479.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st494_fsm_488.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st503_fsm_497.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st512_fsm_506.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st521_fsm_515.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st530_fsm_524.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st539_fsm_533.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st548_fsm_542.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st557_fsm_551.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st566_fsm_560.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st575_fsm_569.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st584_fsm_578.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st593_fsm_587.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st602_fsm_596.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st611_fsm_605.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st620_fsm_614.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st629_fsm_623.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st638_fsm_632.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st647_fsm_641.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st656_fsm_650.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st665_fsm_659.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st674_fsm_668.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st683_fsm_677.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st692_fsm_686.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st701_fsm_695.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st710_fsm_704.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st719_fsm_713.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st728_fsm_722.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st737_fsm_731.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st746_fsm_740.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st755_fsm_749.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st764_fsm_758.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st773_fsm_767.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st782_fsm_776.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st791_fsm_785.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st800_fsm_794.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st809_fsm_803.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st818_fsm_812.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st827_fsm_821.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st836_fsm_830.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st845_fsm_839.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st854_fsm_848.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st863_fsm_857.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st872_fsm_866.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st881_fsm_875.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st890_fsm_884.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st899_fsm_893.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st908_fsm_902.read()))) {
        grp_fu_839_p1 = reg_862.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st17_fsm_11.read())) {
        grp_fu_839_p1 = ap_const_lv32_415A75F7;
    } else {
        grp_fu_839_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_grp_fu_844_ce() {
    grp_fu_844_ce = ap_const_logic_1;
}

void projection_gp::thread_grp_fu_844_p0() {
    grp_fu_844_p0 = reg_852.read();
}

void projection_gp::thread_grp_fu_844_p1() {
    grp_fu_844_p1 = reg_857.read();
}

void projection_gp::thread_grp_fu_943_ce() {
    grp_fu_943_ce = ap_const_logic_1;
}

void projection_gp::thread_grp_fu_943_p0() {
    grp_fu_943_p0 = bvCnt.read();
}

void projection_gp::thread_grp_fu_943_p1() {
    grp_fu_943_p1 =  (sc_lv<5>) (ap_const_lv32_D);
}

void projection_gp::thread_grp_projection_gp_K_fu_730_ap_start() {
    grp_projection_gp_K_fu_730_ap_start = grp_projection_gp_K_fu_730_ap_start_ap_start_reg.read();
}

void projection_gp::thread_grp_projection_gp_K_fu_730_pX1_q0() {
    grp_projection_gp_K_fu_730_pX1_q0 = basisVectors_q0.read();
}

void projection_gp::thread_grp_projection_gp_K_fu_730_pX2_q0() {
    grp_projection_gp_K_fu_730_pX2_q0 = pX_q0.read();
}

void projection_gp::thread_grp_projection_gp_K_fu_730_tmp_94() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st902_fsm_896.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_507;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st893_fsm_887.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_4FA;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st884_fsm_878.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_4ED;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st875_fsm_869.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_4E0;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st866_fsm_860.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_4D3;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st857_fsm_851.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_4C6;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st848_fsm_842.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_4B9;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st839_fsm_833.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_4AC;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st830_fsm_824.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_49F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st821_fsm_815.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_492;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st812_fsm_806.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_485;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st803_fsm_797.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_478;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st794_fsm_788.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_46B;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st785_fsm_779.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_45E;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st776_fsm_770.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_451;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st767_fsm_761.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_444;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st758_fsm_752.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_437;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st749_fsm_743.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_42A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st740_fsm_734.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_41D;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st731_fsm_725.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_410;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st722_fsm_716.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_403;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st713_fsm_707.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_3F6;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st704_fsm_698.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_3E9;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st695_fsm_689.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_3DC;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st686_fsm_680.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_3CF;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st677_fsm_671.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_3C2;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st668_fsm_662.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_3B5;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st659_fsm_653.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_3A8;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st650_fsm_644.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_39B;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st641_fsm_635.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_38E;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st632_fsm_626.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_381;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st623_fsm_617.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_374;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st614_fsm_608.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_367;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st605_fsm_599.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_35A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st596_fsm_590.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_34D;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st587_fsm_581.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_340;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st578_fsm_572.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_333;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st569_fsm_563.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_326;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st560_fsm_554.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_319;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st551_fsm_545.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_30C;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st542_fsm_536.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_2FF;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st533_fsm_527.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_2F2;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st524_fsm_518.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_2E5;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st515_fsm_509.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_2D8;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st506_fsm_500.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_2CB;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st497_fsm_491.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_2BE;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st488_fsm_482.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_2B1;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st479_fsm_473.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_2A4;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st470_fsm_464.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_297;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st461_fsm_455.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_28A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st452_fsm_446.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_27D;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st443_fsm_437.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_270;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st434_fsm_428.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_263;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st425_fsm_419.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_256;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st416_fsm_410.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_249;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st407_fsm_401.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_23C;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st398_fsm_392.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_22F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st389_fsm_383.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_222;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st380_fsm_374.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_215;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st371_fsm_365.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_208;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st362_fsm_356.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1FB;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st353_fsm_347.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1EE;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st344_fsm_338.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1E1;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st335_fsm_329.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1D4;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st326_fsm_320.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1C7;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st317_fsm_311.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1BA;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st308_fsm_302.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1AD;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st299_fsm_293.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1A0;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st290_fsm_284.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_193;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st281_fsm_275.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_186;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st272_fsm_266.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_179;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st263_fsm_257.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_16C;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st254_fsm_248.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_15F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st245_fsm_239.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_152;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st236_fsm_230.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_145;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st227_fsm_221.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_138;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st218_fsm_212.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_12B;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st209_fsm_203.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_11E;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st200_fsm_194.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_111;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st191_fsm_185.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_104;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st182_fsm_176.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_F7;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st173_fsm_167.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_EA;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st164_fsm_158.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_DD;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st155_fsm_149.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_D0;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st146_fsm_140.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_C3;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st137_fsm_131.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_B6;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st128_fsm_122.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_A9;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st119_fsm_113.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_9C;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st110_fsm_104.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_8F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st101_fsm_95.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_82;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st92_fsm_86.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_75;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st83_fsm_77.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_68;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st74_fsm_68.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_5B;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st65_fsm_59.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_4E;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st56_fsm_50.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_41;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st47_fsm_41.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_34;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st38_fsm_32.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_27;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st29_fsm_23.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_1A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st20_fsm_14.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_D;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_5.read())) {
        grp_projection_gp_K_fu_730_tmp_94 = ap_const_lv12_0;
    } else {
        grp_projection_gp_K_fu_730_tmp_94 =  (sc_lv<12>) ("XXXXXXXXXXXX");
    }
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_Q_q0() {
    grp_projection_gp_train_full_bv_set_fu_707_Q_q0 = Q_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_Q_q1() {
    grp_projection_gp_train_full_bv_set_fu_707_Q_q1 = Q_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_alpha_q0() {
    grp_projection_gp_train_full_bv_set_fu_707_alpha_q0 = alpha_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_alpha_q1() {
    grp_projection_gp_train_full_bv_set_fu_707_alpha_q1 = alpha_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_ap_start() {
    grp_projection_gp_train_full_bv_set_fu_707_ap_start = grp_projection_gp_train_full_bv_set_fu_707_ap_start_ap_start_reg.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_basisVectors_q0() {
    grp_projection_gp_train_full_bv_set_fu_707_basisVectors_q0 = basisVectors_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_basisVectors_q1() {
    grp_projection_gp_train_full_bv_set_fu_707_basisVectors_q1 = basisVectors_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_bvCnt() {
    grp_projection_gp_train_full_bv_set_fu_707_bvCnt = bvCnt.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_pX_q0() {
    grp_projection_gp_train_full_bv_set_fu_707_pX_q0 = pX_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_707_pY() {
    grp_projection_gp_train_full_bv_set_fu_707_pY = pY_read_reg_988.read();
}

void projection_gp::thread_i_1_fu_880_p2() {
    i_1_fu_880_p2 = (!i_reg_663.read().is_01() || !ap_const_lv7_1.is_01())? sc_lv<7>(): (sc_biguint<7>(i_reg_663.read()) + sc_biguint<7>(ap_const_lv7_1));
}

void projection_gp::thread_i_2_fu_937_p2() {
    i_2_fu_937_p2 = (!i_i_phi_fu_688_p4.read().is_01() || !ap_const_lv4_1.is_01())? sc_lv<4>(): (sc_biguint<4>(i_i_phi_fu_688_p4.read()) + sc_biguint<4>(ap_const_lv4_1));
}

void projection_gp::thread_i_i_cast2_fu_954_p1() {
    i_i_cast2_fu_954_p1 = esl_zext<32,4>(ap_reg_ppstg_i_i_reg_684_pp0_it3.read());
}

void projection_gp::thread_i_i_phi_fu_688_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_2.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, exitcond_i_reg_1011.read()))) {
        i_i_phi_fu_688_p4 = i_2_reg_1015.read();
    } else {
        i_i_phi_fu_688_p4 = i_i_reg_684.read();
    }
}

void projection_gp::thread_pInit_read_read_fu_504_p2() {
    pInit_read_read_fu_504_p2 =  (sc_lv<1>) (pInit.read());
}

void projection_gp::thread_pPredict_read_read_fu_510_p2() {
    pPredict_read_read_fu_510_p2 =  (sc_lv<1>) (pPredict.read());
}

void projection_gp::thread_pX_address0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it3.read())) {
        pX_address0 =  (sc_lv<4>) (tmp_i_fu_949_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_5.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st20_fsm_14.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st29_fsm_23.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st38_fsm_32.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st47_fsm_41.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st56_fsm_50.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st65_fsm_59.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st74_fsm_68.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st83_fsm_77.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st92_fsm_86.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st101_fsm_95.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st110_fsm_104.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st119_fsm_113.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st128_fsm_122.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st137_fsm_131.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st146_fsm_140.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st155_fsm_149.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st164_fsm_158.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st173_fsm_167.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st182_fsm_176.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st191_fsm_185.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st200_fsm_194.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st209_fsm_203.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st218_fsm_212.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st227_fsm_221.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st236_fsm_230.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st245_fsm_239.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st254_fsm_248.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st263_fsm_257.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st272_fsm_266.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st281_fsm_275.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st290_fsm_284.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st299_fsm_293.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st308_fsm_302.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st317_fsm_311.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st326_fsm_320.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st335_fsm_329.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st344_fsm_338.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st353_fsm_347.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st362_fsm_356.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st371_fsm_365.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st380_fsm_374.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st389_fsm_383.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st398_fsm_392.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st407_fsm_401.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st416_fsm_410.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st425_fsm_419.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st434_fsm_428.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st443_fsm_437.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st452_fsm_446.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st461_fsm_455.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st470_fsm_464.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st479_fsm_473.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st488_fsm_482.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st497_fsm_491.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st506_fsm_500.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st515_fsm_509.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st524_fsm_518.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st533_fsm_527.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st542_fsm_536.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st551_fsm_545.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st560_fsm_554.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st569_fsm_563.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st578_fsm_572.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st587_fsm_581.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st596_fsm_590.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st605_fsm_599.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st614_fsm_608.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st623_fsm_617.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st632_fsm_626.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st641_fsm_635.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st650_fsm_644.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st659_fsm_653.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st668_fsm_662.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st677_fsm_671.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st686_fsm_680.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st695_fsm_689.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st704_fsm_698.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st713_fsm_707.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st722_fsm_716.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st731_fsm_725.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st740_fsm_734.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st749_fsm_743.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st758_fsm_752.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st767_fsm_761.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st776_fsm_770.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st785_fsm_779.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st794_fsm_788.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st803_fsm_797.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st812_fsm_806.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st821_fsm_815.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st830_fsm_824.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st839_fsm_833.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st848_fsm_842.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st857_fsm_851.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st866_fsm_860.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st875_fsm_869.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st884_fsm_878.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st893_fsm_887.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st902_fsm_896.read()))) {
        pX_address0 = grp_projection_gp_K_fu_730_pX2_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read())) {
        pX_address0 = grp_projection_gp_train_full_bv_set_fu_707_pX_address0.read();
    } else {
        pX_address0 =  (sc_lv<4>) ("XXXX");
    }
}

void projection_gp::thread_pX_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it3.read())) {
        pX_ce0 = ap_const_logic_1;
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_5.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st20_fsm_14.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st29_fsm_23.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st38_fsm_32.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st47_fsm_41.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st56_fsm_50.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st65_fsm_59.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st74_fsm_68.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st83_fsm_77.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st92_fsm_86.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st101_fsm_95.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st110_fsm_104.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st119_fsm_113.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st128_fsm_122.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st137_fsm_131.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st146_fsm_140.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st155_fsm_149.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st164_fsm_158.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st173_fsm_167.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st182_fsm_176.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st191_fsm_185.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st200_fsm_194.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st209_fsm_203.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st218_fsm_212.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st227_fsm_221.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st236_fsm_230.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st245_fsm_239.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st254_fsm_248.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st263_fsm_257.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st272_fsm_266.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st281_fsm_275.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st290_fsm_284.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st299_fsm_293.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st308_fsm_302.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st317_fsm_311.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st326_fsm_320.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st335_fsm_329.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st344_fsm_338.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st353_fsm_347.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st362_fsm_356.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st371_fsm_365.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st380_fsm_374.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st389_fsm_383.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st398_fsm_392.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st407_fsm_401.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st416_fsm_410.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st425_fsm_419.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st434_fsm_428.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st443_fsm_437.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st452_fsm_446.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st461_fsm_455.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st470_fsm_464.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st479_fsm_473.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st488_fsm_482.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st497_fsm_491.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st506_fsm_500.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st515_fsm_509.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st524_fsm_518.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st533_fsm_527.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st542_fsm_536.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st551_fsm_545.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st560_fsm_554.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st569_fsm_563.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st578_fsm_572.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st587_fsm_581.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st596_fsm_590.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st605_fsm_599.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st614_fsm_608.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st623_fsm_617.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st632_fsm_626.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st641_fsm_635.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st650_fsm_644.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st659_fsm_653.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st668_fsm_662.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st677_fsm_671.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st686_fsm_680.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st695_fsm_689.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st704_fsm_698.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st713_fsm_707.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st722_fsm_716.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st731_fsm_725.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st740_fsm_734.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st749_fsm_743.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st758_fsm_752.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st767_fsm_761.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st776_fsm_770.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st785_fsm_779.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st794_fsm_788.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st803_fsm_797.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st812_fsm_806.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st821_fsm_815.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st830_fsm_824.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st839_fsm_833.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st848_fsm_842.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st857_fsm_851.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st866_fsm_860.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st875_fsm_869.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st884_fsm_878.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st893_fsm_887.read()) || 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st902_fsm_896.read()))) {
        pX_ce0 = grp_projection_gp_K_fu_730_pX2_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_4.read())) {
        pX_ce0 = grp_projection_gp_train_full_bv_set_fu_707_pX_ce0.read();
    } else {
        pX_ce0 = ap_const_logic_0;
    }
}

void projection_gp::thread_p_0_phi_fu_700_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st917_fsm_911.read()) && 
         !esl_seteq<1,1,1>(pPredict_read_reg_984.read(), ap_const_lv1_0))) {
        p_0_phi_fu_700_p4 = reg_868.read();
    } else {
        p_0_phi_fu_700_p4 = p_0_reg_696.read();
    }
}

void projection_gp::thread_p_shl1_cast_fu_906_p1() {
    p_shl1_cast_fu_906_p1 = esl_zext<12,8>(p_shl1_fu_898_p3.read());
}

void projection_gp::thread_p_shl1_fu_898_p3() {
    p_shl1_fu_898_p3 = esl_concat<7,1>(i_reg_663.read(), ap_const_lv1_0);
}

void projection_gp::thread_p_shl_cast_fu_894_p1() {
    p_shl_cast_fu_894_p1 = esl_zext<12,11>(p_shl_fu_886_p3.read());
}

void projection_gp::thread_p_shl_fu_886_p3() {
    p_shl_fu_886_p3 = esl_concat<7,4>(i_reg_663.read(), ap_const_lv4_0);
}

void projection_gp::thread_projection_gp_AXILiteS_s_axi_U_ap_dummy_ce() {
    projection_gp_AXILiteS_s_axi_U_ap_dummy_ce = ap_const_logic_1;
}

void projection_gp::thread_tmp_22_fu_920_p1() {
    tmp_22_fu_920_p1 = esl_zext<64,32>(tmp_cast_fu_916_p1.read());
}

void projection_gp::thread_tmp_40_fu_925_p2() {
    tmp_40_fu_925_p2 = (!bvCnt.read().is_01() || !ap_const_lv32_64.is_01())? sc_lv<1>(): sc_lv<1>(bvCnt.read() == ap_const_lv32_64);
}

void projection_gp::thread_tmp_41_fu_968_p2() {
    tmp_41_fu_968_p2 = (!bvCnt_load_1_reg_674.read().is_01() || !ap_const_lv32_1.is_01())? sc_lv<32>(): (sc_biguint<32>(bvCnt_load_1_reg_674.read()) + sc_biguint<32>(ap_const_lv32_1));
}

void projection_gp::thread_tmp_62_i_fu_958_p2() {
    tmp_62_i_fu_958_p2 = (!grp_fu_943_p2.read().is_01() || !i_i_cast2_fu_954_p1.read().is_01())? sc_lv<32>(): (sc_bigint<32>(grp_fu_943_p2.read()) + sc_biguint<32>(i_i_cast2_fu_954_p1.read()));
}

void projection_gp::thread_tmp_63_i_fu_964_p1() {
    tmp_63_i_fu_964_p1 = esl_zext<64,32>(tmp_62_i_reg_1036.read());
}

void projection_gp::thread_tmp_cast_fu_916_p1() {
    tmp_cast_fu_916_p1 = esl_sext<32,12>(tmp_s_fu_910_p2.read());
}

void projection_gp::thread_tmp_i_fu_949_p1() {
    tmp_i_fu_949_p1 = esl_zext<64,4>(ap_reg_ppstg_i_i_reg_684_pp0_it2.read());
}

void projection_gp::thread_tmp_s_fu_910_p2() {
    tmp_s_fu_910_p2 = (!p_shl_cast_fu_894_p1.read().is_01() || !p_shl1_cast_fu_906_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(p_shl_cast_fu_894_p1.read()) - sc_biguint<12>(p_shl1_cast_fu_906_p1.read()));
}

}

