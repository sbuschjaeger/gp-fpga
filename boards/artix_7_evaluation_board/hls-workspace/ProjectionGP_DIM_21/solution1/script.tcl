############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project ProjectionGP_DIM_21
set_top projection_gp
add_files ../../../implementation/hls/Projection_GP_DIM_21/include/ProjectionGP_config.h
add_files ../../../implementation/hls/Projection_GP_DIM_21/include/ProjectionGP_FULLY_OPTIMIZED.h
add_files ../../../implementation/hls/Projection_GP_DIM_21/include/ProjectionGP_FULLY_OPTIMIZED.cpp
open_solution "solution1"
set_part {xc7a200tfbg676-2}
create_clock -period 5 -name default
#source "./ProjectionGP_DIM_21/solution1/directives.tcl"
#csim_design
csynth_design
#cosim_design
export_design -format ip_catalog -description "PGP with Dim 13" -vendor "de.tudo.buschjae" -library "ml" -display_name "PGP_DIM_13"
