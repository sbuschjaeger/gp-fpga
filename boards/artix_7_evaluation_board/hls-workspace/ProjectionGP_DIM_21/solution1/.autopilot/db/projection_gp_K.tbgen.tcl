set moduleName projection_gp_K
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {projection_gp_K}
set C_modelType { float 32 }
set C_modelArgList { 
	{ pX1 float 32 regular {array 1313 { 1 3 } 1 1 }  }
	{ tmp_94 int 12 regular  }
	{ pX2 float 32 regular {array 13 { 1 } 1 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "pX1", "interface" : "memory", "bitwidth" : 32 ,"direction" : "READONLY" } , 
 	{ "Name" : "tmp_94", "interface" : "wire", "bitwidth" : 12 ,"direction" : "READONLY" } , 
 	{ "Name" : "pX2", "interface" : "memory", "bitwidth" : 32 ,"direction" : "READONLY" } , 
 	{ "Name" : "ap_return", "interface" : "wire", "bitwidth" : 32} ]}
# RTL Port declarations: 
set portNum 14
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ pX1_address0 sc_out sc_lv 11 signal 0 } 
	{ pX1_ce0 sc_out sc_logic 1 signal 0 } 
	{ pX1_q0 sc_in sc_lv 32 signal 0 } 
	{ tmp_94 sc_in sc_lv 12 signal 1 } 
	{ pX2_address0 sc_out sc_lv 4 signal 2 } 
	{ pX2_ce0 sc_out sc_logic 1 signal 2 } 
	{ pX2_q0 sc_in sc_lv 32 signal 2 } 
	{ ap_return sc_out sc_lv 32 signal -1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "pX1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "pX1", "role": "address0" }} , 
 	{ "name": "pX1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pX1", "role": "ce0" }} , 
 	{ "name": "pX1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "pX1", "role": "q0" }} , 
 	{ "name": "tmp_94", "direction": "in", "datatype": "sc_lv", "bitwidth":12, "type": "signal", "bundle":{"name": "tmp_94", "role": "default" }} , 
 	{ "name": "pX2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "pX2", "role": "address0" }} , 
 	{ "name": "pX2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pX2", "role": "ce0" }} , 
 	{ "name": "pX2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "pX2", "role": "q0" }} , 
 	{ "name": "ap_return", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "ap_return", "role": "default" }}  ]}
set Spec2ImplPortList { 
	pX1 { ap_memory {  { pX1_address0 mem_address 1 11 }  { pX1_ce0 mem_ce 1 1 }  { pX1_q0 mem_dout 0 32 } } }
	tmp_94 { ap_none {  { tmp_94 in_data 0 12 } } }
	pX2 { ap_memory {  { pX2_address0 mem_address 1 4 }  { pX2_ce0 mem_ce 1 1 }  { pX2_q0 mem_dout 0 32 } } }
}
