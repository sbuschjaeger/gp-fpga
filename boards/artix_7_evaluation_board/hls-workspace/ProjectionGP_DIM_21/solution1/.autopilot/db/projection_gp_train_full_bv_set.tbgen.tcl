set moduleName projection_gp_train_full_bv_set
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {projection_gp_train_full_bv_set}
set C_modelType { void 0 }
set C_modelArgList { 
	{ pX float 32 regular {array 13 { 1 } 1 1 }  }
	{ pY float 32 regular  }
	{ basisVectors float 32 regular {array 1313 { 2 2 } 1 1 } {global 2}  }
	{ Q float 32 regular {array 10201 { 2 2 } 1 1 } {global 2}  }
	{ bvCnt int 32 regular {pointer 0} {global 0}  }
	{ alpha float 32 regular {array 101 { 2 2 } 1 1 } {global 2}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "pX", "interface" : "memory", "bitwidth" : 32 ,"direction" : "READONLY" } , 
 	{ "Name" : "pY", "interface" : "wire", "bitwidth" : 32 ,"direction" : "READONLY" } , 
 	{ "Name" : "basisVectors", "interface" : "memory", "bitwidth" : 32 ,"direction" : "READWRITE" ,"bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "basisVectors","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1312,"step" : 1}]}]}],"extern" : 0} , 
 	{ "Name" : "Q", "interface" : "memory", "bitwidth" : 32 ,"direction" : "READWRITE" ,"bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "Q","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 10200,"step" : 1}]}]}],"extern" : 0} , 
 	{ "Name" : "bvCnt", "interface" : "wire", "bitwidth" : 32 ,"direction" : "READONLY" ,"bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "bvCnt","cData": "unsigned int","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}],"extern" : 0} , 
 	{ "Name" : "alpha", "interface" : "memory", "bitwidth" : 32 ,"direction" : "READWRITE" ,"bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "alpha","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 100,"step" : 1}]}]}],"extern" : 0} ]}
# RTL Port declarations: 
set portNum 41
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ pX_address0 sc_out sc_lv 4 signal 0 } 
	{ pX_ce0 sc_out sc_logic 1 signal 0 } 
	{ pX_q0 sc_in sc_lv 32 signal 0 } 
	{ pY sc_in sc_lv 32 signal 1 } 
	{ basisVectors_address0 sc_out sc_lv 11 signal 2 } 
	{ basisVectors_ce0 sc_out sc_logic 1 signal 2 } 
	{ basisVectors_we0 sc_out sc_logic 1 signal 2 } 
	{ basisVectors_d0 sc_out sc_lv 32 signal 2 } 
	{ basisVectors_q0 sc_in sc_lv 32 signal 2 } 
	{ basisVectors_address1 sc_out sc_lv 11 signal 2 } 
	{ basisVectors_ce1 sc_out sc_logic 1 signal 2 } 
	{ basisVectors_we1 sc_out sc_logic 1 signal 2 } 
	{ basisVectors_d1 sc_out sc_lv 32 signal 2 } 
	{ basisVectors_q1 sc_in sc_lv 32 signal 2 } 
	{ Q_address0 sc_out sc_lv 14 signal 3 } 
	{ Q_ce0 sc_out sc_logic 1 signal 3 } 
	{ Q_we0 sc_out sc_logic 1 signal 3 } 
	{ Q_d0 sc_out sc_lv 32 signal 3 } 
	{ Q_q0 sc_in sc_lv 32 signal 3 } 
	{ Q_address1 sc_out sc_lv 14 signal 3 } 
	{ Q_ce1 sc_out sc_logic 1 signal 3 } 
	{ Q_we1 sc_out sc_logic 1 signal 3 } 
	{ Q_d1 sc_out sc_lv 32 signal 3 } 
	{ Q_q1 sc_in sc_lv 32 signal 3 } 
	{ bvCnt sc_in sc_lv 32 signal 4 } 
	{ alpha_address0 sc_out sc_lv 7 signal 5 } 
	{ alpha_ce0 sc_out sc_logic 1 signal 5 } 
	{ alpha_we0 sc_out sc_logic 1 signal 5 } 
	{ alpha_d0 sc_out sc_lv 32 signal 5 } 
	{ alpha_q0 sc_in sc_lv 32 signal 5 } 
	{ alpha_address1 sc_out sc_lv 7 signal 5 } 
	{ alpha_ce1 sc_out sc_logic 1 signal 5 } 
	{ alpha_we1 sc_out sc_logic 1 signal 5 } 
	{ alpha_d1 sc_out sc_lv 32 signal 5 } 
	{ alpha_q1 sc_in sc_lv 32 signal 5 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "pX_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "pX", "role": "address0" }} , 
 	{ "name": "pX_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pX", "role": "ce0" }} , 
 	{ "name": "pX_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "pX", "role": "q0" }} , 
 	{ "name": "pY", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "pY", "role": "default" }} , 
 	{ "name": "basisVectors_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "basisVectors", "role": "address0" }} , 
 	{ "name": "basisVectors_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "basisVectors", "role": "ce0" }} , 
 	{ "name": "basisVectors_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "basisVectors", "role": "we0" }} , 
 	{ "name": "basisVectors_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "basisVectors", "role": "d0" }} , 
 	{ "name": "basisVectors_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "basisVectors", "role": "q0" }} , 
 	{ "name": "basisVectors_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "basisVectors", "role": "address1" }} , 
 	{ "name": "basisVectors_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "basisVectors", "role": "ce1" }} , 
 	{ "name": "basisVectors_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "basisVectors", "role": "we1" }} , 
 	{ "name": "basisVectors_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "basisVectors", "role": "d1" }} , 
 	{ "name": "basisVectors_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "basisVectors", "role": "q1" }} , 
 	{ "name": "Q_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "Q", "role": "address0" }} , 
 	{ "name": "Q_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "Q", "role": "ce0" }} , 
 	{ "name": "Q_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "Q", "role": "we0" }} , 
 	{ "name": "Q_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "Q", "role": "d0" }} , 
 	{ "name": "Q_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "Q", "role": "q0" }} , 
 	{ "name": "Q_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "Q", "role": "address1" }} , 
 	{ "name": "Q_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "Q", "role": "ce1" }} , 
 	{ "name": "Q_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "Q", "role": "we1" }} , 
 	{ "name": "Q_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "Q", "role": "d1" }} , 
 	{ "name": "Q_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "Q", "role": "q1" }} , 
 	{ "name": "bvCnt", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "bvCnt", "role": "default" }} , 
 	{ "name": "alpha_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "alpha", "role": "address0" }} , 
 	{ "name": "alpha_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "alpha", "role": "ce0" }} , 
 	{ "name": "alpha_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "alpha", "role": "we0" }} , 
 	{ "name": "alpha_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "alpha", "role": "d0" }} , 
 	{ "name": "alpha_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "alpha", "role": "q0" }} , 
 	{ "name": "alpha_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "alpha", "role": "address1" }} , 
 	{ "name": "alpha_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "alpha", "role": "ce1" }} , 
 	{ "name": "alpha_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "alpha", "role": "we1" }} , 
 	{ "name": "alpha_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "alpha", "role": "d1" }} , 
 	{ "name": "alpha_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "alpha", "role": "q1" }}  ]}
set Spec2ImplPortList { 
	pX { ap_memory {  { pX_address0 mem_address 1 4 }  { pX_ce0 mem_ce 1 1 }  { pX_q0 mem_dout 0 32 } } }
	pY { ap_none {  { pY in_data 0 32 } } }
	basisVectors { ap_memory {  { basisVectors_address0 mem_address 1 11 }  { basisVectors_ce0 mem_ce 1 1 }  { basisVectors_we0 mem_we 1 1 }  { basisVectors_d0 mem_din 1 32 }  { basisVectors_q0 mem_dout 0 32 }  { basisVectors_address1 mem_address 1 11 }  { basisVectors_ce1 mem_ce 1 1 }  { basisVectors_we1 mem_we 1 1 }  { basisVectors_d1 mem_din 1 32 }  { basisVectors_q1 mem_dout 0 32 } } }
	Q { ap_memory {  { Q_address0 mem_address 1 14 }  { Q_ce0 mem_ce 1 1 }  { Q_we0 mem_we 1 1 }  { Q_d0 mem_din 1 32 }  { Q_q0 mem_dout 0 32 }  { Q_address1 mem_address 1 14 }  { Q_ce1 mem_ce 1 1 }  { Q_we1 mem_we 1 1 }  { Q_d1 mem_din 1 32 }  { Q_q1 mem_dout 0 32 } } }
	bvCnt { ap_none {  { bvCnt in_data 0 32 } } }
	alpha { ap_memory {  { alpha_address0 mem_address 1 7 }  { alpha_ce0 mem_ce 1 1 }  { alpha_we0 mem_we 1 1 }  { alpha_d0 mem_din 1 32 }  { alpha_q0 mem_dout 0 32 }  { alpha_address1 mem_address 1 7 }  { alpha_ce1 mem_ce 1 1 }  { alpha_we1 mem_we 1 1 }  { alpha_d1 mem_din 1 32 }  { alpha_q1 mem_dout 0 32 } } }
}
