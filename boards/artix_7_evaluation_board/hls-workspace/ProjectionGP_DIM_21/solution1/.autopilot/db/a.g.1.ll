; ModuleID = '/home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace/ProjectionGP_DIM_21/solution1/.autopilot/db/a.g.1.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@C = global [10201 x float] zeroinitializer, align 16 ; [#uses=11 type=[10201 x float]*]
@Q = global [10201 x float] zeroinitializer, align 16 ; [#uses=12 type=[10201 x float]*]
@e = global [101 x float] zeroinitializer, align 16 ; [#uses=4 type=[101 x float]*]
@k = global [100 x float] zeroinitializer, align 16 ; [#uses=4 type=[100 x float]*]
@s = global [101 x float] zeroinitializer, align 16 ; [#uses=6 type=[101 x float]*]
@alpha = global [101 x float] zeroinitializer, align 16 ; [#uses=8 type=[101 x float]*]
@basisVectors = global [1313 x float] zeroinitializer, align 16 ; [#uses=5 type=[1313 x float]*]
@bvCnt = global i32 0, align 4                    ; [#uses=4 type=i32*]
@.str = private unnamed_addr constant [12 x i8] c"hls_label_0\00", align 1 ; [#uses=1 type=[12 x i8]*]
@.str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=1 type=[1 x i8]*]
@.str2 = private unnamed_addr constant [20 x i8] c"DELETE_BV_SWAP_LOOP\00", align 1 ; [#uses=1 type=[20 x i8]*]
@.str3 = private unnamed_addr constant [16 x i8] c"DELETE_BV_ALPHA\00", align 1 ; [#uses=1 type=[16 x i8]*]
@.str4 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_OUTER\00", align 1 ; [#uses=1 type=[18 x i8]*]
@.str5 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_INNER\00", align 1 ; [#uses=1 type=[18 x i8]*]
@.str6 = private unnamed_addr constant [20 x i8] c"DELETE_BV_UNSET_C_Q\00", align 1 ; [#uses=1 type=[20 x i8]*]
@.str7 = private unnamed_addr constant [7 x i8] c"CALC_K\00", align 1 ; [#uses=1 type=[7 x i8]*]
@.str8 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_OUTER\00", align 1 ; [#uses=1 type=[17 x i8]*]
@.str9 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_INNER\00", align 1 ; [#uses=1 type=[17 x i8]*]
@.str10 = private unnamed_addr constant [7 x i8] c"CALC_S\00", align 1 ; [#uses=1 type=[7 x i8]*]
@.str11 = private unnamed_addr constant [13 x i8] c"UPDATE_ALPHA\00", align 1 ; [#uses=1 type=[13 x i8]*]
@.str12 = private unnamed_addr constant [15 x i8] c"UPDATE_C_OUTER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str13 = private unnamed_addr constant [15 x i8] c"UPDATE_C_INNER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str14 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_OUTER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str15 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_INNER\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str16 = private unnamed_addr constant [8 x i8] c"COPY_BV\00", align 1 ; [#uses=1 type=[8 x i8]*]
@.str17 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1 ; [#uses=1 type=[10 x i8]*]
@.str18 = private unnamed_addr constant [5 x i8] c"INIT\00", align 1 ; [#uses=1 type=[5 x i8]*]
@.str19 = private unnamed_addr constant [11 x i8] c"PREDICTION\00", align 1 ; [#uses=1 type=[11 x i8]*]
@projection_gp.str = internal unnamed_addr constant [14 x i8] c"projection_gp\00" ; [#uses=1 type=[14 x i8]*]

; [#uses=2]
define internal fastcc float @K(float* %pX1, float* %pX2) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pX1}, i64 0, metadata !66), !dbg !67 ; [debug line = 19:21] [debug variable = pX1]
  call void @llvm.dbg.value(metadata !{float* %pX2}, i64 0, metadata !68), !dbg !69 ; [debug line = 19:42] [debug variable = pX2]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX1, i32 13) nounwind, !dbg !70 ; [debug line = 19:52]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX2, i32 13) nounwind, !dbg !72 ; [debug line = 19:83]
  br label %1, !dbg !73                           ; [debug line = 21:28]

; <label>:1                                       ; preds = %2, %0
  %sum = phi float [ 0.000000e+00, %0 ], [ %sum.1, %2 ] ; [#uses=2 type=float]
  %i = phi i32 [ 0, %0 ], [ %i.1, %2 ]            ; [#uses=3 type=i32]
  %exitcond = icmp eq i32 %i, 13, !dbg !73        ; [#uses=1 type=i1] [debug line = 21:28]
  br i1 %exitcond, label %3, label %2, !dbg !73   ; [debug line = 21:28]

; <label>:2                                       ; preds = %1
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([12 x i8]* @.str, i64 0, i64 0)) nounwind, !dbg !75 ; [#uses=1 type=i32] [debug line = 21:44]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !77 ; [debug line = 22:1]
  %tmp.2 = zext i32 %i to i64, !dbg !78           ; [#uses=2 type=i64] [debug line = 23:31]
  %pX1.addr = getelementptr inbounds float* %pX1, i64 %tmp.2, !dbg !78 ; [#uses=1 type=float*] [debug line = 23:31]
  %pX1.load = load float* %pX1.addr, align 4, !dbg !78 ; [#uses=2 type=float] [debug line = 23:31]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pX1.load) nounwind
  %pX2.addr = getelementptr inbounds float* %pX2, i64 %tmp.2, !dbg !78 ; [#uses=1 type=float*] [debug line = 23:31]
  %pX2.load = load float* %pX2.addr, align 4, !dbg !78 ; [#uses=2 type=float] [debug line = 23:31]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pX2.load) nounwind
  %val = fsub float %pX1.load, %pX2.load, !dbg !78 ; [#uses=2 type=float] [debug line = 23:31]
  call void @llvm.dbg.value(metadata !{float %val}, i64 0, metadata !79), !dbg !78 ; [debug line = 23:31] [debug variable = val]
  %tmp.3 = fmul float %val, %val, !dbg !80        ; [#uses=1 type=float] [debug line = 24:9]
  %sum.1 = fadd float %sum, %tmp.3, !dbg !80      ; [#uses=1 type=float] [debug line = 24:9]
  call void @llvm.dbg.value(metadata !{float %sum.1}, i64 0, metadata !81), !dbg !80 ; [debug line = 24:9] [debug variable = sum]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([12 x i8]* @.str, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !82 ; [#uses=0 type=i32] [debug line = 25:5]
  %i.1 = add i32 %i, 1, !dbg !83                  ; [#uses=1 type=i32] [debug line = 21:38]
  call void @llvm.dbg.value(metadata !{i32 %i.1}, i64 0, metadata !84), !dbg !83 ; [debug line = 21:38] [debug variable = i]
  br label %1, !dbg !83                           ; [debug line = 21:38]

; <label>:3                                       ; preds = %1
  %sum.0.lcssa = phi float [ %sum, %1 ]           ; [#uses=1 type=float]
  %tmp = fmul float %sum.0.lcssa, -5.000000e-01, !dbg !85 ; [#uses=1 type=float] [debug line = 27:12]
  %tmp.1 = call fastcc float @"std::exp"(float %tmp), !dbg !85 ; [#uses=1 type=float] [debug line = 27:12]
  ret float %tmp.1, !dbg !85                      ; [debug line = 27:12]
}

; [#uses=6]
declare void @_ssdm_SpecArrayDimSize(...) nounwind

; [#uses=6]
declare void @_ssdm_op_SpecPipeline(...) nounwind

; [#uses=1]
define internal fastcc float @"std::exp"(float %__x) nounwind uwtable inlinehint {
  call void @llvm.dbg.value(metadata !{float %__x}, i64 0, metadata !86), !dbg !87 ; [debug line = 216:13] [debug variable = __x]
  %tmp = call float @llvm.exp.f32(float %__x), !dbg !88 ; [#uses=1 type=float] [debug line = 217:12]
  ret float %tmp, !dbg !88                        ; [debug line = 217:12]
}

; [#uses=2]
define internal fastcc void @swapRowAndColumn(float* %pM, i32 %rowA) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pM}, i64 0, metadata !90), !dbg !91 ; [debug line = 30:29] [debug variable = pM]
  call void @llvm.dbg.value(metadata !{i32 %rowA}, i64 0, metadata !92), !dbg !93 ; [debug line = 30:79] [debug variable = rowA]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pM, i32 10201) nounwind, !dbg !94 ; [debug line = 30:106]
  %tmp = mul i32 %rowA, 101, !dbg !96             ; [#uses=1 type=i32] [debug line = 33:47]
  br label %1, !dbg !99                           ; [debug line = 31:28]

; <label>:1                                       ; preds = %2, %0
  %i = phi i32 [ 0, %0 ], [ %i.2, %2 ]            ; [#uses=4 type=i32]
  %exitcond1 = icmp eq i32 %i, 101, !dbg !99      ; [#uses=1 type=i1] [debug line = 31:28]
  br i1 %exitcond1, label %.preheader.preheader, label %2, !dbg !99 ; [debug line = 31:28]

.preheader.preheader:                             ; preds = %1
  br label %.preheader, !dbg !100                 ; [debug line = 38:28]

; <label>:2                                       ; preds = %1
  %tmp.6 = add i32 %i, %tmp, !dbg !96             ; [#uses=1 type=i32] [debug line = 33:47]
  %tmp.7 = zext i32 %tmp.6 to i64, !dbg !96       ; [#uses=1 type=i64] [debug line = 33:47]
  %pM.addr = getelementptr inbounds float* %pM, i64 %tmp.7, !dbg !96 ; [#uses=2 type=float*] [debug line = 33:47]
  %temp = load float* %pM.addr, align 4, !dbg !96 ; [#uses=2 type=float] [debug line = 33:47]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %temp) nounwind
  call void @llvm.dbg.value(metadata !{float %temp}, i64 0, metadata !102), !dbg !96 ; [debug line = 33:47] [debug variable = temp]
  %tmp.8 = add i32 %i, 10100, !dbg !103           ; [#uses=1 type=i32] [debug line = 34:9]
  %tmp.9 = zext i32 %tmp.8 to i64, !dbg !103      ; [#uses=1 type=i64] [debug line = 34:9]
  %pM.addr.1 = getelementptr inbounds float* %pM, i64 %tmp.9, !dbg !103 ; [#uses=2 type=float*] [debug line = 34:9]
  %pM.load = load float* %pM.addr.1, align 4, !dbg !103 ; [#uses=2 type=float] [debug line = 34:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pM.load) nounwind
  store float %pM.load, float* %pM.addr, align 4, !dbg !103 ; [debug line = 34:9]
  store float %temp, float* %pM.addr.1, align 4, !dbg !104 ; [debug line = 35:9]
  %i.2 = add i32 %i, 1, !dbg !105                 ; [#uses=1 type=i32] [debug line = 31:50]
  call void @llvm.dbg.value(metadata !{i32 %i.2}, i64 0, metadata !106), !dbg !105 ; [debug line = 31:50] [debug variable = i]
  br label %1, !dbg !105                          ; [debug line = 31:50]

.preheader:                                       ; preds = %3, %.preheader.preheader
  %i1 = phi i32 [ %i.3, %3 ], [ 0, %.preheader.preheader ] ; [#uses=3 type=i32]
  %exitcond = icmp eq i32 %i1, 101, !dbg !100     ; [#uses=1 type=i1] [debug line = 38:28]
  br i1 %exitcond, label %4, label %3, !dbg !100  ; [debug line = 38:28]

; <label>:3                                       ; preds = %.preheader
  %tmp.11 = mul i32 %i1, 101, !dbg !107           ; [#uses=2 type=i32] [debug line = 40:47]
  %tmp.12 = add i32 %tmp.11, %rowA, !dbg !107     ; [#uses=1 type=i32] [debug line = 40:47]
  %tmp.13 = zext i32 %tmp.12 to i64, !dbg !107    ; [#uses=1 type=i64] [debug line = 40:47]
  %pM.addr.2 = getelementptr inbounds float* %pM, i64 %tmp.13, !dbg !107 ; [#uses=2 type=float*] [debug line = 40:47]
  %temp.1 = load float* %pM.addr.2, align 4, !dbg !107 ; [#uses=2 type=float] [debug line = 40:47]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %temp.1) nounwind
  call void @llvm.dbg.value(metadata !{float %temp.1}, i64 0, metadata !109), !dbg !107 ; [debug line = 40:47] [debug variable = temp]
  %tmp.14 = add i32 %tmp.11, 100, !dbg !110       ; [#uses=1 type=i32] [debug line = 41:9]
  %tmp.15 = zext i32 %tmp.14 to i64, !dbg !110    ; [#uses=1 type=i64] [debug line = 41:9]
  %pM.addr.3 = getelementptr inbounds float* %pM, i64 %tmp.15, !dbg !110 ; [#uses=2 type=float*] [debug line = 41:9]
  %pM.load.2 = load float* %pM.addr.3, align 4, !dbg !110 ; [#uses=2 type=float] [debug line = 41:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pM.load.2) nounwind
  store float %pM.load.2, float* %pM.addr.2, align 4, !dbg !110 ; [debug line = 41:9]
  store float %temp.1, float* %pM.addr.3, align 4, !dbg !111 ; [debug line = 42:9]
  %i.3 = add i32 %i1, 1, !dbg !112                ; [#uses=1 type=i32] [debug line = 38:50]
  call void @llvm.dbg.value(metadata !{i32 %i.3}, i64 0, metadata !113), !dbg !112 ; [debug line = 38:50] [debug variable = i]
  br label %.preheader, !dbg !112                 ; [debug line = 38:50]

; <label>:4                                       ; preds = %.preheader
  ret void, !dbg !114                             ; [debug line = 44:1]
}

; [#uses=1]
define internal fastcc void @deleteBV(i32 %pIndex) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{i32 %pIndex}, i64 0, metadata !115), !dbg !116 ; [debug line = 46:29] [debug variable = pIndex]
  %tmp = zext i32 %pIndex to i64, !dbg !117       ; [#uses=1 type=i64] [debug line = 50:31]
  %alpha.addr = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp, !dbg !117 ; [#uses=2 type=float*] [debug line = 50:31]
  %temp = load float* %alpha.addr, align 4, !dbg !117 ; [#uses=2 type=float] [debug line = 50:31]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %temp) nounwind
  call void @llvm.dbg.value(metadata !{float %temp}, i64 0, metadata !119), !dbg !117 ; [debug line = 50:31] [debug variable = temp]
  %alpha.load = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16, !dbg !120 ; [#uses=2 type=float] [debug line = 51:5]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  store float %alpha.load, float* %alpha.addr, align 4, !dbg !120 ; [debug line = 51:5]
  store float %temp, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16, !dbg !121 ; [debug line = 52:5]
  call fastcc void @swapRowAndColumn(float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 0), i32 %pIndex), !dbg !122 ; [debug line = 54:5]
  call fastcc void @swapRowAndColumn(float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 0), i32 %pIndex), !dbg !123 ; [debug line = 55:5]
  %tmp.17 = mul i32 %pIndex, 13, !dbg !124        ; [#uses=1 type=i32] [debug line = 59:2]
  br label %1, !dbg !127                          ; [debug line = 57:48]

; <label>:1                                       ; preds = %2, %0
  %i = phi i32 [ 0, %0 ], [ %i.4, %2 ]            ; [#uses=4 type=i32]
  %exitcond4 = icmp eq i32 %i, 13, !dbg !127      ; [#uses=1 type=i1] [debug line = 57:48]
  br i1 %exitcond4, label %3, label %2, !dbg !127 ; [debug line = 57:48]

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([20 x i8]* @.str2, i64 0, i64 0)) nounwind, !dbg !128 ; [debug line = 57:64]
  %rbegin9 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([20 x i8]* @.str2, i64 0, i64 0)) nounwind, !dbg !128 ; [#uses=1 type=i32] [debug line = 57:64]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !129 ; [debug line = 58:1]
  %tmp.20 = add i32 %i, %tmp.17, !dbg !124        ; [#uses=1 type=i32] [debug line = 59:2]
  %tmp.21 = zext i32 %tmp.20 to i64, !dbg !124    ; [#uses=1 type=i64] [debug line = 59:2]
  %basisVectors.addr = getelementptr inbounds [1313 x float]* @basisVectors, i64 0, i64 %tmp.21, !dbg !124 ; [#uses=2 type=float*] [debug line = 59:2]
  %temp.3 = load float* %basisVectors.addr, align 4, !dbg !124 ; [#uses=2 type=float] [debug line = 59:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %temp.3) nounwind
  call void @llvm.dbg.value(metadata !{float %temp.3}, i64 0, metadata !119), !dbg !124 ; [debug line = 59:2] [debug variable = temp]
  %tmp.22 = add i32 %i, 1300, !dbg !130           ; [#uses=1 type=i32] [debug line = 60:9]
  %tmp.23 = zext i32 %tmp.22 to i64, !dbg !130    ; [#uses=1 type=i64] [debug line = 60:9]
  %basisVectors.addr.1 = getelementptr inbounds [1313 x float]* @basisVectors, i64 0, i64 %tmp.23, !dbg !130 ; [#uses=2 type=float*] [debug line = 60:9]
  %basisVectors.load = load float* %basisVectors.addr.1, align 4, !dbg !130 ; [#uses=2 type=float] [debug line = 60:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %basisVectors.load) nounwind
  store float %basisVectors.load, float* %basisVectors.addr, align 4, !dbg !130 ; [debug line = 60:9]
  store float %temp.3, float* %basisVectors.addr.1, align 4, !dbg !131 ; [debug line = 61:9]
  %rend10 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([20 x i8]* @.str2, i64 0, i64 0), i32 %rbegin9) nounwind, !dbg !132 ; [#uses=0 type=i32] [debug line = 62:5]
  %i.4 = add i32 %i, 1, !dbg !133                 ; [#uses=1 type=i32] [debug line = 57:58]
  call void @llvm.dbg.value(metadata !{i32 %i.4}, i64 0, metadata !134), !dbg !133 ; [debug line = 57:58] [debug variable = i]
  br label %1, !dbg !133                          ; [debug line = 57:58]

; <label>:3                                       ; preds = %1
  %alphaStar = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16, !dbg !135 ; [#uses=2 type=float] [debug line = 65:40]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alphaStar) nounwind
  call void @llvm.dbg.value(metadata !{float %alphaStar}, i64 0, metadata !136), !dbg !135 ; [debug line = 65:40] [debug variable = alphaStar]
  %cStar = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10200), align 16, !dbg !137 ; [#uses=2 type=float] [debug line = 66:24]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %cStar) nounwind
  call void @llvm.dbg.value(metadata !{float %cStar}, i64 0, metadata !138), !dbg !137 ; [debug line = 66:24] [debug variable = cStar]
  %qStar = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10200), align 16, !dbg !139 ; [#uses=3 type=float] [debug line = 84:45]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %qStar) nounwind
  call void @llvm.dbg.value(metadata !{float %qStar}, i64 0, metadata !144), !dbg !145 ; [debug line = 67:24] [debug variable = qStar]
  %tmp.18 = fadd float %cStar, %qStar, !dbg !146  ; [#uses=2 type=float] [debug line = 68:5]
  %temp.2 = fdiv float %alphaStar, %tmp.18, !dbg !146 ; [#uses=1 type=float] [debug line = 68:5]
  call void @llvm.dbg.value(metadata !{float %temp.2}, i64 0, metadata !119), !dbg !146 ; [debug line = 68:5] [debug variable = temp]
  br label %4, !dbg !147                          ; [debug line = 70:44]

; <label>:4                                       ; preds = %5, %3
  %i1 = phi i32 [ 0, %3 ], [ %i.5, %5 ]           ; [#uses=4 type=i32]
  %exitcond3 = icmp eq i32 %i1, 100, !dbg !147    ; [#uses=1 type=i1] [debug line = 70:44]
  br i1 %exitcond3, label %6, label %5, !dbg !147 ; [debug line = 70:44]

; <label>:5                                       ; preds = %4
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([16 x i8]* @.str3, i64 0, i64 0)) nounwind, !dbg !149 ; [debug line = 70:68]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([16 x i8]* @.str3, i64 0, i64 0)) nounwind, !dbg !149 ; [#uses=1 type=i32] [debug line = 70:68]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !151 ; [debug line = 71:1]
  %a = add i32 %i1, 10100, !dbg !152              ; [#uses=1 type=i32] [debug line = 72:46]
  call void @llvm.dbg.value(metadata !{i32 %a}, i64 0, metadata !153), !dbg !152 ; [debug line = 72:46] [debug variable = a]
  %tmp.25 = zext i32 %a to i64, !dbg !154         ; [#uses=2 type=i64] [debug line = 73:9]
  %C.addr = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.25, !dbg !154 ; [#uses=1 type=float*] [debug line = 73:9]
  %C.load = load float* %C.addr, align 4, !dbg !154 ; [#uses=2 type=float] [debug line = 73:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load) nounwind
  %Q.addr = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.25, !dbg !154 ; [#uses=1 type=float*] [debug line = 73:9]
  %Q.load = load float* %Q.addr, align 4, !dbg !154 ; [#uses=2 type=float] [debug line = 73:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load) nounwind
  %tmp.26 = fadd float %C.load, %Q.load, !dbg !154 ; [#uses=1 type=float] [debug line = 73:9]
  %tmp.27 = fmul float %tmp.26, %temp.2, !dbg !154 ; [#uses=1 type=float] [debug line = 73:9]
  %tmp.28 = zext i32 %i1 to i64, !dbg !154        ; [#uses=1 type=i64] [debug line = 73:9]
  %alpha.addr.1 = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp.28, !dbg !154 ; [#uses=2 type=float*] [debug line = 73:9]
  %alpha.load.2 = load float* %alpha.addr.1, align 4, !dbg !154 ; [#uses=2 type=float] [debug line = 73:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.2) nounwind
  %tmp.29 = fsub float %alpha.load.2, %tmp.27, !dbg !154 ; [#uses=1 type=float] [debug line = 73:9]
  store float %tmp.29, float* %alpha.addr.1, align 4, !dbg !154 ; [debug line = 73:9]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([16 x i8]* @.str3, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !155 ; [#uses=0 type=i32] [debug line = 74:5]
  %i.5 = add i32 %i1, 1, !dbg !156                ; [#uses=1 type=i32] [debug line = 70:62]
  call void @llvm.dbg.value(metadata !{i32 %i.5}, i64 0, metadata !157), !dbg !156 ; [debug line = 70:62] [debug variable = i]
  br label %4, !dbg !156                          ; [debug line = 70:62]

; <label>:6                                       ; preds = %4
  store float 0.000000e+00, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16, !dbg !158 ; [debug line = 75:5]
  br label %7, !dbg !159                          ; [debug line = 77:45]

; <label>:7                                       ; preds = %11, %6
  %i2 = phi i32 [ 0, %6 ], [ %i.7, %11 ]          ; [#uses=4 type=i32]
  %exitcond2 = icmp eq i32 %i2, 100, !dbg !159    ; [#uses=1 type=i1] [debug line = 77:45]
  br i1 %exitcond2, label %.preheader.preheader, label %8, !dbg !159 ; [debug line = 77:45]

.preheader.preheader:                             ; preds = %7
  br label %.preheader, !dbg !160                 ; [debug line = 97:48]

; <label>:8                                       ; preds = %7
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([18 x i8]* @.str4, i64 0, i64 0)) nounwind, !dbg !162 ; [debug line = 77:69]
  %rbegin7 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([18 x i8]* @.str4, i64 0, i64 0)) nounwind, !dbg !162 ; [#uses=1 type=i32] [debug line = 77:69]
  %a.2 = add i32 %i2, 10100, !dbg !163            ; [#uses=1 type=i32] [debug line = 81:46]
  %tmp.31 = mul i32 %i2, 101, !dbg !164           ; [#uses=1 type=i32] [debug line = 83:45]
  %tmp.32 = zext i32 %a.2 to i64, !dbg !139       ; [#uses=2 type=i64] [debug line = 84:45]
  %Q.addr.1 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.32, !dbg !139 ; [#uses=1 type=float*] [debug line = 84:45]
  %C.addr.1 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.32, !dbg !165 ; [#uses=1 type=float*] [debug line = 87:13]
  br label %9, !dbg !166                          ; [debug line = 79:47]

; <label>:9                                       ; preds = %10, %8
  %j = phi i32 [ 0, %8 ], [ %j.1, %10 ]           ; [#uses=4 type=i32]
  %exitcond1 = icmp eq i32 %j, 100, !dbg !166     ; [#uses=1 type=i1] [debug line = 79:47]
  br i1 %exitcond1, label %11, label %10, !dbg !166 ; [debug line = 79:47]

; <label>:10                                      ; preds = %9
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([18 x i8]* @.str5, i64 0, i64 0)) nounwind, !dbg !167 ; [debug line = 79:71]
  %rbegin5 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([18 x i8]* @.str5, i64 0, i64 0)) nounwind, !dbg !167 ; [#uses=1 type=i32] [debug line = 79:71]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !168 ; [debug line = 80:1]
  call void @llvm.dbg.value(metadata !{i32 %a.2}, i64 0, metadata !169), !dbg !163 ; [debug line = 81:46] [debug variable = a]
  %b.2 = add i32 %j, 10100, !dbg !170             ; [#uses=1 type=i32] [debug line = 82:54]
  call void @llvm.dbg.value(metadata !{i32 %b.2}, i64 0, metadata !171), !dbg !170 ; [debug line = 82:54] [debug variable = b]
  %c = add i32 %j, %tmp.31, !dbg !164             ; [#uses=1 type=i32] [debug line = 83:45]
  call void @llvm.dbg.value(metadata !{i32 %c}, i64 0, metadata !172), !dbg !164 ; [debug line = 83:45] [debug variable = c]
  %Q.load.1 = load float* %Q.addr.1, align 4, !dbg !139 ; [#uses=5 type=float] [debug line = 84:45]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.1) nounwind
  %tmp.38 = zext i32 %b.2 to i64, !dbg !139       ; [#uses=2 type=i64] [debug line = 84:45]
  %Q.addr.4 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.38, !dbg !139 ; [#uses=1 type=float*] [debug line = 84:45]
  %Q.load.2 = load float* %Q.addr.4, align 4, !dbg !139 ; [#uses=5 type=float] [debug line = 84:45]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.2) nounwind
  %tmp.39 = fmul float %Q.load.1, %Q.load.2, !dbg !139 ; [#uses=2 type=float] [debug line = 84:45]
  %temp.4 = fdiv float %tmp.39, %qStar, !dbg !139 ; [#uses=2 type=float] [debug line = 84:45]
  call void @llvm.dbg.value(metadata !{float %temp.4}, i64 0, metadata !173), !dbg !139 ; [debug line = 84:45] [debug variable = temp]
  %C.load.1 = load float* %C.addr.1, align 4, !dbg !165 ; [#uses=4 type=float] [debug line = 87:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.1) nounwind
  %C.addr.4 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.38, !dbg !165 ; [#uses=1 type=float*] [debug line = 87:13]
  %C.load.2 = load float* %C.addr.4, align 4, !dbg !165 ; [#uses=4 type=float] [debug line = 87:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.2) nounwind
  %tmp.40 = fmul float %C.load.1, %C.load.2, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.1) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.2) nounwind
  %tmp.41 = fmul float %C.load.1, %Q.load.2, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  %tmp.42 = fadd float %tmp.40, %tmp.41, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.1) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.2) nounwind
  %tmp.43 = fmul float %Q.load.1, %C.load.2, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  %tmp.44 = fadd float %tmp.42, %tmp.43, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.1) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.2) nounwind
  %tmp.45 = fadd float %tmp.44, %tmp.39, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  %tmp.46 = fdiv float %tmp.45, %tmp.18, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  %tmp.47 = fsub float %temp.4, %tmp.46, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  %tmp.48 = zext i32 %c to i64, !dbg !165         ; [#uses=2 type=i64] [debug line = 87:13]
  %C.addr.5 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.48, !dbg !165 ; [#uses=2 type=float*] [debug line = 87:13]
  %C.load.3 = load float* %C.addr.5, align 4, !dbg !165 ; [#uses=2 type=float] [debug line = 87:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.3) nounwind
  %tmp.49 = fadd float %C.load.3, %tmp.47, !dbg !165 ; [#uses=1 type=float] [debug line = 87:13]
  store float %tmp.49, float* %C.addr.5, align 4, !dbg !165 ; [debug line = 87:13]
  %Q.addr.5 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.48, !dbg !174 ; [#uses=2 type=float*] [debug line = 93:13]
  %Q.load.3 = load float* %Q.addr.5, align 4, !dbg !174 ; [#uses=2 type=float] [debug line = 93:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.3) nounwind
  %tmp.50 = fsub float %Q.load.3, %temp.4, !dbg !174 ; [#uses=1 type=float] [debug line = 93:13]
  store float %tmp.50, float* %Q.addr.5, align 4, !dbg !174 ; [debug line = 93:13]
  %rend6 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([18 x i8]* @.str5, i64 0, i64 0), i32 %rbegin5) nounwind, !dbg !175 ; [#uses=0 type=i32] [debug line = 94:9]
  %j.1 = add i32 %j, 1, !dbg !176                 ; [#uses=1 type=i32] [debug line = 79:65]
  call void @llvm.dbg.value(metadata !{i32 %j.1}, i64 0, metadata !177), !dbg !176 ; [debug line = 79:65] [debug variable = j]
  br label %9, !dbg !176                          ; [debug line = 79:65]

; <label>:11                                      ; preds = %9
  %rend8 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([18 x i8]* @.str4, i64 0, i64 0), i32 %rbegin7) nounwind, !dbg !178 ; [#uses=0 type=i32] [debug line = 95:5]
  %i.7 = add i32 %i2, 1, !dbg !179                ; [#uses=1 type=i32] [debug line = 77:63]
  call void @llvm.dbg.value(metadata !{i32 %i.7}, i64 0, metadata !180), !dbg !179 ; [debug line = 77:63] [debug variable = i]
  br label %7, !dbg !179                          ; [debug line = 77:63]

.preheader:                                       ; preds = %12, %.preheader.preheader
  %i5 = phi i32 [ %i.6, %12 ], [ 0, %.preheader.preheader ] ; [#uses=4 type=i32]
  %exitcond = icmp eq i32 %i5, 101, !dbg !160     ; [#uses=1 type=i1] [debug line = 97:48]
  br i1 %exitcond, label %13, label %12, !dbg !160 ; [debug line = 97:48]

; <label>:12                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([20 x i8]* @.str6, i64 0, i64 0)) nounwind, !dbg !181 ; [debug line = 97:73]
  %rbegin1 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([20 x i8]* @.str6, i64 0, i64 0)) nounwind, !dbg !181 ; [#uses=1 type=i32] [debug line = 97:73]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !183 ; [debug line = 98:1]
  %tmp.33 = mul i32 %i5, 101, !dbg !184           ; [#uses=1 type=i32] [debug line = 99:46]
  %a.1 = add i32 %tmp.33, 100, !dbg !184          ; [#uses=1 type=i32] [debug line = 99:46]
  call void @llvm.dbg.value(metadata !{i32 %a.1}, i64 0, metadata !185), !dbg !184 ; [debug line = 99:46] [debug variable = a]
  %b = add i32 %i5, 10100, !dbg !186              ; [#uses=1 type=i32] [debug line = 100:50]
  call void @llvm.dbg.value(metadata !{i32 %b}, i64 0, metadata !187), !dbg !186 ; [debug line = 100:50] [debug variable = b]
  %tmp.34 = zext i32 %a.1 to i64, !dbg !188       ; [#uses=2 type=i64] [debug line = 102:6]
  %Q.addr.2 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.34, !dbg !188 ; [#uses=1 type=float*] [debug line = 102:6]
  store float 0.000000e+00, float* %Q.addr.2, align 4, !dbg !188 ; [debug line = 102:6]
  %tmp.35 = zext i32 %b to i64, !dbg !189         ; [#uses=2 type=i64] [debug line = 103:9]
  %Q.addr.3 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.35, !dbg !189 ; [#uses=1 type=float*] [debug line = 103:9]
  store float 0.000000e+00, float* %Q.addr.3, align 4, !dbg !189 ; [debug line = 103:9]
  %C.addr.2 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.34, !dbg !190 ; [#uses=1 type=float*] [debug line = 104:9]
  store float 0.000000e+00, float* %C.addr.2, align 4, !dbg !190 ; [debug line = 104:9]
  %C.addr.3 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.35, !dbg !191 ; [#uses=1 type=float*] [debug line = 105:9]
  store float 0.000000e+00, float* %C.addr.3, align 4, !dbg !191 ; [debug line = 105:9]
  %rend12 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([20 x i8]* @.str6, i64 0, i64 0), i32 %rbegin1) nounwind, !dbg !192 ; [#uses=0 type=i32] [debug line = 106:5]
  %i.6 = add i32 %i5, 1, !dbg !193                ; [#uses=1 type=i32] [debug line = 97:67]
  call void @llvm.dbg.value(metadata !{i32 %i.6}, i64 0, metadata !194), !dbg !193 ; [debug line = 97:67] [debug variable = i]
  br label %.preheader, !dbg !193                 ; [debug line = 97:67]

; <label>:13                                      ; preds = %.preheader
  ret void, !dbg !195                             ; [debug line = 107:1]
}

; [#uses=17]
declare void @_ssdm_op_SpecLoopName(...) nounwind

; [#uses=10]
declare void @_ssdm_Unroll(...) nounwind

; [#uses=1]
define internal fastcc i32 @getMinKLApprox() nounwind uwtable {
  %alpha.load = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 0), align 16, !dbg !196 ; [#uses=4 type=float] [debug line = 111:91]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  %tmp = fmul float %alpha.load, %alpha.load, !dbg !196 ; [#uses=1 type=float] [debug line = 111:91]
  %Q.load = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 0), align 16, !dbg !196 ; [#uses=2 type=float] [debug line = 111:91]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load) nounwind
  %C.load = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 0), align 16, !dbg !196 ; [#uses=2 type=float] [debug line = 111:91]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load) nounwind
  %tmp.52 = fadd float %Q.load, %C.load, !dbg !196 ; [#uses=1 type=float] [debug line = 111:91]
  %minScore = fdiv float %tmp, %tmp.52, !dbg !196 ; [#uses=1 type=float] [debug line = 111:91]
  call void @llvm.dbg.value(metadata !{float %minScore}, i64 0, metadata !198), !dbg !196 ; [debug line = 111:91] [debug variable = minScore]
  br label %1, !dbg !199                          ; [debug line = 113:28]

; <label>:1                                       ; preds = %2, %0
  %index.2 = phi i32 [ 1, %0 ], [ %i, %2 ]        ; [#uses=5 type=i32]
  %minScore1 = phi float [ %minScore, %0 ], [ %minScore.1, %2 ] ; [#uses=2 type=float]
  %index = phi i32 [ 0, %0 ], [ %index.1, %2 ]    ; [#uses=2 type=i32]
  %exitcond = icmp eq i32 %index.2, 101, !dbg !199 ; [#uses=1 type=i1] [debug line = 113:28]
  br i1 %exitcond, label %3, label %2, !dbg !199  ; [debug line = 113:28]

; <label>:2                                       ; preds = %1
  %tmp.54 = zext i32 %index.2 to i64, !dbg !201   ; [#uses=1 type=i64] [debug line = 114:93]
  %alpha.addr = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp.54, !dbg !201 ; [#uses=1 type=float*] [debug line = 114:93]
  %alpha.load.3 = load float* %alpha.addr, align 4, !dbg !201 ; [#uses=4 type=float] [debug line = 114:93]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.3) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.3) nounwind
  %tmp.55 = fmul float %alpha.load.3, %alpha.load.3, !dbg !201 ; [#uses=1 type=float] [debug line = 114:93]
  %tmp.56 = mul i32 %index.2, 102, !dbg !201      ; [#uses=1 type=i32] [debug line = 114:93]
  %tmp.57 = zext i32 %tmp.56 to i64, !dbg !201    ; [#uses=2 type=i64] [debug line = 114:93]
  %Q.addr = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.57, !dbg !201 ; [#uses=1 type=float*] [debug line = 114:93]
  %Q.load.4 = load float* %Q.addr, align 8, !dbg !201 ; [#uses=2 type=float] [debug line = 114:93]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.4) nounwind
  %C.addr = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.57, !dbg !201 ; [#uses=1 type=float*] [debug line = 114:93]
  %C.load.4 = load float* %C.addr, align 8, !dbg !201 ; [#uses=2 type=float] [debug line = 114:93]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.4) nounwind
  %tmp.58 = fadd float %Q.load.4, %C.load.4, !dbg !201 ; [#uses=1 type=float] [debug line = 114:93]
  %tScore = fdiv float %tmp.55, %tmp.58, !dbg !201 ; [#uses=2 type=float] [debug line = 114:93]
  call void @llvm.dbg.value(metadata !{float %tScore}, i64 0, metadata !203), !dbg !201 ; [debug line = 114:93] [debug variable = tScore]
  %tmp.59 = fcmp olt float %tScore, %minScore1, !dbg !204 ; [#uses=2 type=i1] [debug line = 116:9]
  call void @llvm.dbg.value(metadata !{float %tScore}, i64 0, metadata !198), !dbg !205 ; [debug line = 117:13] [debug variable = minScore]
  call void @llvm.dbg.value(metadata !{i32 %index.2}, i64 0, metadata !207), !dbg !208 ; [debug line = 118:13] [debug variable = index]
  %minScore.1 = select i1 %tmp.59, float %tScore, float %minScore1, !dbg !204 ; [#uses=1 type=float] [debug line = 116:9]
  call void @llvm.dbg.value(metadata !{float %minScore.1}, i64 0, metadata !198), !dbg !204 ; [debug line = 116:9] [debug variable = minScore]
  %index.1 = select i1 %tmp.59, i32 %index.2, i32 %index, !dbg !204 ; [#uses=1 type=i32] [debug line = 116:9]
  call void @llvm.dbg.value(metadata !{i32 %index.1}, i64 0, metadata !207), !dbg !204 ; [debug line = 116:9] [debug variable = index]
  %i = add i32 %index.2, 1, !dbg !209             ; [#uses=1 type=i32] [debug line = 113:50]
  call void @llvm.dbg.value(metadata !{i32 %i}, i64 0, metadata !210), !dbg !209 ; [debug line = 113:50] [debug variable = i]
  br label %1, !dbg !209                          ; [debug line = 113:50]

; <label>:3                                       ; preds = %1
  %index.0.lcssa = phi i32 [ %index, %1 ]         ; [#uses=1 type=i32]
  ret i32 %index.0.lcssa, !dbg !211               ; [debug line = 122:5]
}

; [#uses=1]
define internal fastcc void @train_full_bv_set(float* %pX, float %pY) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pX}, i64 0, metadata !212), !dbg !213 ; [debug line = 125:37] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !214), !dbg !215 ; [debug line = 125:57] [debug variable = pY]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX, i32 13) nounwind, !dbg !216 ; [debug line = 125:62]
  br label %1, !dbg !218                          ; [debug line = 129:35]

; <label>:1                                       ; preds = %2, %0
  %i = phi i32 [ 0, %0 ], [ %i.8, %2 ]            ; [#uses=4 type=i32]
  %m = phi float [ 0x402B4EBEE0000000, %0 ], [ %m.1, %2 ] ; [#uses=2 type=float]
  %exitcond8 = icmp eq i32 %i, 100, !dbg !218     ; [#uses=1 type=i1] [debug line = 129:35]
  br i1 %exitcond8, label %.preheader10.preheader, label %2, !dbg !218 ; [debug line = 129:35]

.preheader10.preheader:                           ; preds = %1
  %m.0.lcssa = phi float [ %m, %1 ]               ; [#uses=1 type=float]
  br label %.preheader10, !dbg !220               ; [debug line = 140:45]

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([7 x i8]* @.str7, i64 0, i64 0)) nounwind, !dbg !222 ; [debug line = 129:59]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([7 x i8]* @.str7, i64 0, i64 0)) nounwind, !dbg !222 ; [#uses=1 type=i32] [debug line = 129:59]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !224 ; [debug line = 130:1]
  %tmp = mul i32 %i, 13, !dbg !225                ; [#uses=1 type=i32] [debug line = 131:9]
  %tmp.61 = zext i32 %tmp to i64, !dbg !225       ; [#uses=1 type=i64] [debug line = 131:9]
  %basisVectors.addr = getelementptr inbounds [1313 x float]* @basisVectors, i64 0, i64 %tmp.61, !dbg !225 ; [#uses=1 type=float*] [debug line = 131:9]
  %tmp.62 = call fastcc float @K(float* %basisVectors.addr, float* %pX), !dbg !225 ; [#uses=3 type=float] [debug line = 131:9]
  %tmp.63 = zext i32 %i to i64, !dbg !225         ; [#uses=2 type=i64] [debug line = 131:9]
  %k.addr = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp.63, !dbg !225 ; [#uses=1 type=float*] [debug line = 131:9]
  store float %tmp.62, float* %k.addr, align 4, !dbg !225 ; [debug line = 131:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp.62) nounwind
  %alpha.addr = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp.63, !dbg !226 ; [#uses=1 type=float*] [debug line = 132:9]
  %alpha.load = load float* %alpha.addr, align 4, !dbg !226 ; [#uses=2 type=float] [debug line = 132:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  %tmp.64 = fmul float %tmp.62, %alpha.load, !dbg !226 ; [#uses=1 type=float] [debug line = 132:9]
  %m.1 = fadd float %m, %tmp.64, !dbg !226        ; [#uses=1 type=float] [debug line = 132:9]
  call void @llvm.dbg.value(metadata !{float %m.1}, i64 0, metadata !227), !dbg !226 ; [debug line = 132:9] [debug variable = m]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([7 x i8]* @.str7, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !228 ; [#uses=0 type=i32] [debug line = 133:5]
  %i.8 = add i32 %i, 1, !dbg !229                 ; [#uses=1 type=i32] [debug line = 129:53]
  call void @llvm.dbg.value(metadata !{i32 %i.8}, i64 0, metadata !230), !dbg !229 ; [debug line = 129:53] [debug variable = i]
  br label %1, !dbg !229                          ; [debug line = 129:53]

.preheader10:                                     ; preds = %6, %.preheader10.preheader
  %i1 = phi i32 [ %i.10, %6 ], [ 0, %.preheader10.preheader ] ; [#uses=4 type=i32]
  %exitcond7 = icmp eq i32 %i1, 100, !dbg !220    ; [#uses=1 type=i1] [debug line = 140:45]
  br i1 %exitcond7, label %.preheader9.preheader, label %3, !dbg !220 ; [debug line = 140:45]

.preheader9.preheader:                            ; preds = %.preheader10
  br label %.preheader9, !dbg !231                ; [debug line = 151:36]

; <label>:3                                       ; preds = %.preheader10
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([17 x i8]* @.str8, i64 0, i64 0)) nounwind, !dbg !233 ; [debug line = 140:69]
  %rbegin2 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([17 x i8]* @.str8, i64 0, i64 0)) nounwind, !dbg !233 ; [#uses=1 type=i32] [debug line = 140:69]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !235 ; [debug line = 141:1]
  %tmp.67 = zext i32 %i1 to i64, !dbg !236        ; [#uses=2 type=i64] [debug line = 142:2]
  %s.addr = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp.67, !dbg !236 ; [#uses=2 type=float*] [debug line = 142:2]
  store float 0.000000e+00, float* %s.addr, align 4, !dbg !236 ; [debug line = 142:2]
  %e.addr = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp.67, !dbg !237 ; [#uses=2 type=float*] [debug line = 143:6]
  store float 0.000000e+00, float* %e.addr, align 4, !dbg !237 ; [debug line = 143:6]
  %tmp.68 = mul i32 %i1, 101, !dbg !238           ; [#uses=1 type=i32] [debug line = 146:2]
  br label %4, !dbg !241                          ; [debug line = 144:49]

; <label>:4                                       ; preds = %5, %3
  %tmp.69 = phi float [ 0.000000e+00, %3 ], [ %tmp.88, %5 ] ; [#uses=2 type=float]
  %tmp.70 = phi float [ 0.000000e+00, %3 ], [ %tmp.86, %5 ] ; [#uses=2 type=float]
  %j = phi i32 [ 0, %3 ], [ %j.2, %5 ]            ; [#uses=4 type=i32]
  %exitcond6 = icmp eq i32 %j, 100, !dbg !241     ; [#uses=1 type=i1] [debug line = 144:49]
  br i1 %exitcond6, label %6, label %5, !dbg !241 ; [debug line = 144:49]

; <label>:5                                       ; preds = %4
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([17 x i8]* @.str9, i64 0, i64 0)) nounwind, !dbg !242 ; [debug line = 144:73]
  %rbegin4 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([17 x i8]* @.str9, i64 0, i64 0)) nounwind, !dbg !242 ; [#uses=1 type=i32] [debug line = 144:73]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !243 ; [debug line = 145:1]
  %tmp.82 = add i32 %j, %tmp.68, !dbg !238        ; [#uses=1 type=i32] [debug line = 146:2]
  %tmp.83 = zext i32 %tmp.82 to i64, !dbg !238    ; [#uses=2 type=i64] [debug line = 146:2]
  %C.addr = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.83, !dbg !238 ; [#uses=1 type=float*] [debug line = 146:2]
  %C.load = load float* %C.addr, align 4, !dbg !238 ; [#uses=2 type=float] [debug line = 146:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load) nounwind
  %tmp.84 = zext i32 %j to i64, !dbg !238         ; [#uses=1 type=i64] [debug line = 146:2]
  %k.addr.2 = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp.84, !dbg !238 ; [#uses=1 type=float*] [debug line = 146:2]
  %k.load.1 = load float* %k.addr.2, align 4, !dbg !238 ; [#uses=4 type=float] [debug line = 146:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %k.load.1) nounwind
  %tmp.85 = fmul float %C.load, %k.load.1, !dbg !238 ; [#uses=1 type=float] [debug line = 146:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp.70) nounwind
  %tmp.86 = fadd float %tmp.70, %tmp.85, !dbg !238 ; [#uses=2 type=float] [debug line = 146:2]
  store float %tmp.86, float* %s.addr, align 4, !dbg !238 ; [debug line = 146:2]
  %Q.addr = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.83, !dbg !244 ; [#uses=1 type=float*] [debug line = 147:10]
  %Q.load = load float* %Q.addr, align 4, !dbg !244 ; [#uses=2 type=float] [debug line = 147:10]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %k.load.1) nounwind
  %tmp.87 = fmul float %Q.load, %k.load.1, !dbg !244 ; [#uses=1 type=float] [debug line = 147:10]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp.69) nounwind
  %tmp.88 = fadd float %tmp.69, %tmp.87, !dbg !244 ; [#uses=2 type=float] [debug line = 147:10]
  store float %tmp.88, float* %e.addr, align 4, !dbg !244 ; [debug line = 147:10]
  %rend15 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([17 x i8]* @.str9, i64 0, i64 0), i32 %rbegin4) nounwind, !dbg !245 ; [#uses=0 type=i32] [debug line = 148:9]
  %j.2 = add i32 %j, 1, !dbg !246                 ; [#uses=1 type=i32] [debug line = 144:67]
  call void @llvm.dbg.value(metadata !{i32 %j.2}, i64 0, metadata !247), !dbg !246 ; [debug line = 144:67] [debug variable = j]
  br label %4, !dbg !246                          ; [debug line = 144:67]

; <label>:6                                       ; preds = %4
  %rend17 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([17 x i8]* @.str8, i64 0, i64 0), i32 %rbegin2) nounwind, !dbg !248 ; [#uses=0 type=i32] [debug line = 149:5]
  %i.10 = add i32 %i1, 1, !dbg !249               ; [#uses=1 type=i32] [debug line = 140:63]
  call void @llvm.dbg.value(metadata !{i32 %i.10}, i64 0, metadata !250), !dbg !249 ; [debug line = 140:63] [debug variable = i]
  br label %.preheader10, !dbg !249               ; [debug line = 140:63]

.preheader9:                                      ; preds = %7, %.preheader9.preheader
  %i2 = phi i32 [ %i.9, %7 ], [ 0, %.preheader9.preheader ] ; [#uses=3 type=i32]
  %sigma2 = phi float [ %sigma2.1, %7 ], [ 1.000000e+00, %.preheader9.preheader ] ; [#uses=2 type=float]
  %exitcond5 = icmp eq i32 %i2, 100, !dbg !231    ; [#uses=1 type=i1] [debug line = 151:36]
  br i1 %exitcond5, label %8, label %7, !dbg !231 ; [debug line = 151:36]

; <label>:7                                       ; preds = %.preheader9
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([7 x i8]* @.str10, i64 0, i64 0)) nounwind, !dbg !251 ; [debug line = 151:60]
  %rbegin3 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([7 x i8]* @.str10, i64 0, i64 0)) nounwind, !dbg !251 ; [#uses=1 type=i32] [debug line = 151:60]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !253 ; [debug line = 152:1]
  %tmp.77 = zext i32 %i2 to i64, !dbg !254        ; [#uses=2 type=i64] [debug line = 153:2]
  %s.addr.1 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp.77, !dbg !254 ; [#uses=1 type=float*] [debug line = 153:2]
  %s.load = load float* %s.addr.1, align 4, !dbg !254 ; [#uses=2 type=float] [debug line = 153:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %s.load) nounwind
  %k.addr.1 = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp.77, !dbg !254 ; [#uses=1 type=float*] [debug line = 153:2]
  %k.load = load float* %k.addr.1, align 4, !dbg !254 ; [#uses=2 type=float] [debug line = 153:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %k.load) nounwind
  %tmp.78 = fmul float %s.load, %k.load, !dbg !254 ; [#uses=1 type=float] [debug line = 153:2]
  %sigma2.1 = fadd float %sigma2, %tmp.78, !dbg !254 ; [#uses=1 type=float] [debug line = 153:2]
  call void @llvm.dbg.value(metadata !{float %sigma2.1}, i64 0, metadata !255), !dbg !254 ; [debug line = 153:2] [debug variable = sigma2]
  %rend13 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([7 x i8]* @.str10, i64 0, i64 0), i32 %rbegin3) nounwind, !dbg !256 ; [#uses=0 type=i32] [debug line = 154:5]
  %i.9 = add i32 %i2, 1, !dbg !257                ; [#uses=1 type=i32] [debug line = 151:54]
  call void @llvm.dbg.value(metadata !{i32 %i.9}, i64 0, metadata !258), !dbg !257 ; [debug line = 151:54] [debug variable = i]
  br label %.preheader9, !dbg !257                ; [debug line = 151:54]

; <label>:8                                       ; preds = %.preheader9
  %sigma2.0.lcssa = phi float [ %sigma2, %.preheader9 ] ; [#uses=1 type=float]
  store float 1.000000e+00, float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 100), align 16, !dbg !259 ; [debug line = 155:5]
  %tmp.71 = fsub float %pY, %m.0.lcssa, !dbg !260 ; [#uses=1 type=float] [debug line = 157:40]
  %tmp.72 = fpext float %tmp.71 to double, !dbg !260 ; [#uses=1 type=double] [debug line = 157:40]
  %tmp.73 = fpext float %sigma2.0.lcssa to double, !dbg !260 ; [#uses=1 type=double] [debug line = 157:40]
  %tmp.74 = fadd double %tmp.73, 5.000000e-01, !dbg !260 ; [#uses=2 type=double] [debug line = 157:40]
  %tmp.75 = fdiv double %tmp.72, %tmp.74, !dbg !260 ; [#uses=1 type=double] [debug line = 157:40]
  %q = fptrunc double %tmp.75 to float, !dbg !260 ; [#uses=2 type=float] [debug line = 157:40]
  call void @llvm.dbg.value(metadata !{float %q}, i64 0, metadata !261), !dbg !260 ; [debug line = 157:40] [debug variable = q]
  %tmp.76 = fdiv double -1.000000e+00, %tmp.74, !dbg !262 ; [#uses=1 type=double] [debug line = 158:35]
  %r = fptrunc double %tmp.76 to float, !dbg !262 ; [#uses=1 type=float] [debug line = 158:35]
  call void @llvm.dbg.value(metadata !{float %r}, i64 0, metadata !263), !dbg !262 ; [debug line = 158:35] [debug variable = r]
  br label %9, !dbg !264                          ; [debug line = 161:42]

; <label>:9                                       ; preds = %10, %8
  %gamma = phi float [ 1.000000e+00, %8 ], [ %gamma.1, %10 ] ; [#uses=2 type=float]
  %i3 = phi i32 [ 0, %8 ], [ %i.11, %10 ]         ; [#uses=3 type=i32]
  %exitcond4 = icmp eq i32 %i3, 100, !dbg !264    ; [#uses=1 type=i1] [debug line = 161:42]
  br i1 %exitcond4, label %11, label %10, !dbg !264 ; [debug line = 161:42]

; <label>:10                                      ; preds = %9
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([13 x i8]* @.str11, i64 0, i64 0)) nounwind, !dbg !266 ; [debug line = 161:66]
  %rbegin5 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([13 x i8]* @.str11, i64 0, i64 0)) nounwind, !dbg !266 ; [#uses=1 type=i32] [debug line = 161:66]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !268 ; [debug line = 162:1]
  %tmp.91 = zext i32 %i3 to i64, !dbg !269        ; [#uses=4 type=i64] [debug line = 163:2]
  %e.addr.1 = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp.91, !dbg !269 ; [#uses=1 type=float*] [debug line = 163:2]
  %e.load = load float* %e.addr.1, align 4, !dbg !269 ; [#uses=2 type=float] [debug line = 163:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %e.load) nounwind
  %k.addr.3 = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp.91, !dbg !269 ; [#uses=1 type=float*] [debug line = 163:2]
  %k.load.2 = load float* %k.addr.3, align 4, !dbg !269 ; [#uses=2 type=float] [debug line = 163:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %k.load.2) nounwind
  %tmp.92 = fmul float %e.load, %k.load.2, !dbg !269 ; [#uses=1 type=float] [debug line = 163:2]
  %gamma.1 = fsub float %gamma, %tmp.92, !dbg !269 ; [#uses=1 type=float] [debug line = 163:2]
  call void @llvm.dbg.value(metadata !{float %gamma.1}, i64 0, metadata !270), !dbg !269 ; [debug line = 163:2] [debug variable = gamma]
  %s.addr.2 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp.91, !dbg !271 ; [#uses=1 type=float*] [debug line = 164:6]
  %s.load.1 = load float* %s.addr.2, align 4, !dbg !271 ; [#uses=2 type=float] [debug line = 164:6]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %s.load.1) nounwind
  %tmp.94 = fmul float %s.load.1, %q, !dbg !271   ; [#uses=1 type=float] [debug line = 164:6]
  %alpha.addr.2 = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp.91, !dbg !271 ; [#uses=2 type=float*] [debug line = 164:6]
  %alpha.load.5 = load float* %alpha.addr.2, align 4, !dbg !271 ; [#uses=2 type=float] [debug line = 164:6]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.5) nounwind
  %tmp.95 = fadd float %alpha.load.5, %tmp.94, !dbg !271 ; [#uses=1 type=float] [debug line = 164:6]
  store float %tmp.95, float* %alpha.addr.2, align 4, !dbg !271 ; [debug line = 164:6]
  %rend19 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([13 x i8]* @.str11, i64 0, i64 0), i32 %rbegin5) nounwind, !dbg !272 ; [#uses=0 type=i32] [debug line = 165:5]
  %i.11 = add i32 %i3, 1, !dbg !273               ; [#uses=1 type=i32] [debug line = 161:60]
  call void @llvm.dbg.value(metadata !{i32 %i.11}, i64 0, metadata !274), !dbg !273 ; [debug line = 161:60] [debug variable = i]
  br label %9, !dbg !273                          ; [debug line = 161:60]

; <label>:11                                      ; preds = %9
  %gamma.0.lcssa = phi float [ %gamma, %9 ]       ; [#uses=1 type=float]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float 1.000000e+00) nounwind
  %alpha.load.4 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16, !dbg !275 ; [#uses=2 type=float] [debug line = 171:5]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load.4) nounwind
  %tmp.90 = fadd float %alpha.load.4, %q, !dbg !275 ; [#uses=1 type=float] [debug line = 171:5]
  store float %tmp.90, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16, !dbg !275 ; [debug line = 171:5]
  br label %12, !dbg !276                         ; [debug line = 174:43]

; <label>:12                                      ; preds = %16, %11
  %i4 = phi i32 [ 0, %11 ], [ %i.12, %16 ]        ; [#uses=4 type=i32]
  %exitcond3 = icmp eq i32 %i4, 101, !dbg !276    ; [#uses=1 type=i1] [debug line = 174:43]
  br i1 %exitcond3, label %.preheader.preheader, label %13, !dbg !276 ; [debug line = 174:43]

.preheader.preheader:                             ; preds = %12
  %tmp.97 = fpext float %gamma.0.lcssa to double, !dbg !278 ; [#uses=1 type=double] [debug line = 190:4]
  br label %.preheader, !dbg !283                 ; [debug line = 183:43]

; <label>:13                                      ; preds = %12
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str12, i64 0, i64 0)) nounwind, !dbg !284 ; [debug line = 174:68]
  %rbegin6 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str12, i64 0, i64 0)) nounwind, !dbg !284 ; [#uses=1 type=i32] [debug line = 174:68]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !286 ; [debug line = 175:1]
  %tmp.98 = zext i32 %i4 to i64, !dbg !287        ; [#uses=1 type=i64] [debug line = 179:13]
  %s.addr.3 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp.98, !dbg !287 ; [#uses=1 type=float*] [debug line = 179:13]
  %tmp.99 = mul i32 %i4, 101, !dbg !287           ; [#uses=1 type=i32] [debug line = 179:13]
  br label %14, !dbg !290                         ; [debug line = 176:40]

; <label>:14                                      ; preds = %15, %13
  %j5 = phi i32 [ 0, %13 ], [ %j.3, %15 ]         ; [#uses=4 type=i32]
  %exitcond2 = icmp eq i32 %j5, 101, !dbg !290    ; [#uses=1 type=i1] [debug line = 176:40]
  br i1 %exitcond2, label %16, label %15, !dbg !290 ; [debug line = 176:40]

; <label>:15                                      ; preds = %14
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str13, i64 0, i64 0)) nounwind, !dbg !291 ; [debug line = 176:65]
  %rbegin8 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str13, i64 0, i64 0)) nounwind, !dbg !291 ; [#uses=1 type=i32] [debug line = 176:65]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !292 ; [debug line = 177:1]
  %s.load.2 = load float* %s.addr.3, align 4, !dbg !287 ; [#uses=2 type=float] [debug line = 179:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %s.load.2) nounwind
  %tmp.104 = zext i32 %j5 to i64, !dbg !287       ; [#uses=1 type=i64] [debug line = 179:13]
  %s.addr.4 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp.104, !dbg !287 ; [#uses=1 type=float*] [debug line = 179:13]
  %s.load.3 = load float* %s.addr.4, align 4, !dbg !287 ; [#uses=2 type=float] [debug line = 179:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %s.load.3) nounwind
  %tmp.105 = fmul float %s.load.2, %s.load.3, !dbg !287 ; [#uses=1 type=float] [debug line = 179:13]
  %tmp.106 = fmul float %tmp.105, %r, !dbg !287   ; [#uses=1 type=float] [debug line = 179:13]
  %tmp.107 = add i32 %j5, %tmp.99, !dbg !287      ; [#uses=1 type=i32] [debug line = 179:13]
  %tmp.108 = zext i32 %tmp.107 to i64, !dbg !287  ; [#uses=1 type=i64] [debug line = 179:13]
  %C.addr.6 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp.108, !dbg !287 ; [#uses=2 type=float*] [debug line = 179:13]
  %C.load.5 = load float* %C.addr.6, align 4, !dbg !287 ; [#uses=2 type=float] [debug line = 179:13]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %C.load.5) nounwind
  %tmp.109 = fadd float %C.load.5, %tmp.106, !dbg !287 ; [#uses=1 type=float] [debug line = 179:13]
  store float %tmp.109, float* %C.addr.6, align 4, !dbg !287 ; [debug line = 179:13]
  %rend21 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str13, i64 0, i64 0), i32 %rbegin8) nounwind, !dbg !293 ; [#uses=0 type=i32] [debug line = 180:9]
  %j.3 = add i32 %j5, 1, !dbg !294                ; [#uses=1 type=i32] [debug line = 176:59]
  call void @llvm.dbg.value(metadata !{i32 %j.3}, i64 0, metadata !295), !dbg !294 ; [debug line = 176:59] [debug variable = j]
  br label %14, !dbg !294                         ; [debug line = 176:59]

; <label>:16                                      ; preds = %14
  %rend23 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str12, i64 0, i64 0), i32 %rbegin6) nounwind, !dbg !296 ; [#uses=0 type=i32] [debug line = 181:5]
  %i.12 = add i32 %i4, 1, !dbg !297               ; [#uses=1 type=i32] [debug line = 174:62]
  call void @llvm.dbg.value(metadata !{i32 %i.12}, i64 0, metadata !298), !dbg !297 ; [debug line = 174:62] [debug variable = i]
  br label %12, !dbg !297                         ; [debug line = 174:62]

.preheader:                                       ; preds = %22, %.preheader.preheader
  %i6 = phi i32 [ %i.13, %22 ], [ 0, %.preheader.preheader ] ; [#uses=5 type=i32]
  %exitcond1 = icmp eq i32 %i6, 101, !dbg !283    ; [#uses=1 type=i1] [debug line = 183:43]
  br i1 %exitcond1, label %23, label %17, !dbg !283 ; [debug line = 183:43]

; <label>:17                                      ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str14, i64 0, i64 0)) nounwind, !dbg !299 ; [debug line = 183:68]
  %rbegin7 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str14, i64 0, i64 0)) nounwind, !dbg !299 ; [#uses=1 type=i32] [debug line = 183:68]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !300 ; [debug line = 184:1]
  %tmp.100 = icmp eq i32 %i6, 100, !dbg !301      ; [#uses=1 type=i1] [debug line = 187:40]
  %tmp.101 = mul i32 %i6, 101, !dbg !278          ; [#uses=1 type=i32] [debug line = 190:4]
  %tmp.102 = zext i32 %i6 to i64, !dbg !301       ; [#uses=1 type=i64] [debug line = 187:40]
  %e.addr.2 = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp.102, !dbg !301 ; [#uses=1 type=float*] [debug line = 187:40]
  br label %18, !dbg !302                         ; [debug line = 185:40]

; <label>:18                                      ; preds = %._crit_edge11, %17
  %j7 = phi i32 [ 0, %17 ], [ %j.4, %._crit_edge11 ] ; [#uses=5 type=i32]
  %exitcond = icmp eq i32 %j7, 101, !dbg !302     ; [#uses=1 type=i1] [debug line = 185:40]
  br i1 %exitcond, label %22, label %19, !dbg !302 ; [debug line = 185:40]

; <label>:19                                      ; preds = %18
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str15, i64 0, i64 0)) nounwind, !dbg !303 ; [debug line = 185:65]
  %rbegin9 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str15, i64 0, i64 0)) nounwind, !dbg !303 ; [#uses=1 type=i32] [debug line = 185:65]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !304 ; [debug line = 186:1]
  br i1 %tmp.100, label %._crit_edge, label %20, !dbg !301 ; [debug line = 187:40]

; <label>:20                                      ; preds = %19
  %e.load.1 = load float* %e.addr.2, align 4, !dbg !301 ; [#uses=2 type=float] [debug line = 187:40]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %e.load.1) nounwind
  br label %._crit_edge, !dbg !301                ; [debug line = 187:40]

._crit_edge:                                      ; preds = %20, %19
  %ti = phi float [ %e.load.1, %20 ], [ -1.000000e+00, %19 ], !dbg !301 ; [#uses=1 type=float] [debug line = 187:40]
  call void @llvm.dbg.value(metadata !{float %ti}, i64 0, metadata !305), !dbg !301 ; [debug line = 187:40] [debug variable = ti]
  %tmp.112 = icmp eq i32 %j7, 100, !dbg !306      ; [#uses=1 type=i1] [debug line = 188:42]
  br i1 %tmp.112, label %._crit_edge11, label %21, !dbg !306 ; [debug line = 188:42]

; <label>:21                                      ; preds = %._crit_edge
  %tmp.113 = zext i32 %j7 to i64, !dbg !306       ; [#uses=1 type=i64] [debug line = 188:42]
  %e.addr.3 = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp.113, !dbg !306 ; [#uses=1 type=float*] [debug line = 188:42]
  %e.load.2 = load float* %e.addr.3, align 4, !dbg !306 ; [#uses=2 type=float] [debug line = 188:42]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %e.load.2) nounwind
  br label %._crit_edge11, !dbg !306              ; [debug line = 188:42]

._crit_edge11:                                    ; preds = %21, %._crit_edge
  %tj = phi float [ %e.load.2, %21 ], [ -1.000000e+00, %._crit_edge ], !dbg !306 ; [#uses=1 type=float] [debug line = 188:42]
  call void @llvm.dbg.value(metadata !{float %tj}, i64 0, metadata !307), !dbg !306 ; [debug line = 188:42] [debug variable = tj]
  %tmp.114 = fmul float %ti, %tj, !dbg !278       ; [#uses=1 type=float] [debug line = 190:4]
  %tmp.115 = fpext float %tmp.114 to double, !dbg !278 ; [#uses=1 type=double] [debug line = 190:4]
  %tmp.116 = fdiv double %tmp.115, %tmp.97, !dbg !278 ; [#uses=1 type=double] [debug line = 190:4]
  %tmp.117 = add i32 %j7, %tmp.101, !dbg !278     ; [#uses=1 type=i32] [debug line = 190:4]
  %tmp.118 = zext i32 %tmp.117 to i64, !dbg !278  ; [#uses=1 type=i64] [debug line = 190:4]
  %Q.addr.6 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.118, !dbg !278 ; [#uses=2 type=float*] [debug line = 190:4]
  %Q.load.5 = load float* %Q.addr.6, align 4, !dbg !278 ; [#uses=2 type=float] [debug line = 190:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %Q.load.5) nounwind
  %tmp.119 = fpext float %Q.load.5 to double, !dbg !278 ; [#uses=1 type=double] [debug line = 190:4]
  %tmp.120 = fadd double %tmp.119, %tmp.116, !dbg !278 ; [#uses=1 type=double] [debug line = 190:4]
  %tmp.121 = fptrunc double %tmp.120 to float, !dbg !278 ; [#uses=1 type=float] [debug line = 190:4]
  store float %tmp.121, float* %Q.addr.6, align 4, !dbg !278 ; [debug line = 190:4]
  %rend25 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str15, i64 0, i64 0), i32 %rbegin9) nounwind, !dbg !308 ; [#uses=0 type=i32] [debug line = 191:6]
  %j.4 = add i32 %j7, 1, !dbg !309                ; [#uses=1 type=i32] [debug line = 185:59]
  call void @llvm.dbg.value(metadata !{i32 %j.4}, i64 0, metadata !310), !dbg !309 ; [debug line = 185:59] [debug variable = j]
  br label %18, !dbg !309                         ; [debug line = 185:59]

; <label>:22                                      ; preds = %18
  %rend27 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str14, i64 0, i64 0), i32 %rbegin7) nounwind, !dbg !311 ; [#uses=0 type=i32] [debug line = 192:5]
  %i.13 = add i32 %i6, 1, !dbg !312               ; [#uses=1 type=i32] [debug line = 183:62]
  call void @llvm.dbg.value(metadata !{i32 %i.13}, i64 0, metadata !313), !dbg !312 ; [debug line = 183:62] [debug variable = i]
  br label %.preheader, !dbg !312                 ; [debug line = 183:62]

; <label>:23                                      ; preds = %.preheader
  call fastcc void @copyBV(float* %pX), !dbg !314 ; [debug line = 211:5]
  %index = call fastcc i32 @getMinKLApprox(), !dbg !315 ; [#uses=1 type=i32] [debug line = 213:23]
  call void @llvm.dbg.value(metadata !{i32 %index}, i64 0, metadata !316), !dbg !315 ; [debug line = 213:23] [debug variable = index]
  call fastcc void @deleteBV(i32 %index), !dbg !317 ; [debug line = 214:2]
  ret void, !dbg !318                             ; [debug line = 215:1]
}

; [#uses=2]
define internal fastcc void @copyBV(float* %pX) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pX}, i64 0, metadata !319), !dbg !320 ; [debug line = 217:25] [debug variable = pX]
  call void @llvm.dbg.value(metadata !2, i64 0, metadata !321), !dbg !322 ; [debug line = 217:46] [debug variable = pIndex]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX, i32 13) nounwind, !dbg !323 ; [debug line = 217:55]
  br label %1, !dbg !325                          ; [debug line = 218:33]

; <label>:1                                       ; preds = %2, %0
  %i = phi i32 [ 0, %0 ], [ %i.14, %2 ]           ; [#uses=4 type=i32]
  %exitcond = icmp eq i32 %i, 13, !dbg !325       ; [#uses=1 type=i1] [debug line = 218:33]
  br i1 %exitcond, label %3, label %2, !dbg !325  ; [debug line = 218:33]

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([8 x i8]* @.str16, i64 0, i64 0)) nounwind, !dbg !327 ; [debug line = 218:49]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([8 x i8]* @.str16, i64 0, i64 0)) nounwind, !dbg !327 ; [#uses=1 type=i32] [debug line = 218:49]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !329 ; [debug line = 219:1]
  %tmp = zext i32 %i to i64, !dbg !330            ; [#uses=1 type=i64] [debug line = 220:2]
  %pX.addr = getelementptr inbounds float* %pX, i64 %tmp, !dbg !330 ; [#uses=1 type=float*] [debug line = 220:2]
  %pX.load = load float* %pX.addr, align 4, !dbg !330 ; [#uses=2 type=float] [debug line = 220:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pX.load) nounwind
  %bvCnt.load = load i32* @bvCnt, align 4, !dbg !330 ; [#uses=1 type=i32] [debug line = 220:2]
  %tmp.123 = mul i32 %bvCnt.load, 13, !dbg !330   ; [#uses=1 type=i32] [debug line = 220:2]
  %tmp.124 = add i32 %tmp.123, %i, !dbg !330      ; [#uses=1 type=i32] [debug line = 220:2]
  %tmp.125 = zext i32 %tmp.124 to i64, !dbg !330  ; [#uses=1 type=i64] [debug line = 220:2]
  %basisVectors.addr = getelementptr inbounds [1313 x float]* @basisVectors, i64 0, i64 %tmp.125, !dbg !330 ; [#uses=1 type=float*] [debug line = 220:2]
  store float %pX.load, float* %basisVectors.addr, align 4, !dbg !330 ; [debug line = 220:2]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([8 x i8]* @.str16, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !331 ; [#uses=0 type=i32] [debug line = 221:2]
  %i.14 = add i32 %i, 1, !dbg !332                ; [#uses=1 type=i32] [debug line = 218:43]
  call void @llvm.dbg.value(metadata !{i32 %i.14}, i64 0, metadata !333), !dbg !332 ; [debug line = 218:43] [debug variable = i]
  br label %1, !dbg !332                          ; [debug line = 218:43]

; <label>:3                                       ; preds = %1
  ret void, !dbg !334                             ; [debug line = 222:1]
}

; [#uses=0]
define float @projection_gp(float* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pInit) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !335
  call void (...)* @_ssdm_op_SpecTopModule([14 x i8]* @projection_gp.str) nounwind
  call void @llvm.dbg.value(metadata !{float* %pX}, i64 0, metadata !341), !dbg !342 ; [debug line = 224:33] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !343), !dbg !344 ; [debug line = 224:53] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{i1 %pPredict}, i64 0, metadata !345), !dbg !346 ; [debug line = 224:68] [debug variable = pPredict]
  call void @llvm.dbg.value(metadata !{i1 %pInit}, i64 0, metadata !347), !dbg !348 ; [debug line = 224:89] [debug variable = pInit]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX, i32 13) nounwind, !dbg !349 ; [debug line = 224:97]
  call void (...)* @_ssdm_op_SpecInterface(float* %pX, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 13, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !351 ; [debug line = 225:1]
  %tmp = fpext float %pY to double, !dbg !352     ; [#uses=1 type=double] [debug line = 226:1]
  call void (...)* @_ssdm_op_SpecInterface(double %tmp, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !352 ; [debug line = 226:1]
  %tmp.127 = zext i1 %pPredict to i32, !dbg !353  ; [#uses=1 type=i32] [debug line = 227:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 %tmp.127, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !353 ; [debug line = 227:1]
  %tmp.128 = zext i1 %pInit to i32, !dbg !354     ; [#uses=1 type=i32] [debug line = 228:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 %tmp.128, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !354 ; [debug line = 228:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 0, i8* getelementptr inbounds ([10 x i8]* @.str17, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !355 ; [debug line = 229:1]
  br i1 %pInit, label %.preheader2.preheader, label %.loopexit3, !dbg !356 ; [debug line = 231:2]

.preheader2.preheader:                            ; preds = %0
  br label %.preheader2, !dbg !357                ; [debug line = 232:31]

.preheader2:                                      ; preds = %1, %.preheader2.preheader
  %i = phi i32 [ %i.15, %1 ], [ 0, %.preheader2.preheader ] ; [#uses=3 type=i32]
  %exitcond1 = icmp eq i32 %i, 101, !dbg !357     ; [#uses=1 type=i1] [debug line = 232:31]
  br i1 %exitcond1, label %.loopexit3.loopexit, label %1, !dbg !357 ; [debug line = 232:31]

; <label>:1                                       ; preds = %.preheader2
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([5 x i8]* @.str18, i64 0, i64 0)) nounwind, !dbg !360 ; [debug line = 232:59]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([5 x i8]* @.str18, i64 0, i64 0)) nounwind, !dbg !360 ; [#uses=1 type=i32] [debug line = 232:59]
  %tmp.129 = mul i32 %i, 14, !dbg !362            ; [#uses=1 type=i32] [debug line = 233:4]
  %tmp.130 = zext i32 %tmp.129 to i64, !dbg !362  ; [#uses=1 type=i64] [debug line = 233:4]
  %Q.addr = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp.130, !dbg !362 ; [#uses=1 type=float*] [debug line = 233:4]
  store float 1.000000e+00, float* %Q.addr, align 8, !dbg !362 ; [debug line = 233:4]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([5 x i8]* @.str18, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !363 ; [#uses=0 type=i32] [debug line = 234:3]
  %i.15 = add i32 %i, 1, !dbg !364                ; [#uses=1 type=i32] [debug line = 232:53]
  call void @llvm.dbg.value(metadata !{i32 %i.15}, i64 0, metadata !365), !dbg !364 ; [debug line = 232:53] [debug variable = i]
  br label %.preheader2, !dbg !364                ; [debug line = 232:53]

.loopexit3.loopexit:                              ; preds = %.preheader2
  br label %.loopexit3

.loopexit3:                                       ; preds = %.loopexit3.loopexit, %0
  br i1 %pPredict, label %.preheader.preheader, label %2, !dbg !366 ; [debug line = 237:2]

.preheader.preheader:                             ; preds = %.loopexit3
  br label %.preheader, !dbg !367                 ; [debug line = 247:37]

; <label>:2                                       ; preds = %.loopexit3
  %bvCnt.load = load i32* @bvCnt, align 4, !dbg !370 ; [#uses=1 type=i32] [debug line = 238:3]
  %tmp.132 = icmp eq i32 %bvCnt.load, 100, !dbg !370 ; [#uses=1 type=i1] [debug line = 238:3]
  br i1 %tmp.132, label %3, label %4, !dbg !370   ; [debug line = 238:3]

; <label>:3                                       ; preds = %2
  call fastcc void @train_full_bv_set(float* %pX, float %pY), !dbg !372 ; [debug line = 239:4]
  br label %5, !dbg !374                          ; [debug line = 240:3]

; <label>:4                                       ; preds = %2
  call fastcc void @copyBV(float* %pX), !dbg !375 ; [debug line = 241:4]
  %bvCnt.load.1 = load i32* @bvCnt, align 4, !dbg !377 ; [#uses=1 type=i32] [debug line = 242:4]
  %tmp.133 = add i32 %bvCnt.load.1, 1, !dbg !377  ; [#uses=1 type=i32] [debug line = 242:4]
  store i32 %tmp.133, i32* @bvCnt, align 4, !dbg !377 ; [debug line = 242:4]
  br label %5

; <label>:5                                       ; preds = %4, %3
  br label %.loopexit, !dbg !378                  ; [debug line = 244:3]

.preheader:                                       ; preds = %6, %.preheader.preheader
  %sum = phi float [ %sum.2, %6 ], [ 0x402B4EBEE0000000, %.preheader.preheader ] ; [#uses=2 type=float]
  %i1 = phi i32 [ %i.16, %6 ], [ 0, %.preheader.preheader ] ; [#uses=4 type=i32]
  %exitcond = icmp eq i32 %i1, 100, !dbg !367     ; [#uses=1 type=i1] [debug line = 247:37]
  br i1 %exitcond, label %.loopexit.loopexit, label %6, !dbg !367 ; [debug line = 247:37]

; <label>:6                                       ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([11 x i8]* @.str19, i64 0, i64 0)) nounwind, !dbg !379 ; [debug line = 247:61]
  %rbegin4 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([11 x i8]* @.str19, i64 0, i64 0)) nounwind, !dbg !379 ; [#uses=1 type=i32] [debug line = 247:61]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !381 ; [debug line = 248:1]
  %tmp.134 = mul i32 %i1, 13, !dbg !382           ; [#uses=1 type=i32] [debug line = 249:9]
  %tmp.135 = zext i32 %tmp.134 to i64, !dbg !382  ; [#uses=1 type=i64] [debug line = 249:9]
  %basisVectors.addr = getelementptr inbounds [1313 x float]* @basisVectors, i64 0, i64 %tmp.135, !dbg !382 ; [#uses=1 type=float*] [debug line = 249:9]
  %tmp.136 = call fastcc float @K(float* %basisVectors.addr, float* %pX), !dbg !382 ; [#uses=1 type=float] [debug line = 249:9]
  %tmp.137 = zext i32 %i1 to i64, !dbg !382       ; [#uses=1 type=i64] [debug line = 249:9]
  %alpha.addr = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp.137, !dbg !382 ; [#uses=1 type=float*] [debug line = 249:9]
  %alpha.load = load float* %alpha.addr, align 4, !dbg !382 ; [#uses=2 type=float] [debug line = 249:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %alpha.load) nounwind
  %tmp.138 = fmul float %tmp.136, %alpha.load, !dbg !382 ; [#uses=1 type=float] [debug line = 249:9]
  %sum.2 = fadd float %sum, %tmp.138, !dbg !382   ; [#uses=1 type=float] [debug line = 249:9]
  call void @llvm.dbg.value(metadata !{float %sum.2}, i64 0, metadata !383), !dbg !382 ; [debug line = 249:9] [debug variable = sum]
  %rend5 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([11 x i8]* @.str19, i64 0, i64 0), i32 %rbegin4) nounwind, !dbg !384 ; [#uses=0 type=i32] [debug line = 250:3]
  %i.16 = add i32 %i1, 1, !dbg !385               ; [#uses=1 type=i32] [debug line = 247:55]
  call void @llvm.dbg.value(metadata !{i32 %i.16}, i64 0, metadata !386), !dbg !385 ; [debug line = 247:55] [debug variable = i]
  br label %.preheader, !dbg !385                 ; [debug line = 247:55]

.loopexit.loopexit:                               ; preds = %.preheader
  %sum.0.lcssa = phi float [ %sum, %.preheader ]  ; [#uses=1 type=float]
  br label %.loopexit

.loopexit:                                        ; preds = %.loopexit.loopexit, %5
  %.0 = phi float [ 0.000000e+00, %5 ], [ %sum.0.lcssa, %.loopexit.loopexit ] ; [#uses=1 type=float]
  ret float %.0, !dbg !387                        ; [debug line = 254:1]
}

; [#uses=5]
declare void @_ssdm_op_SpecInterface(...) nounwind

; [#uses=67]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=60]
declare void @_ssdm_SpecKeepArrayLoad(...)

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=1]
declare void @_ssdm_op_SpecBitsMap(...)

; [#uses=1]
declare float @llvm.exp.f32(float) nounwind readonly

; [#uses=18]
declare i32 @_ssdm_op_SpecRegionBegin(...)

; [#uses=18]
declare i32 @_ssdm_op_SpecRegionEnd(...)

!llvm.dbg.cu = !{!0}
!hls.encrypted.func = !{}

!0 = metadata !{i32 786449, i32 0, i32 4, metadata !"/home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace/ProjectionGP_DIM_21/solution1/.autopilot/db/ProjectionGP_FULLY_OPTIMIZED.pragma.2.cpp", metadata !"/home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, metadata !1, metadata !1, metadata !3, metadata !41} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !5, metadata !14, metadata !19, metadata !22, metadata !25, metadata !28, metadata !31, metadata !36}
!5 = metadata !{i32 786478, i32 0, metadata !6, metadata !"K", metadata !"K", metadata !"_Z1KPKfS0_", metadata !6, i32 19, metadata !7, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, float (float*, float*)* @K, null, null, metadata !12, i32 19} ; [ DW_TAG_subprogram ]
!6 = metadata !{i32 786473, metadata !"../../../implementation/hls/Projection_GP_DIM_21/include/ProjectionGP_FULLY_OPTIMIZED.cpp", metadata !"/home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace", null} ; [ DW_TAG_file_type ]
!7 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !8, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!8 = metadata !{metadata !9, metadata !10, metadata !10}
!9 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!10 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !11} ; [ DW_TAG_pointer_type ]
!11 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !9} ; [ DW_TAG_const_type ]
!12 = metadata !{metadata !13}
!13 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!14 = metadata !{i32 786478, i32 0, metadata !6, metadata !"swapRowAndColumn", metadata !"swapRowAndColumn", metadata !"_Z16swapRowAndColumnPfjj", metadata !6, i32 30, metadata !15, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !12, i32 30} ; [ DW_TAG_subprogram ]
!15 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !16, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!16 = metadata !{null, metadata !17, metadata !18, metadata !18}
!17 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !9} ; [ DW_TAG_pointer_type ]
!18 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!19 = metadata !{i32 786478, i32 0, metadata !6, metadata !"deleteBV", metadata !"deleteBV", metadata !"_Z8deleteBVj", metadata !6, i32 46, metadata !20, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32)* @deleteBV, null, null, metadata !12, i32 46} ; [ DW_TAG_subprogram ]
!20 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !21, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!21 = metadata !{null, metadata !18}
!22 = metadata !{i32 786478, i32 0, metadata !6, metadata !"getMinKLApprox", metadata !"getMinKLApprox", metadata !"_Z14getMinKLApproxv", metadata !6, i32 109, metadata !23, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @getMinKLApprox, null, null, metadata !12, i32 109} ; [ DW_TAG_subprogram ]
!23 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !24, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!24 = metadata !{metadata !18}
!25 = metadata !{i32 786478, i32 0, metadata !6, metadata !"train_full_bv_set", metadata !"train_full_bv_set", metadata !"_Z17train_full_bv_setPKff", metadata !6, i32 125, metadata !26, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (float*, float)* @train_full_bv_set, null, null, metadata !12, i32 125} ; [ DW_TAG_subprogram ]
!26 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !27, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!27 = metadata !{null, metadata !10, metadata !11}
!28 = metadata !{i32 786478, i32 0, metadata !6, metadata !"copyBV", metadata !"copyBV", metadata !"_Z6copyBVPKfj", metadata !6, i32 217, metadata !29, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !12, i32 217} ; [ DW_TAG_subprogram ]
!29 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !30, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!30 = metadata !{null, metadata !10, metadata !18}
!31 = metadata !{i32 786478, i32 0, metadata !6, metadata !"projection_gp", metadata !"projection_gp", metadata !"_Z13projection_gpPKffbb", metadata !6, i32 224, metadata !32, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, float (float*, float, i1, i1)* @projection_gp, null, null, metadata !12, i32 224} ; [ DW_TAG_subprogram ]
!32 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !33, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!33 = metadata !{metadata !9, metadata !10, metadata !11, metadata !34, metadata !34}
!34 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !35} ; [ DW_TAG_const_type ]
!35 = metadata !{i32 786468, null, metadata !"bool", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ]
!36 = metadata !{i32 786478, i32 0, metadata !37, metadata !"exp", metadata !"exp", metadata !"_ZSt3expf", metadata !38, i32 216, metadata !39, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, float (float)* @"std::exp", null, null, metadata !12, i32 217} ; [ DW_TAG_subprogram ]
!37 = metadata !{i32 786489, null, metadata !"std", metadata !38, i32 76} ; [ DW_TAG_namespace ]
!38 = metadata !{i32 786473, metadata !"/opt/Xilinx/Vivado_HLS/2015.2/lnx64/tools/gcc/lib/gcc/x86_64-unknown-linux-gnu/4.6.3/../../../../include/c++/4.6.3/cmath", metadata !"/home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace", null} ; [ DW_TAG_file_type ]
!39 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !40, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!40 = metadata !{metadata !9, metadata !9}
!41 = metadata !{metadata !42}
!42 = metadata !{metadata !43, metadata !47, metadata !48, metadata !52, metadata !56, metadata !57, metadata !58, metadata !62, metadata !63}
!43 = metadata !{i32 786484, i32 0, null, metadata !"C", metadata !"C", metadata !"", metadata !6, i32 10, metadata !44, i32 0, i32 1, [10201 x float]* @C} ; [ DW_TAG_variable ]
!44 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 326432, i64 32, i32 0, i32 0, metadata !9, metadata !45, i32 0, i32 0} ; [ DW_TAG_array_type ]
!45 = metadata !{metadata !46}
!46 = metadata !{i32 786465, i64 0, i64 10200}    ; [ DW_TAG_subrange_type ]
!47 = metadata !{i32 786484, i32 0, null, metadata !"Q", metadata !"Q", metadata !"", metadata !6, i32 11, metadata !44, i32 0, i32 1, [10201 x float]* @Q} ; [ DW_TAG_variable ]
!48 = metadata !{i32 786484, i32 0, null, metadata !"e", metadata !"e", metadata !"", metadata !6, i32 12, metadata !49, i32 0, i32 1, [101 x float]* @e} ; [ DW_TAG_variable ]
!49 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 3232, i64 32, i32 0, i32 0, metadata !9, metadata !50, i32 0, i32 0} ; [ DW_TAG_array_type ]
!50 = metadata !{metadata !51}
!51 = metadata !{i32 786465, i64 0, i64 100}      ; [ DW_TAG_subrange_type ]
!52 = metadata !{i32 786484, i32 0, null, metadata !"k", metadata !"k", metadata !"", metadata !6, i32 13, metadata !53, i32 0, i32 1, [100 x float]* @k} ; [ DW_TAG_variable ]
!53 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 3200, i64 32, i32 0, i32 0, metadata !9, metadata !54, i32 0, i32 0} ; [ DW_TAG_array_type ]
!54 = metadata !{metadata !55}
!55 = metadata !{i32 786465, i64 0, i64 99}       ; [ DW_TAG_subrange_type ]
!56 = metadata !{i32 786484, i32 0, null, metadata !"s", metadata !"s", metadata !"", metadata !6, i32 14, metadata !49, i32 0, i32 1, [101 x float]* @s} ; [ DW_TAG_variable ]
!57 = metadata !{i32 786484, i32 0, null, metadata !"alpha", metadata !"alpha", metadata !"", metadata !6, i32 15, metadata !49, i32 0, i32 1, [101 x float]* @alpha} ; [ DW_TAG_variable ]
!58 = metadata !{i32 786484, i32 0, null, metadata !"basisVectors", metadata !"basisVectors", metadata !"", metadata !6, i32 16, metadata !59, i32 0, i32 1, [1313 x float]* @basisVectors} ; [ DW_TAG_variable ]
!59 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 42016, i64 32, i32 0, i32 0, metadata !9, metadata !60, i32 0, i32 0} ; [ DW_TAG_array_type ]
!60 = metadata !{metadata !61}
!61 = metadata !{i32 786465, i64 0, i64 1312}     ; [ DW_TAG_subrange_type ]
!62 = metadata !{i32 786484, i32 0, null, metadata !"bvCnt", metadata !"bvCnt", metadata !"", metadata !6, i32 17, metadata !18, i32 0, i32 1, i32* @bvCnt} ; [ DW_TAG_variable ]
!63 = metadata !{i32 786484, i32 0, null, metadata !"signgam", metadata !"signgam", metadata !"", metadata !64, i32 148, metadata !65, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!64 = metadata !{i32 786473, metadata !"/usr/include/math.h", metadata !"/home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace", null} ; [ DW_TAG_file_type ]
!65 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!66 = metadata !{i32 786689, metadata !5, metadata !"pX1", metadata !6, i32 16777235, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!67 = metadata !{i32 19, i32 21, metadata !5, null}
!68 = metadata !{i32 786689, metadata !5, metadata !"pX2", metadata !6, i32 33554451, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!69 = metadata !{i32 19, i32 42, metadata !5, null}
!70 = metadata !{i32 19, i32 52, metadata !71, null}
!71 = metadata !{i32 786443, metadata !5, i32 19, i32 51, metadata !6, i32 0} ; [ DW_TAG_lexical_block ]
!72 = metadata !{i32 19, i32 83, metadata !71, null}
!73 = metadata !{i32 21, i32 28, metadata !74, null}
!74 = metadata !{i32 786443, metadata !71, i32 21, i32 5, metadata !6, i32 1} ; [ DW_TAG_lexical_block ]
!75 = metadata !{i32 21, i32 44, metadata !76, null}
!76 = metadata !{i32 786443, metadata !74, i32 21, i32 43, metadata !6, i32 2} ; [ DW_TAG_lexical_block ]
!77 = metadata !{i32 22, i32 1, metadata !76, null}
!78 = metadata !{i32 23, i32 31, metadata !76, null}
!79 = metadata !{i32 786688, metadata !76, metadata !"val", metadata !6, i32 23, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!80 = metadata !{i32 24, i32 9, metadata !76, null}
!81 = metadata !{i32 786688, metadata !71, metadata !"sum", metadata !6, i32 20, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!82 = metadata !{i32 25, i32 5, metadata !76, null}
!83 = metadata !{i32 21, i32 38, metadata !74, null}
!84 = metadata !{i32 786688, metadata !74, metadata !"i", metadata !6, i32 21, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!85 = metadata !{i32 27, i32 12, metadata !71, null}
!86 = metadata !{i32 786689, metadata !36, metadata !"__x", metadata !38, i32 16777432, metadata !9, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!87 = metadata !{i32 216, i32 13, metadata !36, null}
!88 = metadata !{i32 217, i32 12, metadata !89, null}
!89 = metadata !{i32 786443, metadata !36, i32 217, i32 3, metadata !38, i32 55} ; [ DW_TAG_lexical_block ]
!90 = metadata !{i32 786689, metadata !14, metadata !"pM", metadata !6, i32 16777246, metadata !17, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!91 = metadata !{i32 30, i32 29, metadata !14, null}
!92 = metadata !{i32 786689, metadata !14, metadata !"rowA", metadata !6, i32 33554462, metadata !18, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!93 = metadata !{i32 30, i32 79, metadata !14, null}
!94 = metadata !{i32 30, i32 106, metadata !95, null}
!95 = metadata !{i32 786443, metadata !14, i32 30, i32 105, metadata !6, i32 3} ; [ DW_TAG_lexical_block ]
!96 = metadata !{i32 33, i32 47, metadata !97, null}
!97 = metadata !{i32 786443, metadata !98, i32 31, i32 55, metadata !6, i32 5} ; [ DW_TAG_lexical_block ]
!98 = metadata !{i32 786443, metadata !95, i32 31, i32 5, metadata !6, i32 4} ; [ DW_TAG_lexical_block ]
!99 = metadata !{i32 31, i32 28, metadata !98, null}
!100 = metadata !{i32 38, i32 28, metadata !101, null}
!101 = metadata !{i32 786443, metadata !95, i32 38, i32 5, metadata !6, i32 6} ; [ DW_TAG_lexical_block ]
!102 = metadata !{i32 786688, metadata !97, metadata !"temp", metadata !6, i32 33, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!103 = metadata !{i32 34, i32 9, metadata !97, null}
!104 = metadata !{i32 35, i32 9, metadata !97, null}
!105 = metadata !{i32 31, i32 50, metadata !98, null}
!106 = metadata !{i32 786688, metadata !98, metadata !"i", metadata !6, i32 31, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!107 = metadata !{i32 40, i32 47, metadata !108, null}
!108 = metadata !{i32 786443, metadata !101, i32 38, i32 55, metadata !6, i32 7} ; [ DW_TAG_lexical_block ]
!109 = metadata !{i32 786688, metadata !108, metadata !"temp", metadata !6, i32 40, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!110 = metadata !{i32 41, i32 9, metadata !108, null}
!111 = metadata !{i32 42, i32 9, metadata !108, null}
!112 = metadata !{i32 38, i32 50, metadata !101, null}
!113 = metadata !{i32 786688, metadata !101, metadata !"i", metadata !6, i32 38, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!114 = metadata !{i32 44, i32 1, metadata !95, null}
!115 = metadata !{i32 786689, metadata !19, metadata !"pIndex", metadata !6, i32 16777262, metadata !18, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!116 = metadata !{i32 46, i32 29, metadata !19, null}
!117 = metadata !{i32 50, i32 31, metadata !118, null}
!118 = metadata !{i32 786443, metadata !19, i32 46, i32 37, metadata !6, i32 8} ; [ DW_TAG_lexical_block ]
!119 = metadata !{i32 786688, metadata !118, metadata !"temp", metadata !6, i32 50, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!120 = metadata !{i32 51, i32 5, metadata !118, null}
!121 = metadata !{i32 52, i32 5, metadata !118, null}
!122 = metadata !{i32 54, i32 5, metadata !118, null}
!123 = metadata !{i32 55, i32 5, metadata !118, null}
!124 = metadata !{i32 59, i32 2, metadata !125, null}
!125 = metadata !{i32 786443, metadata !126, i32 57, i32 63, metadata !6, i32 10} ; [ DW_TAG_lexical_block ]
!126 = metadata !{i32 786443, metadata !118, i32 57, i32 25, metadata !6, i32 9} ; [ DW_TAG_lexical_block ]
!127 = metadata !{i32 57, i32 48, metadata !126, null}
!128 = metadata !{i32 57, i32 64, metadata !125, null}
!129 = metadata !{i32 58, i32 1, metadata !125, null}
!130 = metadata !{i32 60, i32 9, metadata !125, null}
!131 = metadata !{i32 61, i32 9, metadata !125, null}
!132 = metadata !{i32 62, i32 5, metadata !125, null}
!133 = metadata !{i32 57, i32 58, metadata !126, null}
!134 = metadata !{i32 786688, metadata !126, metadata !"i", metadata !6, i32 57, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!135 = metadata !{i32 65, i32 40, metadata !118, null}
!136 = metadata !{i32 786688, metadata !118, metadata !"alphaStar", metadata !6, i32 65, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!137 = metadata !{i32 66, i32 24, metadata !118, null}
!138 = metadata !{i32 786688, metadata !118, metadata !"cStar", metadata !6, i32 66, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!139 = metadata !{i32 84, i32 45, metadata !140, null}
!140 = metadata !{i32 786443, metadata !141, i32 79, i32 70, metadata !6, i32 16} ; [ DW_TAG_lexical_block ]
!141 = metadata !{i32 786443, metadata !142, i32 79, i32 24, metadata !6, i32 15} ; [ DW_TAG_lexical_block ]
!142 = metadata !{i32 786443, metadata !143, i32 77, i32 68, metadata !6, i32 14} ; [ DW_TAG_lexical_block ]
!143 = metadata !{i32 786443, metadata !118, i32 77, i32 23, metadata !6, i32 13} ; [ DW_TAG_lexical_block ]
!144 = metadata !{i32 786688, metadata !118, metadata !"qStar", metadata !6, i32 67, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!145 = metadata !{i32 67, i32 24, metadata !118, null}
!146 = metadata !{i32 68, i32 5, metadata !118, null}
!147 = metadata !{i32 70, i32 44, metadata !148, null}
!148 = metadata !{i32 786443, metadata !118, i32 70, i32 21, metadata !6, i32 11} ; [ DW_TAG_lexical_block ]
!149 = metadata !{i32 70, i32 68, metadata !150, null}
!150 = metadata !{i32 786443, metadata !148, i32 70, i32 67, metadata !6, i32 12} ; [ DW_TAG_lexical_block ]
!151 = metadata !{i32 71, i32 1, metadata !150, null}
!152 = metadata !{i32 72, i32 46, metadata !150, null}
!153 = metadata !{i32 786688, metadata !150, metadata !"a", metadata !6, i32 72, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!154 = metadata !{i32 73, i32 9, metadata !150, null}
!155 = metadata !{i32 74, i32 5, metadata !150, null}
!156 = metadata !{i32 70, i32 62, metadata !148, null}
!157 = metadata !{i32 786688, metadata !148, metadata !"i", metadata !6, i32 70, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!158 = metadata !{i32 75, i32 5, metadata !118, null}
!159 = metadata !{i32 77, i32 45, metadata !143, null}
!160 = metadata !{i32 97, i32 48, metadata !161, null}
!161 = metadata !{i32 786443, metadata !118, i32 97, i32 25, metadata !6, i32 17} ; [ DW_TAG_lexical_block ]
!162 = metadata !{i32 77, i32 69, metadata !142, null}
!163 = metadata !{i32 81, i32 46, metadata !140, null}
!164 = metadata !{i32 83, i32 45, metadata !140, null}
!165 = metadata !{i32 87, i32 13, metadata !140, null}
!166 = metadata !{i32 79, i32 47, metadata !141, null}
!167 = metadata !{i32 79, i32 71, metadata !140, null}
!168 = metadata !{i32 80, i32 1, metadata !140, null}
!169 = metadata !{i32 786688, metadata !140, metadata !"a", metadata !6, i32 81, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!170 = metadata !{i32 82, i32 54, metadata !140, null}
!171 = metadata !{i32 786688, metadata !140, metadata !"b", metadata !6, i32 82, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!172 = metadata !{i32 786688, metadata !140, metadata !"c", metadata !6, i32 83, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!173 = metadata !{i32 786688, metadata !140, metadata !"temp", metadata !6, i32 84, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!174 = metadata !{i32 93, i32 13, metadata !140, null}
!175 = metadata !{i32 94, i32 9, metadata !140, null}
!176 = metadata !{i32 79, i32 65, metadata !141, null}
!177 = metadata !{i32 786688, metadata !141, metadata !"j", metadata !6, i32 79, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!178 = metadata !{i32 95, i32 5, metadata !142, null}
!179 = metadata !{i32 77, i32 63, metadata !143, null}
!180 = metadata !{i32 786688, metadata !143, metadata !"i", metadata !6, i32 77, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!181 = metadata !{i32 97, i32 73, metadata !182, null}
!182 = metadata !{i32 786443, metadata !161, i32 97, i32 72, metadata !6, i32 18} ; [ DW_TAG_lexical_block ]
!183 = metadata !{i32 98, i32 1, metadata !182, null}
!184 = metadata !{i32 99, i32 46, metadata !182, null}
!185 = metadata !{i32 786688, metadata !182, metadata !"a", metadata !6, i32 99, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!186 = metadata !{i32 100, i32 50, metadata !182, null}
!187 = metadata !{i32 786688, metadata !182, metadata !"b", metadata !6, i32 100, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!188 = metadata !{i32 102, i32 6, metadata !182, null}
!189 = metadata !{i32 103, i32 9, metadata !182, null}
!190 = metadata !{i32 104, i32 9, metadata !182, null}
!191 = metadata !{i32 105, i32 9, metadata !182, null}
!192 = metadata !{i32 106, i32 5, metadata !182, null}
!193 = metadata !{i32 97, i32 67, metadata !161, null}
!194 = metadata !{i32 786688, metadata !161, metadata !"i", metadata !6, i32 97, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!195 = metadata !{i32 107, i32 1, metadata !118, null}
!196 = metadata !{i32 111, i32 91, metadata !197, null}
!197 = metadata !{i32 786443, metadata !22, i32 109, i32 31, metadata !6, i32 19} ; [ DW_TAG_lexical_block ]
!198 = metadata !{i32 786688, metadata !197, metadata !"minScore", metadata !6, i32 111, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!199 = metadata !{i32 113, i32 28, metadata !200, null}
!200 = metadata !{i32 786443, metadata !197, i32 113, i32 5, metadata !6, i32 20} ; [ DW_TAG_lexical_block ]
!201 = metadata !{i32 114, i32 93, metadata !202, null}
!202 = metadata !{i32 786443, metadata !200, i32 113, i32 55, metadata !6, i32 21} ; [ DW_TAG_lexical_block ]
!203 = metadata !{i32 786688, metadata !202, metadata !"tScore", metadata !6, i32 114, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!204 = metadata !{i32 116, i32 9, metadata !202, null}
!205 = metadata !{i32 117, i32 13, metadata !206, null}
!206 = metadata !{i32 786443, metadata !202, i32 116, i32 32, metadata !6, i32 22} ; [ DW_TAG_lexical_block ]
!207 = metadata !{i32 786688, metadata !197, metadata !"index", metadata !6, i32 110, metadata !65, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!208 = metadata !{i32 118, i32 13, metadata !206, null}
!209 = metadata !{i32 113, i32 50, metadata !200, null}
!210 = metadata !{i32 786688, metadata !200, metadata !"i", metadata !6, i32 113, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!211 = metadata !{i32 122, i32 5, metadata !197, null}
!212 = metadata !{i32 786689, metadata !25, metadata !"pX", metadata !6, i32 16777341, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!213 = metadata !{i32 125, i32 37, metadata !25, null}
!214 = metadata !{i32 786689, metadata !25, metadata !"pY", metadata !6, i32 33554557, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!215 = metadata !{i32 125, i32 57, metadata !25, null}
!216 = metadata !{i32 125, i32 62, metadata !217, null}
!217 = metadata !{i32 786443, metadata !25, i32 125, i32 61, metadata !6, i32 23} ; [ DW_TAG_lexical_block ]
!218 = metadata !{i32 129, i32 35, metadata !219, null}
!219 = metadata !{i32 786443, metadata !217, i32 129, i32 12, metadata !6, i32 24} ; [ DW_TAG_lexical_block ]
!220 = metadata !{i32 140, i32 45, metadata !221, null}
!221 = metadata !{i32 786443, metadata !217, i32 140, i32 22, metadata !6, i32 26} ; [ DW_TAG_lexical_block ]
!222 = metadata !{i32 129, i32 59, metadata !223, null}
!223 = metadata !{i32 786443, metadata !219, i32 129, i32 58, metadata !6, i32 25} ; [ DW_TAG_lexical_block ]
!224 = metadata !{i32 130, i32 1, metadata !223, null}
!225 = metadata !{i32 131, i32 9, metadata !223, null}
!226 = metadata !{i32 132, i32 9, metadata !223, null}
!227 = metadata !{i32 786688, metadata !217, metadata !"m", metadata !6, i32 126, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!228 = metadata !{i32 133, i32 5, metadata !223, null}
!229 = metadata !{i32 129, i32 53, metadata !219, null}
!230 = metadata !{i32 786688, metadata !219, metadata !"i", metadata !6, i32 129, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!231 = metadata !{i32 151, i32 36, metadata !232, null}
!232 = metadata !{i32 786443, metadata !217, i32 151, i32 13, metadata !6, i32 30} ; [ DW_TAG_lexical_block ]
!233 = metadata !{i32 140, i32 69, metadata !234, null}
!234 = metadata !{i32 786443, metadata !221, i32 140, i32 68, metadata !6, i32 27} ; [ DW_TAG_lexical_block ]
!235 = metadata !{i32 141, i32 1, metadata !234, null}
!236 = metadata !{i32 142, i32 2, metadata !234, null}
!237 = metadata !{i32 143, i32 6, metadata !234, null}
!238 = metadata !{i32 146, i32 2, metadata !239, null}
!239 = metadata !{i32 786443, metadata !240, i32 144, i32 72, metadata !6, i32 29} ; [ DW_TAG_lexical_block ]
!240 = metadata !{i32 786443, metadata !234, i32 144, i32 26, metadata !6, i32 28} ; [ DW_TAG_lexical_block ]
!241 = metadata !{i32 144, i32 49, metadata !240, null}
!242 = metadata !{i32 144, i32 73, metadata !239, null}
!243 = metadata !{i32 145, i32 1, metadata !239, null}
!244 = metadata !{i32 147, i32 10, metadata !239, null}
!245 = metadata !{i32 148, i32 9, metadata !239, null}
!246 = metadata !{i32 144, i32 67, metadata !240, null}
!247 = metadata !{i32 786688, metadata !240, metadata !"j", metadata !6, i32 144, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!248 = metadata !{i32 149, i32 5, metadata !234, null}
!249 = metadata !{i32 140, i32 63, metadata !221, null}
!250 = metadata !{i32 786688, metadata !221, metadata !"i", metadata !6, i32 140, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!251 = metadata !{i32 151, i32 60, metadata !252, null}
!252 = metadata !{i32 786443, metadata !232, i32 151, i32 59, metadata !6, i32 31} ; [ DW_TAG_lexical_block ]
!253 = metadata !{i32 152, i32 1, metadata !252, null}
!254 = metadata !{i32 153, i32 2, metadata !252, null}
!255 = metadata !{i32 786688, metadata !217, metadata !"sigma2", metadata !6, i32 139, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!256 = metadata !{i32 154, i32 5, metadata !252, null}
!257 = metadata !{i32 151, i32 54, metadata !232, null}
!258 = metadata !{i32 786688, metadata !232, metadata !"i", metadata !6, i32 151, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!259 = metadata !{i32 155, i32 5, metadata !217, null}
!260 = metadata !{i32 157, i32 40, metadata !217, null}
!261 = metadata !{i32 786688, metadata !217, metadata !"q", metadata !6, i32 157, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!262 = metadata !{i32 158, i32 35, metadata !217, null}
!263 = metadata !{i32 786688, metadata !217, metadata !"r", metadata !6, i32 158, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!264 = metadata !{i32 161, i32 42, metadata !265, null}
!265 = metadata !{i32 786443, metadata !217, i32 161, i32 19, metadata !6, i32 32} ; [ DW_TAG_lexical_block ]
!266 = metadata !{i32 161, i32 66, metadata !267, null}
!267 = metadata !{i32 786443, metadata !265, i32 161, i32 65, metadata !6, i32 33} ; [ DW_TAG_lexical_block ]
!268 = metadata !{i32 162, i32 1, metadata !267, null}
!269 = metadata !{i32 163, i32 2, metadata !267, null}
!270 = metadata !{i32 786688, metadata !217, metadata !"gamma", metadata !6, i32 159, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!271 = metadata !{i32 164, i32 6, metadata !267, null}
!272 = metadata !{i32 165, i32 5, metadata !267, null}
!273 = metadata !{i32 161, i32 60, metadata !265, null}
!274 = metadata !{i32 786688, metadata !265, metadata !"i", metadata !6, i32 161, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!275 = metadata !{i32 171, i32 5, metadata !217, null}
!276 = metadata !{i32 174, i32 43, metadata !277, null}
!277 = metadata !{i32 786443, metadata !217, i32 174, i32 20, metadata !6, i32 34} ; [ DW_TAG_lexical_block ]
!278 = metadata !{i32 190, i32 4, metadata !279, null}
!279 = metadata !{i32 786443, metadata !280, i32 185, i32 64, metadata !6, i32 41} ; [ DW_TAG_lexical_block ]
!280 = metadata !{i32 786443, metadata !281, i32 185, i32 17, metadata !6, i32 40} ; [ DW_TAG_lexical_block ]
!281 = metadata !{i32 786443, metadata !282, i32 183, i32 67, metadata !6, i32 39} ; [ DW_TAG_lexical_block ]
!282 = metadata !{i32 786443, metadata !217, i32 183, i32 20, metadata !6, i32 38} ; [ DW_TAG_lexical_block ]
!283 = metadata !{i32 183, i32 43, metadata !282, null}
!284 = metadata !{i32 174, i32 68, metadata !285, null}
!285 = metadata !{i32 786443, metadata !277, i32 174, i32 67, metadata !6, i32 35} ; [ DW_TAG_lexical_block ]
!286 = metadata !{i32 175, i32 1, metadata !285, null}
!287 = metadata !{i32 179, i32 13, metadata !288, null}
!288 = metadata !{i32 786443, metadata !289, i32 176, i32 64, metadata !6, i32 37} ; [ DW_TAG_lexical_block ]
!289 = metadata !{i32 786443, metadata !285, i32 176, i32 17, metadata !6, i32 36} ; [ DW_TAG_lexical_block ]
!290 = metadata !{i32 176, i32 40, metadata !289, null}
!291 = metadata !{i32 176, i32 65, metadata !288, null}
!292 = metadata !{i32 177, i32 1, metadata !288, null}
!293 = metadata !{i32 180, i32 9, metadata !288, null}
!294 = metadata !{i32 176, i32 59, metadata !289, null}
!295 = metadata !{i32 786688, metadata !289, metadata !"j", metadata !6, i32 176, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!296 = metadata !{i32 181, i32 5, metadata !285, null}
!297 = metadata !{i32 174, i32 62, metadata !277, null}
!298 = metadata !{i32 786688, metadata !277, metadata !"i", metadata !6, i32 174, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!299 = metadata !{i32 183, i32 68, metadata !281, null}
!300 = metadata !{i32 184, i32 1, metadata !281, null}
!301 = metadata !{i32 187, i32 40, metadata !279, null}
!302 = metadata !{i32 185, i32 40, metadata !280, null}
!303 = metadata !{i32 185, i32 65, metadata !279, null}
!304 = metadata !{i32 186, i32 1, metadata !279, null}
!305 = metadata !{i32 786688, metadata !279, metadata !"ti", metadata !6, i32 187, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!306 = metadata !{i32 188, i32 42, metadata !279, null}
!307 = metadata !{i32 786688, metadata !279, metadata !"tj", metadata !6, i32 188, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!308 = metadata !{i32 191, i32 6, metadata !279, null}
!309 = metadata !{i32 185, i32 59, metadata !280, null}
!310 = metadata !{i32 786688, metadata !280, metadata !"j", metadata !6, i32 185, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!311 = metadata !{i32 192, i32 5, metadata !281, null}
!312 = metadata !{i32 183, i32 62, metadata !282, null}
!313 = metadata !{i32 786688, metadata !282, metadata !"i", metadata !6, i32 183, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!314 = metadata !{i32 211, i32 5, metadata !217, null}
!315 = metadata !{i32 213, i32 23, metadata !217, null}
!316 = metadata !{i32 786688, metadata !217, metadata !"index", metadata !6, i32 213, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!317 = metadata !{i32 214, i32 2, metadata !217, null}
!318 = metadata !{i32 215, i32 1, metadata !217, null}
!319 = metadata !{i32 786689, metadata !28, metadata !"pX", metadata !6, i32 16777433, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!320 = metadata !{i32 217, i32 25, metadata !28, null}
!321 = metadata !{i32 786689, metadata !28, metadata !"pIndex", metadata !6, i32 33554649, metadata !18, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!322 = metadata !{i32 217, i32 46, metadata !28, null}
!323 = metadata !{i32 217, i32 55, metadata !324, null}
!324 = metadata !{i32 786443, metadata !28, i32 217, i32 54, metadata !6, i32 42} ; [ DW_TAG_lexical_block ]
!325 = metadata !{i32 218, i32 33, metadata !326, null}
!326 = metadata !{i32 786443, metadata !324, i32 218, i32 10, metadata !6, i32 43} ; [ DW_TAG_lexical_block ]
!327 = metadata !{i32 218, i32 49, metadata !328, null}
!328 = metadata !{i32 786443, metadata !326, i32 218, i32 48, metadata !6, i32 44} ; [ DW_TAG_lexical_block ]
!329 = metadata !{i32 219, i32 1, metadata !328, null}
!330 = metadata !{i32 220, i32 2, metadata !328, null}
!331 = metadata !{i32 221, i32 2, metadata !328, null}
!332 = metadata !{i32 218, i32 43, metadata !326, null}
!333 = metadata !{i32 786688, metadata !326, metadata !"i", metadata !6, i32 218, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!334 = metadata !{i32 222, i32 1, metadata !324, null}
!335 = metadata !{metadata !336}
!336 = metadata !{i32 0, i32 31, metadata !337}
!337 = metadata !{metadata !338}
!338 = metadata !{metadata !"return", metadata !339, metadata !"float", i32 0, i32 31}
!339 = metadata !{metadata !340}
!340 = metadata !{i32 0, i32 1, i32 0}
!341 = metadata !{i32 786689, metadata !31, metadata !"pX", metadata !6, i32 16777440, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!342 = metadata !{i32 224, i32 33, metadata !31, null}
!343 = metadata !{i32 786689, metadata !31, metadata !"pY", metadata !6, i32 33554656, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!344 = metadata !{i32 224, i32 53, metadata !31, null}
!345 = metadata !{i32 786689, metadata !31, metadata !"pPredict", metadata !6, i32 50331872, metadata !34, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!346 = metadata !{i32 224, i32 68, metadata !31, null}
!347 = metadata !{i32 786689, metadata !31, metadata !"pInit", metadata !6, i32 67109088, metadata !34, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!348 = metadata !{i32 224, i32 89, metadata !31, null}
!349 = metadata !{i32 224, i32 97, metadata !350, null}
!350 = metadata !{i32 786443, metadata !31, i32 224, i32 96, metadata !6, i32 45} ; [ DW_TAG_lexical_block ]
!351 = metadata !{i32 225, i32 1, metadata !350, null}
!352 = metadata !{i32 226, i32 1, metadata !350, null}
!353 = metadata !{i32 227, i32 1, metadata !350, null}
!354 = metadata !{i32 228, i32 1, metadata !350, null}
!355 = metadata !{i32 229, i32 1, metadata !350, null}
!356 = metadata !{i32 231, i32 2, metadata !350, null}
!357 = metadata !{i32 232, i32 31, metadata !358, null}
!358 = metadata !{i32 786443, metadata !359, i32 232, i32 8, metadata !6, i32 47} ; [ DW_TAG_lexical_block ]
!359 = metadata !{i32 786443, metadata !350, i32 231, i32 13, metadata !6, i32 46} ; [ DW_TAG_lexical_block ]
!360 = metadata !{i32 232, i32 59, metadata !361, null}
!361 = metadata !{i32 786443, metadata !358, i32 232, i32 58, metadata !6, i32 48} ; [ DW_TAG_lexical_block ]
!362 = metadata !{i32 233, i32 4, metadata !361, null}
!363 = metadata !{i32 234, i32 3, metadata !361, null}
!364 = metadata !{i32 232, i32 53, metadata !358, null}
!365 = metadata !{i32 786688, metadata !358, metadata !"i", metadata !6, i32 232, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!366 = metadata !{i32 237, i32 2, metadata !350, null}
!367 = metadata !{i32 247, i32 37, metadata !368, null}
!368 = metadata !{i32 786443, metadata !369, i32 247, i32 14, metadata !6, i32 53} ; [ DW_TAG_lexical_block ]
!369 = metadata !{i32 786443, metadata !350, i32 245, i32 9, metadata !6, i32 52} ; [ DW_TAG_lexical_block ]
!370 = metadata !{i32 238, i32 3, metadata !371, null}
!371 = metadata !{i32 786443, metadata !350, i32 237, i32 17, metadata !6, i32 49} ; [ DW_TAG_lexical_block ]
!372 = metadata !{i32 239, i32 4, metadata !373, null}
!373 = metadata !{i32 786443, metadata !371, i32 238, i32 28, metadata !6, i32 50} ; [ DW_TAG_lexical_block ]
!374 = metadata !{i32 240, i32 3, metadata !373, null}
!375 = metadata !{i32 241, i32 4, metadata !376, null}
!376 = metadata !{i32 786443, metadata !371, i32 240, i32 10, metadata !6, i32 51} ; [ DW_TAG_lexical_block ]
!377 = metadata !{i32 242, i32 4, metadata !376, null}
!378 = metadata !{i32 244, i32 3, metadata !371, null}
!379 = metadata !{i32 247, i32 61, metadata !380, null}
!380 = metadata !{i32 786443, metadata !368, i32 247, i32 60, metadata !6, i32 54} ; [ DW_TAG_lexical_block ]
!381 = metadata !{i32 248, i32 1, metadata !380, null}
!382 = metadata !{i32 249, i32 9, metadata !380, null}
!383 = metadata !{i32 786688, metadata !369, metadata !"sum", metadata !6, i32 246, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!384 = metadata !{i32 250, i32 3, metadata !380, null}
!385 = metadata !{i32 247, i32 55, metadata !368, null}
!386 = metadata !{i32 786688, metadata !368, metadata !"i", metadata !6, i32 247, metadata !18, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!387 = metadata !{i32 254, i32 1, metadata !350, null}
