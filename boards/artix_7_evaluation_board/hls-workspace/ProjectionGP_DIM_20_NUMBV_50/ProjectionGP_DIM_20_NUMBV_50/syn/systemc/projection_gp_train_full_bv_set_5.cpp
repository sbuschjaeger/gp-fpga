#include "projection_gp_train_full_bv_set.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void projection_gp_train_full_bv_set::thread_tmp_137_16_fu_4446_p2() {
    tmp_137_16_fu_4446_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_11.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_11));
}

void projection_gp_train_full_bv_set::thread_tmp_137_17_fu_4457_p2() {
    tmp_137_17_fu_4457_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_12.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_12));
}

void projection_gp_train_full_bv_set::thread_tmp_137_18_fu_4468_p2() {
    tmp_137_18_fu_4468_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_13.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_13));
}

void projection_gp_train_full_bv_set::thread_tmp_137_19_fu_4479_p2() {
    tmp_137_19_fu_4479_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_14.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_14));
}

void projection_gp_train_full_bv_set::thread_tmp_137_1_fu_4270_p2() {
    tmp_137_1_fu_4270_p2 = (!phi_mul2_phi_fu_2340_p4.read().is_01() || !ap_const_lv12_1.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_phi_fu_2340_p4.read()) + sc_biguint<12>(ap_const_lv12_1));
}

void projection_gp_train_full_bv_set::thread_tmp_137_20_fu_4490_p2() {
    tmp_137_20_fu_4490_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_15.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_15));
}

void projection_gp_train_full_bv_set::thread_tmp_137_21_fu_4501_p2() {
    tmp_137_21_fu_4501_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_16.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_16));
}

void projection_gp_train_full_bv_set::thread_tmp_137_22_fu_4512_p2() {
    tmp_137_22_fu_4512_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_17.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_17));
}

void projection_gp_train_full_bv_set::thread_tmp_137_23_fu_4523_p2() {
    tmp_137_23_fu_4523_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_18.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_18));
}

void projection_gp_train_full_bv_set::thread_tmp_137_24_fu_4534_p2() {
    tmp_137_24_fu_4534_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_19.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_19));
}

void projection_gp_train_full_bv_set::thread_tmp_137_25_fu_4545_p2() {
    tmp_137_25_fu_4545_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_1A.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_1A));
}

void projection_gp_train_full_bv_set::thread_tmp_137_26_fu_4556_p2() {
    tmp_137_26_fu_4556_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_1B.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_1B));
}

void projection_gp_train_full_bv_set::thread_tmp_137_27_fu_4567_p2() {
    tmp_137_27_fu_4567_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_1C.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_1C));
}

void projection_gp_train_full_bv_set::thread_tmp_137_28_fu_4578_p2() {
    tmp_137_28_fu_4578_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_1D.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_1D));
}

void projection_gp_train_full_bv_set::thread_tmp_137_29_fu_4589_p2() {
    tmp_137_29_fu_4589_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_1E.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_1E));
}

void projection_gp_train_full_bv_set::thread_tmp_137_2_fu_4281_p2() {
    tmp_137_2_fu_4281_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_2.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_2));
}

void projection_gp_train_full_bv_set::thread_tmp_137_30_fu_4600_p2() {
    tmp_137_30_fu_4600_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_1F.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_1F));
}

void projection_gp_train_full_bv_set::thread_tmp_137_31_fu_4611_p2() {
    tmp_137_31_fu_4611_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_20.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_20));
}

void projection_gp_train_full_bv_set::thread_tmp_137_32_fu_4622_p2() {
    tmp_137_32_fu_4622_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_21.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_21));
}

void projection_gp_train_full_bv_set::thread_tmp_137_33_fu_4633_p2() {
    tmp_137_33_fu_4633_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_22.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_22));
}

void projection_gp_train_full_bv_set::thread_tmp_137_34_fu_4644_p2() {
    tmp_137_34_fu_4644_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_23.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_23));
}

void projection_gp_train_full_bv_set::thread_tmp_137_35_fu_4655_p2() {
    tmp_137_35_fu_4655_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_24.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_24));
}

void projection_gp_train_full_bv_set::thread_tmp_137_36_fu_4666_p2() {
    tmp_137_36_fu_4666_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_25.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_25));
}

void projection_gp_train_full_bv_set::thread_tmp_137_37_fu_4677_p2() {
    tmp_137_37_fu_4677_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_26.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_26));
}

void projection_gp_train_full_bv_set::thread_tmp_137_38_fu_4705_p2() {
    tmp_137_38_fu_4705_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_27.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_27));
}

void projection_gp_train_full_bv_set::thread_tmp_137_39_fu_4716_p2() {
    tmp_137_39_fu_4716_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_28.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_28));
}

void projection_gp_train_full_bv_set::thread_tmp_137_3_fu_4292_p2() {
    tmp_137_3_fu_4292_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_3.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_3));
}

void projection_gp_train_full_bv_set::thread_tmp_137_40_fu_4727_p2() {
    tmp_137_40_fu_4727_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_29.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_29));
}

void projection_gp_train_full_bv_set::thread_tmp_137_41_fu_4738_p2() {
    tmp_137_41_fu_4738_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_2A.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_2A));
}

void projection_gp_train_full_bv_set::thread_tmp_137_42_fu_4749_p2() {
    tmp_137_42_fu_4749_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_2B.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_2B));
}

void projection_gp_train_full_bv_set::thread_tmp_137_43_fu_4760_p2() {
    tmp_137_43_fu_4760_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_2C.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_2C));
}

void projection_gp_train_full_bv_set::thread_tmp_137_44_fu_4771_p2() {
    tmp_137_44_fu_4771_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_2D.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_2D));
}

void projection_gp_train_full_bv_set::thread_tmp_137_45_fu_4782_p2() {
    tmp_137_45_fu_4782_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_2E.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_2E));
}

void projection_gp_train_full_bv_set::thread_tmp_137_46_fu_4793_p2() {
    tmp_137_46_fu_4793_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_2F.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_2F));
}

void projection_gp_train_full_bv_set::thread_tmp_137_47_fu_4804_p2() {
    tmp_137_47_fu_4804_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_30.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_30));
}

void projection_gp_train_full_bv_set::thread_tmp_137_48_fu_4810_p2() {
    tmp_137_48_fu_4810_p2 = (!ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read().is_01() || !ap_const_lv12_31.is_01())? sc_lv<12>(): (sc_biguint<12>(ap_reg_ppstg_phi_mul2_reg_2336_pp1_it1.read()) + sc_biguint<12>(ap_const_lv12_31));
}

void projection_gp_train_full_bv_set::thread_tmp_137_49_fu_4688_p2() {
    tmp_137_49_fu_4688_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_32.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_32));
}

void projection_gp_train_full_bv_set::thread_tmp_137_4_fu_4303_p2() {
    tmp_137_4_fu_4303_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_4.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_4));
}

void projection_gp_train_full_bv_set::thread_tmp_137_5_fu_4314_p2() {
    tmp_137_5_fu_4314_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_5.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_5));
}

void projection_gp_train_full_bv_set::thread_tmp_137_6_fu_4325_p2() {
    tmp_137_6_fu_4325_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_6.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_6));
}

void projection_gp_train_full_bv_set::thread_tmp_137_7_fu_4336_p2() {
    tmp_137_7_fu_4336_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_7.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_7));
}

void projection_gp_train_full_bv_set::thread_tmp_137_8_fu_4347_p2() {
    tmp_137_8_fu_4347_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_8.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_8));
}

void projection_gp_train_full_bv_set::thread_tmp_137_9_fu_4358_p2() {
    tmp_137_9_fu_4358_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_9.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_9));
}

void projection_gp_train_full_bv_set::thread_tmp_137_s_fu_4369_p2() {
    tmp_137_s_fu_4369_p2 = (!phi_mul2_reg_2336.read().is_01() || !ap_const_lv12_A.is_01())? sc_lv<12>(): (sc_biguint<12>(phi_mul2_reg_2336.read()) + sc_biguint<12>(ap_const_lv12_A));
}

void projection_gp_train_full_bv_set::thread_tmp_138_10_fu_4386_p1() {
    tmp_138_10_fu_4386_p1 = esl_zext<64,12>(tmp_137_10_fu_4380_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_11_fu_4397_p1() {
    tmp_138_11_fu_4397_p1 = esl_zext<64,12>(tmp_137_11_fu_4391_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_12_fu_4408_p1() {
    tmp_138_12_fu_4408_p1 = esl_zext<64,12>(tmp_137_12_fu_4402_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_13_fu_4419_p1() {
    tmp_138_13_fu_4419_p1 = esl_zext<64,12>(tmp_137_13_fu_4413_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_14_fu_4430_p1() {
    tmp_138_14_fu_4430_p1 = esl_zext<64,12>(tmp_137_14_fu_4424_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_15_fu_4441_p1() {
    tmp_138_15_fu_4441_p1 = esl_zext<64,12>(tmp_137_15_fu_4435_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_16_fu_4452_p1() {
    tmp_138_16_fu_4452_p1 = esl_zext<64,12>(tmp_137_16_fu_4446_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_17_fu_4463_p1() {
    tmp_138_17_fu_4463_p1 = esl_zext<64,12>(tmp_137_17_fu_4457_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_18_fu_4474_p1() {
    tmp_138_18_fu_4474_p1 = esl_zext<64,12>(tmp_137_18_fu_4468_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_19_fu_4485_p1() {
    tmp_138_19_fu_4485_p1 = esl_zext<64,12>(tmp_137_19_fu_4479_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_1_fu_4276_p1() {
    tmp_138_1_fu_4276_p1 = esl_zext<64,12>(tmp_137_1_fu_4270_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_20_fu_4496_p1() {
    tmp_138_20_fu_4496_p1 = esl_zext<64,12>(tmp_137_20_fu_4490_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_21_fu_4507_p1() {
    tmp_138_21_fu_4507_p1 = esl_zext<64,12>(tmp_137_21_fu_4501_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_22_fu_4518_p1() {
    tmp_138_22_fu_4518_p1 = esl_zext<64,12>(tmp_137_22_fu_4512_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_23_fu_4529_p1() {
    tmp_138_23_fu_4529_p1 = esl_zext<64,12>(tmp_137_23_fu_4523_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_24_fu_4540_p1() {
    tmp_138_24_fu_4540_p1 = esl_zext<64,12>(tmp_137_24_fu_4534_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_25_fu_4551_p1() {
    tmp_138_25_fu_4551_p1 = esl_zext<64,12>(tmp_137_25_fu_4545_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_26_fu_4562_p1() {
    tmp_138_26_fu_4562_p1 = esl_zext<64,12>(tmp_137_26_fu_4556_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_27_fu_4573_p1() {
    tmp_138_27_fu_4573_p1 = esl_zext<64,12>(tmp_137_27_fu_4567_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_28_fu_4584_p1() {
    tmp_138_28_fu_4584_p1 = esl_zext<64,12>(tmp_137_28_fu_4578_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_29_fu_4595_p1() {
    tmp_138_29_fu_4595_p1 = esl_zext<64,12>(tmp_137_29_fu_4589_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_2_fu_4287_p1() {
    tmp_138_2_fu_4287_p1 = esl_zext<64,12>(tmp_137_2_fu_4281_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_30_fu_4606_p1() {
    tmp_138_30_fu_4606_p1 = esl_zext<64,12>(tmp_137_30_fu_4600_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_31_fu_4617_p1() {
    tmp_138_31_fu_4617_p1 = esl_zext<64,12>(tmp_137_31_fu_4611_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_32_fu_4628_p1() {
    tmp_138_32_fu_4628_p1 = esl_zext<64,12>(tmp_137_32_fu_4622_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_33_fu_4639_p1() {
    tmp_138_33_fu_4639_p1 = esl_zext<64,12>(tmp_137_33_fu_4633_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_34_fu_4650_p1() {
    tmp_138_34_fu_4650_p1 = esl_zext<64,12>(tmp_137_34_fu_4644_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_35_fu_4661_p1() {
    tmp_138_35_fu_4661_p1 = esl_zext<64,12>(tmp_137_35_fu_4655_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_36_fu_4672_p1() {
    tmp_138_36_fu_4672_p1 = esl_zext<64,12>(tmp_137_36_fu_4666_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_37_fu_4683_p1() {
    tmp_138_37_fu_4683_p1 = esl_zext<64,12>(tmp_137_37_fu_4677_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_38_fu_4711_p1() {
    tmp_138_38_fu_4711_p1 = esl_zext<64,12>(tmp_137_38_fu_4705_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_39_fu_4722_p1() {
    tmp_138_39_fu_4722_p1 = esl_zext<64,12>(tmp_137_39_fu_4716_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_3_fu_4298_p1() {
    tmp_138_3_fu_4298_p1 = esl_zext<64,12>(tmp_137_3_fu_4292_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_40_fu_4733_p1() {
    tmp_138_40_fu_4733_p1 = esl_zext<64,12>(tmp_137_40_fu_4727_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_41_fu_4744_p1() {
    tmp_138_41_fu_4744_p1 = esl_zext<64,12>(tmp_137_41_fu_4738_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_42_fu_4755_p1() {
    tmp_138_42_fu_4755_p1 = esl_zext<64,12>(tmp_137_42_fu_4749_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_43_fu_4766_p1() {
    tmp_138_43_fu_4766_p1 = esl_zext<64,12>(tmp_137_43_fu_4760_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_44_fu_4777_p1() {
    tmp_138_44_fu_4777_p1 = esl_zext<64,12>(tmp_137_44_fu_4771_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_45_fu_4788_p1() {
    tmp_138_45_fu_4788_p1 = esl_zext<64,12>(tmp_137_45_fu_4782_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_46_fu_4799_p1() {
    tmp_138_46_fu_4799_p1 = esl_zext<64,12>(tmp_137_46_fu_4793_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_47_fu_4816_p1() {
    tmp_138_47_fu_4816_p1 = esl_zext<64,12>(tmp_137_47_reg_7867.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_48_fu_4820_p1() {
    tmp_138_48_fu_4820_p1 = esl_zext<64,12>(tmp_137_48_reg_7872.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_49_fu_4694_p1() {
    tmp_138_49_fu_4694_p1 = esl_zext<64,12>(tmp_137_49_fu_4688_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_4_fu_4309_p1() {
    tmp_138_4_fu_4309_p1 = esl_zext<64,12>(tmp_137_4_fu_4303_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_5_fu_4320_p1() {
    tmp_138_5_fu_4320_p1 = esl_zext<64,12>(tmp_137_5_fu_4314_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_6_fu_4331_p1() {
    tmp_138_6_fu_4331_p1 = esl_zext<64,12>(tmp_137_6_fu_4325_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_7_fu_4342_p1() {
    tmp_138_7_fu_4342_p1 = esl_zext<64,12>(tmp_137_7_fu_4336_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_8_fu_4353_p1() {
    tmp_138_8_fu_4353_p1 = esl_zext<64,12>(tmp_137_8_fu_4347_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_9_fu_4364_p1() {
    tmp_138_9_fu_4364_p1 = esl_zext<64,12>(tmp_137_9_fu_4358_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_fu_4265_p1() {
    tmp_138_fu_4265_p1 = esl_zext<64,12>(phi_mul2_phi_fu_2340_p4.read());
}

void projection_gp_train_full_bv_set::thread_tmp_138_s_fu_4375_p1() {
    tmp_138_s_fu_4375_p1 = esl_zext<64,12>(tmp_137_s_fu_4369_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_14_fu_5048_p2() {
    tmp_14_fu_5048_p2 = (notrhs_fu_5042_p2.read() | notlhs_fu_5036_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_15_fu_5066_p2() {
    tmp_15_fu_5066_p2 = (notrhs1_fu_5060_p2.read() | notlhs1_fu_5054_p2.read());
}

void projection_gp_train_full_bv_set::thread_tmp_16_fu_5072_p2() {
    tmp_16_fu_5072_p2 = (tmp_14_reg_8062.read() & tmp_15_reg_8067.read());
}

void projection_gp_train_full_bv_set::thread_tmp_17_i_fu_4996_p1() {
    tmp_17_i_fu_4996_p1 = esl_zext<64,6>(index_3_reg_2392.read());
}

void projection_gp_train_full_bv_set::thread_tmp_18_fu_5076_p2() {
    tmp_18_fu_5076_p2 = (tmp_16_fu_5072_p2.read() & tmp_17_reg_8072.read());
}

void projection_gp_train_full_bv_set::thread_tmp_20_i_fu_4991_p1() {
    tmp_20_i_fu_4991_p1 = esl_zext<64,13>(tmp_19_i_reg_8035.read());
}

void projection_gp_train_full_bv_set::thread_tmp_59_fu_4938_p2() {
    tmp_59_fu_4938_p2 = (!ap_const_lv32_4.is_01())? sc_lv<32>(): bvCnt.read() << (unsigned short)ap_const_lv32_4.to_uint();
}

void projection_gp_train_full_bv_set::thread_tmp_60_fu_4944_p2() {
    tmp_60_fu_4944_p2 = (!ap_const_lv32_2.is_01())? sc_lv<32>(): bvCnt.read() << (unsigned short)ap_const_lv32_2.to_uint();
}

void projection_gp_train_full_bv_set::thread_tmp_61_fu_5014_p1() {
    tmp_61_fu_5014_p1 = tScore_to_int_fu_5001_p1.read().range(23-1, 0);
}

void projection_gp_train_full_bv_set::thread_tmp_62_fu_5032_p1() {
    tmp_62_fu_5032_p1 = minScore1_i_to_int_fu_5018_p1.read().range(23-1, 0);
}

void projection_gp_train_full_bv_set::thread_tmp_89_fu_4242_p1() {
    tmp_89_fu_4242_p1 = esl_zext<64,6>(ap_reg_ppstg_i1_reg_2301_pp0_it18.read());
}

void projection_gp_train_full_bv_set::thread_tmp_92_fu_4260_p1() {
    tmp_92_fu_4260_p1 = esl_zext<64,6>(i4_phi_fu_2329_p4.read());
}

void projection_gp_train_full_bv_set::thread_tmp_94_fu_4877_p2() {
    tmp_94_fu_4877_p2 = (!ap_reg_ppstg_i6_mid2_reg_7930_pp2_it1.read().is_01() || !ap_const_lv6_32.is_01())? sc_lv<1>(): sc_lv<1>(ap_reg_ppstg_i6_mid2_reg_7930_pp2_it1.read() == ap_const_lv6_32);
}

void projection_gp_train_full_bv_set::thread_tmp_96_fu_4864_p1() {
    tmp_96_fu_4864_p1 = esl_zext<64,6>(i6_mid2_reg_7930.read());
}

void projection_gp_train_full_bv_set::thread_tmp_97_fu_4890_p2() {
    tmp_97_fu_4890_p2 = (!ap_reg_ppstg_j7_mid2_reg_7922_pp2_it1.read().is_01() || !ap_const_lv6_32.is_01())? sc_lv<1>(): sc_lv<1>(ap_reg_ppstg_j7_mid2_reg_7922_pp2_it1.read() == ap_const_lv6_32);
}

void projection_gp_train_full_bv_set::thread_tmp_98_fu_4868_p1() {
    tmp_98_fu_4868_p1 = esl_zext<64,6>(j7_mid2_reg_7922.read());
}

void projection_gp_train_full_bv_set::thread_tmp_99_i_fu_4955_p2() {
    tmp_99_i_fu_4955_p2 = (!tmp_fu_4950_p2.read().is_01() || !tmp_59_fu_4938_p2.read().is_01())? sc_lv<32>(): (sc_biguint<32>(tmp_fu_4950_p2.read()) + sc_biguint<32>(tmp_59_fu_4938_p2.read()));
}

void projection_gp_train_full_bv_set::thread_tmp_fu_4950_p2() {
    tmp_fu_4950_p2 = (!tmp_60_fu_4944_p2.read().is_01() || !i_i_cast2_reg_7984.read().is_01())? sc_lv<32>(): (sc_biguint<32>(tmp_60_fu_4944_p2.read()) + sc_biguint<32>(i_i_cast2_reg_7984.read()));
}

void projection_gp_train_full_bv_set::thread_tmp_i_fu_4929_p1() {
    tmp_i_fu_4929_p1 = esl_zext<64,5>(i_i_reg_2381.read());
}

}

