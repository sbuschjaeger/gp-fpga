; ModuleID = '/home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace/ProjectionGP_DIM_20_NUMBV_50/ProjectionGP_DIM_20_NUMBV_50/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@C = global [2601 x float] zeroinitializer, align 16
@Q = global [2601 x float] zeroinitializer, align 16
@e = global [51 x float] zeroinitializer, align 16
@k = global [50 x float] zeroinitializer, align 16
@s = global [51 x float] zeroinitializer, align 16
@alpha = global [51 x float] zeroinitializer, align 16
@basisVectors = global [1020 x float] zeroinitializer, align 16
@bvCnt = global i32 0, align 4
@p_str = private unnamed_addr constant [12 x i8] c"hls_label_0\00", align 1
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str2 = private unnamed_addr constant [20 x i8] c"DELETE_BV_SWAP_LOOP\00", align 1
@p_str4 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_OUTER\00", align 1
@p_str7 = private unnamed_addr constant [7 x i8] c"CALC_K\00", align 1
@p_str8 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_OUTER\00", align 1
@p_str9 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_INNER\00", align 1
@p_str10 = private unnamed_addr constant [7 x i8] c"CALC_S\00", align 1
@p_str11 = private unnamed_addr constant [13 x i8] c"UPDATE_ALPHA\00", align 1
@p_str12 = private unnamed_addr constant [15 x i8] c"UPDATE_C_OUTER\00", align 1
@p_str13 = private unnamed_addr constant [15 x i8] c"UPDATE_C_INNER\00", align 1
@p_str15 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_INNER\00", align 1
@p_str16 = private unnamed_addr constant [8 x i8] c"COPY_BV\00", align 1
@p_str17 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@projection_gp_str = internal unnamed_addr constant [14 x i8] c"projection_gp\00"
@UPDATE_Q_OUTER_UPDATE_Q_INNER_s = internal unnamed_addr constant [30 x i8] c"UPDATE_Q_OUTER_UPDATE_Q_INNER\00"
@p_str3 = internal unnamed_addr constant [1 x i8] zeroinitializer
@RAM_1P_str = internal unnamed_addr constant [7 x i8] c"RAM_1P\00"

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

define internal fastcc void @projection_gp_deleteBV(i32 %pIndex) nounwind uwtable {
  %pIndex_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %pIndex) nounwind
  %tmp = zext i32 %pIndex_read to i64
  %alpha_addr = getelementptr inbounds [51 x float]* @alpha, i64 0, i64 %tmp
  %temp = load float* %alpha_addr, align 4
  %alpha_load = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 50), align 8
  store float %alpha_load, float* %alpha_addr, align 4
  store float %temp, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 50), align 8
  call fastcc void @projection_gp_swapRowAndColumn([2601 x float]* @C, i32 %pIndex_read) nounwind
  call fastcc void @projection_gp_swapRowAndColumn([2601 x float]* @Q, i32 %pIndex_read) nounwind
  %tmp_10 = shl i32 %pIndex_read, 4
  %tmp_24 = shl i32 %pIndex_read, 2
  br label %1

; <label>:1                                       ; preds = %2, %0
  %i = phi i5 [ 0, %0 ], [ %i_1, %2 ]
  %exitcond4 = icmp eq i5 %i, -12
  %i_1 = add i5 %i, 1
  br i1 %exitcond4, label %3, label %2

; <label>:2                                       ; preds = %1
  %i_cast6_cast = zext i5 %i to i6
  %i_cast5 = zext i5 %i to i32
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 20, i64 20, i64 20)
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @p_str2) nounwind
  %tmp_4 = call i32 (...)* @_ssdm_op_SpecRegionBegin([20 x i8]* @p_str2) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp1 = add i32 %tmp_24, %i_cast5
  %tmp_6 = add i32 %tmp1, %tmp_10
  %tmp_7 = zext i32 %tmp_6 to i64
  %basisVectors_addr = getelementptr inbounds [1020 x float]* @basisVectors, i64 0, i64 %tmp_7
  %temp_2 = load float* %basisVectors_addr, align 4
  %tmp_8 = add i6 %i_cast6_cast, -24
  %tmp_8_cast7 = sext i6 %tmp_8 to i10
  %tmp_9 = zext i10 %tmp_8_cast7 to i64
  %basisVectors_addr_1 = getelementptr inbounds [1020 x float]* @basisVectors, i64 0, i64 %tmp_9
  %basisVectors_load = load float* %basisVectors_addr_1, align 4
  store float %basisVectors_load, float* %basisVectors_addr, align 4
  store float %temp_2, float* %basisVectors_addr_1, align 4
  %empty = call i32 (...)* @_ssdm_op_SpecRegionEnd([20 x i8]* @p_str2, i32 %tmp_4) nounwind
  br label %1

; <label>:3                                       ; preds = %1
  %alphaStar = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 50), align 8
  %cStar = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2600), align 16
  %qStar = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2600), align 16
  %tmp_5 = fadd float %cStar, %qStar
  %temp_1 = fdiv float %alphaStar, %tmp_5
  %C_load = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2550), align 8
  %Q_load = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2550), align 8
  %tmp_1 = fadd float %C_load, %Q_load
  %tmp_2 = fmul float %tmp_1, %temp_1
  %alpha_load_3 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_3 = fsub float %alpha_load_3, %tmp_2
  store float %tmp_3, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 0), align 16
  %C_load_1 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2551), align 4
  %Q_load_1 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2551), align 4
  %tmp_11_1 = fadd float %C_load_1, %Q_load_1
  %tmp_12_1 = fmul float %tmp_11_1, %temp_1
  %alpha_load_4 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 1), align 4
  %tmp_14_1 = fsub float %alpha_load_4, %tmp_12_1
  store float %tmp_14_1, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 1), align 4
  %C_load_2 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2552), align 16
  %Q_load_2 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2552), align 16
  %tmp_11_2 = fadd float %C_load_2, %Q_load_2
  %tmp_12_2 = fmul float %tmp_11_2, %temp_1
  %alpha_load_5 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 2), align 8
  %tmp_14_2 = fsub float %alpha_load_5, %tmp_12_2
  store float %tmp_14_2, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 2), align 8
  %C_load_3 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2553), align 4
  %Q_load_3 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2553), align 4
  %tmp_11_3 = fadd float %C_load_3, %Q_load_3
  %tmp_12_3 = fmul float %tmp_11_3, %temp_1
  %alpha_load_6 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 3), align 4
  %tmp_14_3 = fsub float %alpha_load_6, %tmp_12_3
  store float %tmp_14_3, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 3), align 4
  %C_load_4 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2554), align 8
  %Q_load_4 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2554), align 8
  %tmp_11_4 = fadd float %C_load_4, %Q_load_4
  %tmp_12_4 = fmul float %tmp_11_4, %temp_1
  %alpha_load_7 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 4), align 16
  %tmp_14_4 = fsub float %alpha_load_7, %tmp_12_4
  store float %tmp_14_4, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 4), align 16
  %C_load_5 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2555), align 4
  %Q_load_5 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2555), align 4
  %tmp_11_5 = fadd float %C_load_5, %Q_load_5
  %tmp_12_5 = fmul float %tmp_11_5, %temp_1
  %alpha_load_8 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 5), align 4
  %tmp_14_5 = fsub float %alpha_load_8, %tmp_12_5
  store float %tmp_14_5, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 5), align 4
  %C_load_6 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2556), align 16
  %Q_load_6 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2556), align 16
  %tmp_11_6 = fadd float %C_load_6, %Q_load_6
  %tmp_12_6 = fmul float %tmp_11_6, %temp_1
  %alpha_load_9 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 6), align 8
  %tmp_14_6 = fsub float %alpha_load_9, %tmp_12_6
  store float %tmp_14_6, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 6), align 8
  %C_load_7 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2557), align 4
  %Q_load_7 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2557), align 4
  %tmp_11_7 = fadd float %C_load_7, %Q_load_7
  %tmp_12_7 = fmul float %tmp_11_7, %temp_1
  %alpha_load_10 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 7), align 4
  %tmp_14_7 = fsub float %alpha_load_10, %tmp_12_7
  store float %tmp_14_7, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 7), align 4
  %C_load_8 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2558), align 8
  %Q_load_8 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2558), align 8
  %tmp_11_8 = fadd float %C_load_8, %Q_load_8
  %tmp_12_8 = fmul float %tmp_11_8, %temp_1
  %alpha_load_11 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 8), align 16
  %tmp_14_8 = fsub float %alpha_load_11, %tmp_12_8
  store float %tmp_14_8, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 8), align 16
  %C_load_9 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2559), align 4
  %Q_load_9 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2559), align 4
  %tmp_11_9 = fadd float %C_load_9, %Q_load_9
  %tmp_12_9 = fmul float %tmp_11_9, %temp_1
  %alpha_load_12 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 9), align 4
  %tmp_14_9 = fsub float %alpha_load_12, %tmp_12_9
  store float %tmp_14_9, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 9), align 4
  %C_load_10 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2560), align 16
  %Q_load_10 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2560), align 16
  %tmp_11_s = fadd float %C_load_10, %Q_load_10
  %tmp_12_s = fmul float %tmp_11_s, %temp_1
  %alpha_load_13 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 10), align 8
  %tmp_14_s = fsub float %alpha_load_13, %tmp_12_s
  store float %tmp_14_s, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 10), align 8
  %C_load_11 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2561), align 4
  %Q_load_11 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2561), align 4
  %tmp_11_10 = fadd float %C_load_11, %Q_load_11
  %tmp_12_10 = fmul float %tmp_11_10, %temp_1
  %alpha_load_14 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 11), align 4
  %tmp_14_10 = fsub float %alpha_load_14, %tmp_12_10
  store float %tmp_14_10, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 11), align 4
  %C_load_12 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2562), align 8
  %Q_load_12 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2562), align 8
  %tmp_11_11 = fadd float %C_load_12, %Q_load_12
  %tmp_12_11 = fmul float %tmp_11_11, %temp_1
  %alpha_load_15 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 12), align 16
  %tmp_14_11 = fsub float %alpha_load_15, %tmp_12_11
  store float %tmp_14_11, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 12), align 16
  %C_load_13 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2563), align 4
  %Q_load_13 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2563), align 4
  %tmp_11_12 = fadd float %C_load_13, %Q_load_13
  %tmp_12_12 = fmul float %tmp_11_12, %temp_1
  %alpha_load_16 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 13), align 4
  %tmp_14_12 = fsub float %alpha_load_16, %tmp_12_12
  store float %tmp_14_12, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 13), align 4
  %C_load_14 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2564), align 16
  %Q_load_14 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2564), align 16
  %tmp_11_13 = fadd float %C_load_14, %Q_load_14
  %tmp_12_13 = fmul float %tmp_11_13, %temp_1
  %alpha_load_17 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 14), align 8
  %tmp_14_13 = fsub float %alpha_load_17, %tmp_12_13
  store float %tmp_14_13, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 14), align 8
  %C_load_15 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2565), align 4
  %Q_load_15 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2565), align 4
  %tmp_11_14 = fadd float %C_load_15, %Q_load_15
  %tmp_12_14 = fmul float %tmp_11_14, %temp_1
  %alpha_load_18 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 15), align 4
  %tmp_14_14 = fsub float %alpha_load_18, %tmp_12_14
  store float %tmp_14_14, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 15), align 4
  %C_load_16 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2566), align 8
  %Q_load_16 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2566), align 8
  %tmp_11_15 = fadd float %C_load_16, %Q_load_16
  %tmp_12_15 = fmul float %tmp_11_15, %temp_1
  %alpha_load_19 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 16), align 16
  %tmp_14_15 = fsub float %alpha_load_19, %tmp_12_15
  store float %tmp_14_15, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 16), align 16
  %C_load_17 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2567), align 4
  %Q_load_17 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2567), align 4
  %tmp_11_16 = fadd float %C_load_17, %Q_load_17
  %tmp_12_16 = fmul float %tmp_11_16, %temp_1
  %alpha_load_20 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 17), align 4
  %tmp_14_16 = fsub float %alpha_load_20, %tmp_12_16
  store float %tmp_14_16, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 17), align 4
  %C_load_18 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2568), align 16
  %Q_load_18 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2568), align 16
  %tmp_11_17 = fadd float %C_load_18, %Q_load_18
  %tmp_12_17 = fmul float %tmp_11_17, %temp_1
  %alpha_load_21 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 18), align 8
  %tmp_14_17 = fsub float %alpha_load_21, %tmp_12_17
  store float %tmp_14_17, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 18), align 8
  %C_load_19 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2569), align 4
  %Q_load_19 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2569), align 4
  %tmp_11_18 = fadd float %C_load_19, %Q_load_19
  %tmp_12_18 = fmul float %tmp_11_18, %temp_1
  %alpha_load_22 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 19), align 4
  %tmp_14_18 = fsub float %alpha_load_22, %tmp_12_18
  store float %tmp_14_18, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 19), align 4
  %C_load_20 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2570), align 8
  %Q_load_20 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2570), align 8
  %tmp_11_19 = fadd float %C_load_20, %Q_load_20
  %tmp_12_19 = fmul float %tmp_11_19, %temp_1
  %alpha_load_23 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 20), align 16
  %tmp_14_19 = fsub float %alpha_load_23, %tmp_12_19
  store float %tmp_14_19, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 20), align 16
  %C_load_21 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2571), align 4
  %Q_load_21 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2571), align 4
  %tmp_11_20 = fadd float %C_load_21, %Q_load_21
  %tmp_12_20 = fmul float %tmp_11_20, %temp_1
  %alpha_load_24 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 21), align 4
  %tmp_14_20 = fsub float %alpha_load_24, %tmp_12_20
  store float %tmp_14_20, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 21), align 4
  %C_load_22 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2572), align 16
  %Q_load_22 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2572), align 16
  %tmp_11_21 = fadd float %C_load_22, %Q_load_22
  %tmp_12_21 = fmul float %tmp_11_21, %temp_1
  %alpha_load_25 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 22), align 8
  %tmp_14_21 = fsub float %alpha_load_25, %tmp_12_21
  store float %tmp_14_21, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 22), align 8
  %C_load_23 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2573), align 4
  %Q_load_23 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2573), align 4
  %tmp_11_22 = fadd float %C_load_23, %Q_load_23
  %tmp_12_22 = fmul float %tmp_11_22, %temp_1
  %alpha_load_26 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 23), align 4
  %tmp_14_22 = fsub float %alpha_load_26, %tmp_12_22
  store float %tmp_14_22, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 23), align 4
  %C_load_24 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2574), align 8
  %Q_load_24 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2574), align 8
  %tmp_11_23 = fadd float %C_load_24, %Q_load_24
  %tmp_12_23 = fmul float %tmp_11_23, %temp_1
  %alpha_load_27 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 24), align 16
  %tmp_14_23 = fsub float %alpha_load_27, %tmp_12_23
  store float %tmp_14_23, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 24), align 16
  %C_load_25 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2575), align 4
  %Q_load_25 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2575), align 4
  %tmp_11_24 = fadd float %C_load_25, %Q_load_25
  %tmp_12_24 = fmul float %tmp_11_24, %temp_1
  %alpha_load_28 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 25), align 4
  %tmp_14_24 = fsub float %alpha_load_28, %tmp_12_24
  store float %tmp_14_24, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 25), align 4
  %C_load_26 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2576), align 16
  %Q_load_26 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2576), align 16
  %tmp_11_25 = fadd float %C_load_26, %Q_load_26
  %tmp_12_25 = fmul float %tmp_11_25, %temp_1
  %alpha_load_29 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 26), align 8
  %tmp_14_25 = fsub float %alpha_load_29, %tmp_12_25
  store float %tmp_14_25, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 26), align 8
  %C_load_27 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2577), align 4
  %Q_load_27 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2577), align 4
  %tmp_11_26 = fadd float %C_load_27, %Q_load_27
  %tmp_12_26 = fmul float %tmp_11_26, %temp_1
  %alpha_load_30 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 27), align 4
  %tmp_14_26 = fsub float %alpha_load_30, %tmp_12_26
  store float %tmp_14_26, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 27), align 4
  %C_load_28 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2578), align 8
  %Q_load_28 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2578), align 8
  %tmp_11_27 = fadd float %C_load_28, %Q_load_28
  %tmp_12_27 = fmul float %tmp_11_27, %temp_1
  %alpha_load_31 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 28), align 16
  %tmp_14_27 = fsub float %alpha_load_31, %tmp_12_27
  store float %tmp_14_27, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 28), align 16
  %C_load_29 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2579), align 4
  %Q_load_29 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2579), align 4
  %tmp_11_28 = fadd float %C_load_29, %Q_load_29
  %tmp_12_28 = fmul float %tmp_11_28, %temp_1
  %alpha_load_32 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 29), align 4
  %tmp_14_28 = fsub float %alpha_load_32, %tmp_12_28
  store float %tmp_14_28, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 29), align 4
  %C_load_30 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2580), align 16
  %Q_load_30 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2580), align 16
  %tmp_11_29 = fadd float %C_load_30, %Q_load_30
  %tmp_12_29 = fmul float %tmp_11_29, %temp_1
  %alpha_load_33 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 30), align 8
  %tmp_14_29 = fsub float %alpha_load_33, %tmp_12_29
  store float %tmp_14_29, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 30), align 8
  %C_load_31 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2581), align 4
  %Q_load_31 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2581), align 4
  %tmp_11_30 = fadd float %C_load_31, %Q_load_31
  %tmp_12_30 = fmul float %tmp_11_30, %temp_1
  %alpha_load_34 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 31), align 4
  %tmp_14_30 = fsub float %alpha_load_34, %tmp_12_30
  store float %tmp_14_30, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 31), align 4
  %C_load_32 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2582), align 8
  %Q_load_32 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2582), align 8
  %tmp_11_31 = fadd float %C_load_32, %Q_load_32
  %tmp_12_31 = fmul float %tmp_11_31, %temp_1
  %alpha_load_35 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 32), align 16
  %tmp_14_31 = fsub float %alpha_load_35, %tmp_12_31
  store float %tmp_14_31, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 32), align 16
  %C_load_33 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2583), align 4
  %Q_load_33 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2583), align 4
  %tmp_11_32 = fadd float %C_load_33, %Q_load_33
  %tmp_12_32 = fmul float %tmp_11_32, %temp_1
  %alpha_load_36 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 33), align 4
  %tmp_14_32 = fsub float %alpha_load_36, %tmp_12_32
  store float %tmp_14_32, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 33), align 4
  %C_load_34 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2584), align 16
  %Q_load_34 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2584), align 16
  %tmp_11_33 = fadd float %C_load_34, %Q_load_34
  %tmp_12_33 = fmul float %tmp_11_33, %temp_1
  %alpha_load_37 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 34), align 8
  %tmp_14_33 = fsub float %alpha_load_37, %tmp_12_33
  store float %tmp_14_33, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 34), align 8
  %C_load_35 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2585), align 4
  %Q_load_35 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2585), align 4
  %tmp_11_34 = fadd float %C_load_35, %Q_load_35
  %tmp_12_34 = fmul float %tmp_11_34, %temp_1
  %alpha_load_38 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 35), align 4
  %tmp_14_34 = fsub float %alpha_load_38, %tmp_12_34
  store float %tmp_14_34, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 35), align 4
  %C_load_36 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2586), align 8
  %Q_load_36 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2586), align 8
  %tmp_11_35 = fadd float %C_load_36, %Q_load_36
  %tmp_12_35 = fmul float %tmp_11_35, %temp_1
  %alpha_load_39 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 36), align 16
  %tmp_14_35 = fsub float %alpha_load_39, %tmp_12_35
  store float %tmp_14_35, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 36), align 16
  %C_load_37 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2587), align 4
  %Q_load_37 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2587), align 4
  %tmp_11_36 = fadd float %C_load_37, %Q_load_37
  %tmp_12_36 = fmul float %tmp_11_36, %temp_1
  %alpha_load_40 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 37), align 4
  %tmp_14_36 = fsub float %alpha_load_40, %tmp_12_36
  store float %tmp_14_36, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 37), align 4
  %C_load_38 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2588), align 16
  %Q_load_38 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2588), align 16
  %tmp_11_37 = fadd float %C_load_38, %Q_load_38
  %tmp_12_37 = fmul float %tmp_11_37, %temp_1
  %alpha_load_41 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 38), align 8
  %tmp_14_37 = fsub float %alpha_load_41, %tmp_12_37
  store float %tmp_14_37, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 38), align 8
  %C_load_39 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2589), align 4
  %Q_load_39 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2589), align 4
  %tmp_11_38 = fadd float %C_load_39, %Q_load_39
  %tmp_12_38 = fmul float %tmp_11_38, %temp_1
  %alpha_load_42 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 39), align 4
  %tmp_14_38 = fsub float %alpha_load_42, %tmp_12_38
  store float %tmp_14_38, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 39), align 4
  %C_load_40 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2590), align 8
  %Q_load_40 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2590), align 8
  %tmp_11_39 = fadd float %C_load_40, %Q_load_40
  %tmp_12_39 = fmul float %tmp_11_39, %temp_1
  %alpha_load_43 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 40), align 16
  %tmp_14_39 = fsub float %alpha_load_43, %tmp_12_39
  store float %tmp_14_39, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 40), align 16
  %C_load_41 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2591), align 4
  %Q_load_41 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2591), align 4
  %tmp_11_40 = fadd float %C_load_41, %Q_load_41
  %tmp_12_40 = fmul float %tmp_11_40, %temp_1
  %alpha_load_44 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 41), align 4
  %tmp_14_40 = fsub float %alpha_load_44, %tmp_12_40
  store float %tmp_14_40, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 41), align 4
  %C_load_42 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2592), align 16
  %Q_load_42 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2592), align 16
  %tmp_11_41 = fadd float %C_load_42, %Q_load_42
  %tmp_12_41 = fmul float %tmp_11_41, %temp_1
  %alpha_load_45 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 42), align 8
  %tmp_14_41 = fsub float %alpha_load_45, %tmp_12_41
  store float %tmp_14_41, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 42), align 8
  %C_load_43 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2593), align 4
  %Q_load_43 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2593), align 4
  %tmp_11_42 = fadd float %C_load_43, %Q_load_43
  %tmp_12_42 = fmul float %tmp_11_42, %temp_1
  %alpha_load_46 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 43), align 4
  %tmp_14_42 = fsub float %alpha_load_46, %tmp_12_42
  store float %tmp_14_42, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 43), align 4
  %C_load_44 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2594), align 8
  %Q_load_44 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2594), align 8
  %tmp_11_43 = fadd float %C_load_44, %Q_load_44
  %tmp_12_43 = fmul float %tmp_11_43, %temp_1
  %alpha_load_47 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 44), align 16
  %tmp_14_43 = fsub float %alpha_load_47, %tmp_12_43
  store float %tmp_14_43, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 44), align 16
  %C_load_45 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2595), align 4
  %Q_load_45 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2595), align 4
  %tmp_11_44 = fadd float %C_load_45, %Q_load_45
  %tmp_12_44 = fmul float %tmp_11_44, %temp_1
  %alpha_load_48 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 45), align 4
  %tmp_14_44 = fsub float %alpha_load_48, %tmp_12_44
  store float %tmp_14_44, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 45), align 4
  %C_load_46 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2596), align 16
  %Q_load_46 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2596), align 16
  %tmp_11_45 = fadd float %C_load_46, %Q_load_46
  %tmp_12_45 = fmul float %tmp_11_45, %temp_1
  %alpha_load_49 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 46), align 8
  %tmp_14_45 = fsub float %alpha_load_49, %tmp_12_45
  store float %tmp_14_45, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 46), align 8
  %C_load_47 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2597), align 4
  %Q_load_47 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2597), align 4
  %tmp_11_46 = fadd float %C_load_47, %Q_load_47
  %tmp_12_46 = fmul float %tmp_11_46, %temp_1
  %alpha_load_50 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 47), align 4
  %tmp_14_46 = fsub float %alpha_load_50, %tmp_12_46
  store float %tmp_14_46, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 47), align 4
  %C_load_48 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2598), align 8
  %Q_load_48 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2598), align 8
  %tmp_11_47 = fadd float %C_load_48, %Q_load_48
  %tmp_12_47 = fmul float %tmp_11_47, %temp_1
  %alpha_load_51 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 48), align 16
  %tmp_14_47 = fsub float %alpha_load_51, %tmp_12_47
  store float %tmp_14_47, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 48), align 16
  %C_load_49 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2599), align 4
  %Q_load_49 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2599), align 4
  %tmp_11_48 = fadd float %C_load_49, %Q_load_49
  %tmp_12_48 = fmul float %tmp_11_48, %temp_1
  %alpha_load_52 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 49), align 4
  %tmp_14_48 = fsub float %alpha_load_52, %tmp_12_48
  store float %tmp_14_48, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 49), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 50), align 8
  br label %4

; <label>:4                                       ; preds = %5, %3
  %i2 = phi i6 [ 0, %3 ], [ %i_2, %5 ]
  %phi_mul = phi i12 [ 0, %3 ], [ %next_mul, %5 ]
  %exitcond2 = icmp eq i6 %i2, -14
  %i_2 = add i6 %i2, 1
  br i1 %exitcond2, label %.preheader.0, label %5

; <label>:5                                       ; preds = %4
  %i2_cast3 = zext i6 %i2 to i12
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 50, i64 50, i64 50)
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @p_str4) nounwind
  %tmp_s = call i32 (...)* @_ssdm_op_SpecRegionBegin([18 x i8]* @p_str4) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %a = add i12 %i2_cast3, -1546
  %next_mul = add i12 %phi_mul, 51
  %tmp_11 = zext i12 %a to i64
  %Q_addr = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_11
  %C_addr = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_11
  %Q_load_50 = load float* %Q_addr, align 4
  %Q_load_51 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2550), align 8
  %tmp_12 = fmul float %Q_load_50, %Q_load_51
  %temp_4 = fdiv float %tmp_12, %qStar
  %C_load_50 = load float* %C_addr, align 4
  %C_load_51 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2550), align 8
  %tmp_13 = fmul float %C_load_50, %C_load_51
  %tmp_14 = fmul float %C_load_50, %Q_load_51
  %tmp_15 = fadd float %tmp_13, %tmp_14
  %tmp_16 = fmul float %Q_load_50, %C_load_51
  %tmp_17 = fadd float %tmp_15, %tmp_16
  %tmp_18 = fadd float %tmp_17, %tmp_12
  %tmp_19 = fdiv float %tmp_18, %tmp_5
  %tmp_20 = fsub float %temp_4, %tmp_19
  %tmp_21 = zext i12 %phi_mul to i64
  %C_addr_1 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_21
  %C_load_52 = load float* %C_addr_1, align 4
  %tmp_22 = fadd float %C_load_52, %tmp_20
  store float %tmp_22, float* %C_addr_1, align 4
  %Q_addr_1 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_21
  %Q_load_52 = load float* %Q_addr_1, align 4
  %tmp_23 = fsub float %Q_load_52, %temp_4
  store float %tmp_23, float* %Q_addr_1, align 4
  %c_1 = add i12 %phi_mul, 1
  %Q_load_53 = load float* %Q_addr, align 4
  %Q_load_54 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2551), align 4
  %tmp_21_1 = fmul float %Q_load_53, %Q_load_54
  %temp_4_1 = fdiv float %tmp_21_1, %qStar
  %C_load_53 = load float* %C_addr, align 4
  %C_load_54 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2551), align 4
  %tmp_22_1 = fmul float %C_load_53, %C_load_54
  %tmp_23_1 = fmul float %C_load_53, %Q_load_54
  %tmp_24_1 = fadd float %tmp_22_1, %tmp_23_1
  %tmp_25_1 = fmul float %Q_load_53, %C_load_54
  %tmp_26_1 = fadd float %tmp_24_1, %tmp_25_1
  %tmp_27_1 = fadd float %tmp_26_1, %tmp_21_1
  %tmp_28_1 = fdiv float %tmp_27_1, %tmp_5
  %tmp_29_1 = fsub float %temp_4_1, %tmp_28_1
  %tmp_30_1 = zext i12 %c_1 to i64
  %C_addr_2 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_1
  %C_load_55 = load float* %C_addr_2, align 4
  %tmp_31_1 = fadd float %C_load_55, %tmp_29_1
  store float %tmp_31_1, float* %C_addr_2, align 4
  %Q_addr_2 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_1
  %Q_load_55 = load float* %Q_addr_2, align 4
  %tmp_32_1 = fsub float %Q_load_55, %temp_4_1
  store float %tmp_32_1, float* %Q_addr_2, align 4
  %c_2 = add i12 %phi_mul, 2
  %Q_load_56 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2552), align 16
  %tmp_21_2 = fmul float %Q_load_53, %Q_load_56
  %temp_4_2 = fdiv float %tmp_21_2, %qStar
  %C_load_56 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2552), align 16
  %tmp_22_2 = fmul float %C_load_53, %C_load_56
  %tmp_23_2 = fmul float %C_load_53, %Q_load_56
  %tmp_24_2 = fadd float %tmp_22_2, %tmp_23_2
  %tmp_25_2 = fmul float %Q_load_53, %C_load_56
  %tmp_26_2 = fadd float %tmp_24_2, %tmp_25_2
  %tmp_27_2 = fadd float %tmp_26_2, %tmp_21_2
  %tmp_28_2 = fdiv float %tmp_27_2, %tmp_5
  %tmp_29_2 = fsub float %temp_4_2, %tmp_28_2
  %tmp_30_2 = zext i12 %c_2 to i64
  %C_addr_3 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_2
  %C_load_57 = load float* %C_addr_3, align 4
  %tmp_31_2 = fadd float %C_load_57, %tmp_29_2
  store float %tmp_31_2, float* %C_addr_3, align 4
  %Q_addr_3 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_2
  %Q_load_57 = load float* %Q_addr_3, align 4
  %tmp_32_2 = fsub float %Q_load_57, %temp_4_2
  store float %tmp_32_2, float* %Q_addr_3, align 4
  %c_3 = add i12 %phi_mul, 3
  %Q_load_58 = load float* %Q_addr, align 4
  %Q_load_59 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2553), align 4
  %tmp_21_3 = fmul float %Q_load_58, %Q_load_59
  %temp_4_3 = fdiv float %tmp_21_3, %qStar
  %C_load_58 = load float* %C_addr, align 4
  %C_load_59 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2553), align 4
  %tmp_22_3 = fmul float %C_load_58, %C_load_59
  %tmp_23_3 = fmul float %C_load_58, %Q_load_59
  %tmp_24_3 = fadd float %tmp_22_3, %tmp_23_3
  %tmp_25_3 = fmul float %Q_load_58, %C_load_59
  %tmp_26_3 = fadd float %tmp_24_3, %tmp_25_3
  %tmp_27_3 = fadd float %tmp_26_3, %tmp_21_3
  %tmp_28_3 = fdiv float %tmp_27_3, %tmp_5
  %tmp_29_3 = fsub float %temp_4_3, %tmp_28_3
  %tmp_30_3 = zext i12 %c_3 to i64
  %C_addr_4 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_3
  %C_load_60 = load float* %C_addr_4, align 4
  %tmp_31_3 = fadd float %C_load_60, %tmp_29_3
  store float %tmp_31_3, float* %C_addr_4, align 4
  %Q_addr_4 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_3
  %Q_load_60 = load float* %Q_addr_4, align 4
  %tmp_32_3 = fsub float %Q_load_60, %temp_4_3
  store float %tmp_32_3, float* %Q_addr_4, align 4
  %c_4 = add i12 %phi_mul, 4
  %Q_load_61 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2554), align 8
  %tmp_21_4 = fmul float %Q_load_58, %Q_load_61
  %temp_4_4 = fdiv float %tmp_21_4, %qStar
  %C_load_61 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2554), align 8
  %tmp_22_4 = fmul float %C_load_58, %C_load_61
  %tmp_23_4 = fmul float %C_load_58, %Q_load_61
  %tmp_24_4 = fadd float %tmp_22_4, %tmp_23_4
  %tmp_25_4 = fmul float %Q_load_58, %C_load_61
  %tmp_26_4 = fadd float %tmp_24_4, %tmp_25_4
  %tmp_27_4 = fadd float %tmp_26_4, %tmp_21_4
  %tmp_28_4 = fdiv float %tmp_27_4, %tmp_5
  %tmp_29_4 = fsub float %temp_4_4, %tmp_28_4
  %tmp_30_4 = zext i12 %c_4 to i64
  %C_addr_5 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_4
  %C_load_62 = load float* %C_addr_5, align 4
  %tmp_31_4 = fadd float %C_load_62, %tmp_29_4
  store float %tmp_31_4, float* %C_addr_5, align 4
  %Q_addr_5 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_4
  %Q_load_62 = load float* %Q_addr_5, align 4
  %tmp_32_4 = fsub float %Q_load_62, %temp_4_4
  store float %tmp_32_4, float* %Q_addr_5, align 4
  %c_5 = add i12 %phi_mul, 5
  %Q_load_63 = load float* %Q_addr, align 4
  %Q_load_64 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2555), align 4
  %tmp_21_5 = fmul float %Q_load_63, %Q_load_64
  %temp_4_5 = fdiv float %tmp_21_5, %qStar
  %C_load_63 = load float* %C_addr, align 4
  %C_load_64 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2555), align 4
  %tmp_22_5 = fmul float %C_load_63, %C_load_64
  %tmp_23_5 = fmul float %C_load_63, %Q_load_64
  %tmp_24_5 = fadd float %tmp_22_5, %tmp_23_5
  %tmp_25_5 = fmul float %Q_load_63, %C_load_64
  %tmp_26_5 = fadd float %tmp_24_5, %tmp_25_5
  %tmp_27_5 = fadd float %tmp_26_5, %tmp_21_5
  %tmp_28_5 = fdiv float %tmp_27_5, %tmp_5
  %tmp_29_5 = fsub float %temp_4_5, %tmp_28_5
  %tmp_30_5 = zext i12 %c_5 to i64
  %C_addr_6 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_5
  %C_load_65 = load float* %C_addr_6, align 4
  %tmp_31_5 = fadd float %C_load_65, %tmp_29_5
  store float %tmp_31_5, float* %C_addr_6, align 4
  %Q_addr_6 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_5
  %Q_load_65 = load float* %Q_addr_6, align 4
  %tmp_32_5 = fsub float %Q_load_65, %temp_4_5
  store float %tmp_32_5, float* %Q_addr_6, align 4
  %c_6 = add i12 %phi_mul, 6
  %Q_load_66 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2556), align 16
  %tmp_21_6 = fmul float %Q_load_63, %Q_load_66
  %temp_4_6 = fdiv float %tmp_21_6, %qStar
  %C_load_66 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2556), align 16
  %tmp_22_6 = fmul float %C_load_63, %C_load_66
  %tmp_23_6 = fmul float %C_load_63, %Q_load_66
  %tmp_24_6 = fadd float %tmp_22_6, %tmp_23_6
  %tmp_25_6 = fmul float %Q_load_63, %C_load_66
  %tmp_26_6 = fadd float %tmp_24_6, %tmp_25_6
  %tmp_27_6 = fadd float %tmp_26_6, %tmp_21_6
  %tmp_28_6 = fdiv float %tmp_27_6, %tmp_5
  %tmp_29_6 = fsub float %temp_4_6, %tmp_28_6
  %tmp_30_6 = zext i12 %c_6 to i64
  %C_addr_7 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_6
  %C_load_67 = load float* %C_addr_7, align 4
  %tmp_31_6 = fadd float %C_load_67, %tmp_29_6
  store float %tmp_31_6, float* %C_addr_7, align 4
  %Q_addr_7 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_6
  %Q_load_67 = load float* %Q_addr_7, align 4
  %tmp_32_6 = fsub float %Q_load_67, %temp_4_6
  store float %tmp_32_6, float* %Q_addr_7, align 4
  %c_7 = add i12 %phi_mul, 7
  %Q_load_68 = load float* %Q_addr, align 4
  %Q_load_69 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2557), align 4
  %tmp_21_7 = fmul float %Q_load_68, %Q_load_69
  %temp_4_7 = fdiv float %tmp_21_7, %qStar
  %C_load_68 = load float* %C_addr, align 4
  %C_load_69 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2557), align 4
  %tmp_22_7 = fmul float %C_load_68, %C_load_69
  %tmp_23_7 = fmul float %C_load_68, %Q_load_69
  %tmp_24_7 = fadd float %tmp_22_7, %tmp_23_7
  %tmp_25_7 = fmul float %Q_load_68, %C_load_69
  %tmp_26_7 = fadd float %tmp_24_7, %tmp_25_7
  %tmp_27_7 = fadd float %tmp_26_7, %tmp_21_7
  %tmp_28_7 = fdiv float %tmp_27_7, %tmp_5
  %tmp_29_7 = fsub float %temp_4_7, %tmp_28_7
  %tmp_30_7 = zext i12 %c_7 to i64
  %C_addr_8 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_7
  %C_load_70 = load float* %C_addr_8, align 4
  %tmp_31_7 = fadd float %C_load_70, %tmp_29_7
  store float %tmp_31_7, float* %C_addr_8, align 4
  %Q_addr_8 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_7
  %Q_load_70 = load float* %Q_addr_8, align 4
  %tmp_32_7 = fsub float %Q_load_70, %temp_4_7
  store float %tmp_32_7, float* %Q_addr_8, align 4
  %c_8 = add i12 %phi_mul, 8
  %Q_load_71 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2558), align 8
  %tmp_21_8 = fmul float %Q_load_68, %Q_load_71
  %temp_4_8 = fdiv float %tmp_21_8, %qStar
  %C_load_71 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2558), align 8
  %tmp_22_8 = fmul float %C_load_68, %C_load_71
  %tmp_23_8 = fmul float %C_load_68, %Q_load_71
  %tmp_24_8 = fadd float %tmp_22_8, %tmp_23_8
  %tmp_25_8 = fmul float %Q_load_68, %C_load_71
  %tmp_26_8 = fadd float %tmp_24_8, %tmp_25_8
  %tmp_27_8 = fadd float %tmp_26_8, %tmp_21_8
  %tmp_28_8 = fdiv float %tmp_27_8, %tmp_5
  %tmp_29_8 = fsub float %temp_4_8, %tmp_28_8
  %tmp_30_8 = zext i12 %c_8 to i64
  %C_addr_9 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_8
  %C_load_72 = load float* %C_addr_9, align 4
  %tmp_31_8 = fadd float %C_load_72, %tmp_29_8
  store float %tmp_31_8, float* %C_addr_9, align 4
  %Q_addr_9 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_8
  %Q_load_72 = load float* %Q_addr_9, align 4
  %tmp_32_8 = fsub float %Q_load_72, %temp_4_8
  store float %tmp_32_8, float* %Q_addr_9, align 4
  %c_9 = add i12 %phi_mul, 9
  %Q_load_73 = load float* %Q_addr, align 4
  %Q_load_74 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2559), align 4
  %tmp_21_9 = fmul float %Q_load_73, %Q_load_74
  %temp_4_9 = fdiv float %tmp_21_9, %qStar
  %C_load_73 = load float* %C_addr, align 4
  %C_load_74 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2559), align 4
  %tmp_22_9 = fmul float %C_load_73, %C_load_74
  %tmp_23_9 = fmul float %C_load_73, %Q_load_74
  %tmp_24_9 = fadd float %tmp_22_9, %tmp_23_9
  %tmp_25_9 = fmul float %Q_load_73, %C_load_74
  %tmp_26_9 = fadd float %tmp_24_9, %tmp_25_9
  %tmp_27_9 = fadd float %tmp_26_9, %tmp_21_9
  %tmp_28_9 = fdiv float %tmp_27_9, %tmp_5
  %tmp_29_9 = fsub float %temp_4_9, %tmp_28_9
  %tmp_30_9 = zext i12 %c_9 to i64
  %C_addr_10 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_9
  %C_load_75 = load float* %C_addr_10, align 4
  %tmp_31_9 = fadd float %C_load_75, %tmp_29_9
  store float %tmp_31_9, float* %C_addr_10, align 4
  %Q_addr_10 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_9
  %Q_load_75 = load float* %Q_addr_10, align 4
  %tmp_32_9 = fsub float %Q_load_75, %temp_4_9
  store float %tmp_32_9, float* %Q_addr_10, align 4
  %c_s = add i12 %phi_mul, 10
  %Q_load_76 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2560), align 16
  %tmp_21_s = fmul float %Q_load_73, %Q_load_76
  %temp_4_s = fdiv float %tmp_21_s, %qStar
  %C_load_76 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2560), align 16
  %tmp_22_s = fmul float %C_load_73, %C_load_76
  %tmp_23_s = fmul float %C_load_73, %Q_load_76
  %tmp_24_s = fadd float %tmp_22_s, %tmp_23_s
  %tmp_25_s = fmul float %Q_load_73, %C_load_76
  %tmp_26_s = fadd float %tmp_24_s, %tmp_25_s
  %tmp_27_s = fadd float %tmp_26_s, %tmp_21_s
  %tmp_28_s = fdiv float %tmp_27_s, %tmp_5
  %tmp_29_s = fsub float %temp_4_s, %tmp_28_s
  %tmp_30_s = zext i12 %c_s to i64
  %C_addr_11 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_s
  %C_load_77 = load float* %C_addr_11, align 4
  %tmp_31_s = fadd float %C_load_77, %tmp_29_s
  store float %tmp_31_s, float* %C_addr_11, align 4
  %Q_addr_11 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_s
  %Q_load_77 = load float* %Q_addr_11, align 4
  %tmp_32_s = fsub float %Q_load_77, %temp_4_s
  store float %tmp_32_s, float* %Q_addr_11, align 4
  %c_10 = add i12 %phi_mul, 11
  %Q_load_78 = load float* %Q_addr, align 4
  %Q_load_79 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2561), align 4
  %tmp_21_10 = fmul float %Q_load_78, %Q_load_79
  %temp_4_10 = fdiv float %tmp_21_10, %qStar
  %C_load_78 = load float* %C_addr, align 4
  %C_load_79 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2561), align 4
  %tmp_22_10 = fmul float %C_load_78, %C_load_79
  %tmp_23_10 = fmul float %C_load_78, %Q_load_79
  %tmp_24_10 = fadd float %tmp_22_10, %tmp_23_10
  %tmp_25_10 = fmul float %Q_load_78, %C_load_79
  %tmp_26_10 = fadd float %tmp_24_10, %tmp_25_10
  %tmp_27_10 = fadd float %tmp_26_10, %tmp_21_10
  %tmp_28_10 = fdiv float %tmp_27_10, %tmp_5
  %tmp_29_10 = fsub float %temp_4_10, %tmp_28_10
  %tmp_30_10 = zext i12 %c_10 to i64
  %C_addr_12 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_10
  %C_load_80 = load float* %C_addr_12, align 4
  %tmp_31_10 = fadd float %C_load_80, %tmp_29_10
  store float %tmp_31_10, float* %C_addr_12, align 4
  %Q_addr_12 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_10
  %Q_load_80 = load float* %Q_addr_12, align 4
  %tmp_32_10 = fsub float %Q_load_80, %temp_4_10
  store float %tmp_32_10, float* %Q_addr_12, align 4
  %c_11 = add i12 %phi_mul, 12
  %Q_load_81 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2562), align 8
  %tmp_21_11 = fmul float %Q_load_78, %Q_load_81
  %temp_4_11 = fdiv float %tmp_21_11, %qStar
  %C_load_81 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2562), align 8
  %tmp_22_11 = fmul float %C_load_78, %C_load_81
  %tmp_23_11 = fmul float %C_load_78, %Q_load_81
  %tmp_24_11 = fadd float %tmp_22_11, %tmp_23_11
  %tmp_25_11 = fmul float %Q_load_78, %C_load_81
  %tmp_26_11 = fadd float %tmp_24_11, %tmp_25_11
  %tmp_27_11 = fadd float %tmp_26_11, %tmp_21_11
  %tmp_28_11 = fdiv float %tmp_27_11, %tmp_5
  %tmp_29_11 = fsub float %temp_4_11, %tmp_28_11
  %tmp_30_11 = zext i12 %c_11 to i64
  %C_addr_13 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_11
  %C_load_82 = load float* %C_addr_13, align 4
  %tmp_31_11 = fadd float %C_load_82, %tmp_29_11
  store float %tmp_31_11, float* %C_addr_13, align 4
  %Q_addr_13 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_11
  %Q_load_82 = load float* %Q_addr_13, align 4
  %tmp_32_11 = fsub float %Q_load_82, %temp_4_11
  store float %tmp_32_11, float* %Q_addr_13, align 4
  %c_12 = add i12 %phi_mul, 13
  %Q_load_83 = load float* %Q_addr, align 4
  %Q_load_84 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2563), align 4
  %tmp_21_12 = fmul float %Q_load_83, %Q_load_84
  %temp_4_12 = fdiv float %tmp_21_12, %qStar
  %C_load_83 = load float* %C_addr, align 4
  %C_load_84 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2563), align 4
  %tmp_22_12 = fmul float %C_load_83, %C_load_84
  %tmp_23_12 = fmul float %C_load_83, %Q_load_84
  %tmp_24_12 = fadd float %tmp_22_12, %tmp_23_12
  %tmp_25_12 = fmul float %Q_load_83, %C_load_84
  %tmp_26_12 = fadd float %tmp_24_12, %tmp_25_12
  %tmp_27_12 = fadd float %tmp_26_12, %tmp_21_12
  %tmp_28_12 = fdiv float %tmp_27_12, %tmp_5
  %tmp_29_12 = fsub float %temp_4_12, %tmp_28_12
  %tmp_30_12 = zext i12 %c_12 to i64
  %C_addr_14 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_12
  %C_load_85 = load float* %C_addr_14, align 4
  %tmp_31_12 = fadd float %C_load_85, %tmp_29_12
  store float %tmp_31_12, float* %C_addr_14, align 4
  %Q_addr_14 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_12
  %Q_load_85 = load float* %Q_addr_14, align 4
  %tmp_32_12 = fsub float %Q_load_85, %temp_4_12
  store float %tmp_32_12, float* %Q_addr_14, align 4
  %c_13 = add i12 %phi_mul, 14
  %Q_load_86 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2564), align 16
  %tmp_21_13 = fmul float %Q_load_83, %Q_load_86
  %temp_4_13 = fdiv float %tmp_21_13, %qStar
  %C_load_86 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2564), align 16
  %tmp_22_13 = fmul float %C_load_83, %C_load_86
  %tmp_23_13 = fmul float %C_load_83, %Q_load_86
  %tmp_24_13 = fadd float %tmp_22_13, %tmp_23_13
  %tmp_25_13 = fmul float %Q_load_83, %C_load_86
  %tmp_26_13 = fadd float %tmp_24_13, %tmp_25_13
  %tmp_27_13 = fadd float %tmp_26_13, %tmp_21_13
  %tmp_28_13 = fdiv float %tmp_27_13, %tmp_5
  %tmp_29_13 = fsub float %temp_4_13, %tmp_28_13
  %tmp_30_13 = zext i12 %c_13 to i64
  %C_addr_15 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_13
  %C_load_87 = load float* %C_addr_15, align 4
  %tmp_31_13 = fadd float %C_load_87, %tmp_29_13
  store float %tmp_31_13, float* %C_addr_15, align 4
  %Q_addr_15 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_13
  %Q_load_87 = load float* %Q_addr_15, align 4
  %tmp_32_13 = fsub float %Q_load_87, %temp_4_13
  store float %tmp_32_13, float* %Q_addr_15, align 4
  %c_14 = add i12 %phi_mul, 15
  %Q_load_88 = load float* %Q_addr, align 4
  %Q_load_89 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2565), align 4
  %tmp_21_14 = fmul float %Q_load_88, %Q_load_89
  %temp_4_14 = fdiv float %tmp_21_14, %qStar
  %C_load_88 = load float* %C_addr, align 4
  %C_load_89 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2565), align 4
  %tmp_22_14 = fmul float %C_load_88, %C_load_89
  %tmp_23_14 = fmul float %C_load_88, %Q_load_89
  %tmp_24_14 = fadd float %tmp_22_14, %tmp_23_14
  %tmp_25_14 = fmul float %Q_load_88, %C_load_89
  %tmp_26_14 = fadd float %tmp_24_14, %tmp_25_14
  %tmp_27_14 = fadd float %tmp_26_14, %tmp_21_14
  %tmp_28_14 = fdiv float %tmp_27_14, %tmp_5
  %tmp_29_14 = fsub float %temp_4_14, %tmp_28_14
  %tmp_30_14 = zext i12 %c_14 to i64
  %C_addr_16 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_14
  %C_load_90 = load float* %C_addr_16, align 4
  %tmp_31_14 = fadd float %C_load_90, %tmp_29_14
  store float %tmp_31_14, float* %C_addr_16, align 4
  %Q_addr_16 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_14
  %Q_load_90 = load float* %Q_addr_16, align 4
  %tmp_32_14 = fsub float %Q_load_90, %temp_4_14
  store float %tmp_32_14, float* %Q_addr_16, align 4
  %c_15 = add i12 %phi_mul, 16
  %Q_load_91 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2566), align 8
  %tmp_21_15 = fmul float %Q_load_88, %Q_load_91
  %temp_4_15 = fdiv float %tmp_21_15, %qStar
  %C_load_91 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2566), align 8
  %tmp_22_15 = fmul float %C_load_88, %C_load_91
  %tmp_23_15 = fmul float %C_load_88, %Q_load_91
  %tmp_24_15 = fadd float %tmp_22_15, %tmp_23_15
  %tmp_25_15 = fmul float %Q_load_88, %C_load_91
  %tmp_26_15 = fadd float %tmp_24_15, %tmp_25_15
  %tmp_27_15 = fadd float %tmp_26_15, %tmp_21_15
  %tmp_28_15 = fdiv float %tmp_27_15, %tmp_5
  %tmp_29_15 = fsub float %temp_4_15, %tmp_28_15
  %tmp_30_15 = zext i12 %c_15 to i64
  %C_addr_17 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_15
  %C_load_92 = load float* %C_addr_17, align 4
  %tmp_31_15 = fadd float %C_load_92, %tmp_29_15
  store float %tmp_31_15, float* %C_addr_17, align 4
  %Q_addr_17 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_15
  %Q_load_92 = load float* %Q_addr_17, align 4
  %tmp_32_15 = fsub float %Q_load_92, %temp_4_15
  store float %tmp_32_15, float* %Q_addr_17, align 4
  %c_16 = add i12 %phi_mul, 17
  %Q_load_93 = load float* %Q_addr, align 4
  %Q_load_94 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2567), align 4
  %tmp_21_16 = fmul float %Q_load_93, %Q_load_94
  %temp_4_16 = fdiv float %tmp_21_16, %qStar
  %C_load_93 = load float* %C_addr, align 4
  %C_load_94 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2567), align 4
  %tmp_22_16 = fmul float %C_load_93, %C_load_94
  %tmp_23_16 = fmul float %C_load_93, %Q_load_94
  %tmp_24_16 = fadd float %tmp_22_16, %tmp_23_16
  %tmp_25_16 = fmul float %Q_load_93, %C_load_94
  %tmp_26_16 = fadd float %tmp_24_16, %tmp_25_16
  %tmp_27_16 = fadd float %tmp_26_16, %tmp_21_16
  %tmp_28_16 = fdiv float %tmp_27_16, %tmp_5
  %tmp_29_16 = fsub float %temp_4_16, %tmp_28_16
  %tmp_30_16 = zext i12 %c_16 to i64
  %C_addr_18 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_16
  %C_load_95 = load float* %C_addr_18, align 4
  %tmp_31_16 = fadd float %C_load_95, %tmp_29_16
  store float %tmp_31_16, float* %C_addr_18, align 4
  %Q_addr_18 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_16
  %Q_load_95 = load float* %Q_addr_18, align 4
  %tmp_32_16 = fsub float %Q_load_95, %temp_4_16
  store float %tmp_32_16, float* %Q_addr_18, align 4
  %c_17 = add i12 %phi_mul, 18
  %Q_load_96 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2568), align 16
  %tmp_21_17 = fmul float %Q_load_93, %Q_load_96
  %temp_4_17 = fdiv float %tmp_21_17, %qStar
  %C_load_96 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2568), align 16
  %tmp_22_17 = fmul float %C_load_93, %C_load_96
  %tmp_23_17 = fmul float %C_load_93, %Q_load_96
  %tmp_24_17 = fadd float %tmp_22_17, %tmp_23_17
  %tmp_25_17 = fmul float %Q_load_93, %C_load_96
  %tmp_26_17 = fadd float %tmp_24_17, %tmp_25_17
  %tmp_27_17 = fadd float %tmp_26_17, %tmp_21_17
  %tmp_28_17 = fdiv float %tmp_27_17, %tmp_5
  %tmp_29_17 = fsub float %temp_4_17, %tmp_28_17
  %tmp_30_17 = zext i12 %c_17 to i64
  %C_addr_19 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_17
  %C_load_97 = load float* %C_addr_19, align 4
  %tmp_31_17 = fadd float %C_load_97, %tmp_29_17
  store float %tmp_31_17, float* %C_addr_19, align 4
  %Q_addr_19 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_17
  %Q_load_97 = load float* %Q_addr_19, align 4
  %tmp_32_17 = fsub float %Q_load_97, %temp_4_17
  store float %tmp_32_17, float* %Q_addr_19, align 4
  %c_18 = add i12 %phi_mul, 19
  %Q_load_98 = load float* %Q_addr, align 4
  %Q_load_99 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2569), align 4
  %tmp_21_18 = fmul float %Q_load_98, %Q_load_99
  %temp_4_18 = fdiv float %tmp_21_18, %qStar
  %C_load_98 = load float* %C_addr, align 4
  %C_load_99 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2569), align 4
  %tmp_22_18 = fmul float %C_load_98, %C_load_99
  %tmp_23_18 = fmul float %C_load_98, %Q_load_99
  %tmp_24_18 = fadd float %tmp_22_18, %tmp_23_18
  %tmp_25_18 = fmul float %Q_load_98, %C_load_99
  %tmp_26_18 = fadd float %tmp_24_18, %tmp_25_18
  %tmp_27_18 = fadd float %tmp_26_18, %tmp_21_18
  %tmp_28_18 = fdiv float %tmp_27_18, %tmp_5
  %tmp_29_18 = fsub float %temp_4_18, %tmp_28_18
  %tmp_30_18 = zext i12 %c_18 to i64
  %C_addr_20 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_18
  %C_load_100 = load float* %C_addr_20, align 4
  %tmp_31_18 = fadd float %C_load_100, %tmp_29_18
  store float %tmp_31_18, float* %C_addr_20, align 4
  %Q_addr_20 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_18
  %Q_load_100 = load float* %Q_addr_20, align 4
  %tmp_32_18 = fsub float %Q_load_100, %temp_4_18
  store float %tmp_32_18, float* %Q_addr_20, align 4
  %c_19 = add i12 %phi_mul, 20
  %Q_load_101 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2570), align 8
  %tmp_21_19 = fmul float %Q_load_98, %Q_load_101
  %temp_4_19 = fdiv float %tmp_21_19, %qStar
  %C_load_101 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2570), align 8
  %tmp_22_19 = fmul float %C_load_98, %C_load_101
  %tmp_23_19 = fmul float %C_load_98, %Q_load_101
  %tmp_24_19 = fadd float %tmp_22_19, %tmp_23_19
  %tmp_25_19 = fmul float %Q_load_98, %C_load_101
  %tmp_26_19 = fadd float %tmp_24_19, %tmp_25_19
  %tmp_27_19 = fadd float %tmp_26_19, %tmp_21_19
  %tmp_28_19 = fdiv float %tmp_27_19, %tmp_5
  %tmp_29_19 = fsub float %temp_4_19, %tmp_28_19
  %tmp_30_19 = zext i12 %c_19 to i64
  %C_addr_21 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_19
  %C_load_102 = load float* %C_addr_21, align 4
  %tmp_31_19 = fadd float %C_load_102, %tmp_29_19
  store float %tmp_31_19, float* %C_addr_21, align 4
  %Q_addr_21 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_19
  %Q_load_102 = load float* %Q_addr_21, align 4
  %tmp_32_19 = fsub float %Q_load_102, %temp_4_19
  store float %tmp_32_19, float* %Q_addr_21, align 4
  %c_20 = add i12 %phi_mul, 21
  %Q_load_103 = load float* %Q_addr, align 4
  %Q_load_104 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2571), align 4
  %tmp_21_20 = fmul float %Q_load_103, %Q_load_104
  %temp_4_20 = fdiv float %tmp_21_20, %qStar
  %C_load_103 = load float* %C_addr, align 4
  %C_load_104 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2571), align 4
  %tmp_22_20 = fmul float %C_load_103, %C_load_104
  %tmp_23_20 = fmul float %C_load_103, %Q_load_104
  %tmp_24_20 = fadd float %tmp_22_20, %tmp_23_20
  %tmp_25_20 = fmul float %Q_load_103, %C_load_104
  %tmp_26_20 = fadd float %tmp_24_20, %tmp_25_20
  %tmp_27_20 = fadd float %tmp_26_20, %tmp_21_20
  %tmp_28_20 = fdiv float %tmp_27_20, %tmp_5
  %tmp_29_20 = fsub float %temp_4_20, %tmp_28_20
  %tmp_30_20 = zext i12 %c_20 to i64
  %C_addr_22 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_20
  %C_load_105 = load float* %C_addr_22, align 4
  %tmp_31_20 = fadd float %C_load_105, %tmp_29_20
  store float %tmp_31_20, float* %C_addr_22, align 4
  %Q_addr_22 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_20
  %Q_load_105 = load float* %Q_addr_22, align 4
  %tmp_32_20 = fsub float %Q_load_105, %temp_4_20
  store float %tmp_32_20, float* %Q_addr_22, align 4
  %c_21 = add i12 %phi_mul, 22
  %Q_load_106 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2572), align 16
  %tmp_21_21 = fmul float %Q_load_103, %Q_load_106
  %temp_4_21 = fdiv float %tmp_21_21, %qStar
  %C_load_106 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2572), align 16
  %tmp_22_21 = fmul float %C_load_103, %C_load_106
  %tmp_23_21 = fmul float %C_load_103, %Q_load_106
  %tmp_24_21 = fadd float %tmp_22_21, %tmp_23_21
  %tmp_25_21 = fmul float %Q_load_103, %C_load_106
  %tmp_26_21 = fadd float %tmp_24_21, %tmp_25_21
  %tmp_27_21 = fadd float %tmp_26_21, %tmp_21_21
  %tmp_28_21 = fdiv float %tmp_27_21, %tmp_5
  %tmp_29_21 = fsub float %temp_4_21, %tmp_28_21
  %tmp_30_21 = zext i12 %c_21 to i64
  %C_addr_23 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_21
  %C_load_107 = load float* %C_addr_23, align 4
  %tmp_31_21 = fadd float %C_load_107, %tmp_29_21
  store float %tmp_31_21, float* %C_addr_23, align 4
  %Q_addr_23 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_21
  %Q_load_107 = load float* %Q_addr_23, align 4
  %tmp_32_21 = fsub float %Q_load_107, %temp_4_21
  store float %tmp_32_21, float* %Q_addr_23, align 4
  %c_22 = add i12 %phi_mul, 23
  %Q_load_108 = load float* %Q_addr, align 4
  %Q_load_109 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2573), align 4
  %tmp_21_22 = fmul float %Q_load_108, %Q_load_109
  %temp_4_22 = fdiv float %tmp_21_22, %qStar
  %C_load_108 = load float* %C_addr, align 4
  %C_load_109 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2573), align 4
  %tmp_22_22 = fmul float %C_load_108, %C_load_109
  %tmp_23_22 = fmul float %C_load_108, %Q_load_109
  %tmp_24_22 = fadd float %tmp_22_22, %tmp_23_22
  %tmp_25_22 = fmul float %Q_load_108, %C_load_109
  %tmp_26_22 = fadd float %tmp_24_22, %tmp_25_22
  %tmp_27_22 = fadd float %tmp_26_22, %tmp_21_22
  %tmp_28_22 = fdiv float %tmp_27_22, %tmp_5
  %tmp_29_22 = fsub float %temp_4_22, %tmp_28_22
  %tmp_30_22 = zext i12 %c_22 to i64
  %C_addr_24 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_22
  %C_load_110 = load float* %C_addr_24, align 4
  %tmp_31_22 = fadd float %C_load_110, %tmp_29_22
  store float %tmp_31_22, float* %C_addr_24, align 4
  %Q_addr_24 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_22
  %Q_load_110 = load float* %Q_addr_24, align 4
  %tmp_32_22 = fsub float %Q_load_110, %temp_4_22
  store float %tmp_32_22, float* %Q_addr_24, align 4
  %c_23 = add i12 %phi_mul, 24
  %Q_load_111 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2574), align 8
  %tmp_21_23 = fmul float %Q_load_108, %Q_load_111
  %temp_4_23 = fdiv float %tmp_21_23, %qStar
  %C_load_111 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2574), align 8
  %tmp_22_23 = fmul float %C_load_108, %C_load_111
  %tmp_23_23 = fmul float %C_load_108, %Q_load_111
  %tmp_24_23 = fadd float %tmp_22_23, %tmp_23_23
  %tmp_25_23 = fmul float %Q_load_108, %C_load_111
  %tmp_26_23 = fadd float %tmp_24_23, %tmp_25_23
  %tmp_27_23 = fadd float %tmp_26_23, %tmp_21_23
  %tmp_28_23 = fdiv float %tmp_27_23, %tmp_5
  %tmp_29_23 = fsub float %temp_4_23, %tmp_28_23
  %tmp_30_23 = zext i12 %c_23 to i64
  %C_addr_25 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_23
  %C_load_112 = load float* %C_addr_25, align 4
  %tmp_31_23 = fadd float %C_load_112, %tmp_29_23
  store float %tmp_31_23, float* %C_addr_25, align 4
  %Q_addr_25 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_23
  %Q_load_112 = load float* %Q_addr_25, align 4
  %tmp_32_23 = fsub float %Q_load_112, %temp_4_23
  store float %tmp_32_23, float* %Q_addr_25, align 4
  %c_24 = add i12 %phi_mul, 25
  %Q_load_113 = load float* %Q_addr, align 4
  %Q_load_114 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2575), align 4
  %tmp_21_24 = fmul float %Q_load_113, %Q_load_114
  %temp_4_24 = fdiv float %tmp_21_24, %qStar
  %C_load_113 = load float* %C_addr, align 4
  %C_load_114 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2575), align 4
  %tmp_22_24 = fmul float %C_load_113, %C_load_114
  %tmp_23_24 = fmul float %C_load_113, %Q_load_114
  %tmp_24_24 = fadd float %tmp_22_24, %tmp_23_24
  %tmp_25_24 = fmul float %Q_load_113, %C_load_114
  %tmp_26_24 = fadd float %tmp_24_24, %tmp_25_24
  %tmp_27_24 = fadd float %tmp_26_24, %tmp_21_24
  %tmp_28_24 = fdiv float %tmp_27_24, %tmp_5
  %tmp_29_24 = fsub float %temp_4_24, %tmp_28_24
  %tmp_30_24 = zext i12 %c_24 to i64
  %C_addr_26 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_24
  %C_load_115 = load float* %C_addr_26, align 4
  %tmp_31_24 = fadd float %C_load_115, %tmp_29_24
  store float %tmp_31_24, float* %C_addr_26, align 4
  %Q_addr_26 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_24
  %Q_load_115 = load float* %Q_addr_26, align 4
  %tmp_32_24 = fsub float %Q_load_115, %temp_4_24
  store float %tmp_32_24, float* %Q_addr_26, align 4
  %c_25 = add i12 %phi_mul, 26
  %Q_load_116 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2576), align 16
  %tmp_21_25 = fmul float %Q_load_113, %Q_load_116
  %temp_4_25 = fdiv float %tmp_21_25, %qStar
  %C_load_116 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2576), align 16
  %tmp_22_25 = fmul float %C_load_113, %C_load_116
  %tmp_23_25 = fmul float %C_load_113, %Q_load_116
  %tmp_24_25 = fadd float %tmp_22_25, %tmp_23_25
  %tmp_25_25 = fmul float %Q_load_113, %C_load_116
  %tmp_26_25 = fadd float %tmp_24_25, %tmp_25_25
  %tmp_27_25 = fadd float %tmp_26_25, %tmp_21_25
  %tmp_28_25 = fdiv float %tmp_27_25, %tmp_5
  %tmp_29_25 = fsub float %temp_4_25, %tmp_28_25
  %tmp_30_25 = zext i12 %c_25 to i64
  %C_addr_27 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_25
  %C_load_117 = load float* %C_addr_27, align 4
  %tmp_31_25 = fadd float %C_load_117, %tmp_29_25
  store float %tmp_31_25, float* %C_addr_27, align 4
  %Q_addr_27 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_25
  %Q_load_117 = load float* %Q_addr_27, align 4
  %tmp_32_25 = fsub float %Q_load_117, %temp_4_25
  store float %tmp_32_25, float* %Q_addr_27, align 4
  %c_26 = add i12 %phi_mul, 27
  %Q_load_118 = load float* %Q_addr, align 4
  %Q_load_119 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2577), align 4
  %tmp_21_26 = fmul float %Q_load_118, %Q_load_119
  %temp_4_26 = fdiv float %tmp_21_26, %qStar
  %C_load_118 = load float* %C_addr, align 4
  %C_load_119 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2577), align 4
  %tmp_22_26 = fmul float %C_load_118, %C_load_119
  %tmp_23_26 = fmul float %C_load_118, %Q_load_119
  %tmp_24_26 = fadd float %tmp_22_26, %tmp_23_26
  %tmp_25_26 = fmul float %Q_load_118, %C_load_119
  %tmp_26_26 = fadd float %tmp_24_26, %tmp_25_26
  %tmp_27_26 = fadd float %tmp_26_26, %tmp_21_26
  %tmp_28_26 = fdiv float %tmp_27_26, %tmp_5
  %tmp_29_26 = fsub float %temp_4_26, %tmp_28_26
  %tmp_30_26 = zext i12 %c_26 to i64
  %C_addr_28 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_26
  %C_load_120 = load float* %C_addr_28, align 4
  %tmp_31_26 = fadd float %C_load_120, %tmp_29_26
  store float %tmp_31_26, float* %C_addr_28, align 4
  %Q_addr_28 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_26
  %Q_load_120 = load float* %Q_addr_28, align 4
  %tmp_32_26 = fsub float %Q_load_120, %temp_4_26
  store float %tmp_32_26, float* %Q_addr_28, align 4
  %c_27 = add i12 %phi_mul, 28
  %Q_load_121 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2578), align 8
  %tmp_21_27 = fmul float %Q_load_118, %Q_load_121
  %temp_4_27 = fdiv float %tmp_21_27, %qStar
  %C_load_121 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2578), align 8
  %tmp_22_27 = fmul float %C_load_118, %C_load_121
  %tmp_23_27 = fmul float %C_load_118, %Q_load_121
  %tmp_24_27 = fadd float %tmp_22_27, %tmp_23_27
  %tmp_25_27 = fmul float %Q_load_118, %C_load_121
  %tmp_26_27 = fadd float %tmp_24_27, %tmp_25_27
  %tmp_27_27 = fadd float %tmp_26_27, %tmp_21_27
  %tmp_28_27 = fdiv float %tmp_27_27, %tmp_5
  %tmp_29_27 = fsub float %temp_4_27, %tmp_28_27
  %tmp_30_27 = zext i12 %c_27 to i64
  %C_addr_29 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_27
  %C_load_122 = load float* %C_addr_29, align 4
  %tmp_31_27 = fadd float %C_load_122, %tmp_29_27
  store float %tmp_31_27, float* %C_addr_29, align 4
  %Q_addr_29 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_27
  %Q_load_122 = load float* %Q_addr_29, align 4
  %tmp_32_27 = fsub float %Q_load_122, %temp_4_27
  store float %tmp_32_27, float* %Q_addr_29, align 4
  %c_28 = add i12 %phi_mul, 29
  %Q_load_123 = load float* %Q_addr, align 4
  %Q_load_124 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2579), align 4
  %tmp_21_28 = fmul float %Q_load_123, %Q_load_124
  %temp_4_28 = fdiv float %tmp_21_28, %qStar
  %C_load_123 = load float* %C_addr, align 4
  %C_load_124 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2579), align 4
  %tmp_22_28 = fmul float %C_load_123, %C_load_124
  %tmp_23_28 = fmul float %C_load_123, %Q_load_124
  %tmp_24_28 = fadd float %tmp_22_28, %tmp_23_28
  %tmp_25_28 = fmul float %Q_load_123, %C_load_124
  %tmp_26_28 = fadd float %tmp_24_28, %tmp_25_28
  %tmp_27_28 = fadd float %tmp_26_28, %tmp_21_28
  %tmp_28_28 = fdiv float %tmp_27_28, %tmp_5
  %tmp_29_28 = fsub float %temp_4_28, %tmp_28_28
  %tmp_30_28 = zext i12 %c_28 to i64
  %C_addr_30 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_28
  %C_load_125 = load float* %C_addr_30, align 4
  %tmp_31_28 = fadd float %C_load_125, %tmp_29_28
  store float %tmp_31_28, float* %C_addr_30, align 4
  %Q_addr_30 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_28
  %Q_load_125 = load float* %Q_addr_30, align 4
  %tmp_32_28 = fsub float %Q_load_125, %temp_4_28
  store float %tmp_32_28, float* %Q_addr_30, align 4
  %c_29 = add i12 %phi_mul, 30
  %Q_load_126 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2580), align 16
  %tmp_21_29 = fmul float %Q_load_123, %Q_load_126
  %temp_4_29 = fdiv float %tmp_21_29, %qStar
  %C_load_126 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2580), align 16
  %tmp_22_29 = fmul float %C_load_123, %C_load_126
  %tmp_23_29 = fmul float %C_load_123, %Q_load_126
  %tmp_24_29 = fadd float %tmp_22_29, %tmp_23_29
  %tmp_25_29 = fmul float %Q_load_123, %C_load_126
  %tmp_26_29 = fadd float %tmp_24_29, %tmp_25_29
  %tmp_27_29 = fadd float %tmp_26_29, %tmp_21_29
  %tmp_28_29 = fdiv float %tmp_27_29, %tmp_5
  %tmp_29_29 = fsub float %temp_4_29, %tmp_28_29
  %tmp_30_29 = zext i12 %c_29 to i64
  %C_addr_31 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_29
  %C_load_127 = load float* %C_addr_31, align 4
  %tmp_31_29 = fadd float %C_load_127, %tmp_29_29
  store float %tmp_31_29, float* %C_addr_31, align 4
  %Q_addr_31 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_29
  %Q_load_127 = load float* %Q_addr_31, align 4
  %tmp_32_29 = fsub float %Q_load_127, %temp_4_29
  store float %tmp_32_29, float* %Q_addr_31, align 4
  %c_30 = add i12 %phi_mul, 31
  %Q_load_128 = load float* %Q_addr, align 4
  %Q_load_129 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2581), align 4
  %tmp_21_30 = fmul float %Q_load_128, %Q_load_129
  %temp_4_30 = fdiv float %tmp_21_30, %qStar
  %C_load_128 = load float* %C_addr, align 4
  %C_load_129 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2581), align 4
  %tmp_22_30 = fmul float %C_load_128, %C_load_129
  %tmp_23_30 = fmul float %C_load_128, %Q_load_129
  %tmp_24_30 = fadd float %tmp_22_30, %tmp_23_30
  %tmp_25_30 = fmul float %Q_load_128, %C_load_129
  %tmp_26_30 = fadd float %tmp_24_30, %tmp_25_30
  %tmp_27_30 = fadd float %tmp_26_30, %tmp_21_30
  %tmp_28_30 = fdiv float %tmp_27_30, %tmp_5
  %tmp_29_30 = fsub float %temp_4_30, %tmp_28_30
  %tmp_30_30 = zext i12 %c_30 to i64
  %C_addr_32 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_30
  %C_load_130 = load float* %C_addr_32, align 4
  %tmp_31_30 = fadd float %C_load_130, %tmp_29_30
  store float %tmp_31_30, float* %C_addr_32, align 4
  %Q_addr_32 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_30
  %Q_load_130 = load float* %Q_addr_32, align 4
  %tmp_32_30 = fsub float %Q_load_130, %temp_4_30
  store float %tmp_32_30, float* %Q_addr_32, align 4
  %c_31 = add i12 %phi_mul, 32
  %Q_load_131 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2582), align 8
  %tmp_21_31 = fmul float %Q_load_128, %Q_load_131
  %temp_4_31 = fdiv float %tmp_21_31, %qStar
  %C_load_131 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2582), align 8
  %tmp_22_31 = fmul float %C_load_128, %C_load_131
  %tmp_23_31 = fmul float %C_load_128, %Q_load_131
  %tmp_24_31 = fadd float %tmp_22_31, %tmp_23_31
  %tmp_25_31 = fmul float %Q_load_128, %C_load_131
  %tmp_26_31 = fadd float %tmp_24_31, %tmp_25_31
  %tmp_27_31 = fadd float %tmp_26_31, %tmp_21_31
  %tmp_28_31 = fdiv float %tmp_27_31, %tmp_5
  %tmp_29_31 = fsub float %temp_4_31, %tmp_28_31
  %tmp_30_31 = zext i12 %c_31 to i64
  %C_addr_33 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_31
  %C_load_132 = load float* %C_addr_33, align 4
  %tmp_31_31 = fadd float %C_load_132, %tmp_29_31
  store float %tmp_31_31, float* %C_addr_33, align 4
  %Q_addr_33 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_31
  %Q_load_132 = load float* %Q_addr_33, align 4
  %tmp_32_31 = fsub float %Q_load_132, %temp_4_31
  store float %tmp_32_31, float* %Q_addr_33, align 4
  %c_32 = add i12 %phi_mul, 33
  %Q_load_133 = load float* %Q_addr, align 4
  %Q_load_134 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2583), align 4
  %tmp_21_32 = fmul float %Q_load_133, %Q_load_134
  %temp_4_32 = fdiv float %tmp_21_32, %qStar
  %C_load_133 = load float* %C_addr, align 4
  %C_load_134 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2583), align 4
  %tmp_22_32 = fmul float %C_load_133, %C_load_134
  %tmp_23_32 = fmul float %C_load_133, %Q_load_134
  %tmp_24_32 = fadd float %tmp_22_32, %tmp_23_32
  %tmp_25_32 = fmul float %Q_load_133, %C_load_134
  %tmp_26_32 = fadd float %tmp_24_32, %tmp_25_32
  %tmp_27_32 = fadd float %tmp_26_32, %tmp_21_32
  %tmp_28_32 = fdiv float %tmp_27_32, %tmp_5
  %tmp_29_32 = fsub float %temp_4_32, %tmp_28_32
  %tmp_30_32 = zext i12 %c_32 to i64
  %C_addr_34 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_32
  %C_load_135 = load float* %C_addr_34, align 4
  %tmp_31_32 = fadd float %C_load_135, %tmp_29_32
  store float %tmp_31_32, float* %C_addr_34, align 4
  %Q_addr_34 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_32
  %Q_load_135 = load float* %Q_addr_34, align 4
  %tmp_32_32 = fsub float %Q_load_135, %temp_4_32
  store float %tmp_32_32, float* %Q_addr_34, align 4
  %c_33 = add i12 %phi_mul, 34
  %Q_load_136 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2584), align 16
  %tmp_21_33 = fmul float %Q_load_133, %Q_load_136
  %temp_4_33 = fdiv float %tmp_21_33, %qStar
  %C_load_136 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2584), align 16
  %tmp_22_33 = fmul float %C_load_133, %C_load_136
  %tmp_23_33 = fmul float %C_load_133, %Q_load_136
  %tmp_24_33 = fadd float %tmp_22_33, %tmp_23_33
  %tmp_25_33 = fmul float %Q_load_133, %C_load_136
  %tmp_26_33 = fadd float %tmp_24_33, %tmp_25_33
  %tmp_27_33 = fadd float %tmp_26_33, %tmp_21_33
  %tmp_28_33 = fdiv float %tmp_27_33, %tmp_5
  %tmp_29_33 = fsub float %temp_4_33, %tmp_28_33
  %tmp_30_33 = zext i12 %c_33 to i64
  %C_addr_35 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_33
  %C_load_137 = load float* %C_addr_35, align 4
  %tmp_31_33 = fadd float %C_load_137, %tmp_29_33
  store float %tmp_31_33, float* %C_addr_35, align 4
  %Q_addr_35 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_33
  %Q_load_137 = load float* %Q_addr_35, align 4
  %tmp_32_33 = fsub float %Q_load_137, %temp_4_33
  store float %tmp_32_33, float* %Q_addr_35, align 4
  %c_34 = add i12 %phi_mul, 35
  %Q_load_138 = load float* %Q_addr, align 4
  %Q_load_139 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2585), align 4
  %tmp_21_34 = fmul float %Q_load_138, %Q_load_139
  %temp_4_34 = fdiv float %tmp_21_34, %qStar
  %C_load_138 = load float* %C_addr, align 4
  %C_load_139 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2585), align 4
  %tmp_22_34 = fmul float %C_load_138, %C_load_139
  %tmp_23_34 = fmul float %C_load_138, %Q_load_139
  %tmp_24_34 = fadd float %tmp_22_34, %tmp_23_34
  %tmp_25_34 = fmul float %Q_load_138, %C_load_139
  %tmp_26_34 = fadd float %tmp_24_34, %tmp_25_34
  %tmp_27_34 = fadd float %tmp_26_34, %tmp_21_34
  %tmp_28_34 = fdiv float %tmp_27_34, %tmp_5
  %tmp_29_34 = fsub float %temp_4_34, %tmp_28_34
  %tmp_30_34 = zext i12 %c_34 to i64
  %C_addr_36 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_34
  %C_load_140 = load float* %C_addr_36, align 4
  %tmp_31_34 = fadd float %C_load_140, %tmp_29_34
  store float %tmp_31_34, float* %C_addr_36, align 4
  %Q_addr_36 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_34
  %Q_load_140 = load float* %Q_addr_36, align 4
  %tmp_32_34 = fsub float %Q_load_140, %temp_4_34
  store float %tmp_32_34, float* %Q_addr_36, align 4
  %c_35 = add i12 %phi_mul, 36
  %Q_load_141 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2586), align 8
  %tmp_21_35 = fmul float %Q_load_138, %Q_load_141
  %temp_4_35 = fdiv float %tmp_21_35, %qStar
  %C_load_141 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2586), align 8
  %tmp_22_35 = fmul float %C_load_138, %C_load_141
  %tmp_23_35 = fmul float %C_load_138, %Q_load_141
  %tmp_24_35 = fadd float %tmp_22_35, %tmp_23_35
  %tmp_25_35 = fmul float %Q_load_138, %C_load_141
  %tmp_26_35 = fadd float %tmp_24_35, %tmp_25_35
  %tmp_27_35 = fadd float %tmp_26_35, %tmp_21_35
  %tmp_28_35 = fdiv float %tmp_27_35, %tmp_5
  %tmp_29_35 = fsub float %temp_4_35, %tmp_28_35
  %tmp_30_35 = zext i12 %c_35 to i64
  %C_addr_37 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_35
  %C_load_142 = load float* %C_addr_37, align 4
  %tmp_31_35 = fadd float %C_load_142, %tmp_29_35
  store float %tmp_31_35, float* %C_addr_37, align 4
  %Q_addr_37 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_35
  %Q_load_142 = load float* %Q_addr_37, align 4
  %tmp_32_35 = fsub float %Q_load_142, %temp_4_35
  store float %tmp_32_35, float* %Q_addr_37, align 4
  %c_36 = add i12 %phi_mul, 37
  %Q_load_143 = load float* %Q_addr, align 4
  %Q_load_144 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2587), align 4
  %tmp_21_36 = fmul float %Q_load_143, %Q_load_144
  %temp_4_36 = fdiv float %tmp_21_36, %qStar
  %C_load_143 = load float* %C_addr, align 4
  %C_load_144 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2587), align 4
  %tmp_22_36 = fmul float %C_load_143, %C_load_144
  %tmp_23_36 = fmul float %C_load_143, %Q_load_144
  %tmp_24_36 = fadd float %tmp_22_36, %tmp_23_36
  %tmp_25_36 = fmul float %Q_load_143, %C_load_144
  %tmp_26_36 = fadd float %tmp_24_36, %tmp_25_36
  %tmp_27_36 = fadd float %tmp_26_36, %tmp_21_36
  %tmp_28_36 = fdiv float %tmp_27_36, %tmp_5
  %tmp_29_36 = fsub float %temp_4_36, %tmp_28_36
  %tmp_30_36 = zext i12 %c_36 to i64
  %C_addr_38 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_36
  %C_load_145 = load float* %C_addr_38, align 4
  %tmp_31_36 = fadd float %C_load_145, %tmp_29_36
  store float %tmp_31_36, float* %C_addr_38, align 4
  %Q_addr_38 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_36
  %Q_load_145 = load float* %Q_addr_38, align 4
  %tmp_32_36 = fsub float %Q_load_145, %temp_4_36
  store float %tmp_32_36, float* %Q_addr_38, align 4
  %c_37 = add i12 %phi_mul, 38
  %Q_load_146 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2588), align 16
  %tmp_21_37 = fmul float %Q_load_143, %Q_load_146
  %temp_4_37 = fdiv float %tmp_21_37, %qStar
  %C_load_146 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2588), align 16
  %tmp_22_37 = fmul float %C_load_143, %C_load_146
  %tmp_23_37 = fmul float %C_load_143, %Q_load_146
  %tmp_24_37 = fadd float %tmp_22_37, %tmp_23_37
  %tmp_25_37 = fmul float %Q_load_143, %C_load_146
  %tmp_26_37 = fadd float %tmp_24_37, %tmp_25_37
  %tmp_27_37 = fadd float %tmp_26_37, %tmp_21_37
  %tmp_28_37 = fdiv float %tmp_27_37, %tmp_5
  %tmp_29_37 = fsub float %temp_4_37, %tmp_28_37
  %tmp_30_37 = zext i12 %c_37 to i64
  %C_addr_39 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_37
  %C_load_147 = load float* %C_addr_39, align 4
  %tmp_31_37 = fadd float %C_load_147, %tmp_29_37
  store float %tmp_31_37, float* %C_addr_39, align 4
  %Q_addr_39 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_37
  %Q_load_147 = load float* %Q_addr_39, align 4
  %tmp_32_37 = fsub float %Q_load_147, %temp_4_37
  store float %tmp_32_37, float* %Q_addr_39, align 4
  %c_38 = add i12 %phi_mul, 39
  %Q_load_148 = load float* %Q_addr, align 4
  %Q_load_149 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2589), align 4
  %tmp_21_38 = fmul float %Q_load_148, %Q_load_149
  %temp_4_38 = fdiv float %tmp_21_38, %qStar
  %C_load_148 = load float* %C_addr, align 4
  %C_load_149 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2589), align 4
  %tmp_22_38 = fmul float %C_load_148, %C_load_149
  %tmp_23_38 = fmul float %C_load_148, %Q_load_149
  %tmp_24_38 = fadd float %tmp_22_38, %tmp_23_38
  %tmp_25_38 = fmul float %Q_load_148, %C_load_149
  %tmp_26_38 = fadd float %tmp_24_38, %tmp_25_38
  %tmp_27_38 = fadd float %tmp_26_38, %tmp_21_38
  %tmp_28_38 = fdiv float %tmp_27_38, %tmp_5
  %tmp_29_38 = fsub float %temp_4_38, %tmp_28_38
  %tmp_30_38 = zext i12 %c_38 to i64
  %C_addr_40 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_38
  %C_load_150 = load float* %C_addr_40, align 4
  %tmp_31_38 = fadd float %C_load_150, %tmp_29_38
  store float %tmp_31_38, float* %C_addr_40, align 4
  %Q_addr_40 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_38
  %Q_load_150 = load float* %Q_addr_40, align 4
  %tmp_32_38 = fsub float %Q_load_150, %temp_4_38
  store float %tmp_32_38, float* %Q_addr_40, align 4
  %c_39 = add i12 %phi_mul, 40
  %Q_load_151 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2590), align 8
  %tmp_21_39 = fmul float %Q_load_148, %Q_load_151
  %temp_4_39 = fdiv float %tmp_21_39, %qStar
  %C_load_151 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2590), align 8
  %tmp_22_39 = fmul float %C_load_148, %C_load_151
  %tmp_23_39 = fmul float %C_load_148, %Q_load_151
  %tmp_24_39 = fadd float %tmp_22_39, %tmp_23_39
  %tmp_25_39 = fmul float %Q_load_148, %C_load_151
  %tmp_26_39 = fadd float %tmp_24_39, %tmp_25_39
  %tmp_27_39 = fadd float %tmp_26_39, %tmp_21_39
  %tmp_28_39 = fdiv float %tmp_27_39, %tmp_5
  %tmp_29_39 = fsub float %temp_4_39, %tmp_28_39
  %tmp_30_39 = zext i12 %c_39 to i64
  %C_addr_41 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_39
  %C_load_152 = load float* %C_addr_41, align 4
  %tmp_31_39 = fadd float %C_load_152, %tmp_29_39
  store float %tmp_31_39, float* %C_addr_41, align 4
  %Q_addr_41 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_39
  %Q_load_152 = load float* %Q_addr_41, align 4
  %tmp_32_39 = fsub float %Q_load_152, %temp_4_39
  store float %tmp_32_39, float* %Q_addr_41, align 4
  %c_40 = add i12 %phi_mul, 41
  %Q_load_153 = load float* %Q_addr, align 4
  %Q_load_154 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2591), align 4
  %tmp_21_40 = fmul float %Q_load_153, %Q_load_154
  %temp_4_40 = fdiv float %tmp_21_40, %qStar
  %C_load_153 = load float* %C_addr, align 4
  %C_load_154 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2591), align 4
  %tmp_22_40 = fmul float %C_load_153, %C_load_154
  %tmp_23_40 = fmul float %C_load_153, %Q_load_154
  %tmp_24_40 = fadd float %tmp_22_40, %tmp_23_40
  %tmp_25_40 = fmul float %Q_load_153, %C_load_154
  %tmp_26_40 = fadd float %tmp_24_40, %tmp_25_40
  %tmp_27_40 = fadd float %tmp_26_40, %tmp_21_40
  %tmp_28_40 = fdiv float %tmp_27_40, %tmp_5
  %tmp_29_40 = fsub float %temp_4_40, %tmp_28_40
  %tmp_30_40 = zext i12 %c_40 to i64
  %C_addr_42 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_40
  %C_load_155 = load float* %C_addr_42, align 4
  %tmp_31_40 = fadd float %C_load_155, %tmp_29_40
  store float %tmp_31_40, float* %C_addr_42, align 4
  %Q_addr_42 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_40
  %Q_load_155 = load float* %Q_addr_42, align 4
  %tmp_32_40 = fsub float %Q_load_155, %temp_4_40
  store float %tmp_32_40, float* %Q_addr_42, align 4
  %c_41 = add i12 %phi_mul, 42
  %Q_load_156 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2592), align 16
  %tmp_21_41 = fmul float %Q_load_153, %Q_load_156
  %temp_4_41 = fdiv float %tmp_21_41, %qStar
  %C_load_156 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2592), align 16
  %tmp_22_41 = fmul float %C_load_153, %C_load_156
  %tmp_23_41 = fmul float %C_load_153, %Q_load_156
  %tmp_24_41 = fadd float %tmp_22_41, %tmp_23_41
  %tmp_25_41 = fmul float %Q_load_153, %C_load_156
  %tmp_26_41 = fadd float %tmp_24_41, %tmp_25_41
  %tmp_27_41 = fadd float %tmp_26_41, %tmp_21_41
  %tmp_28_41 = fdiv float %tmp_27_41, %tmp_5
  %tmp_29_41 = fsub float %temp_4_41, %tmp_28_41
  %tmp_30_41 = zext i12 %c_41 to i64
  %C_addr_43 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_41
  %C_load_157 = load float* %C_addr_43, align 4
  %tmp_31_41 = fadd float %C_load_157, %tmp_29_41
  store float %tmp_31_41, float* %C_addr_43, align 4
  %Q_addr_43 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_41
  %Q_load_157 = load float* %Q_addr_43, align 4
  %tmp_32_41 = fsub float %Q_load_157, %temp_4_41
  store float %tmp_32_41, float* %Q_addr_43, align 4
  %c_42 = add i12 %phi_mul, 43
  %Q_load_158 = load float* %Q_addr, align 4
  %Q_load_159 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2593), align 4
  %tmp_21_42 = fmul float %Q_load_158, %Q_load_159
  %temp_4_42 = fdiv float %tmp_21_42, %qStar
  %C_load_158 = load float* %C_addr, align 4
  %C_load_159 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2593), align 4
  %tmp_22_42 = fmul float %C_load_158, %C_load_159
  %tmp_23_42 = fmul float %C_load_158, %Q_load_159
  %tmp_24_42 = fadd float %tmp_22_42, %tmp_23_42
  %tmp_25_42 = fmul float %Q_load_158, %C_load_159
  %tmp_26_42 = fadd float %tmp_24_42, %tmp_25_42
  %tmp_27_42 = fadd float %tmp_26_42, %tmp_21_42
  %tmp_28_42 = fdiv float %tmp_27_42, %tmp_5
  %tmp_29_42 = fsub float %temp_4_42, %tmp_28_42
  %tmp_30_42 = zext i12 %c_42 to i64
  %C_addr_44 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_42
  %C_load_160 = load float* %C_addr_44, align 4
  %tmp_31_42 = fadd float %C_load_160, %tmp_29_42
  store float %tmp_31_42, float* %C_addr_44, align 4
  %Q_addr_44 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_42
  %Q_load_160 = load float* %Q_addr_44, align 4
  %tmp_32_42 = fsub float %Q_load_160, %temp_4_42
  store float %tmp_32_42, float* %Q_addr_44, align 4
  %c_43 = add i12 %phi_mul, 44
  %Q_load_161 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2594), align 8
  %tmp_21_43 = fmul float %Q_load_158, %Q_load_161
  %temp_4_43 = fdiv float %tmp_21_43, %qStar
  %C_load_161 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2594), align 8
  %tmp_22_43 = fmul float %C_load_158, %C_load_161
  %tmp_23_43 = fmul float %C_load_158, %Q_load_161
  %tmp_24_43 = fadd float %tmp_22_43, %tmp_23_43
  %tmp_25_43 = fmul float %Q_load_158, %C_load_161
  %tmp_26_43 = fadd float %tmp_24_43, %tmp_25_43
  %tmp_27_43 = fadd float %tmp_26_43, %tmp_21_43
  %tmp_28_43 = fdiv float %tmp_27_43, %tmp_5
  %tmp_29_43 = fsub float %temp_4_43, %tmp_28_43
  %tmp_30_43 = zext i12 %c_43 to i64
  %C_addr_45 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_43
  %C_load_162 = load float* %C_addr_45, align 4
  %tmp_31_43 = fadd float %C_load_162, %tmp_29_43
  store float %tmp_31_43, float* %C_addr_45, align 4
  %Q_addr_45 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_43
  %Q_load_162 = load float* %Q_addr_45, align 4
  %tmp_32_43 = fsub float %Q_load_162, %temp_4_43
  store float %tmp_32_43, float* %Q_addr_45, align 4
  %c_44 = add i12 %phi_mul, 45
  %Q_load_163 = load float* %Q_addr, align 4
  %Q_load_164 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2595), align 4
  %tmp_21_44 = fmul float %Q_load_163, %Q_load_164
  %temp_4_44 = fdiv float %tmp_21_44, %qStar
  %C_load_163 = load float* %C_addr, align 4
  %C_load_164 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2595), align 4
  %tmp_22_44 = fmul float %C_load_163, %C_load_164
  %tmp_23_44 = fmul float %C_load_163, %Q_load_164
  %tmp_24_44 = fadd float %tmp_22_44, %tmp_23_44
  %tmp_25_44 = fmul float %Q_load_163, %C_load_164
  %tmp_26_44 = fadd float %tmp_24_44, %tmp_25_44
  %tmp_27_44 = fadd float %tmp_26_44, %tmp_21_44
  %tmp_28_44 = fdiv float %tmp_27_44, %tmp_5
  %tmp_29_44 = fsub float %temp_4_44, %tmp_28_44
  %tmp_30_44 = zext i12 %c_44 to i64
  %C_addr_46 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_44
  %C_load_165 = load float* %C_addr_46, align 4
  %tmp_31_44 = fadd float %C_load_165, %tmp_29_44
  store float %tmp_31_44, float* %C_addr_46, align 4
  %Q_addr_46 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_44
  %Q_load_165 = load float* %Q_addr_46, align 4
  %tmp_32_44 = fsub float %Q_load_165, %temp_4_44
  store float %tmp_32_44, float* %Q_addr_46, align 4
  %c_45 = add i12 %phi_mul, 46
  %Q_load_166 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2596), align 16
  %tmp_21_45 = fmul float %Q_load_163, %Q_load_166
  %temp_4_45 = fdiv float %tmp_21_45, %qStar
  %C_load_166 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2596), align 16
  %tmp_22_45 = fmul float %C_load_163, %C_load_166
  %tmp_23_45 = fmul float %C_load_163, %Q_load_166
  %tmp_24_45 = fadd float %tmp_22_45, %tmp_23_45
  %tmp_25_45 = fmul float %Q_load_163, %C_load_166
  %tmp_26_45 = fadd float %tmp_24_45, %tmp_25_45
  %tmp_27_45 = fadd float %tmp_26_45, %tmp_21_45
  %tmp_28_45 = fdiv float %tmp_27_45, %tmp_5
  %tmp_29_45 = fsub float %temp_4_45, %tmp_28_45
  %tmp_30_45 = zext i12 %c_45 to i64
  %C_addr_47 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_45
  %C_load_167 = load float* %C_addr_47, align 4
  %tmp_31_45 = fadd float %C_load_167, %tmp_29_45
  store float %tmp_31_45, float* %C_addr_47, align 4
  %Q_addr_47 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_45
  %Q_load_167 = load float* %Q_addr_47, align 4
  %tmp_32_45 = fsub float %Q_load_167, %temp_4_45
  store float %tmp_32_45, float* %Q_addr_47, align 4
  %c_46 = add i12 %phi_mul, 47
  %Q_load_168 = load float* %Q_addr, align 4
  %Q_load_169 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2597), align 4
  %tmp_21_46 = fmul float %Q_load_168, %Q_load_169
  %temp_4_46 = fdiv float %tmp_21_46, %qStar
  %C_load_168 = load float* %C_addr, align 4
  %C_load_169 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2597), align 4
  %tmp_22_46 = fmul float %C_load_168, %C_load_169
  %tmp_23_46 = fmul float %C_load_168, %Q_load_169
  %tmp_24_46 = fadd float %tmp_22_46, %tmp_23_46
  %tmp_25_46 = fmul float %Q_load_168, %C_load_169
  %tmp_26_46 = fadd float %tmp_24_46, %tmp_25_46
  %tmp_27_46 = fadd float %tmp_26_46, %tmp_21_46
  %tmp_28_46 = fdiv float %tmp_27_46, %tmp_5
  %tmp_29_46 = fsub float %temp_4_46, %tmp_28_46
  %tmp_30_46 = zext i12 %c_46 to i64
  %C_addr_48 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_46
  %C_load_170 = load float* %C_addr_48, align 4
  %tmp_31_46 = fadd float %C_load_170, %tmp_29_46
  store float %tmp_31_46, float* %C_addr_48, align 4
  %Q_addr_48 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_46
  %Q_load_170 = load float* %Q_addr_48, align 4
  %tmp_32_46 = fsub float %Q_load_170, %temp_4_46
  store float %tmp_32_46, float* %Q_addr_48, align 4
  %c_47 = add i12 %phi_mul, 48
  %Q_load_171 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2598), align 8
  %tmp_21_47 = fmul float %Q_load_168, %Q_load_171
  %temp_4_47 = fdiv float %tmp_21_47, %qStar
  %C_load_171 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2598), align 8
  %tmp_22_47 = fmul float %C_load_168, %C_load_171
  %tmp_23_47 = fmul float %C_load_168, %Q_load_171
  %tmp_24_47 = fadd float %tmp_22_47, %tmp_23_47
  %tmp_25_47 = fmul float %Q_load_168, %C_load_171
  %tmp_26_47 = fadd float %tmp_24_47, %tmp_25_47
  %tmp_27_47 = fadd float %tmp_26_47, %tmp_21_47
  %tmp_28_47 = fdiv float %tmp_27_47, %tmp_5
  %tmp_29_47 = fsub float %temp_4_47, %tmp_28_47
  %tmp_30_47 = zext i12 %c_47 to i64
  %C_addr_49 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_47
  %C_load_172 = load float* %C_addr_49, align 4
  %tmp_31_47 = fadd float %C_load_172, %tmp_29_47
  store float %tmp_31_47, float* %C_addr_49, align 4
  %Q_addr_49 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_47
  %Q_load_172 = load float* %Q_addr_49, align 4
  %tmp_32_47 = fsub float %Q_load_172, %temp_4_47
  store float %tmp_32_47, float* %Q_addr_49, align 4
  %c_48 = add i12 %phi_mul, 49
  %Q_load_173 = load float* %Q_addr, align 4
  %Q_load_174 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2599), align 4
  %tmp_21_48 = fmul float %Q_load_173, %Q_load_174
  %temp_4_48 = fdiv float %tmp_21_48, %qStar
  %C_load_173 = load float* %C_addr, align 4
  %C_load_174 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2599), align 4
  %tmp_22_48 = fmul float %C_load_173, %C_load_174
  %tmp_23_48 = fmul float %C_load_173, %Q_load_174
  %tmp_24_48 = fadd float %tmp_22_48, %tmp_23_48
  %tmp_25_48 = fmul float %Q_load_173, %C_load_174
  %tmp_26_48 = fadd float %tmp_24_48, %tmp_25_48
  %tmp_27_48 = fadd float %tmp_26_48, %tmp_21_48
  %tmp_28_48 = fdiv float %tmp_27_48, %tmp_5
  %tmp_29_48 = fsub float %temp_4_48, %tmp_28_48
  %tmp_30_48 = zext i12 %c_48 to i64
  %C_addr_50 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_30_48
  %C_load_175 = load float* %C_addr_50, align 4
  %tmp_31_48 = fadd float %C_load_175, %tmp_29_48
  store float %tmp_31_48, float* %C_addr_50, align 4
  %Q_addr_50 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_30_48
  %Q_load_175 = load float* %Q_addr_50, align 4
  %tmp_32_48 = fsub float %Q_load_175, %temp_4_48
  store float %tmp_32_48, float* %Q_addr_50, align 4
  %empty_4 = call i32 (...)* @_ssdm_op_SpecRegionEnd([18 x i8]* @p_str4, i32 %tmp_s) nounwind
  br label %4

.preheader.0:                                     ; preds = %4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 50), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2550), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 50), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2550), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 101), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2551), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 101), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2551), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 152), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2552), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 152), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2552), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 203), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2553), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 203), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2553), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 254), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2554), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 254), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2554), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 305), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2555), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 305), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2555), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 356), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2556), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 356), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2556), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 407), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2557), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 407), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2557), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 458), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2558), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 458), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2558), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 509), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2559), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 509), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2559), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 560), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2560), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 560), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2560), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 611), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2561), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 611), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2561), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 662), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2562), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 662), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2562), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 713), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2563), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 713), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2563), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 764), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2564), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 764), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2564), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 815), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2565), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 815), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2565), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 866), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2566), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 866), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2566), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 917), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2567), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 917), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2567), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 968), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2568), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 968), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2568), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1019), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2569), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1019), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2569), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1070), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2570), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1070), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2570), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1121), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2571), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1121), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2571), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1172), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2572), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1172), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2572), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1223), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2573), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1223), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2573), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1274), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2574), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1274), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2574), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1325), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2575), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1325), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2575), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1376), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2576), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1376), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2576), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1427), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2577), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1427), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2577), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1478), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2578), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1478), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2578), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1529), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2579), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1529), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2579), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1580), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2580), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1580), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2580), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1631), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2581), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1631), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2581), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1682), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2582), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1682), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2582), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1733), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2583), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1733), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2583), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1784), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2584), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1784), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2584), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1835), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2585), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1835), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2585), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1886), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2586), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1886), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2586), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1937), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2587), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1937), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2587), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 1988), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2588), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 1988), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2588), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2039), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2589), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2039), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2589), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2090), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2590), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2090), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2590), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2141), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2591), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2141), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2591), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2192), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2592), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2192), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2592), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2243), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2593), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2243), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2593), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2294), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2594), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2294), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2594), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2345), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2595), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2345), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2595), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2396), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2596), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2396), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2596), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2447), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2597), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2447), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2597), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2498), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2598), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2498), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2598), align 8
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2549), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2599), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2549), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2599), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 2600), align 16
  store float 0.000000e+00, float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 2600), align 16
  ret void
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecLoopTripCount(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

declare void @_GLOBAL__I_a() nounwind section ".text.startup"

declare float @llvm.exp.f32(float) nounwind readonly

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define float @projection_gp([20 x float]* %pX, float %pY, i1 zeroext %pPredict) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([20 x float]* %pX) nounwind, !map !55
  call void (...)* @_ssdm_op_SpecBitsMap(float %pY) nounwind, !map !61
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pPredict) nounwind, !map !67
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !71
  call void (...)* @_ssdm_op_SpecTopModule([14 x i8]* @projection_gp_str) nounwind
  %pPredict_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pPredict) nounwind
  %pY_read = call float @_ssdm_op_Read.s_axilite.float(float %pY) nounwind
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([20 x float]* %pX, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3) nounwind
  call void (...)* @_ssdm_op_SpecInterface([20 x float]* %pX, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 20, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(float %pY, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i1 %pPredict, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  br i1 %pPredict_read, label %.preheader.0, label %1

; <label>:1                                       ; preds = %0
  %bvCnt_load = load i32* @bvCnt, align 4
  %tmp_s = icmp eq i32 %bvCnt_load, 50
  br i1 %tmp_s, label %2, label %3

; <label>:2                                       ; preds = %1
  call fastcc void @projection_gp_train_full_bv_set([20 x float]* %pX, float %pY_read) nounwind
  %bvCnt_load_1 = load i32* @bvCnt, align 4
  %tmp_23 = add i32 %bvCnt_load_1, 1
  store i32 %tmp_23, i32* @bvCnt, align 4
  br label %4

; <label>:3                                       ; preds = %1
  call fastcc void @projection_gp_train_not_full_bv_set([20 x float]* %pX, float %pY_read) nounwind
  br label %4

; <label>:4                                       ; preds = %3, %2
  br label %.loopexit

.preheader.0:                                     ; preds = %0
  %tmp_24 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 0, [20 x float]* %pX) nounwind
  %alpha_load = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_25 = fmul float %tmp_24, %alpha_load
  %sum_2 = fadd float %tmp_25, 5.000000e+00
  %tmp_26 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 20, [20 x float]* %pX) nounwind
  %alpha_load_1 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 1), align 4
  %tmp_155_1 = fmul float %tmp_26, %alpha_load_1
  %sum_2_1 = fadd float %sum_2, %tmp_155_1
  %tmp_27 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 40, [20 x float]* %pX) nounwind
  %alpha_load_2 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 2), align 8
  %tmp_155_2 = fmul float %tmp_27, %alpha_load_2
  %sum_2_2 = fadd float %sum_2_1, %tmp_155_2
  %tmp_28 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 60, [20 x float]* %pX) nounwind
  %alpha_load_3 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 3), align 4
  %tmp_155_3 = fmul float %tmp_28, %alpha_load_3
  %sum_2_3 = fadd float %sum_2_2, %tmp_155_3
  %tmp_29 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 80, [20 x float]* %pX) nounwind
  %alpha_load_4 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 4), align 16
  %tmp_155_4 = fmul float %tmp_29, %alpha_load_4
  %sum_2_4 = fadd float %sum_2_3, %tmp_155_4
  %tmp_30 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 100, [20 x float]* %pX) nounwind
  %alpha_load_5 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 5), align 4
  %tmp_155_5 = fmul float %tmp_30, %alpha_load_5
  %sum_2_5 = fadd float %sum_2_4, %tmp_155_5
  %tmp_31 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 120, [20 x float]* %pX) nounwind
  %alpha_load_6 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 6), align 8
  %tmp_155_6 = fmul float %tmp_31, %alpha_load_6
  %sum_2_6 = fadd float %sum_2_5, %tmp_155_6
  %tmp_32 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 140, [20 x float]* %pX) nounwind
  %alpha_load_7 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 7), align 4
  %tmp_155_7 = fmul float %tmp_32, %alpha_load_7
  %sum_2_7 = fadd float %sum_2_6, %tmp_155_7
  %tmp_33 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 160, [20 x float]* %pX) nounwind
  %alpha_load_8 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 8), align 16
  %tmp_155_8 = fmul float %tmp_33, %alpha_load_8
  %sum_2_8 = fadd float %sum_2_7, %tmp_155_8
  %tmp_34 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 180, [20 x float]* %pX) nounwind
  %alpha_load_9 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 9), align 4
  %tmp_155_9 = fmul float %tmp_34, %alpha_load_9
  %sum_2_9 = fadd float %sum_2_8, %tmp_155_9
  %tmp_35 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 200, [20 x float]* %pX) nounwind
  %alpha_load_10 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 10), align 8
  %tmp_155_s = fmul float %tmp_35, %alpha_load_10
  %sum_2_s = fadd float %sum_2_9, %tmp_155_s
  %tmp_36 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 220, [20 x float]* %pX) nounwind
  %alpha_load_11 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 11), align 4
  %tmp_155_10 = fmul float %tmp_36, %alpha_load_11
  %sum_2_10 = fadd float %sum_2_s, %tmp_155_10
  %tmp_37 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 240, [20 x float]* %pX) nounwind
  %alpha_load_12 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 12), align 16
  %tmp_155_11 = fmul float %tmp_37, %alpha_load_12
  %sum_2_11 = fadd float %sum_2_10, %tmp_155_11
  %tmp_38 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 260, [20 x float]* %pX) nounwind
  %alpha_load_13 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 13), align 4
  %tmp_155_12 = fmul float %tmp_38, %alpha_load_13
  %sum_2_12 = fadd float %sum_2_11, %tmp_155_12
  %tmp_39 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 280, [20 x float]* %pX) nounwind
  %alpha_load_14 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 14), align 8
  %tmp_155_13 = fmul float %tmp_39, %alpha_load_14
  %sum_2_13 = fadd float %sum_2_12, %tmp_155_13
  %tmp_40 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 300, [20 x float]* %pX) nounwind
  %alpha_load_15 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 15), align 4
  %tmp_155_14 = fmul float %tmp_40, %alpha_load_15
  %sum_2_14 = fadd float %sum_2_13, %tmp_155_14
  %tmp_41 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 320, [20 x float]* %pX) nounwind
  %alpha_load_16 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 16), align 16
  %tmp_155_15 = fmul float %tmp_41, %alpha_load_16
  %sum_2_15 = fadd float %sum_2_14, %tmp_155_15
  %tmp_42 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 340, [20 x float]* %pX) nounwind
  %alpha_load_17 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 17), align 4
  %tmp_155_16 = fmul float %tmp_42, %alpha_load_17
  %sum_2_16 = fadd float %sum_2_15, %tmp_155_16
  %tmp_43 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 360, [20 x float]* %pX) nounwind
  %alpha_load_18 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 18), align 8
  %tmp_155_17 = fmul float %tmp_43, %alpha_load_18
  %sum_2_17 = fadd float %sum_2_16, %tmp_155_17
  %tmp_44 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 380, [20 x float]* %pX) nounwind
  %alpha_load_19 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 19), align 4
  %tmp_155_18 = fmul float %tmp_44, %alpha_load_19
  %sum_2_18 = fadd float %sum_2_17, %tmp_155_18
  %tmp_45 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 400, [20 x float]* %pX) nounwind
  %alpha_load_20 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 20), align 16
  %tmp_155_19 = fmul float %tmp_45, %alpha_load_20
  %sum_2_19 = fadd float %sum_2_18, %tmp_155_19
  %tmp_46 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 420, [20 x float]* %pX) nounwind
  %alpha_load_21 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 21), align 4
  %tmp_155_20 = fmul float %tmp_46, %alpha_load_21
  %sum_2_20 = fadd float %sum_2_19, %tmp_155_20
  %tmp_47 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 440, [20 x float]* %pX) nounwind
  %alpha_load_22 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 22), align 8
  %tmp_155_21 = fmul float %tmp_47, %alpha_load_22
  %sum_2_21 = fadd float %sum_2_20, %tmp_155_21
  %tmp_48 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 460, [20 x float]* %pX) nounwind
  %alpha_load_23 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 23), align 4
  %tmp_155_22 = fmul float %tmp_48, %alpha_load_23
  %sum_2_22 = fadd float %sum_2_21, %tmp_155_22
  %tmp_49 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 480, [20 x float]* %pX) nounwind
  %alpha_load_24 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 24), align 16
  %tmp_155_23 = fmul float %tmp_49, %alpha_load_24
  %sum_2_23 = fadd float %sum_2_22, %tmp_155_23
  %tmp_50 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 500, [20 x float]* %pX) nounwind
  %alpha_load_25 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 25), align 4
  %tmp_155_24 = fmul float %tmp_50, %alpha_load_25
  %sum_2_24 = fadd float %sum_2_23, %tmp_155_24
  %tmp_51 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 520, [20 x float]* %pX) nounwind
  %alpha_load_26 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 26), align 8
  %tmp_155_25 = fmul float %tmp_51, %alpha_load_26
  %sum_2_25 = fadd float %sum_2_24, %tmp_155_25
  %tmp_52 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 540, [20 x float]* %pX) nounwind
  %alpha_load_27 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 27), align 4
  %tmp_155_26 = fmul float %tmp_52, %alpha_load_27
  %sum_2_26 = fadd float %sum_2_25, %tmp_155_26
  %tmp_53 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 560, [20 x float]* %pX) nounwind
  %alpha_load_28 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 28), align 16
  %tmp_155_27 = fmul float %tmp_53, %alpha_load_28
  %sum_2_27 = fadd float %sum_2_26, %tmp_155_27
  %tmp_54 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 580, [20 x float]* %pX) nounwind
  %alpha_load_29 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 29), align 4
  %tmp_155_28 = fmul float %tmp_54, %alpha_load_29
  %sum_2_28 = fadd float %sum_2_27, %tmp_155_28
  %tmp_55 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 600, [20 x float]* %pX) nounwind
  %alpha_load_30 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 30), align 8
  %tmp_155_29 = fmul float %tmp_55, %alpha_load_30
  %sum_2_29 = fadd float %sum_2_28, %tmp_155_29
  %tmp_56 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 620, [20 x float]* %pX) nounwind
  %alpha_load_31 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 31), align 4
  %tmp_155_30 = fmul float %tmp_56, %alpha_load_31
  %sum_2_30 = fadd float %sum_2_29, %tmp_155_30
  %tmp_57 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 640, [20 x float]* %pX) nounwind
  %alpha_load_32 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 32), align 16
  %tmp_155_31 = fmul float %tmp_57, %alpha_load_32
  %sum_2_31 = fadd float %sum_2_30, %tmp_155_31
  %tmp_58 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 660, [20 x float]* %pX) nounwind
  %alpha_load_33 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 33), align 4
  %tmp_155_32 = fmul float %tmp_58, %alpha_load_33
  %sum_2_32 = fadd float %sum_2_31, %tmp_155_32
  %tmp_59 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 680, [20 x float]* %pX) nounwind
  %alpha_load_34 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 34), align 8
  %tmp_155_33 = fmul float %tmp_59, %alpha_load_34
  %sum_2_33 = fadd float %sum_2_32, %tmp_155_33
  %tmp_60 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 700, [20 x float]* %pX) nounwind
  %alpha_load_35 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 35), align 4
  %tmp_155_34 = fmul float %tmp_60, %alpha_load_35
  %sum_2_34 = fadd float %sum_2_33, %tmp_155_34
  %tmp_61 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 720, [20 x float]* %pX) nounwind
  %alpha_load_36 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 36), align 16
  %tmp_155_35 = fmul float %tmp_61, %alpha_load_36
  %sum_2_35 = fadd float %sum_2_34, %tmp_155_35
  %tmp_62 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 740, [20 x float]* %pX) nounwind
  %alpha_load_37 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 37), align 4
  %tmp_155_36 = fmul float %tmp_62, %alpha_load_37
  %sum_2_36 = fadd float %sum_2_35, %tmp_155_36
  %tmp_63 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 760, [20 x float]* %pX) nounwind
  %alpha_load_38 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 38), align 8
  %tmp_155_37 = fmul float %tmp_63, %alpha_load_38
  %sum_2_37 = fadd float %sum_2_36, %tmp_155_37
  %tmp_64 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 780, [20 x float]* %pX) nounwind
  %alpha_load_39 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 39), align 4
  %tmp_155_38 = fmul float %tmp_64, %alpha_load_39
  %sum_2_38 = fadd float %sum_2_37, %tmp_155_38
  %tmp_65 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 800, [20 x float]* %pX) nounwind
  %alpha_load_40 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 40), align 16
  %tmp_155_39 = fmul float %tmp_65, %alpha_load_40
  %sum_2_39 = fadd float %sum_2_38, %tmp_155_39
  %tmp_66 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 820, [20 x float]* %pX) nounwind
  %alpha_load_41 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 41), align 4
  %tmp_155_40 = fmul float %tmp_66, %alpha_load_41
  %sum_2_40 = fadd float %sum_2_39, %tmp_155_40
  %tmp_67 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 840, [20 x float]* %pX) nounwind
  %alpha_load_42 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 42), align 8
  %tmp_155_41 = fmul float %tmp_67, %alpha_load_42
  %sum_2_41 = fadd float %sum_2_40, %tmp_155_41
  %tmp_68 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 860, [20 x float]* %pX) nounwind
  %alpha_load_43 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 43), align 4
  %tmp_155_42 = fmul float %tmp_68, %alpha_load_43
  %sum_2_42 = fadd float %sum_2_41, %tmp_155_42
  %tmp_69 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 880, [20 x float]* %pX) nounwind
  %alpha_load_44 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 44), align 16
  %tmp_155_43 = fmul float %tmp_69, %alpha_load_44
  %sum_2_43 = fadd float %sum_2_42, %tmp_155_43
  %tmp_70 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 900, [20 x float]* %pX) nounwind
  %alpha_load_45 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 45), align 4
  %tmp_155_44 = fmul float %tmp_70, %alpha_load_45
  %sum_2_44 = fadd float %sum_2_43, %tmp_155_44
  %tmp_71 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 920, [20 x float]* %pX) nounwind
  %alpha_load_46 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 46), align 8
  %tmp_155_45 = fmul float %tmp_71, %alpha_load_46
  %sum_2_45 = fadd float %sum_2_44, %tmp_155_45
  %tmp_72 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 940, [20 x float]* %pX) nounwind
  %alpha_load_47 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 47), align 4
  %tmp_155_46 = fmul float %tmp_72, %alpha_load_47
  %sum_2_46 = fadd float %sum_2_45, %tmp_155_46
  %tmp_73 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 960, [20 x float]* %pX) nounwind
  %alpha_load_48 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 48), align 16
  %tmp_155_47 = fmul float %tmp_73, %alpha_load_48
  %sum_2_47 = fadd float %sum_2_46, %tmp_155_47
  %tmp_74 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 980, [20 x float]* %pX) nounwind
  %alpha_load_49 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 49), align 4
  %tmp_155_48 = fmul float %tmp_74, %alpha_load_49
  %sum_2_48 = fadd float %sum_2_47, %tmp_155_48
  br label %.loopexit

.loopexit:                                        ; preds = %.preheader.0, %4
  %p_0 = phi float [ 0.000000e+00, %4 ], [ %sum_2_48, %.preheader.0 ]
  ret float %p_0
}

define internal fastcc void @projection_gp_train_not_full_bv_set([20 x float]* nocapture %pX, float %pY) {
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([20 x float]* %pX, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3)
  %pY_read = call float @_ssdm_op_Read.ap_auto.float(float %pY)
  br label %1

; <label>:1                                       ; preds = %K.exit, %0
  %i = phi i32 [ 0, %0 ], [ %i_3, %K.exit ]
  %m = phi float [ 5.000000e+00, %0 ], [ %m_1, %K.exit ]
  %bvCnt_load = load i32* @bvCnt, align 4
  %tmp = icmp ult i32 %i, %bvCnt_load
  %i_3 = add i32 %i, 1
  br i1 %tmp, label %2, label %.preheader2

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str7) nounwind
  %tmp_83 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str7)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 50, i32 25, [1 x i8]* @p_str1) nounwind
  %tmp_27 = shl i32 %i, 4
  %tmp_29 = shl i32 %i, 2
  %tmp_s = add i32 %tmp_27, %tmp_29
  %tmp_30 = trunc i32 %tmp_s to i11
  br label %3

; <label>:3                                       ; preds = %4, %2
  %sum_i = phi float [ 0.000000e+00, %2 ], [ %sum, %4 ]
  %i_i = phi i5 [ 0, %2 ], [ %i_8, %4 ]
  %exitcond_i = icmp eq i5 %i_i, -12
  %i_8 = add i5 %i_i, 1
  br i1 %exitcond_i, label %K.exit, label %4

; <label>:4                                       ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 20, i64 20, i64 20)
  %tmp_578_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_2_i = zext i5 %i_i to i64
  %tmp_2_i_cast = zext i5 %i_i to i11
  %sum1_i = add i11 %tmp_2_i_cast, %tmp_30
  %sum1_i_cast = zext i11 %sum1_i to i64
  %basisVectors_addr = getelementptr [1020 x float]* @basisVectors, i64 0, i64 %sum1_i_cast
  %basisVectors_load = load float* %basisVectors_addr, align 4
  %pX_addr = getelementptr [20 x float]* %pX, i64 0, i64 %tmp_2_i
  %pX_load = load float* %pX_addr, align 4
  %val = fsub float %basisVectors_load, %pX_load
  %tmp_3_i = fmul float %val, %val
  %sum = fadd float %sum_i, %tmp_3_i
  %empty_5 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_578_i)
  br label %3

K.exit:                                           ; preds = %3
  %p_x_assign = fmul float %sum_i, -5.000000e-01
  %tmp_i_i = call float @llvm.exp.f32(float %p_x_assign) nounwind
  %tmp_25 = zext i32 %i to i64
  %k_addr = getelementptr inbounds [50 x float]* @k, i64 0, i64 %tmp_25
  store float %tmp_i_i, float* %k_addr, align 4
  %alpha_addr = getelementptr inbounds [51 x float]* @alpha, i64 0, i64 %tmp_25
  %alpha_load = load float* %alpha_addr, align 4
  %tmp_26 = fmul float %tmp_i_i, %alpha_load
  %m_1 = fadd float %m, %tmp_26
  %empty_6 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str7, i32 %tmp_83)
  br label %1

.preheader2:                                      ; preds = %1, %8
  %i1 = phi i32 [ %i_5, %8 ], [ 0, %1 ]
  %phi_mul = phi i32 [ %next_mul, %8 ], [ 0, %1 ]
  %next_mul = add i32 %phi_mul, 51
  %exitcond1 = icmp eq i32 %i1, %bvCnt_load
  %i_5 = add i32 %i1, 1
  br i1 %exitcond1, label %.preheader1, label %5

; <label>:5                                       ; preds = %.preheader2
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str8) nounwind
  %tmp_84 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str8)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 50, i32 25, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_28 = zext i32 %i1 to i64
  %s_addr = getelementptr inbounds [51 x float]* @s, i64 0, i64 %tmp_28
  store float 0.000000e+00, float* %s_addr, align 4
  %e_addr = getelementptr inbounds [51 x float]* @e, i64 0, i64 %tmp_28
  store float 0.000000e+00, float* %e_addr, align 4
  br label %6

; <label>:6                                       ; preds = %7, %5
  %tmp_31 = phi float [ 0.000000e+00, %5 ], [ %tmp_48, %7 ]
  %tmp_32 = phi float [ 0.000000e+00, %5 ], [ %tmp_46, %7 ]
  %j = phi i32 [ 0, %5 ], [ %j_1, %7 ]
  %exitcond3 = icmp eq i32 %j, %bvCnt_load
  %j_1 = add i32 %j, 1
  br i1 %exitcond3, label %8, label %7

; <label>:7                                       ; preds = %6
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str9) nounwind
  %tmp_85 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str9)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 50, i32 25, [1 x i8]* @p_str1) nounwind
  %tmp_42 = add i32 %j, %phi_mul
  %tmp_43 = zext i32 %tmp_42 to i64
  %C_addr = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_43
  %C_load = load float* %C_addr, align 4
  %tmp_44 = zext i32 %j to i64
  %k_addr_1 = getelementptr inbounds [50 x float]* @k, i64 0, i64 %tmp_44
  %k_load = load float* %k_addr_1, align 4
  %tmp_45 = fmul float %C_load, %k_load
  %tmp_46 = fadd float %tmp_32, %tmp_45
  store float %tmp_46, float* %s_addr, align 4
  %Q_addr = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_43
  %Q_load = load float* %Q_addr, align 4
  %tmp_47 = fmul float %Q_load, %k_load
  %tmp_48 = fadd float %tmp_31, %tmp_47
  store float %tmp_48, float* %e_addr, align 4
  %empty_7 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str9, i32 %tmp_85)
  br label %6

; <label>:8                                       ; preds = %6
  %empty_8 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str8, i32 %tmp_84)
  br label %.preheader2

.preheader1:                                      ; preds = %.preheader2, %9
  %i2 = phi i32 [ %i_4, %9 ], [ 0, %.preheader2 ]
  %sigma2 = phi float [ %sigma2_1, %9 ], [ 1.000000e+00, %.preheader2 ]
  %exitcond2 = icmp eq i32 %i2, %bvCnt_load
  %i_4 = add i32 %i2, 1
  br i1 %exitcond2, label %10, label %9

; <label>:9                                       ; preds = %.preheader1
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str10) nounwind
  %tmp_86 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str10)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 50, i32 25, [1 x i8]* @p_str1) nounwind
  %tmp_34 = zext i32 %i2 to i64
  %s_addr_1 = getelementptr inbounds [51 x float]* @s, i64 0, i64 %tmp_34
  %s_load = load float* %s_addr_1, align 4
  %k_addr_2 = getelementptr inbounds [50 x float]* @k, i64 0, i64 %tmp_34
  %k_load_1 = load float* %k_addr_2, align 4
  %tmp_35 = fmul float %s_load, %k_load_1
  %sigma2_1 = fadd float %sigma2, %tmp_35
  %empty_9 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str10, i32 %tmp_86)
  br label %.preheader1

; <label>:10                                      ; preds = %.preheader1
  store float 1.000000e+00, float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 50), align 8
  %tmp_36 = fsub float %pY_read, %m
  %tmp_37 = fpext float %tmp_36 to double
  %tmp_38 = fpext float %sigma2 to double
  %tmp_39 = fadd double %tmp_38, 9.000000e-02
  %tmp_40 = fdiv double %tmp_37, %tmp_39
  %q = fptrunc double %tmp_40 to float
  %tmp_41 = fdiv double -1.000000e+00, %tmp_39
  %r = fptrunc double %tmp_41 to float
  br label %11

; <label>:11                                      ; preds = %12, %10
  %gamma = phi float [ 1.000000e+00, %10 ], [ %gamma_1, %12 ]
  %i3 = phi i32 [ 0, %10 ], [ %i_6, %12 ]
  %exitcond9 = icmp eq i32 %i3, %bvCnt_load
  %i_6 = add i32 %i3, 1
  br i1 %exitcond9, label %13, label %12

; <label>:12                                      ; preds = %11
  call void (...)* @_ssdm_op_SpecLoopName([13 x i8]* @p_str11) nounwind
  %tmp_87 = call i32 (...)* @_ssdm_op_SpecRegionBegin([13 x i8]* @p_str11)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 50, i32 25, [1 x i8]* @p_str1) nounwind
  %tmp_50 = zext i32 %i3 to i64
  %e_addr_1 = getelementptr inbounds [51 x float]* @e, i64 0, i64 %tmp_50
  %e_load = load float* %e_addr_1, align 4
  %k_addr_3 = getelementptr inbounds [50 x float]* @k, i64 0, i64 %tmp_50
  %k_load_2 = load float* %k_addr_3, align 4
  %tmp_51 = fmul float %e_load, %k_load_2
  %gamma_1 = fsub float %gamma, %tmp_51
  %s_addr_2 = getelementptr inbounds [51 x float]* @s, i64 0, i64 %tmp_50
  %s_load_1 = load float* %s_addr_2, align 4
  %tmp_52 = fmul float %s_load_1, %q
  %alpha_addr_1 = getelementptr inbounds [51 x float]* @alpha, i64 0, i64 %tmp_50
  %alpha_load_4 = load float* %alpha_addr_1, align 4
  %tmp_53 = fadd float %alpha_load_4, %tmp_52
  store float %tmp_53, float* %alpha_addr_1, align 4
  %empty_10 = call i32 (...)* @_ssdm_op_SpecRegionEnd([13 x i8]* @p_str11, i32 %tmp_87)
  br label %11

; <label>:13                                      ; preds = %11
  %alpha_load_5 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 50), align 8
  %tmp_54 = fadd float %alpha_load_5, %q
  store float %tmp_54, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 50), align 8
  %tmp_89 = add i32 %bvCnt_load, 1
  br label %14

; <label>:14                                      ; preds = %18, %13
  %i4 = phi i32 [ 0, %13 ], [ %i_7, %18 ]
  %phi_mul1 = phi i32 [ 0, %13 ], [ %next_mul1, %18 ]
  %next_mul1 = add i32 %phi_mul1, 51
  %exitcond8 = icmp eq i32 %i4, %tmp_89
  %i_7 = add i32 %i4, 1
  br i1 %exitcond8, label %.preheader.preheader, label %15

.preheader.preheader:                             ; preds = %14
  %tmp_56 = fpext float %gamma to double
  %cast = zext i32 %tmp_89 to i64
  %bound = mul i64 %cast, %cast
  br label %19

; <label>:15                                      ; preds = %14
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str12) nounwind
  %tmp_88 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str12)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 50, i32 25, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_57 = zext i32 %i4 to i64
  %s_addr_3 = getelementptr inbounds [51 x float]* @s, i64 0, i64 %tmp_57
  %s_load_2 = load float* %s_addr_3, align 4
  br label %16

; <label>:16                                      ; preds = %17, %15
  %j5 = phi i32 [ 0, %15 ], [ %j_2, %17 ]
  %exitcond = icmp eq i32 %j5, %tmp_89
  %j_2 = add i32 %j5, 1
  br i1 %exitcond, label %18, label %17

; <label>:17                                      ; preds = %16
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str13) nounwind
  %tmp_90 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str13)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 50, i32 25, [1 x i8]* @p_str1) nounwind
  %tmp_66 = zext i32 %j5 to i64
  %s_addr_4 = getelementptr inbounds [51 x float]* @s, i64 0, i64 %tmp_66
  %s_load_3 = load float* %s_addr_4, align 4
  %tmp_67 = fmul float %s_load_2, %s_load_3
  %tmp_68 = fmul float %tmp_67, %r
  %tmp_69 = add i32 %j5, %phi_mul1
  %tmp_70 = zext i32 %tmp_69 to i64
  %C_addr_6 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_70
  %C_load_5 = load float* %C_addr_6, align 4
  %tmp_71 = fadd float %C_load_5, %tmp_68
  store float %tmp_71, float* %C_addr_6, align 4
  %empty_11 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str13, i32 %tmp_90)
  br label %16

; <label>:18                                      ; preds = %16
  %empty_12 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str12, i32 %tmp_88)
  br label %14

; <label>:19                                      ; preds = %.preheader.preheader, %.preheader
  %indvar_flatten = phi i64 [ 0, %.preheader.preheader ], [ %indvar_flatten_next, %.preheader ]
  %i6 = phi i32 [ 0, %.preheader.preheader ], [ %i6_mid2, %.preheader ]
  %j7 = phi i32 [ 0, %.preheader.preheader ], [ %j_3, %.preheader ]
  %exitcond_flatten = icmp eq i64 %indvar_flatten, %bound
  %indvar_flatten_next = add i64 %indvar_flatten, 1
  br i1 %exitcond_flatten, label %.preheader9, label %.preheader

.preheader:                                       ; preds = %19
  call void (...)* @_ssdm_op_SpecLoopName([30 x i8]* @UPDATE_Q_OUTER_UPDATE_Q_INNER_s)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 0, i64 2500, i64 625)
  %tmp_92 = icmp ugt i32 %j7, %bvCnt_load
  %j7_mid2 = select i1 %tmp_92, i32 0, i32 %j7
  %i_1 = add i32 %i6, 1
  %i6_mid2 = select i1 %tmp_92, i32 %i_1, i32 %i6
  %tmp_63 = icmp eq i32 %i6_mid2, 50
  %tmp_64 = mul i32 %i6_mid2, 51
  %tmp_65 = zext i32 %i6_mid2 to i64
  %e_addr_2 = getelementptr inbounds [51 x float]* @e, i64 0, i64 %tmp_65
  %e_load_1 = load float* %e_addr_2, align 4
  %ti = select i1 %tmp_63, float -1.000000e+00, float %e_load_1
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str15) nounwind
  %tmp_91 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str15)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_73 = icmp eq i32 %j7_mid2, 50
  %tmp_74 = zext i32 %j7_mid2 to i64
  %e_addr_3 = getelementptr inbounds [51 x float]* @e, i64 0, i64 %tmp_74
  %e_load_2 = load float* %e_addr_3, align 4
  %tj = select i1 %tmp_73, float -1.000000e+00, float %e_load_2
  %tmp_75 = fmul float %ti, %tj
  %tmp_76 = fpext float %tmp_75 to double
  %tmp_77 = fdiv double %tmp_76, %tmp_56
  %tmp_78 = add i32 %j7_mid2, %tmp_64
  %tmp_79 = zext i32 %tmp_78 to i64
  %Q_addr_6 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_79
  %Q_load_5 = load float* %Q_addr_6, align 4
  %tmp_80 = fpext float %Q_load_5 to double
  %tmp_81 = fadd double %tmp_80, %tmp_77
  %tmp_82 = fptrunc double %tmp_81 to float
  store float %tmp_82, float* %Q_addr_6, align 4
  %empty_13 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str15, i32 %tmp_91)
  %j_3 = add i32 %j7_mid2, 1
  br label %19

.preheader9:                                      ; preds = %19, %20
  %bvCnt_load_8 = phi i32 [ %bvCnt_load_2, %20 ], [ %bvCnt_load, %19 ]
  %i_i2 = phi i5 [ %i_9, %20 ], [ 0, %19 ]
  %i_i2_cast4 = zext i5 %i_i2 to i32
  %exitcond_i3 = icmp eq i5 %i_i2, -12
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 20, i64 20, i64 20)
  %i_9 = add i5 %i_i2, 1
  br i1 %exitcond_i3, label %copyBV.exit, label %20

; <label>:20                                      ; preds = %.preheader9
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @p_str16) nounwind
  %tmp_i4 = zext i5 %i_i2 to i64
  %pX_addr_1 = getelementptr [20 x float]* %pX, i64 0, i64 %tmp_i4
  %pX_load_1 = load float* %pX_addr_1, align 4
  %bvCnt_load_2 = load i32* @bvCnt, align 4
  %tmp_33 = shl i32 %bvCnt_load_2, 4
  %tmp_49 = shl i32 %bvCnt_load_2, 2
  %tmp2 = add i32 %tmp_49, %i_i2_cast4
  %tmp_99_i = add i32 %tmp2, %tmp_33
  %tmp_100_i = zext i32 %tmp_99_i to i64
  %basisVectors_addr_2 = getelementptr inbounds [1020 x float]* @basisVectors, i64 0, i64 %tmp_100_i
  store float %pX_load_1, float* %basisVectors_addr_2, align 4
  br label %.preheader9

copyBV.exit:                                      ; preds = %.preheader9
  %tmp_61 = add i32 %bvCnt_load_8, 1
  store i32 %tmp_61, i32* @bvCnt, align 4
  %tmp_62 = icmp ugt i32 %tmp_61, 50
  br i1 %tmp_62, label %21, label %._crit_edge4

; <label>:21                                      ; preds = %copyBV.exit
  %alpha_load_53 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_i5 = fmul float %alpha_load_53, %alpha_load_53
  %Q_load_177 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 0), align 16
  %C_load_177 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 0), align 16
  %tmp_i6 = fadd float %Q_load_177, %C_load_177
  %minScore = fdiv float %tmp_i5, %tmp_i6
  br label %22

; <label>:22                                      ; preds = %23, %21
  %index_1 = phi i6 [ 1, %21 ], [ %i_10, %23 ]
  %minScore1_i = phi float [ %minScore, %21 ], [ %minScore_2, %23 ]
  %index = phi i32 [ 0, %21 ], [ %index_2, %23 ]
  %index_1_cast3 = zext i6 %index_1 to i32
  %index_1_cast3_cast = zext i6 %index_1 to i13
  %exitcond_i7 = icmp eq i6 %index_1, -13
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 50, i64 50, i64 50) nounwind
  br i1 %exitcond_i7, label %getMinKLApprox.exit, label %23

; <label>:23                                      ; preds = %22
  %tmp_17_i = zext i6 %index_1 to i64
  %alpha_addr_2 = getelementptr inbounds [51 x float]* @alpha, i64 0, i64 %tmp_17_i
  %alpha_load_54 = load float* %alpha_addr_2, align 4
  %tmp_18_i = fmul float %alpha_load_54, %alpha_load_54
  %tmp_19_i = mul i13 52, %index_1_cast3_cast
  %tmp_20_i = zext i13 %tmp_19_i to i64
  %Q_addr_51 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_20_i
  %Q_load_178 = load float* %Q_addr_51, align 16
  %C_addr_51 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_20_i
  %C_load_178 = load float* %C_addr_51, align 16
  %tmp_21_i = fadd float %Q_load_178, %C_load_178
  %tScore = fdiv float %tmp_18_i, %tmp_21_i
  %tScore_to_int = bitcast float %tScore to i32
  %tmp_1 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tScore_to_int, i32 23, i32 30)
  %tmp_55 = trunc i32 %tScore_to_int to i23
  %minScore1_i_to_int = bitcast float %minScore1_i to i32
  %tmp_3 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %minScore1_i_to_int, i32 23, i32 30)
  %tmp_58 = trunc i32 %minScore1_i_to_int to i23
  %notlhs = icmp ne i8 %tmp_1, -1
  %notrhs = icmp eq i23 %tmp_55, 0
  %tmp_5 = or i1 %notrhs, %notlhs
  %notlhs7 = icmp ne i8 %tmp_3, -1
  %notrhs8 = icmp eq i23 %tmp_58, 0
  %tmp_6 = or i1 %notrhs8, %notlhs7
  %tmp_7 = and i1 %tmp_5, %tmp_6
  %tmp_8 = fcmp olt float %tScore, %minScore1_i
  %tmp_9 = and i1 %tmp_7, %tmp_8
  %minScore_2 = select i1 %tmp_9, float %tScore, float %minScore1_i
  %index_2 = select i1 %tmp_9, i32 %index_1_cast3, i32 %index
  %i_10 = add i6 1, %index_1
  br label %22

getMinKLApprox.exit:                              ; preds = %22
  call fastcc void @projection_gp_deleteBV(i32 %index)
  br label %._crit_edge4

._crit_edge4:                                     ; preds = %getMinKLApprox.exit, %copyBV.exit
  ret void
}

define internal fastcc void @projection_gp_train_full_bv_set([20 x float]* %pX, float %pY) {
.preheader10.preheader:
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([20 x float]* %pX, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3)
  %pY_read = call float @_ssdm_op_Read.ap_auto.float(float %pY)
  %tmp_93 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 0, [20 x float]* %pX)
  store float %tmp_93, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 0), align 16
  %alpha_load = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_103 = fmul float %tmp_93, %alpha_load
  %m_2 = fadd float %tmp_103, 5.000000e+00
  %tmp_108 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 20, [20 x float]* %pX)
  store float %tmp_108, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 1), align 4
  %alpha_load_1 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 1), align 4
  %tmp_103_1 = fmul float %tmp_108, %alpha_load_1
  %m_2_1 = fadd float %m_2, %tmp_103_1
  %tmp_109 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 40, [20 x float]* %pX)
  store float %tmp_109, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 2), align 8
  %alpha_load_2 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 2), align 8
  %tmp_103_2 = fmul float %tmp_109, %alpha_load_2
  %m_2_2 = fadd float %m_2_1, %tmp_103_2
  %tmp_110 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 60, [20 x float]* %pX)
  store float %tmp_110, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 3), align 4
  %alpha_load_3 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 3), align 4
  %tmp_103_3 = fmul float %tmp_110, %alpha_load_3
  %m_2_3 = fadd float %m_2_2, %tmp_103_3
  %tmp_111 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 80, [20 x float]* %pX)
  store float %tmp_111, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 4), align 16
  %alpha_load_4 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 4), align 16
  %tmp_103_4 = fmul float %tmp_111, %alpha_load_4
  %m_2_4 = fadd float %m_2_3, %tmp_103_4
  %tmp_112 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 100, [20 x float]* %pX)
  store float %tmp_112, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 5), align 4
  %alpha_load_5 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 5), align 4
  %tmp_103_5 = fmul float %tmp_112, %alpha_load_5
  %m_2_5 = fadd float %m_2_4, %tmp_103_5
  %tmp_113 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 120, [20 x float]* %pX)
  store float %tmp_113, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 6), align 8
  %alpha_load_53 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 6), align 8
  %tmp_103_6 = fmul float %tmp_113, %alpha_load_53
  %m_2_6 = fadd float %m_2_5, %tmp_103_6
  %tmp_114 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 140, [20 x float]* %pX)
  store float %tmp_114, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 7), align 4
  %alpha_load_54 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 7), align 4
  %tmp_103_7 = fmul float %tmp_114, %alpha_load_54
  %m_2_7 = fadd float %m_2_6, %tmp_103_7
  %tmp_116 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 160, [20 x float]* %pX)
  store float %tmp_116, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 8), align 16
  %alpha_load_8 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 8), align 16
  %tmp_103_8 = fmul float %tmp_116, %alpha_load_8
  %m_2_8 = fadd float %m_2_7, %tmp_103_8
  %tmp_118 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 180, [20 x float]* %pX)
  store float %tmp_118, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 9), align 4
  %alpha_load_9 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 9), align 4
  %tmp_103_9 = fmul float %tmp_118, %alpha_load_9
  %m_2_9 = fadd float %m_2_8, %tmp_103_9
  %tmp_123 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 200, [20 x float]* %pX)
  store float %tmp_123, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 10), align 8
  %alpha_load_10 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 10), align 8
  %tmp_103_s = fmul float %tmp_123, %alpha_load_10
  %m_2_s = fadd float %m_2_9, %tmp_103_s
  %tmp_124 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 220, [20 x float]* %pX)
  store float %tmp_124, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 11), align 4
  %alpha_load_11 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 11), align 4
  %tmp_103_10 = fmul float %tmp_124, %alpha_load_11
  %m_2_10 = fadd float %m_2_s, %tmp_103_10
  %tmp_128 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 240, [20 x float]* %pX)
  store float %tmp_128, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 12), align 16
  %alpha_load_12 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 12), align 16
  %tmp_103_11 = fmul float %tmp_128, %alpha_load_12
  %m_2_11 = fadd float %m_2_10, %tmp_103_11
  %tmp_129 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 260, [20 x float]* %pX)
  store float %tmp_129, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 13), align 4
  %alpha_load_13 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 13), align 4
  %tmp_103_12 = fmul float %tmp_129, %alpha_load_13
  %m_2_12 = fadd float %m_2_11, %tmp_103_12
  %tmp_130 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 280, [20 x float]* %pX)
  store float %tmp_130, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 14), align 8
  %alpha_load_14 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 14), align 8
  %tmp_103_13 = fmul float %tmp_130, %alpha_load_14
  %m_2_13 = fadd float %m_2_12, %tmp_103_13
  %tmp_131 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 300, [20 x float]* %pX)
  store float %tmp_131, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 15), align 4
  %alpha_load_15 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 15), align 4
  %tmp_103_14 = fmul float %tmp_131, %alpha_load_15
  %m_2_14 = fadd float %m_2_13, %tmp_103_14
  %tmp_132 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 320, [20 x float]* %pX)
  store float %tmp_132, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 16), align 16
  %alpha_load_16 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 16), align 16
  %tmp_103_15 = fmul float %tmp_132, %alpha_load_16
  %m_2_15 = fadd float %m_2_14, %tmp_103_15
  %tmp_133 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 340, [20 x float]* %pX)
  store float %tmp_133, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 17), align 4
  %alpha_load_17 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 17), align 4
  %tmp_103_16 = fmul float %tmp_133, %alpha_load_17
  %m_2_16 = fadd float %m_2_15, %tmp_103_16
  %tmp_134 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 360, [20 x float]* %pX)
  store float %tmp_134, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 18), align 8
  %alpha_load_18 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 18), align 8
  %tmp_103_17 = fmul float %tmp_134, %alpha_load_18
  %m_2_17 = fadd float %m_2_16, %tmp_103_17
  %tmp_137 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 380, [20 x float]* %pX)
  store float %tmp_137, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 19), align 4
  %alpha_load_19 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 19), align 4
  %tmp_103_18 = fmul float %tmp_137, %alpha_load_19
  %m_2_18 = fadd float %m_2_17, %tmp_103_18
  %tmp_140 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 400, [20 x float]* %pX)
  store float %tmp_140, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 20), align 16
  %alpha_load_20 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 20), align 16
  %tmp_103_19 = fmul float %tmp_140, %alpha_load_20
  %m_2_19 = fadd float %m_2_18, %tmp_103_19
  %tmp_141 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 420, [20 x float]* %pX)
  store float %tmp_141, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 21), align 4
  %alpha_load_21 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 21), align 4
  %tmp_103_20 = fmul float %tmp_141, %alpha_load_21
  %m_2_20 = fadd float %m_2_19, %tmp_103_20
  %tmp_142 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 440, [20 x float]* %pX)
  store float %tmp_142, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 22), align 8
  %alpha_load_22 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 22), align 8
  %tmp_103_21 = fmul float %tmp_142, %alpha_load_22
  %m_2_21 = fadd float %m_2_20, %tmp_103_21
  %tmp_143 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 460, [20 x float]* %pX)
  store float %tmp_143, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 23), align 4
  %alpha_load_23 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 23), align 4
  %tmp_103_22 = fmul float %tmp_143, %alpha_load_23
  %m_2_22 = fadd float %m_2_21, %tmp_103_22
  %tmp_144 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 480, [20 x float]* %pX)
  store float %tmp_144, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 24), align 16
  %alpha_load_24 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 24), align 16
  %tmp_103_23 = fmul float %tmp_144, %alpha_load_24
  %m_2_23 = fadd float %m_2_22, %tmp_103_23
  %tmp_145 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 500, [20 x float]* %pX)
  store float %tmp_145, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 25), align 4
  %alpha_load_25 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 25), align 4
  %tmp_103_24 = fmul float %tmp_145, %alpha_load_25
  %m_2_24 = fadd float %m_2_23, %tmp_103_24
  %tmp_146 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 520, [20 x float]* %pX)
  store float %tmp_146, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 26), align 8
  %alpha_load_26 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 26), align 8
  %tmp_103_25 = fmul float %tmp_146, %alpha_load_26
  %m_2_25 = fadd float %m_2_24, %tmp_103_25
  %tmp_147 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 540, [20 x float]* %pX)
  store float %tmp_147, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 27), align 4
  %alpha_load_27 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 27), align 4
  %tmp_103_26 = fmul float %tmp_147, %alpha_load_27
  %m_2_26 = fadd float %m_2_25, %tmp_103_26
  %tmp_148 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 560, [20 x float]* %pX)
  store float %tmp_148, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 28), align 16
  %alpha_load_28 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 28), align 16
  %tmp_103_27 = fmul float %tmp_148, %alpha_load_28
  %m_2_27 = fadd float %m_2_26, %tmp_103_27
  %tmp_149 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 580, [20 x float]* %pX)
  store float %tmp_149, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 29), align 4
  %alpha_load_29 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 29), align 4
  %tmp_103_28 = fmul float %tmp_149, %alpha_load_29
  %m_2_28 = fadd float %m_2_27, %tmp_103_28
  %tmp_150 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 600, [20 x float]* %pX)
  store float %tmp_150, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 30), align 8
  %alpha_load_30 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 30), align 8
  %tmp_103_29 = fmul float %tmp_150, %alpha_load_30
  %m_2_29 = fadd float %m_2_28, %tmp_103_29
  %tmp_151 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 620, [20 x float]* %pX)
  store float %tmp_151, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 31), align 4
  %alpha_load_31 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 31), align 4
  %tmp_103_30 = fmul float %tmp_151, %alpha_load_31
  %m_2_30 = fadd float %m_2_29, %tmp_103_30
  %tmp_152 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 640, [20 x float]* %pX)
  store float %tmp_152, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 32), align 16
  %alpha_load_32 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 32), align 16
  %tmp_103_31 = fmul float %tmp_152, %alpha_load_32
  %m_2_31 = fadd float %m_2_30, %tmp_103_31
  %tmp_153 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 660, [20 x float]* %pX)
  store float %tmp_153, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 33), align 4
  %alpha_load_33 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 33), align 4
  %tmp_103_32 = fmul float %tmp_153, %alpha_load_33
  %m_2_32 = fadd float %m_2_31, %tmp_103_32
  %tmp_154 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 680, [20 x float]* %pX)
  store float %tmp_154, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 34), align 8
  %alpha_load_34 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 34), align 8
  %tmp_103_33 = fmul float %tmp_154, %alpha_load_34
  %m_2_33 = fadd float %m_2_32, %tmp_103_33
  %tmp_155 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 700, [20 x float]* %pX)
  store float %tmp_155, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 35), align 4
  %alpha_load_35 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 35), align 4
  %tmp_103_34 = fmul float %tmp_155, %alpha_load_35
  %m_2_34 = fadd float %m_2_33, %tmp_103_34
  %tmp_156 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 720, [20 x float]* %pX)
  store float %tmp_156, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 36), align 16
  %alpha_load_36 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 36), align 16
  %tmp_103_35 = fmul float %tmp_156, %alpha_load_36
  %m_2_35 = fadd float %m_2_34, %tmp_103_35
  %tmp_157 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 740, [20 x float]* %pX)
  store float %tmp_157, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 37), align 4
  %alpha_load_37 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 37), align 4
  %tmp_103_36 = fmul float %tmp_157, %alpha_load_37
  %m_2_36 = fadd float %m_2_35, %tmp_103_36
  %tmp_158 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 760, [20 x float]* %pX)
  store float %tmp_158, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 38), align 8
  %alpha_load_38 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 38), align 8
  %tmp_103_37 = fmul float %tmp_158, %alpha_load_38
  %m_2_37 = fadd float %m_2_36, %tmp_103_37
  %tmp_159 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 780, [20 x float]* %pX)
  store float %tmp_159, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 39), align 4
  %alpha_load_39 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 39), align 4
  %tmp_103_38 = fmul float %tmp_159, %alpha_load_39
  %m_2_38 = fadd float %m_2_37, %tmp_103_38
  %tmp_160 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 800, [20 x float]* %pX)
  store float %tmp_160, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 40), align 16
  %alpha_load_40 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 40), align 16
  %tmp_103_39 = fmul float %tmp_160, %alpha_load_40
  %m_2_39 = fadd float %m_2_38, %tmp_103_39
  %tmp_161 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 820, [20 x float]* %pX)
  store float %tmp_161, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 41), align 4
  %alpha_load_41 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 41), align 4
  %tmp_103_40 = fmul float %tmp_161, %alpha_load_41
  %m_2_40 = fadd float %m_2_39, %tmp_103_40
  %tmp_162 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 840, [20 x float]* %pX)
  store float %tmp_162, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 42), align 8
  %alpha_load_42 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 42), align 8
  %tmp_103_41 = fmul float %tmp_162, %alpha_load_42
  %m_2_41 = fadd float %m_2_40, %tmp_103_41
  %tmp_163 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 860, [20 x float]* %pX)
  store float %tmp_163, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 43), align 4
  %alpha_load_43 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 43), align 4
  %tmp_103_42 = fmul float %tmp_163, %alpha_load_43
  %m_2_42 = fadd float %m_2_41, %tmp_103_42
  %tmp_164 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 880, [20 x float]* %pX)
  store float %tmp_164, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 44), align 16
  %alpha_load_44 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 44), align 16
  %tmp_103_43 = fmul float %tmp_164, %alpha_load_44
  %m_2_43 = fadd float %m_2_42, %tmp_103_43
  %tmp_165 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 900, [20 x float]* %pX)
  store float %tmp_165, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 45), align 4
  %alpha_load_45 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 45), align 4
  %tmp_103_44 = fmul float %tmp_165, %alpha_load_45
  %m_2_44 = fadd float %m_2_43, %tmp_103_44
  %tmp_166 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 920, [20 x float]* %pX)
  store float %tmp_166, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 46), align 8
  %alpha_load_46 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 46), align 8
  %tmp_103_45 = fmul float %tmp_166, %alpha_load_46
  %m_2_45 = fadd float %m_2_44, %tmp_103_45
  %tmp_167 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 940, [20 x float]* %pX)
  store float %tmp_167, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 47), align 4
  %alpha_load_47 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 47), align 4
  %tmp_103_46 = fmul float %tmp_167, %alpha_load_47
  %m_2_46 = fadd float %m_2_45, %tmp_103_46
  %tmp_168 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 960, [20 x float]* %pX)
  store float %tmp_168, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 48), align 16
  %alpha_load_48 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 48), align 16
  %tmp_103_47 = fmul float %tmp_168, %alpha_load_48
  %m_2_47 = fadd float %m_2_46, %tmp_103_47
  %tmp_169 = call fastcc float @projection_gp_K([1020 x float]* @basisVectors, i11 980, [20 x float]* %pX)
  store float %tmp_169, float* getelementptr inbounds ([50 x float]* @k, i64 0, i64 49), align 4
  %alpha_load_49 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 49), align 4
  %tmp_103_48 = fmul float %tmp_169, %alpha_load_49
  %m_2_48 = fadd float %m_2_47, %tmp_103_48
  br label %.preheader10

.preheader10:                                     ; preds = %0, %.preheader10.preheader
  %i1 = phi i6 [ %i, %0 ], [ 0, %.preheader10.preheader ]
  %phi_mul = phi i12 [ %next_mul, %0 ], [ 0, %.preheader10.preheader ]
  %exitcond7 = icmp eq i6 %i1, -14
  %i = add i6 %i1, 1
  br i1 %exitcond7, label %.preheader9.0, label %0

; <label>:0                                       ; preds = %.preheader10
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 50, i64 50, i64 50)
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str8) nounwind
  %tmp_170 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str8)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_89 = zext i6 %i1 to i64
  %s_addr = getelementptr inbounds [51 x float]* @s, i64 0, i64 %tmp_89
  %e_addr = getelementptr inbounds [51 x float]* @e, i64 0, i64 %tmp_89
  %next_mul = add i12 %phi_mul, 51
  %tmp_117 = zext i12 %phi_mul to i64
  %C_addr = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117
  %C_load = load float* %C_addr, align 4
  %tmp_119 = fmul float %C_load, %tmp_93
  %tmp_120 = fadd float %tmp_119, 0.000000e+00
  %Q_addr = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117
  %Q_load = load float* %Q_addr, align 4
  %tmp_121 = fmul float %Q_load, %tmp_93
  %tmp_122 = fadd float %tmp_121, 0.000000e+00
  %tmp_116_1 = add i12 %phi_mul, 1
  %tmp_117_1 = zext i12 %tmp_116_1 to i64
  %C_addr_1 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_1
  %C_load_1 = load float* %C_addr_1, align 4
  %tmp_119_1 = fmul float %C_load_1, %tmp_108
  %tmp_120_1 = fadd float %tmp_120, %tmp_119_1
  %Q_addr_1 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_1
  %Q_load_1 = load float* %Q_addr_1, align 4
  %tmp_121_1 = fmul float %Q_load_1, %tmp_108
  %tmp_122_1 = fadd float %tmp_122, %tmp_121_1
  %tmp_116_2 = add i12 %phi_mul, 2
  %tmp_117_2 = zext i12 %tmp_116_2 to i64
  %C_addr_2 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_2
  %C_load_2 = load float* %C_addr_2, align 4
  %tmp_119_2 = fmul float %C_load_2, %tmp_109
  %tmp_120_2 = fadd float %tmp_120_1, %tmp_119_2
  %Q_addr_2 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_2
  %Q_load_2 = load float* %Q_addr_2, align 4
  %tmp_121_2 = fmul float %Q_load_2, %tmp_109
  %tmp_122_2 = fadd float %tmp_122_1, %tmp_121_2
  %tmp_116_3 = add i12 %phi_mul, 3
  %tmp_117_3 = zext i12 %tmp_116_3 to i64
  %C_addr_3 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_3
  %C_load_3 = load float* %C_addr_3, align 4
  %tmp_119_3 = fmul float %C_load_3, %tmp_110
  %tmp_120_3 = fadd float %tmp_120_2, %tmp_119_3
  %Q_addr_3 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_3
  %Q_load_3 = load float* %Q_addr_3, align 4
  %tmp_121_3 = fmul float %Q_load_3, %tmp_110
  %tmp_122_3 = fadd float %tmp_122_2, %tmp_121_3
  %tmp_116_4 = add i12 %phi_mul, 4
  %tmp_117_4 = zext i12 %tmp_116_4 to i64
  %C_addr_4 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_4
  %C_load_4 = load float* %C_addr_4, align 4
  %tmp_119_4 = fmul float %C_load_4, %tmp_111
  %tmp_120_4 = fadd float %tmp_120_3, %tmp_119_4
  %Q_addr_4 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_4
  %Q_load_4 = load float* %Q_addr_4, align 4
  %tmp_121_4 = fmul float %Q_load_4, %tmp_111
  %tmp_122_4 = fadd float %tmp_122_3, %tmp_121_4
  %tmp_116_5 = add i12 %phi_mul, 5
  %tmp_117_5 = zext i12 %tmp_116_5 to i64
  %C_addr_5 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_5
  %C_load_5 = load float* %C_addr_5, align 4
  %tmp_119_5 = fmul float %C_load_5, %tmp_112
  %tmp_120_5 = fadd float %tmp_120_4, %tmp_119_5
  %Q_addr_5 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_5
  %Q_load_5 = load float* %Q_addr_5, align 4
  %tmp_121_5 = fmul float %Q_load_5, %tmp_112
  %tmp_122_5 = fadd float %tmp_122_4, %tmp_121_5
  %tmp_116_6 = add i12 %phi_mul, 6
  %tmp_117_6 = zext i12 %tmp_116_6 to i64
  %C_addr_6 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_6
  %C_load_176 = load float* %C_addr_6, align 4
  %tmp_119_6 = fmul float %C_load_176, %tmp_113
  %tmp_120_6 = fadd float %tmp_120_5, %tmp_119_6
  %Q_addr_6 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_6
  %Q_load_176 = load float* %Q_addr_6, align 4
  %tmp_121_6 = fmul float %Q_load_176, %tmp_113
  %tmp_122_6 = fadd float %tmp_122_5, %tmp_121_6
  %tmp_116_7 = add i12 %phi_mul, 7
  %tmp_117_7 = zext i12 %tmp_116_7 to i64
  %C_addr_51 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_7
  %C_load_7 = load float* %C_addr_51, align 4
  %tmp_119_7 = fmul float %C_load_7, %tmp_114
  %tmp_120_7 = fadd float %tmp_120_6, %tmp_119_7
  %Q_addr_51 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_7
  %Q_load_7 = load float* %Q_addr_51, align 4
  %tmp_121_7 = fmul float %Q_load_7, %tmp_114
  %tmp_122_7 = fadd float %tmp_122_6, %tmp_121_7
  %tmp_116_8 = add i12 %phi_mul, 8
  %tmp_117_8 = zext i12 %tmp_116_8 to i64
  %C_addr_8 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_8
  %C_load_8 = load float* %C_addr_8, align 4
  %tmp_119_8 = fmul float %C_load_8, %tmp_116
  %tmp_120_8 = fadd float %tmp_120_7, %tmp_119_8
  %Q_addr_8 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_8
  %Q_load_8 = load float* %Q_addr_8, align 4
  %tmp_121_8 = fmul float %Q_load_8, %tmp_116
  %tmp_122_8 = fadd float %tmp_122_7, %tmp_121_8
  %tmp_116_9 = add i12 %phi_mul, 9
  %tmp_117_9 = zext i12 %tmp_116_9 to i64
  %C_addr_9 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_9
  %C_load_9 = load float* %C_addr_9, align 4
  %tmp_119_9 = fmul float %C_load_9, %tmp_118
  %tmp_120_9 = fadd float %tmp_120_8, %tmp_119_9
  %Q_addr_9 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_9
  %Q_load_9 = load float* %Q_addr_9, align 4
  %tmp_121_9 = fmul float %Q_load_9, %tmp_118
  %tmp_122_9 = fadd float %tmp_122_8, %tmp_121_9
  %tmp_116_s = add i12 %phi_mul, 10
  %tmp_117_s = zext i12 %tmp_116_s to i64
  %C_addr_10 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_s
  %C_load_10 = load float* %C_addr_10, align 4
  %tmp_119_s = fmul float %C_load_10, %tmp_123
  %tmp_120_s = fadd float %tmp_120_9, %tmp_119_s
  %Q_addr_10 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_s
  %Q_load_10 = load float* %Q_addr_10, align 4
  %tmp_121_s = fmul float %Q_load_10, %tmp_123
  %tmp_122_s = fadd float %tmp_122_9, %tmp_121_s
  %tmp_116_10 = add i12 %phi_mul, 11
  %tmp_117_10 = zext i12 %tmp_116_10 to i64
  %C_addr_11 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_10
  %C_load_11 = load float* %C_addr_11, align 4
  %tmp_119_10 = fmul float %C_load_11, %tmp_124
  %tmp_120_10 = fadd float %tmp_120_s, %tmp_119_10
  %Q_addr_11 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_10
  %Q_load_11 = load float* %Q_addr_11, align 4
  %tmp_121_10 = fmul float %Q_load_11, %tmp_124
  %tmp_122_10 = fadd float %tmp_122_s, %tmp_121_10
  %tmp_116_11 = add i12 %phi_mul, 12
  %tmp_117_11 = zext i12 %tmp_116_11 to i64
  %C_addr_12 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_11
  %C_load_12 = load float* %C_addr_12, align 4
  %tmp_119_11 = fmul float %C_load_12, %tmp_128
  %tmp_120_11 = fadd float %tmp_120_10, %tmp_119_11
  %Q_addr_12 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_11
  %Q_load_12 = load float* %Q_addr_12, align 4
  %tmp_121_11 = fmul float %Q_load_12, %tmp_128
  %tmp_122_11 = fadd float %tmp_122_10, %tmp_121_11
  %tmp_116_12 = add i12 %phi_mul, 13
  %tmp_117_12 = zext i12 %tmp_116_12 to i64
  %C_addr_13 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_12
  %C_load_13 = load float* %C_addr_13, align 4
  %tmp_119_12 = fmul float %C_load_13, %tmp_129
  %tmp_120_12 = fadd float %tmp_120_11, %tmp_119_12
  %Q_addr_13 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_12
  %Q_load_13 = load float* %Q_addr_13, align 4
  %tmp_121_12 = fmul float %Q_load_13, %tmp_129
  %tmp_122_12 = fadd float %tmp_122_11, %tmp_121_12
  %tmp_116_13 = add i12 %phi_mul, 14
  %tmp_117_13 = zext i12 %tmp_116_13 to i64
  %C_addr_14 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_13
  %C_load_14 = load float* %C_addr_14, align 4
  %tmp_119_13 = fmul float %C_load_14, %tmp_130
  %tmp_120_13 = fadd float %tmp_120_12, %tmp_119_13
  %Q_addr_14 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_13
  %Q_load_14 = load float* %Q_addr_14, align 4
  %tmp_121_13 = fmul float %Q_load_14, %tmp_130
  %tmp_122_13 = fadd float %tmp_122_12, %tmp_121_13
  %tmp_116_14 = add i12 %phi_mul, 15
  %tmp_117_14 = zext i12 %tmp_116_14 to i64
  %C_addr_15 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_14
  %C_load_15 = load float* %C_addr_15, align 4
  %tmp_119_14 = fmul float %C_load_15, %tmp_131
  %tmp_120_14 = fadd float %tmp_120_13, %tmp_119_14
  %Q_addr_15 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_14
  %Q_load_15 = load float* %Q_addr_15, align 4
  %tmp_121_14 = fmul float %Q_load_15, %tmp_131
  %tmp_122_14 = fadd float %tmp_122_13, %tmp_121_14
  %tmp_116_15 = add i12 %phi_mul, 16
  %tmp_117_15 = zext i12 %tmp_116_15 to i64
  %C_addr_16 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_15
  %C_load_16 = load float* %C_addr_16, align 4
  %tmp_119_15 = fmul float %C_load_16, %tmp_132
  %tmp_120_15 = fadd float %tmp_120_14, %tmp_119_15
  %Q_addr_16 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_15
  %Q_load_16 = load float* %Q_addr_16, align 4
  %tmp_121_15 = fmul float %Q_load_16, %tmp_132
  %tmp_122_15 = fadd float %tmp_122_14, %tmp_121_15
  %tmp_116_16 = add i12 %phi_mul, 17
  %tmp_117_16 = zext i12 %tmp_116_16 to i64
  %C_addr_17 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_16
  %C_load_17 = load float* %C_addr_17, align 4
  %tmp_119_16 = fmul float %C_load_17, %tmp_133
  %tmp_120_16 = fadd float %tmp_120_15, %tmp_119_16
  %Q_addr_17 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_16
  %Q_load_17 = load float* %Q_addr_17, align 4
  %tmp_121_16 = fmul float %Q_load_17, %tmp_133
  %tmp_122_16 = fadd float %tmp_122_15, %tmp_121_16
  %tmp_116_17 = add i12 %phi_mul, 18
  %tmp_117_17 = zext i12 %tmp_116_17 to i64
  %C_addr_18 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_17
  %C_load_18 = load float* %C_addr_18, align 4
  %tmp_119_17 = fmul float %C_load_18, %tmp_134
  %tmp_120_17 = fadd float %tmp_120_16, %tmp_119_17
  %Q_addr_18 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_17
  %Q_load_18 = load float* %Q_addr_18, align 4
  %tmp_121_17 = fmul float %Q_load_18, %tmp_134
  %tmp_122_17 = fadd float %tmp_122_16, %tmp_121_17
  %tmp_116_18 = add i12 %phi_mul, 19
  %tmp_117_18 = zext i12 %tmp_116_18 to i64
  %C_addr_19 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_18
  %C_load_19 = load float* %C_addr_19, align 4
  %tmp_119_18 = fmul float %C_load_19, %tmp_137
  %tmp_120_18 = fadd float %tmp_120_17, %tmp_119_18
  %Q_addr_19 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_18
  %Q_load_19 = load float* %Q_addr_19, align 4
  %tmp_121_18 = fmul float %Q_load_19, %tmp_137
  %tmp_122_18 = fadd float %tmp_122_17, %tmp_121_18
  %tmp_116_19 = add i12 %phi_mul, 20
  %tmp_117_19 = zext i12 %tmp_116_19 to i64
  %C_addr_20 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_19
  %C_load_20 = load float* %C_addr_20, align 4
  %tmp_119_19 = fmul float %C_load_20, %tmp_140
  %tmp_120_19 = fadd float %tmp_120_18, %tmp_119_19
  %Q_addr_20 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_19
  %Q_load_20 = load float* %Q_addr_20, align 4
  %tmp_121_19 = fmul float %Q_load_20, %tmp_140
  %tmp_122_19 = fadd float %tmp_122_18, %tmp_121_19
  %tmp_116_20 = add i12 %phi_mul, 21
  %tmp_117_20 = zext i12 %tmp_116_20 to i64
  %C_addr_21 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_20
  %C_load_21 = load float* %C_addr_21, align 4
  %tmp_119_20 = fmul float %C_load_21, %tmp_141
  %tmp_120_20 = fadd float %tmp_120_19, %tmp_119_20
  %Q_addr_21 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_20
  %Q_load_21 = load float* %Q_addr_21, align 4
  %tmp_121_20 = fmul float %Q_load_21, %tmp_141
  %tmp_122_20 = fadd float %tmp_122_19, %tmp_121_20
  %tmp_116_21 = add i12 %phi_mul, 22
  %tmp_117_21 = zext i12 %tmp_116_21 to i64
  %C_addr_22 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_21
  %C_load_22 = load float* %C_addr_22, align 4
  %tmp_119_21 = fmul float %C_load_22, %tmp_142
  %tmp_120_21 = fadd float %tmp_120_20, %tmp_119_21
  %Q_addr_22 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_21
  %Q_load_22 = load float* %Q_addr_22, align 4
  %tmp_121_21 = fmul float %Q_load_22, %tmp_142
  %tmp_122_21 = fadd float %tmp_122_20, %tmp_121_21
  %tmp_116_22 = add i12 %phi_mul, 23
  %tmp_117_22 = zext i12 %tmp_116_22 to i64
  %C_addr_23 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_22
  %C_load_23 = load float* %C_addr_23, align 4
  %tmp_119_22 = fmul float %C_load_23, %tmp_143
  %tmp_120_22 = fadd float %tmp_120_21, %tmp_119_22
  %Q_addr_23 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_22
  %Q_load_23 = load float* %Q_addr_23, align 4
  %tmp_121_22 = fmul float %Q_load_23, %tmp_143
  %tmp_122_22 = fadd float %tmp_122_21, %tmp_121_22
  %tmp_116_23 = add i12 %phi_mul, 24
  %tmp_117_23 = zext i12 %tmp_116_23 to i64
  %C_addr_24 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_23
  %C_load_24 = load float* %C_addr_24, align 4
  %tmp_119_23 = fmul float %C_load_24, %tmp_144
  %tmp_120_23 = fadd float %tmp_120_22, %tmp_119_23
  %Q_addr_24 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_23
  %Q_load_24 = load float* %Q_addr_24, align 4
  %tmp_121_23 = fmul float %Q_load_24, %tmp_144
  %tmp_122_23 = fadd float %tmp_122_22, %tmp_121_23
  %tmp_116_24 = add i12 %phi_mul, 25
  %tmp_117_24 = zext i12 %tmp_116_24 to i64
  %C_addr_25 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_24
  %C_load_25 = load float* %C_addr_25, align 4
  %tmp_119_24 = fmul float %C_load_25, %tmp_145
  %tmp_120_24 = fadd float %tmp_120_23, %tmp_119_24
  %Q_addr_25 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_24
  %Q_load_25 = load float* %Q_addr_25, align 4
  %tmp_121_24 = fmul float %Q_load_25, %tmp_145
  %tmp_122_24 = fadd float %tmp_122_23, %tmp_121_24
  %tmp_116_25 = add i12 %phi_mul, 26
  %tmp_117_25 = zext i12 %tmp_116_25 to i64
  %C_addr_26 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_25
  %C_load_26 = load float* %C_addr_26, align 4
  %tmp_119_25 = fmul float %C_load_26, %tmp_146
  %tmp_120_25 = fadd float %tmp_120_24, %tmp_119_25
  %Q_addr_26 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_25
  %Q_load_26 = load float* %Q_addr_26, align 4
  %tmp_121_25 = fmul float %Q_load_26, %tmp_146
  %tmp_122_25 = fadd float %tmp_122_24, %tmp_121_25
  %tmp_116_26 = add i12 %phi_mul, 27
  %tmp_117_26 = zext i12 %tmp_116_26 to i64
  %C_addr_27 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_26
  %C_load_27 = load float* %C_addr_27, align 4
  %tmp_119_26 = fmul float %C_load_27, %tmp_147
  %tmp_120_26 = fadd float %tmp_120_25, %tmp_119_26
  %Q_addr_27 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_26
  %Q_load_27 = load float* %Q_addr_27, align 4
  %tmp_121_26 = fmul float %Q_load_27, %tmp_147
  %tmp_122_26 = fadd float %tmp_122_25, %tmp_121_26
  %tmp_116_27 = add i12 %phi_mul, 28
  %tmp_117_27 = zext i12 %tmp_116_27 to i64
  %C_addr_28 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_27
  %C_load_28 = load float* %C_addr_28, align 4
  %tmp_119_27 = fmul float %C_load_28, %tmp_148
  %tmp_120_27 = fadd float %tmp_120_26, %tmp_119_27
  %Q_addr_28 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_27
  %Q_load_28 = load float* %Q_addr_28, align 4
  %tmp_121_27 = fmul float %Q_load_28, %tmp_148
  %tmp_122_27 = fadd float %tmp_122_26, %tmp_121_27
  %tmp_116_28 = add i12 %phi_mul, 29
  %tmp_117_28 = zext i12 %tmp_116_28 to i64
  %C_addr_29 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_28
  %C_load_29 = load float* %C_addr_29, align 4
  %tmp_119_28 = fmul float %C_load_29, %tmp_149
  %tmp_120_28 = fadd float %tmp_120_27, %tmp_119_28
  %Q_addr_29 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_28
  %Q_load_29 = load float* %Q_addr_29, align 4
  %tmp_121_28 = fmul float %Q_load_29, %tmp_149
  %tmp_122_28 = fadd float %tmp_122_27, %tmp_121_28
  %tmp_116_29 = add i12 %phi_mul, 30
  %tmp_117_29 = zext i12 %tmp_116_29 to i64
  %C_addr_30 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_29
  %C_load_30 = load float* %C_addr_30, align 4
  %tmp_119_29 = fmul float %C_load_30, %tmp_150
  %tmp_120_29 = fadd float %tmp_120_28, %tmp_119_29
  %Q_addr_30 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_29
  %Q_load_30 = load float* %Q_addr_30, align 4
  %tmp_121_29 = fmul float %Q_load_30, %tmp_150
  %tmp_122_29 = fadd float %tmp_122_28, %tmp_121_29
  %tmp_116_30 = add i12 %phi_mul, 31
  %tmp_117_30 = zext i12 %tmp_116_30 to i64
  %C_addr_31 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_30
  %C_load_31 = load float* %C_addr_31, align 4
  %tmp_119_30 = fmul float %C_load_31, %tmp_151
  %tmp_120_30 = fadd float %tmp_120_29, %tmp_119_30
  %Q_addr_31 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_30
  %Q_load_31 = load float* %Q_addr_31, align 4
  %tmp_121_30 = fmul float %Q_load_31, %tmp_151
  %tmp_122_30 = fadd float %tmp_122_29, %tmp_121_30
  %tmp_116_31 = add i12 %phi_mul, 32
  %tmp_117_31 = zext i12 %tmp_116_31 to i64
  %C_addr_32 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_31
  %C_load_32 = load float* %C_addr_32, align 4
  %tmp_119_31 = fmul float %C_load_32, %tmp_152
  %tmp_120_31 = fadd float %tmp_120_30, %tmp_119_31
  %Q_addr_32 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_31
  %Q_load_32 = load float* %Q_addr_32, align 4
  %tmp_121_31 = fmul float %Q_load_32, %tmp_152
  %tmp_122_31 = fadd float %tmp_122_30, %tmp_121_31
  %tmp_116_32 = add i12 %phi_mul, 33
  %tmp_117_32 = zext i12 %tmp_116_32 to i64
  %C_addr_33 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_32
  %C_load_33 = load float* %C_addr_33, align 4
  %tmp_119_32 = fmul float %C_load_33, %tmp_153
  %tmp_120_32 = fadd float %tmp_120_31, %tmp_119_32
  %Q_addr_33 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_32
  %Q_load_33 = load float* %Q_addr_33, align 4
  %tmp_121_32 = fmul float %Q_load_33, %tmp_153
  %tmp_122_32 = fadd float %tmp_122_31, %tmp_121_32
  %tmp_116_33 = add i12 %phi_mul, 34
  %tmp_117_33 = zext i12 %tmp_116_33 to i64
  %C_addr_34 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_33
  %C_load_34 = load float* %C_addr_34, align 4
  %tmp_119_33 = fmul float %C_load_34, %tmp_154
  %tmp_120_33 = fadd float %tmp_120_32, %tmp_119_33
  %Q_addr_34 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_33
  %Q_load_34 = load float* %Q_addr_34, align 4
  %tmp_121_33 = fmul float %Q_load_34, %tmp_154
  %tmp_122_33 = fadd float %tmp_122_32, %tmp_121_33
  %tmp_116_34 = add i12 %phi_mul, 35
  %tmp_117_34 = zext i12 %tmp_116_34 to i64
  %C_addr_35 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_34
  %C_load_35 = load float* %C_addr_35, align 4
  %tmp_119_34 = fmul float %C_load_35, %tmp_155
  %tmp_120_34 = fadd float %tmp_120_33, %tmp_119_34
  %Q_addr_35 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_34
  %Q_load_35 = load float* %Q_addr_35, align 4
  %tmp_121_34 = fmul float %Q_load_35, %tmp_155
  %tmp_122_34 = fadd float %tmp_122_33, %tmp_121_34
  %tmp_116_35 = add i12 %phi_mul, 36
  %tmp_117_35 = zext i12 %tmp_116_35 to i64
  %C_addr_36 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_35
  %C_load_36 = load float* %C_addr_36, align 4
  %tmp_119_35 = fmul float %C_load_36, %tmp_156
  %tmp_120_35 = fadd float %tmp_120_34, %tmp_119_35
  %Q_addr_36 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_35
  %Q_load_36 = load float* %Q_addr_36, align 4
  %tmp_121_35 = fmul float %Q_load_36, %tmp_156
  %tmp_122_35 = fadd float %tmp_122_34, %tmp_121_35
  %tmp_116_36 = add i12 %phi_mul, 37
  %tmp_117_36 = zext i12 %tmp_116_36 to i64
  %C_addr_37 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_36
  %C_load_37 = load float* %C_addr_37, align 4
  %tmp_119_36 = fmul float %C_load_37, %tmp_157
  %tmp_120_36 = fadd float %tmp_120_35, %tmp_119_36
  %Q_addr_37 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_36
  %Q_load_37 = load float* %Q_addr_37, align 4
  %tmp_121_36 = fmul float %Q_load_37, %tmp_157
  %tmp_122_36 = fadd float %tmp_122_35, %tmp_121_36
  %tmp_116_37 = add i12 %phi_mul, 38
  %tmp_117_37 = zext i12 %tmp_116_37 to i64
  %C_addr_38 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_37
  %C_load_38 = load float* %C_addr_38, align 4
  %tmp_119_37 = fmul float %C_load_38, %tmp_158
  %tmp_120_37 = fadd float %tmp_120_36, %tmp_119_37
  %Q_addr_38 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_37
  %Q_load_38 = load float* %Q_addr_38, align 4
  %tmp_121_37 = fmul float %Q_load_38, %tmp_158
  %tmp_122_37 = fadd float %tmp_122_36, %tmp_121_37
  %tmp_116_38 = add i12 %phi_mul, 39
  %tmp_117_38 = zext i12 %tmp_116_38 to i64
  %C_addr_39 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_38
  %C_load_39 = load float* %C_addr_39, align 4
  %tmp_119_38 = fmul float %C_load_39, %tmp_159
  %tmp_120_38 = fadd float %tmp_120_37, %tmp_119_38
  %Q_addr_39 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_38
  %Q_load_39 = load float* %Q_addr_39, align 4
  %tmp_121_38 = fmul float %Q_load_39, %tmp_159
  %tmp_122_38 = fadd float %tmp_122_37, %tmp_121_38
  %tmp_116_39 = add i12 %phi_mul, 40
  %tmp_117_39 = zext i12 %tmp_116_39 to i64
  %C_addr_40 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_39
  %C_load_40 = load float* %C_addr_40, align 4
  %tmp_119_39 = fmul float %C_load_40, %tmp_160
  %tmp_120_39 = fadd float %tmp_120_38, %tmp_119_39
  %Q_addr_40 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_39
  %Q_load_40 = load float* %Q_addr_40, align 4
  %tmp_121_39 = fmul float %Q_load_40, %tmp_160
  %tmp_122_39 = fadd float %tmp_122_38, %tmp_121_39
  %tmp_116_40 = add i12 %phi_mul, 41
  %tmp_117_40 = zext i12 %tmp_116_40 to i64
  %C_addr_41 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_40
  %C_load_41 = load float* %C_addr_41, align 4
  %tmp_119_40 = fmul float %C_load_41, %tmp_161
  %tmp_120_40 = fadd float %tmp_120_39, %tmp_119_40
  %Q_addr_41 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_40
  %Q_load_41 = load float* %Q_addr_41, align 4
  %tmp_121_40 = fmul float %Q_load_41, %tmp_161
  %tmp_122_40 = fadd float %tmp_122_39, %tmp_121_40
  %tmp_116_41 = add i12 %phi_mul, 42
  %tmp_117_41 = zext i12 %tmp_116_41 to i64
  %C_addr_42 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_41
  %C_load_42 = load float* %C_addr_42, align 4
  %tmp_119_41 = fmul float %C_load_42, %tmp_162
  %tmp_120_41 = fadd float %tmp_120_40, %tmp_119_41
  %Q_addr_42 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_41
  %Q_load_42 = load float* %Q_addr_42, align 4
  %tmp_121_41 = fmul float %Q_load_42, %tmp_162
  %tmp_122_41 = fadd float %tmp_122_40, %tmp_121_41
  %tmp_116_42 = add i12 %phi_mul, 43
  %tmp_117_42 = zext i12 %tmp_116_42 to i64
  %C_addr_43 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_42
  %C_load_43 = load float* %C_addr_43, align 4
  %tmp_119_42 = fmul float %C_load_43, %tmp_163
  %tmp_120_42 = fadd float %tmp_120_41, %tmp_119_42
  %Q_addr_43 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_42
  %Q_load_43 = load float* %Q_addr_43, align 4
  %tmp_121_42 = fmul float %Q_load_43, %tmp_163
  %tmp_122_42 = fadd float %tmp_122_41, %tmp_121_42
  %tmp_116_43 = add i12 %phi_mul, 44
  %tmp_117_43 = zext i12 %tmp_116_43 to i64
  %C_addr_44 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_43
  %C_load_44 = load float* %C_addr_44, align 4
  %tmp_119_43 = fmul float %C_load_44, %tmp_164
  %tmp_120_43 = fadd float %tmp_120_42, %tmp_119_43
  %Q_addr_44 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_43
  %Q_load_44 = load float* %Q_addr_44, align 4
  %tmp_121_43 = fmul float %Q_load_44, %tmp_164
  %tmp_122_43 = fadd float %tmp_122_42, %tmp_121_43
  %tmp_116_44 = add i12 %phi_mul, 45
  %tmp_117_44 = zext i12 %tmp_116_44 to i64
  %C_addr_45 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_44
  %C_load_45 = load float* %C_addr_45, align 4
  %tmp_119_44 = fmul float %C_load_45, %tmp_165
  %tmp_120_44 = fadd float %tmp_120_43, %tmp_119_44
  %Q_addr_45 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_44
  %Q_load_45 = load float* %Q_addr_45, align 4
  %tmp_121_44 = fmul float %Q_load_45, %tmp_165
  %tmp_122_44 = fadd float %tmp_122_43, %tmp_121_44
  %tmp_116_45 = add i12 %phi_mul, 46
  %tmp_117_45 = zext i12 %tmp_116_45 to i64
  %C_addr_46 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_45
  %C_load_46 = load float* %C_addr_46, align 4
  %tmp_119_45 = fmul float %C_load_46, %tmp_166
  %tmp_120_45 = fadd float %tmp_120_44, %tmp_119_45
  %Q_addr_46 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_45
  %Q_load_46 = load float* %Q_addr_46, align 4
  %tmp_121_45 = fmul float %Q_load_46, %tmp_166
  %tmp_122_45 = fadd float %tmp_122_44, %tmp_121_45
  %tmp_116_46 = add i12 %phi_mul, 47
  %tmp_117_46 = zext i12 %tmp_116_46 to i64
  %C_addr_47 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_46
  %C_load_47 = load float* %C_addr_47, align 4
  %tmp_119_46 = fmul float %C_load_47, %tmp_167
  %tmp_120_46 = fadd float %tmp_120_45, %tmp_119_46
  %Q_addr_47 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_46
  %Q_load_47 = load float* %Q_addr_47, align 4
  %tmp_121_46 = fmul float %Q_load_47, %tmp_167
  %tmp_122_46 = fadd float %tmp_122_45, %tmp_121_46
  %tmp_116_47 = add i12 %phi_mul, 48
  %tmp_117_47 = zext i12 %tmp_116_47 to i64
  %C_addr_48 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_47
  %C_load_48 = load float* %C_addr_48, align 4
  %tmp_119_47 = fmul float %C_load_48, %tmp_168
  %tmp_120_47 = fadd float %tmp_120_46, %tmp_119_47
  %Q_addr_48 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_47
  %Q_load_48 = load float* %Q_addr_48, align 4
  %tmp_121_47 = fmul float %Q_load_48, %tmp_168
  %tmp_122_47 = fadd float %tmp_122_46, %tmp_121_47
  %tmp_116_48 = add i12 %phi_mul, 49
  %tmp_117_48 = zext i12 %tmp_116_48 to i64
  %C_addr_49 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_117_48
  %C_load_49 = load float* %C_addr_49, align 4
  %tmp_119_48 = fmul float %C_load_49, %tmp_169
  %tmp_120_48 = fadd float %tmp_120_47, %tmp_119_48
  store float %tmp_120_48, float* %s_addr, align 4
  %Q_addr_49 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_117_48
  %Q_load_49 = load float* %Q_addr_49, align 4
  %tmp_121_48 = fmul float %Q_load_49, %tmp_169
  %tmp_122_48 = fadd float %tmp_122_47, %tmp_121_48
  store float %tmp_122_48, float* %e_addr, align 4
  %empty_14 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str8, i32 %tmp_170)
  br label %.preheader10

.preheader9.0:                                    ; preds = %.preheader10
  %s_load = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 0), align 16
  %tmp_115 = fmul float %s_load, %tmp_93
  %sigma2_2 = fadd float %tmp_115, 1.000000e+00
  %s_load_1 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 1), align 4
  %tmp_115_1 = fmul float %s_load_1, %tmp_108
  %sigma2_2_1 = fadd float %sigma2_2, %tmp_115_1
  %s_load_2 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 2), align 8
  %tmp_115_2 = fmul float %s_load_2, %tmp_109
  %sigma2_2_2 = fadd float %sigma2_2_1, %tmp_115_2
  %s_load_3 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 3), align 4
  %tmp_115_3 = fmul float %s_load_3, %tmp_110
  %sigma2_2_3 = fadd float %sigma2_2_2, %tmp_115_3
  %s_load_4 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 4), align 16
  %tmp_115_4 = fmul float %s_load_4, %tmp_111
  %sigma2_2_4 = fadd float %sigma2_2_3, %tmp_115_4
  %s_load_5 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 5), align 4
  %tmp_115_5 = fmul float %s_load_5, %tmp_112
  %sigma2_2_5 = fadd float %sigma2_2_4, %tmp_115_5
  %s_load_6 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 6), align 8
  %tmp_115_6 = fmul float %s_load_6, %tmp_113
  %sigma2_2_6 = fadd float %sigma2_2_5, %tmp_115_6
  %s_load_7 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 7), align 4
  %tmp_115_7 = fmul float %s_load_7, %tmp_114
  %sigma2_2_7 = fadd float %sigma2_2_6, %tmp_115_7
  %s_load_8 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 8), align 16
  %tmp_115_8 = fmul float %s_load_8, %tmp_116
  %sigma2_2_8 = fadd float %sigma2_2_7, %tmp_115_8
  %s_load_9 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 9), align 4
  %tmp_115_9 = fmul float %s_load_9, %tmp_118
  %sigma2_2_9 = fadd float %sigma2_2_8, %tmp_115_9
  %s_load_10 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 10), align 8
  %tmp_115_s = fmul float %s_load_10, %tmp_123
  %sigma2_2_s = fadd float %sigma2_2_9, %tmp_115_s
  %s_load_11 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 11), align 4
  %tmp_115_10 = fmul float %s_load_11, %tmp_124
  %sigma2_2_10 = fadd float %sigma2_2_s, %tmp_115_10
  %s_load_12 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 12), align 16
  %tmp_115_11 = fmul float %s_load_12, %tmp_128
  %sigma2_2_11 = fadd float %sigma2_2_10, %tmp_115_11
  %s_load_13 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 13), align 4
  %tmp_115_12 = fmul float %s_load_13, %tmp_129
  %sigma2_2_12 = fadd float %sigma2_2_11, %tmp_115_12
  %s_load_14 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 14), align 8
  %tmp_115_13 = fmul float %s_load_14, %tmp_130
  %sigma2_2_13 = fadd float %sigma2_2_12, %tmp_115_13
  %s_load_15 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 15), align 4
  %tmp_115_14 = fmul float %s_load_15, %tmp_131
  %sigma2_2_14 = fadd float %sigma2_2_13, %tmp_115_14
  %s_load_16 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 16), align 16
  %tmp_115_15 = fmul float %s_load_16, %tmp_132
  %sigma2_2_15 = fadd float %sigma2_2_14, %tmp_115_15
  %s_load_17 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 17), align 4
  %tmp_115_16 = fmul float %s_load_17, %tmp_133
  %sigma2_2_16 = fadd float %sigma2_2_15, %tmp_115_16
  %s_load_18 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 18), align 8
  %tmp_115_17 = fmul float %s_load_18, %tmp_134
  %sigma2_2_17 = fadd float %sigma2_2_16, %tmp_115_17
  %s_load_19 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 19), align 4
  %tmp_115_18 = fmul float %s_load_19, %tmp_137
  %sigma2_2_18 = fadd float %sigma2_2_17, %tmp_115_18
  %s_load_20 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 20), align 16
  %tmp_115_19 = fmul float %s_load_20, %tmp_140
  %sigma2_2_19 = fadd float %sigma2_2_18, %tmp_115_19
  %s_load_21 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 21), align 4
  %tmp_115_20 = fmul float %s_load_21, %tmp_141
  %sigma2_2_20 = fadd float %sigma2_2_19, %tmp_115_20
  %s_load_22 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 22), align 8
  %tmp_115_21 = fmul float %s_load_22, %tmp_142
  %sigma2_2_21 = fadd float %sigma2_2_20, %tmp_115_21
  %s_load_23 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 23), align 4
  %tmp_115_22 = fmul float %s_load_23, %tmp_143
  %sigma2_2_22 = fadd float %sigma2_2_21, %tmp_115_22
  %s_load_24 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 24), align 16
  %tmp_115_23 = fmul float %s_load_24, %tmp_144
  %sigma2_2_23 = fadd float %sigma2_2_22, %tmp_115_23
  %s_load_25 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 25), align 4
  %tmp_115_24 = fmul float %s_load_25, %tmp_145
  %sigma2_2_24 = fadd float %sigma2_2_23, %tmp_115_24
  %s_load_26 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 26), align 8
  %tmp_115_25 = fmul float %s_load_26, %tmp_146
  %sigma2_2_25 = fadd float %sigma2_2_24, %tmp_115_25
  %s_load_27 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 27), align 4
  %tmp_115_26 = fmul float %s_load_27, %tmp_147
  %sigma2_2_26 = fadd float %sigma2_2_25, %tmp_115_26
  %s_load_28 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 28), align 16
  %tmp_115_27 = fmul float %s_load_28, %tmp_148
  %sigma2_2_27 = fadd float %sigma2_2_26, %tmp_115_27
  %s_load_29 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 29), align 4
  %tmp_115_28 = fmul float %s_load_29, %tmp_149
  %sigma2_2_28 = fadd float %sigma2_2_27, %tmp_115_28
  %s_load_30 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 30), align 8
  %tmp_115_29 = fmul float %s_load_30, %tmp_150
  %sigma2_2_29 = fadd float %sigma2_2_28, %tmp_115_29
  %s_load_31 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 31), align 4
  %tmp_115_30 = fmul float %s_load_31, %tmp_151
  %sigma2_2_30 = fadd float %sigma2_2_29, %tmp_115_30
  %s_load_32 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 32), align 16
  %tmp_115_31 = fmul float %s_load_32, %tmp_152
  %sigma2_2_31 = fadd float %sigma2_2_30, %tmp_115_31
  %s_load_33 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 33), align 4
  %tmp_115_32 = fmul float %s_load_33, %tmp_153
  %sigma2_2_32 = fadd float %sigma2_2_31, %tmp_115_32
  %s_load_34 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 34), align 8
  %tmp_115_33 = fmul float %s_load_34, %tmp_154
  %sigma2_2_33 = fadd float %sigma2_2_32, %tmp_115_33
  %s_load_35 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 35), align 4
  %tmp_115_34 = fmul float %s_load_35, %tmp_155
  %sigma2_2_34 = fadd float %sigma2_2_33, %tmp_115_34
  %s_load_36 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 36), align 16
  %tmp_115_35 = fmul float %s_load_36, %tmp_156
  %sigma2_2_35 = fadd float %sigma2_2_34, %tmp_115_35
  %s_load_37 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 37), align 4
  %tmp_115_36 = fmul float %s_load_37, %tmp_157
  %sigma2_2_36 = fadd float %sigma2_2_35, %tmp_115_36
  %s_load_38 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 38), align 8
  %tmp_115_37 = fmul float %s_load_38, %tmp_158
  %sigma2_2_37 = fadd float %sigma2_2_36, %tmp_115_37
  %s_load_39 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 39), align 4
  %tmp_115_38 = fmul float %s_load_39, %tmp_159
  %sigma2_2_38 = fadd float %sigma2_2_37, %tmp_115_38
  %s_load_40 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 40), align 16
  %tmp_115_39 = fmul float %s_load_40, %tmp_160
  %sigma2_2_39 = fadd float %sigma2_2_38, %tmp_115_39
  %s_load_41 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 41), align 4
  %tmp_115_40 = fmul float %s_load_41, %tmp_161
  %sigma2_2_40 = fadd float %sigma2_2_39, %tmp_115_40
  %s_load_42 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 42), align 8
  %tmp_115_41 = fmul float %s_load_42, %tmp_162
  %sigma2_2_41 = fadd float %sigma2_2_40, %tmp_115_41
  %s_load_43 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 43), align 4
  %tmp_115_42 = fmul float %s_load_43, %tmp_163
  %sigma2_2_42 = fadd float %sigma2_2_41, %tmp_115_42
  %s_load_44 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 44), align 16
  %tmp_115_43 = fmul float %s_load_44, %tmp_164
  %sigma2_2_43 = fadd float %sigma2_2_42, %tmp_115_43
  %s_load_45 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 45), align 4
  %tmp_115_44 = fmul float %s_load_45, %tmp_165
  %sigma2_2_44 = fadd float %sigma2_2_43, %tmp_115_44
  %s_load_46 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 46), align 8
  %tmp_115_45 = fmul float %s_load_46, %tmp_166
  %sigma2_2_45 = fadd float %sigma2_2_44, %tmp_115_45
  %s_load_47 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 47), align 4
  %tmp_115_46 = fmul float %s_load_47, %tmp_167
  %sigma2_2_46 = fadd float %sigma2_2_45, %tmp_115_46
  %s_load_48 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 48), align 16
  %tmp_115_47 = fmul float %s_load_48, %tmp_168
  %sigma2_2_47 = fadd float %sigma2_2_46, %tmp_115_47
  %s_load_49 = load float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 49), align 4
  %tmp_115_48 = fmul float %s_load_49, %tmp_169
  %sigma2_2_48 = fadd float %sigma2_2_47, %tmp_115_48
  store float 1.000000e+00, float* getelementptr inbounds ([51 x float]* @s, i64 0, i64 50), align 8
  %tmp_s = fsub float %pY_read, %m_2_48
  %tmp_83 = fpext float %tmp_s to double
  %tmp_84 = fpext float %sigma2_2_48 to double
  %tmp_85 = fadd double %tmp_84, 9.000000e-02
  %tmp_86 = fdiv double %tmp_83, %tmp_85
  %q = fptrunc double %tmp_86 to float
  %tmp_87 = fdiv double -1.000000e+00, %tmp_85
  %r = fptrunc double %tmp_87 to float
  %e_load = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 0), align 16
  %tmp_125 = fmul float %e_load, %tmp_93
  %gamma_2 = fsub float 1.000000e+00, %tmp_125
  %tmp_126 = fmul float %s_load, %q
  %tmp_127 = fadd float %alpha_load, %tmp_126
  store float %tmp_127, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 0), align 16
  %e_load_1 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 1), align 4
  %tmp_125_1 = fmul float %e_load_1, %tmp_108
  %gamma_2_1 = fsub float %gamma_2, %tmp_125_1
  %tmp_126_1 = fmul float %s_load_1, %q
  %tmp_127_1 = fadd float %alpha_load_1, %tmp_126_1
  store float %tmp_127_1, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 1), align 4
  %e_load_2 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 2), align 8
  %tmp_125_2 = fmul float %e_load_2, %tmp_109
  %gamma_2_2 = fsub float %gamma_2_1, %tmp_125_2
  %tmp_126_2 = fmul float %s_load_2, %q
  %tmp_127_2 = fadd float %alpha_load_2, %tmp_126_2
  store float %tmp_127_2, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 2), align 8
  %e_load_50 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 3), align 4
  %tmp_125_3 = fmul float %e_load_50, %tmp_110
  %gamma_2_3 = fsub float %gamma_2_2, %tmp_125_3
  %tmp_126_3 = fmul float %s_load_3, %q
  %tmp_127_3 = fadd float %alpha_load_3, %tmp_126_3
  store float %tmp_127_3, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 3), align 4
  %e_load_51 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 4), align 16
  %tmp_125_4 = fmul float %e_load_51, %tmp_111
  %gamma_2_4 = fsub float %gamma_2_3, %tmp_125_4
  %tmp_126_4 = fmul float %s_load_4, %q
  %tmp_127_4 = fadd float %alpha_load_4, %tmp_126_4
  store float %tmp_127_4, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 4), align 16
  %e_load_5 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 5), align 4
  %tmp_125_5 = fmul float %e_load_5, %tmp_112
  %gamma_2_5 = fsub float %gamma_2_4, %tmp_125_5
  %tmp_126_5 = fmul float %s_load_5, %q
  %tmp_127_5 = fadd float %alpha_load_5, %tmp_126_5
  store float %tmp_127_5, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 5), align 4
  %e_load_6 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 6), align 8
  %tmp_125_6 = fmul float %e_load_6, %tmp_113
  %gamma_2_6 = fsub float %gamma_2_5, %tmp_125_6
  %tmp_126_6 = fmul float %s_load_6, %q
  %tmp_127_6 = fadd float %alpha_load_53, %tmp_126_6
  store float %tmp_127_6, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 6), align 8
  %e_load_7 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 7), align 4
  %tmp_125_7 = fmul float %e_load_7, %tmp_114
  %gamma_2_7 = fsub float %gamma_2_6, %tmp_125_7
  %tmp_126_7 = fmul float %s_load_7, %q
  %tmp_127_7 = fadd float %alpha_load_54, %tmp_126_7
  store float %tmp_127_7, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 7), align 4
  %e_load_8 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 8), align 16
  %tmp_125_8 = fmul float %e_load_8, %tmp_116
  %gamma_2_8 = fsub float %gamma_2_7, %tmp_125_8
  %tmp_126_8 = fmul float %s_load_8, %q
  %tmp_127_8 = fadd float %alpha_load_8, %tmp_126_8
  store float %tmp_127_8, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 8), align 16
  %e_load_9 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 9), align 4
  %tmp_125_9 = fmul float %e_load_9, %tmp_118
  %gamma_2_9 = fsub float %gamma_2_8, %tmp_125_9
  %tmp_126_9 = fmul float %s_load_9, %q
  %tmp_127_9 = fadd float %alpha_load_9, %tmp_126_9
  store float %tmp_127_9, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 9), align 4
  %e_load_10 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 10), align 8
  %tmp_125_s = fmul float %e_load_10, %tmp_123
  %gamma_2_s = fsub float %gamma_2_9, %tmp_125_s
  %tmp_126_s = fmul float %s_load_10, %q
  %tmp_127_s = fadd float %alpha_load_10, %tmp_126_s
  store float %tmp_127_s, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 10), align 8
  %e_load_11 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 11), align 4
  %tmp_125_10 = fmul float %e_load_11, %tmp_124
  %gamma_2_10 = fsub float %gamma_2_s, %tmp_125_10
  %tmp_126_10 = fmul float %s_load_11, %q
  %tmp_127_10 = fadd float %alpha_load_11, %tmp_126_10
  store float %tmp_127_10, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 11), align 4
  %e_load_12 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 12), align 16
  %tmp_125_11 = fmul float %e_load_12, %tmp_128
  %gamma_2_11 = fsub float %gamma_2_10, %tmp_125_11
  %tmp_126_11 = fmul float %s_load_12, %q
  %tmp_127_11 = fadd float %alpha_load_12, %tmp_126_11
  store float %tmp_127_11, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 12), align 16
  %e_load_13 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 13), align 4
  %tmp_125_12 = fmul float %e_load_13, %tmp_129
  %gamma_2_12 = fsub float %gamma_2_11, %tmp_125_12
  %tmp_126_12 = fmul float %s_load_13, %q
  %tmp_127_12 = fadd float %alpha_load_13, %tmp_126_12
  store float %tmp_127_12, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 13), align 4
  %e_load_14 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 14), align 8
  %tmp_125_13 = fmul float %e_load_14, %tmp_130
  %gamma_2_13 = fsub float %gamma_2_12, %tmp_125_13
  %tmp_126_13 = fmul float %s_load_14, %q
  %tmp_127_13 = fadd float %alpha_load_14, %tmp_126_13
  store float %tmp_127_13, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 14), align 8
  %e_load_15 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 15), align 4
  %tmp_125_14 = fmul float %e_load_15, %tmp_131
  %gamma_2_14 = fsub float %gamma_2_13, %tmp_125_14
  %tmp_126_14 = fmul float %s_load_15, %q
  %tmp_127_14 = fadd float %alpha_load_15, %tmp_126_14
  store float %tmp_127_14, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 15), align 4
  %e_load_16 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 16), align 16
  %tmp_125_15 = fmul float %e_load_16, %tmp_132
  %gamma_2_15 = fsub float %gamma_2_14, %tmp_125_15
  %tmp_126_15 = fmul float %s_load_16, %q
  %tmp_127_15 = fadd float %alpha_load_16, %tmp_126_15
  store float %tmp_127_15, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 16), align 16
  %e_load_17 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 17), align 4
  %tmp_125_16 = fmul float %e_load_17, %tmp_133
  %gamma_2_16 = fsub float %gamma_2_15, %tmp_125_16
  %tmp_126_16 = fmul float %s_load_17, %q
  %tmp_127_16 = fadd float %alpha_load_17, %tmp_126_16
  store float %tmp_127_16, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 17), align 4
  %e_load_18 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 18), align 8
  %tmp_125_17 = fmul float %e_load_18, %tmp_134
  %gamma_2_17 = fsub float %gamma_2_16, %tmp_125_17
  %tmp_126_17 = fmul float %s_load_18, %q
  %tmp_127_17 = fadd float %alpha_load_18, %tmp_126_17
  store float %tmp_127_17, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 18), align 8
  %e_load_19 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 19), align 4
  %tmp_125_18 = fmul float %e_load_19, %tmp_137
  %gamma_2_18 = fsub float %gamma_2_17, %tmp_125_18
  %tmp_126_18 = fmul float %s_load_19, %q
  %tmp_127_18 = fadd float %alpha_load_19, %tmp_126_18
  store float %tmp_127_18, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 19), align 4
  %e_load_20 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 20), align 16
  %tmp_125_19 = fmul float %e_load_20, %tmp_140
  %gamma_2_19 = fsub float %gamma_2_18, %tmp_125_19
  %tmp_126_19 = fmul float %s_load_20, %q
  %tmp_127_19 = fadd float %alpha_load_20, %tmp_126_19
  store float %tmp_127_19, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 20), align 16
  %e_load_21 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 21), align 4
  %tmp_125_20 = fmul float %e_load_21, %tmp_141
  %gamma_2_20 = fsub float %gamma_2_19, %tmp_125_20
  %tmp_126_20 = fmul float %s_load_21, %q
  %tmp_127_20 = fadd float %alpha_load_21, %tmp_126_20
  store float %tmp_127_20, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 21), align 4
  %e_load_22 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 22), align 8
  %tmp_125_21 = fmul float %e_load_22, %tmp_142
  %gamma_2_21 = fsub float %gamma_2_20, %tmp_125_21
  %tmp_126_21 = fmul float %s_load_22, %q
  %tmp_127_21 = fadd float %alpha_load_22, %tmp_126_21
  store float %tmp_127_21, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 22), align 8
  %e_load_23 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 23), align 4
  %tmp_125_22 = fmul float %e_load_23, %tmp_143
  %gamma_2_22 = fsub float %gamma_2_21, %tmp_125_22
  %tmp_126_22 = fmul float %s_load_23, %q
  %tmp_127_22 = fadd float %alpha_load_23, %tmp_126_22
  store float %tmp_127_22, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 23), align 4
  %e_load_24 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 24), align 16
  %tmp_125_23 = fmul float %e_load_24, %tmp_144
  %gamma_2_23 = fsub float %gamma_2_22, %tmp_125_23
  %tmp_126_23 = fmul float %s_load_24, %q
  %tmp_127_23 = fadd float %alpha_load_24, %tmp_126_23
  store float %tmp_127_23, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 24), align 16
  %e_load_25 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 25), align 4
  %tmp_125_24 = fmul float %e_load_25, %tmp_145
  %gamma_2_24 = fsub float %gamma_2_23, %tmp_125_24
  %tmp_126_24 = fmul float %s_load_25, %q
  %tmp_127_24 = fadd float %alpha_load_25, %tmp_126_24
  store float %tmp_127_24, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 25), align 4
  %e_load_26 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 26), align 8
  %tmp_125_25 = fmul float %e_load_26, %tmp_146
  %gamma_2_25 = fsub float %gamma_2_24, %tmp_125_25
  %tmp_126_25 = fmul float %s_load_26, %q
  %tmp_127_25 = fadd float %alpha_load_26, %tmp_126_25
  store float %tmp_127_25, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 26), align 8
  %e_load_27 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 27), align 4
  %tmp_125_26 = fmul float %e_load_27, %tmp_147
  %gamma_2_26 = fsub float %gamma_2_25, %tmp_125_26
  %tmp_126_26 = fmul float %s_load_27, %q
  %tmp_127_26 = fadd float %alpha_load_27, %tmp_126_26
  store float %tmp_127_26, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 27), align 4
  %e_load_28 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 28), align 16
  %tmp_125_27 = fmul float %e_load_28, %tmp_148
  %gamma_2_27 = fsub float %gamma_2_26, %tmp_125_27
  %tmp_126_27 = fmul float %s_load_28, %q
  %tmp_127_27 = fadd float %alpha_load_28, %tmp_126_27
  store float %tmp_127_27, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 28), align 16
  %e_load_29 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 29), align 4
  %tmp_125_28 = fmul float %e_load_29, %tmp_149
  %gamma_2_28 = fsub float %gamma_2_27, %tmp_125_28
  %tmp_126_28 = fmul float %s_load_29, %q
  %tmp_127_28 = fadd float %alpha_load_29, %tmp_126_28
  store float %tmp_127_28, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 29), align 4
  %e_load_30 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 30), align 8
  %tmp_125_29 = fmul float %e_load_30, %tmp_150
  %gamma_2_29 = fsub float %gamma_2_28, %tmp_125_29
  %tmp_126_29 = fmul float %s_load_30, %q
  %tmp_127_29 = fadd float %alpha_load_30, %tmp_126_29
  store float %tmp_127_29, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 30), align 8
  %e_load_31 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 31), align 4
  %tmp_125_30 = fmul float %e_load_31, %tmp_151
  %gamma_2_30 = fsub float %gamma_2_29, %tmp_125_30
  %tmp_126_30 = fmul float %s_load_31, %q
  %tmp_127_30 = fadd float %alpha_load_31, %tmp_126_30
  store float %tmp_127_30, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 31), align 4
  %e_load_32 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 32), align 16
  %tmp_125_31 = fmul float %e_load_32, %tmp_152
  %gamma_2_31 = fsub float %gamma_2_30, %tmp_125_31
  %tmp_126_31 = fmul float %s_load_32, %q
  %tmp_127_31 = fadd float %alpha_load_32, %tmp_126_31
  store float %tmp_127_31, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 32), align 16
  %e_load_33 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 33), align 4
  %tmp_125_32 = fmul float %e_load_33, %tmp_153
  %gamma_2_32 = fsub float %gamma_2_31, %tmp_125_32
  %tmp_126_32 = fmul float %s_load_33, %q
  %tmp_127_32 = fadd float %alpha_load_33, %tmp_126_32
  store float %tmp_127_32, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 33), align 4
  %e_load_34 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 34), align 8
  %tmp_125_33 = fmul float %e_load_34, %tmp_154
  %gamma_2_33 = fsub float %gamma_2_32, %tmp_125_33
  %tmp_126_33 = fmul float %s_load_34, %q
  %tmp_127_33 = fadd float %alpha_load_34, %tmp_126_33
  store float %tmp_127_33, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 34), align 8
  %e_load_35 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 35), align 4
  %tmp_125_34 = fmul float %e_load_35, %tmp_155
  %gamma_2_34 = fsub float %gamma_2_33, %tmp_125_34
  %tmp_126_34 = fmul float %s_load_35, %q
  %tmp_127_34 = fadd float %alpha_load_35, %tmp_126_34
  store float %tmp_127_34, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 35), align 4
  %e_load_36 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 36), align 16
  %tmp_125_35 = fmul float %e_load_36, %tmp_156
  %gamma_2_35 = fsub float %gamma_2_34, %tmp_125_35
  %tmp_126_35 = fmul float %s_load_36, %q
  %tmp_127_35 = fadd float %alpha_load_36, %tmp_126_35
  store float %tmp_127_35, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 36), align 16
  %e_load_37 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 37), align 4
  %tmp_125_36 = fmul float %e_load_37, %tmp_157
  %gamma_2_36 = fsub float %gamma_2_35, %tmp_125_36
  %tmp_126_36 = fmul float %s_load_37, %q
  %tmp_127_36 = fadd float %alpha_load_37, %tmp_126_36
  store float %tmp_127_36, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 37), align 4
  %e_load_38 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 38), align 8
  %tmp_125_37 = fmul float %e_load_38, %tmp_158
  %gamma_2_37 = fsub float %gamma_2_36, %tmp_125_37
  %tmp_126_37 = fmul float %s_load_38, %q
  %tmp_127_37 = fadd float %alpha_load_38, %tmp_126_37
  store float %tmp_127_37, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 38), align 8
  %e_load_39 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 39), align 4
  %tmp_125_38 = fmul float %e_load_39, %tmp_159
  %gamma_2_38 = fsub float %gamma_2_37, %tmp_125_38
  %tmp_126_38 = fmul float %s_load_39, %q
  %tmp_127_38 = fadd float %alpha_load_39, %tmp_126_38
  store float %tmp_127_38, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 39), align 4
  %e_load_40 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 40), align 16
  %tmp_125_39 = fmul float %e_load_40, %tmp_160
  %gamma_2_39 = fsub float %gamma_2_38, %tmp_125_39
  %tmp_126_39 = fmul float %s_load_40, %q
  %tmp_127_39 = fadd float %alpha_load_40, %tmp_126_39
  store float %tmp_127_39, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 40), align 16
  %e_load_41 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 41), align 4
  %tmp_125_40 = fmul float %e_load_41, %tmp_161
  %gamma_2_40 = fsub float %gamma_2_39, %tmp_125_40
  %tmp_126_40 = fmul float %s_load_41, %q
  %tmp_127_40 = fadd float %alpha_load_41, %tmp_126_40
  store float %tmp_127_40, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 41), align 4
  %e_load_42 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 42), align 8
  %tmp_125_41 = fmul float %e_load_42, %tmp_162
  %gamma_2_41 = fsub float %gamma_2_40, %tmp_125_41
  %tmp_126_41 = fmul float %s_load_42, %q
  %tmp_127_41 = fadd float %alpha_load_42, %tmp_126_41
  store float %tmp_127_41, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 42), align 8
  %e_load_43 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 43), align 4
  %tmp_125_42 = fmul float %e_load_43, %tmp_163
  %gamma_2_42 = fsub float %gamma_2_41, %tmp_125_42
  %tmp_126_42 = fmul float %s_load_43, %q
  %tmp_127_42 = fadd float %alpha_load_43, %tmp_126_42
  store float %tmp_127_42, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 43), align 4
  %e_load_44 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 44), align 16
  %tmp_125_43 = fmul float %e_load_44, %tmp_164
  %gamma_2_43 = fsub float %gamma_2_42, %tmp_125_43
  %tmp_126_43 = fmul float %s_load_44, %q
  %tmp_127_43 = fadd float %alpha_load_44, %tmp_126_43
  store float %tmp_127_43, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 44), align 16
  %e_load_45 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 45), align 4
  %tmp_125_44 = fmul float %e_load_45, %tmp_165
  %gamma_2_44 = fsub float %gamma_2_43, %tmp_125_44
  %tmp_126_44 = fmul float %s_load_45, %q
  %tmp_127_44 = fadd float %alpha_load_45, %tmp_126_44
  store float %tmp_127_44, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 45), align 4
  %e_load_46 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 46), align 8
  %tmp_125_45 = fmul float %e_load_46, %tmp_166
  %gamma_2_45 = fsub float %gamma_2_44, %tmp_125_45
  %tmp_126_45 = fmul float %s_load_46, %q
  %tmp_127_45 = fadd float %alpha_load_46, %tmp_126_45
  store float %tmp_127_45, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 46), align 8
  %e_load_47 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 47), align 4
  %tmp_125_46 = fmul float %e_load_47, %tmp_167
  %gamma_2_46 = fsub float %gamma_2_45, %tmp_125_46
  %tmp_126_46 = fmul float %s_load_47, %q
  %tmp_127_46 = fadd float %alpha_load_47, %tmp_126_46
  store float %tmp_127_46, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 47), align 4
  %e_load_48 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 48), align 16
  %tmp_125_47 = fmul float %e_load_48, %tmp_168
  %gamma_2_47 = fsub float %gamma_2_46, %tmp_125_47
  %tmp_126_47 = fmul float %s_load_48, %q
  %tmp_127_47 = fadd float %alpha_load_48, %tmp_126_47
  store float %tmp_127_47, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 48), align 16
  %e_load_49 = load float* getelementptr inbounds ([51 x float]* @e, i64 0, i64 49), align 4
  %tmp_125_48 = fmul float %e_load_49, %tmp_169
  %gamma_2_48 = fsub float %gamma_2_47, %tmp_125_48
  %tmp_126_48 = fmul float %s_load_49, %q
  %tmp_127_48 = fadd float %alpha_load_49, %tmp_126_48
  store float %tmp_127_48, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 49), align 4
  %alpha_load_6 = load float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 50), align 8
  %tmp_88 = fadd float %alpha_load_6, %q
  store float %tmp_88, float* getelementptr inbounds ([51 x float]* @alpha, i64 0, i64 50), align 8
  br label %1

; <label>:1                                       ; preds = %2, %.preheader9.0
  %i4 = phi i6 [ 0, %.preheader9.0 ], [ %i_9, %2 ]
  %phi_mul2 = phi i12 [ 0, %.preheader9.0 ], [ %next_mul2, %2 ]
  %exitcond3 = icmp eq i6 %i4, -13
  %i_9 = add i6 %i4, 1
  br i1 %exitcond3, label %.preheader.preheader, label %2

.preheader.preheader:                             ; preds = %1
  %tmp_91 = fpext float %gamma_2_48 to double
  br label %3

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 51, i64 51, i64 51)
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str12) nounwind
  %tmp_171 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str12)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_92 = zext i6 %i4 to i64
  %s_addr_5 = getelementptr inbounds [51 x float]* @s, i64 0, i64 %tmp_92
  %next_mul2 = add i12 %phi_mul2, 51
  %s_load_50 = load float* %s_addr_5, align 4
  %tmp_135 = fmul float %s_load_50, %s_load
  %tmp_136 = fmul float %tmp_135, %r
  %tmp_138 = zext i12 %phi_mul2 to i64
  %C_addr_52 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138
  %C_load_177 = load float* %C_addr_52, align 4
  %tmp_139 = fadd float %C_load_177, %tmp_136
  store float %tmp_139, float* %C_addr_52, align 4
  %tmp_135_1 = fmul float %s_load_50, %s_load_1
  %tmp_136_1 = fmul float %tmp_135_1, %r
  %tmp_137_1 = add i12 %phi_mul2, 1
  %tmp_138_1 = zext i12 %tmp_137_1 to i64
  %C_addr_53 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_1
  %C_load_178 = load float* %C_addr_53, align 4
  %tmp_139_1 = fadd float %C_load_178, %tmp_136_1
  store float %tmp_139_1, float* %C_addr_53, align 4
  %tmp_135_2 = fmul float %s_load_50, %s_load_2
  %tmp_136_2 = fmul float %tmp_135_2, %r
  %tmp_137_2 = add i12 %phi_mul2, 2
  %tmp_138_2 = zext i12 %tmp_137_2 to i64
  %C_addr_54 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_2
  %C_load_179 = load float* %C_addr_54, align 4
  %tmp_139_2 = fadd float %C_load_179, %tmp_136_2
  store float %tmp_139_2, float* %C_addr_54, align 4
  %tmp_135_3 = fmul float %s_load_50, %s_load_3
  %tmp_136_3 = fmul float %tmp_135_3, %r
  %tmp_137_3 = add i12 %phi_mul2, 3
  %tmp_138_3 = zext i12 %tmp_137_3 to i64
  %C_addr_55 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_3
  %C_load_180 = load float* %C_addr_55, align 4
  %tmp_139_3 = fadd float %C_load_180, %tmp_136_3
  store float %tmp_139_3, float* %C_addr_55, align 4
  %tmp_135_4 = fmul float %s_load_50, %s_load_4
  %tmp_136_4 = fmul float %tmp_135_4, %r
  %tmp_137_4 = add i12 %phi_mul2, 4
  %tmp_138_4 = zext i12 %tmp_137_4 to i64
  %C_addr_56 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_4
  %C_load_181 = load float* %C_addr_56, align 4
  %tmp_139_4 = fadd float %C_load_181, %tmp_136_4
  store float %tmp_139_4, float* %C_addr_56, align 4
  %tmp_135_5 = fmul float %s_load_50, %s_load_5
  %tmp_136_5 = fmul float %tmp_135_5, %r
  %tmp_137_5 = add i12 %phi_mul2, 5
  %tmp_138_5 = zext i12 %tmp_137_5 to i64
  %C_addr_57 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_5
  %C_load_182 = load float* %C_addr_57, align 4
  %tmp_139_5 = fadd float %C_load_182, %tmp_136_5
  store float %tmp_139_5, float* %C_addr_57, align 4
  %tmp_135_6 = fmul float %s_load_50, %s_load_6
  %tmp_136_6 = fmul float %tmp_135_6, %r
  %tmp_137_6 = add i12 %phi_mul2, 6
  %tmp_138_6 = zext i12 %tmp_137_6 to i64
  %C_addr_58 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_6
  %C_load_183 = load float* %C_addr_58, align 4
  %tmp_139_6 = fadd float %C_load_183, %tmp_136_6
  store float %tmp_139_6, float* %C_addr_58, align 4
  %tmp_135_7 = fmul float %s_load_50, %s_load_7
  %tmp_136_7 = fmul float %tmp_135_7, %r
  %tmp_137_7 = add i12 %phi_mul2, 7
  %tmp_138_7 = zext i12 %tmp_137_7 to i64
  %C_addr_59 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_7
  %C_load_184 = load float* %C_addr_59, align 4
  %tmp_139_7 = fadd float %C_load_184, %tmp_136_7
  store float %tmp_139_7, float* %C_addr_59, align 4
  %tmp_135_8 = fmul float %s_load_50, %s_load_8
  %tmp_136_8 = fmul float %tmp_135_8, %r
  %tmp_137_8 = add i12 %phi_mul2, 8
  %tmp_138_8 = zext i12 %tmp_137_8 to i64
  %C_addr_60 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_8
  %C_load_185 = load float* %C_addr_60, align 4
  %tmp_139_8 = fadd float %C_load_185, %tmp_136_8
  store float %tmp_139_8, float* %C_addr_60, align 4
  %tmp_135_9 = fmul float %s_load_50, %s_load_9
  %tmp_136_9 = fmul float %tmp_135_9, %r
  %tmp_137_9 = add i12 %phi_mul2, 9
  %tmp_138_9 = zext i12 %tmp_137_9 to i64
  %C_addr_61 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_9
  %C_load_186 = load float* %C_addr_61, align 4
  %tmp_139_9 = fadd float %C_load_186, %tmp_136_9
  store float %tmp_139_9, float* %C_addr_61, align 4
  %tmp_135_s = fmul float %s_load_50, %s_load_10
  %tmp_136_s = fmul float %tmp_135_s, %r
  %tmp_137_s = add i12 %phi_mul2, 10
  %tmp_138_s = zext i12 %tmp_137_s to i64
  %C_addr_62 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_s
  %C_load_187 = load float* %C_addr_62, align 4
  %tmp_139_s = fadd float %C_load_187, %tmp_136_s
  store float %tmp_139_s, float* %C_addr_62, align 4
  %tmp_135_10 = fmul float %s_load_50, %s_load_11
  %tmp_136_10 = fmul float %tmp_135_10, %r
  %tmp_137_10 = add i12 %phi_mul2, 11
  %tmp_138_10 = zext i12 %tmp_137_10 to i64
  %C_addr_63 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_10
  %C_load_188 = load float* %C_addr_63, align 4
  %tmp_139_10 = fadd float %C_load_188, %tmp_136_10
  store float %tmp_139_10, float* %C_addr_63, align 4
  %tmp_135_11 = fmul float %s_load_50, %s_load_12
  %tmp_136_11 = fmul float %tmp_135_11, %r
  %tmp_137_11 = add i12 %phi_mul2, 12
  %tmp_138_11 = zext i12 %tmp_137_11 to i64
  %C_addr_64 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_11
  %C_load_189 = load float* %C_addr_64, align 4
  %tmp_139_11 = fadd float %C_load_189, %tmp_136_11
  store float %tmp_139_11, float* %C_addr_64, align 4
  %tmp_135_12 = fmul float %s_load_50, %s_load_13
  %tmp_136_12 = fmul float %tmp_135_12, %r
  %tmp_137_12 = add i12 %phi_mul2, 13
  %tmp_138_12 = zext i12 %tmp_137_12 to i64
  %C_addr_65 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_12
  %C_load_190 = load float* %C_addr_65, align 4
  %tmp_139_12 = fadd float %C_load_190, %tmp_136_12
  store float %tmp_139_12, float* %C_addr_65, align 4
  %tmp_135_13 = fmul float %s_load_50, %s_load_14
  %tmp_136_13 = fmul float %tmp_135_13, %r
  %tmp_137_13 = add i12 %phi_mul2, 14
  %tmp_138_13 = zext i12 %tmp_137_13 to i64
  %C_addr_66 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_13
  %C_load_191 = load float* %C_addr_66, align 4
  %tmp_139_13 = fadd float %C_load_191, %tmp_136_13
  store float %tmp_139_13, float* %C_addr_66, align 4
  %tmp_135_14 = fmul float %s_load_50, %s_load_15
  %tmp_136_14 = fmul float %tmp_135_14, %r
  %tmp_137_14 = add i12 %phi_mul2, 15
  %tmp_138_14 = zext i12 %tmp_137_14 to i64
  %C_addr_67 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_14
  %C_load_192 = load float* %C_addr_67, align 4
  %tmp_139_14 = fadd float %C_load_192, %tmp_136_14
  store float %tmp_139_14, float* %C_addr_67, align 4
  %tmp_135_15 = fmul float %s_load_50, %s_load_16
  %tmp_136_15 = fmul float %tmp_135_15, %r
  %tmp_137_15 = add i12 %phi_mul2, 16
  %tmp_138_15 = zext i12 %tmp_137_15 to i64
  %C_addr_68 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_15
  %C_load_193 = load float* %C_addr_68, align 4
  %tmp_139_15 = fadd float %C_load_193, %tmp_136_15
  store float %tmp_139_15, float* %C_addr_68, align 4
  %tmp_135_16 = fmul float %s_load_50, %s_load_17
  %tmp_136_16 = fmul float %tmp_135_16, %r
  %tmp_137_16 = add i12 %phi_mul2, 17
  %tmp_138_16 = zext i12 %tmp_137_16 to i64
  %C_addr_69 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_16
  %C_load_194 = load float* %C_addr_69, align 4
  %tmp_139_16 = fadd float %C_load_194, %tmp_136_16
  store float %tmp_139_16, float* %C_addr_69, align 4
  %tmp_135_17 = fmul float %s_load_50, %s_load_18
  %tmp_136_17 = fmul float %tmp_135_17, %r
  %tmp_137_17 = add i12 %phi_mul2, 18
  %tmp_138_17 = zext i12 %tmp_137_17 to i64
  %C_addr_70 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_17
  %C_load_195 = load float* %C_addr_70, align 4
  %tmp_139_17 = fadd float %C_load_195, %tmp_136_17
  store float %tmp_139_17, float* %C_addr_70, align 4
  %tmp_135_18 = fmul float %s_load_50, %s_load_19
  %tmp_136_18 = fmul float %tmp_135_18, %r
  %tmp_137_18 = add i12 %phi_mul2, 19
  %tmp_138_18 = zext i12 %tmp_137_18 to i64
  %C_addr_71 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_18
  %C_load_196 = load float* %C_addr_71, align 4
  %tmp_139_18 = fadd float %C_load_196, %tmp_136_18
  store float %tmp_139_18, float* %C_addr_71, align 4
  %tmp_135_19 = fmul float %s_load_50, %s_load_20
  %tmp_136_19 = fmul float %tmp_135_19, %r
  %tmp_137_19 = add i12 %phi_mul2, 20
  %tmp_138_19 = zext i12 %tmp_137_19 to i64
  %C_addr_72 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_19
  %C_load_197 = load float* %C_addr_72, align 4
  %tmp_139_19 = fadd float %C_load_197, %tmp_136_19
  store float %tmp_139_19, float* %C_addr_72, align 4
  %tmp_135_20 = fmul float %s_load_50, %s_load_21
  %tmp_136_20 = fmul float %tmp_135_20, %r
  %tmp_137_20 = add i12 %phi_mul2, 21
  %tmp_138_20 = zext i12 %tmp_137_20 to i64
  %C_addr_73 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_20
  %C_load_198 = load float* %C_addr_73, align 4
  %tmp_139_20 = fadd float %C_load_198, %tmp_136_20
  store float %tmp_139_20, float* %C_addr_73, align 4
  %tmp_135_21 = fmul float %s_load_50, %s_load_22
  %tmp_136_21 = fmul float %tmp_135_21, %r
  %tmp_137_21 = add i12 %phi_mul2, 22
  %tmp_138_21 = zext i12 %tmp_137_21 to i64
  %C_addr_74 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_21
  %C_load_199 = load float* %C_addr_74, align 4
  %tmp_139_21 = fadd float %C_load_199, %tmp_136_21
  store float %tmp_139_21, float* %C_addr_74, align 4
  %tmp_135_22 = fmul float %s_load_50, %s_load_23
  %tmp_136_22 = fmul float %tmp_135_22, %r
  %tmp_137_22 = add i12 %phi_mul2, 23
  %tmp_138_22 = zext i12 %tmp_137_22 to i64
  %C_addr_75 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_22
  %C_load_200 = load float* %C_addr_75, align 4
  %tmp_139_22 = fadd float %C_load_200, %tmp_136_22
  store float %tmp_139_22, float* %C_addr_75, align 4
  %tmp_135_23 = fmul float %s_load_50, %s_load_24
  %tmp_136_23 = fmul float %tmp_135_23, %r
  %tmp_137_23 = add i12 %phi_mul2, 24
  %tmp_138_23 = zext i12 %tmp_137_23 to i64
  %C_addr_76 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_23
  %C_load_201 = load float* %C_addr_76, align 4
  %tmp_139_23 = fadd float %C_load_201, %tmp_136_23
  store float %tmp_139_23, float* %C_addr_76, align 4
  %tmp_135_24 = fmul float %s_load_50, %s_load_25
  %tmp_136_24 = fmul float %tmp_135_24, %r
  %tmp_137_24 = add i12 %phi_mul2, 25
  %tmp_138_24 = zext i12 %tmp_137_24 to i64
  %C_addr_77 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_24
  %C_load_202 = load float* %C_addr_77, align 4
  %tmp_139_24 = fadd float %C_load_202, %tmp_136_24
  store float %tmp_139_24, float* %C_addr_77, align 4
  %tmp_135_25 = fmul float %s_load_50, %s_load_26
  %tmp_136_25 = fmul float %tmp_135_25, %r
  %tmp_137_25 = add i12 %phi_mul2, 26
  %tmp_138_25 = zext i12 %tmp_137_25 to i64
  %C_addr_78 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_25
  %C_load_203 = load float* %C_addr_78, align 4
  %tmp_139_25 = fadd float %C_load_203, %tmp_136_25
  store float %tmp_139_25, float* %C_addr_78, align 4
  %tmp_135_26 = fmul float %s_load_50, %s_load_27
  %tmp_136_26 = fmul float %tmp_135_26, %r
  %tmp_137_26 = add i12 %phi_mul2, 27
  %tmp_138_26 = zext i12 %tmp_137_26 to i64
  %C_addr_79 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_26
  %C_load_204 = load float* %C_addr_79, align 4
  %tmp_139_26 = fadd float %C_load_204, %tmp_136_26
  store float %tmp_139_26, float* %C_addr_79, align 4
  %tmp_135_27 = fmul float %s_load_50, %s_load_28
  %tmp_136_27 = fmul float %tmp_135_27, %r
  %tmp_137_27 = add i12 %phi_mul2, 28
  %tmp_138_27 = zext i12 %tmp_137_27 to i64
  %C_addr_80 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_27
  %C_load_205 = load float* %C_addr_80, align 4
  %tmp_139_27 = fadd float %C_load_205, %tmp_136_27
  store float %tmp_139_27, float* %C_addr_80, align 4
  %tmp_135_28 = fmul float %s_load_50, %s_load_29
  %tmp_136_28 = fmul float %tmp_135_28, %r
  %tmp_137_28 = add i12 %phi_mul2, 29
  %tmp_138_28 = zext i12 %tmp_137_28 to i64
  %C_addr_81 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_28
  %C_load_206 = load float* %C_addr_81, align 4
  %tmp_139_28 = fadd float %C_load_206, %tmp_136_28
  store float %tmp_139_28, float* %C_addr_81, align 4
  %tmp_135_29 = fmul float %s_load_50, %s_load_30
  %tmp_136_29 = fmul float %tmp_135_29, %r
  %tmp_137_29 = add i12 %phi_mul2, 30
  %tmp_138_29 = zext i12 %tmp_137_29 to i64
  %C_addr_82 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_29
  %C_load_207 = load float* %C_addr_82, align 4
  %tmp_139_29 = fadd float %C_load_207, %tmp_136_29
  store float %tmp_139_29, float* %C_addr_82, align 4
  %tmp_135_30 = fmul float %s_load_50, %s_load_31
  %tmp_136_30 = fmul float %tmp_135_30, %r
  %tmp_137_30 = add i12 %phi_mul2, 31
  %tmp_138_30 = zext i12 %tmp_137_30 to i64
  %C_addr_83 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_30
  %C_load_208 = load float* %C_addr_83, align 4
  %tmp_139_30 = fadd float %C_load_208, %tmp_136_30
  store float %tmp_139_30, float* %C_addr_83, align 4
  %tmp_135_31 = fmul float %s_load_50, %s_load_32
  %tmp_136_31 = fmul float %tmp_135_31, %r
  %tmp_137_31 = add i12 %phi_mul2, 32
  %tmp_138_31 = zext i12 %tmp_137_31 to i64
  %C_addr_84 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_31
  %C_load_209 = load float* %C_addr_84, align 4
  %tmp_139_31 = fadd float %C_load_209, %tmp_136_31
  store float %tmp_139_31, float* %C_addr_84, align 4
  %tmp_135_32 = fmul float %s_load_50, %s_load_33
  %tmp_136_32 = fmul float %tmp_135_32, %r
  %tmp_137_32 = add i12 %phi_mul2, 33
  %tmp_138_32 = zext i12 %tmp_137_32 to i64
  %C_addr_85 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_32
  %C_load_210 = load float* %C_addr_85, align 4
  %tmp_139_32 = fadd float %C_load_210, %tmp_136_32
  store float %tmp_139_32, float* %C_addr_85, align 4
  %tmp_135_33 = fmul float %s_load_50, %s_load_34
  %tmp_136_33 = fmul float %tmp_135_33, %r
  %tmp_137_33 = add i12 %phi_mul2, 34
  %tmp_138_33 = zext i12 %tmp_137_33 to i64
  %C_addr_86 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_33
  %C_load_211 = load float* %C_addr_86, align 4
  %tmp_139_33 = fadd float %C_load_211, %tmp_136_33
  store float %tmp_139_33, float* %C_addr_86, align 4
  %tmp_135_34 = fmul float %s_load_50, %s_load_35
  %tmp_136_34 = fmul float %tmp_135_34, %r
  %tmp_137_34 = add i12 %phi_mul2, 35
  %tmp_138_34 = zext i12 %tmp_137_34 to i64
  %C_addr_87 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_34
  %C_load_212 = load float* %C_addr_87, align 4
  %tmp_139_34 = fadd float %C_load_212, %tmp_136_34
  store float %tmp_139_34, float* %C_addr_87, align 4
  %tmp_135_35 = fmul float %s_load_50, %s_load_36
  %tmp_136_35 = fmul float %tmp_135_35, %r
  %tmp_137_35 = add i12 %phi_mul2, 36
  %tmp_138_35 = zext i12 %tmp_137_35 to i64
  %C_addr_88 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_35
  %C_load_213 = load float* %C_addr_88, align 4
  %tmp_139_35 = fadd float %C_load_213, %tmp_136_35
  store float %tmp_139_35, float* %C_addr_88, align 4
  %tmp_135_36 = fmul float %s_load_50, %s_load_37
  %tmp_136_36 = fmul float %tmp_135_36, %r
  %tmp_137_36 = add i12 %phi_mul2, 37
  %tmp_138_36 = zext i12 %tmp_137_36 to i64
  %C_addr_89 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_36
  %C_load_214 = load float* %C_addr_89, align 4
  %tmp_139_36 = fadd float %C_load_214, %tmp_136_36
  store float %tmp_139_36, float* %C_addr_89, align 4
  %tmp_135_37 = fmul float %s_load_50, %s_load_38
  %tmp_136_37 = fmul float %tmp_135_37, %r
  %tmp_137_37 = add i12 %phi_mul2, 38
  %tmp_138_37 = zext i12 %tmp_137_37 to i64
  %C_addr_90 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_37
  %C_load_215 = load float* %C_addr_90, align 4
  %tmp_139_37 = fadd float %C_load_215, %tmp_136_37
  store float %tmp_139_37, float* %C_addr_90, align 4
  %tmp_135_38 = fmul float %s_load_50, %s_load_39
  %tmp_136_38 = fmul float %tmp_135_38, %r
  %tmp_137_38 = add i12 %phi_mul2, 39
  %tmp_138_38 = zext i12 %tmp_137_38 to i64
  %C_addr_91 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_38
  %C_load_216 = load float* %C_addr_91, align 4
  %tmp_139_38 = fadd float %C_load_216, %tmp_136_38
  store float %tmp_139_38, float* %C_addr_91, align 4
  %tmp_135_39 = fmul float %s_load_50, %s_load_40
  %tmp_136_39 = fmul float %tmp_135_39, %r
  %tmp_137_39 = add i12 %phi_mul2, 40
  %tmp_138_39 = zext i12 %tmp_137_39 to i64
  %C_addr_92 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_39
  %C_load_217 = load float* %C_addr_92, align 4
  %tmp_139_39 = fadd float %C_load_217, %tmp_136_39
  store float %tmp_139_39, float* %C_addr_92, align 4
  %tmp_135_40 = fmul float %s_load_50, %s_load_41
  %tmp_136_40 = fmul float %tmp_135_40, %r
  %tmp_137_40 = add i12 %phi_mul2, 41
  %tmp_138_40 = zext i12 %tmp_137_40 to i64
  %C_addr_93 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_40
  %C_load_218 = load float* %C_addr_93, align 4
  %tmp_139_40 = fadd float %C_load_218, %tmp_136_40
  store float %tmp_139_40, float* %C_addr_93, align 4
  %tmp_135_41 = fmul float %s_load_50, %s_load_42
  %tmp_136_41 = fmul float %tmp_135_41, %r
  %tmp_137_41 = add i12 %phi_mul2, 42
  %tmp_138_41 = zext i12 %tmp_137_41 to i64
  %C_addr_94 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_41
  %C_load_219 = load float* %C_addr_94, align 4
  %tmp_139_41 = fadd float %C_load_219, %tmp_136_41
  store float %tmp_139_41, float* %C_addr_94, align 4
  %tmp_135_42 = fmul float %s_load_50, %s_load_43
  %tmp_136_42 = fmul float %tmp_135_42, %r
  %tmp_137_42 = add i12 %phi_mul2, 43
  %tmp_138_42 = zext i12 %tmp_137_42 to i64
  %C_addr_95 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_42
  %C_load_220 = load float* %C_addr_95, align 4
  %tmp_139_42 = fadd float %C_load_220, %tmp_136_42
  store float %tmp_139_42, float* %C_addr_95, align 4
  %tmp_135_43 = fmul float %s_load_50, %s_load_44
  %tmp_136_43 = fmul float %tmp_135_43, %r
  %tmp_137_43 = add i12 %phi_mul2, 44
  %tmp_138_43 = zext i12 %tmp_137_43 to i64
  %C_addr_96 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_43
  %C_load_221 = load float* %C_addr_96, align 4
  %tmp_139_43 = fadd float %C_load_221, %tmp_136_43
  store float %tmp_139_43, float* %C_addr_96, align 4
  %tmp_135_44 = fmul float %s_load_50, %s_load_45
  %tmp_136_44 = fmul float %tmp_135_44, %r
  %tmp_137_44 = add i12 %phi_mul2, 45
  %tmp_138_44 = zext i12 %tmp_137_44 to i64
  %C_addr_97 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_44
  %C_load_222 = load float* %C_addr_97, align 4
  %tmp_139_44 = fadd float %C_load_222, %tmp_136_44
  store float %tmp_139_44, float* %C_addr_97, align 4
  %tmp_135_45 = fmul float %s_load_50, %s_load_46
  %tmp_136_45 = fmul float %tmp_135_45, %r
  %tmp_137_45 = add i12 %phi_mul2, 46
  %tmp_138_45 = zext i12 %tmp_137_45 to i64
  %C_addr_98 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_45
  %C_load_223 = load float* %C_addr_98, align 4
  %tmp_139_45 = fadd float %C_load_223, %tmp_136_45
  store float %tmp_139_45, float* %C_addr_98, align 4
  %tmp_135_46 = fmul float %s_load_50, %s_load_47
  %tmp_136_46 = fmul float %tmp_135_46, %r
  %tmp_137_46 = add i12 %phi_mul2, 47
  %tmp_138_46 = zext i12 %tmp_137_46 to i64
  %C_addr_99 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_46
  %C_load_224 = load float* %C_addr_99, align 4
  %tmp_139_46 = fadd float %C_load_224, %tmp_136_46
  store float %tmp_139_46, float* %C_addr_99, align 4
  %tmp_135_47 = fmul float %s_load_50, %s_load_48
  %tmp_136_47 = fmul float %tmp_135_47, %r
  %tmp_137_47 = add i12 %phi_mul2, 48
  %tmp_138_47 = zext i12 %tmp_137_47 to i64
  %C_addr_100 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_47
  %C_load_225 = load float* %C_addr_100, align 4
  %tmp_139_47 = fadd float %C_load_225, %tmp_136_47
  store float %tmp_139_47, float* %C_addr_100, align 4
  %tmp_135_48 = fmul float %s_load_50, %s_load_49
  %tmp_136_48 = fmul float %tmp_135_48, %r
  %tmp_137_48 = add i12 %phi_mul2, 49
  %tmp_138_48 = zext i12 %tmp_137_48 to i64
  %C_addr_101 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_48
  %C_load_226 = load float* %C_addr_101, align 4
  %tmp_139_48 = fadd float %C_load_226, %tmp_136_48
  store float %tmp_139_48, float* %C_addr_101, align 4
  %tmp_136_49 = fmul float %s_load_50, %r
  %tmp_137_49 = add i12 %phi_mul2, 50
  %tmp_138_49 = zext i12 %tmp_137_49 to i64
  %C_addr_102 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_138_49
  %C_load_227 = load float* %C_addr_102, align 4
  %tmp_139_49 = fadd float %C_load_227, %tmp_136_49
  store float %tmp_139_49, float* %C_addr_102, align 4
  %empty_15 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str12, i32 %tmp_171)
  br label %1

; <label>:3                                       ; preds = %.preheader.preheader, %.preheader
  %indvar_flatten = phi i12 [ 0, %.preheader.preheader ], [ %indvar_flatten_next, %.preheader ]
  %i6 = phi i6 [ 0, %.preheader.preheader ], [ %i6_mid2, %.preheader ]
  %j7 = phi i6 [ 0, %.preheader.preheader ], [ %j, %.preheader ]
  %exitcond_flatten = icmp eq i12 %indvar_flatten, -1495
  %indvar_flatten_next = add i12 %indvar_flatten, 1
  br i1 %exitcond_flatten, label %.preheader138, label %.preheader

.preheader:                                       ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopName([30 x i8]* @UPDATE_Q_OUTER_UPDATE_Q_INNER_s)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 2601, i64 2601, i64 2601)
  %exitcond = icmp eq i6 %j7, -13
  %j7_mid2 = select i1 %exitcond, i6 0, i6 %j7
  %i_2 = add i6 %i6, 1
  %i6_mid2 = select i1 %exitcond, i6 %i_2, i6 %i6
  %i6_cast6 = zext i6 %i6_mid2 to i12
  %tmp_94 = icmp eq i6 %i6_mid2, -14
  %tmp_95 = mul i12 %i6_cast6, 51
  %tmp_96 = zext i6 %i6_mid2 to i64
  %e_addr_4 = getelementptr inbounds [51 x float]* @e, i64 0, i64 %tmp_96
  %e_load_3 = load float* %e_addr_4, align 4
  %ti = select i1 %tmp_94, float -1.000000e+00, float %e_load_3
  %j7_cast4 = zext i6 %j7_mid2 to i12
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str15) nounwind
  %tmp_172 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str15)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_97 = icmp eq i6 %j7_mid2, -14
  %tmp_98 = zext i6 %j7_mid2 to i64
  %e_addr_5 = getelementptr inbounds [51 x float]* @e, i64 0, i64 %tmp_98
  %e_load_4 = load float* %e_addr_5, align 4
  %tj = select i1 %tmp_97, float -1.000000e+00, float %e_load_4
  %tmp_99 = fmul float %ti, %tj
  %tmp_100 = fpext float %tmp_99 to double
  %tmp_101 = fdiv double %tmp_100, %tmp_91
  %tmp_102 = add i12 %j7_cast4, %tmp_95
  %tmp_104 = zext i12 %tmp_102 to i64
  %Q_addr_7 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_104
  %Q_load_6 = load float* %Q_addr_7, align 4
  %tmp_105 = fpext float %Q_load_6 to double
  %tmp_106 = fadd double %tmp_105, %tmp_101
  %tmp_107 = fptrunc double %tmp_106 to float
  store float %tmp_107, float* %Q_addr_7, align 4
  %empty_16 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str15, i32 %tmp_172)
  %j = add i6 %j7_mid2, 1
  br label %3

.preheader138:                                    ; preds = %3, %4
  %i_i = phi i5 [ %i_11, %4 ], [ 0, %3 ]
  %i_i_cast2 = zext i5 %i_i to i32
  %exitcond_i = icmp eq i5 %i_i, -12
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 20, i64 20, i64 20)
  %i_11 = add i5 %i_i, 1
  br i1 %exitcond_i, label %copyBV.exit, label %4

; <label>:4                                       ; preds = %.preheader138
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @p_str16) nounwind
  %tmp_i = zext i5 %i_i to i64
  %pX_addr = getelementptr [20 x float]* %pX, i64 0, i64 %tmp_i
  %pX_load = load float* %pX_addr, align 4
  %bvCnt_load = load i32* @bvCnt, align 4
  %tmp_59 = shl i32 %bvCnt_load, 4
  %tmp_60 = shl i32 %bvCnt_load, 2
  %tmp = add i32 %tmp_60, %i_i_cast2
  %tmp_99_i = add i32 %tmp, %tmp_59
  %tmp_100_i = zext i32 %tmp_99_i to i64
  %basisVectors_addr = getelementptr inbounds [1020 x float]* @basisVectors, i64 0, i64 %tmp_100_i
  store float %pX_load, float* %basisVectors_addr, align 4
  br label %.preheader138

copyBV.exit:                                      ; preds = %.preheader138
  %tmp_i1 = fmul float %tmp_127, %tmp_127
  %Q_load_179 = load float* getelementptr inbounds ([2601 x float]* @Q, i64 0, i64 0), align 16
  %C_load_228 = load float* getelementptr inbounds ([2601 x float]* @C, i64 0, i64 0), align 16
  %tmp_i1_17 = fadd float %Q_load_179, %C_load_228
  %minScore = fdiv float %tmp_i1, %tmp_i1_17
  br label %5

; <label>:5                                       ; preds = %6, %copyBV.exit
  %index_3 = phi i6 [ 1, %copyBV.exit ], [ %i_12, %6 ]
  %minScore1_i = phi float [ %minScore, %copyBV.exit ], [ %minScore_4, %6 ]
  %index = phi i32 [ 0, %copyBV.exit ], [ %index_4, %6 ]
  %index_3_cast1 = zext i6 %index_3 to i32
  %index_3_cast1_cast = zext i6 %index_3 to i13
  %exitcond_i1 = icmp eq i6 %index_3, -13
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 50, i64 50, i64 50) nounwind
  br i1 %exitcond_i1, label %getMinKLApprox.exit, label %6

; <label>:6                                       ; preds = %5
  %tmp_17_i = zext i6 %index_3 to i64
  %alpha_addr = getelementptr inbounds [51 x float]* @alpha, i64 0, i64 %tmp_17_i
  %alpha_load_55 = load float* %alpha_addr, align 4
  %tmp_18_i = fmul float %alpha_load_55, %alpha_load_55
  %tmp_19_i = mul i13 52, %index_3_cast1_cast
  %tmp_20_i = zext i13 %tmp_19_i to i64
  %Q_addr_52 = getelementptr inbounds [2601 x float]* @Q, i64 0, i64 %tmp_20_i
  %Q_load_180 = load float* %Q_addr_52, align 16
  %C_addr_103 = getelementptr inbounds [2601 x float]* @C, i64 0, i64 %tmp_20_i
  %C_load_229 = load float* %C_addr_103, align 16
  %tmp_21_i = fadd float %Q_load_180, %C_load_229
  %tScore = fdiv float %tmp_18_i, %tmp_21_i
  %tScore_to_int = bitcast float %tScore to i32
  %tmp_10 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tScore_to_int, i32 23, i32 30)
  %tmp_61 = trunc i32 %tScore_to_int to i23
  %minScore1_i_to_int = bitcast float %minScore1_i to i32
  %tmp_12 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %minScore1_i_to_int, i32 23, i32 30)
  %tmp_62 = trunc i32 %minScore1_i_to_int to i23
  %notlhs = icmp ne i8 %tmp_10, -1
  %notrhs = icmp eq i23 %tmp_61, 0
  %tmp_14 = or i1 %notrhs, %notlhs
  %notlhs1 = icmp ne i8 %tmp_12, -1
  %notrhs1 = icmp eq i23 %tmp_62, 0
  %tmp_15 = or i1 %notrhs1, %notlhs1
  %tmp_16 = and i1 %tmp_14, %tmp_15
  %tmp_17 = fcmp olt float %tScore, %minScore1_i
  %tmp_18 = and i1 %tmp_16, %tmp_17
  %minScore_4 = select i1 %tmp_18, float %tScore, float %minScore1_i
  %index_4 = select i1 %tmp_18, i32 %index_3_cast1, i32 %index
  %i_12 = add i6 1, %index_3
  br label %5

getMinKLApprox.exit:                              ; preds = %5
  call fastcc void @projection_gp_deleteBV(i32 %index)
  ret void
}

define internal fastcc void @projection_gp_swapRowAndColumn([2601 x float]* nocapture %pM, i32 %rowA) {
  %rowA_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %rowA)
  %tmp = mul i32 %rowA_read, 51
  br label %1

; <label>:1                                       ; preds = %2, %0
  %i = phi i6 [ 0, %0 ], [ %i_2, %2 ]
  %i_cast4 = zext i6 %i to i12
  %i_cast3 = zext i6 %i to i32
  %exitcond1 = icmp eq i6 %i, -13
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 51, i64 51, i64 51)
  %i_2 = add i6 %i, 1
  br i1 %exitcond1, label %.preheader, label %2

; <label>:2                                       ; preds = %1
  %tmp_6 = add i32 %i_cast3, %tmp
  %tmp_7 = zext i32 %tmp_6 to i64
  %pM_addr = getelementptr [2601 x float]* %pM, i64 0, i64 %tmp_7
  %temp = load float* %pM_addr, align 4
  %tmp_8 = add i12 %i_cast4, -1546
  %tmp_9 = zext i12 %tmp_8 to i64
  %pM_addr_1 = getelementptr [2601 x float]* %pM, i64 0, i64 %tmp_9
  %pM_load = load float* %pM_addr_1, align 4
  store float %pM_load, float* %pM_addr, align 4
  store float %temp, float* %pM_addr_1, align 4
  br label %1

.preheader:                                       ; preds = %1, %3
  %i1 = phi i6 [ %i_3, %3 ], [ 0, %1 ]
  %phi_mul = phi i12 [ %next_mul, %3 ], [ 0, %1 ]
  %exitcond = icmp eq i6 %i1, -13
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 51, i64 51, i64 51)
  %i_3 = add i6 %i1, 1
  br i1 %exitcond, label %4, label %3

; <label>:3                                       ; preds = %.preheader
  %next_mul = add i12 %phi_mul, 51
  %tmp_cast5 = zext i12 %phi_mul to i32
  %tmp_1 = add i32 %tmp_cast5, %rowA_read
  %tmp_2 = zext i32 %tmp_1 to i64
  %pM_addr_2 = getelementptr [2601 x float]* %pM, i64 0, i64 %tmp_2
  %temp_3 = load float* %pM_addr_2, align 4
  %tmp_3 = add i12 %phi_mul, 50
  %tmp_4 = zext i12 %tmp_3 to i64
  %pM_addr_3 = getelementptr [2601 x float]* %pM, i64 0, i64 %tmp_4
  %pM_load_2 = load float* %pM_addr_3, align 4
  store float %pM_load_2, float* %pM_addr_2, align 4
  store float %temp_3, float* %pM_addr_3, align 4
  br label %.preheader

; <label>:4                                       ; preds = %.preheader
  ret void
}

define internal fastcc float @projection_gp_K([1020 x float]* nocapture %pX1, i11 %tmp_152, [20 x float]* nocapture %pX2) readonly {
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([20 x float]* %pX2, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3)
  %tmp_152_read = call i11 @_ssdm_op_Read.ap_auto.i11(i11 %tmp_152)
  %tmp = trunc i11 %tmp_152_read to i10
  br label %1

; <label>:1                                       ; preds = %2, %0
  %sum = phi float [ 0.000000e+00, %0 ], [ %sum_1, %2 ]
  %i = phi i5 [ 0, %0 ], [ %i_1, %2 ]
  %exitcond = icmp eq i5 %i, -12
  %i_1 = add i5 %i, 1
  br i1 %exitcond, label %3, label %2

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 20, i64 20, i64 20)
  %tmp_s = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_2 = zext i5 %i to i64
  %tmp_2_cast = zext i5 %i to i10
  %sum1 = add i10 %tmp_2_cast, %tmp
  %sum1_cast = zext i10 %sum1 to i64
  %pX1_addr = getelementptr [1020 x float]* %pX1, i64 0, i64 %sum1_cast
  %pX1_load = load float* %pX1_addr, align 4
  %pX2_addr = getelementptr [20 x float]* %pX2, i64 0, i64 %tmp_2
  %pX2_load = load float* %pX2_addr, align 4
  %val = fsub float %pX1_load, %pX2_load
  %tmp_3 = fmul float %val, %val
  %sum_1 = fadd float %sum, %tmp_3
  %empty_18 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_s)
  br label %1

; <label>:3                                       ; preds = %1
  %p_x_assign = fmul float %sum, -5.000000e-01
  %tmp_i = call float @llvm.exp.f32(float %p_x_assign) nounwind
  ret float %tmp_i
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_19 = trunc i32 %empty to i8
  ret i8 %empty_19
}

define weak i32 @_ssdm_op_SpecMemCore(...) {
entry:
  ret i32 0
}

define weak float @_ssdm_op_Read.s_axilite.float(float) {
entry:
  ret float %0
}

define weak i1 @_ssdm_op_Read.s_axilite.i1(i1) {
entry:
  ret i1 %0
}

define weak float @_ssdm_op_Read.ap_auto.float(float) {
entry:
  ret float %0
}

define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

define weak i11 @_ssdm_op_Read.ap_auto.i11(i11) {
entry:
  ret i11 %0
}

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare i30 @_ssdm_op_PartSelect.i30.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i32 @_ssdm_op_BitConcatenate.i32.i30.i2(i30, i2) nounwind readnone

declare i28 @_ssdm_op_PartSelect.i28.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i32 @_ssdm_op_BitConcatenate.i32.i28.i4(i28, i4) nounwind readnone

declare i11 @_ssdm_op_PartSelect.i11.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i10 @_ssdm_op_PartSelect.i10.i11.i32.i32(i11, i32, i32) nounwind readnone

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0, !7, !12, !19, !26, !31, !36, !43, !50}

!0 = metadata !{metadata !1, [2601 x float]* @C}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"C", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 2600, i32 1}
!7 = metadata !{metadata !8, [2601 x float]* @Q}
!8 = metadata !{metadata !9}
!9 = metadata !{i32 0, i32 31, metadata !10}
!10 = metadata !{metadata !11}
!11 = metadata !{metadata !"Q", metadata !5, metadata !"float", i32 0, i32 31}
!12 = metadata !{metadata !13, [51 x float]* @e}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"e", metadata !17, metadata !"float", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 50, i32 1}
!19 = metadata !{metadata !20, [50 x float]* @k}
!20 = metadata !{metadata !21}
!21 = metadata !{i32 0, i32 31, metadata !22}
!22 = metadata !{metadata !23}
!23 = metadata !{metadata !"k", metadata !24, metadata !"float", i32 0, i32 31}
!24 = metadata !{metadata !25}
!25 = metadata !{i32 0, i32 49, i32 1}
!26 = metadata !{metadata !27, [51 x float]* @s}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 31, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"s", metadata !17, metadata !"float", i32 0, i32 31}
!31 = metadata !{metadata !32, [51 x float]* @alpha}
!32 = metadata !{metadata !33}
!33 = metadata !{i32 0, i32 31, metadata !34}
!34 = metadata !{metadata !35}
!35 = metadata !{metadata !"alpha", metadata !17, metadata !"float", i32 0, i32 31}
!36 = metadata !{metadata !37, [1020 x float]* @basisVectors}
!37 = metadata !{metadata !38}
!38 = metadata !{i32 0, i32 31, metadata !39}
!39 = metadata !{metadata !40}
!40 = metadata !{metadata !"basisVectors", metadata !41, metadata !"float", i32 0, i32 31}
!41 = metadata !{metadata !42}
!42 = metadata !{i32 0, i32 1019, i32 1}
!43 = metadata !{metadata !44, i32* @bvCnt}
!44 = metadata !{metadata !45}
!45 = metadata !{i32 0, i32 31, metadata !46}
!46 = metadata !{metadata !47}
!47 = metadata !{metadata !"bvCnt", metadata !48, metadata !"unsigned int", i32 0, i32 31}
!48 = metadata !{metadata !49}
!49 = metadata !{i32 0, i32 0, i32 1}
!50 = metadata !{metadata !51, [1 x i32]* @llvm_global_ctors_0}
!51 = metadata !{metadata !52}
!52 = metadata !{i32 0, i32 31, metadata !53}
!53 = metadata !{metadata !54}
!54 = metadata !{metadata !"llvm.global_ctors.0", metadata !48, metadata !"", i32 0, i32 31}
!55 = metadata !{metadata !56}
!56 = metadata !{i32 0, i32 31, metadata !57}
!57 = metadata !{metadata !58}
!58 = metadata !{metadata !"pX", metadata !59, metadata !"float", i32 0, i32 31}
!59 = metadata !{metadata !60}
!60 = metadata !{i32 0, i32 19, i32 1}
!61 = metadata !{metadata !62}
!62 = metadata !{i32 0, i32 31, metadata !63}
!63 = metadata !{metadata !64}
!64 = metadata !{metadata !"pY", metadata !65, metadata !"float", i32 0, i32 31}
!65 = metadata !{metadata !66}
!66 = metadata !{i32 0, i32 0, i32 0}
!67 = metadata !{metadata !68}
!68 = metadata !{i32 0, i32 0, metadata !69}
!69 = metadata !{metadata !70}
!70 = metadata !{metadata !"pPredict", metadata !65, metadata !"bool", i32 0, i32 0}
!71 = metadata !{metadata !72}
!72 = metadata !{i32 0, i32 31, metadata !73}
!73 = metadata !{metadata !74}
!74 = metadata !{metadata !"return", metadata !75, metadata !"float", i32 0, i32 31}
!75 = metadata !{metadata !76}
!76 = metadata !{i32 0, i32 1, i32 0}
