#!/bin/sh
lli=${LLVMINTERP-lli}
exec $lli \
    /home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace/ProjectionGP_DIM_20_NUMBV_50/ProjectionGP_DIM_20_NUMBV_50/.autopilot/db/a.g.bc ${1+"$@"}
