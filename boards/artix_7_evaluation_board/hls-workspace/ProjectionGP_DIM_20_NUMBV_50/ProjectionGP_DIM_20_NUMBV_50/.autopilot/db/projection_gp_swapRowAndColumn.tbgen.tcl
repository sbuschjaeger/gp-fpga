set moduleName projection_gp_swapRowAndColumn
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {projection_gp_swapRowAndColumn}
set C_modelType { void 0 }
set C_modelArgList { 
	{ pM float 32 regular {array 2601 { 2 3 } 1 1 }  }
	{ rowA int 32 regular  }
}
set C_modelArgMapList {[ 
	{ "Name" : "pM", "interface" : "memory", "bitwidth" : 32 ,"direction" : "READWRITE" } , 
 	{ "Name" : "rowA", "interface" : "wire", "bitwidth" : 32 ,"direction" : "READONLY" } ]}
# RTL Port declarations: 
set portNum 12
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ pM_address0 sc_out sc_lv 12 signal 0 } 
	{ pM_ce0 sc_out sc_logic 1 signal 0 } 
	{ pM_we0 sc_out sc_logic 1 signal 0 } 
	{ pM_d0 sc_out sc_lv 32 signal 0 } 
	{ pM_q0 sc_in sc_lv 32 signal 0 } 
	{ rowA sc_in sc_lv 32 signal 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "pM_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":12, "type": "signal", "bundle":{"name": "pM", "role": "address0" }} , 
 	{ "name": "pM_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pM", "role": "ce0" }} , 
 	{ "name": "pM_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "pM", "role": "we0" }} , 
 	{ "name": "pM_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "pM", "role": "d0" }} , 
 	{ "name": "pM_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "pM", "role": "q0" }} , 
 	{ "name": "rowA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "rowA", "role": "default" }}  ]}
set Spec2ImplPortList { 
	pM { ap_memory {  { pM_address0 mem_address 1 12 }  { pM_ce0 mem_ce 1 1 }  { pM_we0 mem_we 1 1 }  { pM_d0 mem_din 1 32 }  { pM_q0 mem_dout 0 32 } } }
	rowA { ap_none {  { rowA in_data 0 32 } } }
}
