; ModuleID = '/home/buschjae/projects/masterarbeit/masterarbeit-implementation/boards/artix_7_evaluation_board/hls-workspace/Projection_GP_NO_OPTIMIZATION/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@C = global [10201 x float] zeroinitializer, align 16
@Q = global [10201 x float] zeroinitializer, align 16
@e = global [101 x float] zeroinitializer, align 16
@k = global [100 x float] zeroinitializer, align 16
@s = global [101 x float] zeroinitializer, align 16
@alpha = global [101 x float] zeroinitializer, align 16
@basisVectors = global [2121 x float] zeroinitializer, align 16
@bvCnt = global i32 0, align 4
@p_str = private unnamed_addr constant [12 x i8] c"hls_label_0\00", align 1
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str2 = private unnamed_addr constant [20 x i8] c"DELETE_BV_SWAP_LOOP\00", align 1
@p_str4 = private unnamed_addr constant [18 x i8] c"DELETE_BV_C_OUTER\00", align 1
@p_str6 = private unnamed_addr constant [20 x i8] c"DELETE_BV_UNSET_C_Q\00", align 1
@p_str7 = private unnamed_addr constant [7 x i8] c"CALC_K\00", align 1
@p_str8 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_OUTER\00", align 1
@p_str9 = private unnamed_addr constant [17 x i8] c"CALC_SIGMA_INNER\00", align 1
@p_str10 = private unnamed_addr constant [7 x i8] c"CALC_S\00", align 1
@p_str11 = private unnamed_addr constant [13 x i8] c"UPDATE_ALPHA\00", align 1
@p_str12 = private unnamed_addr constant [15 x i8] c"UPDATE_C_OUTER\00", align 1
@p_str13 = private unnamed_addr constant [15 x i8] c"UPDATE_C_INNER\00", align 1
@p_str15 = private unnamed_addr constant [15 x i8] c"UPDATE_Q_INNER\00", align 1
@p_str16 = private unnamed_addr constant [8 x i8] c"COPY_BV\00", align 1
@p_str17 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@projection_gp_str = internal unnamed_addr constant [14 x i8] c"projection_gp\00"
@UPDATE_Q_OUTER_UPDATE_Q_INNER_s = internal unnamed_addr constant [30 x i8] c"UPDATE_Q_OUTER_UPDATE_Q_INNER\00"
@p_str3 = internal unnamed_addr constant [1 x i8] zeroinitializer
@RAM_1P_str = internal unnamed_addr constant [7 x i8] c"RAM_1P\00"

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

define internal fastcc void @projection_gp_deleteBV(i32 %pIndex) nounwind uwtable {
  %pIndex_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %pIndex) nounwind
  %tmp = zext i32 %pIndex_read to i64
  %alpha_addr = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp
  %temp = load float* %alpha_addr, align 4
  %alpha_load = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16
  store float %alpha_load, float* %alpha_addr, align 4
  store float %temp, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16
  call fastcc void @projection_gp_swapRowAndColumn([10201 x float]* @C, i32 %pIndex_read) nounwind
  call fastcc void @projection_gp_swapRowAndColumn([10201 x float]* @Q, i32 %pIndex_read) nounwind
  %tmp_s = mul i32 %pIndex_read, 21
  br label %1

; <label>:1                                       ; preds = %2, %0
  %i = phi i5 [ 0, %0 ], [ %i_1, %2 ]
  %exitcond4 = icmp eq i5 %i, -11
  %i_1 = add i5 %i, 1
  br i1 %exitcond4, label %3, label %2

; <label>:2                                       ; preds = %1
  %i_cast7 = zext i5 %i to i12
  %i_cast6 = zext i5 %i to i32
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 21, i64 21, i64 21)
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @p_str2) nounwind
  %tmp_4 = call i32 (...)* @_ssdm_op_SpecRegionBegin([20 x i8]* @p_str2) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_6 = add i32 %i_cast6, %tmp_s
  %tmp_7 = zext i32 %tmp_6 to i64
  %basisVectors_addr = getelementptr inbounds [2121 x float]* @basisVectors, i64 0, i64 %tmp_7
  %temp_2 = load float* %basisVectors_addr, align 4
  %tmp_8 = add i12 %i_cast7, -1996
  %tmp_9 = zext i12 %tmp_8 to i64
  %basisVectors_addr_1 = getelementptr inbounds [2121 x float]* @basisVectors, i64 0, i64 %tmp_9
  %basisVectors_load = load float* %basisVectors_addr_1, align 4
  store float %basisVectors_load, float* %basisVectors_addr, align 4
  store float %temp_2, float* %basisVectors_addr_1, align 4
  %empty = call i32 (...)* @_ssdm_op_SpecRegionEnd([20 x i8]* @p_str2, i32 %tmp_4) nounwind
  br label %1

; <label>:3                                       ; preds = %1
  %alphaStar = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16
  %cStar = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10200), align 16
  %qStar = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10200), align 16
  %tmp_5 = fadd float %cStar, %qStar
  %temp_1 = fdiv float %alphaStar, %tmp_5
  %C_load = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10100), align 16
  %Q_load = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10100), align 16
  %tmp_1 = fadd float %C_load, %Q_load
  %tmp_2 = fmul float %tmp_1, %temp_1
  %alpha_load_3 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_3 = fsub float %alpha_load_3, %tmp_2
  store float %tmp_3, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 0), align 16
  %C_load_1 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10101), align 4
  %Q_load_1 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10101), align 4
  %tmp_11_1 = fadd float %C_load_1, %Q_load_1
  %tmp_12_1 = fmul float %tmp_11_1, %temp_1
  %alpha_load_4 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 1), align 4
  %tmp_14_1 = fsub float %alpha_load_4, %tmp_12_1
  store float %tmp_14_1, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 1), align 4
  %C_load_2 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10102), align 8
  %Q_load_2 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10102), align 8
  %tmp_11_2 = fadd float %C_load_2, %Q_load_2
  %tmp_12_2 = fmul float %tmp_11_2, %temp_1
  %alpha_load_5 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 2), align 8
  %tmp_14_2 = fsub float %alpha_load_5, %tmp_12_2
  store float %tmp_14_2, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 2), align 8
  %C_load_3 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10103), align 4
  %Q_load_3 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10103), align 4
  %tmp_11_3 = fadd float %C_load_3, %Q_load_3
  %tmp_12_3 = fmul float %tmp_11_3, %temp_1
  %alpha_load_6 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 3), align 4
  %tmp_14_3 = fsub float %alpha_load_6, %tmp_12_3
  store float %tmp_14_3, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 3), align 4
  %C_load_4 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10104), align 16
  %Q_load_4 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10104), align 16
  %tmp_11_4 = fadd float %C_load_4, %Q_load_4
  %tmp_12_4 = fmul float %tmp_11_4, %temp_1
  %alpha_load_7 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 4), align 16
  %tmp_14_4 = fsub float %alpha_load_7, %tmp_12_4
  store float %tmp_14_4, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 4), align 16
  %C_load_5 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10105), align 4
  %Q_load_5 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10105), align 4
  %tmp_11_5 = fadd float %C_load_5, %Q_load_5
  %tmp_12_5 = fmul float %tmp_11_5, %temp_1
  %alpha_load_8 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 5), align 4
  %tmp_14_5 = fsub float %alpha_load_8, %tmp_12_5
  store float %tmp_14_5, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 5), align 4
  %C_load_6 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10106), align 8
  %Q_load_6 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10106), align 8
  %tmp_11_6 = fadd float %C_load_6, %Q_load_6
  %tmp_12_6 = fmul float %tmp_11_6, %temp_1
  %alpha_load_9 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 6), align 8
  %tmp_14_6 = fsub float %alpha_load_9, %tmp_12_6
  store float %tmp_14_6, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 6), align 8
  %C_load_7 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10107), align 4
  %Q_load_7 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10107), align 4
  %tmp_11_7 = fadd float %C_load_7, %Q_load_7
  %tmp_12_7 = fmul float %tmp_11_7, %temp_1
  %alpha_load_10 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 7), align 4
  %tmp_14_7 = fsub float %alpha_load_10, %tmp_12_7
  store float %tmp_14_7, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 7), align 4
  %C_load_8 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10108), align 16
  %Q_load_8 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10108), align 16
  %tmp_11_8 = fadd float %C_load_8, %Q_load_8
  %tmp_12_8 = fmul float %tmp_11_8, %temp_1
  %alpha_load_11 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 8), align 16
  %tmp_14_8 = fsub float %alpha_load_11, %tmp_12_8
  store float %tmp_14_8, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 8), align 16
  %C_load_9 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10109), align 4
  %Q_load_9 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10109), align 4
  %tmp_11_9 = fadd float %C_load_9, %Q_load_9
  %tmp_12_9 = fmul float %tmp_11_9, %temp_1
  %alpha_load_12 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 9), align 4
  %tmp_14_9 = fsub float %alpha_load_12, %tmp_12_9
  store float %tmp_14_9, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 9), align 4
  %C_load_10 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10110), align 8
  %Q_load_10 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10110), align 8
  %tmp_11_s = fadd float %C_load_10, %Q_load_10
  %tmp_12_s = fmul float %tmp_11_s, %temp_1
  %alpha_load_13 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 10), align 8
  %tmp_14_s = fsub float %alpha_load_13, %tmp_12_s
  store float %tmp_14_s, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 10), align 8
  %C_load_11 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10111), align 4
  %Q_load_11 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10111), align 4
  %tmp_11_10 = fadd float %C_load_11, %Q_load_11
  %tmp_12_10 = fmul float %tmp_11_10, %temp_1
  %alpha_load_14 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 11), align 4
  %tmp_14_10 = fsub float %alpha_load_14, %tmp_12_10
  store float %tmp_14_10, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 11), align 4
  %C_load_12 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10112), align 16
  %Q_load_12 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10112), align 16
  %tmp_11_11 = fadd float %C_load_12, %Q_load_12
  %tmp_12_11 = fmul float %tmp_11_11, %temp_1
  %alpha_load_15 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 12), align 16
  %tmp_14_11 = fsub float %alpha_load_15, %tmp_12_11
  store float %tmp_14_11, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 12), align 16
  %C_load_13 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10113), align 4
  %Q_load_13 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10113), align 4
  %tmp_11_12 = fadd float %C_load_13, %Q_load_13
  %tmp_12_12 = fmul float %tmp_11_12, %temp_1
  %alpha_load_16 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 13), align 4
  %tmp_14_12 = fsub float %alpha_load_16, %tmp_12_12
  store float %tmp_14_12, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 13), align 4
  %C_load_14 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10114), align 8
  %Q_load_14 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10114), align 8
  %tmp_11_13 = fadd float %C_load_14, %Q_load_14
  %tmp_12_13 = fmul float %tmp_11_13, %temp_1
  %alpha_load_17 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 14), align 8
  %tmp_14_13 = fsub float %alpha_load_17, %tmp_12_13
  store float %tmp_14_13, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 14), align 8
  %C_load_15 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10115), align 4
  %Q_load_15 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10115), align 4
  %tmp_11_14 = fadd float %C_load_15, %Q_load_15
  %tmp_12_14 = fmul float %tmp_11_14, %temp_1
  %alpha_load_18 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 15), align 4
  %tmp_14_14 = fsub float %alpha_load_18, %tmp_12_14
  store float %tmp_14_14, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 15), align 4
  %C_load_16 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10116), align 16
  %Q_load_16 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10116), align 16
  %tmp_11_15 = fadd float %C_load_16, %Q_load_16
  %tmp_12_15 = fmul float %tmp_11_15, %temp_1
  %alpha_load_19 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 16), align 16
  %tmp_14_15 = fsub float %alpha_load_19, %tmp_12_15
  store float %tmp_14_15, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 16), align 16
  %C_load_17 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10117), align 4
  %Q_load_17 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10117), align 4
  %tmp_11_16 = fadd float %C_load_17, %Q_load_17
  %tmp_12_16 = fmul float %tmp_11_16, %temp_1
  %alpha_load_20 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 17), align 4
  %tmp_14_16 = fsub float %alpha_load_20, %tmp_12_16
  store float %tmp_14_16, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 17), align 4
  %C_load_18 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10118), align 8
  %Q_load_18 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10118), align 8
  %tmp_11_17 = fadd float %C_load_18, %Q_load_18
  %tmp_12_17 = fmul float %tmp_11_17, %temp_1
  %alpha_load_21 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 18), align 8
  %tmp_14_17 = fsub float %alpha_load_21, %tmp_12_17
  store float %tmp_14_17, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 18), align 8
  %C_load_19 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10119), align 4
  %Q_load_19 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10119), align 4
  %tmp_11_18 = fadd float %C_load_19, %Q_load_19
  %tmp_12_18 = fmul float %tmp_11_18, %temp_1
  %alpha_load_22 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 19), align 4
  %tmp_14_18 = fsub float %alpha_load_22, %tmp_12_18
  store float %tmp_14_18, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 19), align 4
  %C_load_20 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10120), align 16
  %Q_load_20 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10120), align 16
  %tmp_11_19 = fadd float %C_load_20, %Q_load_20
  %tmp_12_19 = fmul float %tmp_11_19, %temp_1
  %alpha_load_23 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 20), align 16
  %tmp_14_19 = fsub float %alpha_load_23, %tmp_12_19
  store float %tmp_14_19, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 20), align 16
  %C_load_21 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10121), align 4
  %Q_load_21 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10121), align 4
  %tmp_11_20 = fadd float %C_load_21, %Q_load_21
  %tmp_12_20 = fmul float %tmp_11_20, %temp_1
  %alpha_load_24 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 21), align 4
  %tmp_14_20 = fsub float %alpha_load_24, %tmp_12_20
  store float %tmp_14_20, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 21), align 4
  %C_load_22 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10122), align 8
  %Q_load_22 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10122), align 8
  %tmp_11_21 = fadd float %C_load_22, %Q_load_22
  %tmp_12_21 = fmul float %tmp_11_21, %temp_1
  %alpha_load_25 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 22), align 8
  %tmp_14_21 = fsub float %alpha_load_25, %tmp_12_21
  store float %tmp_14_21, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 22), align 8
  %C_load_23 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10123), align 4
  %Q_load_23 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10123), align 4
  %tmp_11_22 = fadd float %C_load_23, %Q_load_23
  %tmp_12_22 = fmul float %tmp_11_22, %temp_1
  %alpha_load_26 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 23), align 4
  %tmp_14_22 = fsub float %alpha_load_26, %tmp_12_22
  store float %tmp_14_22, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 23), align 4
  %C_load_24 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10124), align 16
  %Q_load_24 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10124), align 16
  %tmp_11_23 = fadd float %C_load_24, %Q_load_24
  %tmp_12_23 = fmul float %tmp_11_23, %temp_1
  %alpha_load_27 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 24), align 16
  %tmp_14_23 = fsub float %alpha_load_27, %tmp_12_23
  store float %tmp_14_23, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 24), align 16
  %C_load_25 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10125), align 4
  %Q_load_25 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10125), align 4
  %tmp_11_24 = fadd float %C_load_25, %Q_load_25
  %tmp_12_24 = fmul float %tmp_11_24, %temp_1
  %alpha_load_28 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 25), align 4
  %tmp_14_24 = fsub float %alpha_load_28, %tmp_12_24
  store float %tmp_14_24, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 25), align 4
  %C_load_26 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10126), align 8
  %Q_load_26 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10126), align 8
  %tmp_11_25 = fadd float %C_load_26, %Q_load_26
  %tmp_12_25 = fmul float %tmp_11_25, %temp_1
  %alpha_load_29 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 26), align 8
  %tmp_14_25 = fsub float %alpha_load_29, %tmp_12_25
  store float %tmp_14_25, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 26), align 8
  %C_load_27 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10127), align 4
  %Q_load_27 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10127), align 4
  %tmp_11_26 = fadd float %C_load_27, %Q_load_27
  %tmp_12_26 = fmul float %tmp_11_26, %temp_1
  %alpha_load_30 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 27), align 4
  %tmp_14_26 = fsub float %alpha_load_30, %tmp_12_26
  store float %tmp_14_26, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 27), align 4
  %C_load_28 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10128), align 16
  %Q_load_28 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10128), align 16
  %tmp_11_27 = fadd float %C_load_28, %Q_load_28
  %tmp_12_27 = fmul float %tmp_11_27, %temp_1
  %alpha_load_31 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 28), align 16
  %tmp_14_27 = fsub float %alpha_load_31, %tmp_12_27
  store float %tmp_14_27, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 28), align 16
  %C_load_29 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10129), align 4
  %Q_load_29 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10129), align 4
  %tmp_11_28 = fadd float %C_load_29, %Q_load_29
  %tmp_12_28 = fmul float %tmp_11_28, %temp_1
  %alpha_load_32 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 29), align 4
  %tmp_14_28 = fsub float %alpha_load_32, %tmp_12_28
  store float %tmp_14_28, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 29), align 4
  %C_load_30 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10130), align 8
  %Q_load_30 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10130), align 8
  %tmp_11_29 = fadd float %C_load_30, %Q_load_30
  %tmp_12_29 = fmul float %tmp_11_29, %temp_1
  %alpha_load_33 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 30), align 8
  %tmp_14_29 = fsub float %alpha_load_33, %tmp_12_29
  store float %tmp_14_29, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 30), align 8
  %C_load_31 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10131), align 4
  %Q_load_31 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10131), align 4
  %tmp_11_30 = fadd float %C_load_31, %Q_load_31
  %tmp_12_30 = fmul float %tmp_11_30, %temp_1
  %alpha_load_34 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 31), align 4
  %tmp_14_30 = fsub float %alpha_load_34, %tmp_12_30
  store float %tmp_14_30, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 31), align 4
  %C_load_32 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10132), align 16
  %Q_load_32 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10132), align 16
  %tmp_11_31 = fadd float %C_load_32, %Q_load_32
  %tmp_12_31 = fmul float %tmp_11_31, %temp_1
  %alpha_load_35 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 32), align 16
  %tmp_14_31 = fsub float %alpha_load_35, %tmp_12_31
  store float %tmp_14_31, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 32), align 16
  %C_load_33 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10133), align 4
  %Q_load_33 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10133), align 4
  %tmp_11_32 = fadd float %C_load_33, %Q_load_33
  %tmp_12_32 = fmul float %tmp_11_32, %temp_1
  %alpha_load_36 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 33), align 4
  %tmp_14_32 = fsub float %alpha_load_36, %tmp_12_32
  store float %tmp_14_32, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 33), align 4
  %C_load_34 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10134), align 8
  %Q_load_34 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10134), align 8
  %tmp_11_33 = fadd float %C_load_34, %Q_load_34
  %tmp_12_33 = fmul float %tmp_11_33, %temp_1
  %alpha_load_37 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 34), align 8
  %tmp_14_33 = fsub float %alpha_load_37, %tmp_12_33
  store float %tmp_14_33, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 34), align 8
  %C_load_35 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10135), align 4
  %Q_load_35 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10135), align 4
  %tmp_11_34 = fadd float %C_load_35, %Q_load_35
  %tmp_12_34 = fmul float %tmp_11_34, %temp_1
  %alpha_load_38 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 35), align 4
  %tmp_14_34 = fsub float %alpha_load_38, %tmp_12_34
  store float %tmp_14_34, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 35), align 4
  %C_load_36 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10136), align 16
  %Q_load_36 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10136), align 16
  %tmp_11_35 = fadd float %C_load_36, %Q_load_36
  %tmp_12_35 = fmul float %tmp_11_35, %temp_1
  %alpha_load_39 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 36), align 16
  %tmp_14_35 = fsub float %alpha_load_39, %tmp_12_35
  store float %tmp_14_35, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 36), align 16
  %C_load_37 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10137), align 4
  %Q_load_37 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10137), align 4
  %tmp_11_36 = fadd float %C_load_37, %Q_load_37
  %tmp_12_36 = fmul float %tmp_11_36, %temp_1
  %alpha_load_40 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 37), align 4
  %tmp_14_36 = fsub float %alpha_load_40, %tmp_12_36
  store float %tmp_14_36, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 37), align 4
  %C_load_38 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10138), align 8
  %Q_load_38 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10138), align 8
  %tmp_11_37 = fadd float %C_load_38, %Q_load_38
  %tmp_12_37 = fmul float %tmp_11_37, %temp_1
  %alpha_load_41 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 38), align 8
  %tmp_14_37 = fsub float %alpha_load_41, %tmp_12_37
  store float %tmp_14_37, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 38), align 8
  %C_load_39 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10139), align 4
  %Q_load_39 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10139), align 4
  %tmp_11_38 = fadd float %C_load_39, %Q_load_39
  %tmp_12_38 = fmul float %tmp_11_38, %temp_1
  %alpha_load_42 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 39), align 4
  %tmp_14_38 = fsub float %alpha_load_42, %tmp_12_38
  store float %tmp_14_38, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 39), align 4
  %C_load_40 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10140), align 16
  %Q_load_40 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10140), align 16
  %tmp_11_39 = fadd float %C_load_40, %Q_load_40
  %tmp_12_39 = fmul float %tmp_11_39, %temp_1
  %alpha_load_43 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 40), align 16
  %tmp_14_39 = fsub float %alpha_load_43, %tmp_12_39
  store float %tmp_14_39, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 40), align 16
  %C_load_41 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10141), align 4
  %Q_load_41 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10141), align 4
  %tmp_11_40 = fadd float %C_load_41, %Q_load_41
  %tmp_12_40 = fmul float %tmp_11_40, %temp_1
  %alpha_load_44 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 41), align 4
  %tmp_14_40 = fsub float %alpha_load_44, %tmp_12_40
  store float %tmp_14_40, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 41), align 4
  %C_load_42 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10142), align 8
  %Q_load_42 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10142), align 8
  %tmp_11_41 = fadd float %C_load_42, %Q_load_42
  %tmp_12_41 = fmul float %tmp_11_41, %temp_1
  %alpha_load_45 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 42), align 8
  %tmp_14_41 = fsub float %alpha_load_45, %tmp_12_41
  store float %tmp_14_41, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 42), align 8
  %C_load_43 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10143), align 4
  %Q_load_43 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10143), align 4
  %tmp_11_42 = fadd float %C_load_43, %Q_load_43
  %tmp_12_42 = fmul float %tmp_11_42, %temp_1
  %alpha_load_46 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 43), align 4
  %tmp_14_42 = fsub float %alpha_load_46, %tmp_12_42
  store float %tmp_14_42, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 43), align 4
  %C_load_44 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10144), align 16
  %Q_load_44 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10144), align 16
  %tmp_11_43 = fadd float %C_load_44, %Q_load_44
  %tmp_12_43 = fmul float %tmp_11_43, %temp_1
  %alpha_load_47 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 44), align 16
  %tmp_14_43 = fsub float %alpha_load_47, %tmp_12_43
  store float %tmp_14_43, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 44), align 16
  %C_load_45 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10145), align 4
  %Q_load_45 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10145), align 4
  %tmp_11_44 = fadd float %C_load_45, %Q_load_45
  %tmp_12_44 = fmul float %tmp_11_44, %temp_1
  %alpha_load_48 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 45), align 4
  %tmp_14_44 = fsub float %alpha_load_48, %tmp_12_44
  store float %tmp_14_44, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 45), align 4
  %C_load_46 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10146), align 8
  %Q_load_46 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10146), align 8
  %tmp_11_45 = fadd float %C_load_46, %Q_load_46
  %tmp_12_45 = fmul float %tmp_11_45, %temp_1
  %alpha_load_49 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 46), align 8
  %tmp_14_45 = fsub float %alpha_load_49, %tmp_12_45
  store float %tmp_14_45, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 46), align 8
  %C_load_47 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10147), align 4
  %Q_load_47 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10147), align 4
  %tmp_11_46 = fadd float %C_load_47, %Q_load_47
  %tmp_12_46 = fmul float %tmp_11_46, %temp_1
  %alpha_load_50 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 47), align 4
  %tmp_14_46 = fsub float %alpha_load_50, %tmp_12_46
  store float %tmp_14_46, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 47), align 4
  %C_load_48 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10148), align 16
  %Q_load_48 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10148), align 16
  %tmp_11_47 = fadd float %C_load_48, %Q_load_48
  %tmp_12_47 = fmul float %tmp_11_47, %temp_1
  %alpha_load_51 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 48), align 16
  %tmp_14_47 = fsub float %alpha_load_51, %tmp_12_47
  store float %tmp_14_47, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 48), align 16
  %C_load_49 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10149), align 4
  %Q_load_49 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10149), align 4
  %tmp_11_48 = fadd float %C_load_49, %Q_load_49
  %tmp_12_48 = fmul float %tmp_11_48, %temp_1
  %alpha_load_52 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 49), align 4
  %tmp_14_48 = fsub float %alpha_load_52, %tmp_12_48
  store float %tmp_14_48, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 49), align 4
  %C_load_50 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10150), align 8
  %Q_load_50 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10150), align 8
  %tmp_11_49 = fadd float %C_load_50, %Q_load_50
  %tmp_12_49 = fmul float %tmp_11_49, %temp_1
  %alpha_load_53 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 50), align 8
  %tmp_14_49 = fsub float %alpha_load_53, %tmp_12_49
  store float %tmp_14_49, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 50), align 8
  %C_load_51 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10151), align 4
  %Q_load_51 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10151), align 4
  %tmp_11_50 = fadd float %C_load_51, %Q_load_51
  %tmp_12_50 = fmul float %tmp_11_50, %temp_1
  %alpha_load_54 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 51), align 4
  %tmp_14_50 = fsub float %alpha_load_54, %tmp_12_50
  store float %tmp_14_50, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 51), align 4
  %C_load_52 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10152), align 16
  %Q_load_52 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10152), align 16
  %tmp_11_51 = fadd float %C_load_52, %Q_load_52
  %tmp_12_51 = fmul float %tmp_11_51, %temp_1
  %alpha_load_55 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 52), align 16
  %tmp_14_51 = fsub float %alpha_load_55, %tmp_12_51
  store float %tmp_14_51, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 52), align 16
  %C_load_53 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10153), align 4
  %Q_load_53 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10153), align 4
  %tmp_11_52 = fadd float %C_load_53, %Q_load_53
  %tmp_12_52 = fmul float %tmp_11_52, %temp_1
  %alpha_load_56 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 53), align 4
  %tmp_14_52 = fsub float %alpha_load_56, %tmp_12_52
  store float %tmp_14_52, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 53), align 4
  %C_load_54 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10154), align 8
  %Q_load_54 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10154), align 8
  %tmp_11_53 = fadd float %C_load_54, %Q_load_54
  %tmp_12_53 = fmul float %tmp_11_53, %temp_1
  %alpha_load_57 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 54), align 8
  %tmp_14_53 = fsub float %alpha_load_57, %tmp_12_53
  store float %tmp_14_53, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 54), align 8
  %C_load_55 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10155), align 4
  %Q_load_55 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10155), align 4
  %tmp_11_54 = fadd float %C_load_55, %Q_load_55
  %tmp_12_54 = fmul float %tmp_11_54, %temp_1
  %alpha_load_58 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 55), align 4
  %tmp_14_54 = fsub float %alpha_load_58, %tmp_12_54
  store float %tmp_14_54, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 55), align 4
  %C_load_56 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10156), align 16
  %Q_load_56 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10156), align 16
  %tmp_11_55 = fadd float %C_load_56, %Q_load_56
  %tmp_12_55 = fmul float %tmp_11_55, %temp_1
  %alpha_load_59 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 56), align 16
  %tmp_14_55 = fsub float %alpha_load_59, %tmp_12_55
  store float %tmp_14_55, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 56), align 16
  %C_load_57 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10157), align 4
  %Q_load_57 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10157), align 4
  %tmp_11_56 = fadd float %C_load_57, %Q_load_57
  %tmp_12_56 = fmul float %tmp_11_56, %temp_1
  %alpha_load_60 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 57), align 4
  %tmp_14_56 = fsub float %alpha_load_60, %tmp_12_56
  store float %tmp_14_56, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 57), align 4
  %C_load_58 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10158), align 8
  %Q_load_58 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10158), align 8
  %tmp_11_57 = fadd float %C_load_58, %Q_load_58
  %tmp_12_57 = fmul float %tmp_11_57, %temp_1
  %alpha_load_61 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 58), align 8
  %tmp_14_57 = fsub float %alpha_load_61, %tmp_12_57
  store float %tmp_14_57, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 58), align 8
  %C_load_59 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10159), align 4
  %Q_load_59 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10159), align 4
  %tmp_11_58 = fadd float %C_load_59, %Q_load_59
  %tmp_12_58 = fmul float %tmp_11_58, %temp_1
  %alpha_load_62 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 59), align 4
  %tmp_14_58 = fsub float %alpha_load_62, %tmp_12_58
  store float %tmp_14_58, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 59), align 4
  %C_load_60 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10160), align 16
  %Q_load_60 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10160), align 16
  %tmp_11_59 = fadd float %C_load_60, %Q_load_60
  %tmp_12_59 = fmul float %tmp_11_59, %temp_1
  %alpha_load_63 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 60), align 16
  %tmp_14_59 = fsub float %alpha_load_63, %tmp_12_59
  store float %tmp_14_59, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 60), align 16
  %C_load_61 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10161), align 4
  %Q_load_61 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10161), align 4
  %tmp_11_60 = fadd float %C_load_61, %Q_load_61
  %tmp_12_60 = fmul float %tmp_11_60, %temp_1
  %alpha_load_64 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 61), align 4
  %tmp_14_60 = fsub float %alpha_load_64, %tmp_12_60
  store float %tmp_14_60, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 61), align 4
  %C_load_62 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10162), align 8
  %Q_load_62 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10162), align 8
  %tmp_11_61 = fadd float %C_load_62, %Q_load_62
  %tmp_12_61 = fmul float %tmp_11_61, %temp_1
  %alpha_load_65 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 62), align 8
  %tmp_14_61 = fsub float %alpha_load_65, %tmp_12_61
  store float %tmp_14_61, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 62), align 8
  %C_load_63 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10163), align 4
  %Q_load_63 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10163), align 4
  %tmp_11_62 = fadd float %C_load_63, %Q_load_63
  %tmp_12_62 = fmul float %tmp_11_62, %temp_1
  %alpha_load_66 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 63), align 4
  %tmp_14_62 = fsub float %alpha_load_66, %tmp_12_62
  store float %tmp_14_62, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 63), align 4
  %C_load_64 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10164), align 16
  %Q_load_64 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10164), align 16
  %tmp_11_63 = fadd float %C_load_64, %Q_load_64
  %tmp_12_63 = fmul float %tmp_11_63, %temp_1
  %alpha_load_67 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 64), align 16
  %tmp_14_63 = fsub float %alpha_load_67, %tmp_12_63
  store float %tmp_14_63, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 64), align 16
  %C_load_65 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10165), align 4
  %Q_load_65 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10165), align 4
  %tmp_11_64 = fadd float %C_load_65, %Q_load_65
  %tmp_12_64 = fmul float %tmp_11_64, %temp_1
  %alpha_load_68 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 65), align 4
  %tmp_14_64 = fsub float %alpha_load_68, %tmp_12_64
  store float %tmp_14_64, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 65), align 4
  %C_load_66 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10166), align 8
  %Q_load_66 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10166), align 8
  %tmp_11_65 = fadd float %C_load_66, %Q_load_66
  %tmp_12_65 = fmul float %tmp_11_65, %temp_1
  %alpha_load_69 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 66), align 8
  %tmp_14_65 = fsub float %alpha_load_69, %tmp_12_65
  store float %tmp_14_65, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 66), align 8
  %C_load_67 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10167), align 4
  %Q_load_67 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10167), align 4
  %tmp_11_66 = fadd float %C_load_67, %Q_load_67
  %tmp_12_66 = fmul float %tmp_11_66, %temp_1
  %alpha_load_70 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 67), align 4
  %tmp_14_66 = fsub float %alpha_load_70, %tmp_12_66
  store float %tmp_14_66, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 67), align 4
  %C_load_68 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10168), align 16
  %Q_load_68 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10168), align 16
  %tmp_11_67 = fadd float %C_load_68, %Q_load_68
  %tmp_12_67 = fmul float %tmp_11_67, %temp_1
  %alpha_load_71 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 68), align 16
  %tmp_14_67 = fsub float %alpha_load_71, %tmp_12_67
  store float %tmp_14_67, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 68), align 16
  %C_load_69 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10169), align 4
  %Q_load_69 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10169), align 4
  %tmp_11_68 = fadd float %C_load_69, %Q_load_69
  %tmp_12_68 = fmul float %tmp_11_68, %temp_1
  %alpha_load_72 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 69), align 4
  %tmp_14_68 = fsub float %alpha_load_72, %tmp_12_68
  store float %tmp_14_68, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 69), align 4
  %C_load_70 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10170), align 8
  %Q_load_70 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10170), align 8
  %tmp_11_69 = fadd float %C_load_70, %Q_load_70
  %tmp_12_69 = fmul float %tmp_11_69, %temp_1
  %alpha_load_73 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 70), align 8
  %tmp_14_69 = fsub float %alpha_load_73, %tmp_12_69
  store float %tmp_14_69, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 70), align 8
  %C_load_71 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10171), align 4
  %Q_load_71 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10171), align 4
  %tmp_11_70 = fadd float %C_load_71, %Q_load_71
  %tmp_12_70 = fmul float %tmp_11_70, %temp_1
  %alpha_load_74 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 71), align 4
  %tmp_14_70 = fsub float %alpha_load_74, %tmp_12_70
  store float %tmp_14_70, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 71), align 4
  %C_load_72 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10172), align 16
  %Q_load_72 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10172), align 16
  %tmp_11_71 = fadd float %C_load_72, %Q_load_72
  %tmp_12_71 = fmul float %tmp_11_71, %temp_1
  %alpha_load_75 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 72), align 16
  %tmp_14_71 = fsub float %alpha_load_75, %tmp_12_71
  store float %tmp_14_71, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 72), align 16
  %C_load_73 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10173), align 4
  %Q_load_73 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10173), align 4
  %tmp_11_72 = fadd float %C_load_73, %Q_load_73
  %tmp_12_72 = fmul float %tmp_11_72, %temp_1
  %alpha_load_76 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 73), align 4
  %tmp_14_72 = fsub float %alpha_load_76, %tmp_12_72
  store float %tmp_14_72, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 73), align 4
  %C_load_74 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10174), align 8
  %Q_load_74 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10174), align 8
  %tmp_11_73 = fadd float %C_load_74, %Q_load_74
  %tmp_12_73 = fmul float %tmp_11_73, %temp_1
  %alpha_load_77 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 74), align 8
  %tmp_14_73 = fsub float %alpha_load_77, %tmp_12_73
  store float %tmp_14_73, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 74), align 8
  %C_load_75 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10175), align 4
  %Q_load_75 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10175), align 4
  %tmp_11_74 = fadd float %C_load_75, %Q_load_75
  %tmp_12_74 = fmul float %tmp_11_74, %temp_1
  %alpha_load_78 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 75), align 4
  %tmp_14_74 = fsub float %alpha_load_78, %tmp_12_74
  store float %tmp_14_74, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 75), align 4
  %C_load_76 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10176), align 16
  %Q_load_76 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10176), align 16
  %tmp_11_75 = fadd float %C_load_76, %Q_load_76
  %tmp_12_75 = fmul float %tmp_11_75, %temp_1
  %alpha_load_79 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 76), align 16
  %tmp_14_75 = fsub float %alpha_load_79, %tmp_12_75
  store float %tmp_14_75, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 76), align 16
  %C_load_77 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10177), align 4
  %Q_load_77 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10177), align 4
  %tmp_11_76 = fadd float %C_load_77, %Q_load_77
  %tmp_12_76 = fmul float %tmp_11_76, %temp_1
  %alpha_load_80 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 77), align 4
  %tmp_14_76 = fsub float %alpha_load_80, %tmp_12_76
  store float %tmp_14_76, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 77), align 4
  %C_load_78 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10178), align 8
  %Q_load_78 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10178), align 8
  %tmp_11_77 = fadd float %C_load_78, %Q_load_78
  %tmp_12_77 = fmul float %tmp_11_77, %temp_1
  %alpha_load_81 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 78), align 8
  %tmp_14_77 = fsub float %alpha_load_81, %tmp_12_77
  store float %tmp_14_77, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 78), align 8
  %C_load_79 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10179), align 4
  %Q_load_79 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10179), align 4
  %tmp_11_78 = fadd float %C_load_79, %Q_load_79
  %tmp_12_78 = fmul float %tmp_11_78, %temp_1
  %alpha_load_82 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 79), align 4
  %tmp_14_78 = fsub float %alpha_load_82, %tmp_12_78
  store float %tmp_14_78, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 79), align 4
  %C_load_80 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10180), align 16
  %Q_load_80 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10180), align 16
  %tmp_11_79 = fadd float %C_load_80, %Q_load_80
  %tmp_12_79 = fmul float %tmp_11_79, %temp_1
  %alpha_load_83 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 80), align 16
  %tmp_14_79 = fsub float %alpha_load_83, %tmp_12_79
  store float %tmp_14_79, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 80), align 16
  %C_load_81 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10181), align 4
  %Q_load_81 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10181), align 4
  %tmp_11_80 = fadd float %C_load_81, %Q_load_81
  %tmp_12_80 = fmul float %tmp_11_80, %temp_1
  %alpha_load_84 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 81), align 4
  %tmp_14_80 = fsub float %alpha_load_84, %tmp_12_80
  store float %tmp_14_80, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 81), align 4
  %C_load_82 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10182), align 8
  %Q_load_82 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10182), align 8
  %tmp_11_81 = fadd float %C_load_82, %Q_load_82
  %tmp_12_81 = fmul float %tmp_11_81, %temp_1
  %alpha_load_85 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 82), align 8
  %tmp_14_81 = fsub float %alpha_load_85, %tmp_12_81
  store float %tmp_14_81, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 82), align 8
  %C_load_83 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10183), align 4
  %Q_load_83 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10183), align 4
  %tmp_11_82 = fadd float %C_load_83, %Q_load_83
  %tmp_12_82 = fmul float %tmp_11_82, %temp_1
  %alpha_load_86 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 83), align 4
  %tmp_14_82 = fsub float %alpha_load_86, %tmp_12_82
  store float %tmp_14_82, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 83), align 4
  %C_load_84 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10184), align 16
  %Q_load_84 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10184), align 16
  %tmp_11_83 = fadd float %C_load_84, %Q_load_84
  %tmp_12_83 = fmul float %tmp_11_83, %temp_1
  %alpha_load_87 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 84), align 16
  %tmp_14_83 = fsub float %alpha_load_87, %tmp_12_83
  store float %tmp_14_83, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 84), align 16
  %C_load_85 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10185), align 4
  %Q_load_85 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10185), align 4
  %tmp_11_84 = fadd float %C_load_85, %Q_load_85
  %tmp_12_84 = fmul float %tmp_11_84, %temp_1
  %alpha_load_88 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 85), align 4
  %tmp_14_84 = fsub float %alpha_load_88, %tmp_12_84
  store float %tmp_14_84, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 85), align 4
  %C_load_86 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10186), align 8
  %Q_load_86 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10186), align 8
  %tmp_11_85 = fadd float %C_load_86, %Q_load_86
  %tmp_12_85 = fmul float %tmp_11_85, %temp_1
  %alpha_load_89 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 86), align 8
  %tmp_14_85 = fsub float %alpha_load_89, %tmp_12_85
  store float %tmp_14_85, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 86), align 8
  %C_load_87 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10187), align 4
  %Q_load_87 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10187), align 4
  %tmp_11_86 = fadd float %C_load_87, %Q_load_87
  %tmp_12_86 = fmul float %tmp_11_86, %temp_1
  %alpha_load_90 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 87), align 4
  %tmp_14_86 = fsub float %alpha_load_90, %tmp_12_86
  store float %tmp_14_86, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 87), align 4
  %C_load_88 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10188), align 16
  %Q_load_88 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10188), align 16
  %tmp_11_87 = fadd float %C_load_88, %Q_load_88
  %tmp_12_87 = fmul float %tmp_11_87, %temp_1
  %alpha_load_91 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 88), align 16
  %tmp_14_87 = fsub float %alpha_load_91, %tmp_12_87
  store float %tmp_14_87, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 88), align 16
  %C_load_89 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10189), align 4
  %Q_load_89 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10189), align 4
  %tmp_11_88 = fadd float %C_load_89, %Q_load_89
  %tmp_12_88 = fmul float %tmp_11_88, %temp_1
  %alpha_load_92 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 89), align 4
  %tmp_14_88 = fsub float %alpha_load_92, %tmp_12_88
  store float %tmp_14_88, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 89), align 4
  %C_load_90 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10190), align 8
  %Q_load_90 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10190), align 8
  %tmp_11_89 = fadd float %C_load_90, %Q_load_90
  %tmp_12_89 = fmul float %tmp_11_89, %temp_1
  %alpha_load_93 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 90), align 8
  %tmp_14_89 = fsub float %alpha_load_93, %tmp_12_89
  store float %tmp_14_89, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 90), align 8
  %C_load_91 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10191), align 4
  %Q_load_91 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10191), align 4
  %tmp_11_90 = fadd float %C_load_91, %Q_load_91
  %tmp_12_90 = fmul float %tmp_11_90, %temp_1
  %alpha_load_94 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 91), align 4
  %tmp_14_90 = fsub float %alpha_load_94, %tmp_12_90
  store float %tmp_14_90, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 91), align 4
  %C_load_92 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10192), align 16
  %Q_load_92 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10192), align 16
  %tmp_11_91 = fadd float %C_load_92, %Q_load_92
  %tmp_12_91 = fmul float %tmp_11_91, %temp_1
  %alpha_load_95 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 92), align 16
  %tmp_14_91 = fsub float %alpha_load_95, %tmp_12_91
  store float %tmp_14_91, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 92), align 16
  %C_load_93 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10193), align 4
  %Q_load_93 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10193), align 4
  %tmp_11_92 = fadd float %C_load_93, %Q_load_93
  %tmp_12_92 = fmul float %tmp_11_92, %temp_1
  %alpha_load_96 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 93), align 4
  %tmp_14_92 = fsub float %alpha_load_96, %tmp_12_92
  store float %tmp_14_92, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 93), align 4
  %C_load_94 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10194), align 8
  %Q_load_94 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10194), align 8
  %tmp_11_93 = fadd float %C_load_94, %Q_load_94
  %tmp_12_93 = fmul float %tmp_11_93, %temp_1
  %alpha_load_97 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 94), align 8
  %tmp_14_93 = fsub float %alpha_load_97, %tmp_12_93
  store float %tmp_14_93, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 94), align 8
  %C_load_95 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10195), align 4
  %Q_load_95 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10195), align 4
  %tmp_11_94 = fadd float %C_load_95, %Q_load_95
  %tmp_12_94 = fmul float %tmp_11_94, %temp_1
  %alpha_load_98 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 95), align 4
  %tmp_14_94 = fsub float %alpha_load_98, %tmp_12_94
  store float %tmp_14_94, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 95), align 4
  %C_load_96 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10196), align 16
  %Q_load_96 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10196), align 16
  %tmp_11_95 = fadd float %C_load_96, %Q_load_96
  %tmp_12_95 = fmul float %tmp_11_95, %temp_1
  %alpha_load_99 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 96), align 16
  %tmp_14_95 = fsub float %alpha_load_99, %tmp_12_95
  store float %tmp_14_95, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 96), align 16
  %C_load_97 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10197), align 4
  %Q_load_97 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10197), align 4
  %tmp_11_96 = fadd float %C_load_97, %Q_load_97
  %tmp_12_96 = fmul float %tmp_11_96, %temp_1
  %alpha_load_100 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 97), align 4
  %tmp_14_96 = fsub float %alpha_load_100, %tmp_12_96
  store float %tmp_14_96, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 97), align 4
  %C_load_98 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10198), align 8
  %Q_load_98 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10198), align 8
  %tmp_11_97 = fadd float %C_load_98, %Q_load_98
  %tmp_12_97 = fmul float %tmp_11_97, %temp_1
  %alpha_load_101 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 98), align 8
  %tmp_14_97 = fsub float %alpha_load_101, %tmp_12_97
  store float %tmp_14_97, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 98), align 8
  %C_load_99 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10199), align 4
  %Q_load_99 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10199), align 4
  %tmp_11_98 = fadd float %C_load_99, %Q_load_99
  %tmp_12_98 = fmul float %tmp_11_98, %temp_1
  %alpha_load_102 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 99), align 4
  %tmp_14_98 = fsub float %alpha_load_102, %tmp_12_98
  store float %tmp_14_98, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 99), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16
  br label %4

; <label>:4                                       ; preds = %5, %3
  %i2 = phi i7 [ 0, %3 ], [ %i_2, %5 ]
  %phi_mul = phi i14 [ 0, %3 ], [ %next_mul, %5 ]
  %exitcond2 = icmp eq i7 %i2, -28
  %i_2 = add i7 %i2, 1
  br i1 %exitcond2, label %.preheader, label %5

; <label>:5                                       ; preds = %4
  %i2_cast4 = zext i7 %i2 to i14
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 100, i64 100, i64 100)
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @p_str4) nounwind
  %tmp_10 = call i32 (...)* @_ssdm_op_SpecRegionBegin([18 x i8]* @p_str4) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %a = add i14 %i2_cast4, -6284
  %next_mul = add i14 %phi_mul, 101
  %tmp_11 = zext i14 %a to i64
  %Q_addr = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_11
  %C_addr = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_11
  %Q_load_100 = load float* %Q_addr, align 4
  %Q_load_101 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10100), align 16
  %tmp_12 = fmul float %Q_load_100, %Q_load_101
  %temp_4 = fdiv float %tmp_12, %qStar
  %C_load_100 = load float* %C_addr, align 4
  %C_load_101 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10100), align 16
  %tmp_13 = fmul float %C_load_100, %C_load_101
  %tmp_14 = fmul float %C_load_100, %Q_load_101
  %tmp_15 = fadd float %tmp_13, %tmp_14
  %tmp_16 = fmul float %Q_load_100, %C_load_101
  %tmp_17 = fadd float %tmp_15, %tmp_16
  %tmp_20 = fadd float %tmp_17, %tmp_12
  %tmp_21 = fdiv float %tmp_20, %tmp_5
  %tmp_22 = fsub float %temp_4, %tmp_21
  %tmp_23 = zext i14 %phi_mul to i64
  %C_addr_1 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_23
  %C_load_102 = load float* %C_addr_1, align 4
  %tmp_24 = fadd float %C_load_102, %tmp_22
  store float %tmp_24, float* %C_addr_1, align 4
  %Q_addr_1 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_23
  %Q_load_102 = load float* %Q_addr_1, align 4
  %tmp_25 = fsub float %Q_load_102, %temp_4
  store float %tmp_25, float* %Q_addr_1, align 4
  %c_1 = add i14 %phi_mul, 1
  %Q_load_103 = load float* %Q_addr, align 4
  %Q_load_104 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10101), align 4
  %tmp_21_1 = fmul float %Q_load_103, %Q_load_104
  %temp_4_1 = fdiv float %tmp_21_1, %qStar
  %C_load_103 = load float* %C_addr, align 4
  %C_load_104 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10101), align 4
  %tmp_22_1 = fmul float %C_load_103, %C_load_104
  %tmp_23_1 = fmul float %C_load_103, %Q_load_104
  %tmp_24_1 = fadd float %tmp_22_1, %tmp_23_1
  %tmp_25_1 = fmul float %Q_load_103, %C_load_104
  %tmp_26_1 = fadd float %tmp_24_1, %tmp_25_1
  %tmp_27_1 = fadd float %tmp_26_1, %tmp_21_1
  %tmp_28_1 = fdiv float %tmp_27_1, %tmp_5
  %tmp_29_1 = fsub float %temp_4_1, %tmp_28_1
  %tmp_30_1 = zext i14 %c_1 to i64
  %C_addr_4 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_1
  %C_load_105 = load float* %C_addr_4, align 4
  %tmp_31_1 = fadd float %C_load_105, %tmp_29_1
  store float %tmp_31_1, float* %C_addr_4, align 4
  %Q_addr_4 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_1
  %Q_load_105 = load float* %Q_addr_4, align 4
  %tmp_32_1 = fsub float %Q_load_105, %temp_4_1
  store float %tmp_32_1, float* %Q_addr_4, align 4
  %c_2 = add i14 %phi_mul, 2
  %Q_load_106 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10102), align 8
  %tmp_21_2 = fmul float %Q_load_103, %Q_load_106
  %temp_4_2 = fdiv float %tmp_21_2, %qStar
  %C_load_106 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10102), align 8
  %tmp_22_2 = fmul float %C_load_103, %C_load_106
  %tmp_23_2 = fmul float %C_load_103, %Q_load_106
  %tmp_24_2 = fadd float %tmp_22_2, %tmp_23_2
  %tmp_25_2 = fmul float %Q_load_103, %C_load_106
  %tmp_26_2 = fadd float %tmp_24_2, %tmp_25_2
  %tmp_27_2 = fadd float %tmp_26_2, %tmp_21_2
  %tmp_28_2 = fdiv float %tmp_27_2, %tmp_5
  %tmp_29_2 = fsub float %temp_4_2, %tmp_28_2
  %tmp_30_2 = zext i14 %c_2 to i64
  %C_addr_5 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_2
  %C_load_107 = load float* %C_addr_5, align 4
  %tmp_31_2 = fadd float %C_load_107, %tmp_29_2
  store float %tmp_31_2, float* %C_addr_5, align 4
  %Q_addr_5 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_2
  %Q_load_107 = load float* %Q_addr_5, align 4
  %tmp_32_2 = fsub float %Q_load_107, %temp_4_2
  store float %tmp_32_2, float* %Q_addr_5, align 4
  %c_3 = add i14 %phi_mul, 3
  %Q_load_108 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10103), align 4
  %tmp_21_3 = fmul float %Q_load_103, %Q_load_108
  %temp_4_3 = fdiv float %tmp_21_3, %qStar
  %C_load_108 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10103), align 4
  %tmp_22_3 = fmul float %C_load_103, %C_load_108
  %tmp_23_3 = fmul float %C_load_103, %Q_load_108
  %tmp_24_3 = fadd float %tmp_22_3, %tmp_23_3
  %tmp_25_3 = fmul float %Q_load_103, %C_load_108
  %tmp_26_3 = fadd float %tmp_24_3, %tmp_25_3
  %tmp_27_3 = fadd float %tmp_26_3, %tmp_21_3
  %tmp_28_3 = fdiv float %tmp_27_3, %tmp_5
  %tmp_29_3 = fsub float %temp_4_3, %tmp_28_3
  %tmp_30_3 = zext i14 %c_3 to i64
  %C_addr_6 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_3
  %C_load_109 = load float* %C_addr_6, align 4
  %tmp_31_3 = fadd float %C_load_109, %tmp_29_3
  store float %tmp_31_3, float* %C_addr_6, align 4
  %Q_addr_6 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_3
  %Q_load_109 = load float* %Q_addr_6, align 4
  %tmp_32_3 = fsub float %Q_load_109, %temp_4_3
  store float %tmp_32_3, float* %Q_addr_6, align 4
  %c_4 = add i14 %phi_mul, 4
  %Q_load_110 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10104), align 16
  %tmp_21_4 = fmul float %Q_load_103, %Q_load_110
  %temp_4_4 = fdiv float %tmp_21_4, %qStar
  %C_load_110 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10104), align 16
  %tmp_22_4 = fmul float %C_load_103, %C_load_110
  %tmp_23_4 = fmul float %C_load_103, %Q_load_110
  %tmp_24_4 = fadd float %tmp_22_4, %tmp_23_4
  %tmp_25_4 = fmul float %Q_load_103, %C_load_110
  %tmp_26_4 = fadd float %tmp_24_4, %tmp_25_4
  %tmp_27_4 = fadd float %tmp_26_4, %tmp_21_4
  %tmp_28_4 = fdiv float %tmp_27_4, %tmp_5
  %tmp_29_4 = fsub float %temp_4_4, %tmp_28_4
  %tmp_30_4 = zext i14 %c_4 to i64
  %C_addr_7 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_4
  %C_load_111 = load float* %C_addr_7, align 4
  %tmp_31_4 = fadd float %C_load_111, %tmp_29_4
  store float %tmp_31_4, float* %C_addr_7, align 4
  %Q_addr_7 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_4
  %Q_load_111 = load float* %Q_addr_7, align 4
  %tmp_32_4 = fsub float %Q_load_111, %temp_4_4
  store float %tmp_32_4, float* %Q_addr_7, align 4
  %c_5 = add i14 %phi_mul, 5
  %Q_load_112 = load float* %Q_addr, align 4
  %Q_load_113 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10105), align 4
  %tmp_21_5 = fmul float %Q_load_112, %Q_load_113
  %temp_4_5 = fdiv float %tmp_21_5, %qStar
  %C_load_112 = load float* %C_addr, align 4
  %C_load_113 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10105), align 4
  %tmp_22_5 = fmul float %C_load_112, %C_load_113
  %tmp_23_5 = fmul float %C_load_112, %Q_load_113
  %tmp_24_5 = fadd float %tmp_22_5, %tmp_23_5
  %tmp_25_5 = fmul float %Q_load_112, %C_load_113
  %tmp_26_5 = fadd float %tmp_24_5, %tmp_25_5
  %tmp_27_5 = fadd float %tmp_26_5, %tmp_21_5
  %tmp_28_5 = fdiv float %tmp_27_5, %tmp_5
  %tmp_29_5 = fsub float %temp_4_5, %tmp_28_5
  %tmp_30_5 = zext i14 %c_5 to i64
  %C_addr_8 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_5
  %C_load_114 = load float* %C_addr_8, align 4
  %tmp_31_5 = fadd float %C_load_114, %tmp_29_5
  store float %tmp_31_5, float* %C_addr_8, align 4
  %Q_addr_8 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_5
  %Q_load_114 = load float* %Q_addr_8, align 4
  %tmp_32_5 = fsub float %Q_load_114, %temp_4_5
  store float %tmp_32_5, float* %Q_addr_8, align 4
  %c_6 = add i14 %phi_mul, 6
  %Q_load_115 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10106), align 8
  %tmp_21_6 = fmul float %Q_load_112, %Q_load_115
  %temp_4_6 = fdiv float %tmp_21_6, %qStar
  %C_load_115 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10106), align 8
  %tmp_22_6 = fmul float %C_load_112, %C_load_115
  %tmp_23_6 = fmul float %C_load_112, %Q_load_115
  %tmp_24_6 = fadd float %tmp_22_6, %tmp_23_6
  %tmp_25_6 = fmul float %Q_load_112, %C_load_115
  %tmp_26_6 = fadd float %tmp_24_6, %tmp_25_6
  %tmp_27_6 = fadd float %tmp_26_6, %tmp_21_6
  %tmp_28_6 = fdiv float %tmp_27_6, %tmp_5
  %tmp_29_6 = fsub float %temp_4_6, %tmp_28_6
  %tmp_30_6 = zext i14 %c_6 to i64
  %C_addr_9 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_6
  %C_load_116 = load float* %C_addr_9, align 4
  %tmp_31_6 = fadd float %C_load_116, %tmp_29_6
  store float %tmp_31_6, float* %C_addr_9, align 4
  %Q_addr_9 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_6
  %Q_load_116 = load float* %Q_addr_9, align 4
  %tmp_32_6 = fsub float %Q_load_116, %temp_4_6
  store float %tmp_32_6, float* %Q_addr_9, align 4
  %c_7 = add i14 %phi_mul, 7
  %Q_load_117 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10107), align 4
  %tmp_21_7 = fmul float %Q_load_112, %Q_load_117
  %temp_4_7 = fdiv float %tmp_21_7, %qStar
  %C_load_117 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10107), align 4
  %tmp_22_7 = fmul float %C_load_112, %C_load_117
  %tmp_23_7 = fmul float %C_load_112, %Q_load_117
  %tmp_24_7 = fadd float %tmp_22_7, %tmp_23_7
  %tmp_25_7 = fmul float %Q_load_112, %C_load_117
  %tmp_26_7 = fadd float %tmp_24_7, %tmp_25_7
  %tmp_27_7 = fadd float %tmp_26_7, %tmp_21_7
  %tmp_28_7 = fdiv float %tmp_27_7, %tmp_5
  %tmp_29_7 = fsub float %temp_4_7, %tmp_28_7
  %tmp_30_7 = zext i14 %c_7 to i64
  %C_addr_10 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_7
  %C_load_118 = load float* %C_addr_10, align 4
  %tmp_31_7 = fadd float %C_load_118, %tmp_29_7
  store float %tmp_31_7, float* %C_addr_10, align 4
  %Q_addr_10 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_7
  %Q_load_118 = load float* %Q_addr_10, align 4
  %tmp_32_7 = fsub float %Q_load_118, %temp_4_7
  store float %tmp_32_7, float* %Q_addr_10, align 4
  %c_8 = add i14 %phi_mul, 8
  %Q_load_119 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10108), align 16
  %tmp_21_8 = fmul float %Q_load_112, %Q_load_119
  %temp_4_8 = fdiv float %tmp_21_8, %qStar
  %C_load_119 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10108), align 16
  %tmp_22_8 = fmul float %C_load_112, %C_load_119
  %tmp_23_8 = fmul float %C_load_112, %Q_load_119
  %tmp_24_8 = fadd float %tmp_22_8, %tmp_23_8
  %tmp_25_8 = fmul float %Q_load_112, %C_load_119
  %tmp_26_8 = fadd float %tmp_24_8, %tmp_25_8
  %tmp_27_8 = fadd float %tmp_26_8, %tmp_21_8
  %tmp_28_8 = fdiv float %tmp_27_8, %tmp_5
  %tmp_29_8 = fsub float %temp_4_8, %tmp_28_8
  %tmp_30_8 = zext i14 %c_8 to i64
  %C_addr_11 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_8
  %C_load_120 = load float* %C_addr_11, align 4
  %tmp_31_8 = fadd float %C_load_120, %tmp_29_8
  store float %tmp_31_8, float* %C_addr_11, align 4
  %Q_addr_11 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_8
  %Q_load_120 = load float* %Q_addr_11, align 4
  %tmp_32_8 = fsub float %Q_load_120, %temp_4_8
  store float %tmp_32_8, float* %Q_addr_11, align 4
  %c_9 = add i14 %phi_mul, 9
  %Q_load_121 = load float* %Q_addr, align 4
  %Q_load_122 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10109), align 4
  %tmp_21_9 = fmul float %Q_load_121, %Q_load_122
  %temp_4_9 = fdiv float %tmp_21_9, %qStar
  %C_load_121 = load float* %C_addr, align 4
  %C_load_122 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10109), align 4
  %tmp_22_9 = fmul float %C_load_121, %C_load_122
  %tmp_23_9 = fmul float %C_load_121, %Q_load_122
  %tmp_24_9 = fadd float %tmp_22_9, %tmp_23_9
  %tmp_25_9 = fmul float %Q_load_121, %C_load_122
  %tmp_26_9 = fadd float %tmp_24_9, %tmp_25_9
  %tmp_27_9 = fadd float %tmp_26_9, %tmp_21_9
  %tmp_28_9 = fdiv float %tmp_27_9, %tmp_5
  %tmp_29_9 = fsub float %temp_4_9, %tmp_28_9
  %tmp_30_9 = zext i14 %c_9 to i64
  %C_addr_12 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_9
  %C_load_123 = load float* %C_addr_12, align 4
  %tmp_31_9 = fadd float %C_load_123, %tmp_29_9
  store float %tmp_31_9, float* %C_addr_12, align 4
  %Q_addr_12 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_9
  %Q_load_123 = load float* %Q_addr_12, align 4
  %tmp_32_9 = fsub float %Q_load_123, %temp_4_9
  store float %tmp_32_9, float* %Q_addr_12, align 4
  %c_s = add i14 %phi_mul, 10
  %Q_load_124 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10110), align 8
  %tmp_21_s = fmul float %Q_load_121, %Q_load_124
  %temp_4_s = fdiv float %tmp_21_s, %qStar
  %C_load_124 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10110), align 8
  %tmp_22_s = fmul float %C_load_121, %C_load_124
  %tmp_23_s = fmul float %C_load_121, %Q_load_124
  %tmp_24_s = fadd float %tmp_22_s, %tmp_23_s
  %tmp_25_s = fmul float %Q_load_121, %C_load_124
  %tmp_26_s = fadd float %tmp_24_s, %tmp_25_s
  %tmp_27_s = fadd float %tmp_26_s, %tmp_21_s
  %tmp_28_s = fdiv float %tmp_27_s, %tmp_5
  %tmp_29_s = fsub float %temp_4_s, %tmp_28_s
  %tmp_30_s = zext i14 %c_s to i64
  %C_addr_13 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_s
  %C_load_125 = load float* %C_addr_13, align 4
  %tmp_31_s = fadd float %C_load_125, %tmp_29_s
  store float %tmp_31_s, float* %C_addr_13, align 4
  %Q_addr_13 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_s
  %Q_load_125 = load float* %Q_addr_13, align 4
  %tmp_32_s = fsub float %Q_load_125, %temp_4_s
  store float %tmp_32_s, float* %Q_addr_13, align 4
  %c_10 = add i14 %phi_mul, 11
  %Q_load_126 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10111), align 4
  %tmp_21_10 = fmul float %Q_load_121, %Q_load_126
  %temp_4_10 = fdiv float %tmp_21_10, %qStar
  %C_load_126 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10111), align 4
  %tmp_22_10 = fmul float %C_load_121, %C_load_126
  %tmp_23_10 = fmul float %C_load_121, %Q_load_126
  %tmp_24_10 = fadd float %tmp_22_10, %tmp_23_10
  %tmp_25_10 = fmul float %Q_load_121, %C_load_126
  %tmp_26_10 = fadd float %tmp_24_10, %tmp_25_10
  %tmp_27_10 = fadd float %tmp_26_10, %tmp_21_10
  %tmp_28_10 = fdiv float %tmp_27_10, %tmp_5
  %tmp_29_10 = fsub float %temp_4_10, %tmp_28_10
  %tmp_30_10 = zext i14 %c_10 to i64
  %C_addr_14 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_10
  %C_load_127 = load float* %C_addr_14, align 4
  %tmp_31_10 = fadd float %C_load_127, %tmp_29_10
  store float %tmp_31_10, float* %C_addr_14, align 4
  %Q_addr_14 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_10
  %Q_load_127 = load float* %Q_addr_14, align 4
  %tmp_32_10 = fsub float %Q_load_127, %temp_4_10
  store float %tmp_32_10, float* %Q_addr_14, align 4
  %c_11 = add i14 %phi_mul, 12
  %Q_load_128 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10112), align 16
  %tmp_21_11 = fmul float %Q_load_121, %Q_load_128
  %temp_4_11 = fdiv float %tmp_21_11, %qStar
  %C_load_128 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10112), align 16
  %tmp_22_11 = fmul float %C_load_121, %C_load_128
  %tmp_23_11 = fmul float %C_load_121, %Q_load_128
  %tmp_24_11 = fadd float %tmp_22_11, %tmp_23_11
  %tmp_25_11 = fmul float %Q_load_121, %C_load_128
  %tmp_26_11 = fadd float %tmp_24_11, %tmp_25_11
  %tmp_27_11 = fadd float %tmp_26_11, %tmp_21_11
  %tmp_28_11 = fdiv float %tmp_27_11, %tmp_5
  %tmp_29_11 = fsub float %temp_4_11, %tmp_28_11
  %tmp_30_11 = zext i14 %c_11 to i64
  %C_addr_15 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_11
  %C_load_129 = load float* %C_addr_15, align 4
  %tmp_31_11 = fadd float %C_load_129, %tmp_29_11
  store float %tmp_31_11, float* %C_addr_15, align 4
  %Q_addr_15 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_11
  %Q_load_129 = load float* %Q_addr_15, align 4
  %tmp_32_11 = fsub float %Q_load_129, %temp_4_11
  store float %tmp_32_11, float* %Q_addr_15, align 4
  %c_12 = add i14 %phi_mul, 13
  %Q_load_130 = load float* %Q_addr, align 4
  %Q_load_131 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10113), align 4
  %tmp_21_12 = fmul float %Q_load_130, %Q_load_131
  %temp_4_12 = fdiv float %tmp_21_12, %qStar
  %C_load_130 = load float* %C_addr, align 4
  %C_load_131 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10113), align 4
  %tmp_22_12 = fmul float %C_load_130, %C_load_131
  %tmp_23_12 = fmul float %C_load_130, %Q_load_131
  %tmp_24_12 = fadd float %tmp_22_12, %tmp_23_12
  %tmp_25_12 = fmul float %Q_load_130, %C_load_131
  %tmp_26_12 = fadd float %tmp_24_12, %tmp_25_12
  %tmp_27_12 = fadd float %tmp_26_12, %tmp_21_12
  %tmp_28_12 = fdiv float %tmp_27_12, %tmp_5
  %tmp_29_12 = fsub float %temp_4_12, %tmp_28_12
  %tmp_30_12 = zext i14 %c_12 to i64
  %C_addr_16 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_12
  %C_load_132 = load float* %C_addr_16, align 4
  %tmp_31_12 = fadd float %C_load_132, %tmp_29_12
  store float %tmp_31_12, float* %C_addr_16, align 4
  %Q_addr_16 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_12
  %Q_load_132 = load float* %Q_addr_16, align 4
  %tmp_32_12 = fsub float %Q_load_132, %temp_4_12
  store float %tmp_32_12, float* %Q_addr_16, align 4
  %c_13 = add i14 %phi_mul, 14
  %Q_load_133 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10114), align 8
  %tmp_21_13 = fmul float %Q_load_130, %Q_load_133
  %temp_4_13 = fdiv float %tmp_21_13, %qStar
  %C_load_133 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10114), align 8
  %tmp_22_13 = fmul float %C_load_130, %C_load_133
  %tmp_23_13 = fmul float %C_load_130, %Q_load_133
  %tmp_24_13 = fadd float %tmp_22_13, %tmp_23_13
  %tmp_25_13 = fmul float %Q_load_130, %C_load_133
  %tmp_26_13 = fadd float %tmp_24_13, %tmp_25_13
  %tmp_27_13 = fadd float %tmp_26_13, %tmp_21_13
  %tmp_28_13 = fdiv float %tmp_27_13, %tmp_5
  %tmp_29_13 = fsub float %temp_4_13, %tmp_28_13
  %tmp_30_13 = zext i14 %c_13 to i64
  %C_addr_17 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_13
  %C_load_134 = load float* %C_addr_17, align 4
  %tmp_31_13 = fadd float %C_load_134, %tmp_29_13
  store float %tmp_31_13, float* %C_addr_17, align 4
  %Q_addr_17 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_13
  %Q_load_134 = load float* %Q_addr_17, align 4
  %tmp_32_13 = fsub float %Q_load_134, %temp_4_13
  store float %tmp_32_13, float* %Q_addr_17, align 4
  %c_14 = add i14 %phi_mul, 15
  %Q_load_135 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10115), align 4
  %tmp_21_14 = fmul float %Q_load_130, %Q_load_135
  %temp_4_14 = fdiv float %tmp_21_14, %qStar
  %C_load_135 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10115), align 4
  %tmp_22_14 = fmul float %C_load_130, %C_load_135
  %tmp_23_14 = fmul float %C_load_130, %Q_load_135
  %tmp_24_14 = fadd float %tmp_22_14, %tmp_23_14
  %tmp_25_14 = fmul float %Q_load_130, %C_load_135
  %tmp_26_14 = fadd float %tmp_24_14, %tmp_25_14
  %tmp_27_14 = fadd float %tmp_26_14, %tmp_21_14
  %tmp_28_14 = fdiv float %tmp_27_14, %tmp_5
  %tmp_29_14 = fsub float %temp_4_14, %tmp_28_14
  %tmp_30_14 = zext i14 %c_14 to i64
  %C_addr_18 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_14
  %C_load_136 = load float* %C_addr_18, align 4
  %tmp_31_14 = fadd float %C_load_136, %tmp_29_14
  store float %tmp_31_14, float* %C_addr_18, align 4
  %Q_addr_18 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_14
  %Q_load_136 = load float* %Q_addr_18, align 4
  %tmp_32_14 = fsub float %Q_load_136, %temp_4_14
  store float %tmp_32_14, float* %Q_addr_18, align 4
  %c_15 = add i14 %phi_mul, 16
  %Q_load_137 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10116), align 16
  %tmp_21_15 = fmul float %Q_load_130, %Q_load_137
  %temp_4_15 = fdiv float %tmp_21_15, %qStar
  %C_load_137 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10116), align 16
  %tmp_22_15 = fmul float %C_load_130, %C_load_137
  %tmp_23_15 = fmul float %C_load_130, %Q_load_137
  %tmp_24_15 = fadd float %tmp_22_15, %tmp_23_15
  %tmp_25_15 = fmul float %Q_load_130, %C_load_137
  %tmp_26_15 = fadd float %tmp_24_15, %tmp_25_15
  %tmp_27_15 = fadd float %tmp_26_15, %tmp_21_15
  %tmp_28_15 = fdiv float %tmp_27_15, %tmp_5
  %tmp_29_15 = fsub float %temp_4_15, %tmp_28_15
  %tmp_30_15 = zext i14 %c_15 to i64
  %C_addr_19 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_15
  %C_load_138 = load float* %C_addr_19, align 4
  %tmp_31_15 = fadd float %C_load_138, %tmp_29_15
  store float %tmp_31_15, float* %C_addr_19, align 4
  %Q_addr_19 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_15
  %Q_load_138 = load float* %Q_addr_19, align 4
  %tmp_32_15 = fsub float %Q_load_138, %temp_4_15
  store float %tmp_32_15, float* %Q_addr_19, align 4
  %c_16 = add i14 %phi_mul, 17
  %Q_load_139 = load float* %Q_addr, align 4
  %Q_load_140 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10117), align 4
  %tmp_21_16 = fmul float %Q_load_139, %Q_load_140
  %temp_4_16 = fdiv float %tmp_21_16, %qStar
  %C_load_139 = load float* %C_addr, align 4
  %C_load_140 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10117), align 4
  %tmp_22_16 = fmul float %C_load_139, %C_load_140
  %tmp_23_16 = fmul float %C_load_139, %Q_load_140
  %tmp_24_16 = fadd float %tmp_22_16, %tmp_23_16
  %tmp_25_16 = fmul float %Q_load_139, %C_load_140
  %tmp_26_16 = fadd float %tmp_24_16, %tmp_25_16
  %tmp_27_16 = fadd float %tmp_26_16, %tmp_21_16
  %tmp_28_16 = fdiv float %tmp_27_16, %tmp_5
  %tmp_29_16 = fsub float %temp_4_16, %tmp_28_16
  %tmp_30_16 = zext i14 %c_16 to i64
  %C_addr_20 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_16
  %C_load_141 = load float* %C_addr_20, align 4
  %tmp_31_16 = fadd float %C_load_141, %tmp_29_16
  store float %tmp_31_16, float* %C_addr_20, align 4
  %Q_addr_20 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_16
  %Q_load_141 = load float* %Q_addr_20, align 4
  %tmp_32_16 = fsub float %Q_load_141, %temp_4_16
  store float %tmp_32_16, float* %Q_addr_20, align 4
  %c_17 = add i14 %phi_mul, 18
  %Q_load_142 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10118), align 8
  %tmp_21_17 = fmul float %Q_load_139, %Q_load_142
  %temp_4_17 = fdiv float %tmp_21_17, %qStar
  %C_load_142 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10118), align 8
  %tmp_22_17 = fmul float %C_load_139, %C_load_142
  %tmp_23_17 = fmul float %C_load_139, %Q_load_142
  %tmp_24_17 = fadd float %tmp_22_17, %tmp_23_17
  %tmp_25_17 = fmul float %Q_load_139, %C_load_142
  %tmp_26_17 = fadd float %tmp_24_17, %tmp_25_17
  %tmp_27_17 = fadd float %tmp_26_17, %tmp_21_17
  %tmp_28_17 = fdiv float %tmp_27_17, %tmp_5
  %tmp_29_17 = fsub float %temp_4_17, %tmp_28_17
  %tmp_30_17 = zext i14 %c_17 to i64
  %C_addr_21 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_17
  %C_load_143 = load float* %C_addr_21, align 4
  %tmp_31_17 = fadd float %C_load_143, %tmp_29_17
  store float %tmp_31_17, float* %C_addr_21, align 4
  %Q_addr_21 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_17
  %Q_load_143 = load float* %Q_addr_21, align 4
  %tmp_32_17 = fsub float %Q_load_143, %temp_4_17
  store float %tmp_32_17, float* %Q_addr_21, align 4
  %c_18 = add i14 %phi_mul, 19
  %Q_load_144 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10119), align 4
  %tmp_21_18 = fmul float %Q_load_139, %Q_load_144
  %temp_4_18 = fdiv float %tmp_21_18, %qStar
  %C_load_144 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10119), align 4
  %tmp_22_18 = fmul float %C_load_139, %C_load_144
  %tmp_23_18 = fmul float %C_load_139, %Q_load_144
  %tmp_24_18 = fadd float %tmp_22_18, %tmp_23_18
  %tmp_25_18 = fmul float %Q_load_139, %C_load_144
  %tmp_26_18 = fadd float %tmp_24_18, %tmp_25_18
  %tmp_27_18 = fadd float %tmp_26_18, %tmp_21_18
  %tmp_28_18 = fdiv float %tmp_27_18, %tmp_5
  %tmp_29_18 = fsub float %temp_4_18, %tmp_28_18
  %tmp_30_18 = zext i14 %c_18 to i64
  %C_addr_22 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_18
  %C_load_145 = load float* %C_addr_22, align 4
  %tmp_31_18 = fadd float %C_load_145, %tmp_29_18
  store float %tmp_31_18, float* %C_addr_22, align 4
  %Q_addr_22 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_18
  %Q_load_145 = load float* %Q_addr_22, align 4
  %tmp_32_18 = fsub float %Q_load_145, %temp_4_18
  store float %tmp_32_18, float* %Q_addr_22, align 4
  %c_19 = add i14 %phi_mul, 20
  %Q_load_146 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10120), align 16
  %tmp_21_19 = fmul float %Q_load_139, %Q_load_146
  %temp_4_19 = fdiv float %tmp_21_19, %qStar
  %C_load_146 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10120), align 16
  %tmp_22_19 = fmul float %C_load_139, %C_load_146
  %tmp_23_19 = fmul float %C_load_139, %Q_load_146
  %tmp_24_19 = fadd float %tmp_22_19, %tmp_23_19
  %tmp_25_19 = fmul float %Q_load_139, %C_load_146
  %tmp_26_19 = fadd float %tmp_24_19, %tmp_25_19
  %tmp_27_19 = fadd float %tmp_26_19, %tmp_21_19
  %tmp_28_19 = fdiv float %tmp_27_19, %tmp_5
  %tmp_29_19 = fsub float %temp_4_19, %tmp_28_19
  %tmp_30_19 = zext i14 %c_19 to i64
  %C_addr_23 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_19
  %C_load_147 = load float* %C_addr_23, align 4
  %tmp_31_19 = fadd float %C_load_147, %tmp_29_19
  store float %tmp_31_19, float* %C_addr_23, align 4
  %Q_addr_23 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_19
  %Q_load_147 = load float* %Q_addr_23, align 4
  %tmp_32_19 = fsub float %Q_load_147, %temp_4_19
  store float %tmp_32_19, float* %Q_addr_23, align 4
  %c_20 = add i14 %phi_mul, 21
  %Q_load_148 = load float* %Q_addr, align 4
  %Q_load_149 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10121), align 4
  %tmp_21_20 = fmul float %Q_load_148, %Q_load_149
  %temp_4_20 = fdiv float %tmp_21_20, %qStar
  %C_load_148 = load float* %C_addr, align 4
  %C_load_149 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10121), align 4
  %tmp_22_20 = fmul float %C_load_148, %C_load_149
  %tmp_23_20 = fmul float %C_load_148, %Q_load_149
  %tmp_24_20 = fadd float %tmp_22_20, %tmp_23_20
  %tmp_25_20 = fmul float %Q_load_148, %C_load_149
  %tmp_26_20 = fadd float %tmp_24_20, %tmp_25_20
  %tmp_27_20 = fadd float %tmp_26_20, %tmp_21_20
  %tmp_28_20 = fdiv float %tmp_27_20, %tmp_5
  %tmp_29_20 = fsub float %temp_4_20, %tmp_28_20
  %tmp_30_20 = zext i14 %c_20 to i64
  %C_addr_24 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_20
  %C_load_150 = load float* %C_addr_24, align 4
  %tmp_31_20 = fadd float %C_load_150, %tmp_29_20
  store float %tmp_31_20, float* %C_addr_24, align 4
  %Q_addr_24 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_20
  %Q_load_150 = load float* %Q_addr_24, align 4
  %tmp_32_20 = fsub float %Q_load_150, %temp_4_20
  store float %tmp_32_20, float* %Q_addr_24, align 4
  %c_21 = add i14 %phi_mul, 22
  %Q_load_151 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10122), align 8
  %tmp_21_21 = fmul float %Q_load_148, %Q_load_151
  %temp_4_21 = fdiv float %tmp_21_21, %qStar
  %C_load_151 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10122), align 8
  %tmp_22_21 = fmul float %C_load_148, %C_load_151
  %tmp_23_21 = fmul float %C_load_148, %Q_load_151
  %tmp_24_21 = fadd float %tmp_22_21, %tmp_23_21
  %tmp_25_21 = fmul float %Q_load_148, %C_load_151
  %tmp_26_21 = fadd float %tmp_24_21, %tmp_25_21
  %tmp_27_21 = fadd float %tmp_26_21, %tmp_21_21
  %tmp_28_21 = fdiv float %tmp_27_21, %tmp_5
  %tmp_29_21 = fsub float %temp_4_21, %tmp_28_21
  %tmp_30_21 = zext i14 %c_21 to i64
  %C_addr_25 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_21
  %C_load_152 = load float* %C_addr_25, align 4
  %tmp_31_21 = fadd float %C_load_152, %tmp_29_21
  store float %tmp_31_21, float* %C_addr_25, align 4
  %Q_addr_25 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_21
  %Q_load_152 = load float* %Q_addr_25, align 4
  %tmp_32_21 = fsub float %Q_load_152, %temp_4_21
  store float %tmp_32_21, float* %Q_addr_25, align 4
  %c_22 = add i14 %phi_mul, 23
  %Q_load_153 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10123), align 4
  %tmp_21_22 = fmul float %Q_load_148, %Q_load_153
  %temp_4_22 = fdiv float %tmp_21_22, %qStar
  %C_load_153 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10123), align 4
  %tmp_22_22 = fmul float %C_load_148, %C_load_153
  %tmp_23_22 = fmul float %C_load_148, %Q_load_153
  %tmp_24_22 = fadd float %tmp_22_22, %tmp_23_22
  %tmp_25_22 = fmul float %Q_load_148, %C_load_153
  %tmp_26_22 = fadd float %tmp_24_22, %tmp_25_22
  %tmp_27_22 = fadd float %tmp_26_22, %tmp_21_22
  %tmp_28_22 = fdiv float %tmp_27_22, %tmp_5
  %tmp_29_22 = fsub float %temp_4_22, %tmp_28_22
  %tmp_30_22 = zext i14 %c_22 to i64
  %C_addr_26 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_22
  %C_load_154 = load float* %C_addr_26, align 4
  %tmp_31_22 = fadd float %C_load_154, %tmp_29_22
  store float %tmp_31_22, float* %C_addr_26, align 4
  %Q_addr_26 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_22
  %Q_load_154 = load float* %Q_addr_26, align 4
  %tmp_32_22 = fsub float %Q_load_154, %temp_4_22
  store float %tmp_32_22, float* %Q_addr_26, align 4
  %c_23 = add i14 %phi_mul, 24
  %Q_load_155 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10124), align 16
  %tmp_21_23 = fmul float %Q_load_148, %Q_load_155
  %temp_4_23 = fdiv float %tmp_21_23, %qStar
  %C_load_155 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10124), align 16
  %tmp_22_23 = fmul float %C_load_148, %C_load_155
  %tmp_23_23 = fmul float %C_load_148, %Q_load_155
  %tmp_24_23 = fadd float %tmp_22_23, %tmp_23_23
  %tmp_25_23 = fmul float %Q_load_148, %C_load_155
  %tmp_26_23 = fadd float %tmp_24_23, %tmp_25_23
  %tmp_27_23 = fadd float %tmp_26_23, %tmp_21_23
  %tmp_28_23 = fdiv float %tmp_27_23, %tmp_5
  %tmp_29_23 = fsub float %temp_4_23, %tmp_28_23
  %tmp_30_23 = zext i14 %c_23 to i64
  %C_addr_27 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_23
  %C_load_156 = load float* %C_addr_27, align 4
  %tmp_31_23 = fadd float %C_load_156, %tmp_29_23
  store float %tmp_31_23, float* %C_addr_27, align 4
  %Q_addr_27 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_23
  %Q_load_156 = load float* %Q_addr_27, align 4
  %tmp_32_23 = fsub float %Q_load_156, %temp_4_23
  store float %tmp_32_23, float* %Q_addr_27, align 4
  %c_24 = add i14 %phi_mul, 25
  %Q_load_157 = load float* %Q_addr, align 4
  %Q_load_158 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10125), align 4
  %tmp_21_24 = fmul float %Q_load_157, %Q_load_158
  %temp_4_24 = fdiv float %tmp_21_24, %qStar
  %C_load_157 = load float* %C_addr, align 4
  %C_load_158 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10125), align 4
  %tmp_22_24 = fmul float %C_load_157, %C_load_158
  %tmp_23_24 = fmul float %C_load_157, %Q_load_158
  %tmp_24_24 = fadd float %tmp_22_24, %tmp_23_24
  %tmp_25_24 = fmul float %Q_load_157, %C_load_158
  %tmp_26_24 = fadd float %tmp_24_24, %tmp_25_24
  %tmp_27_24 = fadd float %tmp_26_24, %tmp_21_24
  %tmp_28_24 = fdiv float %tmp_27_24, %tmp_5
  %tmp_29_24 = fsub float %temp_4_24, %tmp_28_24
  %tmp_30_24 = zext i14 %c_24 to i64
  %C_addr_28 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_24
  %C_load_159 = load float* %C_addr_28, align 4
  %tmp_31_24 = fadd float %C_load_159, %tmp_29_24
  store float %tmp_31_24, float* %C_addr_28, align 4
  %Q_addr_28 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_24
  %Q_load_159 = load float* %Q_addr_28, align 4
  %tmp_32_24 = fsub float %Q_load_159, %temp_4_24
  store float %tmp_32_24, float* %Q_addr_28, align 4
  %c_25 = add i14 %phi_mul, 26
  %Q_load_160 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10126), align 8
  %tmp_21_25 = fmul float %Q_load_157, %Q_load_160
  %temp_4_25 = fdiv float %tmp_21_25, %qStar
  %C_load_160 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10126), align 8
  %tmp_22_25 = fmul float %C_load_157, %C_load_160
  %tmp_23_25 = fmul float %C_load_157, %Q_load_160
  %tmp_24_25 = fadd float %tmp_22_25, %tmp_23_25
  %tmp_25_25 = fmul float %Q_load_157, %C_load_160
  %tmp_26_25 = fadd float %tmp_24_25, %tmp_25_25
  %tmp_27_25 = fadd float %tmp_26_25, %tmp_21_25
  %tmp_28_25 = fdiv float %tmp_27_25, %tmp_5
  %tmp_29_25 = fsub float %temp_4_25, %tmp_28_25
  %tmp_30_25 = zext i14 %c_25 to i64
  %C_addr_29 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_25
  %C_load_161 = load float* %C_addr_29, align 4
  %tmp_31_25 = fadd float %C_load_161, %tmp_29_25
  store float %tmp_31_25, float* %C_addr_29, align 4
  %Q_addr_29 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_25
  %Q_load_161 = load float* %Q_addr_29, align 4
  %tmp_32_25 = fsub float %Q_load_161, %temp_4_25
  store float %tmp_32_25, float* %Q_addr_29, align 4
  %c_26 = add i14 %phi_mul, 27
  %Q_load_162 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10127), align 4
  %tmp_21_26 = fmul float %Q_load_157, %Q_load_162
  %temp_4_26 = fdiv float %tmp_21_26, %qStar
  %C_load_162 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10127), align 4
  %tmp_22_26 = fmul float %C_load_157, %C_load_162
  %tmp_23_26 = fmul float %C_load_157, %Q_load_162
  %tmp_24_26 = fadd float %tmp_22_26, %tmp_23_26
  %tmp_25_26 = fmul float %Q_load_157, %C_load_162
  %tmp_26_26 = fadd float %tmp_24_26, %tmp_25_26
  %tmp_27_26 = fadd float %tmp_26_26, %tmp_21_26
  %tmp_28_26 = fdiv float %tmp_27_26, %tmp_5
  %tmp_29_26 = fsub float %temp_4_26, %tmp_28_26
  %tmp_30_26 = zext i14 %c_26 to i64
  %C_addr_30 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_26
  %C_load_163 = load float* %C_addr_30, align 4
  %tmp_31_26 = fadd float %C_load_163, %tmp_29_26
  store float %tmp_31_26, float* %C_addr_30, align 4
  %Q_addr_30 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_26
  %Q_load_163 = load float* %Q_addr_30, align 4
  %tmp_32_26 = fsub float %Q_load_163, %temp_4_26
  store float %tmp_32_26, float* %Q_addr_30, align 4
  %c_27 = add i14 %phi_mul, 28
  %Q_load_164 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10128), align 16
  %tmp_21_27 = fmul float %Q_load_157, %Q_load_164
  %temp_4_27 = fdiv float %tmp_21_27, %qStar
  %C_load_164 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10128), align 16
  %tmp_22_27 = fmul float %C_load_157, %C_load_164
  %tmp_23_27 = fmul float %C_load_157, %Q_load_164
  %tmp_24_27 = fadd float %tmp_22_27, %tmp_23_27
  %tmp_25_27 = fmul float %Q_load_157, %C_load_164
  %tmp_26_27 = fadd float %tmp_24_27, %tmp_25_27
  %tmp_27_27 = fadd float %tmp_26_27, %tmp_21_27
  %tmp_28_27 = fdiv float %tmp_27_27, %tmp_5
  %tmp_29_27 = fsub float %temp_4_27, %tmp_28_27
  %tmp_30_27 = zext i14 %c_27 to i64
  %C_addr_31 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_27
  %C_load_165 = load float* %C_addr_31, align 4
  %tmp_31_27 = fadd float %C_load_165, %tmp_29_27
  store float %tmp_31_27, float* %C_addr_31, align 4
  %Q_addr_31 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_27
  %Q_load_165 = load float* %Q_addr_31, align 4
  %tmp_32_27 = fsub float %Q_load_165, %temp_4_27
  store float %tmp_32_27, float* %Q_addr_31, align 4
  %c_28 = add i14 %phi_mul, 29
  %Q_load_166 = load float* %Q_addr, align 4
  %Q_load_167 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10129), align 4
  %tmp_21_28 = fmul float %Q_load_166, %Q_load_167
  %temp_4_28 = fdiv float %tmp_21_28, %qStar
  %C_load_166 = load float* %C_addr, align 4
  %C_load_167 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10129), align 4
  %tmp_22_28 = fmul float %C_load_166, %C_load_167
  %tmp_23_28 = fmul float %C_load_166, %Q_load_167
  %tmp_24_28 = fadd float %tmp_22_28, %tmp_23_28
  %tmp_25_28 = fmul float %Q_load_166, %C_load_167
  %tmp_26_28 = fadd float %tmp_24_28, %tmp_25_28
  %tmp_27_28 = fadd float %tmp_26_28, %tmp_21_28
  %tmp_28_28 = fdiv float %tmp_27_28, %tmp_5
  %tmp_29_28 = fsub float %temp_4_28, %tmp_28_28
  %tmp_30_28 = zext i14 %c_28 to i64
  %C_addr_32 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_28
  %C_load_168 = load float* %C_addr_32, align 4
  %tmp_31_28 = fadd float %C_load_168, %tmp_29_28
  store float %tmp_31_28, float* %C_addr_32, align 4
  %Q_addr_32 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_28
  %Q_load_168 = load float* %Q_addr_32, align 4
  %tmp_32_28 = fsub float %Q_load_168, %temp_4_28
  store float %tmp_32_28, float* %Q_addr_32, align 4
  %c_29 = add i14 %phi_mul, 30
  %Q_load_169 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10130), align 8
  %tmp_21_29 = fmul float %Q_load_166, %Q_load_169
  %temp_4_29 = fdiv float %tmp_21_29, %qStar
  %C_load_169 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10130), align 8
  %tmp_22_29 = fmul float %C_load_166, %C_load_169
  %tmp_23_29 = fmul float %C_load_166, %Q_load_169
  %tmp_24_29 = fadd float %tmp_22_29, %tmp_23_29
  %tmp_25_29 = fmul float %Q_load_166, %C_load_169
  %tmp_26_29 = fadd float %tmp_24_29, %tmp_25_29
  %tmp_27_29 = fadd float %tmp_26_29, %tmp_21_29
  %tmp_28_29 = fdiv float %tmp_27_29, %tmp_5
  %tmp_29_29 = fsub float %temp_4_29, %tmp_28_29
  %tmp_30_29 = zext i14 %c_29 to i64
  %C_addr_33 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_29
  %C_load_170 = load float* %C_addr_33, align 4
  %tmp_31_29 = fadd float %C_load_170, %tmp_29_29
  store float %tmp_31_29, float* %C_addr_33, align 4
  %Q_addr_33 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_29
  %Q_load_170 = load float* %Q_addr_33, align 4
  %tmp_32_29 = fsub float %Q_load_170, %temp_4_29
  store float %tmp_32_29, float* %Q_addr_33, align 4
  %c_30 = add i14 %phi_mul, 31
  %Q_load_171 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10131), align 4
  %tmp_21_30 = fmul float %Q_load_166, %Q_load_171
  %temp_4_30 = fdiv float %tmp_21_30, %qStar
  %C_load_171 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10131), align 4
  %tmp_22_30 = fmul float %C_load_166, %C_load_171
  %tmp_23_30 = fmul float %C_load_166, %Q_load_171
  %tmp_24_30 = fadd float %tmp_22_30, %tmp_23_30
  %tmp_25_30 = fmul float %Q_load_166, %C_load_171
  %tmp_26_30 = fadd float %tmp_24_30, %tmp_25_30
  %tmp_27_30 = fadd float %tmp_26_30, %tmp_21_30
  %tmp_28_30 = fdiv float %tmp_27_30, %tmp_5
  %tmp_29_30 = fsub float %temp_4_30, %tmp_28_30
  %tmp_30_30 = zext i14 %c_30 to i64
  %C_addr_34 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_30
  %C_load_172 = load float* %C_addr_34, align 4
  %tmp_31_30 = fadd float %C_load_172, %tmp_29_30
  store float %tmp_31_30, float* %C_addr_34, align 4
  %Q_addr_34 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_30
  %Q_load_172 = load float* %Q_addr_34, align 4
  %tmp_32_30 = fsub float %Q_load_172, %temp_4_30
  store float %tmp_32_30, float* %Q_addr_34, align 4
  %c_31 = add i14 %phi_mul, 32
  %Q_load_173 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10132), align 16
  %tmp_21_31 = fmul float %Q_load_166, %Q_load_173
  %temp_4_31 = fdiv float %tmp_21_31, %qStar
  %C_load_173 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10132), align 16
  %tmp_22_31 = fmul float %C_load_166, %C_load_173
  %tmp_23_31 = fmul float %C_load_166, %Q_load_173
  %tmp_24_31 = fadd float %tmp_22_31, %tmp_23_31
  %tmp_25_31 = fmul float %Q_load_166, %C_load_173
  %tmp_26_31 = fadd float %tmp_24_31, %tmp_25_31
  %tmp_27_31 = fadd float %tmp_26_31, %tmp_21_31
  %tmp_28_31 = fdiv float %tmp_27_31, %tmp_5
  %tmp_29_31 = fsub float %temp_4_31, %tmp_28_31
  %tmp_30_31 = zext i14 %c_31 to i64
  %C_addr_35 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_31
  %C_load_174 = load float* %C_addr_35, align 4
  %tmp_31_31 = fadd float %C_load_174, %tmp_29_31
  store float %tmp_31_31, float* %C_addr_35, align 4
  %Q_addr_35 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_31
  %Q_load_174 = load float* %Q_addr_35, align 4
  %tmp_32_31 = fsub float %Q_load_174, %temp_4_31
  store float %tmp_32_31, float* %Q_addr_35, align 4
  %c_32 = add i14 %phi_mul, 33
  %Q_load_175 = load float* %Q_addr, align 4
  %Q_load_176 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10133), align 4
  %tmp_21_32 = fmul float %Q_load_175, %Q_load_176
  %temp_4_32 = fdiv float %tmp_21_32, %qStar
  %C_load_175 = load float* %C_addr, align 4
  %C_load_176 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10133), align 4
  %tmp_22_32 = fmul float %C_load_175, %C_load_176
  %tmp_23_32 = fmul float %C_load_175, %Q_load_176
  %tmp_24_32 = fadd float %tmp_22_32, %tmp_23_32
  %tmp_25_32 = fmul float %Q_load_175, %C_load_176
  %tmp_26_32 = fadd float %tmp_24_32, %tmp_25_32
  %tmp_27_32 = fadd float %tmp_26_32, %tmp_21_32
  %tmp_28_32 = fdiv float %tmp_27_32, %tmp_5
  %tmp_29_32 = fsub float %temp_4_32, %tmp_28_32
  %tmp_30_32 = zext i14 %c_32 to i64
  %C_addr_36 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_32
  %C_load_177 = load float* %C_addr_36, align 4
  %tmp_31_32 = fadd float %C_load_177, %tmp_29_32
  store float %tmp_31_32, float* %C_addr_36, align 4
  %Q_addr_36 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_32
  %Q_load_177 = load float* %Q_addr_36, align 4
  %tmp_32_32 = fsub float %Q_load_177, %temp_4_32
  store float %tmp_32_32, float* %Q_addr_36, align 4
  %c_33 = add i14 %phi_mul, 34
  %Q_load_178 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10134), align 8
  %tmp_21_33 = fmul float %Q_load_175, %Q_load_178
  %temp_4_33 = fdiv float %tmp_21_33, %qStar
  %C_load_178 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10134), align 8
  %tmp_22_33 = fmul float %C_load_175, %C_load_178
  %tmp_23_33 = fmul float %C_load_175, %Q_load_178
  %tmp_24_33 = fadd float %tmp_22_33, %tmp_23_33
  %tmp_25_33 = fmul float %Q_load_175, %C_load_178
  %tmp_26_33 = fadd float %tmp_24_33, %tmp_25_33
  %tmp_27_33 = fadd float %tmp_26_33, %tmp_21_33
  %tmp_28_33 = fdiv float %tmp_27_33, %tmp_5
  %tmp_29_33 = fsub float %temp_4_33, %tmp_28_33
  %tmp_30_33 = zext i14 %c_33 to i64
  %C_addr_37 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_33
  %C_load_179 = load float* %C_addr_37, align 4
  %tmp_31_33 = fadd float %C_load_179, %tmp_29_33
  store float %tmp_31_33, float* %C_addr_37, align 4
  %Q_addr_37 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_33
  %Q_load_179 = load float* %Q_addr_37, align 4
  %tmp_32_33 = fsub float %Q_load_179, %temp_4_33
  store float %tmp_32_33, float* %Q_addr_37, align 4
  %c_34 = add i14 %phi_mul, 35
  %Q_load_180 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10135), align 4
  %tmp_21_34 = fmul float %Q_load_175, %Q_load_180
  %temp_4_34 = fdiv float %tmp_21_34, %qStar
  %C_load_180 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10135), align 4
  %tmp_22_34 = fmul float %C_load_175, %C_load_180
  %tmp_23_34 = fmul float %C_load_175, %Q_load_180
  %tmp_24_34 = fadd float %tmp_22_34, %tmp_23_34
  %tmp_25_34 = fmul float %Q_load_175, %C_load_180
  %tmp_26_34 = fadd float %tmp_24_34, %tmp_25_34
  %tmp_27_34 = fadd float %tmp_26_34, %tmp_21_34
  %tmp_28_34 = fdiv float %tmp_27_34, %tmp_5
  %tmp_29_34 = fsub float %temp_4_34, %tmp_28_34
  %tmp_30_34 = zext i14 %c_34 to i64
  %C_addr_38 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_34
  %C_load_181 = load float* %C_addr_38, align 4
  %tmp_31_34 = fadd float %C_load_181, %tmp_29_34
  store float %tmp_31_34, float* %C_addr_38, align 4
  %Q_addr_38 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_34
  %Q_load_181 = load float* %Q_addr_38, align 4
  %tmp_32_34 = fsub float %Q_load_181, %temp_4_34
  store float %tmp_32_34, float* %Q_addr_38, align 4
  %c_35 = add i14 %phi_mul, 36
  %Q_load_182 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10136), align 16
  %tmp_21_35 = fmul float %Q_load_175, %Q_load_182
  %temp_4_35 = fdiv float %tmp_21_35, %qStar
  %C_load_182 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10136), align 16
  %tmp_22_35 = fmul float %C_load_175, %C_load_182
  %tmp_23_35 = fmul float %C_load_175, %Q_load_182
  %tmp_24_35 = fadd float %tmp_22_35, %tmp_23_35
  %tmp_25_35 = fmul float %Q_load_175, %C_load_182
  %tmp_26_35 = fadd float %tmp_24_35, %tmp_25_35
  %tmp_27_35 = fadd float %tmp_26_35, %tmp_21_35
  %tmp_28_35 = fdiv float %tmp_27_35, %tmp_5
  %tmp_29_35 = fsub float %temp_4_35, %tmp_28_35
  %tmp_30_35 = zext i14 %c_35 to i64
  %C_addr_39 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_35
  %C_load_183 = load float* %C_addr_39, align 4
  %tmp_31_35 = fadd float %C_load_183, %tmp_29_35
  store float %tmp_31_35, float* %C_addr_39, align 4
  %Q_addr_39 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_35
  %Q_load_183 = load float* %Q_addr_39, align 4
  %tmp_32_35 = fsub float %Q_load_183, %temp_4_35
  store float %tmp_32_35, float* %Q_addr_39, align 4
  %c_36 = add i14 %phi_mul, 37
  %Q_load_184 = load float* %Q_addr, align 4
  %Q_load_185 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10137), align 4
  %tmp_21_36 = fmul float %Q_load_184, %Q_load_185
  %temp_4_36 = fdiv float %tmp_21_36, %qStar
  %C_load_184 = load float* %C_addr, align 4
  %C_load_185 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10137), align 4
  %tmp_22_36 = fmul float %C_load_184, %C_load_185
  %tmp_23_36 = fmul float %C_load_184, %Q_load_185
  %tmp_24_36 = fadd float %tmp_22_36, %tmp_23_36
  %tmp_25_36 = fmul float %Q_load_184, %C_load_185
  %tmp_26_36 = fadd float %tmp_24_36, %tmp_25_36
  %tmp_27_36 = fadd float %tmp_26_36, %tmp_21_36
  %tmp_28_36 = fdiv float %tmp_27_36, %tmp_5
  %tmp_29_36 = fsub float %temp_4_36, %tmp_28_36
  %tmp_30_36 = zext i14 %c_36 to i64
  %C_addr_40 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_36
  %C_load_186 = load float* %C_addr_40, align 4
  %tmp_31_36 = fadd float %C_load_186, %tmp_29_36
  store float %tmp_31_36, float* %C_addr_40, align 4
  %Q_addr_40 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_36
  %Q_load_186 = load float* %Q_addr_40, align 4
  %tmp_32_36 = fsub float %Q_load_186, %temp_4_36
  store float %tmp_32_36, float* %Q_addr_40, align 4
  %c_37 = add i14 %phi_mul, 38
  %Q_load_187 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10138), align 8
  %tmp_21_37 = fmul float %Q_load_184, %Q_load_187
  %temp_4_37 = fdiv float %tmp_21_37, %qStar
  %C_load_187 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10138), align 8
  %tmp_22_37 = fmul float %C_load_184, %C_load_187
  %tmp_23_37 = fmul float %C_load_184, %Q_load_187
  %tmp_24_37 = fadd float %tmp_22_37, %tmp_23_37
  %tmp_25_37 = fmul float %Q_load_184, %C_load_187
  %tmp_26_37 = fadd float %tmp_24_37, %tmp_25_37
  %tmp_27_37 = fadd float %tmp_26_37, %tmp_21_37
  %tmp_28_37 = fdiv float %tmp_27_37, %tmp_5
  %tmp_29_37 = fsub float %temp_4_37, %tmp_28_37
  %tmp_30_37 = zext i14 %c_37 to i64
  %C_addr_41 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_37
  %C_load_188 = load float* %C_addr_41, align 4
  %tmp_31_37 = fadd float %C_load_188, %tmp_29_37
  store float %tmp_31_37, float* %C_addr_41, align 4
  %Q_addr_41 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_37
  %Q_load_188 = load float* %Q_addr_41, align 4
  %tmp_32_37 = fsub float %Q_load_188, %temp_4_37
  store float %tmp_32_37, float* %Q_addr_41, align 4
  %c_38 = add i14 %phi_mul, 39
  %Q_load_189 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10139), align 4
  %tmp_21_38 = fmul float %Q_load_184, %Q_load_189
  %temp_4_38 = fdiv float %tmp_21_38, %qStar
  %C_load_189 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10139), align 4
  %tmp_22_38 = fmul float %C_load_184, %C_load_189
  %tmp_23_38 = fmul float %C_load_184, %Q_load_189
  %tmp_24_38 = fadd float %tmp_22_38, %tmp_23_38
  %tmp_25_38 = fmul float %Q_load_184, %C_load_189
  %tmp_26_38 = fadd float %tmp_24_38, %tmp_25_38
  %tmp_27_38 = fadd float %tmp_26_38, %tmp_21_38
  %tmp_28_38 = fdiv float %tmp_27_38, %tmp_5
  %tmp_29_38 = fsub float %temp_4_38, %tmp_28_38
  %tmp_30_38 = zext i14 %c_38 to i64
  %C_addr_42 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_38
  %C_load_190 = load float* %C_addr_42, align 4
  %tmp_31_38 = fadd float %C_load_190, %tmp_29_38
  store float %tmp_31_38, float* %C_addr_42, align 4
  %Q_addr_42 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_38
  %Q_load_190 = load float* %Q_addr_42, align 4
  %tmp_32_38 = fsub float %Q_load_190, %temp_4_38
  store float %tmp_32_38, float* %Q_addr_42, align 4
  %c_39 = add i14 %phi_mul, 40
  %Q_load_191 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10140), align 16
  %tmp_21_39 = fmul float %Q_load_184, %Q_load_191
  %temp_4_39 = fdiv float %tmp_21_39, %qStar
  %C_load_191 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10140), align 16
  %tmp_22_39 = fmul float %C_load_184, %C_load_191
  %tmp_23_39 = fmul float %C_load_184, %Q_load_191
  %tmp_24_39 = fadd float %tmp_22_39, %tmp_23_39
  %tmp_25_39 = fmul float %Q_load_184, %C_load_191
  %tmp_26_39 = fadd float %tmp_24_39, %tmp_25_39
  %tmp_27_39 = fadd float %tmp_26_39, %tmp_21_39
  %tmp_28_39 = fdiv float %tmp_27_39, %tmp_5
  %tmp_29_39 = fsub float %temp_4_39, %tmp_28_39
  %tmp_30_39 = zext i14 %c_39 to i64
  %C_addr_43 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_39
  %C_load_192 = load float* %C_addr_43, align 4
  %tmp_31_39 = fadd float %C_load_192, %tmp_29_39
  store float %tmp_31_39, float* %C_addr_43, align 4
  %Q_addr_43 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_39
  %Q_load_192 = load float* %Q_addr_43, align 4
  %tmp_32_39 = fsub float %Q_load_192, %temp_4_39
  store float %tmp_32_39, float* %Q_addr_43, align 4
  %c_40 = add i14 %phi_mul, 41
  %Q_load_193 = load float* %Q_addr, align 4
  %Q_load_194 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10141), align 4
  %tmp_21_40 = fmul float %Q_load_193, %Q_load_194
  %temp_4_40 = fdiv float %tmp_21_40, %qStar
  %C_load_193 = load float* %C_addr, align 4
  %C_load_194 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10141), align 4
  %tmp_22_40 = fmul float %C_load_193, %C_load_194
  %tmp_23_40 = fmul float %C_load_193, %Q_load_194
  %tmp_24_40 = fadd float %tmp_22_40, %tmp_23_40
  %tmp_25_40 = fmul float %Q_load_193, %C_load_194
  %tmp_26_40 = fadd float %tmp_24_40, %tmp_25_40
  %tmp_27_40 = fadd float %tmp_26_40, %tmp_21_40
  %tmp_28_40 = fdiv float %tmp_27_40, %tmp_5
  %tmp_29_40 = fsub float %temp_4_40, %tmp_28_40
  %tmp_30_40 = zext i14 %c_40 to i64
  %C_addr_44 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_40
  %C_load_195 = load float* %C_addr_44, align 4
  %tmp_31_40 = fadd float %C_load_195, %tmp_29_40
  store float %tmp_31_40, float* %C_addr_44, align 4
  %Q_addr_44 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_40
  %Q_load_195 = load float* %Q_addr_44, align 4
  %tmp_32_40 = fsub float %Q_load_195, %temp_4_40
  store float %tmp_32_40, float* %Q_addr_44, align 4
  %c_41 = add i14 %phi_mul, 42
  %Q_load_196 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10142), align 8
  %tmp_21_41 = fmul float %Q_load_193, %Q_load_196
  %temp_4_41 = fdiv float %tmp_21_41, %qStar
  %C_load_196 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10142), align 8
  %tmp_22_41 = fmul float %C_load_193, %C_load_196
  %tmp_23_41 = fmul float %C_load_193, %Q_load_196
  %tmp_24_41 = fadd float %tmp_22_41, %tmp_23_41
  %tmp_25_41 = fmul float %Q_load_193, %C_load_196
  %tmp_26_41 = fadd float %tmp_24_41, %tmp_25_41
  %tmp_27_41 = fadd float %tmp_26_41, %tmp_21_41
  %tmp_28_41 = fdiv float %tmp_27_41, %tmp_5
  %tmp_29_41 = fsub float %temp_4_41, %tmp_28_41
  %tmp_30_41 = zext i14 %c_41 to i64
  %C_addr_45 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_41
  %C_load_197 = load float* %C_addr_45, align 4
  %tmp_31_41 = fadd float %C_load_197, %tmp_29_41
  store float %tmp_31_41, float* %C_addr_45, align 4
  %Q_addr_45 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_41
  %Q_load_197 = load float* %Q_addr_45, align 4
  %tmp_32_41 = fsub float %Q_load_197, %temp_4_41
  store float %tmp_32_41, float* %Q_addr_45, align 4
  %c_42 = add i14 %phi_mul, 43
  %Q_load_198 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10143), align 4
  %tmp_21_42 = fmul float %Q_load_193, %Q_load_198
  %temp_4_42 = fdiv float %tmp_21_42, %qStar
  %C_load_198 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10143), align 4
  %tmp_22_42 = fmul float %C_load_193, %C_load_198
  %tmp_23_42 = fmul float %C_load_193, %Q_load_198
  %tmp_24_42 = fadd float %tmp_22_42, %tmp_23_42
  %tmp_25_42 = fmul float %Q_load_193, %C_load_198
  %tmp_26_42 = fadd float %tmp_24_42, %tmp_25_42
  %tmp_27_42 = fadd float %tmp_26_42, %tmp_21_42
  %tmp_28_42 = fdiv float %tmp_27_42, %tmp_5
  %tmp_29_42 = fsub float %temp_4_42, %tmp_28_42
  %tmp_30_42 = zext i14 %c_42 to i64
  %C_addr_46 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_42
  %C_load_199 = load float* %C_addr_46, align 4
  %tmp_31_42 = fadd float %C_load_199, %tmp_29_42
  store float %tmp_31_42, float* %C_addr_46, align 4
  %Q_addr_46 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_42
  %Q_load_199 = load float* %Q_addr_46, align 4
  %tmp_32_42 = fsub float %Q_load_199, %temp_4_42
  store float %tmp_32_42, float* %Q_addr_46, align 4
  %c_43 = add i14 %phi_mul, 44
  %Q_load_200 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10144), align 16
  %tmp_21_43 = fmul float %Q_load_193, %Q_load_200
  %temp_4_43 = fdiv float %tmp_21_43, %qStar
  %C_load_200 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10144), align 16
  %tmp_22_43 = fmul float %C_load_193, %C_load_200
  %tmp_23_43 = fmul float %C_load_193, %Q_load_200
  %tmp_24_43 = fadd float %tmp_22_43, %tmp_23_43
  %tmp_25_43 = fmul float %Q_load_193, %C_load_200
  %tmp_26_43 = fadd float %tmp_24_43, %tmp_25_43
  %tmp_27_43 = fadd float %tmp_26_43, %tmp_21_43
  %tmp_28_43 = fdiv float %tmp_27_43, %tmp_5
  %tmp_29_43 = fsub float %temp_4_43, %tmp_28_43
  %tmp_30_43 = zext i14 %c_43 to i64
  %C_addr_47 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_43
  %C_load_201 = load float* %C_addr_47, align 4
  %tmp_31_43 = fadd float %C_load_201, %tmp_29_43
  store float %tmp_31_43, float* %C_addr_47, align 4
  %Q_addr_47 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_43
  %Q_load_201 = load float* %Q_addr_47, align 4
  %tmp_32_43 = fsub float %Q_load_201, %temp_4_43
  store float %tmp_32_43, float* %Q_addr_47, align 4
  %c_44 = add i14 %phi_mul, 45
  %Q_load_202 = load float* %Q_addr, align 4
  %Q_load_203 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10145), align 4
  %tmp_21_44 = fmul float %Q_load_202, %Q_load_203
  %temp_4_44 = fdiv float %tmp_21_44, %qStar
  %C_load_202 = load float* %C_addr, align 4
  %C_load_203 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10145), align 4
  %tmp_22_44 = fmul float %C_load_202, %C_load_203
  %tmp_23_44 = fmul float %C_load_202, %Q_load_203
  %tmp_24_44 = fadd float %tmp_22_44, %tmp_23_44
  %tmp_25_44 = fmul float %Q_load_202, %C_load_203
  %tmp_26_44 = fadd float %tmp_24_44, %tmp_25_44
  %tmp_27_44 = fadd float %tmp_26_44, %tmp_21_44
  %tmp_28_44 = fdiv float %tmp_27_44, %tmp_5
  %tmp_29_44 = fsub float %temp_4_44, %tmp_28_44
  %tmp_30_44 = zext i14 %c_44 to i64
  %C_addr_48 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_44
  %C_load_204 = load float* %C_addr_48, align 4
  %tmp_31_44 = fadd float %C_load_204, %tmp_29_44
  store float %tmp_31_44, float* %C_addr_48, align 4
  %Q_addr_48 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_44
  %Q_load_204 = load float* %Q_addr_48, align 4
  %tmp_32_44 = fsub float %Q_load_204, %temp_4_44
  store float %tmp_32_44, float* %Q_addr_48, align 4
  %c_45 = add i14 %phi_mul, 46
  %Q_load_205 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10146), align 8
  %tmp_21_45 = fmul float %Q_load_202, %Q_load_205
  %temp_4_45 = fdiv float %tmp_21_45, %qStar
  %C_load_205 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10146), align 8
  %tmp_22_45 = fmul float %C_load_202, %C_load_205
  %tmp_23_45 = fmul float %C_load_202, %Q_load_205
  %tmp_24_45 = fadd float %tmp_22_45, %tmp_23_45
  %tmp_25_45 = fmul float %Q_load_202, %C_load_205
  %tmp_26_45 = fadd float %tmp_24_45, %tmp_25_45
  %tmp_27_45 = fadd float %tmp_26_45, %tmp_21_45
  %tmp_28_45 = fdiv float %tmp_27_45, %tmp_5
  %tmp_29_45 = fsub float %temp_4_45, %tmp_28_45
  %tmp_30_45 = zext i14 %c_45 to i64
  %C_addr_49 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_45
  %C_load_206 = load float* %C_addr_49, align 4
  %tmp_31_45 = fadd float %C_load_206, %tmp_29_45
  store float %tmp_31_45, float* %C_addr_49, align 4
  %Q_addr_49 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_45
  %Q_load_206 = load float* %Q_addr_49, align 4
  %tmp_32_45 = fsub float %Q_load_206, %temp_4_45
  store float %tmp_32_45, float* %Q_addr_49, align 4
  %c_46 = add i14 %phi_mul, 47
  %Q_load_207 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10147), align 4
  %tmp_21_46 = fmul float %Q_load_202, %Q_load_207
  %temp_4_46 = fdiv float %tmp_21_46, %qStar
  %C_load_207 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10147), align 4
  %tmp_22_46 = fmul float %C_load_202, %C_load_207
  %tmp_23_46 = fmul float %C_load_202, %Q_load_207
  %tmp_24_46 = fadd float %tmp_22_46, %tmp_23_46
  %tmp_25_46 = fmul float %Q_load_202, %C_load_207
  %tmp_26_46 = fadd float %tmp_24_46, %tmp_25_46
  %tmp_27_46 = fadd float %tmp_26_46, %tmp_21_46
  %tmp_28_46 = fdiv float %tmp_27_46, %tmp_5
  %tmp_29_46 = fsub float %temp_4_46, %tmp_28_46
  %tmp_30_46 = zext i14 %c_46 to i64
  %C_addr_50 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_46
  %C_load_208 = load float* %C_addr_50, align 4
  %tmp_31_46 = fadd float %C_load_208, %tmp_29_46
  store float %tmp_31_46, float* %C_addr_50, align 4
  %Q_addr_50 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_46
  %Q_load_208 = load float* %Q_addr_50, align 4
  %tmp_32_46 = fsub float %Q_load_208, %temp_4_46
  store float %tmp_32_46, float* %Q_addr_50, align 4
  %c_47 = add i14 %phi_mul, 48
  %Q_load_209 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10148), align 16
  %tmp_21_47 = fmul float %Q_load_202, %Q_load_209
  %temp_4_47 = fdiv float %tmp_21_47, %qStar
  %C_load_209 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10148), align 16
  %tmp_22_47 = fmul float %C_load_202, %C_load_209
  %tmp_23_47 = fmul float %C_load_202, %Q_load_209
  %tmp_24_47 = fadd float %tmp_22_47, %tmp_23_47
  %tmp_25_47 = fmul float %Q_load_202, %C_load_209
  %tmp_26_47 = fadd float %tmp_24_47, %tmp_25_47
  %tmp_27_47 = fadd float %tmp_26_47, %tmp_21_47
  %tmp_28_47 = fdiv float %tmp_27_47, %tmp_5
  %tmp_29_47 = fsub float %temp_4_47, %tmp_28_47
  %tmp_30_47 = zext i14 %c_47 to i64
  %C_addr_51 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_47
  %C_load_210 = load float* %C_addr_51, align 4
  %tmp_31_47 = fadd float %C_load_210, %tmp_29_47
  store float %tmp_31_47, float* %C_addr_51, align 4
  %Q_addr_51 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_47
  %Q_load_210 = load float* %Q_addr_51, align 4
  %tmp_32_47 = fsub float %Q_load_210, %temp_4_47
  store float %tmp_32_47, float* %Q_addr_51, align 4
  %c_48 = add i14 %phi_mul, 49
  %Q_load_211 = load float* %Q_addr, align 4
  %Q_load_212 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10149), align 4
  %tmp_21_48 = fmul float %Q_load_211, %Q_load_212
  %temp_4_48 = fdiv float %tmp_21_48, %qStar
  %C_load_211 = load float* %C_addr, align 4
  %C_load_212 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10149), align 4
  %tmp_22_48 = fmul float %C_load_211, %C_load_212
  %tmp_23_48 = fmul float %C_load_211, %Q_load_212
  %tmp_24_48 = fadd float %tmp_22_48, %tmp_23_48
  %tmp_25_48 = fmul float %Q_load_211, %C_load_212
  %tmp_26_48 = fadd float %tmp_24_48, %tmp_25_48
  %tmp_27_48 = fadd float %tmp_26_48, %tmp_21_48
  %tmp_28_48 = fdiv float %tmp_27_48, %tmp_5
  %tmp_29_48 = fsub float %temp_4_48, %tmp_28_48
  %tmp_30_48 = zext i14 %c_48 to i64
  %C_addr_52 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_48
  %C_load_213 = load float* %C_addr_52, align 4
  %tmp_31_48 = fadd float %C_load_213, %tmp_29_48
  store float %tmp_31_48, float* %C_addr_52, align 4
  %Q_addr_52 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_48
  %Q_load_213 = load float* %Q_addr_52, align 4
  %tmp_32_48 = fsub float %Q_load_213, %temp_4_48
  store float %tmp_32_48, float* %Q_addr_52, align 4
  %c_49 = add i14 %phi_mul, 50
  %Q_load_214 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10150), align 8
  %tmp_21_49 = fmul float %Q_load_211, %Q_load_214
  %temp_4_49 = fdiv float %tmp_21_49, %qStar
  %C_load_214 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10150), align 8
  %tmp_22_49 = fmul float %C_load_211, %C_load_214
  %tmp_23_49 = fmul float %C_load_211, %Q_load_214
  %tmp_24_49 = fadd float %tmp_22_49, %tmp_23_49
  %tmp_25_49 = fmul float %Q_load_211, %C_load_214
  %tmp_26_49 = fadd float %tmp_24_49, %tmp_25_49
  %tmp_27_49 = fadd float %tmp_26_49, %tmp_21_49
  %tmp_28_49 = fdiv float %tmp_27_49, %tmp_5
  %tmp_29_49 = fsub float %temp_4_49, %tmp_28_49
  %tmp_30_49 = zext i14 %c_49 to i64
  %C_addr_53 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_49
  %C_load_215 = load float* %C_addr_53, align 4
  %tmp_31_49 = fadd float %C_load_215, %tmp_29_49
  store float %tmp_31_49, float* %C_addr_53, align 4
  %Q_addr_53 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_49
  %Q_load_215 = load float* %Q_addr_53, align 4
  %tmp_32_49 = fsub float %Q_load_215, %temp_4_49
  store float %tmp_32_49, float* %Q_addr_53, align 4
  %c_50 = add i14 %phi_mul, 51
  %Q_load_216 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10151), align 4
  %tmp_21_50 = fmul float %Q_load_211, %Q_load_216
  %temp_4_50 = fdiv float %tmp_21_50, %qStar
  %C_load_216 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10151), align 4
  %tmp_22_50 = fmul float %C_load_211, %C_load_216
  %tmp_23_50 = fmul float %C_load_211, %Q_load_216
  %tmp_24_50 = fadd float %tmp_22_50, %tmp_23_50
  %tmp_25_50 = fmul float %Q_load_211, %C_load_216
  %tmp_26_50 = fadd float %tmp_24_50, %tmp_25_50
  %tmp_27_50 = fadd float %tmp_26_50, %tmp_21_50
  %tmp_28_50 = fdiv float %tmp_27_50, %tmp_5
  %tmp_29_50 = fsub float %temp_4_50, %tmp_28_50
  %tmp_30_50 = zext i14 %c_50 to i64
  %C_addr_54 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_50
  %C_load_217 = load float* %C_addr_54, align 4
  %tmp_31_50 = fadd float %C_load_217, %tmp_29_50
  store float %tmp_31_50, float* %C_addr_54, align 4
  %Q_addr_54 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_50
  %Q_load_217 = load float* %Q_addr_54, align 4
  %tmp_32_50 = fsub float %Q_load_217, %temp_4_50
  store float %tmp_32_50, float* %Q_addr_54, align 4
  %c_51 = add i14 %phi_mul, 52
  %Q_load_218 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10152), align 16
  %tmp_21_51 = fmul float %Q_load_211, %Q_load_218
  %temp_4_51 = fdiv float %tmp_21_51, %qStar
  %C_load_218 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10152), align 16
  %tmp_22_51 = fmul float %C_load_211, %C_load_218
  %tmp_23_51 = fmul float %C_load_211, %Q_load_218
  %tmp_24_51 = fadd float %tmp_22_51, %tmp_23_51
  %tmp_25_51 = fmul float %Q_load_211, %C_load_218
  %tmp_26_51 = fadd float %tmp_24_51, %tmp_25_51
  %tmp_27_51 = fadd float %tmp_26_51, %tmp_21_51
  %tmp_28_51 = fdiv float %tmp_27_51, %tmp_5
  %tmp_29_51 = fsub float %temp_4_51, %tmp_28_51
  %tmp_30_51 = zext i14 %c_51 to i64
  %C_addr_55 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_51
  %C_load_219 = load float* %C_addr_55, align 4
  %tmp_31_51 = fadd float %C_load_219, %tmp_29_51
  store float %tmp_31_51, float* %C_addr_55, align 4
  %Q_addr_55 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_51
  %Q_load_219 = load float* %Q_addr_55, align 4
  %tmp_32_51 = fsub float %Q_load_219, %temp_4_51
  store float %tmp_32_51, float* %Q_addr_55, align 4
  %c_52 = add i14 %phi_mul, 53
  %Q_load_220 = load float* %Q_addr, align 4
  %Q_load_221 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10153), align 4
  %tmp_21_52 = fmul float %Q_load_220, %Q_load_221
  %temp_4_52 = fdiv float %tmp_21_52, %qStar
  %C_load_220 = load float* %C_addr, align 4
  %C_load_221 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10153), align 4
  %tmp_22_52 = fmul float %C_load_220, %C_load_221
  %tmp_23_52 = fmul float %C_load_220, %Q_load_221
  %tmp_24_52 = fadd float %tmp_22_52, %tmp_23_52
  %tmp_25_52 = fmul float %Q_load_220, %C_load_221
  %tmp_26_52 = fadd float %tmp_24_52, %tmp_25_52
  %tmp_27_52 = fadd float %tmp_26_52, %tmp_21_52
  %tmp_28_52 = fdiv float %tmp_27_52, %tmp_5
  %tmp_29_52 = fsub float %temp_4_52, %tmp_28_52
  %tmp_30_52 = zext i14 %c_52 to i64
  %C_addr_56 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_52
  %C_load_222 = load float* %C_addr_56, align 4
  %tmp_31_52 = fadd float %C_load_222, %tmp_29_52
  store float %tmp_31_52, float* %C_addr_56, align 4
  %Q_addr_56 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_52
  %Q_load_222 = load float* %Q_addr_56, align 4
  %tmp_32_52 = fsub float %Q_load_222, %temp_4_52
  store float %tmp_32_52, float* %Q_addr_56, align 4
  %c_53 = add i14 %phi_mul, 54
  %Q_load_223 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10154), align 8
  %tmp_21_53 = fmul float %Q_load_220, %Q_load_223
  %temp_4_53 = fdiv float %tmp_21_53, %qStar
  %C_load_223 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10154), align 8
  %tmp_22_53 = fmul float %C_load_220, %C_load_223
  %tmp_23_53 = fmul float %C_load_220, %Q_load_223
  %tmp_24_53 = fadd float %tmp_22_53, %tmp_23_53
  %tmp_25_53 = fmul float %Q_load_220, %C_load_223
  %tmp_26_53 = fadd float %tmp_24_53, %tmp_25_53
  %tmp_27_53 = fadd float %tmp_26_53, %tmp_21_53
  %tmp_28_53 = fdiv float %tmp_27_53, %tmp_5
  %tmp_29_53 = fsub float %temp_4_53, %tmp_28_53
  %tmp_30_53 = zext i14 %c_53 to i64
  %C_addr_57 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_53
  %C_load_224 = load float* %C_addr_57, align 4
  %tmp_31_53 = fadd float %C_load_224, %tmp_29_53
  store float %tmp_31_53, float* %C_addr_57, align 4
  %Q_addr_57 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_53
  %Q_load_224 = load float* %Q_addr_57, align 4
  %tmp_32_53 = fsub float %Q_load_224, %temp_4_53
  store float %tmp_32_53, float* %Q_addr_57, align 4
  %c_54 = add i14 %phi_mul, 55
  %Q_load_225 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10155), align 4
  %tmp_21_54 = fmul float %Q_load_220, %Q_load_225
  %temp_4_54 = fdiv float %tmp_21_54, %qStar
  %C_load_225 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10155), align 4
  %tmp_22_54 = fmul float %C_load_220, %C_load_225
  %tmp_23_54 = fmul float %C_load_220, %Q_load_225
  %tmp_24_54 = fadd float %tmp_22_54, %tmp_23_54
  %tmp_25_54 = fmul float %Q_load_220, %C_load_225
  %tmp_26_54 = fadd float %tmp_24_54, %tmp_25_54
  %tmp_27_54 = fadd float %tmp_26_54, %tmp_21_54
  %tmp_28_54 = fdiv float %tmp_27_54, %tmp_5
  %tmp_29_54 = fsub float %temp_4_54, %tmp_28_54
  %tmp_30_54 = zext i14 %c_54 to i64
  %C_addr_58 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_54
  %C_load_226 = load float* %C_addr_58, align 4
  %tmp_31_54 = fadd float %C_load_226, %tmp_29_54
  store float %tmp_31_54, float* %C_addr_58, align 4
  %Q_addr_58 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_54
  %Q_load_226 = load float* %Q_addr_58, align 4
  %tmp_32_54 = fsub float %Q_load_226, %temp_4_54
  store float %tmp_32_54, float* %Q_addr_58, align 4
  %c_55 = add i14 %phi_mul, 56
  %Q_load_227 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10156), align 16
  %tmp_21_55 = fmul float %Q_load_220, %Q_load_227
  %temp_4_55 = fdiv float %tmp_21_55, %qStar
  %C_load_227 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10156), align 16
  %tmp_22_55 = fmul float %C_load_220, %C_load_227
  %tmp_23_55 = fmul float %C_load_220, %Q_load_227
  %tmp_24_55 = fadd float %tmp_22_55, %tmp_23_55
  %tmp_25_55 = fmul float %Q_load_220, %C_load_227
  %tmp_26_55 = fadd float %tmp_24_55, %tmp_25_55
  %tmp_27_55 = fadd float %tmp_26_55, %tmp_21_55
  %tmp_28_55 = fdiv float %tmp_27_55, %tmp_5
  %tmp_29_55 = fsub float %temp_4_55, %tmp_28_55
  %tmp_30_55 = zext i14 %c_55 to i64
  %C_addr_59 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_55
  %C_load_228 = load float* %C_addr_59, align 4
  %tmp_31_55 = fadd float %C_load_228, %tmp_29_55
  store float %tmp_31_55, float* %C_addr_59, align 4
  %Q_addr_59 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_55
  %Q_load_228 = load float* %Q_addr_59, align 4
  %tmp_32_55 = fsub float %Q_load_228, %temp_4_55
  store float %tmp_32_55, float* %Q_addr_59, align 4
  %c_56 = add i14 %phi_mul, 57
  %Q_load_229 = load float* %Q_addr, align 4
  %Q_load_230 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10157), align 4
  %tmp_21_56 = fmul float %Q_load_229, %Q_load_230
  %temp_4_56 = fdiv float %tmp_21_56, %qStar
  %C_load_229 = load float* %C_addr, align 4
  %C_load_230 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10157), align 4
  %tmp_22_56 = fmul float %C_load_229, %C_load_230
  %tmp_23_56 = fmul float %C_load_229, %Q_load_230
  %tmp_24_56 = fadd float %tmp_22_56, %tmp_23_56
  %tmp_25_56 = fmul float %Q_load_229, %C_load_230
  %tmp_26_56 = fadd float %tmp_24_56, %tmp_25_56
  %tmp_27_56 = fadd float %tmp_26_56, %tmp_21_56
  %tmp_28_56 = fdiv float %tmp_27_56, %tmp_5
  %tmp_29_56 = fsub float %temp_4_56, %tmp_28_56
  %tmp_30_56 = zext i14 %c_56 to i64
  %C_addr_60 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_56
  %C_load_231 = load float* %C_addr_60, align 4
  %tmp_31_56 = fadd float %C_load_231, %tmp_29_56
  store float %tmp_31_56, float* %C_addr_60, align 4
  %Q_addr_60 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_56
  %Q_load_231 = load float* %Q_addr_60, align 4
  %tmp_32_56 = fsub float %Q_load_231, %temp_4_56
  store float %tmp_32_56, float* %Q_addr_60, align 4
  %c_57 = add i14 %phi_mul, 58
  %Q_load_232 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10158), align 8
  %tmp_21_57 = fmul float %Q_load_229, %Q_load_232
  %temp_4_57 = fdiv float %tmp_21_57, %qStar
  %C_load_232 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10158), align 8
  %tmp_22_57 = fmul float %C_load_229, %C_load_232
  %tmp_23_57 = fmul float %C_load_229, %Q_load_232
  %tmp_24_57 = fadd float %tmp_22_57, %tmp_23_57
  %tmp_25_57 = fmul float %Q_load_229, %C_load_232
  %tmp_26_57 = fadd float %tmp_24_57, %tmp_25_57
  %tmp_27_57 = fadd float %tmp_26_57, %tmp_21_57
  %tmp_28_57 = fdiv float %tmp_27_57, %tmp_5
  %tmp_29_57 = fsub float %temp_4_57, %tmp_28_57
  %tmp_30_57 = zext i14 %c_57 to i64
  %C_addr_61 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_57
  %C_load_233 = load float* %C_addr_61, align 4
  %tmp_31_57 = fadd float %C_load_233, %tmp_29_57
  store float %tmp_31_57, float* %C_addr_61, align 4
  %Q_addr_61 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_57
  %Q_load_233 = load float* %Q_addr_61, align 4
  %tmp_32_57 = fsub float %Q_load_233, %temp_4_57
  store float %tmp_32_57, float* %Q_addr_61, align 4
  %c_58 = add i14 %phi_mul, 59
  %Q_load_234 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10159), align 4
  %tmp_21_58 = fmul float %Q_load_229, %Q_load_234
  %temp_4_58 = fdiv float %tmp_21_58, %qStar
  %C_load_234 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10159), align 4
  %tmp_22_58 = fmul float %C_load_229, %C_load_234
  %tmp_23_58 = fmul float %C_load_229, %Q_load_234
  %tmp_24_58 = fadd float %tmp_22_58, %tmp_23_58
  %tmp_25_58 = fmul float %Q_load_229, %C_load_234
  %tmp_26_58 = fadd float %tmp_24_58, %tmp_25_58
  %tmp_27_58 = fadd float %tmp_26_58, %tmp_21_58
  %tmp_28_58 = fdiv float %tmp_27_58, %tmp_5
  %tmp_29_58 = fsub float %temp_4_58, %tmp_28_58
  %tmp_30_58 = zext i14 %c_58 to i64
  %C_addr_62 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_58
  %C_load_235 = load float* %C_addr_62, align 4
  %tmp_31_58 = fadd float %C_load_235, %tmp_29_58
  store float %tmp_31_58, float* %C_addr_62, align 4
  %Q_addr_62 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_58
  %Q_load_235 = load float* %Q_addr_62, align 4
  %tmp_32_58 = fsub float %Q_load_235, %temp_4_58
  store float %tmp_32_58, float* %Q_addr_62, align 4
  %c_59 = add i14 %phi_mul, 60
  %Q_load_236 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10160), align 16
  %tmp_21_59 = fmul float %Q_load_229, %Q_load_236
  %temp_4_59 = fdiv float %tmp_21_59, %qStar
  %C_load_236 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10160), align 16
  %tmp_22_59 = fmul float %C_load_229, %C_load_236
  %tmp_23_59 = fmul float %C_load_229, %Q_load_236
  %tmp_24_59 = fadd float %tmp_22_59, %tmp_23_59
  %tmp_25_59 = fmul float %Q_load_229, %C_load_236
  %tmp_26_59 = fadd float %tmp_24_59, %tmp_25_59
  %tmp_27_59 = fadd float %tmp_26_59, %tmp_21_59
  %tmp_28_59 = fdiv float %tmp_27_59, %tmp_5
  %tmp_29_59 = fsub float %temp_4_59, %tmp_28_59
  %tmp_30_59 = zext i14 %c_59 to i64
  %C_addr_63 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_59
  %C_load_237 = load float* %C_addr_63, align 4
  %tmp_31_59 = fadd float %C_load_237, %tmp_29_59
  store float %tmp_31_59, float* %C_addr_63, align 4
  %Q_addr_63 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_59
  %Q_load_237 = load float* %Q_addr_63, align 4
  %tmp_32_59 = fsub float %Q_load_237, %temp_4_59
  store float %tmp_32_59, float* %Q_addr_63, align 4
  %c_60 = add i14 %phi_mul, 61
  %Q_load_238 = load float* %Q_addr, align 4
  %Q_load_239 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10161), align 4
  %tmp_21_60 = fmul float %Q_load_238, %Q_load_239
  %temp_4_60 = fdiv float %tmp_21_60, %qStar
  %C_load_238 = load float* %C_addr, align 4
  %C_load_239 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10161), align 4
  %tmp_22_60 = fmul float %C_load_238, %C_load_239
  %tmp_23_60 = fmul float %C_load_238, %Q_load_239
  %tmp_24_60 = fadd float %tmp_22_60, %tmp_23_60
  %tmp_25_60 = fmul float %Q_load_238, %C_load_239
  %tmp_26_60 = fadd float %tmp_24_60, %tmp_25_60
  %tmp_27_60 = fadd float %tmp_26_60, %tmp_21_60
  %tmp_28_60 = fdiv float %tmp_27_60, %tmp_5
  %tmp_29_60 = fsub float %temp_4_60, %tmp_28_60
  %tmp_30_60 = zext i14 %c_60 to i64
  %C_addr_64 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_60
  %C_load_240 = load float* %C_addr_64, align 4
  %tmp_31_60 = fadd float %C_load_240, %tmp_29_60
  store float %tmp_31_60, float* %C_addr_64, align 4
  %Q_addr_64 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_60
  %Q_load_240 = load float* %Q_addr_64, align 4
  %tmp_32_60 = fsub float %Q_load_240, %temp_4_60
  store float %tmp_32_60, float* %Q_addr_64, align 4
  %c_61 = add i14 %phi_mul, 62
  %Q_load_241 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10162), align 8
  %tmp_21_61 = fmul float %Q_load_238, %Q_load_241
  %temp_4_61 = fdiv float %tmp_21_61, %qStar
  %C_load_241 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10162), align 8
  %tmp_22_61 = fmul float %C_load_238, %C_load_241
  %tmp_23_61 = fmul float %C_load_238, %Q_load_241
  %tmp_24_61 = fadd float %tmp_22_61, %tmp_23_61
  %tmp_25_61 = fmul float %Q_load_238, %C_load_241
  %tmp_26_61 = fadd float %tmp_24_61, %tmp_25_61
  %tmp_27_61 = fadd float %tmp_26_61, %tmp_21_61
  %tmp_28_61 = fdiv float %tmp_27_61, %tmp_5
  %tmp_29_61 = fsub float %temp_4_61, %tmp_28_61
  %tmp_30_61 = zext i14 %c_61 to i64
  %C_addr_65 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_61
  %C_load_242 = load float* %C_addr_65, align 4
  %tmp_31_61 = fadd float %C_load_242, %tmp_29_61
  store float %tmp_31_61, float* %C_addr_65, align 4
  %Q_addr_65 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_61
  %Q_load_242 = load float* %Q_addr_65, align 4
  %tmp_32_61 = fsub float %Q_load_242, %temp_4_61
  store float %tmp_32_61, float* %Q_addr_65, align 4
  %c_62 = add i14 %phi_mul, 63
  %Q_load_243 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10163), align 4
  %tmp_21_62 = fmul float %Q_load_238, %Q_load_243
  %temp_4_62 = fdiv float %tmp_21_62, %qStar
  %C_load_243 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10163), align 4
  %tmp_22_62 = fmul float %C_load_238, %C_load_243
  %tmp_23_62 = fmul float %C_load_238, %Q_load_243
  %tmp_24_62 = fadd float %tmp_22_62, %tmp_23_62
  %tmp_25_62 = fmul float %Q_load_238, %C_load_243
  %tmp_26_62 = fadd float %tmp_24_62, %tmp_25_62
  %tmp_27_62 = fadd float %tmp_26_62, %tmp_21_62
  %tmp_28_62 = fdiv float %tmp_27_62, %tmp_5
  %tmp_29_62 = fsub float %temp_4_62, %tmp_28_62
  %tmp_30_62 = zext i14 %c_62 to i64
  %C_addr_66 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_62
  %C_load_244 = load float* %C_addr_66, align 4
  %tmp_31_62 = fadd float %C_load_244, %tmp_29_62
  store float %tmp_31_62, float* %C_addr_66, align 4
  %Q_addr_66 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_62
  %Q_load_244 = load float* %Q_addr_66, align 4
  %tmp_32_62 = fsub float %Q_load_244, %temp_4_62
  store float %tmp_32_62, float* %Q_addr_66, align 4
  %c_63 = add i14 %phi_mul, 64
  %Q_load_245 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10164), align 16
  %tmp_21_63 = fmul float %Q_load_238, %Q_load_245
  %temp_4_63 = fdiv float %tmp_21_63, %qStar
  %C_load_245 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10164), align 16
  %tmp_22_63 = fmul float %C_load_238, %C_load_245
  %tmp_23_63 = fmul float %C_load_238, %Q_load_245
  %tmp_24_63 = fadd float %tmp_22_63, %tmp_23_63
  %tmp_25_63 = fmul float %Q_load_238, %C_load_245
  %tmp_26_63 = fadd float %tmp_24_63, %tmp_25_63
  %tmp_27_63 = fadd float %tmp_26_63, %tmp_21_63
  %tmp_28_63 = fdiv float %tmp_27_63, %tmp_5
  %tmp_29_63 = fsub float %temp_4_63, %tmp_28_63
  %tmp_30_63 = zext i14 %c_63 to i64
  %C_addr_67 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_63
  %C_load_246 = load float* %C_addr_67, align 4
  %tmp_31_63 = fadd float %C_load_246, %tmp_29_63
  store float %tmp_31_63, float* %C_addr_67, align 4
  %Q_addr_67 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_63
  %Q_load_246 = load float* %Q_addr_67, align 4
  %tmp_32_63 = fsub float %Q_load_246, %temp_4_63
  store float %tmp_32_63, float* %Q_addr_67, align 4
  %c_64 = add i14 %phi_mul, 65
  %Q_load_247 = load float* %Q_addr, align 4
  %Q_load_248 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10165), align 4
  %tmp_21_64 = fmul float %Q_load_247, %Q_load_248
  %temp_4_64 = fdiv float %tmp_21_64, %qStar
  %C_load_247 = load float* %C_addr, align 4
  %C_load_248 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10165), align 4
  %tmp_22_64 = fmul float %C_load_247, %C_load_248
  %tmp_23_64 = fmul float %C_load_247, %Q_load_248
  %tmp_24_64 = fadd float %tmp_22_64, %tmp_23_64
  %tmp_25_64 = fmul float %Q_load_247, %C_load_248
  %tmp_26_64 = fadd float %tmp_24_64, %tmp_25_64
  %tmp_27_64 = fadd float %tmp_26_64, %tmp_21_64
  %tmp_28_64 = fdiv float %tmp_27_64, %tmp_5
  %tmp_29_64 = fsub float %temp_4_64, %tmp_28_64
  %tmp_30_64 = zext i14 %c_64 to i64
  %C_addr_68 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_64
  %C_load_249 = load float* %C_addr_68, align 4
  %tmp_31_64 = fadd float %C_load_249, %tmp_29_64
  store float %tmp_31_64, float* %C_addr_68, align 4
  %Q_addr_68 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_64
  %Q_load_249 = load float* %Q_addr_68, align 4
  %tmp_32_64 = fsub float %Q_load_249, %temp_4_64
  store float %tmp_32_64, float* %Q_addr_68, align 4
  %c_65 = add i14 %phi_mul, 66
  %Q_load_250 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10166), align 8
  %tmp_21_65 = fmul float %Q_load_247, %Q_load_250
  %temp_4_65 = fdiv float %tmp_21_65, %qStar
  %C_load_250 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10166), align 8
  %tmp_22_65 = fmul float %C_load_247, %C_load_250
  %tmp_23_65 = fmul float %C_load_247, %Q_load_250
  %tmp_24_65 = fadd float %tmp_22_65, %tmp_23_65
  %tmp_25_65 = fmul float %Q_load_247, %C_load_250
  %tmp_26_65 = fadd float %tmp_24_65, %tmp_25_65
  %tmp_27_65 = fadd float %tmp_26_65, %tmp_21_65
  %tmp_28_65 = fdiv float %tmp_27_65, %tmp_5
  %tmp_29_65 = fsub float %temp_4_65, %tmp_28_65
  %tmp_30_65 = zext i14 %c_65 to i64
  %C_addr_69 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_65
  %C_load_251 = load float* %C_addr_69, align 4
  %tmp_31_65 = fadd float %C_load_251, %tmp_29_65
  store float %tmp_31_65, float* %C_addr_69, align 4
  %Q_addr_69 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_65
  %Q_load_251 = load float* %Q_addr_69, align 4
  %tmp_32_65 = fsub float %Q_load_251, %temp_4_65
  store float %tmp_32_65, float* %Q_addr_69, align 4
  %c_66 = add i14 %phi_mul, 67
  %Q_load_252 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10167), align 4
  %tmp_21_66 = fmul float %Q_load_247, %Q_load_252
  %temp_4_66 = fdiv float %tmp_21_66, %qStar
  %C_load_252 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10167), align 4
  %tmp_22_66 = fmul float %C_load_247, %C_load_252
  %tmp_23_66 = fmul float %C_load_247, %Q_load_252
  %tmp_24_66 = fadd float %tmp_22_66, %tmp_23_66
  %tmp_25_66 = fmul float %Q_load_247, %C_load_252
  %tmp_26_66 = fadd float %tmp_24_66, %tmp_25_66
  %tmp_27_66 = fadd float %tmp_26_66, %tmp_21_66
  %tmp_28_66 = fdiv float %tmp_27_66, %tmp_5
  %tmp_29_66 = fsub float %temp_4_66, %tmp_28_66
  %tmp_30_66 = zext i14 %c_66 to i64
  %C_addr_70 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_66
  %C_load_253 = load float* %C_addr_70, align 4
  %tmp_31_66 = fadd float %C_load_253, %tmp_29_66
  store float %tmp_31_66, float* %C_addr_70, align 4
  %Q_addr_70 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_66
  %Q_load_253 = load float* %Q_addr_70, align 4
  %tmp_32_66 = fsub float %Q_load_253, %temp_4_66
  store float %tmp_32_66, float* %Q_addr_70, align 4
  %c_67 = add i14 %phi_mul, 68
  %Q_load_254 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10168), align 16
  %tmp_21_67 = fmul float %Q_load_247, %Q_load_254
  %temp_4_67 = fdiv float %tmp_21_67, %qStar
  %C_load_254 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10168), align 16
  %tmp_22_67 = fmul float %C_load_247, %C_load_254
  %tmp_23_67 = fmul float %C_load_247, %Q_load_254
  %tmp_24_67 = fadd float %tmp_22_67, %tmp_23_67
  %tmp_25_67 = fmul float %Q_load_247, %C_load_254
  %tmp_26_67 = fadd float %tmp_24_67, %tmp_25_67
  %tmp_27_67 = fadd float %tmp_26_67, %tmp_21_67
  %tmp_28_67 = fdiv float %tmp_27_67, %tmp_5
  %tmp_29_67 = fsub float %temp_4_67, %tmp_28_67
  %tmp_30_67 = zext i14 %c_67 to i64
  %C_addr_71 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_67
  %C_load_255 = load float* %C_addr_71, align 4
  %tmp_31_67 = fadd float %C_load_255, %tmp_29_67
  store float %tmp_31_67, float* %C_addr_71, align 4
  %Q_addr_71 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_67
  %Q_load_255 = load float* %Q_addr_71, align 4
  %tmp_32_67 = fsub float %Q_load_255, %temp_4_67
  store float %tmp_32_67, float* %Q_addr_71, align 4
  %c_68 = add i14 %phi_mul, 69
  %Q_load_256 = load float* %Q_addr, align 4
  %Q_load_257 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10169), align 4
  %tmp_21_68 = fmul float %Q_load_256, %Q_load_257
  %temp_4_68 = fdiv float %tmp_21_68, %qStar
  %C_load_256 = load float* %C_addr, align 4
  %C_load_257 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10169), align 4
  %tmp_22_68 = fmul float %C_load_256, %C_load_257
  %tmp_23_68 = fmul float %C_load_256, %Q_load_257
  %tmp_24_68 = fadd float %tmp_22_68, %tmp_23_68
  %tmp_25_68 = fmul float %Q_load_256, %C_load_257
  %tmp_26_68 = fadd float %tmp_24_68, %tmp_25_68
  %tmp_27_68 = fadd float %tmp_26_68, %tmp_21_68
  %tmp_28_68 = fdiv float %tmp_27_68, %tmp_5
  %tmp_29_68 = fsub float %temp_4_68, %tmp_28_68
  %tmp_30_68 = zext i14 %c_68 to i64
  %C_addr_72 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_68
  %C_load_258 = load float* %C_addr_72, align 4
  %tmp_31_68 = fadd float %C_load_258, %tmp_29_68
  store float %tmp_31_68, float* %C_addr_72, align 4
  %Q_addr_72 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_68
  %Q_load_258 = load float* %Q_addr_72, align 4
  %tmp_32_68 = fsub float %Q_load_258, %temp_4_68
  store float %tmp_32_68, float* %Q_addr_72, align 4
  %c_69 = add i14 %phi_mul, 70
  %Q_load_259 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10170), align 8
  %tmp_21_69 = fmul float %Q_load_256, %Q_load_259
  %temp_4_69 = fdiv float %tmp_21_69, %qStar
  %C_load_259 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10170), align 8
  %tmp_22_69 = fmul float %C_load_256, %C_load_259
  %tmp_23_69 = fmul float %C_load_256, %Q_load_259
  %tmp_24_69 = fadd float %tmp_22_69, %tmp_23_69
  %tmp_25_69 = fmul float %Q_load_256, %C_load_259
  %tmp_26_69 = fadd float %tmp_24_69, %tmp_25_69
  %tmp_27_69 = fadd float %tmp_26_69, %tmp_21_69
  %tmp_28_69 = fdiv float %tmp_27_69, %tmp_5
  %tmp_29_69 = fsub float %temp_4_69, %tmp_28_69
  %tmp_30_69 = zext i14 %c_69 to i64
  %C_addr_73 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_69
  %C_load_260 = load float* %C_addr_73, align 4
  %tmp_31_69 = fadd float %C_load_260, %tmp_29_69
  store float %tmp_31_69, float* %C_addr_73, align 4
  %Q_addr_73 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_69
  %Q_load_260 = load float* %Q_addr_73, align 4
  %tmp_32_69 = fsub float %Q_load_260, %temp_4_69
  store float %tmp_32_69, float* %Q_addr_73, align 4
  %c_70 = add i14 %phi_mul, 71
  %Q_load_261 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10171), align 4
  %tmp_21_70 = fmul float %Q_load_256, %Q_load_261
  %temp_4_70 = fdiv float %tmp_21_70, %qStar
  %C_load_261 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10171), align 4
  %tmp_22_70 = fmul float %C_load_256, %C_load_261
  %tmp_23_70 = fmul float %C_load_256, %Q_load_261
  %tmp_24_70 = fadd float %tmp_22_70, %tmp_23_70
  %tmp_25_70 = fmul float %Q_load_256, %C_load_261
  %tmp_26_70 = fadd float %tmp_24_70, %tmp_25_70
  %tmp_27_70 = fadd float %tmp_26_70, %tmp_21_70
  %tmp_28_70 = fdiv float %tmp_27_70, %tmp_5
  %tmp_29_70 = fsub float %temp_4_70, %tmp_28_70
  %tmp_30_70 = zext i14 %c_70 to i64
  %C_addr_74 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_70
  %C_load_262 = load float* %C_addr_74, align 4
  %tmp_31_70 = fadd float %C_load_262, %tmp_29_70
  store float %tmp_31_70, float* %C_addr_74, align 4
  %Q_addr_74 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_70
  %Q_load_262 = load float* %Q_addr_74, align 4
  %tmp_32_70 = fsub float %Q_load_262, %temp_4_70
  store float %tmp_32_70, float* %Q_addr_74, align 4
  %c_71 = add i14 %phi_mul, 72
  %Q_load_263 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10172), align 16
  %tmp_21_71 = fmul float %Q_load_256, %Q_load_263
  %temp_4_71 = fdiv float %tmp_21_71, %qStar
  %C_load_263 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10172), align 16
  %tmp_22_71 = fmul float %C_load_256, %C_load_263
  %tmp_23_71 = fmul float %C_load_256, %Q_load_263
  %tmp_24_71 = fadd float %tmp_22_71, %tmp_23_71
  %tmp_25_71 = fmul float %Q_load_256, %C_load_263
  %tmp_26_71 = fadd float %tmp_24_71, %tmp_25_71
  %tmp_27_71 = fadd float %tmp_26_71, %tmp_21_71
  %tmp_28_71 = fdiv float %tmp_27_71, %tmp_5
  %tmp_29_71 = fsub float %temp_4_71, %tmp_28_71
  %tmp_30_71 = zext i14 %c_71 to i64
  %C_addr_75 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_71
  %C_load_264 = load float* %C_addr_75, align 4
  %tmp_31_71 = fadd float %C_load_264, %tmp_29_71
  store float %tmp_31_71, float* %C_addr_75, align 4
  %Q_addr_75 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_71
  %Q_load_264 = load float* %Q_addr_75, align 4
  %tmp_32_71 = fsub float %Q_load_264, %temp_4_71
  store float %tmp_32_71, float* %Q_addr_75, align 4
  %c_72 = add i14 %phi_mul, 73
  %Q_load_265 = load float* %Q_addr, align 4
  %Q_load_266 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10173), align 4
  %tmp_21_72 = fmul float %Q_load_265, %Q_load_266
  %temp_4_72 = fdiv float %tmp_21_72, %qStar
  %C_load_265 = load float* %C_addr, align 4
  %C_load_266 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10173), align 4
  %tmp_22_72 = fmul float %C_load_265, %C_load_266
  %tmp_23_72 = fmul float %C_load_265, %Q_load_266
  %tmp_24_72 = fadd float %tmp_22_72, %tmp_23_72
  %tmp_25_72 = fmul float %Q_load_265, %C_load_266
  %tmp_26_72 = fadd float %tmp_24_72, %tmp_25_72
  %tmp_27_72 = fadd float %tmp_26_72, %tmp_21_72
  %tmp_28_72 = fdiv float %tmp_27_72, %tmp_5
  %tmp_29_72 = fsub float %temp_4_72, %tmp_28_72
  %tmp_30_72 = zext i14 %c_72 to i64
  %C_addr_76 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_72
  %C_load_267 = load float* %C_addr_76, align 4
  %tmp_31_72 = fadd float %C_load_267, %tmp_29_72
  store float %tmp_31_72, float* %C_addr_76, align 4
  %Q_addr_76 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_72
  %Q_load_267 = load float* %Q_addr_76, align 4
  %tmp_32_72 = fsub float %Q_load_267, %temp_4_72
  store float %tmp_32_72, float* %Q_addr_76, align 4
  %c_73 = add i14 %phi_mul, 74
  %Q_load_268 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10174), align 8
  %tmp_21_73 = fmul float %Q_load_265, %Q_load_268
  %temp_4_73 = fdiv float %tmp_21_73, %qStar
  %C_load_268 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10174), align 8
  %tmp_22_73 = fmul float %C_load_265, %C_load_268
  %tmp_23_73 = fmul float %C_load_265, %Q_load_268
  %tmp_24_73 = fadd float %tmp_22_73, %tmp_23_73
  %tmp_25_73 = fmul float %Q_load_265, %C_load_268
  %tmp_26_73 = fadd float %tmp_24_73, %tmp_25_73
  %tmp_27_73 = fadd float %tmp_26_73, %tmp_21_73
  %tmp_28_73 = fdiv float %tmp_27_73, %tmp_5
  %tmp_29_73 = fsub float %temp_4_73, %tmp_28_73
  %tmp_30_73 = zext i14 %c_73 to i64
  %C_addr_77 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_73
  %C_load_269 = load float* %C_addr_77, align 4
  %tmp_31_73 = fadd float %C_load_269, %tmp_29_73
  store float %tmp_31_73, float* %C_addr_77, align 4
  %Q_addr_77 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_73
  %Q_load_269 = load float* %Q_addr_77, align 4
  %tmp_32_73 = fsub float %Q_load_269, %temp_4_73
  store float %tmp_32_73, float* %Q_addr_77, align 4
  %c_74 = add i14 %phi_mul, 75
  %Q_load_270 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10175), align 4
  %tmp_21_74 = fmul float %Q_load_265, %Q_load_270
  %temp_4_74 = fdiv float %tmp_21_74, %qStar
  %C_load_270 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10175), align 4
  %tmp_22_74 = fmul float %C_load_265, %C_load_270
  %tmp_23_74 = fmul float %C_load_265, %Q_load_270
  %tmp_24_74 = fadd float %tmp_22_74, %tmp_23_74
  %tmp_25_74 = fmul float %Q_load_265, %C_load_270
  %tmp_26_74 = fadd float %tmp_24_74, %tmp_25_74
  %tmp_27_74 = fadd float %tmp_26_74, %tmp_21_74
  %tmp_28_74 = fdiv float %tmp_27_74, %tmp_5
  %tmp_29_74 = fsub float %temp_4_74, %tmp_28_74
  %tmp_30_74 = zext i14 %c_74 to i64
  %C_addr_78 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_74
  %C_load_271 = load float* %C_addr_78, align 4
  %tmp_31_74 = fadd float %C_load_271, %tmp_29_74
  store float %tmp_31_74, float* %C_addr_78, align 4
  %Q_addr_78 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_74
  %Q_load_271 = load float* %Q_addr_78, align 4
  %tmp_32_74 = fsub float %Q_load_271, %temp_4_74
  store float %tmp_32_74, float* %Q_addr_78, align 4
  %c_75 = add i14 %phi_mul, 76
  %Q_load_272 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10176), align 16
  %tmp_21_75 = fmul float %Q_load_265, %Q_load_272
  %temp_4_75 = fdiv float %tmp_21_75, %qStar
  %C_load_272 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10176), align 16
  %tmp_22_75 = fmul float %C_load_265, %C_load_272
  %tmp_23_75 = fmul float %C_load_265, %Q_load_272
  %tmp_24_75 = fadd float %tmp_22_75, %tmp_23_75
  %tmp_25_75 = fmul float %Q_load_265, %C_load_272
  %tmp_26_75 = fadd float %tmp_24_75, %tmp_25_75
  %tmp_27_75 = fadd float %tmp_26_75, %tmp_21_75
  %tmp_28_75 = fdiv float %tmp_27_75, %tmp_5
  %tmp_29_75 = fsub float %temp_4_75, %tmp_28_75
  %tmp_30_75 = zext i14 %c_75 to i64
  %C_addr_79 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_75
  %C_load_273 = load float* %C_addr_79, align 4
  %tmp_31_75 = fadd float %C_load_273, %tmp_29_75
  store float %tmp_31_75, float* %C_addr_79, align 4
  %Q_addr_79 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_75
  %Q_load_273 = load float* %Q_addr_79, align 4
  %tmp_32_75 = fsub float %Q_load_273, %temp_4_75
  store float %tmp_32_75, float* %Q_addr_79, align 4
  %c_76 = add i14 %phi_mul, 77
  %Q_load_274 = load float* %Q_addr, align 4
  %Q_load_275 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10177), align 4
  %tmp_21_76 = fmul float %Q_load_274, %Q_load_275
  %temp_4_76 = fdiv float %tmp_21_76, %qStar
  %C_load_274 = load float* %C_addr, align 4
  %C_load_275 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10177), align 4
  %tmp_22_76 = fmul float %C_load_274, %C_load_275
  %tmp_23_76 = fmul float %C_load_274, %Q_load_275
  %tmp_24_76 = fadd float %tmp_22_76, %tmp_23_76
  %tmp_25_76 = fmul float %Q_load_274, %C_load_275
  %tmp_26_76 = fadd float %tmp_24_76, %tmp_25_76
  %tmp_27_76 = fadd float %tmp_26_76, %tmp_21_76
  %tmp_28_76 = fdiv float %tmp_27_76, %tmp_5
  %tmp_29_76 = fsub float %temp_4_76, %tmp_28_76
  %tmp_30_76 = zext i14 %c_76 to i64
  %C_addr_80 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_76
  %C_load_276 = load float* %C_addr_80, align 4
  %tmp_31_76 = fadd float %C_load_276, %tmp_29_76
  store float %tmp_31_76, float* %C_addr_80, align 4
  %Q_addr_80 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_76
  %Q_load_276 = load float* %Q_addr_80, align 4
  %tmp_32_76 = fsub float %Q_load_276, %temp_4_76
  store float %tmp_32_76, float* %Q_addr_80, align 4
  %c_77 = add i14 %phi_mul, 78
  %Q_load_277 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10178), align 8
  %tmp_21_77 = fmul float %Q_load_274, %Q_load_277
  %temp_4_77 = fdiv float %tmp_21_77, %qStar
  %C_load_277 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10178), align 8
  %tmp_22_77 = fmul float %C_load_274, %C_load_277
  %tmp_23_77 = fmul float %C_load_274, %Q_load_277
  %tmp_24_77 = fadd float %tmp_22_77, %tmp_23_77
  %tmp_25_77 = fmul float %Q_load_274, %C_load_277
  %tmp_26_77 = fadd float %tmp_24_77, %tmp_25_77
  %tmp_27_77 = fadd float %tmp_26_77, %tmp_21_77
  %tmp_28_77 = fdiv float %tmp_27_77, %tmp_5
  %tmp_29_77 = fsub float %temp_4_77, %tmp_28_77
  %tmp_30_77 = zext i14 %c_77 to i64
  %C_addr_81 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_77
  %C_load_278 = load float* %C_addr_81, align 4
  %tmp_31_77 = fadd float %C_load_278, %tmp_29_77
  store float %tmp_31_77, float* %C_addr_81, align 4
  %Q_addr_81 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_77
  %Q_load_278 = load float* %Q_addr_81, align 4
  %tmp_32_77 = fsub float %Q_load_278, %temp_4_77
  store float %tmp_32_77, float* %Q_addr_81, align 4
  %c_78 = add i14 %phi_mul, 79
  %Q_load_279 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10179), align 4
  %tmp_21_78 = fmul float %Q_load_274, %Q_load_279
  %temp_4_78 = fdiv float %tmp_21_78, %qStar
  %C_load_279 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10179), align 4
  %tmp_22_78 = fmul float %C_load_274, %C_load_279
  %tmp_23_78 = fmul float %C_load_274, %Q_load_279
  %tmp_24_78 = fadd float %tmp_22_78, %tmp_23_78
  %tmp_25_78 = fmul float %Q_load_274, %C_load_279
  %tmp_26_78 = fadd float %tmp_24_78, %tmp_25_78
  %tmp_27_78 = fadd float %tmp_26_78, %tmp_21_78
  %tmp_28_78 = fdiv float %tmp_27_78, %tmp_5
  %tmp_29_78 = fsub float %temp_4_78, %tmp_28_78
  %tmp_30_78 = zext i14 %c_78 to i64
  %C_addr_82 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_78
  %C_load_280 = load float* %C_addr_82, align 4
  %tmp_31_78 = fadd float %C_load_280, %tmp_29_78
  store float %tmp_31_78, float* %C_addr_82, align 4
  %Q_addr_82 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_78
  %Q_load_280 = load float* %Q_addr_82, align 4
  %tmp_32_78 = fsub float %Q_load_280, %temp_4_78
  store float %tmp_32_78, float* %Q_addr_82, align 4
  %c_79 = add i14 %phi_mul, 80
  %Q_load_281 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10180), align 16
  %tmp_21_79 = fmul float %Q_load_274, %Q_load_281
  %temp_4_79 = fdiv float %tmp_21_79, %qStar
  %C_load_281 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10180), align 16
  %tmp_22_79 = fmul float %C_load_274, %C_load_281
  %tmp_23_79 = fmul float %C_load_274, %Q_load_281
  %tmp_24_79 = fadd float %tmp_22_79, %tmp_23_79
  %tmp_25_79 = fmul float %Q_load_274, %C_load_281
  %tmp_26_79 = fadd float %tmp_24_79, %tmp_25_79
  %tmp_27_79 = fadd float %tmp_26_79, %tmp_21_79
  %tmp_28_79 = fdiv float %tmp_27_79, %tmp_5
  %tmp_29_79 = fsub float %temp_4_79, %tmp_28_79
  %tmp_30_79 = zext i14 %c_79 to i64
  %C_addr_83 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_79
  %C_load_282 = load float* %C_addr_83, align 4
  %tmp_31_79 = fadd float %C_load_282, %tmp_29_79
  store float %tmp_31_79, float* %C_addr_83, align 4
  %Q_addr_83 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_79
  %Q_load_282 = load float* %Q_addr_83, align 4
  %tmp_32_79 = fsub float %Q_load_282, %temp_4_79
  store float %tmp_32_79, float* %Q_addr_83, align 4
  %c_80 = add i14 %phi_mul, 81
  %Q_load_283 = load float* %Q_addr, align 4
  %Q_load_284 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10181), align 4
  %tmp_21_80 = fmul float %Q_load_283, %Q_load_284
  %temp_4_80 = fdiv float %tmp_21_80, %qStar
  %C_load_283 = load float* %C_addr, align 4
  %C_load_284 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10181), align 4
  %tmp_22_80 = fmul float %C_load_283, %C_load_284
  %tmp_23_80 = fmul float %C_load_283, %Q_load_284
  %tmp_24_80 = fadd float %tmp_22_80, %tmp_23_80
  %tmp_25_80 = fmul float %Q_load_283, %C_load_284
  %tmp_26_80 = fadd float %tmp_24_80, %tmp_25_80
  %tmp_27_80 = fadd float %tmp_26_80, %tmp_21_80
  %tmp_28_80 = fdiv float %tmp_27_80, %tmp_5
  %tmp_29_80 = fsub float %temp_4_80, %tmp_28_80
  %tmp_30_80 = zext i14 %c_80 to i64
  %C_addr_84 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_80
  %C_load_285 = load float* %C_addr_84, align 4
  %tmp_31_80 = fadd float %C_load_285, %tmp_29_80
  store float %tmp_31_80, float* %C_addr_84, align 4
  %Q_addr_84 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_80
  %Q_load_285 = load float* %Q_addr_84, align 4
  %tmp_32_80 = fsub float %Q_load_285, %temp_4_80
  store float %tmp_32_80, float* %Q_addr_84, align 4
  %c_81 = add i14 %phi_mul, 82
  %Q_load_286 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10182), align 8
  %tmp_21_81 = fmul float %Q_load_283, %Q_load_286
  %temp_4_81 = fdiv float %tmp_21_81, %qStar
  %C_load_286 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10182), align 8
  %tmp_22_81 = fmul float %C_load_283, %C_load_286
  %tmp_23_81 = fmul float %C_load_283, %Q_load_286
  %tmp_24_81 = fadd float %tmp_22_81, %tmp_23_81
  %tmp_25_81 = fmul float %Q_load_283, %C_load_286
  %tmp_26_81 = fadd float %tmp_24_81, %tmp_25_81
  %tmp_27_81 = fadd float %tmp_26_81, %tmp_21_81
  %tmp_28_81 = fdiv float %tmp_27_81, %tmp_5
  %tmp_29_81 = fsub float %temp_4_81, %tmp_28_81
  %tmp_30_81 = zext i14 %c_81 to i64
  %C_addr_85 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_81
  %C_load_287 = load float* %C_addr_85, align 4
  %tmp_31_81 = fadd float %C_load_287, %tmp_29_81
  store float %tmp_31_81, float* %C_addr_85, align 4
  %Q_addr_85 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_81
  %Q_load_287 = load float* %Q_addr_85, align 4
  %tmp_32_81 = fsub float %Q_load_287, %temp_4_81
  store float %tmp_32_81, float* %Q_addr_85, align 4
  %c_82 = add i14 %phi_mul, 83
  %Q_load_288 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10183), align 4
  %tmp_21_82 = fmul float %Q_load_283, %Q_load_288
  %temp_4_82 = fdiv float %tmp_21_82, %qStar
  %C_load_288 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10183), align 4
  %tmp_22_82 = fmul float %C_load_283, %C_load_288
  %tmp_23_82 = fmul float %C_load_283, %Q_load_288
  %tmp_24_82 = fadd float %tmp_22_82, %tmp_23_82
  %tmp_25_82 = fmul float %Q_load_283, %C_load_288
  %tmp_26_82 = fadd float %tmp_24_82, %tmp_25_82
  %tmp_27_82 = fadd float %tmp_26_82, %tmp_21_82
  %tmp_28_82 = fdiv float %tmp_27_82, %tmp_5
  %tmp_29_82 = fsub float %temp_4_82, %tmp_28_82
  %tmp_30_82 = zext i14 %c_82 to i64
  %C_addr_86 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_82
  %C_load_289 = load float* %C_addr_86, align 4
  %tmp_31_82 = fadd float %C_load_289, %tmp_29_82
  store float %tmp_31_82, float* %C_addr_86, align 4
  %Q_addr_86 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_82
  %Q_load_289 = load float* %Q_addr_86, align 4
  %tmp_32_82 = fsub float %Q_load_289, %temp_4_82
  store float %tmp_32_82, float* %Q_addr_86, align 4
  %c_83 = add i14 %phi_mul, 84
  %Q_load_290 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10184), align 16
  %tmp_21_83 = fmul float %Q_load_283, %Q_load_290
  %temp_4_83 = fdiv float %tmp_21_83, %qStar
  %C_load_290 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10184), align 16
  %tmp_22_83 = fmul float %C_load_283, %C_load_290
  %tmp_23_83 = fmul float %C_load_283, %Q_load_290
  %tmp_24_83 = fadd float %tmp_22_83, %tmp_23_83
  %tmp_25_83 = fmul float %Q_load_283, %C_load_290
  %tmp_26_83 = fadd float %tmp_24_83, %tmp_25_83
  %tmp_27_83 = fadd float %tmp_26_83, %tmp_21_83
  %tmp_28_83 = fdiv float %tmp_27_83, %tmp_5
  %tmp_29_83 = fsub float %temp_4_83, %tmp_28_83
  %tmp_30_83 = zext i14 %c_83 to i64
  %C_addr_87 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_83
  %C_load_291 = load float* %C_addr_87, align 4
  %tmp_31_83 = fadd float %C_load_291, %tmp_29_83
  store float %tmp_31_83, float* %C_addr_87, align 4
  %Q_addr_87 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_83
  %Q_load_291 = load float* %Q_addr_87, align 4
  %tmp_32_83 = fsub float %Q_load_291, %temp_4_83
  store float %tmp_32_83, float* %Q_addr_87, align 4
  %c_84 = add i14 %phi_mul, 85
  %Q_load_292 = load float* %Q_addr, align 4
  %Q_load_293 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10185), align 4
  %tmp_21_84 = fmul float %Q_load_292, %Q_load_293
  %temp_4_84 = fdiv float %tmp_21_84, %qStar
  %C_load_292 = load float* %C_addr, align 4
  %C_load_293 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10185), align 4
  %tmp_22_84 = fmul float %C_load_292, %C_load_293
  %tmp_23_84 = fmul float %C_load_292, %Q_load_293
  %tmp_24_84 = fadd float %tmp_22_84, %tmp_23_84
  %tmp_25_84 = fmul float %Q_load_292, %C_load_293
  %tmp_26_84 = fadd float %tmp_24_84, %tmp_25_84
  %tmp_27_84 = fadd float %tmp_26_84, %tmp_21_84
  %tmp_28_84 = fdiv float %tmp_27_84, %tmp_5
  %tmp_29_84 = fsub float %temp_4_84, %tmp_28_84
  %tmp_30_84 = zext i14 %c_84 to i64
  %C_addr_88 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_84
  %C_load_294 = load float* %C_addr_88, align 4
  %tmp_31_84 = fadd float %C_load_294, %tmp_29_84
  store float %tmp_31_84, float* %C_addr_88, align 4
  %Q_addr_88 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_84
  %Q_load_294 = load float* %Q_addr_88, align 4
  %tmp_32_84 = fsub float %Q_load_294, %temp_4_84
  store float %tmp_32_84, float* %Q_addr_88, align 4
  %c_85 = add i14 %phi_mul, 86
  %Q_load_295 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10186), align 8
  %tmp_21_85 = fmul float %Q_load_292, %Q_load_295
  %temp_4_85 = fdiv float %tmp_21_85, %qStar
  %C_load_295 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10186), align 8
  %tmp_22_85 = fmul float %C_load_292, %C_load_295
  %tmp_23_85 = fmul float %C_load_292, %Q_load_295
  %tmp_24_85 = fadd float %tmp_22_85, %tmp_23_85
  %tmp_25_85 = fmul float %Q_load_292, %C_load_295
  %tmp_26_85 = fadd float %tmp_24_85, %tmp_25_85
  %tmp_27_85 = fadd float %tmp_26_85, %tmp_21_85
  %tmp_28_85 = fdiv float %tmp_27_85, %tmp_5
  %tmp_29_85 = fsub float %temp_4_85, %tmp_28_85
  %tmp_30_85 = zext i14 %c_85 to i64
  %C_addr_89 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_85
  %C_load_296 = load float* %C_addr_89, align 4
  %tmp_31_85 = fadd float %C_load_296, %tmp_29_85
  store float %tmp_31_85, float* %C_addr_89, align 4
  %Q_addr_89 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_85
  %Q_load_296 = load float* %Q_addr_89, align 4
  %tmp_32_85 = fsub float %Q_load_296, %temp_4_85
  store float %tmp_32_85, float* %Q_addr_89, align 4
  %c_86 = add i14 %phi_mul, 87
  %Q_load_297 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10187), align 4
  %tmp_21_86 = fmul float %Q_load_292, %Q_load_297
  %temp_4_86 = fdiv float %tmp_21_86, %qStar
  %C_load_297 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10187), align 4
  %tmp_22_86 = fmul float %C_load_292, %C_load_297
  %tmp_23_86 = fmul float %C_load_292, %Q_load_297
  %tmp_24_86 = fadd float %tmp_22_86, %tmp_23_86
  %tmp_25_86 = fmul float %Q_load_292, %C_load_297
  %tmp_26_86 = fadd float %tmp_24_86, %tmp_25_86
  %tmp_27_86 = fadd float %tmp_26_86, %tmp_21_86
  %tmp_28_86 = fdiv float %tmp_27_86, %tmp_5
  %tmp_29_86 = fsub float %temp_4_86, %tmp_28_86
  %tmp_30_86 = zext i14 %c_86 to i64
  %C_addr_90 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_86
  %C_load_298 = load float* %C_addr_90, align 4
  %tmp_31_86 = fadd float %C_load_298, %tmp_29_86
  store float %tmp_31_86, float* %C_addr_90, align 4
  %Q_addr_90 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_86
  %Q_load_298 = load float* %Q_addr_90, align 4
  %tmp_32_86 = fsub float %Q_load_298, %temp_4_86
  store float %tmp_32_86, float* %Q_addr_90, align 4
  %c_87 = add i14 %phi_mul, 88
  %Q_load_299 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10188), align 16
  %tmp_21_87 = fmul float %Q_load_292, %Q_load_299
  %temp_4_87 = fdiv float %tmp_21_87, %qStar
  %C_load_299 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10188), align 16
  %tmp_22_87 = fmul float %C_load_292, %C_load_299
  %tmp_23_87 = fmul float %C_load_292, %Q_load_299
  %tmp_24_87 = fadd float %tmp_22_87, %tmp_23_87
  %tmp_25_87 = fmul float %Q_load_292, %C_load_299
  %tmp_26_87 = fadd float %tmp_24_87, %tmp_25_87
  %tmp_27_87 = fadd float %tmp_26_87, %tmp_21_87
  %tmp_28_87 = fdiv float %tmp_27_87, %tmp_5
  %tmp_29_87 = fsub float %temp_4_87, %tmp_28_87
  %tmp_30_87 = zext i14 %c_87 to i64
  %C_addr_91 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_87
  %C_load_300 = load float* %C_addr_91, align 4
  %tmp_31_87 = fadd float %C_load_300, %tmp_29_87
  store float %tmp_31_87, float* %C_addr_91, align 4
  %Q_addr_91 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_87
  %Q_load_300 = load float* %Q_addr_91, align 4
  %tmp_32_87 = fsub float %Q_load_300, %temp_4_87
  store float %tmp_32_87, float* %Q_addr_91, align 4
  %c_88 = add i14 %phi_mul, 89
  %Q_load_301 = load float* %Q_addr, align 4
  %Q_load_302 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10189), align 4
  %tmp_21_88 = fmul float %Q_load_301, %Q_load_302
  %temp_4_88 = fdiv float %tmp_21_88, %qStar
  %C_load_301 = load float* %C_addr, align 4
  %C_load_302 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10189), align 4
  %tmp_22_88 = fmul float %C_load_301, %C_load_302
  %tmp_23_88 = fmul float %C_load_301, %Q_load_302
  %tmp_24_88 = fadd float %tmp_22_88, %tmp_23_88
  %tmp_25_88 = fmul float %Q_load_301, %C_load_302
  %tmp_26_88 = fadd float %tmp_24_88, %tmp_25_88
  %tmp_27_88 = fadd float %tmp_26_88, %tmp_21_88
  %tmp_28_88 = fdiv float %tmp_27_88, %tmp_5
  %tmp_29_88 = fsub float %temp_4_88, %tmp_28_88
  %tmp_30_88 = zext i14 %c_88 to i64
  %C_addr_92 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_88
  %C_load_303 = load float* %C_addr_92, align 4
  %tmp_31_88 = fadd float %C_load_303, %tmp_29_88
  store float %tmp_31_88, float* %C_addr_92, align 4
  %Q_addr_92 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_88
  %Q_load_303 = load float* %Q_addr_92, align 4
  %tmp_32_88 = fsub float %Q_load_303, %temp_4_88
  store float %tmp_32_88, float* %Q_addr_92, align 4
  %c_89 = add i14 %phi_mul, 90
  %Q_load_304 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10190), align 8
  %tmp_21_89 = fmul float %Q_load_301, %Q_load_304
  %temp_4_89 = fdiv float %tmp_21_89, %qStar
  %C_load_304 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10190), align 8
  %tmp_22_89 = fmul float %C_load_301, %C_load_304
  %tmp_23_89 = fmul float %C_load_301, %Q_load_304
  %tmp_24_89 = fadd float %tmp_22_89, %tmp_23_89
  %tmp_25_89 = fmul float %Q_load_301, %C_load_304
  %tmp_26_89 = fadd float %tmp_24_89, %tmp_25_89
  %tmp_27_89 = fadd float %tmp_26_89, %tmp_21_89
  %tmp_28_89 = fdiv float %tmp_27_89, %tmp_5
  %tmp_29_89 = fsub float %temp_4_89, %tmp_28_89
  %tmp_30_89 = zext i14 %c_89 to i64
  %C_addr_93 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_89
  %C_load_305 = load float* %C_addr_93, align 4
  %tmp_31_89 = fadd float %C_load_305, %tmp_29_89
  store float %tmp_31_89, float* %C_addr_93, align 4
  %Q_addr_93 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_89
  %Q_load_305 = load float* %Q_addr_93, align 4
  %tmp_32_89 = fsub float %Q_load_305, %temp_4_89
  store float %tmp_32_89, float* %Q_addr_93, align 4
  %c_90 = add i14 %phi_mul, 91
  %Q_load_306 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10191), align 4
  %tmp_21_90 = fmul float %Q_load_301, %Q_load_306
  %temp_4_90 = fdiv float %tmp_21_90, %qStar
  %C_load_306 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10191), align 4
  %tmp_22_90 = fmul float %C_load_301, %C_load_306
  %tmp_23_90 = fmul float %C_load_301, %Q_load_306
  %tmp_24_90 = fadd float %tmp_22_90, %tmp_23_90
  %tmp_25_90 = fmul float %Q_load_301, %C_load_306
  %tmp_26_90 = fadd float %tmp_24_90, %tmp_25_90
  %tmp_27_90 = fadd float %tmp_26_90, %tmp_21_90
  %tmp_28_90 = fdiv float %tmp_27_90, %tmp_5
  %tmp_29_90 = fsub float %temp_4_90, %tmp_28_90
  %tmp_30_90 = zext i14 %c_90 to i64
  %C_addr_94 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_90
  %C_load_307 = load float* %C_addr_94, align 4
  %tmp_31_90 = fadd float %C_load_307, %tmp_29_90
  store float %tmp_31_90, float* %C_addr_94, align 4
  %Q_addr_94 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_90
  %Q_load_307 = load float* %Q_addr_94, align 4
  %tmp_32_90 = fsub float %Q_load_307, %temp_4_90
  store float %tmp_32_90, float* %Q_addr_94, align 4
  %c_91 = add i14 %phi_mul, 92
  %Q_load_308 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10192), align 16
  %tmp_21_91 = fmul float %Q_load_301, %Q_load_308
  %temp_4_91 = fdiv float %tmp_21_91, %qStar
  %C_load_308 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10192), align 16
  %tmp_22_91 = fmul float %C_load_301, %C_load_308
  %tmp_23_91 = fmul float %C_load_301, %Q_load_308
  %tmp_24_91 = fadd float %tmp_22_91, %tmp_23_91
  %tmp_25_91 = fmul float %Q_load_301, %C_load_308
  %tmp_26_91 = fadd float %tmp_24_91, %tmp_25_91
  %tmp_27_91 = fadd float %tmp_26_91, %tmp_21_91
  %tmp_28_91 = fdiv float %tmp_27_91, %tmp_5
  %tmp_29_91 = fsub float %temp_4_91, %tmp_28_91
  %tmp_30_91 = zext i14 %c_91 to i64
  %C_addr_95 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_91
  %C_load_309 = load float* %C_addr_95, align 4
  %tmp_31_91 = fadd float %C_load_309, %tmp_29_91
  store float %tmp_31_91, float* %C_addr_95, align 4
  %Q_addr_95 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_91
  %Q_load_309 = load float* %Q_addr_95, align 4
  %tmp_32_91 = fsub float %Q_load_309, %temp_4_91
  store float %tmp_32_91, float* %Q_addr_95, align 4
  %c_92 = add i14 %phi_mul, 93
  %Q_load_310 = load float* %Q_addr, align 4
  %Q_load_311 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10193), align 4
  %tmp_21_92 = fmul float %Q_load_310, %Q_load_311
  %temp_4_92 = fdiv float %tmp_21_92, %qStar
  %C_load_310 = load float* %C_addr, align 4
  %C_load_311 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10193), align 4
  %tmp_22_92 = fmul float %C_load_310, %C_load_311
  %tmp_23_92 = fmul float %C_load_310, %Q_load_311
  %tmp_24_92 = fadd float %tmp_22_92, %tmp_23_92
  %tmp_25_92 = fmul float %Q_load_310, %C_load_311
  %tmp_26_92 = fadd float %tmp_24_92, %tmp_25_92
  %tmp_27_92 = fadd float %tmp_26_92, %tmp_21_92
  %tmp_28_92 = fdiv float %tmp_27_92, %tmp_5
  %tmp_29_92 = fsub float %temp_4_92, %tmp_28_92
  %tmp_30_92 = zext i14 %c_92 to i64
  %C_addr_96 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_92
  %C_load_312 = load float* %C_addr_96, align 4
  %tmp_31_92 = fadd float %C_load_312, %tmp_29_92
  store float %tmp_31_92, float* %C_addr_96, align 4
  %Q_addr_96 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_92
  %Q_load_312 = load float* %Q_addr_96, align 4
  %tmp_32_92 = fsub float %Q_load_312, %temp_4_92
  store float %tmp_32_92, float* %Q_addr_96, align 4
  %c_93 = add i14 %phi_mul, 94
  %Q_load_313 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10194), align 8
  %tmp_21_93 = fmul float %Q_load_310, %Q_load_313
  %temp_4_93 = fdiv float %tmp_21_93, %qStar
  %C_load_313 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10194), align 8
  %tmp_22_93 = fmul float %C_load_310, %C_load_313
  %tmp_23_93 = fmul float %C_load_310, %Q_load_313
  %tmp_24_93 = fadd float %tmp_22_93, %tmp_23_93
  %tmp_25_93 = fmul float %Q_load_310, %C_load_313
  %tmp_26_93 = fadd float %tmp_24_93, %tmp_25_93
  %tmp_27_93 = fadd float %tmp_26_93, %tmp_21_93
  %tmp_28_93 = fdiv float %tmp_27_93, %tmp_5
  %tmp_29_93 = fsub float %temp_4_93, %tmp_28_93
  %tmp_30_93 = zext i14 %c_93 to i64
  %C_addr_97 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_93
  %C_load_314 = load float* %C_addr_97, align 4
  %tmp_31_93 = fadd float %C_load_314, %tmp_29_93
  store float %tmp_31_93, float* %C_addr_97, align 4
  %Q_addr_97 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_93
  %Q_load_314 = load float* %Q_addr_97, align 4
  %tmp_32_93 = fsub float %Q_load_314, %temp_4_93
  store float %tmp_32_93, float* %Q_addr_97, align 4
  %c_94 = add i14 %phi_mul, 95
  %Q_load_315 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10195), align 4
  %tmp_21_94 = fmul float %Q_load_310, %Q_load_315
  %temp_4_94 = fdiv float %tmp_21_94, %qStar
  %C_load_315 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10195), align 4
  %tmp_22_94 = fmul float %C_load_310, %C_load_315
  %tmp_23_94 = fmul float %C_load_310, %Q_load_315
  %tmp_24_94 = fadd float %tmp_22_94, %tmp_23_94
  %tmp_25_94 = fmul float %Q_load_310, %C_load_315
  %tmp_26_94 = fadd float %tmp_24_94, %tmp_25_94
  %tmp_27_94 = fadd float %tmp_26_94, %tmp_21_94
  %tmp_28_94 = fdiv float %tmp_27_94, %tmp_5
  %tmp_29_94 = fsub float %temp_4_94, %tmp_28_94
  %tmp_30_94 = zext i14 %c_94 to i64
  %C_addr_98 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_94
  %C_load_316 = load float* %C_addr_98, align 4
  %tmp_31_94 = fadd float %C_load_316, %tmp_29_94
  store float %tmp_31_94, float* %C_addr_98, align 4
  %Q_addr_98 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_94
  %Q_load_316 = load float* %Q_addr_98, align 4
  %tmp_32_94 = fsub float %Q_load_316, %temp_4_94
  store float %tmp_32_94, float* %Q_addr_98, align 4
  %c_95 = add i14 %phi_mul, 96
  %Q_load_317 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10196), align 16
  %tmp_21_95 = fmul float %Q_load_310, %Q_load_317
  %temp_4_95 = fdiv float %tmp_21_95, %qStar
  %C_load_317 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10196), align 16
  %tmp_22_95 = fmul float %C_load_310, %C_load_317
  %tmp_23_95 = fmul float %C_load_310, %Q_load_317
  %tmp_24_95 = fadd float %tmp_22_95, %tmp_23_95
  %tmp_25_95 = fmul float %Q_load_310, %C_load_317
  %tmp_26_95 = fadd float %tmp_24_95, %tmp_25_95
  %tmp_27_95 = fadd float %tmp_26_95, %tmp_21_95
  %tmp_28_95 = fdiv float %tmp_27_95, %tmp_5
  %tmp_29_95 = fsub float %temp_4_95, %tmp_28_95
  %tmp_30_95 = zext i14 %c_95 to i64
  %C_addr_99 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_95
  %C_load_318 = load float* %C_addr_99, align 4
  %tmp_31_95 = fadd float %C_load_318, %tmp_29_95
  store float %tmp_31_95, float* %C_addr_99, align 4
  %Q_addr_99 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_95
  %Q_load_318 = load float* %Q_addr_99, align 4
  %tmp_32_95 = fsub float %Q_load_318, %temp_4_95
  store float %tmp_32_95, float* %Q_addr_99, align 4
  %c_96 = add i14 %phi_mul, 97
  %Q_load_319 = load float* %Q_addr, align 4
  %Q_load_320 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10197), align 4
  %tmp_21_96 = fmul float %Q_load_319, %Q_load_320
  %temp_4_96 = fdiv float %tmp_21_96, %qStar
  %C_load_319 = load float* %C_addr, align 4
  %C_load_320 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10197), align 4
  %tmp_22_96 = fmul float %C_load_319, %C_load_320
  %tmp_23_96 = fmul float %C_load_319, %Q_load_320
  %tmp_24_96 = fadd float %tmp_22_96, %tmp_23_96
  %tmp_25_96 = fmul float %Q_load_319, %C_load_320
  %tmp_26_96 = fadd float %tmp_24_96, %tmp_25_96
  %tmp_27_96 = fadd float %tmp_26_96, %tmp_21_96
  %tmp_28_96 = fdiv float %tmp_27_96, %tmp_5
  %tmp_29_96 = fsub float %temp_4_96, %tmp_28_96
  %tmp_30_96 = zext i14 %c_96 to i64
  %C_addr_100 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_96
  %C_load_321 = load float* %C_addr_100, align 4
  %tmp_31_96 = fadd float %C_load_321, %tmp_29_96
  store float %tmp_31_96, float* %C_addr_100, align 4
  %Q_addr_100 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_96
  %Q_load_321 = load float* %Q_addr_100, align 4
  %tmp_32_96 = fsub float %Q_load_321, %temp_4_96
  store float %tmp_32_96, float* %Q_addr_100, align 4
  %c_97 = add i14 %phi_mul, 98
  %Q_load_322 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10198), align 8
  %tmp_21_97 = fmul float %Q_load_319, %Q_load_322
  %temp_4_97 = fdiv float %tmp_21_97, %qStar
  %C_load_322 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10198), align 8
  %tmp_22_97 = fmul float %C_load_319, %C_load_322
  %tmp_23_97 = fmul float %C_load_319, %Q_load_322
  %tmp_24_97 = fadd float %tmp_22_97, %tmp_23_97
  %tmp_25_97 = fmul float %Q_load_319, %C_load_322
  %tmp_26_97 = fadd float %tmp_24_97, %tmp_25_97
  %tmp_27_97 = fadd float %tmp_26_97, %tmp_21_97
  %tmp_28_97 = fdiv float %tmp_27_97, %tmp_5
  %tmp_29_97 = fsub float %temp_4_97, %tmp_28_97
  %tmp_30_97 = zext i14 %c_97 to i64
  %C_addr_101 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_97
  %C_load_323 = load float* %C_addr_101, align 4
  %tmp_31_97 = fadd float %C_load_323, %tmp_29_97
  store float %tmp_31_97, float* %C_addr_101, align 4
  %Q_addr_101 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_97
  %Q_load_323 = load float* %Q_addr_101, align 4
  %tmp_32_97 = fsub float %Q_load_323, %temp_4_97
  store float %tmp_32_97, float* %Q_addr_101, align 4
  %c_98 = add i14 %phi_mul, 99
  %Q_load_324 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 10199), align 4
  %tmp_21_98 = fmul float %Q_load_319, %Q_load_324
  %temp_4_98 = fdiv float %tmp_21_98, %qStar
  %C_load_324 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 10199), align 4
  %tmp_22_98 = fmul float %C_load_319, %C_load_324
  %tmp_23_98 = fmul float %C_load_319, %Q_load_324
  %tmp_24_98 = fadd float %tmp_22_98, %tmp_23_98
  %tmp_25_98 = fmul float %Q_load_319, %C_load_324
  %tmp_26_98 = fadd float %tmp_24_98, %tmp_25_98
  %tmp_27_98 = fadd float %tmp_26_98, %tmp_21_98
  %tmp_28_98 = fdiv float %tmp_27_98, %tmp_5
  %tmp_29_98 = fsub float %temp_4_98, %tmp_28_98
  %tmp_30_98 = zext i14 %c_98 to i64
  %C_addr_102 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_30_98
  %C_load_325 = load float* %C_addr_102, align 4
  %tmp_31_98 = fadd float %C_load_325, %tmp_29_98
  store float %tmp_31_98, float* %C_addr_102, align 4
  %Q_addr_102 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_30_98
  %Q_load_325 = load float* %Q_addr_102, align 4
  %tmp_32_98 = fsub float %Q_load_325, %temp_4_98
  store float %tmp_32_98, float* %Q_addr_102, align 4
  %empty_4 = call i32 (...)* @_ssdm_op_SpecRegionEnd([18 x i8]* @p_str4, i32 %tmp_10) nounwind
  br label %4

.preheader:                                       ; preds = %4, %6
  %i5 = phi i7 [ %i_3, %6 ], [ 0, %4 ]
  %phi_mul1 = phi i14 [ %next_mul1, %6 ], [ 0, %4 ]
  %exitcond = icmp eq i7 %i5, -27
  %i_3 = add i7 %i5, 1
  br i1 %exitcond, label %7, label %6

; <label>:6                                       ; preds = %.preheader
  %i5_cast2 = zext i7 %i5 to i14
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 101, i64 101, i64 101)
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @p_str6) nounwind
  %tmp_26 = call i32 (...)* @_ssdm_op_SpecRegionBegin([20 x i8]* @p_str6) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %next_mul1 = add i14 %phi_mul1, 101
  %a_1 = add i14 %phi_mul1, 100
  %b = add i14 %i5_cast2, -6284
  %tmp_18 = zext i14 %a_1 to i64
  %Q_addr_2 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_18
  store float 0.000000e+00, float* %Q_addr_2, align 4
  %tmp_19 = zext i14 %b to i64
  %Q_addr_3 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_19
  store float 0.000000e+00, float* %Q_addr_3, align 4
  %C_addr_2 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_18
  store float 0.000000e+00, float* %C_addr_2, align 4
  %C_addr_3 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_19
  store float 0.000000e+00, float* %C_addr_3, align 4
  %empty_5 = call i32 (...)* @_ssdm_op_SpecRegionEnd([20 x i8]* @p_str6, i32 %tmp_26) nounwind
  br label %.preheader

; <label>:7                                       ; preds = %.preheader
  ret void
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecLoopTripCount(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

declare void @_GLOBAL__I_a() nounwind section ".text.startup"

declare float @llvm.exp.f32(float) nounwind readonly

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define float @projection_gp([21 x float]* %pX, float %pY, i1 zeroext %pPredict) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([21 x float]* %pX) nounwind, !map !55
  call void (...)* @_ssdm_op_SpecBitsMap(float %pY) nounwind, !map !61
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pPredict) nounwind, !map !67
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !71
  call void (...)* @_ssdm_op_SpecTopModule([14 x i8]* @projection_gp_str) nounwind
  %pPredict_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pPredict) nounwind
  %pY_read = call float @_ssdm_op_Read.s_axilite.float(float %pY) nounwind
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([21 x float]* %pX, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3) nounwind
  call void (...)* @_ssdm_op_SpecInterface([21 x float]* %pX, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 21, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(float %pY, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i1 %pPredict, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @p_str17, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  br i1 %pPredict_read, label %.preheader.0, label %1

; <label>:1                                       ; preds = %0
  %bvCnt_load = load i32* @bvCnt, align 4
  %tmp_s = icmp eq i32 %bvCnt_load, 100
  br i1 %tmp_s, label %2, label %3

; <label>:2                                       ; preds = %1
  call fastcc void @projection_gp_train_full_bv_set([21 x float]* %pX, float %pY_read) nounwind
  %bvCnt_load_1 = load i32* @bvCnt, align 4
  %tmp_26 = add i32 %bvCnt_load_1, 1
  store i32 %tmp_26, i32* @bvCnt, align 4
  br label %4

; <label>:3                                       ; preds = %1
  call fastcc void @projection_gp_train_not_full_bv_set([21 x float]* %pX, float %pY_read) nounwind
  br label %4

; <label>:4                                       ; preds = %3, %2
  br label %.loopexit

.preheader.0:                                     ; preds = %0
  %tmp_27 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 0, [21 x float]* %pX) nounwind
  %alpha_load = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_28 = fmul float %tmp_27, %alpha_load
  %sum_2 = fadd float %tmp_28, 5.000000e+00
  %tmp_29 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 21, [21 x float]* %pX) nounwind
  %alpha_load_1 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 1), align 4
  %tmp_155_1 = fmul float %tmp_29, %alpha_load_1
  %sum_2_1 = fadd float %sum_2, %tmp_155_1
  %tmp_30 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 42, [21 x float]* %pX) nounwind
  %alpha_load_2 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 2), align 8
  %tmp_155_2 = fmul float %tmp_30, %alpha_load_2
  %sum_2_2 = fadd float %sum_2_1, %tmp_155_2
  %tmp_31 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 63, [21 x float]* %pX) nounwind
  %alpha_load_3 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 3), align 4
  %tmp_155_3 = fmul float %tmp_31, %alpha_load_3
  %sum_2_3 = fadd float %sum_2_2, %tmp_155_3
  %tmp_32 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 84, [21 x float]* %pX) nounwind
  %alpha_load_4 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 4), align 16
  %tmp_155_4 = fmul float %tmp_32, %alpha_load_4
  %sum_2_4 = fadd float %sum_2_3, %tmp_155_4
  %tmp_33 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 105, [21 x float]* %pX) nounwind
  %alpha_load_5 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 5), align 4
  %tmp_155_5 = fmul float %tmp_33, %alpha_load_5
  %sum_2_5 = fadd float %sum_2_4, %tmp_155_5
  %tmp_34 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 126, [21 x float]* %pX) nounwind
  %alpha_load_6 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 6), align 8
  %tmp_155_6 = fmul float %tmp_34, %alpha_load_6
  %sum_2_6 = fadd float %sum_2_5, %tmp_155_6
  %tmp_35 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 147, [21 x float]* %pX) nounwind
  %alpha_load_7 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 7), align 4
  %tmp_155_7 = fmul float %tmp_35, %alpha_load_7
  %sum_2_7 = fadd float %sum_2_6, %tmp_155_7
  %tmp_36 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 168, [21 x float]* %pX) nounwind
  %alpha_load_8 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 8), align 16
  %tmp_155_8 = fmul float %tmp_36, %alpha_load_8
  %sum_2_8 = fadd float %sum_2_7, %tmp_155_8
  %tmp_37 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 189, [21 x float]* %pX) nounwind
  %alpha_load_9 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 9), align 4
  %tmp_155_9 = fmul float %tmp_37, %alpha_load_9
  %sum_2_9 = fadd float %sum_2_8, %tmp_155_9
  %tmp_38 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 210, [21 x float]* %pX) nounwind
  %alpha_load_10 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 10), align 8
  %tmp_155_s = fmul float %tmp_38, %alpha_load_10
  %sum_2_s = fadd float %sum_2_9, %tmp_155_s
  %tmp_39 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 231, [21 x float]* %pX) nounwind
  %alpha_load_11 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 11), align 4
  %tmp_155_10 = fmul float %tmp_39, %alpha_load_11
  %sum_2_10 = fadd float %sum_2_s, %tmp_155_10
  %tmp_40 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 252, [21 x float]* %pX) nounwind
  %alpha_load_12 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 12), align 16
  %tmp_155_11 = fmul float %tmp_40, %alpha_load_12
  %sum_2_11 = fadd float %sum_2_10, %tmp_155_11
  %tmp_41 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 273, [21 x float]* %pX) nounwind
  %alpha_load_13 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 13), align 4
  %tmp_155_12 = fmul float %tmp_41, %alpha_load_13
  %sum_2_12 = fadd float %sum_2_11, %tmp_155_12
  %tmp_42 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 294, [21 x float]* %pX) nounwind
  %alpha_load_14 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 14), align 8
  %tmp_155_13 = fmul float %tmp_42, %alpha_load_14
  %sum_2_13 = fadd float %sum_2_12, %tmp_155_13
  %tmp_43 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 315, [21 x float]* %pX) nounwind
  %alpha_load_15 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 15), align 4
  %tmp_155_14 = fmul float %tmp_43, %alpha_load_15
  %sum_2_14 = fadd float %sum_2_13, %tmp_155_14
  %tmp_44 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 336, [21 x float]* %pX) nounwind
  %alpha_load_16 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 16), align 16
  %tmp_155_15 = fmul float %tmp_44, %alpha_load_16
  %sum_2_15 = fadd float %sum_2_14, %tmp_155_15
  %tmp_45 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 357, [21 x float]* %pX) nounwind
  %alpha_load_17 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 17), align 4
  %tmp_155_16 = fmul float %tmp_45, %alpha_load_17
  %sum_2_16 = fadd float %sum_2_15, %tmp_155_16
  %tmp_46 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 378, [21 x float]* %pX) nounwind
  %alpha_load_18 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 18), align 8
  %tmp_155_17 = fmul float %tmp_46, %alpha_load_18
  %sum_2_17 = fadd float %sum_2_16, %tmp_155_17
  %tmp_47 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 399, [21 x float]* %pX) nounwind
  %alpha_load_19 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 19), align 4
  %tmp_155_18 = fmul float %tmp_47, %alpha_load_19
  %sum_2_18 = fadd float %sum_2_17, %tmp_155_18
  %tmp_48 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 420, [21 x float]* %pX) nounwind
  %alpha_load_20 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 20), align 16
  %tmp_155_19 = fmul float %tmp_48, %alpha_load_20
  %sum_2_19 = fadd float %sum_2_18, %tmp_155_19
  %tmp_49 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 441, [21 x float]* %pX) nounwind
  %alpha_load_21 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 21), align 4
  %tmp_155_20 = fmul float %tmp_49, %alpha_load_21
  %sum_2_20 = fadd float %sum_2_19, %tmp_155_20
  %tmp_50 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 462, [21 x float]* %pX) nounwind
  %alpha_load_22 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 22), align 8
  %tmp_155_21 = fmul float %tmp_50, %alpha_load_22
  %sum_2_21 = fadd float %sum_2_20, %tmp_155_21
  %tmp_51 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 483, [21 x float]* %pX) nounwind
  %alpha_load_23 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 23), align 4
  %tmp_155_22 = fmul float %tmp_51, %alpha_load_23
  %sum_2_22 = fadd float %sum_2_21, %tmp_155_22
  %tmp_52 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 504, [21 x float]* %pX) nounwind
  %alpha_load_24 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 24), align 16
  %tmp_155_23 = fmul float %tmp_52, %alpha_load_24
  %sum_2_23 = fadd float %sum_2_22, %tmp_155_23
  %tmp_53 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 525, [21 x float]* %pX) nounwind
  %alpha_load_25 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 25), align 4
  %tmp_155_24 = fmul float %tmp_53, %alpha_load_25
  %sum_2_24 = fadd float %sum_2_23, %tmp_155_24
  %tmp_54 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 546, [21 x float]* %pX) nounwind
  %alpha_load_26 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 26), align 8
  %tmp_155_25 = fmul float %tmp_54, %alpha_load_26
  %sum_2_25 = fadd float %sum_2_24, %tmp_155_25
  %tmp_55 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 567, [21 x float]* %pX) nounwind
  %alpha_load_27 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 27), align 4
  %tmp_155_26 = fmul float %tmp_55, %alpha_load_27
  %sum_2_26 = fadd float %sum_2_25, %tmp_155_26
  %tmp_56 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 588, [21 x float]* %pX) nounwind
  %alpha_load_28 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 28), align 16
  %tmp_155_27 = fmul float %tmp_56, %alpha_load_28
  %sum_2_27 = fadd float %sum_2_26, %tmp_155_27
  %tmp_57 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 609, [21 x float]* %pX) nounwind
  %alpha_load_29 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 29), align 4
  %tmp_155_28 = fmul float %tmp_57, %alpha_load_29
  %sum_2_28 = fadd float %sum_2_27, %tmp_155_28
  %tmp_58 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 630, [21 x float]* %pX) nounwind
  %alpha_load_30 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 30), align 8
  %tmp_155_29 = fmul float %tmp_58, %alpha_load_30
  %sum_2_29 = fadd float %sum_2_28, %tmp_155_29
  %tmp_59 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 651, [21 x float]* %pX) nounwind
  %alpha_load_31 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 31), align 4
  %tmp_155_30 = fmul float %tmp_59, %alpha_load_31
  %sum_2_30 = fadd float %sum_2_29, %tmp_155_30
  %tmp_60 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 672, [21 x float]* %pX) nounwind
  %alpha_load_32 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 32), align 16
  %tmp_155_31 = fmul float %tmp_60, %alpha_load_32
  %sum_2_31 = fadd float %sum_2_30, %tmp_155_31
  %tmp_61 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 693, [21 x float]* %pX) nounwind
  %alpha_load_33 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 33), align 4
  %tmp_155_32 = fmul float %tmp_61, %alpha_load_33
  %sum_2_32 = fadd float %sum_2_31, %tmp_155_32
  %tmp_62 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 714, [21 x float]* %pX) nounwind
  %alpha_load_34 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 34), align 8
  %tmp_155_33 = fmul float %tmp_62, %alpha_load_34
  %sum_2_33 = fadd float %sum_2_32, %tmp_155_33
  %tmp_63 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 735, [21 x float]* %pX) nounwind
  %alpha_load_35 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 35), align 4
  %tmp_155_34 = fmul float %tmp_63, %alpha_load_35
  %sum_2_34 = fadd float %sum_2_33, %tmp_155_34
  %tmp_64 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 756, [21 x float]* %pX) nounwind
  %alpha_load_36 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 36), align 16
  %tmp_155_35 = fmul float %tmp_64, %alpha_load_36
  %sum_2_35 = fadd float %sum_2_34, %tmp_155_35
  %tmp_65 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 777, [21 x float]* %pX) nounwind
  %alpha_load_37 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 37), align 4
  %tmp_155_36 = fmul float %tmp_65, %alpha_load_37
  %sum_2_36 = fadd float %sum_2_35, %tmp_155_36
  %tmp_66 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 798, [21 x float]* %pX) nounwind
  %alpha_load_38 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 38), align 8
  %tmp_155_37 = fmul float %tmp_66, %alpha_load_38
  %sum_2_37 = fadd float %sum_2_36, %tmp_155_37
  %tmp_67 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 819, [21 x float]* %pX) nounwind
  %alpha_load_39 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 39), align 4
  %tmp_155_38 = fmul float %tmp_67, %alpha_load_39
  %sum_2_38 = fadd float %sum_2_37, %tmp_155_38
  %tmp_68 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 840, [21 x float]* %pX) nounwind
  %alpha_load_40 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 40), align 16
  %tmp_155_39 = fmul float %tmp_68, %alpha_load_40
  %sum_2_39 = fadd float %sum_2_38, %tmp_155_39
  %tmp_69 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 861, [21 x float]* %pX) nounwind
  %alpha_load_41 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 41), align 4
  %tmp_155_40 = fmul float %tmp_69, %alpha_load_41
  %sum_2_40 = fadd float %sum_2_39, %tmp_155_40
  %tmp_70 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 882, [21 x float]* %pX) nounwind
  %alpha_load_42 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 42), align 8
  %tmp_155_41 = fmul float %tmp_70, %alpha_load_42
  %sum_2_41 = fadd float %sum_2_40, %tmp_155_41
  %tmp_71 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 903, [21 x float]* %pX) nounwind
  %alpha_load_43 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 43), align 4
  %tmp_155_42 = fmul float %tmp_71, %alpha_load_43
  %sum_2_42 = fadd float %sum_2_41, %tmp_155_42
  %tmp_72 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 924, [21 x float]* %pX) nounwind
  %alpha_load_44 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 44), align 16
  %tmp_155_43 = fmul float %tmp_72, %alpha_load_44
  %sum_2_43 = fadd float %sum_2_42, %tmp_155_43
  %tmp_73 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 945, [21 x float]* %pX) nounwind
  %alpha_load_45 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 45), align 4
  %tmp_155_44 = fmul float %tmp_73, %alpha_load_45
  %sum_2_44 = fadd float %sum_2_43, %tmp_155_44
  %tmp_74 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 966, [21 x float]* %pX) nounwind
  %alpha_load_46 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 46), align 8
  %tmp_155_45 = fmul float %tmp_74, %alpha_load_46
  %sum_2_45 = fadd float %sum_2_44, %tmp_155_45
  %tmp_75 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 987, [21 x float]* %pX) nounwind
  %alpha_load_47 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 47), align 4
  %tmp_155_46 = fmul float %tmp_75, %alpha_load_47
  %sum_2_46 = fadd float %sum_2_45, %tmp_155_46
  %tmp_76 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1008, [21 x float]* %pX) nounwind
  %alpha_load_48 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 48), align 16
  %tmp_155_47 = fmul float %tmp_76, %alpha_load_48
  %sum_2_47 = fadd float %sum_2_46, %tmp_155_47
  %tmp_77 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1029, [21 x float]* %pX) nounwind
  %alpha_load_49 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 49), align 4
  %tmp_155_48 = fmul float %tmp_77, %alpha_load_49
  %sum_2_48 = fadd float %sum_2_47, %tmp_155_48
  %tmp_78 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1050, [21 x float]* %pX) nounwind
  %alpha_load_50 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 50), align 8
  %tmp_155_49 = fmul float %tmp_78, %alpha_load_50
  %sum_2_49 = fadd float %sum_2_48, %tmp_155_49
  %tmp_79 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1071, [21 x float]* %pX) nounwind
  %alpha_load_51 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 51), align 4
  %tmp_155_50 = fmul float %tmp_79, %alpha_load_51
  %sum_2_50 = fadd float %sum_2_49, %tmp_155_50
  %tmp_80 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1092, [21 x float]* %pX) nounwind
  %alpha_load_52 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 52), align 16
  %tmp_155_51 = fmul float %tmp_80, %alpha_load_52
  %sum_2_51 = fadd float %sum_2_50, %tmp_155_51
  %tmp_81 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1113, [21 x float]* %pX) nounwind
  %alpha_load_53 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 53), align 4
  %tmp_155_52 = fmul float %tmp_81, %alpha_load_53
  %sum_2_52 = fadd float %sum_2_51, %tmp_155_52
  %tmp_82 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1134, [21 x float]* %pX) nounwind
  %alpha_load_54 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 54), align 8
  %tmp_155_53 = fmul float %tmp_82, %alpha_load_54
  %sum_2_53 = fadd float %sum_2_52, %tmp_155_53
  %tmp_83 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1155, [21 x float]* %pX) nounwind
  %alpha_load_55 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 55), align 4
  %tmp_155_54 = fmul float %tmp_83, %alpha_load_55
  %sum_2_54 = fadd float %sum_2_53, %tmp_155_54
  %tmp_84 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1176, [21 x float]* %pX) nounwind
  %alpha_load_56 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 56), align 16
  %tmp_155_55 = fmul float %tmp_84, %alpha_load_56
  %sum_2_55 = fadd float %sum_2_54, %tmp_155_55
  %tmp_85 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1197, [21 x float]* %pX) nounwind
  %alpha_load_57 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 57), align 4
  %tmp_155_56 = fmul float %tmp_85, %alpha_load_57
  %sum_2_56 = fadd float %sum_2_55, %tmp_155_56
  %tmp_86 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1218, [21 x float]* %pX) nounwind
  %alpha_load_58 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 58), align 8
  %tmp_155_57 = fmul float %tmp_86, %alpha_load_58
  %sum_2_57 = fadd float %sum_2_56, %tmp_155_57
  %tmp_87 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1239, [21 x float]* %pX) nounwind
  %alpha_load_59 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 59), align 4
  %tmp_155_58 = fmul float %tmp_87, %alpha_load_59
  %sum_2_58 = fadd float %sum_2_57, %tmp_155_58
  %tmp_88 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1260, [21 x float]* %pX) nounwind
  %alpha_load_60 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 60), align 16
  %tmp_155_59 = fmul float %tmp_88, %alpha_load_60
  %sum_2_59 = fadd float %sum_2_58, %tmp_155_59
  %tmp_89 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1281, [21 x float]* %pX) nounwind
  %alpha_load_61 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 61), align 4
  %tmp_155_60 = fmul float %tmp_89, %alpha_load_61
  %sum_2_60 = fadd float %sum_2_59, %tmp_155_60
  %tmp_90 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1302, [21 x float]* %pX) nounwind
  %alpha_load_62 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 62), align 8
  %tmp_155_61 = fmul float %tmp_90, %alpha_load_62
  %sum_2_61 = fadd float %sum_2_60, %tmp_155_61
  %tmp_91 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1323, [21 x float]* %pX) nounwind
  %alpha_load_63 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 63), align 4
  %tmp_155_62 = fmul float %tmp_91, %alpha_load_63
  %sum_2_62 = fadd float %sum_2_61, %tmp_155_62
  %tmp_92 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1344, [21 x float]* %pX) nounwind
  %alpha_load_64 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 64), align 16
  %tmp_155_63 = fmul float %tmp_92, %alpha_load_64
  %sum_2_63 = fadd float %sum_2_62, %tmp_155_63
  %tmp_93 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1365, [21 x float]* %pX) nounwind
  %alpha_load_65 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 65), align 4
  %tmp_155_64 = fmul float %tmp_93, %alpha_load_65
  %sum_2_64 = fadd float %sum_2_63, %tmp_155_64
  %tmp_94 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1386, [21 x float]* %pX) nounwind
  %alpha_load_66 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 66), align 8
  %tmp_155_65 = fmul float %tmp_94, %alpha_load_66
  %sum_2_65 = fadd float %sum_2_64, %tmp_155_65
  %tmp_95 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1407, [21 x float]* %pX) nounwind
  %alpha_load_67 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 67), align 4
  %tmp_155_66 = fmul float %tmp_95, %alpha_load_67
  %sum_2_66 = fadd float %sum_2_65, %tmp_155_66
  %tmp_96 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1428, [21 x float]* %pX) nounwind
  %alpha_load_68 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 68), align 16
  %tmp_155_67 = fmul float %tmp_96, %alpha_load_68
  %sum_2_67 = fadd float %sum_2_66, %tmp_155_67
  %tmp_97 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1449, [21 x float]* %pX) nounwind
  %alpha_load_69 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 69), align 4
  %tmp_155_68 = fmul float %tmp_97, %alpha_load_69
  %sum_2_68 = fadd float %sum_2_67, %tmp_155_68
  %tmp_98 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1470, [21 x float]* %pX) nounwind
  %alpha_load_70 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 70), align 8
  %tmp_155_69 = fmul float %tmp_98, %alpha_load_70
  %sum_2_69 = fadd float %sum_2_68, %tmp_155_69
  %tmp_99 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1491, [21 x float]* %pX) nounwind
  %alpha_load_71 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 71), align 4
  %tmp_155_70 = fmul float %tmp_99, %alpha_load_71
  %sum_2_70 = fadd float %sum_2_69, %tmp_155_70
  %tmp_100 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1512, [21 x float]* %pX) nounwind
  %alpha_load_72 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 72), align 16
  %tmp_155_71 = fmul float %tmp_100, %alpha_load_72
  %sum_2_71 = fadd float %sum_2_70, %tmp_155_71
  %tmp_101 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1533, [21 x float]* %pX) nounwind
  %alpha_load_73 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 73), align 4
  %tmp_155_72 = fmul float %tmp_101, %alpha_load_73
  %sum_2_72 = fadd float %sum_2_71, %tmp_155_72
  %tmp_102 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1554, [21 x float]* %pX) nounwind
  %alpha_load_74 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 74), align 8
  %tmp_155_73 = fmul float %tmp_102, %alpha_load_74
  %sum_2_73 = fadd float %sum_2_72, %tmp_155_73
  %tmp_103 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1575, [21 x float]* %pX) nounwind
  %alpha_load_75 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 75), align 4
  %tmp_155_74 = fmul float %tmp_103, %alpha_load_75
  %sum_2_74 = fadd float %sum_2_73, %tmp_155_74
  %tmp_104 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1596, [21 x float]* %pX) nounwind
  %alpha_load_76 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 76), align 16
  %tmp_155_75 = fmul float %tmp_104, %alpha_load_76
  %sum_2_75 = fadd float %sum_2_74, %tmp_155_75
  %tmp_105 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1617, [21 x float]* %pX) nounwind
  %alpha_load_77 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 77), align 4
  %tmp_155_76 = fmul float %tmp_105, %alpha_load_77
  %sum_2_76 = fadd float %sum_2_75, %tmp_155_76
  %tmp_106 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1638, [21 x float]* %pX) nounwind
  %alpha_load_78 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 78), align 8
  %tmp_155_77 = fmul float %tmp_106, %alpha_load_78
  %sum_2_77 = fadd float %sum_2_76, %tmp_155_77
  %tmp_107 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1659, [21 x float]* %pX) nounwind
  %alpha_load_79 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 79), align 4
  %tmp_155_78 = fmul float %tmp_107, %alpha_load_79
  %sum_2_78 = fadd float %sum_2_77, %tmp_155_78
  %tmp_108 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1680, [21 x float]* %pX) nounwind
  %alpha_load_80 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 80), align 16
  %tmp_155_79 = fmul float %tmp_108, %alpha_load_80
  %sum_2_79 = fadd float %sum_2_78, %tmp_155_79
  %tmp_109 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1701, [21 x float]* %pX) nounwind
  %alpha_load_81 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 81), align 4
  %tmp_155_80 = fmul float %tmp_109, %alpha_load_81
  %sum_2_80 = fadd float %sum_2_79, %tmp_155_80
  %tmp_110 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1722, [21 x float]* %pX) nounwind
  %alpha_load_82 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 82), align 8
  %tmp_155_81 = fmul float %tmp_110, %alpha_load_82
  %sum_2_81 = fadd float %sum_2_80, %tmp_155_81
  %tmp_111 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1743, [21 x float]* %pX) nounwind
  %alpha_load_83 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 83), align 4
  %tmp_155_82 = fmul float %tmp_111, %alpha_load_83
  %sum_2_82 = fadd float %sum_2_81, %tmp_155_82
  %tmp_112 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1764, [21 x float]* %pX) nounwind
  %alpha_load_84 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 84), align 16
  %tmp_155_83 = fmul float %tmp_112, %alpha_load_84
  %sum_2_83 = fadd float %sum_2_82, %tmp_155_83
  %tmp_113 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1785, [21 x float]* %pX) nounwind
  %alpha_load_85 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 85), align 4
  %tmp_155_84 = fmul float %tmp_113, %alpha_load_85
  %sum_2_84 = fadd float %sum_2_83, %tmp_155_84
  %tmp_114 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1806, [21 x float]* %pX) nounwind
  %alpha_load_86 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 86), align 8
  %tmp_155_85 = fmul float %tmp_114, %alpha_load_86
  %sum_2_85 = fadd float %sum_2_84, %tmp_155_85
  %tmp_115 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1827, [21 x float]* %pX) nounwind
  %alpha_load_87 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 87), align 4
  %tmp_155_86 = fmul float %tmp_115, %alpha_load_87
  %sum_2_86 = fadd float %sum_2_85, %tmp_155_86
  %tmp_116 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1848, [21 x float]* %pX) nounwind
  %alpha_load_88 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 88), align 16
  %tmp_155_87 = fmul float %tmp_116, %alpha_load_88
  %sum_2_87 = fadd float %sum_2_86, %tmp_155_87
  %tmp_117 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1869, [21 x float]* %pX) nounwind
  %alpha_load_89 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 89), align 4
  %tmp_155_88 = fmul float %tmp_117, %alpha_load_89
  %sum_2_88 = fadd float %sum_2_87, %tmp_155_88
  %tmp_118 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1890, [21 x float]* %pX) nounwind
  %alpha_load_90 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 90), align 8
  %tmp_155_89 = fmul float %tmp_118, %alpha_load_90
  %sum_2_89 = fadd float %sum_2_88, %tmp_155_89
  %tmp_119 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1911, [21 x float]* %pX) nounwind
  %alpha_load_91 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 91), align 4
  %tmp_155_90 = fmul float %tmp_119, %alpha_load_91
  %sum_2_90 = fadd float %sum_2_89, %tmp_155_90
  %tmp_120 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1932, [21 x float]* %pX) nounwind
  %alpha_load_92 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 92), align 16
  %tmp_155_91 = fmul float %tmp_120, %alpha_load_92
  %sum_2_91 = fadd float %sum_2_90, %tmp_155_91
  %tmp_121 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1953, [21 x float]* %pX) nounwind
  %alpha_load_93 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 93), align 4
  %tmp_155_92 = fmul float %tmp_121, %alpha_load_93
  %sum_2_92 = fadd float %sum_2_91, %tmp_155_92
  %tmp_122 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1974, [21 x float]* %pX) nounwind
  %alpha_load_94 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 94), align 8
  %tmp_155_93 = fmul float %tmp_122, %alpha_load_94
  %sum_2_93 = fadd float %sum_2_92, %tmp_155_93
  %tmp_123 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 1995, [21 x float]* %pX) nounwind
  %alpha_load_95 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 95), align 4
  %tmp_155_94 = fmul float %tmp_123, %alpha_load_95
  %sum_2_94 = fadd float %sum_2_93, %tmp_155_94
  %tmp_124 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 2016, [21 x float]* %pX) nounwind
  %alpha_load_96 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 96), align 16
  %tmp_155_95 = fmul float %tmp_124, %alpha_load_96
  %sum_2_95 = fadd float %sum_2_94, %tmp_155_95
  %tmp_125 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 2037, [21 x float]* %pX) nounwind
  %alpha_load_97 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 97), align 4
  %tmp_155_96 = fmul float %tmp_125, %alpha_load_97
  %sum_2_96 = fadd float %sum_2_95, %tmp_155_96
  %tmp_126 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 2058, [21 x float]* %pX) nounwind
  %alpha_load_98 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 98), align 8
  %tmp_155_97 = fmul float %tmp_126, %alpha_load_98
  %sum_2_97 = fadd float %sum_2_96, %tmp_155_97
  %tmp_127 = call fastcc float @projection_gp_K([2121 x float]* @basisVectors, i13 2079, [21 x float]* %pX) nounwind
  %alpha_load_99 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 99), align 4
  %tmp_155_98 = fmul float %tmp_127, %alpha_load_99
  %sum_2_98 = fadd float %sum_2_97, %tmp_155_98
  br label %.loopexit

.loopexit:                                        ; preds = %.preheader.0, %4
  %p_0 = phi float [ 0.000000e+00, %4 ], [ %sum_2_98, %.preheader.0 ]
  ret float %p_0
}

define internal fastcc void @projection_gp_train_not_full_bv_set([21 x float]* nocapture %pX, float %pY) {
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([21 x float]* %pX, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3)
  %pY_read = call float @_ssdm_op_Read.ap_auto.float(float %pY)
  br label %1

; <label>:1                                       ; preds = %K.exit, %0
  %i = phi i32 [ 0, %0 ], [ %i_4, %K.exit ]
  %m = phi float [ 5.000000e+00, %0 ], [ %m_1, %K.exit ]
  %bvCnt_load = load i32* @bvCnt, align 4
  %tmp = icmp ult i32 %i, %bvCnt_load
  %i_4 = add i32 %i, 1
  br i1 %tmp, label %2, label %.preheader2

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str7) nounwind
  %tmp_s = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str7)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 100, i32 50, [1 x i8]* @p_str1) nounwind
  %tmp_1 = trunc i32 %i to i13
  %tmp_27_cast = mul i13 21, %tmp_1
  br label %3

; <label>:3                                       ; preds = %4, %2
  %sum_i = phi float [ 0.000000e+00, %2 ], [ %sum, %4 ]
  %i_i = phi i5 [ 0, %2 ], [ %i_9, %4 ]
  %exitcond_i = icmp eq i5 %i_i, -11
  %i_9 = add i5 %i_i, 1
  br i1 %exitcond_i, label %K.exit, label %4

; <label>:4                                       ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 21, i64 21, i64 21)
  %tmp_736_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_2_i = zext i5 %i_i to i64
  %tmp_2_i_cast = zext i5 %i_i to i13
  %sum1_i = add i13 %tmp_2_i_cast, %tmp_27_cast
  %sum1_i_cast = zext i13 %sum1_i to i64
  %basisVectors_addr = getelementptr [2121 x float]* @basisVectors, i64 0, i64 %sum1_i_cast
  %basisVectors_load = load float* %basisVectors_addr, align 4
  %pX_addr = getelementptr [21 x float]* %pX, i64 0, i64 %tmp_2_i
  %pX_load = load float* %pX_addr, align 4
  %val = fsub float %basisVectors_load, %pX_load
  %tmp_3_i = fmul float %val, %val
  %sum = fadd float %sum_i, %tmp_3_i
  %empty_6 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_736_i)
  br label %3

K.exit:                                           ; preds = %3
  %p_x_assign = fmul float %sum_i, -5.000000e-01
  %tmp_i_i = call float @llvm.exp.f32(float %p_x_assign) nounwind
  %tmp_28 = zext i32 %i to i64
  %k_addr = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp_28
  store float %tmp_i_i, float* %k_addr, align 4
  %alpha_addr = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp_28
  %alpha_load = load float* %alpha_addr, align 4
  %tmp_29 = fmul float %tmp_i_i, %alpha_load
  %m_1 = fadd float %m, %tmp_29
  %empty_7 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str7, i32 %tmp_s)
  br label %1

.preheader2:                                      ; preds = %1, %8
  %i1 = phi i32 [ %i_6, %8 ], [ 0, %1 ]
  %phi_mul = phi i32 [ %next_mul, %8 ], [ 0, %1 ]
  %next_mul = add i32 %phi_mul, 101
  %exitcond1 = icmp eq i32 %i1, %bvCnt_load
  %i_6 = add i32 %i1, 1
  br i1 %exitcond1, label %.preheader1, label %5

; <label>:5                                       ; preds = %.preheader2
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str8) nounwind
  %tmp_128 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str8)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 100, i32 50, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_31 = zext i32 %i1 to i64
  %s_addr = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp_31
  store float 0.000000e+00, float* %s_addr, align 4
  %e_addr = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp_31
  store float 0.000000e+00, float* %e_addr, align 4
  br label %6

; <label>:6                                       ; preds = %7, %5
  %tmp_34 = phi float [ 0.000000e+00, %5 ], [ %tmp_51, %7 ]
  %tmp_35 = phi float [ 0.000000e+00, %5 ], [ %tmp_49, %7 ]
  %j = phi i32 [ 0, %5 ], [ %j_1, %7 ]
  %exitcond3 = icmp eq i32 %j, %bvCnt_load
  %j_1 = add i32 %j, 1
  br i1 %exitcond3, label %8, label %7

; <label>:7                                       ; preds = %6
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str9) nounwind
  %tmp_130 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str9)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 100, i32 50, [1 x i8]* @p_str1) nounwind
  %tmp_45 = add i32 %j, %phi_mul
  %tmp_46 = zext i32 %tmp_45 to i64
  %C_addr = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_46
  %C_load = load float* %C_addr, align 4
  %tmp_47 = zext i32 %j to i64
  %k_addr_1 = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp_47
  %k_load = load float* %k_addr_1, align 4
  %tmp_48 = fmul float %C_load, %k_load
  %tmp_49 = fadd float %tmp_35, %tmp_48
  store float %tmp_49, float* %s_addr, align 4
  %Q_addr = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_46
  %Q_load = load float* %Q_addr, align 4
  %tmp_50 = fmul float %Q_load, %k_load
  %tmp_51 = fadd float %tmp_34, %tmp_50
  store float %tmp_51, float* %e_addr, align 4
  %empty_8 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str9, i32 %tmp_130)
  br label %6

; <label>:8                                       ; preds = %6
  %empty_9 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str8, i32 %tmp_128)
  br label %.preheader2

.preheader1:                                      ; preds = %.preheader2, %9
  %i2 = phi i32 [ %i_5, %9 ], [ 0, %.preheader2 ]
  %sigma2 = phi float [ %sigma2_1, %9 ], [ 1.000000e+00, %.preheader2 ]
  %exitcond2 = icmp eq i32 %i2, %bvCnt_load
  %i_5 = add i32 %i2, 1
  br i1 %exitcond2, label %10, label %9

; <label>:9                                       ; preds = %.preheader1
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str10) nounwind
  %tmp_129 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str10)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 100, i32 50, [1 x i8]* @p_str1) nounwind
  %tmp_37 = zext i32 %i2 to i64
  %s_addr_1 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp_37
  %s_load = load float* %s_addr_1, align 4
  %k_addr_2 = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp_37
  %k_load_1 = load float* %k_addr_2, align 4
  %tmp_38 = fmul float %s_load, %k_load_1
  %sigma2_1 = fadd float %sigma2, %tmp_38
  %empty_10 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str10, i32 %tmp_129)
  br label %.preheader1

; <label>:10                                      ; preds = %.preheader1
  store float 1.000000e+00, float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 100), align 16
  %tmp_39 = fsub float %pY_read, %m
  %tmp_40 = fpext float %tmp_39 to double
  %tmp_41 = fpext float %sigma2 to double
  %tmp_42 = fadd double %tmp_41, 9.000000e-02
  %tmp_43 = fdiv double %tmp_40, %tmp_42
  %q = fptrunc double %tmp_43 to float
  %tmp_44 = fdiv double -1.000000e+00, %tmp_42
  %r = fptrunc double %tmp_44 to float
  br label %11

; <label>:11                                      ; preds = %12, %10
  %gamma = phi float [ 1.000000e+00, %10 ], [ %gamma_1, %12 ]
  %i3 = phi i32 [ 0, %10 ], [ %i_7, %12 ]
  %exitcond9 = icmp eq i32 %i3, %bvCnt_load
  %i_7 = add i32 %i3, 1
  br i1 %exitcond9, label %13, label %12

; <label>:12                                      ; preds = %11
  call void (...)* @_ssdm_op_SpecLoopName([13 x i8]* @p_str11) nounwind
  %tmp_131 = call i32 (...)* @_ssdm_op_SpecRegionBegin([13 x i8]* @p_str11)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 100, i32 50, [1 x i8]* @p_str1) nounwind
  %tmp_53 = zext i32 %i3 to i64
  %e_addr_1 = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp_53
  %e_load = load float* %e_addr_1, align 4
  %k_addr_3 = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp_53
  %k_load_2 = load float* %k_addr_3, align 4
  %tmp_54 = fmul float %e_load, %k_load_2
  %gamma_1 = fsub float %gamma, %tmp_54
  %s_addr_2 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp_53
  %s_load_1 = load float* %s_addr_2, align 4
  %tmp_55 = fmul float %s_load_1, %q
  %alpha_addr_1 = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp_53
  %alpha_load_4 = load float* %alpha_addr_1, align 4
  %tmp_56 = fadd float %alpha_load_4, %tmp_55
  store float %tmp_56, float* %alpha_addr_1, align 4
  %empty_11 = call i32 (...)* @_ssdm_op_SpecRegionEnd([13 x i8]* @p_str11, i32 %tmp_131)
  br label %11

; <label>:13                                      ; preds = %11
  %alpha_load_5 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16
  %tmp_57 = fadd float %alpha_load_5, %q
  store float %tmp_57, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16
  %tmp_11 = add i32 %bvCnt_load, 1
  br label %14

; <label>:14                                      ; preds = %18, %13
  %i4 = phi i32 [ 0, %13 ], [ %i_8, %18 ]
  %phi_mul2 = phi i32 [ 0, %13 ], [ %next_mul2, %18 ]
  %next_mul2 = add i32 %phi_mul2, 101
  %exitcond8 = icmp eq i32 %i4, %tmp_11
  %i_8 = add i32 %i4, 1
  br i1 %exitcond8, label %.preheader.preheader, label %15

.preheader.preheader:                             ; preds = %14
  %tmp_59 = fpext float %gamma to double
  %cast = zext i32 %tmp_11 to i64
  %bound = mul i64 %cast, %cast
  br label %19

; <label>:15                                      ; preds = %14
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str12) nounwind
  %tmp_132 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str12)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 100, i32 50, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_60 = zext i32 %i4 to i64
  %s_addr_3 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp_60
  %s_load_2 = load float* %s_addr_3, align 4
  br label %16

; <label>:16                                      ; preds = %17, %15
  %j5 = phi i32 [ 0, %15 ], [ %j_2, %17 ]
  %exitcond = icmp eq i32 %j5, %tmp_11
  %j_2 = add i32 %j5, 1
  br i1 %exitcond, label %18, label %17

; <label>:17                                      ; preds = %16
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str13) nounwind
  %tmp_135 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str13)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 100, i32 50, [1 x i8]* @p_str1) nounwind
  %tmp_69 = zext i32 %j5 to i64
  %s_addr_4 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp_69
  %s_load_3 = load float* %s_addr_4, align 4
  %tmp_70 = fmul float %s_load_2, %s_load_3
  %tmp_71 = fmul float %tmp_70, %r
  %tmp_72 = add i32 %j5, %phi_mul2
  %tmp_73 = zext i32 %tmp_72 to i64
  %C_addr_6 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_73
  %C_load_5 = load float* %C_addr_6, align 4
  %tmp_74 = fadd float %C_load_5, %tmp_71
  store float %tmp_74, float* %C_addr_6, align 4
  %empty_12 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str13, i32 %tmp_135)
  br label %16

; <label>:18                                      ; preds = %16
  %empty_13 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str12, i32 %tmp_132)
  br label %14

; <label>:19                                      ; preds = %.preheader.preheader, %.preheader
  %indvar_flatten = phi i64 [ 0, %.preheader.preheader ], [ %indvar_flatten_next, %.preheader ]
  %i6 = phi i32 [ 0, %.preheader.preheader ], [ %i6_mid2, %.preheader ]
  %j7 = phi i32 [ 0, %.preheader.preheader ], [ %j_3, %.preheader ]
  %exitcond_flatten = icmp eq i64 %indvar_flatten, %bound
  %indvar_flatten_next = add i64 %indvar_flatten, 1
  br i1 %exitcond_flatten, label %.preheader8, label %.preheader

.preheader:                                       ; preds = %19
  call void (...)* @_ssdm_op_SpecLoopName([30 x i8]* @UPDATE_Q_OUTER_UPDATE_Q_INNER_s)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 0, i64 10000, i64 2500)
  %tmp_133 = icmp ugt i32 %j7, %bvCnt_load
  %j7_mid2 = select i1 %tmp_133, i32 0, i32 %j7
  %i_1 = add i32 %i6, 1
  %i6_mid2 = select i1 %tmp_133, i32 %i_1, i32 %i6
  %tmp_66 = icmp eq i32 %i6_mid2, 100
  %tmp_67 = mul i32 %i6_mid2, 101
  %tmp_68 = zext i32 %i6_mid2 to i64
  %e_addr_2 = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp_68
  %e_load_1 = load float* %e_addr_2, align 4
  %ti = select i1 %tmp_66, float -1.000000e+00, float %e_load_1
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str15) nounwind
  %tmp_134 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str15)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_76 = icmp eq i32 %j7_mid2, 100
  %tmp_77 = zext i32 %j7_mid2 to i64
  %e_addr_3 = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp_77
  %e_load_2 = load float* %e_addr_3, align 4
  %tj = select i1 %tmp_76, float -1.000000e+00, float %e_load_2
  %tmp_78 = fmul float %ti, %tj
  %tmp_79 = fpext float %tmp_78 to double
  %tmp_80 = fdiv double %tmp_79, %tmp_59
  %tmp_81 = add i32 %j7_mid2, %tmp_67
  %tmp_82 = zext i32 %tmp_81 to i64
  %Q_addr_6 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_82
  %Q_load_5 = load float* %Q_addr_6, align 4
  %tmp_83 = fpext float %Q_load_5 to double
  %tmp_84 = fadd double %tmp_83, %tmp_80
  %tmp_85 = fptrunc double %tmp_84 to float
  store float %tmp_85, float* %Q_addr_6, align 4
  %empty_14 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str15, i32 %tmp_134)
  %j_3 = add i32 %j7_mid2, 1
  br label %19

.preheader8:                                      ; preds = %19, %20
  %bvCnt_load_8 = phi i32 [ %bvCnt_load_2, %20 ], [ %bvCnt_load, %19 ]
  %i_i1 = phi i5 [ %i_10, %20 ], [ 0, %19 ]
  %i_i1_cast3 = zext i5 %i_i1 to i32
  %exitcond_i2 = icmp eq i5 %i_i1, -11
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 21, i64 21, i64 21)
  %i_10 = add i5 %i_i1, 1
  br i1 %exitcond_i2, label %copyBV.exit, label %20

; <label>:20                                      ; preds = %.preheader8
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @p_str16) nounwind
  %tmp_i3 = zext i5 %i_i1 to i64
  %pX_addr_1 = getelementptr [21 x float]* %pX, i64 0, i64 %tmp_i3
  %pX_load_1 = load float* %pX_addr_1, align 4
  %bvCnt_load_2 = load i32* @bvCnt, align 4
  %tmp_i = mul i32 %bvCnt_load_2, 21
  %tmp_99_i = add i32 %tmp_i, %i_i1_cast3
  %tmp_100_i = zext i32 %tmp_99_i to i64
  %basisVectors_addr_2 = getelementptr inbounds [2121 x float]* @basisVectors, i64 0, i64 %tmp_100_i
  store float %pX_load_1, float* %basisVectors_addr_2, align 4
  br label %.preheader8

copyBV.exit:                                      ; preds = %.preheader8
  %tmp_64 = add i32 %bvCnt_load_8, 1
  store i32 %tmp_64, i32* @bvCnt, align 4
  %tmp_65 = icmp ugt i32 %tmp_64, 100
  br i1 %tmp_65, label %21, label %._crit_edge4

; <label>:21                                      ; preds = %copyBV.exit
  %alpha_load_103 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_i4 = fmul float %alpha_load_103, %alpha_load_103
  %Q_load_327 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 0), align 16
  %C_load_327 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 0), align 16
  %tmp_i5 = fadd float %Q_load_327, %C_load_327
  %minScore = fdiv float %tmp_i4, %tmp_i5
  br label %22

; <label>:22                                      ; preds = %23, %21
  %index_1 = phi i7 [ 1, %21 ], [ %i_11, %23 ]
  %minScore1_i = phi float [ %minScore, %21 ], [ %minScore_2, %23 ]
  %index = phi i32 [ 0, %21 ], [ %index_2, %23 ]
  %index_1_cast2 = zext i7 %index_1 to i32
  %index_1_cast2_cast = zext i7 %index_1 to i15
  %exitcond_i6 = icmp eq i7 %index_1, -27
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 100, i64 100, i64 100) nounwind
  br i1 %exitcond_i6, label %getMinKLApprox.exit, label %23

; <label>:23                                      ; preds = %22
  %tmp_20_i = zext i7 %index_1 to i64
  %alpha_addr_2 = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp_20_i
  %alpha_load_104 = load float* %alpha_addr_2, align 4
  %tmp_21_i = fmul float %alpha_load_104, %alpha_load_104
  %tmp_22_i = mul i15 102, %index_1_cast2_cast
  %tmp_23_i = zext i15 %tmp_22_i to i64
  %Q_addr_103 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_23_i
  %Q_load_328 = load float* %Q_addr_103, align 8
  %C_addr_103 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_23_i
  %C_load_328 = load float* %C_addr_103, align 8
  %tmp_24_i = fadd float %Q_load_328, %C_load_328
  %tScore = fdiv float %tmp_21_i, %tmp_24_i
  %tScore_to_int = bitcast float %tScore to i32
  %tmp_2 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tScore_to_int, i32 23, i32 30)
  %tmp_3 = trunc i32 %tScore_to_int to i23
  %minScore1_i_to_int = bitcast float %minScore1_i to i32
  %tmp_4 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %minScore1_i_to_int, i32 23, i32 30)
  %tmp_5 = trunc i32 %minScore1_i_to_int to i23
  %notlhs = icmp ne i8 %tmp_2, -1
  %notrhs = icmp eq i23 %tmp_3, 0
  %tmp_6 = or i1 %notrhs, %notlhs
  %notlhs6 = icmp ne i8 %tmp_4, -1
  %notrhs7 = icmp eq i23 %tmp_5, 0
  %tmp_7 = or i1 %notrhs7, %notlhs6
  %tmp_8 = and i1 %tmp_6, %tmp_7
  %tmp_9 = fcmp olt float %tScore, %minScore1_i
  %tmp_10 = and i1 %tmp_8, %tmp_9
  %minScore_2 = select i1 %tmp_10, float %tScore, float %minScore1_i
  %index_2 = select i1 %tmp_10, i32 %index_1_cast2, i32 %index
  %i_11 = add i7 1, %index_1
  br label %22

getMinKLApprox.exit:                              ; preds = %22
  call fastcc void @projection_gp_deleteBV(i32 %index)
  br label %._crit_edge4

._crit_edge4:                                     ; preds = %getMinKLApprox.exit, %copyBV.exit
  ret void
}

define internal fastcc void @projection_gp_train_full_bv_set([21 x float]* nocapture %pX, float %pY) {
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([21 x float]* %pX, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3)
  %pY_read = call float @_ssdm_op_Read.ap_auto.float(float %pY)
  br label %1

; <label>:1                                       ; preds = %K.exit, %0
  %i = phi i7 [ 0, %0 ], [ %i_12, %K.exit ]
  %m = phi float [ 5.000000e+00, %0 ], [ %m_2, %K.exit ]
  %phi_mul = phi i12 [ 0, %0 ], [ %next_mul, %K.exit ]
  %next_mul = add i12 %phi_mul, 21
  %exitcond8 = icmp eq i7 %i, -28
  %i_12 = add i7 %i, 1
  br i1 %exitcond8, label %.preheader10.preheader, label %2

.preheader10.preheader:                           ; preds = %1
  %k_load_3 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 0), align 16
  %k_load_4 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 1), align 4
  %k_load_5 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 2), align 8
  %k_load_6 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 3), align 4
  %k_load_7 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 4), align 16
  %k_load_8 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 5), align 4
  %k_load_9 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 6), align 8
  %k_load_10 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 7), align 4
  %k_load_11 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 8), align 16
  %k_load_12 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 9), align 4
  %k_load_13 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 10), align 8
  %k_load_14 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 11), align 4
  %k_load_15 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 12), align 16
  %k_load_16 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 13), align 4
  %k_load_17 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 14), align 8
  %k_load_18 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 15), align 4
  %k_load_19 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 16), align 16
  %k_load_20 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 17), align 4
  %k_load_21 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 18), align 8
  %k_load_22 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 19), align 4
  %k_load_23 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 20), align 16
  %k_load_24 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 21), align 4
  %k_load_25 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 22), align 8
  %k_load_26 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 23), align 4
  %k_load_27 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 24), align 16
  %k_load_28 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 25), align 4
  %k_load_29 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 26), align 8
  %k_load_30 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 27), align 4
  %k_load_31 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 28), align 16
  %k_load_32 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 29), align 4
  %k_load_33 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 30), align 8
  %k_load_34 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 31), align 4
  %k_load_35 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 32), align 16
  %k_load_36 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 33), align 4
  %k_load_37 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 34), align 8
  %k_load_38 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 35), align 4
  %k_load_39 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 36), align 16
  %k_load_40 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 37), align 4
  %k_load_41 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 38), align 8
  %k_load_42 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 39), align 4
  %k_load_43 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 40), align 16
  %k_load_44 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 41), align 4
  %k_load_45 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 42), align 8
  %k_load_46 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 43), align 4
  %k_load_47 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 44), align 16
  %k_load_48 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 45), align 4
  %k_load_49 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 46), align 8
  %k_load_50 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 47), align 4
  %k_load_51 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 48), align 16
  %k_load_52 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 49), align 4
  %k_load_53 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 50), align 8
  %k_load_54 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 51), align 4
  %k_load_55 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 52), align 16
  %k_load_56 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 53), align 4
  %k_load_57 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 54), align 8
  %k_load_58 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 55), align 4
  %k_load_59 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 56), align 16
  %k_load_60 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 57), align 4
  %k_load_61 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 58), align 8
  %k_load_62 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 59), align 4
  %k_load_63 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 60), align 16
  %k_load_64 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 61), align 4
  %k_load_65 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 62), align 8
  %k_load_66 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 63), align 4
  %k_load_67 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 64), align 16
  %k_load_68 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 65), align 4
  %k_load_69 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 66), align 8
  %k_load_70 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 67), align 4
  %k_load_71 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 68), align 16
  %k_load_72 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 69), align 4
  %k_load_73 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 70), align 8
  %k_load_74 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 71), align 4
  %k_load_75 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 72), align 16
  %k_load_76 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 73), align 4
  %k_load_77 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 74), align 8
  %k_load_78 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 75), align 4
  %k_load_79 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 76), align 16
  %k_load_80 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 77), align 4
  %k_load_81 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 78), align 8
  %k_load_82 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 79), align 4
  %k_load_83 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 80), align 16
  %k_load_84 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 81), align 4
  %k_load_85 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 82), align 8
  %k_load_86 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 83), align 4
  %k_load_87 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 84), align 16
  %k_load_88 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 85), align 4
  %k_load_89 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 86), align 8
  %k_load_90 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 87), align 4
  %k_load_91 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 88), align 16
  %k_load_92 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 89), align 4
  %k_load_93 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 90), align 8
  %k_load_94 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 91), align 4
  %k_load_95 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 92), align 16
  %k_load_96 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 93), align 4
  %k_load_97 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 94), align 8
  %k_load_98 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 95), align 4
  %k_load_99 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 96), align 16
  %k_load_100 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 97), align 4
  %k_load_101 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 98), align 8
  %k_load_102 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 99), align 4
  br label %.preheader10

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 100, i64 100, i64 100)
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str7) nounwind
  %tmp_s = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str7)
  br label %3

; <label>:3                                       ; preds = %4, %2
  %sum_i = phi float [ 0.000000e+00, %2 ], [ %sum, %4 ]
  %i_i = phi i5 [ 0, %2 ], [ %i_3, %4 ]
  %exitcond_i = icmp eq i5 %i_i, -11
  %i_3 = add i5 %i_i, 1
  br i1 %exitcond_i, label %K.exit, label %4

; <label>:4                                       ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 21, i64 21, i64 21)
  %tmp_736_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_2_i = zext i5 %i_i to i64
  %tmp_2_i_cast = zext i5 %i_i to i12
  %sum1_i = add i12 %tmp_2_i_cast, %phi_mul
  %sum1_i_cast = zext i12 %sum1_i to i64
  %basisVectors_addr = getelementptr [2121 x float]* @basisVectors, i64 0, i64 %sum1_i_cast
  %basisVectors_load = load float* %basisVectors_addr, align 4
  %pX_addr = getelementptr [21 x float]* %pX, i64 0, i64 %tmp_2_i
  %pX_load = load float* %pX_addr, align 4
  %val = fsub float %basisVectors_load, %pX_load
  %tmp_3_i = fmul float %val, %val
  %sum = fadd float %sum_i, %tmp_3_i
  %empty_15 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_736_i)
  br label %3

K.exit:                                           ; preds = %3
  %p_x_assign = fmul float %sum_i, -5.000000e-01
  %tmp_i_i = call float @llvm.exp.f32(float %p_x_assign) nounwind
  %tmp_86 = zext i7 %i to i64
  %k_addr = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp_86
  store float %tmp_i_i, float* %k_addr, align 4
  %alpha_addr = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp_86
  %alpha_load = load float* %alpha_addr, align 4
  %tmp_87 = fmul float %tmp_i_i, %alpha_load
  %m_2 = fadd float %m, %tmp_87
  %empty_16 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str7, i32 %tmp_s)
  br label %1

.preheader10:                                     ; preds = %5, %.preheader10.preheader
  %i1 = phi i7 [ %i_2, %5 ], [ 0, %.preheader10.preheader ]
  %phi_mul3 = phi i14 [ %next_mul3, %5 ], [ 0, %.preheader10.preheader ]
  %exitcond7 = icmp eq i7 %i1, -28
  %i_2 = add i7 %i1, 1
  br i1 %exitcond7, label %.preheader9, label %5

; <label>:5                                       ; preds = %.preheader10
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 100, i64 100, i64 100)
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str8) nounwind
  %tmp_137 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str8)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_88 = zext i7 %i1 to i64
  %s_addr = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp_88
  %e_addr = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp_88
  %next_mul3 = add i14 %phi_mul3, 101
  %tmp_117 = zext i14 %phi_mul3 to i64
  %C_addr = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117
  %C_load = load float* %C_addr, align 4
  %tmp_119 = fmul float %C_load, %k_load_3
  %tmp_120 = fadd float %tmp_119, 0.000000e+00
  %Q_addr = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117
  %Q_load = load float* %Q_addr, align 4
  %tmp_121 = fmul float %Q_load, %k_load_3
  %tmp_122 = fadd float %tmp_121, 0.000000e+00
  %tmp_116_1 = add i14 %phi_mul3, 1
  %tmp_117_1 = zext i14 %tmp_116_1 to i64
  %C_addr_1 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_1
  %C_load_1 = load float* %C_addr_1, align 4
  %tmp_119_1 = fmul float %C_load_1, %k_load_4
  %tmp_120_1 = fadd float %tmp_120, %tmp_119_1
  %Q_addr_1 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_1
  %Q_load_1 = load float* %Q_addr_1, align 4
  %tmp_121_1 = fmul float %Q_load_1, %k_load_4
  %tmp_122_1 = fadd float %tmp_122, %tmp_121_1
  %tmp_116_2 = add i14 %phi_mul3, 2
  %tmp_117_2 = zext i14 %tmp_116_2 to i64
  %C_addr_2 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_2
  %C_load_2 = load float* %C_addr_2, align 4
  %tmp_119_2 = fmul float %C_load_2, %k_load_5
  %tmp_120_2 = fadd float %tmp_120_1, %tmp_119_2
  %Q_addr_2 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_2
  %Q_load_2 = load float* %Q_addr_2, align 4
  %tmp_121_2 = fmul float %Q_load_2, %k_load_5
  %tmp_122_2 = fadd float %tmp_122_1, %tmp_121_2
  %tmp_116_3 = add i14 %phi_mul3, 3
  %tmp_117_3 = zext i14 %tmp_116_3 to i64
  %C_addr_3 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_3
  %C_load_3 = load float* %C_addr_3, align 4
  %tmp_119_3 = fmul float %C_load_3, %k_load_6
  %tmp_120_3 = fadd float %tmp_120_2, %tmp_119_3
  %Q_addr_3 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_3
  %Q_load_3 = load float* %Q_addr_3, align 4
  %tmp_121_3 = fmul float %Q_load_3, %k_load_6
  %tmp_122_3 = fadd float %tmp_122_2, %tmp_121_3
  %tmp_116_4 = add i14 %phi_mul3, 4
  %tmp_117_4 = zext i14 %tmp_116_4 to i64
  %C_addr_4 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_4
  %C_load_4 = load float* %C_addr_4, align 4
  %tmp_119_4 = fmul float %C_load_4, %k_load_7
  %tmp_120_4 = fadd float %tmp_120_3, %tmp_119_4
  %Q_addr_4 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_4
  %Q_load_4 = load float* %Q_addr_4, align 4
  %tmp_121_4 = fmul float %Q_load_4, %k_load_7
  %tmp_122_4 = fadd float %tmp_122_3, %tmp_121_4
  %tmp_116_5 = add i14 %phi_mul3, 5
  %tmp_117_5 = zext i14 %tmp_116_5 to i64
  %C_addr_5 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_5
  %C_load_5 = load float* %C_addr_5, align 4
  %tmp_119_5 = fmul float %C_load_5, %k_load_8
  %tmp_120_5 = fadd float %tmp_120_4, %tmp_119_5
  %Q_addr_5 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_5
  %Q_load_5 = load float* %Q_addr_5, align 4
  %tmp_121_5 = fmul float %Q_load_5, %k_load_8
  %tmp_122_5 = fadd float %tmp_122_4, %tmp_121_5
  %tmp_116_6 = add i14 %phi_mul3, 6
  %tmp_117_6 = zext i14 %tmp_116_6 to i64
  %C_addr_6 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_6
  %C_load_63 = load float* %C_addr_6, align 4
  %tmp_119_6 = fmul float %C_load_63, %k_load_9
  %tmp_120_6 = fadd float %tmp_120_5, %tmp_119_6
  %Q_addr_6 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_6
  %Q_load_64 = load float* %Q_addr_6, align 4
  %tmp_121_6 = fmul float %Q_load_64, %k_load_9
  %tmp_122_6 = fadd float %tmp_122_5, %tmp_121_6
  %tmp_116_7 = add i14 %phi_mul3, 7
  %tmp_117_7 = zext i14 %tmp_116_7 to i64
  %C_addr_76 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_7
  %C_load_7 = load float* %C_addr_76, align 4
  %tmp_119_7 = fmul float %C_load_7, %k_load_10
  %tmp_120_7 = fadd float %tmp_120_6, %tmp_119_7
  %Q_addr_77 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_7
  %Q_load_7 = load float* %Q_addr_77, align 4
  %tmp_121_7 = fmul float %Q_load_7, %k_load_10
  %tmp_122_7 = fadd float %tmp_122_6, %tmp_121_7
  %tmp_116_8 = add i14 %phi_mul3, 8
  %tmp_117_8 = zext i14 %tmp_116_8 to i64
  %C_addr_8 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_8
  %C_load_8 = load float* %C_addr_8, align 4
  %tmp_119_8 = fmul float %C_load_8, %k_load_11
  %tmp_120_8 = fadd float %tmp_120_7, %tmp_119_8
  %Q_addr_8 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_8
  %Q_load_8 = load float* %Q_addr_8, align 4
  %tmp_121_8 = fmul float %Q_load_8, %k_load_11
  %tmp_122_8 = fadd float %tmp_122_7, %tmp_121_8
  %tmp_116_9 = add i14 %phi_mul3, 9
  %tmp_117_9 = zext i14 %tmp_116_9 to i64
  %C_addr_9 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_9
  %C_load_9 = load float* %C_addr_9, align 4
  %tmp_119_9 = fmul float %C_load_9, %k_load_12
  %tmp_120_9 = fadd float %tmp_120_8, %tmp_119_9
  %Q_addr_9 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_9
  %Q_load_9 = load float* %Q_addr_9, align 4
  %tmp_121_9 = fmul float %Q_load_9, %k_load_12
  %tmp_122_9 = fadd float %tmp_122_8, %tmp_121_9
  %tmp_116_s = add i14 %phi_mul3, 10
  %tmp_117_s = zext i14 %tmp_116_s to i64
  %C_addr_10 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_s
  %C_load_10 = load float* %C_addr_10, align 4
  %tmp_119_s = fmul float %C_load_10, %k_load_13
  %tmp_120_s = fadd float %tmp_120_9, %tmp_119_s
  %Q_addr_10 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_s
  %Q_load_10 = load float* %Q_addr_10, align 4
  %tmp_121_s = fmul float %Q_load_10, %k_load_13
  %tmp_122_s = fadd float %tmp_122_9, %tmp_121_s
  %tmp_116_10 = add i14 %phi_mul3, 11
  %tmp_117_10 = zext i14 %tmp_116_10 to i64
  %C_addr_11 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_10
  %C_load_11 = load float* %C_addr_11, align 4
  %tmp_119_10 = fmul float %C_load_11, %k_load_14
  %tmp_120_10 = fadd float %tmp_120_s, %tmp_119_10
  %Q_addr_11 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_10
  %Q_load_11 = load float* %Q_addr_11, align 4
  %tmp_121_10 = fmul float %Q_load_11, %k_load_14
  %tmp_122_10 = fadd float %tmp_122_s, %tmp_121_10
  %tmp_116_11 = add i14 %phi_mul3, 12
  %tmp_117_11 = zext i14 %tmp_116_11 to i64
  %C_addr_12 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_11
  %C_load_12 = load float* %C_addr_12, align 4
  %tmp_119_11 = fmul float %C_load_12, %k_load_15
  %tmp_120_11 = fadd float %tmp_120_10, %tmp_119_11
  %Q_addr_12 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_11
  %Q_load_12 = load float* %Q_addr_12, align 4
  %tmp_121_11 = fmul float %Q_load_12, %k_load_15
  %tmp_122_11 = fadd float %tmp_122_10, %tmp_121_11
  %tmp_116_12 = add i14 %phi_mul3, 13
  %tmp_117_12 = zext i14 %tmp_116_12 to i64
  %C_addr_13 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_12
  %C_load_13 = load float* %C_addr_13, align 4
  %tmp_119_12 = fmul float %C_load_13, %k_load_16
  %tmp_120_12 = fadd float %tmp_120_11, %tmp_119_12
  %Q_addr_13 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_12
  %Q_load_13 = load float* %Q_addr_13, align 4
  %tmp_121_12 = fmul float %Q_load_13, %k_load_16
  %tmp_122_12 = fadd float %tmp_122_11, %tmp_121_12
  %tmp_116_13 = add i14 %phi_mul3, 14
  %tmp_117_13 = zext i14 %tmp_116_13 to i64
  %C_addr_14 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_13
  %C_load_14 = load float* %C_addr_14, align 4
  %tmp_119_13 = fmul float %C_load_14, %k_load_17
  %tmp_120_13 = fadd float %tmp_120_12, %tmp_119_13
  %Q_addr_14 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_13
  %Q_load_14 = load float* %Q_addr_14, align 4
  %tmp_121_13 = fmul float %Q_load_14, %k_load_17
  %tmp_122_13 = fadd float %tmp_122_12, %tmp_121_13
  %tmp_116_14 = add i14 %phi_mul3, 15
  %tmp_117_14 = zext i14 %tmp_116_14 to i64
  %C_addr_15 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_14
  %C_load_15 = load float* %C_addr_15, align 4
  %tmp_119_14 = fmul float %C_load_15, %k_load_18
  %tmp_120_14 = fadd float %tmp_120_13, %tmp_119_14
  %Q_addr_15 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_14
  %Q_load_15 = load float* %Q_addr_15, align 4
  %tmp_121_14 = fmul float %Q_load_15, %k_load_18
  %tmp_122_14 = fadd float %tmp_122_13, %tmp_121_14
  %tmp_116_15 = add i14 %phi_mul3, 16
  %tmp_117_15 = zext i14 %tmp_116_15 to i64
  %C_addr_16 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_15
  %C_load_16 = load float* %C_addr_16, align 4
  %tmp_119_15 = fmul float %C_load_16, %k_load_19
  %tmp_120_15 = fadd float %tmp_120_14, %tmp_119_15
  %Q_addr_16 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_15
  %Q_load_16 = load float* %Q_addr_16, align 4
  %tmp_121_15 = fmul float %Q_load_16, %k_load_19
  %tmp_122_15 = fadd float %tmp_122_14, %tmp_121_15
  %tmp_116_16 = add i14 %phi_mul3, 17
  %tmp_117_16 = zext i14 %tmp_116_16 to i64
  %C_addr_17 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_16
  %C_load_17 = load float* %C_addr_17, align 4
  %tmp_119_16 = fmul float %C_load_17, %k_load_20
  %tmp_120_16 = fadd float %tmp_120_15, %tmp_119_16
  %Q_addr_17 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_16
  %Q_load_17 = load float* %Q_addr_17, align 4
  %tmp_121_16 = fmul float %Q_load_17, %k_load_20
  %tmp_122_16 = fadd float %tmp_122_15, %tmp_121_16
  %tmp_116_17 = add i14 %phi_mul3, 18
  %tmp_117_17 = zext i14 %tmp_116_17 to i64
  %C_addr_18 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_17
  %C_load_18 = load float* %C_addr_18, align 4
  %tmp_119_17 = fmul float %C_load_18, %k_load_21
  %tmp_120_17 = fadd float %tmp_120_16, %tmp_119_17
  %Q_addr_18 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_17
  %Q_load_18 = load float* %Q_addr_18, align 4
  %tmp_121_17 = fmul float %Q_load_18, %k_load_21
  %tmp_122_17 = fadd float %tmp_122_16, %tmp_121_17
  %tmp_116_18 = add i14 %phi_mul3, 19
  %tmp_117_18 = zext i14 %tmp_116_18 to i64
  %C_addr_19 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_18
  %C_load_19 = load float* %C_addr_19, align 4
  %tmp_119_18 = fmul float %C_load_19, %k_load_22
  %tmp_120_18 = fadd float %tmp_120_17, %tmp_119_18
  %Q_addr_19 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_18
  %Q_load_19 = load float* %Q_addr_19, align 4
  %tmp_121_18 = fmul float %Q_load_19, %k_load_22
  %tmp_122_18 = fadd float %tmp_122_17, %tmp_121_18
  %tmp_116_19 = add i14 %phi_mul3, 20
  %tmp_117_19 = zext i14 %tmp_116_19 to i64
  %C_addr_20 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_19
  %C_load_20 = load float* %C_addr_20, align 4
  %tmp_119_19 = fmul float %C_load_20, %k_load_23
  %tmp_120_19 = fadd float %tmp_120_18, %tmp_119_19
  %Q_addr_20 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_19
  %Q_load_20 = load float* %Q_addr_20, align 4
  %tmp_121_19 = fmul float %Q_load_20, %k_load_23
  %tmp_122_19 = fadd float %tmp_122_18, %tmp_121_19
  %tmp_116_20 = add i14 %phi_mul3, 21
  %tmp_117_20 = zext i14 %tmp_116_20 to i64
  %C_addr_21 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_20
  %C_load_21 = load float* %C_addr_21, align 4
  %tmp_119_20 = fmul float %C_load_21, %k_load_24
  %tmp_120_20 = fadd float %tmp_120_19, %tmp_119_20
  %Q_addr_21 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_20
  %Q_load_21 = load float* %Q_addr_21, align 4
  %tmp_121_20 = fmul float %Q_load_21, %k_load_24
  %tmp_122_20 = fadd float %tmp_122_19, %tmp_121_20
  %tmp_116_21 = add i14 %phi_mul3, 22
  %tmp_117_21 = zext i14 %tmp_116_21 to i64
  %C_addr_22 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_21
  %C_load_22 = load float* %C_addr_22, align 4
  %tmp_119_21 = fmul float %C_load_22, %k_load_25
  %tmp_120_21 = fadd float %tmp_120_20, %tmp_119_21
  %Q_addr_22 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_21
  %Q_load_22 = load float* %Q_addr_22, align 4
  %tmp_121_21 = fmul float %Q_load_22, %k_load_25
  %tmp_122_21 = fadd float %tmp_122_20, %tmp_121_21
  %tmp_116_22 = add i14 %phi_mul3, 23
  %tmp_117_22 = zext i14 %tmp_116_22 to i64
  %C_addr_23 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_22
  %C_load_23 = load float* %C_addr_23, align 4
  %tmp_119_22 = fmul float %C_load_23, %k_load_26
  %tmp_120_22 = fadd float %tmp_120_21, %tmp_119_22
  %Q_addr_23 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_22
  %Q_load_23 = load float* %Q_addr_23, align 4
  %tmp_121_22 = fmul float %Q_load_23, %k_load_26
  %tmp_122_22 = fadd float %tmp_122_21, %tmp_121_22
  %tmp_116_23 = add i14 %phi_mul3, 24
  %tmp_117_23 = zext i14 %tmp_116_23 to i64
  %C_addr_24 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_23
  %C_load_24 = load float* %C_addr_24, align 4
  %tmp_119_23 = fmul float %C_load_24, %k_load_27
  %tmp_120_23 = fadd float %tmp_120_22, %tmp_119_23
  %Q_addr_24 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_23
  %Q_load_24 = load float* %Q_addr_24, align 4
  %tmp_121_23 = fmul float %Q_load_24, %k_load_27
  %tmp_122_23 = fadd float %tmp_122_22, %tmp_121_23
  %tmp_116_24 = add i14 %phi_mul3, 25
  %tmp_117_24 = zext i14 %tmp_116_24 to i64
  %C_addr_25 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_24
  %C_load_25 = load float* %C_addr_25, align 4
  %tmp_119_24 = fmul float %C_load_25, %k_load_28
  %tmp_120_24 = fadd float %tmp_120_23, %tmp_119_24
  %Q_addr_25 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_24
  %Q_load_25 = load float* %Q_addr_25, align 4
  %tmp_121_24 = fmul float %Q_load_25, %k_load_28
  %tmp_122_24 = fadd float %tmp_122_23, %tmp_121_24
  %tmp_116_25 = add i14 %phi_mul3, 26
  %tmp_117_25 = zext i14 %tmp_116_25 to i64
  %C_addr_26 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_25
  %C_load_26 = load float* %C_addr_26, align 4
  %tmp_119_25 = fmul float %C_load_26, %k_load_29
  %tmp_120_25 = fadd float %tmp_120_24, %tmp_119_25
  %Q_addr_26 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_25
  %Q_load_26 = load float* %Q_addr_26, align 4
  %tmp_121_25 = fmul float %Q_load_26, %k_load_29
  %tmp_122_25 = fadd float %tmp_122_24, %tmp_121_25
  %tmp_116_26 = add i14 %phi_mul3, 27
  %tmp_117_26 = zext i14 %tmp_116_26 to i64
  %C_addr_27 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_26
  %C_load_27 = load float* %C_addr_27, align 4
  %tmp_119_26 = fmul float %C_load_27, %k_load_30
  %tmp_120_26 = fadd float %tmp_120_25, %tmp_119_26
  %Q_addr_27 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_26
  %Q_load_27 = load float* %Q_addr_27, align 4
  %tmp_121_26 = fmul float %Q_load_27, %k_load_30
  %tmp_122_26 = fadd float %tmp_122_25, %tmp_121_26
  %tmp_116_27 = add i14 %phi_mul3, 28
  %tmp_117_27 = zext i14 %tmp_116_27 to i64
  %C_addr_28 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_27
  %C_load_28 = load float* %C_addr_28, align 4
  %tmp_119_27 = fmul float %C_load_28, %k_load_31
  %tmp_120_27 = fadd float %tmp_120_26, %tmp_119_27
  %Q_addr_28 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_27
  %Q_load_28 = load float* %Q_addr_28, align 4
  %tmp_121_27 = fmul float %Q_load_28, %k_load_31
  %tmp_122_27 = fadd float %tmp_122_26, %tmp_121_27
  %tmp_116_28 = add i14 %phi_mul3, 29
  %tmp_117_28 = zext i14 %tmp_116_28 to i64
  %C_addr_29 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_28
  %C_load_29 = load float* %C_addr_29, align 4
  %tmp_119_28 = fmul float %C_load_29, %k_load_32
  %tmp_120_28 = fadd float %tmp_120_27, %tmp_119_28
  %Q_addr_29 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_28
  %Q_load_29 = load float* %Q_addr_29, align 4
  %tmp_121_28 = fmul float %Q_load_29, %k_load_32
  %tmp_122_28 = fadd float %tmp_122_27, %tmp_121_28
  %tmp_116_29 = add i14 %phi_mul3, 30
  %tmp_117_29 = zext i14 %tmp_116_29 to i64
  %C_addr_30 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_29
  %C_load_30 = load float* %C_addr_30, align 4
  %tmp_119_29 = fmul float %C_load_30, %k_load_33
  %tmp_120_29 = fadd float %tmp_120_28, %tmp_119_29
  %Q_addr_30 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_29
  %Q_load_30 = load float* %Q_addr_30, align 4
  %tmp_121_29 = fmul float %Q_load_30, %k_load_33
  %tmp_122_29 = fadd float %tmp_122_28, %tmp_121_29
  %tmp_116_30 = add i14 %phi_mul3, 31
  %tmp_117_30 = zext i14 %tmp_116_30 to i64
  %C_addr_31 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_30
  %C_load_31 = load float* %C_addr_31, align 4
  %tmp_119_30 = fmul float %C_load_31, %k_load_34
  %tmp_120_30 = fadd float %tmp_120_29, %tmp_119_30
  %Q_addr_31 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_30
  %Q_load_31 = load float* %Q_addr_31, align 4
  %tmp_121_30 = fmul float %Q_load_31, %k_load_34
  %tmp_122_30 = fadd float %tmp_122_29, %tmp_121_30
  %tmp_116_31 = add i14 %phi_mul3, 32
  %tmp_117_31 = zext i14 %tmp_116_31 to i64
  %C_addr_32 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_31
  %C_load_32 = load float* %C_addr_32, align 4
  %tmp_119_31 = fmul float %C_load_32, %k_load_35
  %tmp_120_31 = fadd float %tmp_120_30, %tmp_119_31
  %Q_addr_32 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_31
  %Q_load_32 = load float* %Q_addr_32, align 4
  %tmp_121_31 = fmul float %Q_load_32, %k_load_35
  %tmp_122_31 = fadd float %tmp_122_30, %tmp_121_31
  %tmp_116_32 = add i14 %phi_mul3, 33
  %tmp_117_32 = zext i14 %tmp_116_32 to i64
  %C_addr_33 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_32
  %C_load_33 = load float* %C_addr_33, align 4
  %tmp_119_32 = fmul float %C_load_33, %k_load_36
  %tmp_120_32 = fadd float %tmp_120_31, %tmp_119_32
  %Q_addr_33 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_32
  %Q_load_33 = load float* %Q_addr_33, align 4
  %tmp_121_32 = fmul float %Q_load_33, %k_load_36
  %tmp_122_32 = fadd float %tmp_122_31, %tmp_121_32
  %tmp_116_33 = add i14 %phi_mul3, 34
  %tmp_117_33 = zext i14 %tmp_116_33 to i64
  %C_addr_34 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_33
  %C_load_34 = load float* %C_addr_34, align 4
  %tmp_119_33 = fmul float %C_load_34, %k_load_37
  %tmp_120_33 = fadd float %tmp_120_32, %tmp_119_33
  %Q_addr_34 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_33
  %Q_load_34 = load float* %Q_addr_34, align 4
  %tmp_121_33 = fmul float %Q_load_34, %k_load_37
  %tmp_122_33 = fadd float %tmp_122_32, %tmp_121_33
  %tmp_116_34 = add i14 %phi_mul3, 35
  %tmp_117_34 = zext i14 %tmp_116_34 to i64
  %C_addr_35 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_34
  %C_load_35 = load float* %C_addr_35, align 4
  %tmp_119_34 = fmul float %C_load_35, %k_load_38
  %tmp_120_34 = fadd float %tmp_120_33, %tmp_119_34
  %Q_addr_35 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_34
  %Q_load_35 = load float* %Q_addr_35, align 4
  %tmp_121_34 = fmul float %Q_load_35, %k_load_38
  %tmp_122_34 = fadd float %tmp_122_33, %tmp_121_34
  %tmp_116_35 = add i14 %phi_mul3, 36
  %tmp_117_35 = zext i14 %tmp_116_35 to i64
  %C_addr_36 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_35
  %C_load_36 = load float* %C_addr_36, align 4
  %tmp_119_35 = fmul float %C_load_36, %k_load_39
  %tmp_120_35 = fadd float %tmp_120_34, %tmp_119_35
  %Q_addr_36 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_35
  %Q_load_36 = load float* %Q_addr_36, align 4
  %tmp_121_35 = fmul float %Q_load_36, %k_load_39
  %tmp_122_35 = fadd float %tmp_122_34, %tmp_121_35
  %tmp_116_36 = add i14 %phi_mul3, 37
  %tmp_117_36 = zext i14 %tmp_116_36 to i64
  %C_addr_37 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_36
  %C_load_37 = load float* %C_addr_37, align 4
  %tmp_119_36 = fmul float %C_load_37, %k_load_40
  %tmp_120_36 = fadd float %tmp_120_35, %tmp_119_36
  %Q_addr_37 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_36
  %Q_load_37 = load float* %Q_addr_37, align 4
  %tmp_121_36 = fmul float %Q_load_37, %k_load_40
  %tmp_122_36 = fadd float %tmp_122_35, %tmp_121_36
  %tmp_116_37 = add i14 %phi_mul3, 38
  %tmp_117_37 = zext i14 %tmp_116_37 to i64
  %C_addr_38 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_37
  %C_load_38 = load float* %C_addr_38, align 4
  %tmp_119_37 = fmul float %C_load_38, %k_load_41
  %tmp_120_37 = fadd float %tmp_120_36, %tmp_119_37
  %Q_addr_38 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_37
  %Q_load_38 = load float* %Q_addr_38, align 4
  %tmp_121_37 = fmul float %Q_load_38, %k_load_41
  %tmp_122_37 = fadd float %tmp_122_36, %tmp_121_37
  %tmp_116_38 = add i14 %phi_mul3, 39
  %tmp_117_38 = zext i14 %tmp_116_38 to i64
  %C_addr_39 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_38
  %C_load_39 = load float* %C_addr_39, align 4
  %tmp_119_38 = fmul float %C_load_39, %k_load_42
  %tmp_120_38 = fadd float %tmp_120_37, %tmp_119_38
  %Q_addr_39 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_38
  %Q_load_39 = load float* %Q_addr_39, align 4
  %tmp_121_38 = fmul float %Q_load_39, %k_load_42
  %tmp_122_38 = fadd float %tmp_122_37, %tmp_121_38
  %tmp_116_39 = add i14 %phi_mul3, 40
  %tmp_117_39 = zext i14 %tmp_116_39 to i64
  %C_addr_40 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_39
  %C_load_40 = load float* %C_addr_40, align 4
  %tmp_119_39 = fmul float %C_load_40, %k_load_43
  %tmp_120_39 = fadd float %tmp_120_38, %tmp_119_39
  %Q_addr_40 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_39
  %Q_load_40 = load float* %Q_addr_40, align 4
  %tmp_121_39 = fmul float %Q_load_40, %k_load_43
  %tmp_122_39 = fadd float %tmp_122_38, %tmp_121_39
  %tmp_116_40 = add i14 %phi_mul3, 41
  %tmp_117_40 = zext i14 %tmp_116_40 to i64
  %C_addr_41 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_40
  %C_load_41 = load float* %C_addr_41, align 4
  %tmp_119_40 = fmul float %C_load_41, %k_load_44
  %tmp_120_40 = fadd float %tmp_120_39, %tmp_119_40
  %Q_addr_41 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_40
  %Q_load_41 = load float* %Q_addr_41, align 4
  %tmp_121_40 = fmul float %Q_load_41, %k_load_44
  %tmp_122_40 = fadd float %tmp_122_39, %tmp_121_40
  %tmp_116_41 = add i14 %phi_mul3, 42
  %tmp_117_41 = zext i14 %tmp_116_41 to i64
  %C_addr_42 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_41
  %C_load_42 = load float* %C_addr_42, align 4
  %tmp_119_41 = fmul float %C_load_42, %k_load_45
  %tmp_120_41 = fadd float %tmp_120_40, %tmp_119_41
  %Q_addr_42 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_41
  %Q_load_42 = load float* %Q_addr_42, align 4
  %tmp_121_41 = fmul float %Q_load_42, %k_load_45
  %tmp_122_41 = fadd float %tmp_122_40, %tmp_121_41
  %tmp_116_42 = add i14 %phi_mul3, 43
  %tmp_117_42 = zext i14 %tmp_116_42 to i64
  %C_addr_43 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_42
  %C_load_43 = load float* %C_addr_43, align 4
  %tmp_119_42 = fmul float %C_load_43, %k_load_46
  %tmp_120_42 = fadd float %tmp_120_41, %tmp_119_42
  %Q_addr_43 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_42
  %Q_load_43 = load float* %Q_addr_43, align 4
  %tmp_121_42 = fmul float %Q_load_43, %k_load_46
  %tmp_122_42 = fadd float %tmp_122_41, %tmp_121_42
  %tmp_116_43 = add i14 %phi_mul3, 44
  %tmp_117_43 = zext i14 %tmp_116_43 to i64
  %C_addr_44 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_43
  %C_load_44 = load float* %C_addr_44, align 4
  %tmp_119_43 = fmul float %C_load_44, %k_load_47
  %tmp_120_43 = fadd float %tmp_120_42, %tmp_119_43
  %Q_addr_44 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_43
  %Q_load_44 = load float* %Q_addr_44, align 4
  %tmp_121_43 = fmul float %Q_load_44, %k_load_47
  %tmp_122_43 = fadd float %tmp_122_42, %tmp_121_43
  %tmp_116_44 = add i14 %phi_mul3, 45
  %tmp_117_44 = zext i14 %tmp_116_44 to i64
  %C_addr_45 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_44
  %C_load_45 = load float* %C_addr_45, align 4
  %tmp_119_44 = fmul float %C_load_45, %k_load_48
  %tmp_120_44 = fadd float %tmp_120_43, %tmp_119_44
  %Q_addr_45 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_44
  %Q_load_45 = load float* %Q_addr_45, align 4
  %tmp_121_44 = fmul float %Q_load_45, %k_load_48
  %tmp_122_44 = fadd float %tmp_122_43, %tmp_121_44
  %tmp_116_45 = add i14 %phi_mul3, 46
  %tmp_117_45 = zext i14 %tmp_116_45 to i64
  %C_addr_46 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_45
  %C_load_46 = load float* %C_addr_46, align 4
  %tmp_119_45 = fmul float %C_load_46, %k_load_49
  %tmp_120_45 = fadd float %tmp_120_44, %tmp_119_45
  %Q_addr_46 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_45
  %Q_load_46 = load float* %Q_addr_46, align 4
  %tmp_121_45 = fmul float %Q_load_46, %k_load_49
  %tmp_122_45 = fadd float %tmp_122_44, %tmp_121_45
  %tmp_116_46 = add i14 %phi_mul3, 47
  %tmp_117_46 = zext i14 %tmp_116_46 to i64
  %C_addr_47 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_46
  %C_load_47 = load float* %C_addr_47, align 4
  %tmp_119_46 = fmul float %C_load_47, %k_load_50
  %tmp_120_46 = fadd float %tmp_120_45, %tmp_119_46
  %Q_addr_47 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_46
  %Q_load_47 = load float* %Q_addr_47, align 4
  %tmp_121_46 = fmul float %Q_load_47, %k_load_50
  %tmp_122_46 = fadd float %tmp_122_45, %tmp_121_46
  %tmp_116_47 = add i14 %phi_mul3, 48
  %tmp_117_47 = zext i14 %tmp_116_47 to i64
  %C_addr_48 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_47
  %C_load_48 = load float* %C_addr_48, align 4
  %tmp_119_47 = fmul float %C_load_48, %k_load_51
  %tmp_120_47 = fadd float %tmp_120_46, %tmp_119_47
  %Q_addr_48 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_47
  %Q_load_48 = load float* %Q_addr_48, align 4
  %tmp_121_47 = fmul float %Q_load_48, %k_load_51
  %tmp_122_47 = fadd float %tmp_122_46, %tmp_121_47
  %tmp_116_48 = add i14 %phi_mul3, 49
  %tmp_117_48 = zext i14 %tmp_116_48 to i64
  %C_addr_49 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_48
  %C_load_49 = load float* %C_addr_49, align 4
  %tmp_119_48 = fmul float %C_load_49, %k_load_52
  %tmp_120_48 = fadd float %tmp_120_47, %tmp_119_48
  %Q_addr_49 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_48
  %Q_load_49 = load float* %Q_addr_49, align 4
  %tmp_121_48 = fmul float %Q_load_49, %k_load_52
  %tmp_122_48 = fadd float %tmp_122_47, %tmp_121_48
  %tmp_116_49 = add i14 %phi_mul3, 50
  %tmp_117_49 = zext i14 %tmp_116_49 to i64
  %C_addr_50 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_49
  %C_load_50 = load float* %C_addr_50, align 4
  %tmp_119_49 = fmul float %C_load_50, %k_load_53
  %tmp_120_49 = fadd float %tmp_120_48, %tmp_119_49
  %Q_addr_50 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_49
  %Q_load_50 = load float* %Q_addr_50, align 4
  %tmp_121_49 = fmul float %Q_load_50, %k_load_53
  %tmp_122_49 = fadd float %tmp_122_48, %tmp_121_49
  %tmp_116_50 = add i14 %phi_mul3, 51
  %tmp_117_50 = zext i14 %tmp_116_50 to i64
  %C_addr_51 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_50
  %C_load_51 = load float* %C_addr_51, align 4
  %tmp_119_50 = fmul float %C_load_51, %k_load_54
  %tmp_120_50 = fadd float %tmp_120_49, %tmp_119_50
  %Q_addr_51 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_50
  %Q_load_51 = load float* %Q_addr_51, align 4
  %tmp_121_50 = fmul float %Q_load_51, %k_load_54
  %tmp_122_50 = fadd float %tmp_122_49, %tmp_121_50
  %tmp_116_51 = add i14 %phi_mul3, 52
  %tmp_117_51 = zext i14 %tmp_116_51 to i64
  %C_addr_52 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_51
  %C_load_52 = load float* %C_addr_52, align 4
  %tmp_119_51 = fmul float %C_load_52, %k_load_55
  %tmp_120_51 = fadd float %tmp_120_50, %tmp_119_51
  %Q_addr_52 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_51
  %Q_load_52 = load float* %Q_addr_52, align 4
  %tmp_121_51 = fmul float %Q_load_52, %k_load_55
  %tmp_122_51 = fadd float %tmp_122_50, %tmp_121_51
  %tmp_116_52 = add i14 %phi_mul3, 53
  %tmp_117_52 = zext i14 %tmp_116_52 to i64
  %C_addr_53 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_52
  %C_load_53 = load float* %C_addr_53, align 4
  %tmp_119_52 = fmul float %C_load_53, %k_load_56
  %tmp_120_52 = fadd float %tmp_120_51, %tmp_119_52
  %Q_addr_53 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_52
  %Q_load_53 = load float* %Q_addr_53, align 4
  %tmp_121_52 = fmul float %Q_load_53, %k_load_56
  %tmp_122_52 = fadd float %tmp_122_51, %tmp_121_52
  %tmp_116_53 = add i14 %phi_mul3, 54
  %tmp_117_53 = zext i14 %tmp_116_53 to i64
  %C_addr_54 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_53
  %C_load_54 = load float* %C_addr_54, align 4
  %tmp_119_53 = fmul float %C_load_54, %k_load_57
  %tmp_120_53 = fadd float %tmp_120_52, %tmp_119_53
  %Q_addr_54 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_53
  %Q_load_54 = load float* %Q_addr_54, align 4
  %tmp_121_53 = fmul float %Q_load_54, %k_load_57
  %tmp_122_53 = fadd float %tmp_122_52, %tmp_121_53
  %tmp_116_54 = add i14 %phi_mul3, 55
  %tmp_117_54 = zext i14 %tmp_116_54 to i64
  %C_addr_55 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_54
  %C_load_55 = load float* %C_addr_55, align 4
  %tmp_119_54 = fmul float %C_load_55, %k_load_58
  %tmp_120_54 = fadd float %tmp_120_53, %tmp_119_54
  %Q_addr_55 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_54
  %Q_load_55 = load float* %Q_addr_55, align 4
  %tmp_121_54 = fmul float %Q_load_55, %k_load_58
  %tmp_122_54 = fadd float %tmp_122_53, %tmp_121_54
  %tmp_116_55 = add i14 %phi_mul3, 56
  %tmp_117_55 = zext i14 %tmp_116_55 to i64
  %C_addr_56 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_55
  %C_load_56 = load float* %C_addr_56, align 4
  %tmp_119_55 = fmul float %C_load_56, %k_load_59
  %tmp_120_55 = fadd float %tmp_120_54, %tmp_119_55
  %Q_addr_56 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_55
  %Q_load_56 = load float* %Q_addr_56, align 4
  %tmp_121_55 = fmul float %Q_load_56, %k_load_59
  %tmp_122_55 = fadd float %tmp_122_54, %tmp_121_55
  %tmp_116_56 = add i14 %phi_mul3, 57
  %tmp_117_56 = zext i14 %tmp_116_56 to i64
  %C_addr_57 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_56
  %C_load_57 = load float* %C_addr_57, align 4
  %tmp_119_56 = fmul float %C_load_57, %k_load_60
  %tmp_120_56 = fadd float %tmp_120_55, %tmp_119_56
  %Q_addr_57 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_56
  %Q_load_57 = load float* %Q_addr_57, align 4
  %tmp_121_56 = fmul float %Q_load_57, %k_load_60
  %tmp_122_56 = fadd float %tmp_122_55, %tmp_121_56
  %tmp_116_57 = add i14 %phi_mul3, 58
  %tmp_117_57 = zext i14 %tmp_116_57 to i64
  %C_addr_58 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_57
  %C_load_58 = load float* %C_addr_58, align 4
  %tmp_119_57 = fmul float %C_load_58, %k_load_61
  %tmp_120_57 = fadd float %tmp_120_56, %tmp_119_57
  %Q_addr_58 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_57
  %Q_load_58 = load float* %Q_addr_58, align 4
  %tmp_121_57 = fmul float %Q_load_58, %k_load_61
  %tmp_122_57 = fadd float %tmp_122_56, %tmp_121_57
  %tmp_116_58 = add i14 %phi_mul3, 59
  %tmp_117_58 = zext i14 %tmp_116_58 to i64
  %C_addr_59 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_58
  %C_load_59 = load float* %C_addr_59, align 4
  %tmp_119_58 = fmul float %C_load_59, %k_load_62
  %tmp_120_58 = fadd float %tmp_120_57, %tmp_119_58
  %Q_addr_59 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_58
  %Q_load_59 = load float* %Q_addr_59, align 4
  %tmp_121_58 = fmul float %Q_load_59, %k_load_62
  %tmp_122_58 = fadd float %tmp_122_57, %tmp_121_58
  %tmp_116_59 = add i14 %phi_mul3, 60
  %tmp_117_59 = zext i14 %tmp_116_59 to i64
  %C_addr_60 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_59
  %C_load_60 = load float* %C_addr_60, align 4
  %tmp_119_59 = fmul float %C_load_60, %k_load_63
  %tmp_120_59 = fadd float %tmp_120_58, %tmp_119_59
  %Q_addr_60 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_59
  %Q_load_60 = load float* %Q_addr_60, align 4
  %tmp_121_59 = fmul float %Q_load_60, %k_load_63
  %tmp_122_59 = fadd float %tmp_122_58, %tmp_121_59
  %tmp_116_60 = add i14 %phi_mul3, 61
  %tmp_117_60 = zext i14 %tmp_116_60 to i64
  %C_addr_61 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_60
  %C_load_61 = load float* %C_addr_61, align 4
  %tmp_119_60 = fmul float %C_load_61, %k_load_64
  %tmp_120_60 = fadd float %tmp_120_59, %tmp_119_60
  %Q_addr_61 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_60
  %Q_load_61 = load float* %Q_addr_61, align 4
  %tmp_121_60 = fmul float %Q_load_61, %k_load_64
  %tmp_122_60 = fadd float %tmp_122_59, %tmp_121_60
  %tmp_116_61 = add i14 %phi_mul3, 62
  %tmp_117_61 = zext i14 %tmp_116_61 to i64
  %C_addr_62 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_61
  %C_load_62 = load float* %C_addr_62, align 4
  %tmp_119_61 = fmul float %C_load_62, %k_load_65
  %tmp_120_61 = fadd float %tmp_120_60, %tmp_119_61
  %Q_addr_62 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_61
  %Q_load_62 = load float* %Q_addr_62, align 4
  %tmp_121_61 = fmul float %Q_load_62, %k_load_65
  %tmp_122_61 = fadd float %tmp_122_60, %tmp_121_61
  %tmp_116_62 = add i14 %phi_mul3, 63
  %tmp_117_62 = zext i14 %tmp_116_62 to i64
  %C_addr_63 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_62
  %C_load_326 = load float* %C_addr_63, align 4
  %tmp_119_62 = fmul float %C_load_326, %k_load_66
  %tmp_120_62 = fadd float %tmp_120_61, %tmp_119_62
  %Q_addr_63 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_62
  %Q_load_63 = load float* %Q_addr_63, align 4
  %tmp_121_62 = fmul float %Q_load_63, %k_load_66
  %tmp_122_62 = fadd float %tmp_122_61, %tmp_121_62
  %tmp_116_63 = add i14 %phi_mul3, 64
  %tmp_117_63 = zext i14 %tmp_116_63 to i64
  %C_addr_64 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_63
  %C_load_64 = load float* %C_addr_64, align 4
  %tmp_119_63 = fmul float %C_load_64, %k_load_67
  %tmp_120_63 = fadd float %tmp_120_62, %tmp_119_63
  %Q_addr_64 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_63
  %Q_load_326 = load float* %Q_addr_64, align 4
  %tmp_121_63 = fmul float %Q_load_326, %k_load_67
  %tmp_122_63 = fadd float %tmp_122_62, %tmp_121_63
  %tmp_116_64 = add i14 %phi_mul3, 65
  %tmp_117_64 = zext i14 %tmp_116_64 to i64
  %C_addr_65 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_64
  %C_load_65 = load float* %C_addr_65, align 4
  %tmp_119_64 = fmul float %C_load_65, %k_load_68
  %tmp_120_64 = fadd float %tmp_120_63, %tmp_119_64
  %Q_addr_65 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_64
  %Q_load_65 = load float* %Q_addr_65, align 4
  %tmp_121_64 = fmul float %Q_load_65, %k_load_68
  %tmp_122_64 = fadd float %tmp_122_63, %tmp_121_64
  %tmp_116_65 = add i14 %phi_mul3, 66
  %tmp_117_65 = zext i14 %tmp_116_65 to i64
  %C_addr_66 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_65
  %C_load_66 = load float* %C_addr_66, align 4
  %tmp_119_65 = fmul float %C_load_66, %k_load_69
  %tmp_120_65 = fadd float %tmp_120_64, %tmp_119_65
  %Q_addr_66 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_65
  %Q_load_66 = load float* %Q_addr_66, align 4
  %tmp_121_65 = fmul float %Q_load_66, %k_load_69
  %tmp_122_65 = fadd float %tmp_122_64, %tmp_121_65
  %tmp_116_66 = add i14 %phi_mul3, 67
  %tmp_117_66 = zext i14 %tmp_116_66 to i64
  %C_addr_67 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_66
  %C_load_67 = load float* %C_addr_67, align 4
  %tmp_119_66 = fmul float %C_load_67, %k_load_70
  %tmp_120_66 = fadd float %tmp_120_65, %tmp_119_66
  %Q_addr_67 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_66
  %Q_load_67 = load float* %Q_addr_67, align 4
  %tmp_121_66 = fmul float %Q_load_67, %k_load_70
  %tmp_122_66 = fadd float %tmp_122_65, %tmp_121_66
  %tmp_116_67 = add i14 %phi_mul3, 68
  %tmp_117_67 = zext i14 %tmp_116_67 to i64
  %C_addr_68 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_67
  %C_load_68 = load float* %C_addr_68, align 4
  %tmp_119_67 = fmul float %C_load_68, %k_load_71
  %tmp_120_67 = fadd float %tmp_120_66, %tmp_119_67
  %Q_addr_68 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_67
  %Q_load_68 = load float* %Q_addr_68, align 4
  %tmp_121_67 = fmul float %Q_load_68, %k_load_71
  %tmp_122_67 = fadd float %tmp_122_66, %tmp_121_67
  %tmp_116_68 = add i14 %phi_mul3, 69
  %tmp_117_68 = zext i14 %tmp_116_68 to i64
  %C_addr_69 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_68
  %C_load_69 = load float* %C_addr_69, align 4
  %tmp_119_68 = fmul float %C_load_69, %k_load_72
  %tmp_120_68 = fadd float %tmp_120_67, %tmp_119_68
  %Q_addr_69 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_68
  %Q_load_69 = load float* %Q_addr_69, align 4
  %tmp_121_68 = fmul float %Q_load_69, %k_load_72
  %tmp_122_68 = fadd float %tmp_122_67, %tmp_121_68
  %tmp_116_69 = add i14 %phi_mul3, 70
  %tmp_117_69 = zext i14 %tmp_116_69 to i64
  %C_addr_70 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_69
  %C_load_70 = load float* %C_addr_70, align 4
  %tmp_119_69 = fmul float %C_load_70, %k_load_73
  %tmp_120_69 = fadd float %tmp_120_68, %tmp_119_69
  %Q_addr_70 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_69
  %Q_load_70 = load float* %Q_addr_70, align 4
  %tmp_121_69 = fmul float %Q_load_70, %k_load_73
  %tmp_122_69 = fadd float %tmp_122_68, %tmp_121_69
  %tmp_116_70 = add i14 %phi_mul3, 71
  %tmp_117_70 = zext i14 %tmp_116_70 to i64
  %C_addr_71 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_70
  %C_load_71 = load float* %C_addr_71, align 4
  %tmp_119_70 = fmul float %C_load_71, %k_load_74
  %tmp_120_70 = fadd float %tmp_120_69, %tmp_119_70
  %Q_addr_71 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_70
  %Q_load_71 = load float* %Q_addr_71, align 4
  %tmp_121_70 = fmul float %Q_load_71, %k_load_74
  %tmp_122_70 = fadd float %tmp_122_69, %tmp_121_70
  %tmp_116_71 = add i14 %phi_mul3, 72
  %tmp_117_71 = zext i14 %tmp_116_71 to i64
  %C_addr_72 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_71
  %C_load_72 = load float* %C_addr_72, align 4
  %tmp_119_71 = fmul float %C_load_72, %k_load_75
  %tmp_120_71 = fadd float %tmp_120_70, %tmp_119_71
  %Q_addr_72 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_71
  %Q_load_72 = load float* %Q_addr_72, align 4
  %tmp_121_71 = fmul float %Q_load_72, %k_load_75
  %tmp_122_71 = fadd float %tmp_122_70, %tmp_121_71
  %tmp_116_72 = add i14 %phi_mul3, 73
  %tmp_117_72 = zext i14 %tmp_116_72 to i64
  %C_addr_73 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_72
  %C_load_73 = load float* %C_addr_73, align 4
  %tmp_119_72 = fmul float %C_load_73, %k_load_76
  %tmp_120_72 = fadd float %tmp_120_71, %tmp_119_72
  %Q_addr_73 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_72
  %Q_load_73 = load float* %Q_addr_73, align 4
  %tmp_121_72 = fmul float %Q_load_73, %k_load_76
  %tmp_122_72 = fadd float %tmp_122_71, %tmp_121_72
  %tmp_116_73 = add i14 %phi_mul3, 74
  %tmp_117_73 = zext i14 %tmp_116_73 to i64
  %C_addr_74 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_73
  %C_load_74 = load float* %C_addr_74, align 4
  %tmp_119_73 = fmul float %C_load_74, %k_load_77
  %tmp_120_73 = fadd float %tmp_120_72, %tmp_119_73
  %Q_addr_74 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_73
  %Q_load_74 = load float* %Q_addr_74, align 4
  %tmp_121_73 = fmul float %Q_load_74, %k_load_77
  %tmp_122_73 = fadd float %tmp_122_72, %tmp_121_73
  %tmp_116_74 = add i14 %phi_mul3, 75
  %tmp_117_74 = zext i14 %tmp_116_74 to i64
  %C_addr_75 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_74
  %C_load_75 = load float* %C_addr_75, align 4
  %tmp_119_74 = fmul float %C_load_75, %k_load_78
  %tmp_120_74 = fadd float %tmp_120_73, %tmp_119_74
  %Q_addr_75 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_74
  %Q_load_75 = load float* %Q_addr_75, align 4
  %tmp_121_74 = fmul float %Q_load_75, %k_load_78
  %tmp_122_74 = fadd float %tmp_122_73, %tmp_121_74
  %tmp_116_75 = add i14 %phi_mul3, 76
  %tmp_117_75 = zext i14 %tmp_116_75 to i64
  %C_addr_103 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_75
  %C_load_76 = load float* %C_addr_103, align 4
  %tmp_119_75 = fmul float %C_load_76, %k_load_79
  %tmp_120_75 = fadd float %tmp_120_74, %tmp_119_75
  %Q_addr_76 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_75
  %Q_load_76 = load float* %Q_addr_76, align 4
  %tmp_121_75 = fmul float %Q_load_76, %k_load_79
  %tmp_122_75 = fadd float %tmp_122_74, %tmp_121_75
  %tmp_116_76 = add i14 %phi_mul3, 77
  %tmp_117_76 = zext i14 %tmp_116_76 to i64
  %C_addr_77 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_76
  %C_load_77 = load float* %C_addr_77, align 4
  %tmp_119_76 = fmul float %C_load_77, %k_load_80
  %tmp_120_76 = fadd float %tmp_120_75, %tmp_119_76
  %Q_addr_103 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_76
  %Q_load_77 = load float* %Q_addr_103, align 4
  %tmp_121_76 = fmul float %Q_load_77, %k_load_80
  %tmp_122_76 = fadd float %tmp_122_75, %tmp_121_76
  %tmp_116_77 = add i14 %phi_mul3, 78
  %tmp_117_77 = zext i14 %tmp_116_77 to i64
  %C_addr_78 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_77
  %C_load_78 = load float* %C_addr_78, align 4
  %tmp_119_77 = fmul float %C_load_78, %k_load_81
  %tmp_120_77 = fadd float %tmp_120_76, %tmp_119_77
  %Q_addr_78 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_77
  %Q_load_78 = load float* %Q_addr_78, align 4
  %tmp_121_77 = fmul float %Q_load_78, %k_load_81
  %tmp_122_77 = fadd float %tmp_122_76, %tmp_121_77
  %tmp_116_78 = add i14 %phi_mul3, 79
  %tmp_117_78 = zext i14 %tmp_116_78 to i64
  %C_addr_79 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_78
  %C_load_79 = load float* %C_addr_79, align 4
  %tmp_119_78 = fmul float %C_load_79, %k_load_82
  %tmp_120_78 = fadd float %tmp_120_77, %tmp_119_78
  %Q_addr_79 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_78
  %Q_load_79 = load float* %Q_addr_79, align 4
  %tmp_121_78 = fmul float %Q_load_79, %k_load_82
  %tmp_122_78 = fadd float %tmp_122_77, %tmp_121_78
  %tmp_116_79 = add i14 %phi_mul3, 80
  %tmp_117_79 = zext i14 %tmp_116_79 to i64
  %C_addr_80 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_79
  %C_load_80 = load float* %C_addr_80, align 4
  %tmp_119_79 = fmul float %C_load_80, %k_load_83
  %tmp_120_79 = fadd float %tmp_120_78, %tmp_119_79
  %Q_addr_80 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_79
  %Q_load_80 = load float* %Q_addr_80, align 4
  %tmp_121_79 = fmul float %Q_load_80, %k_load_83
  %tmp_122_79 = fadd float %tmp_122_78, %tmp_121_79
  %tmp_116_80 = add i14 %phi_mul3, 81
  %tmp_117_80 = zext i14 %tmp_116_80 to i64
  %C_addr_81 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_80
  %C_load_81 = load float* %C_addr_81, align 4
  %tmp_119_80 = fmul float %C_load_81, %k_load_84
  %tmp_120_80 = fadd float %tmp_120_79, %tmp_119_80
  %Q_addr_81 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_80
  %Q_load_81 = load float* %Q_addr_81, align 4
  %tmp_121_80 = fmul float %Q_load_81, %k_load_84
  %tmp_122_80 = fadd float %tmp_122_79, %tmp_121_80
  %tmp_116_81 = add i14 %phi_mul3, 82
  %tmp_117_81 = zext i14 %tmp_116_81 to i64
  %C_addr_82 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_81
  %C_load_82 = load float* %C_addr_82, align 4
  %tmp_119_81 = fmul float %C_load_82, %k_load_85
  %tmp_120_81 = fadd float %tmp_120_80, %tmp_119_81
  %Q_addr_82 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_81
  %Q_load_82 = load float* %Q_addr_82, align 4
  %tmp_121_81 = fmul float %Q_load_82, %k_load_85
  %tmp_122_81 = fadd float %tmp_122_80, %tmp_121_81
  %tmp_116_82 = add i14 %phi_mul3, 83
  %tmp_117_82 = zext i14 %tmp_116_82 to i64
  %C_addr_83 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_82
  %C_load_83 = load float* %C_addr_83, align 4
  %tmp_119_82 = fmul float %C_load_83, %k_load_86
  %tmp_120_82 = fadd float %tmp_120_81, %tmp_119_82
  %Q_addr_83 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_82
  %Q_load_83 = load float* %Q_addr_83, align 4
  %tmp_121_82 = fmul float %Q_load_83, %k_load_86
  %tmp_122_82 = fadd float %tmp_122_81, %tmp_121_82
  %tmp_116_83 = add i14 %phi_mul3, 84
  %tmp_117_83 = zext i14 %tmp_116_83 to i64
  %C_addr_84 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_83
  %C_load_84 = load float* %C_addr_84, align 4
  %tmp_119_83 = fmul float %C_load_84, %k_load_87
  %tmp_120_83 = fadd float %tmp_120_82, %tmp_119_83
  %Q_addr_84 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_83
  %Q_load_84 = load float* %Q_addr_84, align 4
  %tmp_121_83 = fmul float %Q_load_84, %k_load_87
  %tmp_122_83 = fadd float %tmp_122_82, %tmp_121_83
  %tmp_116_84 = add i14 %phi_mul3, 85
  %tmp_117_84 = zext i14 %tmp_116_84 to i64
  %C_addr_85 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_84
  %C_load_85 = load float* %C_addr_85, align 4
  %tmp_119_84 = fmul float %C_load_85, %k_load_88
  %tmp_120_84 = fadd float %tmp_120_83, %tmp_119_84
  %Q_addr_85 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_84
  %Q_load_85 = load float* %Q_addr_85, align 4
  %tmp_121_84 = fmul float %Q_load_85, %k_load_88
  %tmp_122_84 = fadd float %tmp_122_83, %tmp_121_84
  %tmp_116_85 = add i14 %phi_mul3, 86
  %tmp_117_85 = zext i14 %tmp_116_85 to i64
  %C_addr_86 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_85
  %C_load_86 = load float* %C_addr_86, align 4
  %tmp_119_85 = fmul float %C_load_86, %k_load_89
  %tmp_120_85 = fadd float %tmp_120_84, %tmp_119_85
  %Q_addr_86 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_85
  %Q_load_86 = load float* %Q_addr_86, align 4
  %tmp_121_85 = fmul float %Q_load_86, %k_load_89
  %tmp_122_85 = fadd float %tmp_122_84, %tmp_121_85
  %tmp_116_86 = add i14 %phi_mul3, 87
  %tmp_117_86 = zext i14 %tmp_116_86 to i64
  %C_addr_87 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_86
  %C_load_87 = load float* %C_addr_87, align 4
  %tmp_119_86 = fmul float %C_load_87, %k_load_90
  %tmp_120_86 = fadd float %tmp_120_85, %tmp_119_86
  %Q_addr_87 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_86
  %Q_load_87 = load float* %Q_addr_87, align 4
  %tmp_121_86 = fmul float %Q_load_87, %k_load_90
  %tmp_122_86 = fadd float %tmp_122_85, %tmp_121_86
  %tmp_116_87 = add i14 %phi_mul3, 88
  %tmp_117_87 = zext i14 %tmp_116_87 to i64
  %C_addr_88 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_87
  %C_load_88 = load float* %C_addr_88, align 4
  %tmp_119_87 = fmul float %C_load_88, %k_load_91
  %tmp_120_87 = fadd float %tmp_120_86, %tmp_119_87
  %Q_addr_88 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_87
  %Q_load_88 = load float* %Q_addr_88, align 4
  %tmp_121_87 = fmul float %Q_load_88, %k_load_91
  %tmp_122_87 = fadd float %tmp_122_86, %tmp_121_87
  %tmp_116_88 = add i14 %phi_mul3, 89
  %tmp_117_88 = zext i14 %tmp_116_88 to i64
  %C_addr_89 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_88
  %C_load_89 = load float* %C_addr_89, align 4
  %tmp_119_88 = fmul float %C_load_89, %k_load_92
  %tmp_120_88 = fadd float %tmp_120_87, %tmp_119_88
  %Q_addr_89 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_88
  %Q_load_89 = load float* %Q_addr_89, align 4
  %tmp_121_88 = fmul float %Q_load_89, %k_load_92
  %tmp_122_88 = fadd float %tmp_122_87, %tmp_121_88
  %tmp_116_89 = add i14 %phi_mul3, 90
  %tmp_117_89 = zext i14 %tmp_116_89 to i64
  %C_addr_90 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_89
  %C_load_90 = load float* %C_addr_90, align 4
  %tmp_119_89 = fmul float %C_load_90, %k_load_93
  %tmp_120_89 = fadd float %tmp_120_88, %tmp_119_89
  %Q_addr_90 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_89
  %Q_load_90 = load float* %Q_addr_90, align 4
  %tmp_121_89 = fmul float %Q_load_90, %k_load_93
  %tmp_122_89 = fadd float %tmp_122_88, %tmp_121_89
  %tmp_116_90 = add i14 %phi_mul3, 91
  %tmp_117_90 = zext i14 %tmp_116_90 to i64
  %C_addr_91 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_90
  %C_load_91 = load float* %C_addr_91, align 4
  %tmp_119_90 = fmul float %C_load_91, %k_load_94
  %tmp_120_90 = fadd float %tmp_120_89, %tmp_119_90
  %Q_addr_91 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_90
  %Q_load_91 = load float* %Q_addr_91, align 4
  %tmp_121_90 = fmul float %Q_load_91, %k_load_94
  %tmp_122_90 = fadd float %tmp_122_89, %tmp_121_90
  %tmp_116_91 = add i14 %phi_mul3, 92
  %tmp_117_91 = zext i14 %tmp_116_91 to i64
  %C_addr_92 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_91
  %C_load_92 = load float* %C_addr_92, align 4
  %tmp_119_91 = fmul float %C_load_92, %k_load_95
  %tmp_120_91 = fadd float %tmp_120_90, %tmp_119_91
  %Q_addr_92 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_91
  %Q_load_92 = load float* %Q_addr_92, align 4
  %tmp_121_91 = fmul float %Q_load_92, %k_load_95
  %tmp_122_91 = fadd float %tmp_122_90, %tmp_121_91
  %tmp_116_92 = add i14 %phi_mul3, 93
  %tmp_117_92 = zext i14 %tmp_116_92 to i64
  %C_addr_93 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_92
  %C_load_93 = load float* %C_addr_93, align 4
  %tmp_119_92 = fmul float %C_load_93, %k_load_96
  %tmp_120_92 = fadd float %tmp_120_91, %tmp_119_92
  %Q_addr_93 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_92
  %Q_load_93 = load float* %Q_addr_93, align 4
  %tmp_121_92 = fmul float %Q_load_93, %k_load_96
  %tmp_122_92 = fadd float %tmp_122_91, %tmp_121_92
  %tmp_116_93 = add i14 %phi_mul3, 94
  %tmp_117_93 = zext i14 %tmp_116_93 to i64
  %C_addr_94 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_93
  %C_load_94 = load float* %C_addr_94, align 4
  %tmp_119_93 = fmul float %C_load_94, %k_load_97
  %tmp_120_93 = fadd float %tmp_120_92, %tmp_119_93
  %Q_addr_94 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_93
  %Q_load_94 = load float* %Q_addr_94, align 4
  %tmp_121_93 = fmul float %Q_load_94, %k_load_97
  %tmp_122_93 = fadd float %tmp_122_92, %tmp_121_93
  %tmp_116_94 = add i14 %phi_mul3, 95
  %tmp_117_94 = zext i14 %tmp_116_94 to i64
  %C_addr_95 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_94
  %C_load_95 = load float* %C_addr_95, align 4
  %tmp_119_94 = fmul float %C_load_95, %k_load_98
  %tmp_120_94 = fadd float %tmp_120_93, %tmp_119_94
  %Q_addr_95 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_94
  %Q_load_95 = load float* %Q_addr_95, align 4
  %tmp_121_94 = fmul float %Q_load_95, %k_load_98
  %tmp_122_94 = fadd float %tmp_122_93, %tmp_121_94
  %tmp_116_95 = add i14 %phi_mul3, 96
  %tmp_117_95 = zext i14 %tmp_116_95 to i64
  %C_addr_96 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_95
  %C_load_96 = load float* %C_addr_96, align 4
  %tmp_119_95 = fmul float %C_load_96, %k_load_99
  %tmp_120_95 = fadd float %tmp_120_94, %tmp_119_95
  %Q_addr_96 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_95
  %Q_load_96 = load float* %Q_addr_96, align 4
  %tmp_121_95 = fmul float %Q_load_96, %k_load_99
  %tmp_122_95 = fadd float %tmp_122_94, %tmp_121_95
  %tmp_116_96 = add i14 %phi_mul3, 97
  %tmp_117_96 = zext i14 %tmp_116_96 to i64
  %C_addr_97 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_96
  %C_load_97 = load float* %C_addr_97, align 4
  %tmp_119_96 = fmul float %C_load_97, %k_load_100
  %tmp_120_96 = fadd float %tmp_120_95, %tmp_119_96
  %Q_addr_97 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_96
  %Q_load_97 = load float* %Q_addr_97, align 4
  %tmp_121_96 = fmul float %Q_load_97, %k_load_100
  %tmp_122_96 = fadd float %tmp_122_95, %tmp_121_96
  %tmp_116_97 = add i14 %phi_mul3, 98
  %tmp_117_97 = zext i14 %tmp_116_97 to i64
  %C_addr_98 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_97
  %C_load_98 = load float* %C_addr_98, align 4
  %tmp_119_97 = fmul float %C_load_98, %k_load_101
  %tmp_120_97 = fadd float %tmp_120_96, %tmp_119_97
  %Q_addr_98 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_97
  %Q_load_98 = load float* %Q_addr_98, align 4
  %tmp_121_97 = fmul float %Q_load_98, %k_load_101
  %tmp_122_97 = fadd float %tmp_122_96, %tmp_121_97
  %tmp_116_98 = add i14 %phi_mul3, 99
  %tmp_117_98 = zext i14 %tmp_116_98 to i64
  %C_addr_99 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_117_98
  %C_load_99 = load float* %C_addr_99, align 4
  %tmp_119_98 = fmul float %C_load_99, %k_load_102
  %tmp_120_98 = fadd float %tmp_120_97, %tmp_119_98
  store float %tmp_120_98, float* %s_addr, align 4
  %Q_addr_99 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_117_98
  %Q_load_99 = load float* %Q_addr_99, align 4
  %tmp_121_98 = fmul float %Q_load_99, %k_load_102
  %tmp_122_98 = fadd float %tmp_122_97, %tmp_121_98
  store float %tmp_122_98, float* %e_addr, align 4
  %empty_17 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str8, i32 %tmp_137)
  br label %.preheader10

.preheader9:                                      ; preds = %.preheader10, %6
  %i2 = phi i7 [ %i_4, %6 ], [ 0, %.preheader10 ]
  %sigma2 = phi float [ %sigma2_2, %6 ], [ 1.000000e+00, %.preheader10 ]
  %exitcond5 = icmp eq i7 %i2, -28
  %i_4 = add i7 %i2, 1
  br i1 %exitcond5, label %7, label %6

; <label>:6                                       ; preds = %.preheader9
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 100, i64 100, i64 100)
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str10) nounwind
  %tmp_140 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str10)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_97 = zext i7 %i2 to i64
  %s_addr_5 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp_97
  %s_load = load float* %s_addr_5, align 4
  %k_addr_4 = getelementptr inbounds [100 x float]* @k, i64 0, i64 %tmp_97
  %k_load = load float* %k_addr_4, align 4
  %tmp_98 = fmul float %s_load, %k_load
  %sigma2_2 = fadd float %sigma2, %tmp_98
  %empty_18 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str10, i32 %tmp_140)
  br label %.preheader9

; <label>:7                                       ; preds = %.preheader9
  store float 1.000000e+00, float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 100), align 16
  %tmp_90 = fsub float %pY_read, %m
  %tmp_91 = fpext float %tmp_90 to double
  %tmp_92 = fpext float %sigma2 to double
  %tmp_93 = fadd double %tmp_92, 9.000000e-02
  %tmp_94 = fdiv double %tmp_91, %tmp_93
  %q = fptrunc double %tmp_94 to float
  %tmp_95 = fdiv double -1.000000e+00, %tmp_93
  %r = fptrunc double %tmp_95 to float
  %e_load = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 0), align 16
  %k_load_103 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 0), align 16
  %tmp_125 = fmul float %e_load, %k_load_103
  %gamma_2 = fsub float 1.000000e+00, %tmp_125
  %s_load_4 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 0), align 16
  %tmp_126 = fmul float %s_load_4, %q
  %alpha_load_103 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 0), align 16
  %tmp_127 = fadd float %alpha_load_103, %tmp_126
  store float %tmp_127, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 0), align 16
  %e_load_1 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 1), align 4
  %k_load_104 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 1), align 4
  %tmp_125_1 = fmul float %e_load_1, %k_load_104
  %gamma_2_1 = fsub float %gamma_2, %tmp_125_1
  %s_load_5 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 1), align 4
  %tmp_126_1 = fmul float %s_load_5, %q
  %alpha_load_104 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 1), align 4
  %tmp_127_1 = fadd float %alpha_load_104, %tmp_126_1
  store float %tmp_127_1, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 1), align 4
  %e_load_2 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 2), align 8
  %k_load_105 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 2), align 8
  %tmp_125_2 = fmul float %e_load_2, %k_load_105
  %gamma_2_2 = fsub float %gamma_2_1, %tmp_125_2
  %s_load_6 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 2), align 8
  %tmp_126_2 = fmul float %s_load_6, %q
  %alpha_load_105 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 2), align 8
  %tmp_127_2 = fadd float %alpha_load_105, %tmp_126_2
  store float %tmp_127_2, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 2), align 8
  %e_load_100 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 3), align 4
  %k_load_106 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 3), align 4
  %tmp_125_3 = fmul float %e_load_100, %k_load_106
  %gamma_2_3 = fsub float %gamma_2_2, %tmp_125_3
  %s_load_7 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 3), align 4
  %tmp_126_3 = fmul float %s_load_7, %q
  %alpha_load_106 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 3), align 4
  %tmp_127_3 = fadd float %alpha_load_106, %tmp_126_3
  store float %tmp_127_3, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 3), align 4
  %e_load_101 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 4), align 16
  %k_load_107 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 4), align 16
  %tmp_125_4 = fmul float %e_load_101, %k_load_107
  %gamma_2_4 = fsub float %gamma_2_3, %tmp_125_4
  %s_load_8 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 4), align 16
  %tmp_126_4 = fmul float %s_load_8, %q
  %alpha_load_107 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 4), align 16
  %tmp_127_4 = fadd float %alpha_load_107, %tmp_126_4
  store float %tmp_127_4, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 4), align 16
  %e_load_5 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 5), align 4
  %k_load_108 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 5), align 4
  %tmp_125_5 = fmul float %e_load_5, %k_load_108
  %gamma_2_5 = fsub float %gamma_2_4, %tmp_125_5
  %s_load_9 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 5), align 4
  %tmp_126_5 = fmul float %s_load_9, %q
  %alpha_load_108 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 5), align 4
  %tmp_127_5 = fadd float %alpha_load_108, %tmp_126_5
  store float %tmp_127_5, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 5), align 4
  %e_load_6 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 6), align 8
  %k_load_109 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 6), align 8
  %tmp_125_6 = fmul float %e_load_6, %k_load_109
  %gamma_2_6 = fsub float %gamma_2_5, %tmp_125_6
  %s_load_10 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 6), align 8
  %tmp_126_6 = fmul float %s_load_10, %q
  %alpha_load_109 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 6), align 8
  %tmp_127_6 = fadd float %alpha_load_109, %tmp_126_6
  store float %tmp_127_6, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 6), align 8
  %e_load_7 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 7), align 4
  %k_load_110 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 7), align 4
  %tmp_125_7 = fmul float %e_load_7, %k_load_110
  %gamma_2_7 = fsub float %gamma_2_6, %tmp_125_7
  %s_load_11 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 7), align 4
  %tmp_126_7 = fmul float %s_load_11, %q
  %alpha_load_110 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 7), align 4
  %tmp_127_7 = fadd float %alpha_load_110, %tmp_126_7
  store float %tmp_127_7, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 7), align 4
  %e_load_8 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 8), align 16
  %k_load_111 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 8), align 16
  %tmp_125_8 = fmul float %e_load_8, %k_load_111
  %gamma_2_8 = fsub float %gamma_2_7, %tmp_125_8
  %s_load_12 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 8), align 16
  %tmp_126_8 = fmul float %s_load_12, %q
  %alpha_load_111 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 8), align 16
  %tmp_127_8 = fadd float %alpha_load_111, %tmp_126_8
  store float %tmp_127_8, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 8), align 16
  %e_load_9 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 9), align 4
  %k_load_112 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 9), align 4
  %tmp_125_9 = fmul float %e_load_9, %k_load_112
  %gamma_2_9 = fsub float %gamma_2_8, %tmp_125_9
  %s_load_13 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 9), align 4
  %tmp_126_9 = fmul float %s_load_13, %q
  %alpha_load_112 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 9), align 4
  %tmp_127_9 = fadd float %alpha_load_112, %tmp_126_9
  store float %tmp_127_9, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 9), align 4
  %e_load_10 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 10), align 8
  %k_load_113 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 10), align 8
  %tmp_125_s = fmul float %e_load_10, %k_load_113
  %gamma_2_s = fsub float %gamma_2_9, %tmp_125_s
  %s_load_14 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 10), align 8
  %tmp_126_s = fmul float %s_load_14, %q
  %alpha_load_113 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 10), align 8
  %tmp_127_s = fadd float %alpha_load_113, %tmp_126_s
  store float %tmp_127_s, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 10), align 8
  %e_load_11 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 11), align 4
  %k_load_114 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 11), align 4
  %tmp_125_10 = fmul float %e_load_11, %k_load_114
  %gamma_2_10 = fsub float %gamma_2_s, %tmp_125_10
  %s_load_15 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 11), align 4
  %tmp_126_10 = fmul float %s_load_15, %q
  %alpha_load_114 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 11), align 4
  %tmp_127_10 = fadd float %alpha_load_114, %tmp_126_10
  store float %tmp_127_10, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 11), align 4
  %e_load_12 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 12), align 16
  %k_load_115 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 12), align 16
  %tmp_125_11 = fmul float %e_load_12, %k_load_115
  %gamma_2_11 = fsub float %gamma_2_10, %tmp_125_11
  %s_load_16 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 12), align 16
  %tmp_126_11 = fmul float %s_load_16, %q
  %alpha_load_115 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 12), align 16
  %tmp_127_11 = fadd float %alpha_load_115, %tmp_126_11
  store float %tmp_127_11, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 12), align 16
  %e_load_13 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 13), align 4
  %k_load_116 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 13), align 4
  %tmp_125_12 = fmul float %e_load_13, %k_load_116
  %gamma_2_12 = fsub float %gamma_2_11, %tmp_125_12
  %s_load_17 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 13), align 4
  %tmp_126_12 = fmul float %s_load_17, %q
  %alpha_load_116 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 13), align 4
  %tmp_127_12 = fadd float %alpha_load_116, %tmp_126_12
  store float %tmp_127_12, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 13), align 4
  %e_load_14 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 14), align 8
  %k_load_117 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 14), align 8
  %tmp_125_13 = fmul float %e_load_14, %k_load_117
  %gamma_2_13 = fsub float %gamma_2_12, %tmp_125_13
  %s_load_18 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 14), align 8
  %tmp_126_13 = fmul float %s_load_18, %q
  %alpha_load_117 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 14), align 8
  %tmp_127_13 = fadd float %alpha_load_117, %tmp_126_13
  store float %tmp_127_13, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 14), align 8
  %e_load_15 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 15), align 4
  %k_load_118 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 15), align 4
  %tmp_125_14 = fmul float %e_load_15, %k_load_118
  %gamma_2_14 = fsub float %gamma_2_13, %tmp_125_14
  %s_load_19 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 15), align 4
  %tmp_126_14 = fmul float %s_load_19, %q
  %alpha_load_118 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 15), align 4
  %tmp_127_14 = fadd float %alpha_load_118, %tmp_126_14
  store float %tmp_127_14, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 15), align 4
  %e_load_16 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 16), align 16
  %k_load_119 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 16), align 16
  %tmp_125_15 = fmul float %e_load_16, %k_load_119
  %gamma_2_15 = fsub float %gamma_2_14, %tmp_125_15
  %s_load_20 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 16), align 16
  %tmp_126_15 = fmul float %s_load_20, %q
  %alpha_load_119 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 16), align 16
  %tmp_127_15 = fadd float %alpha_load_119, %tmp_126_15
  store float %tmp_127_15, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 16), align 16
  %e_load_17 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 17), align 4
  %k_load_120 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 17), align 4
  %tmp_125_16 = fmul float %e_load_17, %k_load_120
  %gamma_2_16 = fsub float %gamma_2_15, %tmp_125_16
  %s_load_21 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 17), align 4
  %tmp_126_16 = fmul float %s_load_21, %q
  %alpha_load_120 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 17), align 4
  %tmp_127_16 = fadd float %alpha_load_120, %tmp_126_16
  store float %tmp_127_16, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 17), align 4
  %e_load_18 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 18), align 8
  %k_load_121 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 18), align 8
  %tmp_125_17 = fmul float %e_load_18, %k_load_121
  %gamma_2_17 = fsub float %gamma_2_16, %tmp_125_17
  %s_load_22 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 18), align 8
  %tmp_126_17 = fmul float %s_load_22, %q
  %alpha_load_121 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 18), align 8
  %tmp_127_17 = fadd float %alpha_load_121, %tmp_126_17
  store float %tmp_127_17, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 18), align 8
  %e_load_19 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 19), align 4
  %k_load_122 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 19), align 4
  %tmp_125_18 = fmul float %e_load_19, %k_load_122
  %gamma_2_18 = fsub float %gamma_2_17, %tmp_125_18
  %s_load_23 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 19), align 4
  %tmp_126_18 = fmul float %s_load_23, %q
  %alpha_load_122 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 19), align 4
  %tmp_127_18 = fadd float %alpha_load_122, %tmp_126_18
  store float %tmp_127_18, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 19), align 4
  %e_load_20 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 20), align 16
  %k_load_123 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 20), align 16
  %tmp_125_19 = fmul float %e_load_20, %k_load_123
  %gamma_2_19 = fsub float %gamma_2_18, %tmp_125_19
  %s_load_24 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 20), align 16
  %tmp_126_19 = fmul float %s_load_24, %q
  %alpha_load_123 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 20), align 16
  %tmp_127_19 = fadd float %alpha_load_123, %tmp_126_19
  store float %tmp_127_19, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 20), align 16
  %e_load_21 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 21), align 4
  %k_load_124 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 21), align 4
  %tmp_125_20 = fmul float %e_load_21, %k_load_124
  %gamma_2_20 = fsub float %gamma_2_19, %tmp_125_20
  %s_load_25 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 21), align 4
  %tmp_126_20 = fmul float %s_load_25, %q
  %alpha_load_124 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 21), align 4
  %tmp_127_20 = fadd float %alpha_load_124, %tmp_126_20
  store float %tmp_127_20, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 21), align 4
  %e_load_22 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 22), align 8
  %k_load_125 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 22), align 8
  %tmp_125_21 = fmul float %e_load_22, %k_load_125
  %gamma_2_21 = fsub float %gamma_2_20, %tmp_125_21
  %s_load_26 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 22), align 8
  %tmp_126_21 = fmul float %s_load_26, %q
  %alpha_load_125 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 22), align 8
  %tmp_127_21 = fadd float %alpha_load_125, %tmp_126_21
  store float %tmp_127_21, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 22), align 8
  %e_load_23 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 23), align 4
  %k_load_126 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 23), align 4
  %tmp_125_22 = fmul float %e_load_23, %k_load_126
  %gamma_2_22 = fsub float %gamma_2_21, %tmp_125_22
  %s_load_27 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 23), align 4
  %tmp_126_22 = fmul float %s_load_27, %q
  %alpha_load_126 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 23), align 4
  %tmp_127_22 = fadd float %alpha_load_126, %tmp_126_22
  store float %tmp_127_22, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 23), align 4
  %e_load_24 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 24), align 16
  %k_load_127 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 24), align 16
  %tmp_125_23 = fmul float %e_load_24, %k_load_127
  %gamma_2_23 = fsub float %gamma_2_22, %tmp_125_23
  %s_load_28 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 24), align 16
  %tmp_126_23 = fmul float %s_load_28, %q
  %alpha_load_127 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 24), align 16
  %tmp_127_23 = fadd float %alpha_load_127, %tmp_126_23
  store float %tmp_127_23, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 24), align 16
  %e_load_25 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 25), align 4
  %k_load_128 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 25), align 4
  %tmp_125_24 = fmul float %e_load_25, %k_load_128
  %gamma_2_24 = fsub float %gamma_2_23, %tmp_125_24
  %s_load_29 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 25), align 4
  %tmp_126_24 = fmul float %s_load_29, %q
  %alpha_load_128 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 25), align 4
  %tmp_127_24 = fadd float %alpha_load_128, %tmp_126_24
  store float %tmp_127_24, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 25), align 4
  %e_load_26 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 26), align 8
  %k_load_129 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 26), align 8
  %tmp_125_25 = fmul float %e_load_26, %k_load_129
  %gamma_2_25 = fsub float %gamma_2_24, %tmp_125_25
  %s_load_30 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 26), align 8
  %tmp_126_25 = fmul float %s_load_30, %q
  %alpha_load_129 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 26), align 8
  %tmp_127_25 = fadd float %alpha_load_129, %tmp_126_25
  store float %tmp_127_25, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 26), align 8
  %e_load_27 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 27), align 4
  %k_load_130 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 27), align 4
  %tmp_125_26 = fmul float %e_load_27, %k_load_130
  %gamma_2_26 = fsub float %gamma_2_25, %tmp_125_26
  %s_load_31 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 27), align 4
  %tmp_126_26 = fmul float %s_load_31, %q
  %alpha_load_130 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 27), align 4
  %tmp_127_26 = fadd float %alpha_load_130, %tmp_126_26
  store float %tmp_127_26, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 27), align 4
  %e_load_28 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 28), align 16
  %k_load_131 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 28), align 16
  %tmp_125_27 = fmul float %e_load_28, %k_load_131
  %gamma_2_27 = fsub float %gamma_2_26, %tmp_125_27
  %s_load_32 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 28), align 16
  %tmp_126_27 = fmul float %s_load_32, %q
  %alpha_load_131 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 28), align 16
  %tmp_127_27 = fadd float %alpha_load_131, %tmp_126_27
  store float %tmp_127_27, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 28), align 16
  %e_load_29 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 29), align 4
  %k_load_132 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 29), align 4
  %tmp_125_28 = fmul float %e_load_29, %k_load_132
  %gamma_2_28 = fsub float %gamma_2_27, %tmp_125_28
  %s_load_33 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 29), align 4
  %tmp_126_28 = fmul float %s_load_33, %q
  %alpha_load_132 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 29), align 4
  %tmp_127_28 = fadd float %alpha_load_132, %tmp_126_28
  store float %tmp_127_28, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 29), align 4
  %e_load_30 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 30), align 8
  %k_load_133 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 30), align 8
  %tmp_125_29 = fmul float %e_load_30, %k_load_133
  %gamma_2_29 = fsub float %gamma_2_28, %tmp_125_29
  %s_load_34 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 30), align 8
  %tmp_126_29 = fmul float %s_load_34, %q
  %alpha_load_133 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 30), align 8
  %tmp_127_29 = fadd float %alpha_load_133, %tmp_126_29
  store float %tmp_127_29, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 30), align 8
  %e_load_31 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 31), align 4
  %k_load_134 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 31), align 4
  %tmp_125_30 = fmul float %e_load_31, %k_load_134
  %gamma_2_30 = fsub float %gamma_2_29, %tmp_125_30
  %s_load_35 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 31), align 4
  %tmp_126_30 = fmul float %s_load_35, %q
  %alpha_load_134 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 31), align 4
  %tmp_127_30 = fadd float %alpha_load_134, %tmp_126_30
  store float %tmp_127_30, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 31), align 4
  %e_load_32 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 32), align 16
  %k_load_135 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 32), align 16
  %tmp_125_31 = fmul float %e_load_32, %k_load_135
  %gamma_2_31 = fsub float %gamma_2_30, %tmp_125_31
  %s_load_36 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 32), align 16
  %tmp_126_31 = fmul float %s_load_36, %q
  %alpha_load_135 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 32), align 16
  %tmp_127_31 = fadd float %alpha_load_135, %tmp_126_31
  store float %tmp_127_31, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 32), align 16
  %e_load_33 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 33), align 4
  %k_load_136 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 33), align 4
  %tmp_125_32 = fmul float %e_load_33, %k_load_136
  %gamma_2_32 = fsub float %gamma_2_31, %tmp_125_32
  %s_load_37 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 33), align 4
  %tmp_126_32 = fmul float %s_load_37, %q
  %alpha_load_136 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 33), align 4
  %tmp_127_32 = fadd float %alpha_load_136, %tmp_126_32
  store float %tmp_127_32, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 33), align 4
  %e_load_34 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 34), align 8
  %k_load_137 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 34), align 8
  %tmp_125_33 = fmul float %e_load_34, %k_load_137
  %gamma_2_33 = fsub float %gamma_2_32, %tmp_125_33
  %s_load_38 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 34), align 8
  %tmp_126_33 = fmul float %s_load_38, %q
  %alpha_load_137 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 34), align 8
  %tmp_127_33 = fadd float %alpha_load_137, %tmp_126_33
  store float %tmp_127_33, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 34), align 8
  %e_load_35 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 35), align 4
  %k_load_138 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 35), align 4
  %tmp_125_34 = fmul float %e_load_35, %k_load_138
  %gamma_2_34 = fsub float %gamma_2_33, %tmp_125_34
  %s_load_39 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 35), align 4
  %tmp_126_34 = fmul float %s_load_39, %q
  %alpha_load_138 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 35), align 4
  %tmp_127_34 = fadd float %alpha_load_138, %tmp_126_34
  store float %tmp_127_34, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 35), align 4
  %e_load_36 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 36), align 16
  %k_load_139 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 36), align 16
  %tmp_125_35 = fmul float %e_load_36, %k_load_139
  %gamma_2_35 = fsub float %gamma_2_34, %tmp_125_35
  %s_load_40 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 36), align 16
  %tmp_126_35 = fmul float %s_load_40, %q
  %alpha_load_139 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 36), align 16
  %tmp_127_35 = fadd float %alpha_load_139, %tmp_126_35
  store float %tmp_127_35, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 36), align 16
  %e_load_37 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 37), align 4
  %k_load_140 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 37), align 4
  %tmp_125_36 = fmul float %e_load_37, %k_load_140
  %gamma_2_36 = fsub float %gamma_2_35, %tmp_125_36
  %s_load_41 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 37), align 4
  %tmp_126_36 = fmul float %s_load_41, %q
  %alpha_load_140 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 37), align 4
  %tmp_127_36 = fadd float %alpha_load_140, %tmp_126_36
  store float %tmp_127_36, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 37), align 4
  %e_load_38 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 38), align 8
  %k_load_141 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 38), align 8
  %tmp_125_37 = fmul float %e_load_38, %k_load_141
  %gamma_2_37 = fsub float %gamma_2_36, %tmp_125_37
  %s_load_42 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 38), align 8
  %tmp_126_37 = fmul float %s_load_42, %q
  %alpha_load_141 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 38), align 8
  %tmp_127_37 = fadd float %alpha_load_141, %tmp_126_37
  store float %tmp_127_37, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 38), align 8
  %e_load_39 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 39), align 4
  %k_load_142 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 39), align 4
  %tmp_125_38 = fmul float %e_load_39, %k_load_142
  %gamma_2_38 = fsub float %gamma_2_37, %tmp_125_38
  %s_load_43 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 39), align 4
  %tmp_126_38 = fmul float %s_load_43, %q
  %alpha_load_142 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 39), align 4
  %tmp_127_38 = fadd float %alpha_load_142, %tmp_126_38
  store float %tmp_127_38, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 39), align 4
  %e_load_40 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 40), align 16
  %k_load_143 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 40), align 16
  %tmp_125_39 = fmul float %e_load_40, %k_load_143
  %gamma_2_39 = fsub float %gamma_2_38, %tmp_125_39
  %s_load_44 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 40), align 16
  %tmp_126_39 = fmul float %s_load_44, %q
  %alpha_load_143 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 40), align 16
  %tmp_127_39 = fadd float %alpha_load_143, %tmp_126_39
  store float %tmp_127_39, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 40), align 16
  %e_load_41 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 41), align 4
  %k_load_144 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 41), align 4
  %tmp_125_40 = fmul float %e_load_41, %k_load_144
  %gamma_2_40 = fsub float %gamma_2_39, %tmp_125_40
  %s_load_45 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 41), align 4
  %tmp_126_40 = fmul float %s_load_45, %q
  %alpha_load_144 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 41), align 4
  %tmp_127_40 = fadd float %alpha_load_144, %tmp_126_40
  store float %tmp_127_40, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 41), align 4
  %e_load_42 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 42), align 8
  %k_load_145 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 42), align 8
  %tmp_125_41 = fmul float %e_load_42, %k_load_145
  %gamma_2_41 = fsub float %gamma_2_40, %tmp_125_41
  %s_load_46 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 42), align 8
  %tmp_126_41 = fmul float %s_load_46, %q
  %alpha_load_145 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 42), align 8
  %tmp_127_41 = fadd float %alpha_load_145, %tmp_126_41
  store float %tmp_127_41, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 42), align 8
  %e_load_43 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 43), align 4
  %k_load_146 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 43), align 4
  %tmp_125_42 = fmul float %e_load_43, %k_load_146
  %gamma_2_42 = fsub float %gamma_2_41, %tmp_125_42
  %s_load_47 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 43), align 4
  %tmp_126_42 = fmul float %s_load_47, %q
  %alpha_load_146 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 43), align 4
  %tmp_127_42 = fadd float %alpha_load_146, %tmp_126_42
  store float %tmp_127_42, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 43), align 4
  %e_load_44 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 44), align 16
  %k_load_147 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 44), align 16
  %tmp_125_43 = fmul float %e_load_44, %k_load_147
  %gamma_2_43 = fsub float %gamma_2_42, %tmp_125_43
  %s_load_48 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 44), align 16
  %tmp_126_43 = fmul float %s_load_48, %q
  %alpha_load_147 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 44), align 16
  %tmp_127_43 = fadd float %alpha_load_147, %tmp_126_43
  store float %tmp_127_43, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 44), align 16
  %e_load_45 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 45), align 4
  %k_load_148 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 45), align 4
  %tmp_125_44 = fmul float %e_load_45, %k_load_148
  %gamma_2_44 = fsub float %gamma_2_43, %tmp_125_44
  %s_load_49 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 45), align 4
  %tmp_126_44 = fmul float %s_load_49, %q
  %alpha_load_148 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 45), align 4
  %tmp_127_44 = fadd float %alpha_load_148, %tmp_126_44
  store float %tmp_127_44, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 45), align 4
  %e_load_46 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 46), align 8
  %k_load_149 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 46), align 8
  %tmp_125_45 = fmul float %e_load_46, %k_load_149
  %gamma_2_45 = fsub float %gamma_2_44, %tmp_125_45
  %s_load_50 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 46), align 8
  %tmp_126_45 = fmul float %s_load_50, %q
  %alpha_load_149 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 46), align 8
  %tmp_127_45 = fadd float %alpha_load_149, %tmp_126_45
  store float %tmp_127_45, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 46), align 8
  %e_load_47 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 47), align 4
  %k_load_150 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 47), align 4
  %tmp_125_46 = fmul float %e_load_47, %k_load_150
  %gamma_2_46 = fsub float %gamma_2_45, %tmp_125_46
  %s_load_51 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 47), align 4
  %tmp_126_46 = fmul float %s_load_51, %q
  %alpha_load_150 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 47), align 4
  %tmp_127_46 = fadd float %alpha_load_150, %tmp_126_46
  store float %tmp_127_46, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 47), align 4
  %e_load_48 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 48), align 16
  %k_load_151 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 48), align 16
  %tmp_125_47 = fmul float %e_load_48, %k_load_151
  %gamma_2_47 = fsub float %gamma_2_46, %tmp_125_47
  %s_load_52 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 48), align 16
  %tmp_126_47 = fmul float %s_load_52, %q
  %alpha_load_151 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 48), align 16
  %tmp_127_47 = fadd float %alpha_load_151, %tmp_126_47
  store float %tmp_127_47, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 48), align 16
  %e_load_49 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 49), align 4
  %k_load_152 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 49), align 4
  %tmp_125_48 = fmul float %e_load_49, %k_load_152
  %gamma_2_48 = fsub float %gamma_2_47, %tmp_125_48
  %s_load_53 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 49), align 4
  %tmp_126_48 = fmul float %s_load_53, %q
  %alpha_load_152 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 49), align 4
  %tmp_127_48 = fadd float %alpha_load_152, %tmp_126_48
  store float %tmp_127_48, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 49), align 4
  %e_load_50 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 50), align 8
  %k_load_153 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 50), align 8
  %tmp_125_49 = fmul float %e_load_50, %k_load_153
  %gamma_2_49 = fsub float %gamma_2_48, %tmp_125_49
  %s_load_54 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 50), align 8
  %tmp_126_49 = fmul float %s_load_54, %q
  %alpha_load_153 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 50), align 8
  %tmp_127_49 = fadd float %alpha_load_153, %tmp_126_49
  store float %tmp_127_49, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 50), align 8
  %e_load_51 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 51), align 4
  %k_load_154 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 51), align 4
  %tmp_125_50 = fmul float %e_load_51, %k_load_154
  %gamma_2_50 = fsub float %gamma_2_49, %tmp_125_50
  %s_load_55 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 51), align 4
  %tmp_126_50 = fmul float %s_load_55, %q
  %alpha_load_154 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 51), align 4
  %tmp_127_50 = fadd float %alpha_load_154, %tmp_126_50
  store float %tmp_127_50, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 51), align 4
  %e_load_52 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 52), align 16
  %k_load_155 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 52), align 16
  %tmp_125_51 = fmul float %e_load_52, %k_load_155
  %gamma_2_51 = fsub float %gamma_2_50, %tmp_125_51
  %s_load_56 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 52), align 16
  %tmp_126_51 = fmul float %s_load_56, %q
  %alpha_load_155 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 52), align 16
  %tmp_127_51 = fadd float %alpha_load_155, %tmp_126_51
  store float %tmp_127_51, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 52), align 16
  %e_load_53 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 53), align 4
  %k_load_156 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 53), align 4
  %tmp_125_52 = fmul float %e_load_53, %k_load_156
  %gamma_2_52 = fsub float %gamma_2_51, %tmp_125_52
  %s_load_57 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 53), align 4
  %tmp_126_52 = fmul float %s_load_57, %q
  %alpha_load_156 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 53), align 4
  %tmp_127_52 = fadd float %alpha_load_156, %tmp_126_52
  store float %tmp_127_52, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 53), align 4
  %e_load_54 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 54), align 8
  %k_load_157 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 54), align 8
  %tmp_125_53 = fmul float %e_load_54, %k_load_157
  %gamma_2_53 = fsub float %gamma_2_52, %tmp_125_53
  %s_load_58 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 54), align 8
  %tmp_126_53 = fmul float %s_load_58, %q
  %alpha_load_157 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 54), align 8
  %tmp_127_53 = fadd float %alpha_load_157, %tmp_126_53
  store float %tmp_127_53, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 54), align 8
  %e_load_55 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 55), align 4
  %k_load_158 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 55), align 4
  %tmp_125_54 = fmul float %e_load_55, %k_load_158
  %gamma_2_54 = fsub float %gamma_2_53, %tmp_125_54
  %s_load_59 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 55), align 4
  %tmp_126_54 = fmul float %s_load_59, %q
  %alpha_load_158 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 55), align 4
  %tmp_127_54 = fadd float %alpha_load_158, %tmp_126_54
  store float %tmp_127_54, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 55), align 4
  %e_load_56 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 56), align 16
  %k_load_159 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 56), align 16
  %tmp_125_55 = fmul float %e_load_56, %k_load_159
  %gamma_2_55 = fsub float %gamma_2_54, %tmp_125_55
  %s_load_60 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 56), align 16
  %tmp_126_55 = fmul float %s_load_60, %q
  %alpha_load_159 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 56), align 16
  %tmp_127_55 = fadd float %alpha_load_159, %tmp_126_55
  store float %tmp_127_55, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 56), align 16
  %e_load_57 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 57), align 4
  %k_load_160 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 57), align 4
  %tmp_125_56 = fmul float %e_load_57, %k_load_160
  %gamma_2_56 = fsub float %gamma_2_55, %tmp_125_56
  %s_load_61 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 57), align 4
  %tmp_126_56 = fmul float %s_load_61, %q
  %alpha_load_160 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 57), align 4
  %tmp_127_56 = fadd float %alpha_load_160, %tmp_126_56
  store float %tmp_127_56, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 57), align 4
  %e_load_58 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 58), align 8
  %k_load_161 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 58), align 8
  %tmp_125_57 = fmul float %e_load_58, %k_load_161
  %gamma_2_57 = fsub float %gamma_2_56, %tmp_125_57
  %s_load_62 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 58), align 8
  %tmp_126_57 = fmul float %s_load_62, %q
  %alpha_load_161 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 58), align 8
  %tmp_127_57 = fadd float %alpha_load_161, %tmp_126_57
  store float %tmp_127_57, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 58), align 8
  %e_load_59 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 59), align 4
  %k_load_162 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 59), align 4
  %tmp_125_58 = fmul float %e_load_59, %k_load_162
  %gamma_2_58 = fsub float %gamma_2_57, %tmp_125_58
  %s_load_63 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 59), align 4
  %tmp_126_58 = fmul float %s_load_63, %q
  %alpha_load_162 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 59), align 4
  %tmp_127_58 = fadd float %alpha_load_162, %tmp_126_58
  store float %tmp_127_58, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 59), align 4
  %e_load_60 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 60), align 16
  %k_load_163 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 60), align 16
  %tmp_125_59 = fmul float %e_load_60, %k_load_163
  %gamma_2_59 = fsub float %gamma_2_58, %tmp_125_59
  %s_load_64 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 60), align 16
  %tmp_126_59 = fmul float %s_load_64, %q
  %alpha_load_163 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 60), align 16
  %tmp_127_59 = fadd float %alpha_load_163, %tmp_126_59
  store float %tmp_127_59, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 60), align 16
  %e_load_61 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 61), align 4
  %k_load_164 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 61), align 4
  %tmp_125_60 = fmul float %e_load_61, %k_load_164
  %gamma_2_60 = fsub float %gamma_2_59, %tmp_125_60
  %s_load_65 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 61), align 4
  %tmp_126_60 = fmul float %s_load_65, %q
  %alpha_load_164 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 61), align 4
  %tmp_127_60 = fadd float %alpha_load_164, %tmp_126_60
  store float %tmp_127_60, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 61), align 4
  %e_load_62 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 62), align 8
  %k_load_165 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 62), align 8
  %tmp_125_61 = fmul float %e_load_62, %k_load_165
  %gamma_2_61 = fsub float %gamma_2_60, %tmp_125_61
  %s_load_66 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 62), align 8
  %tmp_126_61 = fmul float %s_load_66, %q
  %alpha_load_165 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 62), align 8
  %tmp_127_61 = fadd float %alpha_load_165, %tmp_126_61
  store float %tmp_127_61, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 62), align 8
  %e_load_63 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 63), align 4
  %k_load_166 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 63), align 4
  %tmp_125_62 = fmul float %e_load_63, %k_load_166
  %gamma_2_62 = fsub float %gamma_2_61, %tmp_125_62
  %s_load_67 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 63), align 4
  %tmp_126_62 = fmul float %s_load_67, %q
  %alpha_load_166 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 63), align 4
  %tmp_127_62 = fadd float %alpha_load_166, %tmp_126_62
  store float %tmp_127_62, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 63), align 4
  %e_load_64 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 64), align 16
  %k_load_167 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 64), align 16
  %tmp_125_63 = fmul float %e_load_64, %k_load_167
  %gamma_2_63 = fsub float %gamma_2_62, %tmp_125_63
  %s_load_68 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 64), align 16
  %tmp_126_63 = fmul float %s_load_68, %q
  %alpha_load_167 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 64), align 16
  %tmp_127_63 = fadd float %alpha_load_167, %tmp_126_63
  store float %tmp_127_63, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 64), align 16
  %e_load_65 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 65), align 4
  %k_load_168 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 65), align 4
  %tmp_125_64 = fmul float %e_load_65, %k_load_168
  %gamma_2_64 = fsub float %gamma_2_63, %tmp_125_64
  %s_load_69 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 65), align 4
  %tmp_126_64 = fmul float %s_load_69, %q
  %alpha_load_168 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 65), align 4
  %tmp_127_64 = fadd float %alpha_load_168, %tmp_126_64
  store float %tmp_127_64, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 65), align 4
  %e_load_66 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 66), align 8
  %k_load_169 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 66), align 8
  %tmp_125_65 = fmul float %e_load_66, %k_load_169
  %gamma_2_65 = fsub float %gamma_2_64, %tmp_125_65
  %s_load_70 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 66), align 8
  %tmp_126_65 = fmul float %s_load_70, %q
  %alpha_load_169 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 66), align 8
  %tmp_127_65 = fadd float %alpha_load_169, %tmp_126_65
  store float %tmp_127_65, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 66), align 8
  %e_load_67 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 67), align 4
  %k_load_170 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 67), align 4
  %tmp_125_66 = fmul float %e_load_67, %k_load_170
  %gamma_2_66 = fsub float %gamma_2_65, %tmp_125_66
  %s_load_71 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 67), align 4
  %tmp_126_66 = fmul float %s_load_71, %q
  %alpha_load_170 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 67), align 4
  %tmp_127_66 = fadd float %alpha_load_170, %tmp_126_66
  store float %tmp_127_66, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 67), align 4
  %e_load_68 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 68), align 16
  %k_load_171 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 68), align 16
  %tmp_125_67 = fmul float %e_load_68, %k_load_171
  %gamma_2_67 = fsub float %gamma_2_66, %tmp_125_67
  %s_load_72 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 68), align 16
  %tmp_126_67 = fmul float %s_load_72, %q
  %alpha_load_171 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 68), align 16
  %tmp_127_67 = fadd float %alpha_load_171, %tmp_126_67
  store float %tmp_127_67, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 68), align 16
  %e_load_69 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 69), align 4
  %k_load_172 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 69), align 4
  %tmp_125_68 = fmul float %e_load_69, %k_load_172
  %gamma_2_68 = fsub float %gamma_2_67, %tmp_125_68
  %s_load_73 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 69), align 4
  %tmp_126_68 = fmul float %s_load_73, %q
  %alpha_load_172 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 69), align 4
  %tmp_127_68 = fadd float %alpha_load_172, %tmp_126_68
  store float %tmp_127_68, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 69), align 4
  %e_load_70 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 70), align 8
  %k_load_173 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 70), align 8
  %tmp_125_69 = fmul float %e_load_70, %k_load_173
  %gamma_2_69 = fsub float %gamma_2_68, %tmp_125_69
  %s_load_74 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 70), align 8
  %tmp_126_69 = fmul float %s_load_74, %q
  %alpha_load_173 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 70), align 8
  %tmp_127_69 = fadd float %alpha_load_173, %tmp_126_69
  store float %tmp_127_69, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 70), align 8
  %e_load_71 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 71), align 4
  %k_load_174 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 71), align 4
  %tmp_125_70 = fmul float %e_load_71, %k_load_174
  %gamma_2_70 = fsub float %gamma_2_69, %tmp_125_70
  %s_load_75 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 71), align 4
  %tmp_126_70 = fmul float %s_load_75, %q
  %alpha_load_174 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 71), align 4
  %tmp_127_70 = fadd float %alpha_load_174, %tmp_126_70
  store float %tmp_127_70, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 71), align 4
  %e_load_72 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 72), align 16
  %k_load_175 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 72), align 16
  %tmp_125_71 = fmul float %e_load_72, %k_load_175
  %gamma_2_71 = fsub float %gamma_2_70, %tmp_125_71
  %s_load_76 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 72), align 16
  %tmp_126_71 = fmul float %s_load_76, %q
  %alpha_load_175 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 72), align 16
  %tmp_127_71 = fadd float %alpha_load_175, %tmp_126_71
  store float %tmp_127_71, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 72), align 16
  %e_load_73 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 73), align 4
  %k_load_176 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 73), align 4
  %tmp_125_72 = fmul float %e_load_73, %k_load_176
  %gamma_2_72 = fsub float %gamma_2_71, %tmp_125_72
  %s_load_77 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 73), align 4
  %tmp_126_72 = fmul float %s_load_77, %q
  %alpha_load_176 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 73), align 4
  %tmp_127_72 = fadd float %alpha_load_176, %tmp_126_72
  store float %tmp_127_72, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 73), align 4
  %e_load_74 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 74), align 8
  %k_load_177 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 74), align 8
  %tmp_125_73 = fmul float %e_load_74, %k_load_177
  %gamma_2_73 = fsub float %gamma_2_72, %tmp_125_73
  %s_load_78 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 74), align 8
  %tmp_126_73 = fmul float %s_load_78, %q
  %alpha_load_177 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 74), align 8
  %tmp_127_73 = fadd float %alpha_load_177, %tmp_126_73
  store float %tmp_127_73, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 74), align 8
  %e_load_75 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 75), align 4
  %k_load_178 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 75), align 4
  %tmp_125_74 = fmul float %e_load_75, %k_load_178
  %gamma_2_74 = fsub float %gamma_2_73, %tmp_125_74
  %s_load_79 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 75), align 4
  %tmp_126_74 = fmul float %s_load_79, %q
  %alpha_load_178 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 75), align 4
  %tmp_127_74 = fadd float %alpha_load_178, %tmp_126_74
  store float %tmp_127_74, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 75), align 4
  %e_load_76 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 76), align 16
  %k_load_179 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 76), align 16
  %tmp_125_75 = fmul float %e_load_76, %k_load_179
  %gamma_2_75 = fsub float %gamma_2_74, %tmp_125_75
  %s_load_80 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 76), align 16
  %tmp_126_75 = fmul float %s_load_80, %q
  %alpha_load_179 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 76), align 16
  %tmp_127_75 = fadd float %alpha_load_179, %tmp_126_75
  store float %tmp_127_75, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 76), align 16
  %e_load_77 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 77), align 4
  %k_load_180 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 77), align 4
  %tmp_125_76 = fmul float %e_load_77, %k_load_180
  %gamma_2_76 = fsub float %gamma_2_75, %tmp_125_76
  %s_load_81 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 77), align 4
  %tmp_126_76 = fmul float %s_load_81, %q
  %alpha_load_180 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 77), align 4
  %tmp_127_76 = fadd float %alpha_load_180, %tmp_126_76
  store float %tmp_127_76, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 77), align 4
  %e_load_78 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 78), align 8
  %k_load_181 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 78), align 8
  %tmp_125_77 = fmul float %e_load_78, %k_load_181
  %gamma_2_77 = fsub float %gamma_2_76, %tmp_125_77
  %s_load_82 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 78), align 8
  %tmp_126_77 = fmul float %s_load_82, %q
  %alpha_load_181 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 78), align 8
  %tmp_127_77 = fadd float %alpha_load_181, %tmp_126_77
  store float %tmp_127_77, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 78), align 8
  %e_load_79 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 79), align 4
  %k_load_182 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 79), align 4
  %tmp_125_78 = fmul float %e_load_79, %k_load_182
  %gamma_2_78 = fsub float %gamma_2_77, %tmp_125_78
  %s_load_83 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 79), align 4
  %tmp_126_78 = fmul float %s_load_83, %q
  %alpha_load_182 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 79), align 4
  %tmp_127_78 = fadd float %alpha_load_182, %tmp_126_78
  store float %tmp_127_78, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 79), align 4
  %e_load_80 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 80), align 16
  %k_load_183 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 80), align 16
  %tmp_125_79 = fmul float %e_load_80, %k_load_183
  %gamma_2_79 = fsub float %gamma_2_78, %tmp_125_79
  %s_load_84 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 80), align 16
  %tmp_126_79 = fmul float %s_load_84, %q
  %alpha_load_183 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 80), align 16
  %tmp_127_79 = fadd float %alpha_load_183, %tmp_126_79
  store float %tmp_127_79, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 80), align 16
  %e_load_81 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 81), align 4
  %k_load_184 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 81), align 4
  %tmp_125_80 = fmul float %e_load_81, %k_load_184
  %gamma_2_80 = fsub float %gamma_2_79, %tmp_125_80
  %s_load_85 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 81), align 4
  %tmp_126_80 = fmul float %s_load_85, %q
  %alpha_load_184 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 81), align 4
  %tmp_127_80 = fadd float %alpha_load_184, %tmp_126_80
  store float %tmp_127_80, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 81), align 4
  %e_load_82 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 82), align 8
  %k_load_185 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 82), align 8
  %tmp_125_81 = fmul float %e_load_82, %k_load_185
  %gamma_2_81 = fsub float %gamma_2_80, %tmp_125_81
  %s_load_86 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 82), align 8
  %tmp_126_81 = fmul float %s_load_86, %q
  %alpha_load_185 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 82), align 8
  %tmp_127_81 = fadd float %alpha_load_185, %tmp_126_81
  store float %tmp_127_81, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 82), align 8
  %e_load_83 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 83), align 4
  %k_load_186 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 83), align 4
  %tmp_125_82 = fmul float %e_load_83, %k_load_186
  %gamma_2_82 = fsub float %gamma_2_81, %tmp_125_82
  %s_load_87 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 83), align 4
  %tmp_126_82 = fmul float %s_load_87, %q
  %alpha_load_186 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 83), align 4
  %tmp_127_82 = fadd float %alpha_load_186, %tmp_126_82
  store float %tmp_127_82, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 83), align 4
  %e_load_84 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 84), align 16
  %k_load_187 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 84), align 16
  %tmp_125_83 = fmul float %e_load_84, %k_load_187
  %gamma_2_83 = fsub float %gamma_2_82, %tmp_125_83
  %s_load_88 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 84), align 16
  %tmp_126_83 = fmul float %s_load_88, %q
  %alpha_load_187 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 84), align 16
  %tmp_127_83 = fadd float %alpha_load_187, %tmp_126_83
  store float %tmp_127_83, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 84), align 16
  %e_load_85 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 85), align 4
  %k_load_188 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 85), align 4
  %tmp_125_84 = fmul float %e_load_85, %k_load_188
  %gamma_2_84 = fsub float %gamma_2_83, %tmp_125_84
  %s_load_89 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 85), align 4
  %tmp_126_84 = fmul float %s_load_89, %q
  %alpha_load_188 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 85), align 4
  %tmp_127_84 = fadd float %alpha_load_188, %tmp_126_84
  store float %tmp_127_84, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 85), align 4
  %e_load_86 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 86), align 8
  %k_load_189 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 86), align 8
  %tmp_125_85 = fmul float %e_load_86, %k_load_189
  %gamma_2_85 = fsub float %gamma_2_84, %tmp_125_85
  %s_load_90 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 86), align 8
  %tmp_126_85 = fmul float %s_load_90, %q
  %alpha_load_189 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 86), align 8
  %tmp_127_85 = fadd float %alpha_load_189, %tmp_126_85
  store float %tmp_127_85, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 86), align 8
  %e_load_87 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 87), align 4
  %k_load_190 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 87), align 4
  %tmp_125_86 = fmul float %e_load_87, %k_load_190
  %gamma_2_86 = fsub float %gamma_2_85, %tmp_125_86
  %s_load_91 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 87), align 4
  %tmp_126_86 = fmul float %s_load_91, %q
  %alpha_load_190 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 87), align 4
  %tmp_127_86 = fadd float %alpha_load_190, %tmp_126_86
  store float %tmp_127_86, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 87), align 4
  %e_load_88 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 88), align 16
  %k_load_191 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 88), align 16
  %tmp_125_87 = fmul float %e_load_88, %k_load_191
  %gamma_2_87 = fsub float %gamma_2_86, %tmp_125_87
  %s_load_92 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 88), align 16
  %tmp_126_87 = fmul float %s_load_92, %q
  %alpha_load_191 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 88), align 16
  %tmp_127_87 = fadd float %alpha_load_191, %tmp_126_87
  store float %tmp_127_87, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 88), align 16
  %e_load_89 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 89), align 4
  %k_load_192 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 89), align 4
  %tmp_125_88 = fmul float %e_load_89, %k_load_192
  %gamma_2_88 = fsub float %gamma_2_87, %tmp_125_88
  %s_load_93 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 89), align 4
  %tmp_126_88 = fmul float %s_load_93, %q
  %alpha_load_192 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 89), align 4
  %tmp_127_88 = fadd float %alpha_load_192, %tmp_126_88
  store float %tmp_127_88, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 89), align 4
  %e_load_90 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 90), align 8
  %k_load_193 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 90), align 8
  %tmp_125_89 = fmul float %e_load_90, %k_load_193
  %gamma_2_89 = fsub float %gamma_2_88, %tmp_125_89
  %s_load_94 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 90), align 8
  %tmp_126_89 = fmul float %s_load_94, %q
  %alpha_load_193 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 90), align 8
  %tmp_127_89 = fadd float %alpha_load_193, %tmp_126_89
  store float %tmp_127_89, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 90), align 8
  %e_load_91 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 91), align 4
  %k_load_194 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 91), align 4
  %tmp_125_90 = fmul float %e_load_91, %k_load_194
  %gamma_2_90 = fsub float %gamma_2_89, %tmp_125_90
  %s_load_95 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 91), align 4
  %tmp_126_90 = fmul float %s_load_95, %q
  %alpha_load_194 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 91), align 4
  %tmp_127_90 = fadd float %alpha_load_194, %tmp_126_90
  store float %tmp_127_90, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 91), align 4
  %e_load_92 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 92), align 16
  %k_load_195 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 92), align 16
  %tmp_125_91 = fmul float %e_load_92, %k_load_195
  %gamma_2_91 = fsub float %gamma_2_90, %tmp_125_91
  %s_load_96 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 92), align 16
  %tmp_126_91 = fmul float %s_load_96, %q
  %alpha_load_195 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 92), align 16
  %tmp_127_91 = fadd float %alpha_load_195, %tmp_126_91
  store float %tmp_127_91, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 92), align 16
  %e_load_93 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 93), align 4
  %k_load_196 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 93), align 4
  %tmp_125_92 = fmul float %e_load_93, %k_load_196
  %gamma_2_92 = fsub float %gamma_2_91, %tmp_125_92
  %s_load_97 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 93), align 4
  %tmp_126_92 = fmul float %s_load_97, %q
  %alpha_load_196 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 93), align 4
  %tmp_127_92 = fadd float %alpha_load_196, %tmp_126_92
  store float %tmp_127_92, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 93), align 4
  %e_load_94 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 94), align 8
  %k_load_197 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 94), align 8
  %tmp_125_93 = fmul float %e_load_94, %k_load_197
  %gamma_2_93 = fsub float %gamma_2_92, %tmp_125_93
  %s_load_98 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 94), align 8
  %tmp_126_93 = fmul float %s_load_98, %q
  %alpha_load_197 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 94), align 8
  %tmp_127_93 = fadd float %alpha_load_197, %tmp_126_93
  store float %tmp_127_93, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 94), align 8
  %e_load_95 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 95), align 4
  %k_load_198 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 95), align 4
  %tmp_125_94 = fmul float %e_load_95, %k_load_198
  %gamma_2_94 = fsub float %gamma_2_93, %tmp_125_94
  %s_load_99 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 95), align 4
  %tmp_126_94 = fmul float %s_load_99, %q
  %alpha_load_198 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 95), align 4
  %tmp_127_94 = fadd float %alpha_load_198, %tmp_126_94
  store float %tmp_127_94, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 95), align 4
  %e_load_96 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 96), align 16
  %k_load_199 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 96), align 16
  %tmp_125_95 = fmul float %e_load_96, %k_load_199
  %gamma_2_95 = fsub float %gamma_2_94, %tmp_125_95
  %s_load_100 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 96), align 16
  %tmp_126_95 = fmul float %s_load_100, %q
  %alpha_load_199 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 96), align 16
  %tmp_127_95 = fadd float %alpha_load_199, %tmp_126_95
  store float %tmp_127_95, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 96), align 16
  %e_load_97 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 97), align 4
  %k_load_200 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 97), align 4
  %tmp_125_96 = fmul float %e_load_97, %k_load_200
  %gamma_2_96 = fsub float %gamma_2_95, %tmp_125_96
  %s_load_101 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 97), align 4
  %tmp_126_96 = fmul float %s_load_101, %q
  %alpha_load_200 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 97), align 4
  %tmp_127_96 = fadd float %alpha_load_200, %tmp_126_96
  store float %tmp_127_96, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 97), align 4
  %e_load_98 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 98), align 8
  %k_load_201 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 98), align 8
  %tmp_125_97 = fmul float %e_load_98, %k_load_201
  %gamma_2_97 = fsub float %gamma_2_96, %tmp_125_97
  %s_load_102 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 98), align 8
  %tmp_126_97 = fmul float %s_load_102, %q
  %alpha_load_201 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 98), align 8
  %tmp_127_97 = fadd float %alpha_load_201, %tmp_126_97
  store float %tmp_127_97, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 98), align 8
  %e_load_99 = load float* getelementptr inbounds ([101 x float]* @e, i64 0, i64 99), align 4
  %k_load_202 = load float* getelementptr inbounds ([100 x float]* @k, i64 0, i64 99), align 4
  %tmp_125_98 = fmul float %e_load_99, %k_load_202
  %gamma_2_98 = fsub float %gamma_2_97, %tmp_125_98
  %s_load_103 = load float* getelementptr inbounds ([101 x float]* @s, i64 0, i64 99), align 4
  %tmp_126_98 = fmul float %s_load_103, %q
  %alpha_load_202 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 99), align 4
  %tmp_127_98 = fadd float %alpha_load_202, %tmp_126_98
  store float %tmp_127_98, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 99), align 4
  %alpha_load_6 = load float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16
  %tmp_96 = fadd float %alpha_load_6, %q
  store float %tmp_96, float* getelementptr inbounds ([101 x float]* @alpha, i64 0, i64 100), align 16
  br label %8

; <label>:8                                       ; preds = %9, %7
  %i4 = phi i7 [ 0, %7 ], [ %i_13, %9 ]
  %phi_mul4 = phi i14 [ 0, %7 ], [ %next_mul4, %9 ]
  %exitcond3 = icmp eq i7 %i4, -27
  %i_13 = add i7 %i4, 1
  br i1 %exitcond3, label %.preheader.preheader, label %9

.preheader.preheader:                             ; preds = %8
  %tmp_99 = fpext float %gamma_2_98 to double
  br label %10

; <label>:9                                       ; preds = %8
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 101, i64 101, i64 101)
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str12) nounwind
  %tmp_141 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str12)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_100 = zext i7 %i4 to i64
  %s_addr_6 = getelementptr inbounds [101 x float]* @s, i64 0, i64 %tmp_100
  %next_mul4 = add i14 %phi_mul4, 101
  %s_load_104 = load float* %s_addr_6, align 4
  %tmp_135 = fmul float %s_load_104, %s_load_4
  %tmp_136 = fmul float %tmp_135, %r
  %tmp_138 = zext i14 %phi_mul4 to i64
  %C_addr_104 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138
  %C_load_327 = load float* %C_addr_104, align 4
  %tmp_139 = fadd float %C_load_327, %tmp_136
  store float %tmp_139, float* %C_addr_104, align 4
  %tmp_135_1 = fmul float %s_load_104, %s_load_5
  %tmp_136_1 = fmul float %tmp_135_1, %r
  %tmp_137_1 = add i14 %phi_mul4, 1
  %tmp_138_1 = zext i14 %tmp_137_1 to i64
  %C_addr_105 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_1
  %C_load_328 = load float* %C_addr_105, align 4
  %tmp_139_1 = fadd float %C_load_328, %tmp_136_1
  store float %tmp_139_1, float* %C_addr_105, align 4
  %tmp_135_2 = fmul float %s_load_104, %s_load_6
  %tmp_136_2 = fmul float %tmp_135_2, %r
  %tmp_137_2 = add i14 %phi_mul4, 2
  %tmp_138_2 = zext i14 %tmp_137_2 to i64
  %C_addr_106 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_2
  %C_load_329 = load float* %C_addr_106, align 4
  %tmp_139_2 = fadd float %C_load_329, %tmp_136_2
  store float %tmp_139_2, float* %C_addr_106, align 4
  %tmp_135_3 = fmul float %s_load_104, %s_load_7
  %tmp_136_3 = fmul float %tmp_135_3, %r
  %tmp_137_3 = add i14 %phi_mul4, 3
  %tmp_138_3 = zext i14 %tmp_137_3 to i64
  %C_addr_107 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_3
  %C_load_330 = load float* %C_addr_107, align 4
  %tmp_139_3 = fadd float %C_load_330, %tmp_136_3
  store float %tmp_139_3, float* %C_addr_107, align 4
  %tmp_135_4 = fmul float %s_load_104, %s_load_8
  %tmp_136_4 = fmul float %tmp_135_4, %r
  %tmp_137_4 = add i14 %phi_mul4, 4
  %tmp_138_4 = zext i14 %tmp_137_4 to i64
  %C_addr_108 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_4
  %C_load_331 = load float* %C_addr_108, align 4
  %tmp_139_4 = fadd float %C_load_331, %tmp_136_4
  store float %tmp_139_4, float* %C_addr_108, align 4
  %tmp_135_5 = fmul float %s_load_104, %s_load_9
  %tmp_136_5 = fmul float %tmp_135_5, %r
  %tmp_137_5 = add i14 %phi_mul4, 5
  %tmp_138_5 = zext i14 %tmp_137_5 to i64
  %C_addr_109 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_5
  %C_load_332 = load float* %C_addr_109, align 4
  %tmp_139_5 = fadd float %C_load_332, %tmp_136_5
  store float %tmp_139_5, float* %C_addr_109, align 4
  %tmp_135_6 = fmul float %s_load_104, %s_load_10
  %tmp_136_6 = fmul float %tmp_135_6, %r
  %tmp_137_6 = add i14 %phi_mul4, 6
  %tmp_138_6 = zext i14 %tmp_137_6 to i64
  %C_addr_110 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_6
  %C_load_333 = load float* %C_addr_110, align 4
  %tmp_139_6 = fadd float %C_load_333, %tmp_136_6
  store float %tmp_139_6, float* %C_addr_110, align 4
  %tmp_135_7 = fmul float %s_load_104, %s_load_11
  %tmp_136_7 = fmul float %tmp_135_7, %r
  %tmp_137_7 = add i14 %phi_mul4, 7
  %tmp_138_7 = zext i14 %tmp_137_7 to i64
  %C_addr_111 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_7
  %C_load_334 = load float* %C_addr_111, align 4
  %tmp_139_7 = fadd float %C_load_334, %tmp_136_7
  store float %tmp_139_7, float* %C_addr_111, align 4
  %tmp_135_8 = fmul float %s_load_104, %s_load_12
  %tmp_136_8 = fmul float %tmp_135_8, %r
  %tmp_137_8 = add i14 %phi_mul4, 8
  %tmp_138_8 = zext i14 %tmp_137_8 to i64
  %C_addr_112 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_8
  %C_load_335 = load float* %C_addr_112, align 4
  %tmp_139_8 = fadd float %C_load_335, %tmp_136_8
  store float %tmp_139_8, float* %C_addr_112, align 4
  %tmp_135_9 = fmul float %s_load_104, %s_load_13
  %tmp_136_9 = fmul float %tmp_135_9, %r
  %tmp_137_9 = add i14 %phi_mul4, 9
  %tmp_138_9 = zext i14 %tmp_137_9 to i64
  %C_addr_113 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_9
  %C_load_336 = load float* %C_addr_113, align 4
  %tmp_139_9 = fadd float %C_load_336, %tmp_136_9
  store float %tmp_139_9, float* %C_addr_113, align 4
  %tmp_135_s = fmul float %s_load_104, %s_load_14
  %tmp_136_s = fmul float %tmp_135_s, %r
  %tmp_137_s = add i14 %phi_mul4, 10
  %tmp_138_s = zext i14 %tmp_137_s to i64
  %C_addr_114 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_s
  %C_load_337 = load float* %C_addr_114, align 4
  %tmp_139_s = fadd float %C_load_337, %tmp_136_s
  store float %tmp_139_s, float* %C_addr_114, align 4
  %tmp_135_10 = fmul float %s_load_104, %s_load_15
  %tmp_136_10 = fmul float %tmp_135_10, %r
  %tmp_137_10 = add i14 %phi_mul4, 11
  %tmp_138_10 = zext i14 %tmp_137_10 to i64
  %C_addr_115 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_10
  %C_load_338 = load float* %C_addr_115, align 4
  %tmp_139_10 = fadd float %C_load_338, %tmp_136_10
  store float %tmp_139_10, float* %C_addr_115, align 4
  %tmp_135_11 = fmul float %s_load_104, %s_load_16
  %tmp_136_11 = fmul float %tmp_135_11, %r
  %tmp_137_11 = add i14 %phi_mul4, 12
  %tmp_138_11 = zext i14 %tmp_137_11 to i64
  %C_addr_116 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_11
  %C_load_339 = load float* %C_addr_116, align 4
  %tmp_139_11 = fadd float %C_load_339, %tmp_136_11
  store float %tmp_139_11, float* %C_addr_116, align 4
  %tmp_135_12 = fmul float %s_load_104, %s_load_17
  %tmp_136_12 = fmul float %tmp_135_12, %r
  %tmp_137_12 = add i14 %phi_mul4, 13
  %tmp_138_12 = zext i14 %tmp_137_12 to i64
  %C_addr_117 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_12
  %C_load_340 = load float* %C_addr_117, align 4
  %tmp_139_12 = fadd float %C_load_340, %tmp_136_12
  store float %tmp_139_12, float* %C_addr_117, align 4
  %tmp_135_13 = fmul float %s_load_104, %s_load_18
  %tmp_136_13 = fmul float %tmp_135_13, %r
  %tmp_137_13 = add i14 %phi_mul4, 14
  %tmp_138_13 = zext i14 %tmp_137_13 to i64
  %C_addr_118 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_13
  %C_load_341 = load float* %C_addr_118, align 4
  %tmp_139_13 = fadd float %C_load_341, %tmp_136_13
  store float %tmp_139_13, float* %C_addr_118, align 4
  %tmp_135_14 = fmul float %s_load_104, %s_load_19
  %tmp_136_14 = fmul float %tmp_135_14, %r
  %tmp_137_14 = add i14 %phi_mul4, 15
  %tmp_138_14 = zext i14 %tmp_137_14 to i64
  %C_addr_119 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_14
  %C_load_342 = load float* %C_addr_119, align 4
  %tmp_139_14 = fadd float %C_load_342, %tmp_136_14
  store float %tmp_139_14, float* %C_addr_119, align 4
  %tmp_135_15 = fmul float %s_load_104, %s_load_20
  %tmp_136_15 = fmul float %tmp_135_15, %r
  %tmp_137_15 = add i14 %phi_mul4, 16
  %tmp_138_15 = zext i14 %tmp_137_15 to i64
  %C_addr_120 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_15
  %C_load_343 = load float* %C_addr_120, align 4
  %tmp_139_15 = fadd float %C_load_343, %tmp_136_15
  store float %tmp_139_15, float* %C_addr_120, align 4
  %tmp_135_16 = fmul float %s_load_104, %s_load_21
  %tmp_136_16 = fmul float %tmp_135_16, %r
  %tmp_137_16 = add i14 %phi_mul4, 17
  %tmp_138_16 = zext i14 %tmp_137_16 to i64
  %C_addr_121 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_16
  %C_load_344 = load float* %C_addr_121, align 4
  %tmp_139_16 = fadd float %C_load_344, %tmp_136_16
  store float %tmp_139_16, float* %C_addr_121, align 4
  %tmp_135_17 = fmul float %s_load_104, %s_load_22
  %tmp_136_17 = fmul float %tmp_135_17, %r
  %tmp_137_17 = add i14 %phi_mul4, 18
  %tmp_138_17 = zext i14 %tmp_137_17 to i64
  %C_addr_122 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_17
  %C_load_345 = load float* %C_addr_122, align 4
  %tmp_139_17 = fadd float %C_load_345, %tmp_136_17
  store float %tmp_139_17, float* %C_addr_122, align 4
  %tmp_135_18 = fmul float %s_load_104, %s_load_23
  %tmp_136_18 = fmul float %tmp_135_18, %r
  %tmp_137_18 = add i14 %phi_mul4, 19
  %tmp_138_18 = zext i14 %tmp_137_18 to i64
  %C_addr_123 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_18
  %C_load_346 = load float* %C_addr_123, align 4
  %tmp_139_18 = fadd float %C_load_346, %tmp_136_18
  store float %tmp_139_18, float* %C_addr_123, align 4
  %tmp_135_19 = fmul float %s_load_104, %s_load_24
  %tmp_136_19 = fmul float %tmp_135_19, %r
  %tmp_137_19 = add i14 %phi_mul4, 20
  %tmp_138_19 = zext i14 %tmp_137_19 to i64
  %C_addr_124 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_19
  %C_load_347 = load float* %C_addr_124, align 4
  %tmp_139_19 = fadd float %C_load_347, %tmp_136_19
  store float %tmp_139_19, float* %C_addr_124, align 4
  %tmp_135_20 = fmul float %s_load_104, %s_load_25
  %tmp_136_20 = fmul float %tmp_135_20, %r
  %tmp_137_20 = add i14 %phi_mul4, 21
  %tmp_138_20 = zext i14 %tmp_137_20 to i64
  %C_addr_125 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_20
  %C_load_348 = load float* %C_addr_125, align 4
  %tmp_139_20 = fadd float %C_load_348, %tmp_136_20
  store float %tmp_139_20, float* %C_addr_125, align 4
  %tmp_135_21 = fmul float %s_load_104, %s_load_26
  %tmp_136_21 = fmul float %tmp_135_21, %r
  %tmp_137_21 = add i14 %phi_mul4, 22
  %tmp_138_21 = zext i14 %tmp_137_21 to i64
  %C_addr_126 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_21
  %C_load_349 = load float* %C_addr_126, align 4
  %tmp_139_21 = fadd float %C_load_349, %tmp_136_21
  store float %tmp_139_21, float* %C_addr_126, align 4
  %tmp_135_22 = fmul float %s_load_104, %s_load_27
  %tmp_136_22 = fmul float %tmp_135_22, %r
  %tmp_137_22 = add i14 %phi_mul4, 23
  %tmp_138_22 = zext i14 %tmp_137_22 to i64
  %C_addr_127 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_22
  %C_load_350 = load float* %C_addr_127, align 4
  %tmp_139_22 = fadd float %C_load_350, %tmp_136_22
  store float %tmp_139_22, float* %C_addr_127, align 4
  %tmp_135_23 = fmul float %s_load_104, %s_load_28
  %tmp_136_23 = fmul float %tmp_135_23, %r
  %tmp_137_23 = add i14 %phi_mul4, 24
  %tmp_138_23 = zext i14 %tmp_137_23 to i64
  %C_addr_128 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_23
  %C_load_351 = load float* %C_addr_128, align 4
  %tmp_139_23 = fadd float %C_load_351, %tmp_136_23
  store float %tmp_139_23, float* %C_addr_128, align 4
  %tmp_135_24 = fmul float %s_load_104, %s_load_29
  %tmp_136_24 = fmul float %tmp_135_24, %r
  %tmp_137_24 = add i14 %phi_mul4, 25
  %tmp_138_24 = zext i14 %tmp_137_24 to i64
  %C_addr_129 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_24
  %C_load_352 = load float* %C_addr_129, align 4
  %tmp_139_24 = fadd float %C_load_352, %tmp_136_24
  store float %tmp_139_24, float* %C_addr_129, align 4
  %tmp_135_25 = fmul float %s_load_104, %s_load_30
  %tmp_136_25 = fmul float %tmp_135_25, %r
  %tmp_137_25 = add i14 %phi_mul4, 26
  %tmp_138_25 = zext i14 %tmp_137_25 to i64
  %C_addr_130 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_25
  %C_load_353 = load float* %C_addr_130, align 4
  %tmp_139_25 = fadd float %C_load_353, %tmp_136_25
  store float %tmp_139_25, float* %C_addr_130, align 4
  %tmp_135_26 = fmul float %s_load_104, %s_load_31
  %tmp_136_26 = fmul float %tmp_135_26, %r
  %tmp_137_26 = add i14 %phi_mul4, 27
  %tmp_138_26 = zext i14 %tmp_137_26 to i64
  %C_addr_131 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_26
  %C_load_354 = load float* %C_addr_131, align 4
  %tmp_139_26 = fadd float %C_load_354, %tmp_136_26
  store float %tmp_139_26, float* %C_addr_131, align 4
  %tmp_135_27 = fmul float %s_load_104, %s_load_32
  %tmp_136_27 = fmul float %tmp_135_27, %r
  %tmp_137_27 = add i14 %phi_mul4, 28
  %tmp_138_27 = zext i14 %tmp_137_27 to i64
  %C_addr_132 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_27
  %C_load_355 = load float* %C_addr_132, align 4
  %tmp_139_27 = fadd float %C_load_355, %tmp_136_27
  store float %tmp_139_27, float* %C_addr_132, align 4
  %tmp_135_28 = fmul float %s_load_104, %s_load_33
  %tmp_136_28 = fmul float %tmp_135_28, %r
  %tmp_137_28 = add i14 %phi_mul4, 29
  %tmp_138_28 = zext i14 %tmp_137_28 to i64
  %C_addr_133 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_28
  %C_load_356 = load float* %C_addr_133, align 4
  %tmp_139_28 = fadd float %C_load_356, %tmp_136_28
  store float %tmp_139_28, float* %C_addr_133, align 4
  %tmp_135_29 = fmul float %s_load_104, %s_load_34
  %tmp_136_29 = fmul float %tmp_135_29, %r
  %tmp_137_29 = add i14 %phi_mul4, 30
  %tmp_138_29 = zext i14 %tmp_137_29 to i64
  %C_addr_134 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_29
  %C_load_357 = load float* %C_addr_134, align 4
  %tmp_139_29 = fadd float %C_load_357, %tmp_136_29
  store float %tmp_139_29, float* %C_addr_134, align 4
  %tmp_135_30 = fmul float %s_load_104, %s_load_35
  %tmp_136_30 = fmul float %tmp_135_30, %r
  %tmp_137_30 = add i14 %phi_mul4, 31
  %tmp_138_30 = zext i14 %tmp_137_30 to i64
  %C_addr_135 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_30
  %C_load_358 = load float* %C_addr_135, align 4
  %tmp_139_30 = fadd float %C_load_358, %tmp_136_30
  store float %tmp_139_30, float* %C_addr_135, align 4
  %tmp_135_31 = fmul float %s_load_104, %s_load_36
  %tmp_136_31 = fmul float %tmp_135_31, %r
  %tmp_137_31 = add i14 %phi_mul4, 32
  %tmp_138_31 = zext i14 %tmp_137_31 to i64
  %C_addr_136 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_31
  %C_load_359 = load float* %C_addr_136, align 4
  %tmp_139_31 = fadd float %C_load_359, %tmp_136_31
  store float %tmp_139_31, float* %C_addr_136, align 4
  %tmp_135_32 = fmul float %s_load_104, %s_load_37
  %tmp_136_32 = fmul float %tmp_135_32, %r
  %tmp_137_32 = add i14 %phi_mul4, 33
  %tmp_138_32 = zext i14 %tmp_137_32 to i64
  %C_addr_137 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_32
  %C_load_360 = load float* %C_addr_137, align 4
  %tmp_139_32 = fadd float %C_load_360, %tmp_136_32
  store float %tmp_139_32, float* %C_addr_137, align 4
  %tmp_135_33 = fmul float %s_load_104, %s_load_38
  %tmp_136_33 = fmul float %tmp_135_33, %r
  %tmp_137_33 = add i14 %phi_mul4, 34
  %tmp_138_33 = zext i14 %tmp_137_33 to i64
  %C_addr_138 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_33
  %C_load_361 = load float* %C_addr_138, align 4
  %tmp_139_33 = fadd float %C_load_361, %tmp_136_33
  store float %tmp_139_33, float* %C_addr_138, align 4
  %tmp_135_34 = fmul float %s_load_104, %s_load_39
  %tmp_136_34 = fmul float %tmp_135_34, %r
  %tmp_137_34 = add i14 %phi_mul4, 35
  %tmp_138_34 = zext i14 %tmp_137_34 to i64
  %C_addr_139 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_34
  %C_load_362 = load float* %C_addr_139, align 4
  %tmp_139_34 = fadd float %C_load_362, %tmp_136_34
  store float %tmp_139_34, float* %C_addr_139, align 4
  %tmp_135_35 = fmul float %s_load_104, %s_load_40
  %tmp_136_35 = fmul float %tmp_135_35, %r
  %tmp_137_35 = add i14 %phi_mul4, 36
  %tmp_138_35 = zext i14 %tmp_137_35 to i64
  %C_addr_140 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_35
  %C_load_363 = load float* %C_addr_140, align 4
  %tmp_139_35 = fadd float %C_load_363, %tmp_136_35
  store float %tmp_139_35, float* %C_addr_140, align 4
  %tmp_135_36 = fmul float %s_load_104, %s_load_41
  %tmp_136_36 = fmul float %tmp_135_36, %r
  %tmp_137_36 = add i14 %phi_mul4, 37
  %tmp_138_36 = zext i14 %tmp_137_36 to i64
  %C_addr_141 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_36
  %C_load_364 = load float* %C_addr_141, align 4
  %tmp_139_36 = fadd float %C_load_364, %tmp_136_36
  store float %tmp_139_36, float* %C_addr_141, align 4
  %tmp_135_37 = fmul float %s_load_104, %s_load_42
  %tmp_136_37 = fmul float %tmp_135_37, %r
  %tmp_137_37 = add i14 %phi_mul4, 38
  %tmp_138_37 = zext i14 %tmp_137_37 to i64
  %C_addr_142 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_37
  %C_load_365 = load float* %C_addr_142, align 4
  %tmp_139_37 = fadd float %C_load_365, %tmp_136_37
  store float %tmp_139_37, float* %C_addr_142, align 4
  %tmp_135_38 = fmul float %s_load_104, %s_load_43
  %tmp_136_38 = fmul float %tmp_135_38, %r
  %tmp_137_38 = add i14 %phi_mul4, 39
  %tmp_138_38 = zext i14 %tmp_137_38 to i64
  %C_addr_143 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_38
  %C_load_366 = load float* %C_addr_143, align 4
  %tmp_139_38 = fadd float %C_load_366, %tmp_136_38
  store float %tmp_139_38, float* %C_addr_143, align 4
  %tmp_135_39 = fmul float %s_load_104, %s_load_44
  %tmp_136_39 = fmul float %tmp_135_39, %r
  %tmp_137_39 = add i14 %phi_mul4, 40
  %tmp_138_39 = zext i14 %tmp_137_39 to i64
  %C_addr_144 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_39
  %C_load_367 = load float* %C_addr_144, align 4
  %tmp_139_39 = fadd float %C_load_367, %tmp_136_39
  store float %tmp_139_39, float* %C_addr_144, align 4
  %tmp_135_40 = fmul float %s_load_104, %s_load_45
  %tmp_136_40 = fmul float %tmp_135_40, %r
  %tmp_137_40 = add i14 %phi_mul4, 41
  %tmp_138_40 = zext i14 %tmp_137_40 to i64
  %C_addr_145 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_40
  %C_load_368 = load float* %C_addr_145, align 4
  %tmp_139_40 = fadd float %C_load_368, %tmp_136_40
  store float %tmp_139_40, float* %C_addr_145, align 4
  %tmp_135_41 = fmul float %s_load_104, %s_load_46
  %tmp_136_41 = fmul float %tmp_135_41, %r
  %tmp_137_41 = add i14 %phi_mul4, 42
  %tmp_138_41 = zext i14 %tmp_137_41 to i64
  %C_addr_146 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_41
  %C_load_369 = load float* %C_addr_146, align 4
  %tmp_139_41 = fadd float %C_load_369, %tmp_136_41
  store float %tmp_139_41, float* %C_addr_146, align 4
  %tmp_135_42 = fmul float %s_load_104, %s_load_47
  %tmp_136_42 = fmul float %tmp_135_42, %r
  %tmp_137_42 = add i14 %phi_mul4, 43
  %tmp_138_42 = zext i14 %tmp_137_42 to i64
  %C_addr_147 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_42
  %C_load_370 = load float* %C_addr_147, align 4
  %tmp_139_42 = fadd float %C_load_370, %tmp_136_42
  store float %tmp_139_42, float* %C_addr_147, align 4
  %tmp_135_43 = fmul float %s_load_104, %s_load_48
  %tmp_136_43 = fmul float %tmp_135_43, %r
  %tmp_137_43 = add i14 %phi_mul4, 44
  %tmp_138_43 = zext i14 %tmp_137_43 to i64
  %C_addr_148 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_43
  %C_load_371 = load float* %C_addr_148, align 4
  %tmp_139_43 = fadd float %C_load_371, %tmp_136_43
  store float %tmp_139_43, float* %C_addr_148, align 4
  %tmp_135_44 = fmul float %s_load_104, %s_load_49
  %tmp_136_44 = fmul float %tmp_135_44, %r
  %tmp_137_44 = add i14 %phi_mul4, 45
  %tmp_138_44 = zext i14 %tmp_137_44 to i64
  %C_addr_149 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_44
  %C_load_372 = load float* %C_addr_149, align 4
  %tmp_139_44 = fadd float %C_load_372, %tmp_136_44
  store float %tmp_139_44, float* %C_addr_149, align 4
  %tmp_135_45 = fmul float %s_load_104, %s_load_50
  %tmp_136_45 = fmul float %tmp_135_45, %r
  %tmp_137_45 = add i14 %phi_mul4, 46
  %tmp_138_45 = zext i14 %tmp_137_45 to i64
  %C_addr_150 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_45
  %C_load_373 = load float* %C_addr_150, align 4
  %tmp_139_45 = fadd float %C_load_373, %tmp_136_45
  store float %tmp_139_45, float* %C_addr_150, align 4
  %tmp_135_46 = fmul float %s_load_104, %s_load_51
  %tmp_136_46 = fmul float %tmp_135_46, %r
  %tmp_137_46 = add i14 %phi_mul4, 47
  %tmp_138_46 = zext i14 %tmp_137_46 to i64
  %C_addr_151 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_46
  %C_load_374 = load float* %C_addr_151, align 4
  %tmp_139_46 = fadd float %C_load_374, %tmp_136_46
  store float %tmp_139_46, float* %C_addr_151, align 4
  %tmp_135_47 = fmul float %s_load_104, %s_load_52
  %tmp_136_47 = fmul float %tmp_135_47, %r
  %tmp_137_47 = add i14 %phi_mul4, 48
  %tmp_138_47 = zext i14 %tmp_137_47 to i64
  %C_addr_152 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_47
  %C_load_375 = load float* %C_addr_152, align 4
  %tmp_139_47 = fadd float %C_load_375, %tmp_136_47
  store float %tmp_139_47, float* %C_addr_152, align 4
  %tmp_135_48 = fmul float %s_load_104, %s_load_53
  %tmp_136_48 = fmul float %tmp_135_48, %r
  %tmp_137_48 = add i14 %phi_mul4, 49
  %tmp_138_48 = zext i14 %tmp_137_48 to i64
  %C_addr_153 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_48
  %C_load_376 = load float* %C_addr_153, align 4
  %tmp_139_48 = fadd float %C_load_376, %tmp_136_48
  store float %tmp_139_48, float* %C_addr_153, align 4
  %tmp_135_49 = fmul float %s_load_104, %s_load_54
  %tmp_136_49 = fmul float %tmp_135_49, %r
  %tmp_137_49 = add i14 %phi_mul4, 50
  %tmp_138_49 = zext i14 %tmp_137_49 to i64
  %C_addr_154 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_49
  %C_load_377 = load float* %C_addr_154, align 4
  %tmp_139_49 = fadd float %C_load_377, %tmp_136_49
  store float %tmp_139_49, float* %C_addr_154, align 4
  %tmp_135_50 = fmul float %s_load_104, %s_load_55
  %tmp_136_50 = fmul float %tmp_135_50, %r
  %tmp_137_50 = add i14 %phi_mul4, 51
  %tmp_138_50 = zext i14 %tmp_137_50 to i64
  %C_addr_155 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_50
  %C_load_378 = load float* %C_addr_155, align 4
  %tmp_139_50 = fadd float %C_load_378, %tmp_136_50
  store float %tmp_139_50, float* %C_addr_155, align 4
  %tmp_135_51 = fmul float %s_load_104, %s_load_56
  %tmp_136_51 = fmul float %tmp_135_51, %r
  %tmp_137_51 = add i14 %phi_mul4, 52
  %tmp_138_51 = zext i14 %tmp_137_51 to i64
  %C_addr_156 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_51
  %C_load_379 = load float* %C_addr_156, align 4
  %tmp_139_51 = fadd float %C_load_379, %tmp_136_51
  store float %tmp_139_51, float* %C_addr_156, align 4
  %tmp_135_52 = fmul float %s_load_104, %s_load_57
  %tmp_136_52 = fmul float %tmp_135_52, %r
  %tmp_137_52 = add i14 %phi_mul4, 53
  %tmp_138_52 = zext i14 %tmp_137_52 to i64
  %C_addr_157 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_52
  %C_load_380 = load float* %C_addr_157, align 4
  %tmp_139_52 = fadd float %C_load_380, %tmp_136_52
  store float %tmp_139_52, float* %C_addr_157, align 4
  %tmp_135_53 = fmul float %s_load_104, %s_load_58
  %tmp_136_53 = fmul float %tmp_135_53, %r
  %tmp_137_53 = add i14 %phi_mul4, 54
  %tmp_138_53 = zext i14 %tmp_137_53 to i64
  %C_addr_158 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_53
  %C_load_381 = load float* %C_addr_158, align 4
  %tmp_139_53 = fadd float %C_load_381, %tmp_136_53
  store float %tmp_139_53, float* %C_addr_158, align 4
  %tmp_135_54 = fmul float %s_load_104, %s_load_59
  %tmp_136_54 = fmul float %tmp_135_54, %r
  %tmp_137_54 = add i14 %phi_mul4, 55
  %tmp_138_54 = zext i14 %tmp_137_54 to i64
  %C_addr_159 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_54
  %C_load_382 = load float* %C_addr_159, align 4
  %tmp_139_54 = fadd float %C_load_382, %tmp_136_54
  store float %tmp_139_54, float* %C_addr_159, align 4
  %tmp_135_55 = fmul float %s_load_104, %s_load_60
  %tmp_136_55 = fmul float %tmp_135_55, %r
  %tmp_137_55 = add i14 %phi_mul4, 56
  %tmp_138_55 = zext i14 %tmp_137_55 to i64
  %C_addr_160 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_55
  %C_load_383 = load float* %C_addr_160, align 4
  %tmp_139_55 = fadd float %C_load_383, %tmp_136_55
  store float %tmp_139_55, float* %C_addr_160, align 4
  %tmp_135_56 = fmul float %s_load_104, %s_load_61
  %tmp_136_56 = fmul float %tmp_135_56, %r
  %tmp_137_56 = add i14 %phi_mul4, 57
  %tmp_138_56 = zext i14 %tmp_137_56 to i64
  %C_addr_161 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_56
  %C_load_384 = load float* %C_addr_161, align 4
  %tmp_139_56 = fadd float %C_load_384, %tmp_136_56
  store float %tmp_139_56, float* %C_addr_161, align 4
  %tmp_135_57 = fmul float %s_load_104, %s_load_62
  %tmp_136_57 = fmul float %tmp_135_57, %r
  %tmp_137_57 = add i14 %phi_mul4, 58
  %tmp_138_57 = zext i14 %tmp_137_57 to i64
  %C_addr_162 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_57
  %C_load_385 = load float* %C_addr_162, align 4
  %tmp_139_57 = fadd float %C_load_385, %tmp_136_57
  store float %tmp_139_57, float* %C_addr_162, align 4
  %tmp_135_58 = fmul float %s_load_104, %s_load_63
  %tmp_136_58 = fmul float %tmp_135_58, %r
  %tmp_137_58 = add i14 %phi_mul4, 59
  %tmp_138_58 = zext i14 %tmp_137_58 to i64
  %C_addr_163 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_58
  %C_load_386 = load float* %C_addr_163, align 4
  %tmp_139_58 = fadd float %C_load_386, %tmp_136_58
  store float %tmp_139_58, float* %C_addr_163, align 4
  %tmp_135_59 = fmul float %s_load_104, %s_load_64
  %tmp_136_59 = fmul float %tmp_135_59, %r
  %tmp_137_59 = add i14 %phi_mul4, 60
  %tmp_138_59 = zext i14 %tmp_137_59 to i64
  %C_addr_164 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_59
  %C_load_387 = load float* %C_addr_164, align 4
  %tmp_139_59 = fadd float %C_load_387, %tmp_136_59
  store float %tmp_139_59, float* %C_addr_164, align 4
  %tmp_135_60 = fmul float %s_load_104, %s_load_65
  %tmp_136_60 = fmul float %tmp_135_60, %r
  %tmp_137_60 = add i14 %phi_mul4, 61
  %tmp_138_60 = zext i14 %tmp_137_60 to i64
  %C_addr_165 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_60
  %C_load_388 = load float* %C_addr_165, align 4
  %tmp_139_60 = fadd float %C_load_388, %tmp_136_60
  store float %tmp_139_60, float* %C_addr_165, align 4
  %tmp_135_61 = fmul float %s_load_104, %s_load_66
  %tmp_136_61 = fmul float %tmp_135_61, %r
  %tmp_137_61 = add i14 %phi_mul4, 62
  %tmp_138_61 = zext i14 %tmp_137_61 to i64
  %C_addr_166 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_61
  %C_load_389 = load float* %C_addr_166, align 4
  %tmp_139_61 = fadd float %C_load_389, %tmp_136_61
  store float %tmp_139_61, float* %C_addr_166, align 4
  %tmp_135_62 = fmul float %s_load_104, %s_load_67
  %tmp_136_62 = fmul float %tmp_135_62, %r
  %tmp_137_62 = add i14 %phi_mul4, 63
  %tmp_138_62 = zext i14 %tmp_137_62 to i64
  %C_addr_167 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_62
  %C_load_390 = load float* %C_addr_167, align 4
  %tmp_139_62 = fadd float %C_load_390, %tmp_136_62
  store float %tmp_139_62, float* %C_addr_167, align 4
  %tmp_135_63 = fmul float %s_load_104, %s_load_68
  %tmp_136_63 = fmul float %tmp_135_63, %r
  %tmp_137_63 = add i14 %phi_mul4, 64
  %tmp_138_63 = zext i14 %tmp_137_63 to i64
  %C_addr_168 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_63
  %C_load_391 = load float* %C_addr_168, align 4
  %tmp_139_63 = fadd float %C_load_391, %tmp_136_63
  store float %tmp_139_63, float* %C_addr_168, align 4
  %tmp_135_64 = fmul float %s_load_104, %s_load_69
  %tmp_136_64 = fmul float %tmp_135_64, %r
  %tmp_137_64 = add i14 %phi_mul4, 65
  %tmp_138_64 = zext i14 %tmp_137_64 to i64
  %C_addr_169 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_64
  %C_load_392 = load float* %C_addr_169, align 4
  %tmp_139_64 = fadd float %C_load_392, %tmp_136_64
  store float %tmp_139_64, float* %C_addr_169, align 4
  %tmp_135_65 = fmul float %s_load_104, %s_load_70
  %tmp_136_65 = fmul float %tmp_135_65, %r
  %tmp_137_65 = add i14 %phi_mul4, 66
  %tmp_138_65 = zext i14 %tmp_137_65 to i64
  %C_addr_170 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_65
  %C_load_393 = load float* %C_addr_170, align 4
  %tmp_139_65 = fadd float %C_load_393, %tmp_136_65
  store float %tmp_139_65, float* %C_addr_170, align 4
  %tmp_135_66 = fmul float %s_load_104, %s_load_71
  %tmp_136_66 = fmul float %tmp_135_66, %r
  %tmp_137_66 = add i14 %phi_mul4, 67
  %tmp_138_66 = zext i14 %tmp_137_66 to i64
  %C_addr_171 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_66
  %C_load_394 = load float* %C_addr_171, align 4
  %tmp_139_66 = fadd float %C_load_394, %tmp_136_66
  store float %tmp_139_66, float* %C_addr_171, align 4
  %tmp_135_67 = fmul float %s_load_104, %s_load_72
  %tmp_136_67 = fmul float %tmp_135_67, %r
  %tmp_137_67 = add i14 %phi_mul4, 68
  %tmp_138_67 = zext i14 %tmp_137_67 to i64
  %C_addr_172 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_67
  %C_load_395 = load float* %C_addr_172, align 4
  %tmp_139_67 = fadd float %C_load_395, %tmp_136_67
  store float %tmp_139_67, float* %C_addr_172, align 4
  %tmp_135_68 = fmul float %s_load_104, %s_load_73
  %tmp_136_68 = fmul float %tmp_135_68, %r
  %tmp_137_68 = add i14 %phi_mul4, 69
  %tmp_138_68 = zext i14 %tmp_137_68 to i64
  %C_addr_173 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_68
  %C_load_396 = load float* %C_addr_173, align 4
  %tmp_139_68 = fadd float %C_load_396, %tmp_136_68
  store float %tmp_139_68, float* %C_addr_173, align 4
  %tmp_135_69 = fmul float %s_load_104, %s_load_74
  %tmp_136_69 = fmul float %tmp_135_69, %r
  %tmp_137_69 = add i14 %phi_mul4, 70
  %tmp_138_69 = zext i14 %tmp_137_69 to i64
  %C_addr_174 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_69
  %C_load_397 = load float* %C_addr_174, align 4
  %tmp_139_69 = fadd float %C_load_397, %tmp_136_69
  store float %tmp_139_69, float* %C_addr_174, align 4
  %tmp_135_70 = fmul float %s_load_104, %s_load_75
  %tmp_136_70 = fmul float %tmp_135_70, %r
  %tmp_137_70 = add i14 %phi_mul4, 71
  %tmp_138_70 = zext i14 %tmp_137_70 to i64
  %C_addr_175 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_70
  %C_load_398 = load float* %C_addr_175, align 4
  %tmp_139_70 = fadd float %C_load_398, %tmp_136_70
  store float %tmp_139_70, float* %C_addr_175, align 4
  %tmp_135_71 = fmul float %s_load_104, %s_load_76
  %tmp_136_71 = fmul float %tmp_135_71, %r
  %tmp_137_71 = add i14 %phi_mul4, 72
  %tmp_138_71 = zext i14 %tmp_137_71 to i64
  %C_addr_176 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_71
  %C_load_399 = load float* %C_addr_176, align 4
  %tmp_139_71 = fadd float %C_load_399, %tmp_136_71
  store float %tmp_139_71, float* %C_addr_176, align 4
  %tmp_135_72 = fmul float %s_load_104, %s_load_77
  %tmp_136_72 = fmul float %tmp_135_72, %r
  %tmp_137_72 = add i14 %phi_mul4, 73
  %tmp_138_72 = zext i14 %tmp_137_72 to i64
  %C_addr_177 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_72
  %C_load_400 = load float* %C_addr_177, align 4
  %tmp_139_72 = fadd float %C_load_400, %tmp_136_72
  store float %tmp_139_72, float* %C_addr_177, align 4
  %tmp_135_73 = fmul float %s_load_104, %s_load_78
  %tmp_136_73 = fmul float %tmp_135_73, %r
  %tmp_137_73 = add i14 %phi_mul4, 74
  %tmp_138_73 = zext i14 %tmp_137_73 to i64
  %C_addr_178 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_73
  %C_load_401 = load float* %C_addr_178, align 4
  %tmp_139_73 = fadd float %C_load_401, %tmp_136_73
  store float %tmp_139_73, float* %C_addr_178, align 4
  %tmp_135_74 = fmul float %s_load_104, %s_load_79
  %tmp_136_74 = fmul float %tmp_135_74, %r
  %tmp_137_74 = add i14 %phi_mul4, 75
  %tmp_138_74 = zext i14 %tmp_137_74 to i64
  %C_addr_179 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_74
  %C_load_402 = load float* %C_addr_179, align 4
  %tmp_139_74 = fadd float %C_load_402, %tmp_136_74
  store float %tmp_139_74, float* %C_addr_179, align 4
  %tmp_135_75 = fmul float %s_load_104, %s_load_80
  %tmp_136_75 = fmul float %tmp_135_75, %r
  %tmp_137_75 = add i14 %phi_mul4, 76
  %tmp_138_75 = zext i14 %tmp_137_75 to i64
  %C_addr_180 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_75
  %C_load_403 = load float* %C_addr_180, align 4
  %tmp_139_75 = fadd float %C_load_403, %tmp_136_75
  store float %tmp_139_75, float* %C_addr_180, align 4
  %tmp_135_76 = fmul float %s_load_104, %s_load_81
  %tmp_136_76 = fmul float %tmp_135_76, %r
  %tmp_137_76 = add i14 %phi_mul4, 77
  %tmp_138_76 = zext i14 %tmp_137_76 to i64
  %C_addr_181 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_76
  %C_load_404 = load float* %C_addr_181, align 4
  %tmp_139_76 = fadd float %C_load_404, %tmp_136_76
  store float %tmp_139_76, float* %C_addr_181, align 4
  %tmp_135_77 = fmul float %s_load_104, %s_load_82
  %tmp_136_77 = fmul float %tmp_135_77, %r
  %tmp_137_77 = add i14 %phi_mul4, 78
  %tmp_138_77 = zext i14 %tmp_137_77 to i64
  %C_addr_182 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_77
  %C_load_405 = load float* %C_addr_182, align 4
  %tmp_139_77 = fadd float %C_load_405, %tmp_136_77
  store float %tmp_139_77, float* %C_addr_182, align 4
  %tmp_135_78 = fmul float %s_load_104, %s_load_83
  %tmp_136_78 = fmul float %tmp_135_78, %r
  %tmp_137_78 = add i14 %phi_mul4, 79
  %tmp_138_78 = zext i14 %tmp_137_78 to i64
  %C_addr_183 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_78
  %C_load_406 = load float* %C_addr_183, align 4
  %tmp_139_78 = fadd float %C_load_406, %tmp_136_78
  store float %tmp_139_78, float* %C_addr_183, align 4
  %tmp_135_79 = fmul float %s_load_104, %s_load_84
  %tmp_136_79 = fmul float %tmp_135_79, %r
  %tmp_137_79 = add i14 %phi_mul4, 80
  %tmp_138_79 = zext i14 %tmp_137_79 to i64
  %C_addr_184 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_79
  %C_load_407 = load float* %C_addr_184, align 4
  %tmp_139_79 = fadd float %C_load_407, %tmp_136_79
  store float %tmp_139_79, float* %C_addr_184, align 4
  %tmp_135_80 = fmul float %s_load_104, %s_load_85
  %tmp_136_80 = fmul float %tmp_135_80, %r
  %tmp_137_80 = add i14 %phi_mul4, 81
  %tmp_138_80 = zext i14 %tmp_137_80 to i64
  %C_addr_185 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_80
  %C_load_408 = load float* %C_addr_185, align 4
  %tmp_139_80 = fadd float %C_load_408, %tmp_136_80
  store float %tmp_139_80, float* %C_addr_185, align 4
  %tmp_135_81 = fmul float %s_load_104, %s_load_86
  %tmp_136_81 = fmul float %tmp_135_81, %r
  %tmp_137_81 = add i14 %phi_mul4, 82
  %tmp_138_81 = zext i14 %tmp_137_81 to i64
  %C_addr_186 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_81
  %C_load_409 = load float* %C_addr_186, align 4
  %tmp_139_81 = fadd float %C_load_409, %tmp_136_81
  store float %tmp_139_81, float* %C_addr_186, align 4
  %tmp_135_82 = fmul float %s_load_104, %s_load_87
  %tmp_136_82 = fmul float %tmp_135_82, %r
  %tmp_137_82 = add i14 %phi_mul4, 83
  %tmp_138_82 = zext i14 %tmp_137_82 to i64
  %C_addr_187 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_82
  %C_load_410 = load float* %C_addr_187, align 4
  %tmp_139_82 = fadd float %C_load_410, %tmp_136_82
  store float %tmp_139_82, float* %C_addr_187, align 4
  %tmp_135_83 = fmul float %s_load_104, %s_load_88
  %tmp_136_83 = fmul float %tmp_135_83, %r
  %tmp_137_83 = add i14 %phi_mul4, 84
  %tmp_138_83 = zext i14 %tmp_137_83 to i64
  %C_addr_188 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_83
  %C_load_411 = load float* %C_addr_188, align 4
  %tmp_139_83 = fadd float %C_load_411, %tmp_136_83
  store float %tmp_139_83, float* %C_addr_188, align 4
  %tmp_135_84 = fmul float %s_load_104, %s_load_89
  %tmp_136_84 = fmul float %tmp_135_84, %r
  %tmp_137_84 = add i14 %phi_mul4, 85
  %tmp_138_84 = zext i14 %tmp_137_84 to i64
  %C_addr_189 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_84
  %C_load_412 = load float* %C_addr_189, align 4
  %tmp_139_84 = fadd float %C_load_412, %tmp_136_84
  store float %tmp_139_84, float* %C_addr_189, align 4
  %tmp_135_85 = fmul float %s_load_104, %s_load_90
  %tmp_136_85 = fmul float %tmp_135_85, %r
  %tmp_137_85 = add i14 %phi_mul4, 86
  %tmp_138_85 = zext i14 %tmp_137_85 to i64
  %C_addr_190 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_85
  %C_load_413 = load float* %C_addr_190, align 4
  %tmp_139_85 = fadd float %C_load_413, %tmp_136_85
  store float %tmp_139_85, float* %C_addr_190, align 4
  %tmp_135_86 = fmul float %s_load_104, %s_load_91
  %tmp_136_86 = fmul float %tmp_135_86, %r
  %tmp_137_86 = add i14 %phi_mul4, 87
  %tmp_138_86 = zext i14 %tmp_137_86 to i64
  %C_addr_191 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_86
  %C_load_414 = load float* %C_addr_191, align 4
  %tmp_139_86 = fadd float %C_load_414, %tmp_136_86
  store float %tmp_139_86, float* %C_addr_191, align 4
  %tmp_135_87 = fmul float %s_load_104, %s_load_92
  %tmp_136_87 = fmul float %tmp_135_87, %r
  %tmp_137_87 = add i14 %phi_mul4, 88
  %tmp_138_87 = zext i14 %tmp_137_87 to i64
  %C_addr_192 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_87
  %C_load_415 = load float* %C_addr_192, align 4
  %tmp_139_87 = fadd float %C_load_415, %tmp_136_87
  store float %tmp_139_87, float* %C_addr_192, align 4
  %tmp_135_88 = fmul float %s_load_104, %s_load_93
  %tmp_136_88 = fmul float %tmp_135_88, %r
  %tmp_137_88 = add i14 %phi_mul4, 89
  %tmp_138_88 = zext i14 %tmp_137_88 to i64
  %C_addr_193 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_88
  %C_load_416 = load float* %C_addr_193, align 4
  %tmp_139_88 = fadd float %C_load_416, %tmp_136_88
  store float %tmp_139_88, float* %C_addr_193, align 4
  %tmp_135_89 = fmul float %s_load_104, %s_load_94
  %tmp_136_89 = fmul float %tmp_135_89, %r
  %tmp_137_89 = add i14 %phi_mul4, 90
  %tmp_138_89 = zext i14 %tmp_137_89 to i64
  %C_addr_194 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_89
  %C_load_417 = load float* %C_addr_194, align 4
  %tmp_139_89 = fadd float %C_load_417, %tmp_136_89
  store float %tmp_139_89, float* %C_addr_194, align 4
  %tmp_135_90 = fmul float %s_load_104, %s_load_95
  %tmp_136_90 = fmul float %tmp_135_90, %r
  %tmp_137_90 = add i14 %phi_mul4, 91
  %tmp_138_90 = zext i14 %tmp_137_90 to i64
  %C_addr_195 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_90
  %C_load_418 = load float* %C_addr_195, align 4
  %tmp_139_90 = fadd float %C_load_418, %tmp_136_90
  store float %tmp_139_90, float* %C_addr_195, align 4
  %tmp_135_91 = fmul float %s_load_104, %s_load_96
  %tmp_136_91 = fmul float %tmp_135_91, %r
  %tmp_137_91 = add i14 %phi_mul4, 92
  %tmp_138_91 = zext i14 %tmp_137_91 to i64
  %C_addr_196 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_91
  %C_load_419 = load float* %C_addr_196, align 4
  %tmp_139_91 = fadd float %C_load_419, %tmp_136_91
  store float %tmp_139_91, float* %C_addr_196, align 4
  %tmp_135_92 = fmul float %s_load_104, %s_load_97
  %tmp_136_92 = fmul float %tmp_135_92, %r
  %tmp_137_92 = add i14 %phi_mul4, 93
  %tmp_138_92 = zext i14 %tmp_137_92 to i64
  %C_addr_197 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_92
  %C_load_420 = load float* %C_addr_197, align 4
  %tmp_139_92 = fadd float %C_load_420, %tmp_136_92
  store float %tmp_139_92, float* %C_addr_197, align 4
  %tmp_135_93 = fmul float %s_load_104, %s_load_98
  %tmp_136_93 = fmul float %tmp_135_93, %r
  %tmp_137_93 = add i14 %phi_mul4, 94
  %tmp_138_93 = zext i14 %tmp_137_93 to i64
  %C_addr_198 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_93
  %C_load_421 = load float* %C_addr_198, align 4
  %tmp_139_93 = fadd float %C_load_421, %tmp_136_93
  store float %tmp_139_93, float* %C_addr_198, align 4
  %tmp_135_94 = fmul float %s_load_104, %s_load_99
  %tmp_136_94 = fmul float %tmp_135_94, %r
  %tmp_137_94 = add i14 %phi_mul4, 95
  %tmp_138_94 = zext i14 %tmp_137_94 to i64
  %C_addr_199 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_94
  %C_load_422 = load float* %C_addr_199, align 4
  %tmp_139_94 = fadd float %C_load_422, %tmp_136_94
  store float %tmp_139_94, float* %C_addr_199, align 4
  %tmp_135_95 = fmul float %s_load_104, %s_load_100
  %tmp_136_95 = fmul float %tmp_135_95, %r
  %tmp_137_95 = add i14 %phi_mul4, 96
  %tmp_138_95 = zext i14 %tmp_137_95 to i64
  %C_addr_200 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_95
  %C_load_423 = load float* %C_addr_200, align 4
  %tmp_139_95 = fadd float %C_load_423, %tmp_136_95
  store float %tmp_139_95, float* %C_addr_200, align 4
  %tmp_135_96 = fmul float %s_load_104, %s_load_101
  %tmp_136_96 = fmul float %tmp_135_96, %r
  %tmp_137_96 = add i14 %phi_mul4, 97
  %tmp_138_96 = zext i14 %tmp_137_96 to i64
  %C_addr_201 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_96
  %C_load_424 = load float* %C_addr_201, align 4
  %tmp_139_96 = fadd float %C_load_424, %tmp_136_96
  store float %tmp_139_96, float* %C_addr_201, align 4
  %tmp_135_97 = fmul float %s_load_104, %s_load_102
  %tmp_136_97 = fmul float %tmp_135_97, %r
  %tmp_137_97 = add i14 %phi_mul4, 98
  %tmp_138_97 = zext i14 %tmp_137_97 to i64
  %C_addr_202 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_97
  %C_load_425 = load float* %C_addr_202, align 4
  %tmp_139_97 = fadd float %C_load_425, %tmp_136_97
  store float %tmp_139_97, float* %C_addr_202, align 4
  %tmp_135_98 = fmul float %s_load_104, %s_load_103
  %tmp_136_98 = fmul float %tmp_135_98, %r
  %tmp_137_98 = add i14 %phi_mul4, 99
  %tmp_138_98 = zext i14 %tmp_137_98 to i64
  %C_addr_203 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_98
  %C_load_426 = load float* %C_addr_203, align 4
  %tmp_139_98 = fadd float %C_load_426, %tmp_136_98
  store float %tmp_139_98, float* %C_addr_203, align 4
  %tmp_136_99 = fmul float %s_load_104, %r
  %tmp_137_99 = add i14 %phi_mul4, 100
  %tmp_138_99 = zext i14 %tmp_137_99 to i64
  %C_addr_204 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_138_99
  %C_load_427 = load float* %C_addr_204, align 4
  %tmp_139_99 = fadd float %C_load_427, %tmp_136_99
  store float %tmp_139_99, float* %C_addr_204, align 4
  %empty_19 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str12, i32 %tmp_141)
  br label %8

; <label>:10                                      ; preds = %.preheader.preheader, %.preheader
  %indvar_flatten = phi i14 [ 0, %.preheader.preheader ], [ %indvar_flatten_next, %.preheader ]
  %i6 = phi i7 [ 0, %.preheader.preheader ], [ %i6_mid2, %.preheader ]
  %j7 = phi i7 [ 0, %.preheader.preheader ], [ %j, %.preheader ]
  %exitcond_flatten = icmp eq i14 %indvar_flatten, -6183
  %indvar_flatten_next = add i14 %indvar_flatten, 1
  br i1 %exitcond_flatten, label %.preheader126, label %.preheader

.preheader:                                       ; preds = %10
  call void (...)* @_ssdm_op_SpecLoopName([30 x i8]* @UPDATE_Q_OUTER_UPDATE_Q_INNER_s)
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 10201, i64 10201, i64 10201)
  %exitcond = icmp eq i7 %j7, -27
  %j7_mid2 = select i1 %exitcond, i7 0, i7 %j7
  %i_5 = add i7 %i6, 1
  %i6_mid2 = select i1 %exitcond, i7 %i_5, i7 %i6
  %i6_cast6 = zext i7 %i6_mid2 to i14
  %tmp_102 = icmp eq i7 %i6_mid2, -28
  %tmp_103 = mul i14 %i6_cast6, 101
  %tmp_104 = zext i7 %i6_mid2 to i64
  %e_addr_4 = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp_104
  %e_load_3 = load float* %e_addr_4, align 4
  %ti = select i1 %tmp_102, float -1.000000e+00, float %e_load_3
  %j7_cast4 = zext i7 %j7_mid2 to i14
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str15) nounwind
  %tmp_142 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str15)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_105 = icmp eq i7 %j7_mid2, -28
  %tmp_106 = zext i7 %j7_mid2 to i64
  %e_addr_5 = getelementptr inbounds [101 x float]* @e, i64 0, i64 %tmp_106
  %e_load_4 = load float* %e_addr_5, align 4
  %tj = select i1 %tmp_105, float -1.000000e+00, float %e_load_4
  %tmp_107 = fmul float %ti, %tj
  %tmp_108 = fpext float %tmp_107 to double
  %tmp_109 = fdiv double %tmp_108, %tmp_99
  %tmp_110 = add i14 %j7_cast4, %tmp_103
  %tmp_111 = zext i14 %tmp_110 to i64
  %Q_addr_7 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_111
  %Q_load_6 = load float* %Q_addr_7, align 4
  %tmp_112 = fpext float %Q_load_6 to double
  %tmp_113 = fadd double %tmp_112, %tmp_109
  %tmp_114 = fptrunc double %tmp_113 to float
  store float %tmp_114, float* %Q_addr_7, align 4
  %empty_20 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str15, i32 %tmp_142)
  %j = add i7 %j7_mid2, 1
  br label %10

.preheader126:                                    ; preds = %10, %11
  %i_i1 = phi i5 [ %i_14, %11 ], [ 0, %10 ]
  %i_i1_cast2 = zext i5 %i_i1 to i32
  %exitcond_i1 = icmp eq i5 %i_i1, -11
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 21, i64 21, i64 21)
  %i_14 = add i5 %i_i1, 1
  br i1 %exitcond_i1, label %copyBV.exit, label %11

; <label>:11                                      ; preds = %.preheader126
  call void (...)* @_ssdm_op_SpecLoopName([8 x i8]* @p_str16) nounwind
  %tmp_i1 = zext i5 %i_i1 to i64
  %pX_addr_2 = getelementptr [21 x float]* %pX, i64 0, i64 %tmp_i1
  %pX_load_2 = load float* %pX_addr_2, align 4
  %bvCnt_load = load i32* @bvCnt, align 4
  %tmp_i = mul i32 %bvCnt_load, 21
  %tmp_99_i = add i32 %tmp_i, %i_i1_cast2
  %tmp_100_i = zext i32 %tmp_99_i to i64
  %basisVectors_addr_3 = getelementptr inbounds [2121 x float]* @basisVectors, i64 0, i64 %tmp_100_i
  store float %pX_load_2, float* %basisVectors_addr_3, align 4
  br label %.preheader126

copyBV.exit:                                      ; preds = %.preheader126
  %tmp_i_21 = fmul float %tmp_127, %tmp_127
  %Q_load_329 = load float* getelementptr inbounds ([10201 x float]* @Q, i64 0, i64 0), align 16
  %C_load_428 = load float* getelementptr inbounds ([10201 x float]* @C, i64 0, i64 0), align 16
  %tmp_i1_22 = fadd float %Q_load_329, %C_load_428
  %minScore = fdiv float %tmp_i_21, %tmp_i1_22
  br label %12

; <label>:12                                      ; preds = %13, %copyBV.exit
  %index_3 = phi i7 [ 1, %copyBV.exit ], [ %i_15, %13 ]
  %minScore1_i = phi float [ %minScore, %copyBV.exit ], [ %minScore_4, %13 ]
  %index = phi i32 [ 0, %copyBV.exit ], [ %index_4, %13 ]
  %index_3_cast1 = zext i7 %index_3 to i32
  %index_3_cast1_cast = zext i7 %index_3 to i15
  %exitcond_i2 = icmp eq i7 %index_3, -27
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 100, i64 100, i64 100) nounwind
  br i1 %exitcond_i2, label %getMinKLApprox.exit, label %13

; <label>:13                                      ; preds = %12
  %tmp_20_i = zext i7 %index_3 to i64
  %alpha_addr_3 = getelementptr inbounds [101 x float]* @alpha, i64 0, i64 %tmp_20_i
  %alpha_load_203 = load float* %alpha_addr_3, align 4
  %tmp_21_i = fmul float %alpha_load_203, %alpha_load_203
  %tmp_22_i = mul i15 102, %index_3_cast1_cast
  %tmp_23_i = zext i15 %tmp_22_i to i64
  %Q_addr_104 = getelementptr inbounds [10201 x float]* @Q, i64 0, i64 %tmp_23_i
  %Q_load_330 = load float* %Q_addr_104, align 8
  %C_addr_205 = getelementptr inbounds [10201 x float]* @C, i64 0, i64 %tmp_23_i
  %C_load_429 = load float* %C_addr_205, align 8
  %tmp_24_i = fadd float %Q_load_330, %C_load_429
  %tScore = fdiv float %tmp_21_i, %tmp_24_i
  %tScore_to_int = bitcast float %tScore to i32
  %tmp_11 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tScore_to_int, i32 23, i32 30)
  %tmp = trunc i32 %tScore_to_int to i23
  %minScore1_i_to_int = bitcast float %minScore1_i to i32
  %tmp_13 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %minScore1_i_to_int, i32 23, i32 30)
  %tmp_6 = trunc i32 %minScore1_i_to_int to i23
  %notlhs = icmp ne i8 %tmp_11, -1
  %notrhs = icmp eq i23 %tmp, 0
  %tmp_15 = or i1 %notrhs, %notlhs
  %notlhs1 = icmp ne i8 %tmp_13, -1
  %notrhs1 = icmp eq i23 %tmp_6, 0
  %tmp_16 = or i1 %notrhs1, %notlhs1
  %tmp_17 = and i1 %tmp_15, %tmp_16
  %tmp_18 = fcmp olt float %tScore, %minScore1_i
  %tmp_19 = and i1 %tmp_17, %tmp_18
  %minScore_4 = select i1 %tmp_19, float %tScore, float %minScore1_i
  %index_4 = select i1 %tmp_19, i32 %index_3_cast1, i32 %index
  %i_15 = add i7 1, %index_3
  br label %12

getMinKLApprox.exit:                              ; preds = %12
  call fastcc void @projection_gp_deleteBV(i32 %index)
  ret void
}

define internal fastcc void @projection_gp_swapRowAndColumn([10201 x float]* nocapture %pM, i32 %rowA) {
  %rowA_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %rowA)
  %tmp = mul i32 %rowA_read, 101
  br label %1

; <label>:1                                       ; preds = %2, %0
  %i = phi i7 [ 0, %0 ], [ %i_2, %2 ]
  %i_cast4 = zext i7 %i to i14
  %i_cast3 = zext i7 %i to i32
  %exitcond1 = icmp eq i7 %i, -27
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 101, i64 101, i64 101)
  %i_2 = add i7 %i, 1
  br i1 %exitcond1, label %.preheader, label %2

; <label>:2                                       ; preds = %1
  %tmp_6 = add i32 %i_cast3, %tmp
  %tmp_7 = zext i32 %tmp_6 to i64
  %pM_addr = getelementptr [10201 x float]* %pM, i64 0, i64 %tmp_7
  %temp = load float* %pM_addr, align 4
  %tmp_8 = add i14 %i_cast4, -6284
  %tmp_9 = zext i14 %tmp_8 to i64
  %pM_addr_1 = getelementptr [10201 x float]* %pM, i64 0, i64 %tmp_9
  %pM_load = load float* %pM_addr_1, align 4
  store float %pM_load, float* %pM_addr, align 4
  store float %temp, float* %pM_addr_1, align 4
  br label %1

.preheader:                                       ; preds = %1, %3
  %i1 = phi i7 [ %i_3, %3 ], [ 0, %1 ]
  %phi_mul = phi i14 [ %next_mul, %3 ], [ 0, %1 ]
  %exitcond = icmp eq i7 %i1, -27
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 101, i64 101, i64 101)
  %i_3 = add i7 %i1, 1
  br i1 %exitcond, label %4, label %3

; <label>:3                                       ; preds = %.preheader
  %next_mul = add i14 %phi_mul, 101
  %tmp_cast5 = zext i14 %phi_mul to i32
  %tmp_1 = add i32 %tmp_cast5, %rowA_read
  %tmp_2 = zext i32 %tmp_1 to i64
  %pM_addr_2 = getelementptr [10201 x float]* %pM, i64 0, i64 %tmp_2
  %temp_3 = load float* %pM_addr_2, align 4
  %tmp_3 = add i14 %phi_mul, 100
  %tmp_4 = zext i14 %tmp_3 to i64
  %pM_addr_3 = getelementptr [10201 x float]* %pM, i64 0, i64 %tmp_4
  %pM_load_2 = load float* %pM_addr_3, align 4
  store float %pM_load_2, float* %pM_addr_2, align 4
  store float %temp_3, float* %pM_addr_3, align 4
  br label %.preheader

; <label>:4                                       ; preds = %.preheader
  ret void
}

define internal fastcc float @projection_gp_K([2121 x float]* nocapture %pX1, i13 %tmp_152, [21 x float]* nocapture %pX2) readonly {
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([21 x float]* %pX2, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3)
  %tmp_152_read = call i13 @_ssdm_op_Read.ap_auto.i13(i13 %tmp_152)
  %tmp = trunc i13 %tmp_152_read to i12
  br label %1

; <label>:1                                       ; preds = %2, %0
  %sum = phi float [ 0.000000e+00, %0 ], [ %sum_1, %2 ]
  %i = phi i5 [ 0, %0 ], [ %i_1, %2 ]
  %exitcond = icmp eq i5 %i, -11
  %i_1 = add i5 %i, 1
  br i1 %exitcond, label %3, label %2

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 21, i64 21, i64 21)
  %tmp_s = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_2 = zext i5 %i to i64
  %tmp_2_cast = zext i5 %i to i12
  %sum1 = add i12 %tmp_2_cast, %tmp
  %sum1_cast = zext i12 %sum1 to i64
  %pX1_addr = getelementptr [2121 x float]* %pX1, i64 0, i64 %sum1_cast
  %pX1_load = load float* %pX1_addr, align 4
  %pX2_addr = getelementptr [21 x float]* %pX2, i64 0, i64 %tmp_2
  %pX2_load = load float* %pX2_addr, align 4
  %val = fsub float %pX1_load, %pX2_load
  %tmp_3 = fmul float %val, %val
  %sum_1 = fadd float %sum, %tmp_3
  %empty_23 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str, i32 %tmp_s)
  br label %1

; <label>:3                                       ; preds = %1
  %p_x_assign = fmul float %sum, -5.000000e-01
  %tmp_i = call float @llvm.exp.f32(float %p_x_assign) nounwind
  ret float %tmp_i
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_24 = trunc i32 %empty to i8
  ret i8 %empty_24
}

define weak i32 @_ssdm_op_SpecMemCore(...) {
entry:
  ret i32 0
}

define weak float @_ssdm_op_Read.s_axilite.float(float) {
entry:
  ret float %0
}

define weak i1 @_ssdm_op_Read.s_axilite.i1(i1) {
entry:
  ret i1 %0
}

define weak i13 @_ssdm_op_Read.ap_auto.i13(i13) {
entry:
  ret i13 %0
}

define weak float @_ssdm_op_Read.ap_auto.float(float) {
entry:
  ret float %0
}

define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare i13 @_ssdm_op_PartSelect.i13.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i12 @_ssdm_op_PartSelect.i12.i13.i32.i32(i13, i32, i32) nounwind readnone

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0, !7, !12, !19, !26, !31, !36, !43, !50}

!0 = metadata !{metadata !1, [10201 x float]* @C}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"C", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 10200, i32 1}
!7 = metadata !{metadata !8, [10201 x float]* @Q}
!8 = metadata !{metadata !9}
!9 = metadata !{i32 0, i32 31, metadata !10}
!10 = metadata !{metadata !11}
!11 = metadata !{metadata !"Q", metadata !5, metadata !"float", i32 0, i32 31}
!12 = metadata !{metadata !13, [101 x float]* @e}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"e", metadata !17, metadata !"float", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 100, i32 1}
!19 = metadata !{metadata !20, [100 x float]* @k}
!20 = metadata !{metadata !21}
!21 = metadata !{i32 0, i32 31, metadata !22}
!22 = metadata !{metadata !23}
!23 = metadata !{metadata !"k", metadata !24, metadata !"float", i32 0, i32 31}
!24 = metadata !{metadata !25}
!25 = metadata !{i32 0, i32 99, i32 1}
!26 = metadata !{metadata !27, [101 x float]* @s}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 31, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"s", metadata !17, metadata !"float", i32 0, i32 31}
!31 = metadata !{metadata !32, [101 x float]* @alpha}
!32 = metadata !{metadata !33}
!33 = metadata !{i32 0, i32 31, metadata !34}
!34 = metadata !{metadata !35}
!35 = metadata !{metadata !"alpha", metadata !17, metadata !"float", i32 0, i32 31}
!36 = metadata !{metadata !37, [2121 x float]* @basisVectors}
!37 = metadata !{metadata !38}
!38 = metadata !{i32 0, i32 31, metadata !39}
!39 = metadata !{metadata !40}
!40 = metadata !{metadata !"basisVectors", metadata !41, metadata !"float", i32 0, i32 31}
!41 = metadata !{metadata !42}
!42 = metadata !{i32 0, i32 2120, i32 1}
!43 = metadata !{metadata !44, i32* @bvCnt}
!44 = metadata !{metadata !45}
!45 = metadata !{i32 0, i32 31, metadata !46}
!46 = metadata !{metadata !47}
!47 = metadata !{metadata !"bvCnt", metadata !48, metadata !"unsigned int", i32 0, i32 31}
!48 = metadata !{metadata !49}
!49 = metadata !{i32 0, i32 0, i32 1}
!50 = metadata !{metadata !51, [1 x i32]* @llvm_global_ctors_0}
!51 = metadata !{metadata !52}
!52 = metadata !{i32 0, i32 31, metadata !53}
!53 = metadata !{metadata !54}
!54 = metadata !{metadata !"llvm.global_ctors.0", metadata !48, metadata !"", i32 0, i32 31}
!55 = metadata !{metadata !56}
!56 = metadata !{i32 0, i32 31, metadata !57}
!57 = metadata !{metadata !58}
!58 = metadata !{metadata !"pX", metadata !59, metadata !"float", i32 0, i32 31}
!59 = metadata !{metadata !60}
!60 = metadata !{i32 0, i32 20, i32 1}
!61 = metadata !{metadata !62}
!62 = metadata !{i32 0, i32 31, metadata !63}
!63 = metadata !{metadata !64}
!64 = metadata !{metadata !"pY", metadata !65, metadata !"float", i32 0, i32 31}
!65 = metadata !{metadata !66}
!66 = metadata !{i32 0, i32 0, i32 0}
!67 = metadata !{metadata !68}
!68 = metadata !{i32 0, i32 0, metadata !69}
!69 = metadata !{metadata !70}
!70 = metadata !{metadata !"pPredict", metadata !65, metadata !"bool", i32 0, i32 0}
!71 = metadata !{metadata !72}
!72 = metadata !{i32 0, i32 31, metadata !73}
!73 = metadata !{metadata !74}
!74 = metadata !{metadata !"return", metadata !75, metadata !"float", i32 0, i32 31}
!75 = metadata !{metadata !76}
!76 = metadata !{i32 0, i32 1, i32 0}
