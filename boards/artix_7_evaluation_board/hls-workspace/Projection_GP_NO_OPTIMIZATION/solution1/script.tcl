############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project Projection_GP_NO_OPTIMIZATION
set_top projection_gp
add_files ../../../implementation/hls/Projection_GP_DIM_20/include/ProjectionGP.cpp
add_files ../../../implementation/hls/Projection_GP_DIM_20/include/ProjectionGP.h
add_files ../../../implementation/hls/Projection_GP_DIM_20/include/ProjectionGP_config.h
open_solution "solution1"
set_part {xc7a200tfbg676-2}
create_clock -period 5 -name default
#source "./Projection_GP_NO_OPTIMIZATION/solution1/directives.tcl"
#csim_design
csynth_design
#cosim_design
export_design -format ip_catalog
