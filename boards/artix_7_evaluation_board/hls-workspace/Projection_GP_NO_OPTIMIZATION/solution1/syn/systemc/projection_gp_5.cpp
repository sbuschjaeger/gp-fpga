#include "projection_gp.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void projection_gp::thread_ap_sig_cseq_ST_st843_fsm_842() {
    if (ap_sig_bdd_5868.read()) {
        ap_sig_cseq_ST_st843_fsm_842 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st843_fsm_842 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st847_fsm_846() {
    if (ap_sig_bdd_2734.read()) {
        ap_sig_cseq_ST_st847_fsm_846 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st847_fsm_846 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st848_fsm_847() {
    if (ap_sig_bdd_5067.read()) {
        ap_sig_cseq_ST_st848_fsm_847 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st848_fsm_847 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st850_fsm_849() {
    if (ap_sig_bdd_3867.read()) {
        ap_sig_cseq_ST_st850_fsm_849 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st850_fsm_849 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st851_fsm_850() {
    if (ap_sig_bdd_1934.read()) {
        ap_sig_cseq_ST_st851_fsm_850 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st851_fsm_850 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st852_fsm_851() {
    if (ap_sig_bdd_5876.read()) {
        ap_sig_cseq_ST_st852_fsm_851 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st852_fsm_851 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st856_fsm_855() {
    if (ap_sig_bdd_2742.read()) {
        ap_sig_cseq_ST_st856_fsm_855 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st856_fsm_855 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st857_fsm_856() {
    if (ap_sig_bdd_5075.read()) {
        ap_sig_cseq_ST_st857_fsm_856 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st857_fsm_856 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st859_fsm_858() {
    if (ap_sig_bdd_3875.read()) {
        ap_sig_cseq_ST_st859_fsm_858 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st859_fsm_858 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st85_fsm_84() {
    if (ap_sig_bdd_3187.read()) {
        ap_sig_cseq_ST_st85_fsm_84 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st85_fsm_84 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st860_fsm_859() {
    if (ap_sig_bdd_1943.read()) {
        ap_sig_cseq_ST_st860_fsm_859 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st860_fsm_859 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st861_fsm_860() {
    if (ap_sig_bdd_5884.read()) {
        ap_sig_cseq_ST_st861_fsm_860 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st861_fsm_860 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st865_fsm_864() {
    if (ap_sig_bdd_2750.read()) {
        ap_sig_cseq_ST_st865_fsm_864 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st865_fsm_864 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st866_fsm_865() {
    if (ap_sig_bdd_5083.read()) {
        ap_sig_cseq_ST_st866_fsm_865 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st866_fsm_865 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st868_fsm_867() {
    if (ap_sig_bdd_3883.read()) {
        ap_sig_cseq_ST_st868_fsm_867 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st868_fsm_867 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st869_fsm_868() {
    if (ap_sig_bdd_1952.read()) {
        ap_sig_cseq_ST_st869_fsm_868 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st869_fsm_868 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st86_fsm_85() {
    if (ap_sig_bdd_1169.read()) {
        ap_sig_cseq_ST_st86_fsm_85 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st86_fsm_85 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st870_fsm_869() {
    if (ap_sig_bdd_5892.read()) {
        ap_sig_cseq_ST_st870_fsm_869 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st870_fsm_869 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st874_fsm_873() {
    if (ap_sig_bdd_2758.read()) {
        ap_sig_cseq_ST_st874_fsm_873 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st874_fsm_873 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st875_fsm_874() {
    if (ap_sig_bdd_5091.read()) {
        ap_sig_cseq_ST_st875_fsm_874 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st875_fsm_874 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st877_fsm_876() {
    if (ap_sig_bdd_3891.read()) {
        ap_sig_cseq_ST_st877_fsm_876 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st877_fsm_876 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st878_fsm_877() {
    if (ap_sig_bdd_1961.read()) {
        ap_sig_cseq_ST_st878_fsm_877 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st878_fsm_877 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st879_fsm_878() {
    if (ap_sig_bdd_5900.read()) {
        ap_sig_cseq_ST_st879_fsm_878 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st879_fsm_878 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st87_fsm_86() {
    if (ap_sig_bdd_5196.read()) {
        ap_sig_cseq_ST_st87_fsm_86 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st87_fsm_86 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st883_fsm_882() {
    if (ap_sig_bdd_2766.read()) {
        ap_sig_cseq_ST_st883_fsm_882 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st883_fsm_882 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st884_fsm_883() {
    if (ap_sig_bdd_5099.read()) {
        ap_sig_cseq_ST_st884_fsm_883 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st884_fsm_883 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st886_fsm_885() {
    if (ap_sig_bdd_3899.read()) {
        ap_sig_cseq_ST_st886_fsm_885 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st886_fsm_885 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st887_fsm_886() {
    if (ap_sig_bdd_1970.read()) {
        ap_sig_cseq_ST_st887_fsm_886 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st887_fsm_886 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st888_fsm_887() {
    if (ap_sig_bdd_5908.read()) {
        ap_sig_cseq_ST_st888_fsm_887 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st888_fsm_887 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st892_fsm_891() {
    if (ap_sig_bdd_2774.read()) {
        ap_sig_cseq_ST_st892_fsm_891 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st892_fsm_891 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st893_fsm_892() {
    if (ap_sig_bdd_5107.read()) {
        ap_sig_cseq_ST_st893_fsm_892 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st893_fsm_892 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st895_fsm_894() {
    if (ap_sig_bdd_3907.read()) {
        ap_sig_cseq_ST_st895_fsm_894 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st895_fsm_894 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st896_fsm_895() {
    if (ap_sig_bdd_1979.read()) {
        ap_sig_cseq_ST_st896_fsm_895 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st896_fsm_895 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st897_fsm_896() {
    if (ap_sig_bdd_5916.read()) {
        ap_sig_cseq_ST_st897_fsm_896 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st897_fsm_896 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st8_fsm_7() {
    if (ap_sig_bdd_8963.read()) {
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st901_fsm_900() {
    if (ap_sig_bdd_2782.read()) {
        ap_sig_cseq_ST_st901_fsm_900 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st901_fsm_900 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st902_fsm_901() {
    if (ap_sig_bdd_5115.read()) {
        ap_sig_cseq_ST_st902_fsm_901 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st902_fsm_901 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st910_fsm_909() {
    if (ap_sig_bdd_2890.read()) {
        ap_sig_cseq_ST_st910_fsm_909 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st910_fsm_909 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st911_fsm_910() {
    if (ap_sig_bdd_3082.read()) {
        ap_sig_cseq_ST_st911_fsm_910 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st911_fsm_910 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st91_fsm_90() {
    if (ap_sig_bdd_2062.read()) {
        ap_sig_cseq_ST_st91_fsm_90 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st91_fsm_90 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st92_fsm_91() {
    if (ap_sig_bdd_4395.read()) {
        ap_sig_cseq_ST_st92_fsm_91 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st92_fsm_91 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st94_fsm_93() {
    if (ap_sig_bdd_3195.read()) {
        ap_sig_cseq_ST_st94_fsm_93 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st94_fsm_93 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st95_fsm_94() {
    if (ap_sig_bdd_1178.read()) {
        ap_sig_cseq_ST_st95_fsm_94 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st95_fsm_94 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st96_fsm_95() {
    if (ap_sig_bdd_5204.read()) {
        ap_sig_cseq_ST_st96_fsm_95 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st96_fsm_95 = ap_const_logic_0;
    }
}

void projection_gp::thread_ap_sig_cseq_ST_st9_fsm_8() {
    if (ap_sig_bdd_8971.read()) {
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_0;
    }
}

void projection_gp::thread_basisVectors_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st5_fsm_4.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st23_fsm_22.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st32_fsm_31.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st41_fsm_40.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st50_fsm_49.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st59_fsm_58.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st68_fsm_67.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st77_fsm_76.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st86_fsm_85.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st95_fsm_94.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st104_fsm_103.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st113_fsm_112.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st122_fsm_121.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st131_fsm_130.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st140_fsm_139.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st149_fsm_148.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st158_fsm_157.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st167_fsm_166.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st176_fsm_175.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st185_fsm_184.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st194_fsm_193.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st203_fsm_202.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st212_fsm_211.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st221_fsm_220.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st230_fsm_229.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st239_fsm_238.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st248_fsm_247.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st257_fsm_256.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st266_fsm_265.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st275_fsm_274.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st284_fsm_283.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st293_fsm_292.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st302_fsm_301.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st311_fsm_310.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st320_fsm_319.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st329_fsm_328.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st338_fsm_337.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st347_fsm_346.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st356_fsm_355.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st365_fsm_364.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st374_fsm_373.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st383_fsm_382.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st392_fsm_391.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st401_fsm_400.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st410_fsm_409.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st419_fsm_418.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st428_fsm_427.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st437_fsm_436.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st446_fsm_445.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st455_fsm_454.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st464_fsm_463.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st473_fsm_472.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st482_fsm_481.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st491_fsm_490.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st500_fsm_499.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st509_fsm_508.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st518_fsm_517.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st527_fsm_526.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st536_fsm_535.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st545_fsm_544.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st554_fsm_553.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st563_fsm_562.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st572_fsm_571.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st581_fsm_580.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st590_fsm_589.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st599_fsm_598.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st608_fsm_607.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st617_fsm_616.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st626_fsm_625.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st635_fsm_634.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st644_fsm_643.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st653_fsm_652.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st662_fsm_661.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st671_fsm_670.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st680_fsm_679.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st689_fsm_688.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st698_fsm_697.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st707_fsm_706.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st716_fsm_715.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st725_fsm_724.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st734_fsm_733.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st743_fsm_742.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st752_fsm_751.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st761_fsm_760.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st770_fsm_769.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st779_fsm_778.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st788_fsm_787.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st797_fsm_796.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st806_fsm_805.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st815_fsm_814.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st824_fsm_823.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st833_fsm_832.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st842_fsm_841.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st851_fsm_850.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st860_fsm_859.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st869_fsm_868.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st878_fsm_877.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st887_fsm_886.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st896_fsm_895.read()))) {
        basisVectors_address0 = grp_projection_gp_K_fu_638_pX1_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
                esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        basisVectors_address0 = grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        basisVectors_address0 = grp_projection_gp_train_full_bv_set_fu_591_basisVectors_address0.read();
    } else {
        basisVectors_address0 =  (sc_lv<12>) ("XXXXXXXXXXXX");
    }
}

void projection_gp::thread_basisVectors_address1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        basisVectors_address1 = grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_address1.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        basisVectors_address1 = grp_projection_gp_train_full_bv_set_fu_591_basisVectors_address1.read();
    } else {
        basisVectors_address1 =  (sc_lv<12>) ("XXXXXXXXXXXX");
    }
}

void projection_gp::thread_basisVectors_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st5_fsm_4.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st23_fsm_22.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st32_fsm_31.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st41_fsm_40.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st50_fsm_49.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st59_fsm_58.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st68_fsm_67.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st77_fsm_76.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st86_fsm_85.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st95_fsm_94.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st104_fsm_103.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st113_fsm_112.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st122_fsm_121.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st131_fsm_130.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st140_fsm_139.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st149_fsm_148.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st158_fsm_157.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st167_fsm_166.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st176_fsm_175.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st185_fsm_184.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st194_fsm_193.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st203_fsm_202.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st212_fsm_211.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st221_fsm_220.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st230_fsm_229.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st239_fsm_238.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st248_fsm_247.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st257_fsm_256.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st266_fsm_265.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st275_fsm_274.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st284_fsm_283.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st293_fsm_292.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st302_fsm_301.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st311_fsm_310.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st320_fsm_319.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st329_fsm_328.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st338_fsm_337.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st347_fsm_346.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st356_fsm_355.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st365_fsm_364.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st374_fsm_373.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st383_fsm_382.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st392_fsm_391.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st401_fsm_400.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st410_fsm_409.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st419_fsm_418.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st428_fsm_427.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st437_fsm_436.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st446_fsm_445.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st455_fsm_454.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st464_fsm_463.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st473_fsm_472.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st482_fsm_481.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st491_fsm_490.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st500_fsm_499.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st509_fsm_508.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st518_fsm_517.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st527_fsm_526.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st536_fsm_535.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st545_fsm_544.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st554_fsm_553.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st563_fsm_562.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st572_fsm_571.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st581_fsm_580.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st590_fsm_589.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st599_fsm_598.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st608_fsm_607.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st617_fsm_616.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st626_fsm_625.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st635_fsm_634.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st644_fsm_643.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st653_fsm_652.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st662_fsm_661.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st671_fsm_670.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st680_fsm_679.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st689_fsm_688.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st698_fsm_697.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st707_fsm_706.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st716_fsm_715.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st725_fsm_724.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st734_fsm_733.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st743_fsm_742.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st752_fsm_751.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st761_fsm_760.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st770_fsm_769.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st779_fsm_778.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st788_fsm_787.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st797_fsm_796.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st806_fsm_805.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st815_fsm_814.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st824_fsm_823.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st833_fsm_832.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st842_fsm_841.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st851_fsm_850.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st860_fsm_859.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st869_fsm_868.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st878_fsm_877.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st887_fsm_886.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st896_fsm_895.read()))) {
        basisVectors_ce0 = grp_projection_gp_K_fu_638_pX1_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
                esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        basisVectors_ce0 = grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        basisVectors_ce0 = grp_projection_gp_train_full_bv_set_fu_591_basisVectors_ce0.read();
    } else {
        basisVectors_ce0 = ap_const_logic_0;
    }
}

void projection_gp::thread_basisVectors_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        basisVectors_ce1 = grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_ce1.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        basisVectors_ce1 = grp_projection_gp_train_full_bv_set_fu_591_basisVectors_ce1.read();
    } else {
        basisVectors_ce1 = ap_const_logic_0;
    }
}

void projection_gp::thread_basisVectors_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        basisVectors_d0 = grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        basisVectors_d0 = grp_projection_gp_train_full_bv_set_fu_591_basisVectors_d0.read();
    } else {
        basisVectors_d0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_basisVectors_d1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        basisVectors_d1 = grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_d1.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        basisVectors_d1 = grp_projection_gp_train_full_bv_set_fu_591_basisVectors_d1.read();
    } else {
        basisVectors_d1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_basisVectors_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        basisVectors_we0 = grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        basisVectors_we0 = grp_projection_gp_train_full_bv_set_fu_591_basisVectors_we0.read();
    } else {
        basisVectors_we0 = ap_const_logic_0;
    }
}

void projection_gp::thread_basisVectors_we1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        basisVectors_we1 = grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_we1.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        basisVectors_we1 = grp_projection_gp_train_full_bv_set_fu_591_basisVectors_we1.read();
    } else {
        basisVectors_we1 = ap_const_logic_0;
    }
}

void projection_gp::thread_e_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        e_address0 = grp_projection_gp_train_not_full_bv_set_fu_615_e_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        e_address0 = grp_projection_gp_train_full_bv_set_fu_591_e_address0.read();
    } else {
        e_address0 =  (sc_lv<7>) ("XXXXXXX");
    }
}

void projection_gp::thread_e_address1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        e_address1 = grp_projection_gp_train_not_full_bv_set_fu_615_e_address1.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        e_address1 = grp_projection_gp_train_full_bv_set_fu_591_e_address1.read();
    } else {
        e_address1 =  (sc_lv<7>) ("XXXXXXX");
    }
}

void projection_gp::thread_e_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        e_ce0 = grp_projection_gp_train_not_full_bv_set_fu_615_e_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        e_ce0 = grp_projection_gp_train_full_bv_set_fu_591_e_ce0.read();
    } else {
        e_ce0 = ap_const_logic_0;
    }
}

void projection_gp::thread_e_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        e_ce1 = grp_projection_gp_train_not_full_bv_set_fu_615_e_ce1.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        e_ce1 = grp_projection_gp_train_full_bv_set_fu_591_e_ce1.read();
    } else {
        e_ce1 = ap_const_logic_0;
    }
}

void projection_gp::thread_e_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        e_d0 = grp_projection_gp_train_not_full_bv_set_fu_615_e_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        e_d0 = grp_projection_gp_train_full_bv_set_fu_591_e_d0.read();
    } else {
        e_d0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_e_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        e_we0 = grp_projection_gp_train_not_full_bv_set_fu_615_e_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        e_we0 = grp_projection_gp_train_full_bv_set_fu_591_e_we0.read();
    } else {
        e_we0 = ap_const_logic_0;
    }
}

void projection_gp::thread_grp_fu_747_ce() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st5_fsm_4.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st10_fsm_9.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st911_fsm_910.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st2_fsm_1.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st6_fsm_5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st23_fsm_22.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st32_fsm_31.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st41_fsm_40.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st50_fsm_49.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st59_fsm_58.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st68_fsm_67.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st77_fsm_76.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st86_fsm_85.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st95_fsm_94.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st104_fsm_103.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st113_fsm_112.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st122_fsm_121.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st131_fsm_130.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st140_fsm_139.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st149_fsm_148.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st158_fsm_157.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st167_fsm_166.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st176_fsm_175.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st185_fsm_184.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st194_fsm_193.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st203_fsm_202.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st212_fsm_211.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st221_fsm_220.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st230_fsm_229.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st239_fsm_238.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st248_fsm_247.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st257_fsm_256.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st266_fsm_265.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st275_fsm_274.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st284_fsm_283.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st293_fsm_292.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st302_fsm_301.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st311_fsm_310.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st320_fsm_319.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st329_fsm_328.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st338_fsm_337.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st347_fsm_346.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st356_fsm_355.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st365_fsm_364.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st374_fsm_373.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st383_fsm_382.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st392_fsm_391.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st401_fsm_400.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st410_fsm_409.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st419_fsm_418.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st428_fsm_427.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st437_fsm_436.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st446_fsm_445.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st455_fsm_454.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st464_fsm_463.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st473_fsm_472.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st482_fsm_481.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st491_fsm_490.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st500_fsm_499.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st509_fsm_508.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st518_fsm_517.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st527_fsm_526.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st536_fsm_535.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st545_fsm_544.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st554_fsm_553.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st563_fsm_562.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st572_fsm_571.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st581_fsm_580.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st590_fsm_589.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st599_fsm_598.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st608_fsm_607.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st617_fsm_616.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st626_fsm_625.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st635_fsm_634.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st644_fsm_643.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st653_fsm_652.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st662_fsm_661.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st671_fsm_670.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st680_fsm_679.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st689_fsm_688.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st698_fsm_697.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st707_fsm_706.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st716_fsm_715.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st725_fsm_724.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st734_fsm_733.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st743_fsm_742.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st752_fsm_751.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st761_fsm_760.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st770_fsm_769.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st779_fsm_778.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st788_fsm_787.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st797_fsm_796.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st806_fsm_805.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st815_fsm_814.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st824_fsm_823.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st833_fsm_832.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st842_fsm_841.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st851_fsm_850.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st860_fsm_859.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st869_fsm_868.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st878_fsm_877.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st887_fsm_886.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_0, grp_projection_gp_K_fu_638_ap_done.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st896_fsm_895.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st7_fsm_6.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st8_fsm_7.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st9_fsm_8.read()))) {
        grp_fu_747_ce = ap_const_logic_0;
    } else {
        grp_fu_747_ce = ap_const_logic_1;
    }
}

void projection_gp::thread_grp_fu_747_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st20_fsm_19.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st29_fsm_28.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st38_fsm_37.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st47_fsm_46.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st56_fsm_55.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st65_fsm_64.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st74_fsm_73.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st83_fsm_82.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st92_fsm_91.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st101_fsm_100.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st110_fsm_109.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st119_fsm_118.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st128_fsm_127.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st137_fsm_136.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st146_fsm_145.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st155_fsm_154.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st164_fsm_163.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st173_fsm_172.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st182_fsm_181.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st191_fsm_190.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st200_fsm_199.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st209_fsm_208.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st218_fsm_217.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st227_fsm_226.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st236_fsm_235.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st245_fsm_244.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st254_fsm_253.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st263_fsm_262.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st272_fsm_271.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st281_fsm_280.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st290_fsm_289.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st299_fsm_298.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st308_fsm_307.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st317_fsm_316.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st326_fsm_325.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st335_fsm_334.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st344_fsm_343.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st353_fsm_352.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st362_fsm_361.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st371_fsm_370.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st380_fsm_379.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st389_fsm_388.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st398_fsm_397.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st407_fsm_406.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st416_fsm_415.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st425_fsm_424.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st434_fsm_433.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st443_fsm_442.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st452_fsm_451.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st461_fsm_460.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st470_fsm_469.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st479_fsm_478.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st488_fsm_487.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st497_fsm_496.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st506_fsm_505.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st515_fsm_514.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st524_fsm_523.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st533_fsm_532.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st542_fsm_541.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st551_fsm_550.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st560_fsm_559.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st569_fsm_568.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st578_fsm_577.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st587_fsm_586.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st596_fsm_595.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st605_fsm_604.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st614_fsm_613.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st623_fsm_622.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st632_fsm_631.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st641_fsm_640.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st650_fsm_649.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st659_fsm_658.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st668_fsm_667.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st677_fsm_676.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st686_fsm_685.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st695_fsm_694.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st704_fsm_703.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st713_fsm_712.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st722_fsm_721.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st731_fsm_730.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st740_fsm_739.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st749_fsm_748.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st758_fsm_757.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st767_fsm_766.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st776_fsm_775.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st785_fsm_784.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st794_fsm_793.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st803_fsm_802.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st812_fsm_811.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st821_fsm_820.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st830_fsm_829.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st839_fsm_838.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st848_fsm_847.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st857_fsm_856.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st866_fsm_865.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st875_fsm_874.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st884_fsm_883.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st893_fsm_892.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st902_fsm_901.read()))) {
        grp_fu_747_p0 = reg_776.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_10.read())) {
        grp_fu_747_p0 = reg_770.read();
    } else {
        grp_fu_747_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_grp_fu_747_p1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st20_fsm_19.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st29_fsm_28.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st38_fsm_37.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st47_fsm_46.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st56_fsm_55.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st65_fsm_64.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st74_fsm_73.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st83_fsm_82.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st92_fsm_91.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st101_fsm_100.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st110_fsm_109.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st119_fsm_118.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st128_fsm_127.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st137_fsm_136.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st146_fsm_145.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st155_fsm_154.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st164_fsm_163.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st173_fsm_172.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st182_fsm_181.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st191_fsm_190.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st200_fsm_199.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st209_fsm_208.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st218_fsm_217.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st227_fsm_226.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st236_fsm_235.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st245_fsm_244.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st254_fsm_253.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st263_fsm_262.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st272_fsm_271.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st281_fsm_280.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st290_fsm_289.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st299_fsm_298.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st308_fsm_307.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st317_fsm_316.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st326_fsm_325.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st335_fsm_334.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st344_fsm_343.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st353_fsm_352.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st362_fsm_361.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st371_fsm_370.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st380_fsm_379.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st389_fsm_388.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st398_fsm_397.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st407_fsm_406.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st416_fsm_415.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st425_fsm_424.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st434_fsm_433.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st443_fsm_442.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st452_fsm_451.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st461_fsm_460.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st470_fsm_469.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st479_fsm_478.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st488_fsm_487.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st497_fsm_496.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st506_fsm_505.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st515_fsm_514.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st524_fsm_523.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st533_fsm_532.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st542_fsm_541.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st551_fsm_550.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st560_fsm_559.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st569_fsm_568.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st578_fsm_577.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st587_fsm_586.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st596_fsm_595.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st605_fsm_604.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st614_fsm_613.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st623_fsm_622.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st632_fsm_631.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st641_fsm_640.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st650_fsm_649.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st659_fsm_658.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st668_fsm_667.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st677_fsm_676.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st686_fsm_685.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st695_fsm_694.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st704_fsm_703.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st713_fsm_712.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st722_fsm_721.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st731_fsm_730.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st740_fsm_739.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st749_fsm_748.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st758_fsm_757.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st767_fsm_766.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st776_fsm_775.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st785_fsm_784.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st794_fsm_793.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st803_fsm_802.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st812_fsm_811.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st821_fsm_820.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st830_fsm_829.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st839_fsm_838.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st848_fsm_847.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st857_fsm_856.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st866_fsm_865.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st875_fsm_874.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st884_fsm_883.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st893_fsm_892.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st902_fsm_901.read()))) {
        grp_fu_747_p1 = reg_770.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st11_fsm_10.read())) {
        grp_fu_747_p1 = ap_const_lv32_40A00000;
    } else {
        grp_fu_747_p1 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_grp_fu_752_ce() {
    grp_fu_752_ce = ap_const_logic_1;
}

void projection_gp::thread_grp_fu_752_p0() {
    grp_fu_752_p0 = reg_760.read();
}

void projection_gp::thread_grp_fu_752_p1() {
    grp_fu_752_p1 = reg_765.read();
}

void projection_gp::thread_grp_projection_gp_K_fu_638_ap_start() {
    grp_projection_gp_K_fu_638_ap_start = grp_projection_gp_K_fu_638_ap_start_ap_start_reg.read();
}

void projection_gp::thread_grp_projection_gp_K_fu_638_pX1_q0() {
    grp_projection_gp_K_fu_638_pX1_q0 = basisVectors_q0.read();
}

void projection_gp::thread_grp_projection_gp_K_fu_638_pX2_q0() {
    grp_projection_gp_K_fu_638_pX2_q0 = pX_q0.read();
}

void projection_gp::thread_grp_projection_gp_K_fu_638_tmp_152() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st896_fsm_895.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_81F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st887_fsm_886.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_80A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st878_fsm_877.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_7F5;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st869_fsm_868.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_7E0;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st860_fsm_859.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_7CB;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st851_fsm_850.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_7B6;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st842_fsm_841.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_7A1;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st833_fsm_832.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_78C;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st824_fsm_823.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_777;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st815_fsm_814.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_762;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st806_fsm_805.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_74D;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st797_fsm_796.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_738;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st788_fsm_787.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_723;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st779_fsm_778.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_70E;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st770_fsm_769.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_6F9;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st761_fsm_760.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_6E4;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st752_fsm_751.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_6CF;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st743_fsm_742.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_6BA;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st734_fsm_733.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_6A5;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st725_fsm_724.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_690;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st716_fsm_715.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_67B;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st707_fsm_706.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_666;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st698_fsm_697.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_651;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st689_fsm_688.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_63C;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st680_fsm_679.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_627;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st671_fsm_670.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_612;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st662_fsm_661.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_5FD;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st653_fsm_652.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_5E8;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st644_fsm_643.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_5D3;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st635_fsm_634.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_5BE;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st626_fsm_625.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_5A9;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st617_fsm_616.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_594;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st608_fsm_607.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_57F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st599_fsm_598.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_56A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st590_fsm_589.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_555;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st581_fsm_580.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_540;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st572_fsm_571.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_52B;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st563_fsm_562.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_516;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st554_fsm_553.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_501;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st545_fsm_544.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_4EC;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st536_fsm_535.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_4D7;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st527_fsm_526.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_4C2;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st518_fsm_517.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_4AD;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st509_fsm_508.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_498;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st500_fsm_499.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_483;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st491_fsm_490.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_46E;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st482_fsm_481.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_459;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st473_fsm_472.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_444;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st464_fsm_463.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_42F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st455_fsm_454.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_41A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st446_fsm_445.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_405;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st437_fsm_436.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_3F0;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st428_fsm_427.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_3DB;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st419_fsm_418.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_3C6;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st410_fsm_409.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_3B1;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st401_fsm_400.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_39C;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st392_fsm_391.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_387;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st383_fsm_382.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_372;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st374_fsm_373.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_35D;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st365_fsm_364.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_348;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st356_fsm_355.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_333;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st347_fsm_346.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_31E;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st338_fsm_337.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_309;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st329_fsm_328.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_2F4;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st320_fsm_319.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_2DF;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st311_fsm_310.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_2CA;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st302_fsm_301.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_2B5;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st293_fsm_292.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_2A0;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st284_fsm_283.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_28B;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st275_fsm_274.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_276;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st266_fsm_265.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_261;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st257_fsm_256.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_24C;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st248_fsm_247.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_237;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st239_fsm_238.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_222;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st230_fsm_229.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_20D;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st221_fsm_220.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_1F8;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st212_fsm_211.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_1E3;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st203_fsm_202.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_1CE;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st194_fsm_193.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_1B9;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st185_fsm_184.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_1A4;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st176_fsm_175.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_18F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st167_fsm_166.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_17A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st158_fsm_157.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_165;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st149_fsm_148.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_150;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st140_fsm_139.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_13B;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st131_fsm_130.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_126;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st122_fsm_121.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_111;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st113_fsm_112.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_FC;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st104_fsm_103.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_E7;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st95_fsm_94.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_D2;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st86_fsm_85.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_BD;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st77_fsm_76.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_A8;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st68_fsm_67.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_93;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st59_fsm_58.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_7E;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st50_fsm_49.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_69;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st41_fsm_40.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_54;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st32_fsm_31.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_3F;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st23_fsm_22.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_2A;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_15;
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st5_fsm_4.read())) {
        grp_projection_gp_K_fu_638_tmp_152 = ap_const_lv13_0;
    } else {
        grp_projection_gp_K_fu_638_tmp_152 =  (sc_lv<13>) ("XXXXXXXXXXXXX");
    }
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_C_q0() {
    grp_projection_gp_train_full_bv_set_fu_591_C_q0 = C_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_C_q1() {
    grp_projection_gp_train_full_bv_set_fu_591_C_q1 = C_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_Q_q0() {
    grp_projection_gp_train_full_bv_set_fu_591_Q_q0 = Q_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_Q_q1() {
    grp_projection_gp_train_full_bv_set_fu_591_Q_q1 = Q_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_alpha_q0() {
    grp_projection_gp_train_full_bv_set_fu_591_alpha_q0 = alpha_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_alpha_q1() {
    grp_projection_gp_train_full_bv_set_fu_591_alpha_q1 = alpha_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_ap_start() {
    grp_projection_gp_train_full_bv_set_fu_591_ap_start = grp_projection_gp_train_full_bv_set_fu_591_ap_start_ap_start_reg.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_basisVectors_q0() {
    grp_projection_gp_train_full_bv_set_fu_591_basisVectors_q0 = basisVectors_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_bvCnt() {
    grp_projection_gp_train_full_bv_set_fu_591_bvCnt = bvCnt.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_e_q0() {
    grp_projection_gp_train_full_bv_set_fu_591_e_q0 = e_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_e_q1() {
    grp_projection_gp_train_full_bv_set_fu_591_e_q1 = e_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_k_q0() {
    grp_projection_gp_train_full_bv_set_fu_591_k_q0 = k_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_k_q1() {
    grp_projection_gp_train_full_bv_set_fu_591_k_q1 = k_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_pX_q0() {
    grp_projection_gp_train_full_bv_set_fu_591_pX_q0 = pX_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_pY() {
    grp_projection_gp_train_full_bv_set_fu_591_pY = pY_read_reg_804.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_s_q0() {
    grp_projection_gp_train_full_bv_set_fu_591_s_q0 = s_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_full_bv_set_fu_591_s_q1() {
    grp_projection_gp_train_full_bv_set_fu_591_s_q1 = s_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_C_q0() {
    grp_projection_gp_train_not_full_bv_set_fu_615_C_q0 = C_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_C_q1() {
    grp_projection_gp_train_not_full_bv_set_fu_615_C_q1 = C_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_Q_q0() {
    grp_projection_gp_train_not_full_bv_set_fu_615_Q_q0 = Q_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_Q_q1() {
    grp_projection_gp_train_not_full_bv_set_fu_615_Q_q1 = Q_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_alpha_q0() {
    grp_projection_gp_train_not_full_bv_set_fu_615_alpha_q0 = alpha_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_alpha_q1() {
    grp_projection_gp_train_not_full_bv_set_fu_615_alpha_q1 = alpha_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_ap_start() {
    grp_projection_gp_train_not_full_bv_set_fu_615_ap_start = grp_projection_gp_train_not_full_bv_set_fu_615_ap_start_ap_start_reg.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_q0() {
    grp_projection_gp_train_not_full_bv_set_fu_615_basisVectors_q0 = basisVectors_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_bvCnt_i() {
    grp_projection_gp_train_not_full_bv_set_fu_615_bvCnt_i = bvCnt.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_e_q0() {
    grp_projection_gp_train_not_full_bv_set_fu_615_e_q0 = e_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_e_q1() {
    grp_projection_gp_train_not_full_bv_set_fu_615_e_q1 = e_q1.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_k_q0() {
    grp_projection_gp_train_not_full_bv_set_fu_615_k_q0 = k_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_pX_q0() {
    grp_projection_gp_train_not_full_bv_set_fu_615_pX_q0 = pX_q0.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_pY() {
    grp_projection_gp_train_not_full_bv_set_fu_615_pY = pY_read_reg_804.read();
}

void projection_gp::thread_grp_projection_gp_train_not_full_bv_set_fu_615_s_q0() {
    grp_projection_gp_train_not_full_bv_set_fu_615_s_q0 = s_q0.read();
}

void projection_gp::thread_k_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        k_address0 = grp_projection_gp_train_not_full_bv_set_fu_615_k_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        k_address0 = grp_projection_gp_train_full_bv_set_fu_591_k_address0.read();
    } else {
        k_address0 =  (sc_lv<7>) ("XXXXXXX");
    }
}

void projection_gp::thread_k_address1() {
    k_address1 = grp_projection_gp_train_full_bv_set_fu_591_k_address1.read();
}

void projection_gp::thread_k_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        k_ce0 = grp_projection_gp_train_not_full_bv_set_fu_615_k_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        k_ce0 = grp_projection_gp_train_full_bv_set_fu_591_k_ce0.read();
    } else {
        k_ce0 = ap_const_logic_0;
    }
}

void projection_gp::thread_k_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        k_ce1 = grp_projection_gp_train_full_bv_set_fu_591_k_ce1.read();
    } else {
        k_ce1 = ap_const_logic_0;
    }
}

void projection_gp::thread_k_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        k_d0 = grp_projection_gp_train_not_full_bv_set_fu_615_k_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        k_d0 = grp_projection_gp_train_full_bv_set_fu_591_k_d0.read();
    } else {
        k_d0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_k_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        k_we0 = grp_projection_gp_train_not_full_bv_set_fu_615_k_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        k_we0 = grp_projection_gp_train_full_bv_set_fu_591_k_we0.read();
    } else {
        k_we0 = ap_const_logic_0;
    }
}

void projection_gp::thread_pPredict_read_read_fu_464_p2() {
    pPredict_read_read_fu_464_p2 =  (sc_lv<1>) (pPredict.read());
}

void projection_gp::thread_pX_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st5_fsm_4.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st23_fsm_22.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st32_fsm_31.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st41_fsm_40.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st50_fsm_49.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st59_fsm_58.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st68_fsm_67.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st77_fsm_76.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st86_fsm_85.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st95_fsm_94.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st104_fsm_103.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st113_fsm_112.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st122_fsm_121.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st131_fsm_130.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st140_fsm_139.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st149_fsm_148.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st158_fsm_157.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st167_fsm_166.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st176_fsm_175.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st185_fsm_184.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st194_fsm_193.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st203_fsm_202.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st212_fsm_211.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st221_fsm_220.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st230_fsm_229.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st239_fsm_238.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st248_fsm_247.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st257_fsm_256.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st266_fsm_265.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st275_fsm_274.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st284_fsm_283.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st293_fsm_292.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st302_fsm_301.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st311_fsm_310.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st320_fsm_319.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st329_fsm_328.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st338_fsm_337.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st347_fsm_346.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st356_fsm_355.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st365_fsm_364.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st374_fsm_373.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st383_fsm_382.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st392_fsm_391.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st401_fsm_400.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st410_fsm_409.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st419_fsm_418.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st428_fsm_427.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st437_fsm_436.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st446_fsm_445.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st455_fsm_454.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st464_fsm_463.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st473_fsm_472.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st482_fsm_481.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st491_fsm_490.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st500_fsm_499.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st509_fsm_508.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st518_fsm_517.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st527_fsm_526.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st536_fsm_535.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st545_fsm_544.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st554_fsm_553.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st563_fsm_562.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st572_fsm_571.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st581_fsm_580.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st590_fsm_589.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st599_fsm_598.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st608_fsm_607.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st617_fsm_616.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st626_fsm_625.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st635_fsm_634.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st644_fsm_643.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st653_fsm_652.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st662_fsm_661.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st671_fsm_670.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st680_fsm_679.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st689_fsm_688.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st698_fsm_697.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st707_fsm_706.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st716_fsm_715.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st725_fsm_724.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st734_fsm_733.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st743_fsm_742.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st752_fsm_751.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st761_fsm_760.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st770_fsm_769.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st779_fsm_778.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st788_fsm_787.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st797_fsm_796.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st806_fsm_805.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st815_fsm_814.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st824_fsm_823.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st833_fsm_832.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st842_fsm_841.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st851_fsm_850.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st860_fsm_859.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st869_fsm_868.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st878_fsm_877.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st887_fsm_886.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st896_fsm_895.read()))) {
        pX_address0 = grp_projection_gp_K_fu_638_pX2_address0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
                esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        pX_address0 = grp_projection_gp_train_not_full_bv_set_fu_615_pX_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        pX_address0 = grp_projection_gp_train_full_bv_set_fu_591_pX_address0.read();
    } else {
        pX_address0 =  (sc_lv<5>) ("XXXXX");
    }
}

void projection_gp::thread_pX_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st5_fsm_4.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st14_fsm_13.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st23_fsm_22.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st32_fsm_31.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st41_fsm_40.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st50_fsm_49.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st59_fsm_58.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st68_fsm_67.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st77_fsm_76.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st86_fsm_85.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st95_fsm_94.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st104_fsm_103.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st113_fsm_112.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st122_fsm_121.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st131_fsm_130.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st140_fsm_139.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st149_fsm_148.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st158_fsm_157.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st167_fsm_166.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st176_fsm_175.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st185_fsm_184.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st194_fsm_193.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st203_fsm_202.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st212_fsm_211.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st221_fsm_220.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st230_fsm_229.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st239_fsm_238.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st248_fsm_247.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st257_fsm_256.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st266_fsm_265.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st275_fsm_274.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st284_fsm_283.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st293_fsm_292.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st302_fsm_301.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st311_fsm_310.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st320_fsm_319.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st329_fsm_328.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st338_fsm_337.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st347_fsm_346.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st356_fsm_355.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st365_fsm_364.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st374_fsm_373.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st383_fsm_382.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st392_fsm_391.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st401_fsm_400.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st410_fsm_409.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st419_fsm_418.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st428_fsm_427.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st437_fsm_436.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st446_fsm_445.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st455_fsm_454.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st464_fsm_463.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st473_fsm_472.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st482_fsm_481.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st491_fsm_490.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st500_fsm_499.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st509_fsm_508.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st518_fsm_517.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st527_fsm_526.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st536_fsm_535.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st545_fsm_544.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st554_fsm_553.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st563_fsm_562.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st572_fsm_571.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st581_fsm_580.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st590_fsm_589.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st599_fsm_598.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st608_fsm_607.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st617_fsm_616.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st626_fsm_625.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st635_fsm_634.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st644_fsm_643.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st653_fsm_652.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st662_fsm_661.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st671_fsm_670.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st680_fsm_679.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st689_fsm_688.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st698_fsm_697.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st707_fsm_706.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st716_fsm_715.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st725_fsm_724.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st734_fsm_733.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st743_fsm_742.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st752_fsm_751.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st761_fsm_760.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st770_fsm_769.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st779_fsm_778.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st788_fsm_787.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st797_fsm_796.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st806_fsm_805.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st815_fsm_814.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st824_fsm_823.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st833_fsm_832.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st842_fsm_841.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st851_fsm_850.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st860_fsm_859.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st869_fsm_868.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st878_fsm_877.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st887_fsm_886.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st896_fsm_895.read()))) {
        pX_ce0 = grp_projection_gp_K_fu_638_pX2_ce0.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
                esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        pX_ce0 = grp_projection_gp_train_not_full_bv_set_fu_615_pX_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        pX_ce0 = grp_projection_gp_train_full_bv_set_fu_591_pX_ce0.read();
    } else {
        pX_ce0 = ap_const_logic_0;
    }
}

void projection_gp::thread_p_0_phi_fu_584_p4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st911_fsm_910.read()) && 
         !esl_seteq<1,1,1>(pPredict_read_reg_800.read(), ap_const_lv1_0))) {
        p_0_phi_fu_584_p4 = reg_776.read();
    } else {
        p_0_phi_fu_584_p4 = p_0_reg_580.read();
    }
}

void projection_gp::thread_projection_gp_AXILiteS_s_axi_U_ap_dummy_ce() {
    projection_gp_AXILiteS_s_axi_U_ap_dummy_ce = ap_const_logic_1;
}

void projection_gp::thread_s_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        s_address0 = grp_projection_gp_train_not_full_bv_set_fu_615_s_address0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        s_address0 = grp_projection_gp_train_full_bv_set_fu_591_s_address0.read();
    } else {
        s_address0 =  (sc_lv<7>) ("XXXXXXX");
    }
}

void projection_gp::thread_s_address1() {
    s_address1 = grp_projection_gp_train_full_bv_set_fu_591_s_address1.read();
}

void projection_gp::thread_s_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        s_ce0 = grp_projection_gp_train_not_full_bv_set_fu_615_s_ce0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        s_ce0 = grp_projection_gp_train_full_bv_set_fu_591_s_ce0.read();
    } else {
        s_ce0 = ap_const_logic_0;
    }
}

void projection_gp::thread_s_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        s_ce1 = grp_projection_gp_train_full_bv_set_fu_591_s_ce1.read();
    } else {
        s_ce1 = ap_const_logic_0;
    }
}

void projection_gp::thread_s_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        s_d0 = grp_projection_gp_train_not_full_bv_set_fu_615_s_d0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        s_d0 = grp_projection_gp_train_full_bv_set_fu_591_s_d0.read();
    } else {
        s_d0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void projection_gp::thread_s_d1() {
    s_d1 = grp_projection_gp_train_full_bv_set_fu_591_s_d1.read();
}

void projection_gp::thread_s_we0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st3_fsm_2.read()) && 
         esl_seteq<1,1,1>(tmp_s_reg_810.read(), ap_const_lv1_0))) {
        s_we0 = grp_projection_gp_train_not_full_bv_set_fu_615_s_we0.read();
    } else if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        s_we0 = grp_projection_gp_train_full_bv_set_fu_591_s_we0.read();
    } else {
        s_we0 = ap_const_logic_0;
    }
}

void projection_gp::thread_s_we1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st4_fsm_3.read())) {
        s_we1 = grp_projection_gp_train_full_bv_set_fu_591_s_we1.read();
    } else {
        s_we1 = ap_const_logic_0;
    }
}

void projection_gp::thread_tmp_26_fu_788_p2() {
    tmp_26_fu_788_p2 = (!bvCnt.read().is_01() || !ap_const_lv32_1.is_01())? sc_lv<32>(): (sc_biguint<32>(bvCnt.read()) + sc_biguint<32>(ap_const_lv32_1));
}

void projection_gp::thread_tmp_s_fu_782_p2() {
    tmp_s_fu_782_p2 = (!bvCnt.read().is_01() || !ap_const_lv32_64.is_01())? sc_lv<1>(): sc_lv<1>(bvCnt.read() == ap_const_lv32_64);
}

}

