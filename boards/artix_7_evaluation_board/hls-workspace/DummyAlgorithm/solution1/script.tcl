############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project DummyAlgorithm
set_top dummy_algorithm
add_files ../../../implementation/hls/DummyAlgorithm/include/DummyAlgorithm.cpp
add_files ../../../implementation/hls/DummyAlgorithm/include/DummyAlgorithm.h
add_files ../../../implementation/hls/DummyAlgorithm/include/DummyAlgorithm_config.h
add_files -tb ../../../implementation/hls/DummyAlgorithm/test/testDummyAlgorithm.cpp
open_solution "solution1"
set_part {xc7a200tfbg676-2}
create_clock -period 5 -name default
#source "./DummyAlgorithm/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog -description "Simple Perceptron" -vendor "de.tudo.buschjae" -library "ml" -display_name "perceptron_dim_2"
