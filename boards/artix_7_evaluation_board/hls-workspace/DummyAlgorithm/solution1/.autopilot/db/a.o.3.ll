; ModuleID = '/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace/DummyAlgorithm/solution1/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@weights = global [3 x float] zeroinitializer, align 4 ; [#uses=5 type=[3 x float]*]
@dummy_algorithm_str = internal unnamed_addr constant [16 x i8] c"dummy_algorithm\00" ; [#uses=1 type=[16 x i8]*]
@RAM_1P_str = internal unnamed_addr constant [7 x i8] c"RAM_1P\00" ; [#uses=1 type=[7 x i8]*]
@p_str3 = internal unnamed_addr constant [1 x i8] zeroinitializer ; [#uses=5 type=[1 x i8]*]
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=20 type=[1 x i8]*]
@p_str = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1 ; [#uses=5 type=[10 x i8]*]

; [#uses=1]
declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

; [#uses=14]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=0]
define float @dummy_algorithm([2 x float]* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pReset) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([2 x float]* %pX) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(float %pY) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pPredict) nounwind, !map !19
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pReset) nounwind, !map !23
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !27
  call void (...)* @_ssdm_op_SpecTopModule([16 x i8]* @dummy_algorithm_str) nounwind
  %pReset_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pReset) nounwind ; [#uses=1 type=i1]
  call void @llvm.dbg.value(metadata !{i1 %pReset_read}, i64 0, metadata !33), !dbg !45 ; [debug line = 16:90] [debug variable = pReset]
  %pPredict_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pPredict) nounwind ; [#uses=1 type=i1]
  call void @llvm.dbg.value(metadata !{i1 %pPredict_read}, i64 0, metadata !46), !dbg !47 ; [debug line = 16:69] [debug variable = pPredict]
  %pY_read = call float @_ssdm_op_Read.s_axilite.float(float %pY) nounwind ; [#uses=2 type=float]
  call void @llvm.dbg.value(metadata !{float %pY_read}, i64 0, metadata !48), !dbg !49 ; [debug line = 16:54] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{[2 x float]* %pX}, i64 0, metadata !50), !dbg !54 ; [debug line = 16:35] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !48), !dbg !49 ; [debug line = 16:54] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{i1 %pPredict}, i64 0, metadata !46), !dbg !47 ; [debug line = 16:69] [debug variable = pPredict]
  call void @llvm.dbg.value(metadata !{i1 %pReset}, i64 0, metadata !33), !dbg !45 ; [debug line = 16:90] [debug variable = pReset]
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([2 x float]* %pX, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3) nounwind ; [#uses=0 type=i32]
  call void (...)* @_ssdm_op_SpecInterface([2 x float]* %pX, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 2, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(float %pY, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !55 ; [debug line = 18:1]
  call void (...)* @_ssdm_op_SpecInterface(i1 %pPredict, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !57 ; [debug line = 19:1]
  call void (...)* @_ssdm_op_SpecInterface(i1 %pReset, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !58 ; [debug line = 20:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind, !dbg !59 ; [debug line = 21:1]
  br i1 %pReset_read, label %.preheader.0, label %.loopexit, !dbg !60 ; [debug line = 23:2]

.preheader.0:                                     ; preds = %0
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4, !dbg !61 ; [debug line = 26:2]
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4, !dbg !61 ; [debug line = 26:2]
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !61 ; [debug line = 26:2]
  br label %.loopexit

.loopexit:                                        ; preds = %.preheader.0, %0
  br i1 %pPredict_read, label %.preheader, label %.preheader21, !dbg !65 ; [debug line = 30:2]

.preheader21:                                     ; preds = %1, %.loopexit
  %sum_i = phi float [ %sum_3, %1 ], [ 0.000000e+00, %.loopexit ] ; [#uses=2 type=float]
  %i_i = phi i2 [ %i_1, %1 ], [ 0, %.loopexit ]   ; [#uses=3 type=i2]
  %exitcond_i = icmp eq i2 %i_i, -2, !dbg !66     ; [#uses=1 type=i1] [debug line = 8:28@31:28]
  %empty_4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind ; [#uses=0 type=i32]
  %i_1 = add i2 %i_i, 1, !dbg !74                 ; [#uses=1 type=i2] [debug line = 8:37@31:28]
  br i1 %exitcond_i, label %predict.exit, label %1, !dbg !66 ; [debug line = 8:28@31:28]

; <label>:1                                       ; preds = %.preheader21
  %tmp_2_i = zext i2 %i_i to i64, !dbg !75        ; [#uses=2 type=i64] [debug line = 9:9@31:28]
  %pX_addr_3 = getelementptr [2 x float]* %pX, i64 0, i64 %tmp_2_i, !dbg !75 ; [#uses=1 type=float*] [debug line = 9:9@31:28]
  %pX_load_3 = load float* %pX_addr_3, align 4, !dbg !75 ; [#uses=1 type=float] [debug line = 9:9@31:28]
  %weights_addr_1 = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp_2_i, !dbg !75 ; [#uses=1 type=float*] [debug line = 9:9@31:28]
  %weights_load_5 = load float* %weights_addr_1, align 4, !dbg !75 ; [#uses=1 type=float] [debug line = 9:9@31:28]
  %tmp_3_i = fmul float %pX_load_3, %weights_load_5, !dbg !75 ; [#uses=1 type=float] [debug line = 9:9@31:28]
  %sum_3 = fadd float %sum_i, %tmp_3_i, !dbg !75  ; [#uses=1 type=float] [debug line = 9:9@31:28]
  call void @llvm.dbg.value(metadata !{float %sum_3}, i64 0, metadata !77) nounwind, !dbg !75 ; [debug line = 9:9@31:28] [debug variable = sum]
  call void @llvm.dbg.value(metadata !{i2 %i_1}, i64 0, metadata !78) nounwind, !dbg !74 ; [debug line = 8:37@31:28] [debug variable = i]
  br label %.preheader21, !dbg !74                ; [debug line = 8:37@31:28]

predict.exit:                                     ; preds = %.preheader21
  %weights_load_4 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !80 ; [#uses=2 type=float] [debug line = 11:5@31:28]
  %sum_2 = fadd float %sum_i, %weights_load_4, !dbg !80 ; [#uses=2 type=float] [debug line = 11:5@31:28]
  call void @llvm.dbg.value(metadata !{float %sum_2}, i64 0, metadata !77) nounwind, !dbg !80 ; [debug line = 11:5@31:28] [debug variable = sum]
  %sum_2_to_int = bitcast float %sum_2 to i32     ; [#uses=2 type=i32]
  %tmp_8 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %sum_2_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp_9 = trunc i32 %sum_2_to_int to i23         ; [#uses=1 type=i23]
  %notlhs = icmp ne i8 %tmp_8, -1                 ; [#uses=1 type=i1]
  %notrhs = icmp eq i23 %tmp_9, 0                 ; [#uses=1 type=i1]
  %tmp_10 = or i1 %notrhs, %notlhs                ; [#uses=1 type=i1]
  %tmp_11 = fcmp oge float %sum_2, 0.000000e+00, !dbg !81 ; [#uses=1 type=i1] [debug line = 13:5@31:28]
  %tmp_12 = and i1 %tmp_10, %tmp_11, !dbg !81     ; [#uses=1 type=i1] [debug line = 13:5@31:28]
  %prediction = select i1 %tmp_12, float 1.000000e+00, float -1.000000e+00, !dbg !81 ; [#uses=1 type=float] [debug line = 13:5@31:28]
  call void @llvm.dbg.value(metadata !{float %prediction}, i64 0, metadata !82), !dbg !72 ; [debug line = 31:28] [debug variable = prediction]
  %tmp_1 = fsub float %pY_read, %prediction, !dbg !83 ; [#uses=3 type=float] [debug line = 35:2]
  %pX_addr = getelementptr [2 x float]* %pX, i64 0, i64 0, !dbg !83 ; [#uses=1 type=float*] [debug line = 35:2]
  %pX_load = load float* %pX_addr, align 4, !dbg !83 ; [#uses=1 type=float] [debug line = 35:2]
  %tmp_s = fmul float %tmp_1, %pX_load, !dbg !83  ; [#uses=1 type=float] [debug line = 35:2]
  %weights_load_2 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4, !dbg !83 ; [#uses=1 type=float] [debug line = 35:2]
  %tmp_3 = fadd float %weights_load_2, %tmp_s, !dbg !83 ; [#uses=1 type=float] [debug line = 35:2]
  store float %tmp_3, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4, !dbg !83 ; [debug line = 35:2]
  %pX_addr_1 = getelementptr [2 x float]* %pX, i64 0, i64 1, !dbg !83 ; [#uses=1 type=float*] [debug line = 35:2]
  %pX_load_1 = load float* %pX_addr_1, align 4, !dbg !83 ; [#uses=1 type=float] [debug line = 35:2]
  %tmp_4_1 = fmul float %tmp_1, %pX_load_1, !dbg !83 ; [#uses=1 type=float] [debug line = 35:2]
  %weights_load_3 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4, !dbg !83 ; [#uses=1 type=float] [debug line = 35:2]
  %tmp_5_1 = fadd float %weights_load_3, %tmp_4_1, !dbg !83 ; [#uses=1 type=float] [debug line = 35:2]
  store float %tmp_5_1, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4, !dbg !83 ; [debug line = 35:2]
  %tmp_2 = fadd float %weights_load_4, %tmp_1, !dbg !86 ; [#uses=1 type=float] [debug line = 37:9]
  store float %tmp_2, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !86 ; [debug line = 37:9]
  br label %3, !dbg !87                           ; [debug line = 39:9]

.preheader:                                       ; preds = %2, %.loopexit
  %sum_i5 = phi float [ %sum_1, %2 ], [ 0.000000e+00, %.loopexit ] ; [#uses=2 type=float]
  %i_i6 = phi i2 [ %i, %2 ], [ 0, %.loopexit ]    ; [#uses=3 type=i2]
  %exitcond_i7 = icmp eq i2 %i_i6, -2, !dbg !88   ; [#uses=1 type=i1] [debug line = 8:28@41:16]
  %empty_5 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind ; [#uses=0 type=i32]
  %i = add i2 %i_i6, 1, !dbg !91                  ; [#uses=1 type=i2] [debug line = 8:37@41:16]
  br i1 %exitcond_i7, label %predict.exit20, label %2, !dbg !88 ; [debug line = 8:28@41:16]

; <label>:2                                       ; preds = %.preheader
  %tmp_2_i8 = zext i2 %i_i6 to i64, !dbg !92      ; [#uses=2 type=i64] [debug line = 9:9@41:16]
  %pX_addr_2 = getelementptr [2 x float]* %pX, i64 0, i64 %tmp_2_i8, !dbg !92 ; [#uses=1 type=float*] [debug line = 9:9@41:16]
  %pX_load_2 = load float* %pX_addr_2, align 4, !dbg !92 ; [#uses=1 type=float] [debug line = 9:9@41:16]
  %weights_addr = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp_2_i8, !dbg !92 ; [#uses=1 type=float*] [debug line = 9:9@41:16]
  %weights_load_1 = load float* %weights_addr, align 4, !dbg !92 ; [#uses=1 type=float] [debug line = 9:9@41:16]
  %tmp_3_i1 = fmul float %pX_load_2, %weights_load_1, !dbg !92 ; [#uses=1 type=float] [debug line = 9:9@41:16]
  %sum_1 = fadd float %sum_i5, %tmp_3_i1, !dbg !92 ; [#uses=1 type=float] [debug line = 9:9@41:16]
  call void @llvm.dbg.value(metadata !{float %sum_1}, i64 0, metadata !93) nounwind, !dbg !92 ; [debug line = 9:9@41:16] [debug variable = sum]
  call void @llvm.dbg.value(metadata !{i2 %i}, i64 0, metadata !94) nounwind, !dbg !91 ; [debug line = 8:37@41:16] [debug variable = i]
  br label %.preheader, !dbg !91                  ; [debug line = 8:37@41:16]

predict.exit20:                                   ; preds = %.preheader
  %weights_load = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !95 ; [#uses=1 type=float] [debug line = 11:5@41:16]
  %sum = fadd float %sum_i5, %weights_load, !dbg !95 ; [#uses=2 type=float] [debug line = 11:5@41:16]
  call void @llvm.dbg.value(metadata !{float %sum}, i64 0, metadata !93) nounwind, !dbg !95 ; [debug line = 11:5@41:16] [debug variable = sum]
  %sum_to_int = bitcast float %sum to i32         ; [#uses=2 type=i32]
  %tmp = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %sum_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp_4 = trunc i32 %sum_to_int to i23           ; [#uses=1 type=i23]
  %notlhs4 = icmp ne i8 %tmp, -1                  ; [#uses=1 type=i1]
  %notrhs5 = icmp eq i23 %tmp_4, 0                ; [#uses=1 type=i1]
  %tmp_5 = or i1 %notrhs5, %notlhs4               ; [#uses=1 type=i1]
  %tmp_6 = fcmp oge float %sum, 0.000000e+00, !dbg !96 ; [#uses=1 type=i1] [debug line = 13:5@41:16]
  %tmp_7 = and i1 %tmp_5, %tmp_6, !dbg !96        ; [#uses=1 type=i1] [debug line = 13:5@41:16]
  %tmp_1_i1 = select i1 %tmp_7, float 1.000000e+00, float -1.000000e+00, !dbg !96 ; [#uses=1 type=float] [debug line = 13:5@41:16]
  br label %3, !dbg !89                           ; [debug line = 41:16]

; <label>:3                                       ; preds = %predict.exit20, %predict.exit
  %p_0 = phi float [ %tmp_1_i1, %predict.exit20 ], [ %pY_read, %predict.exit ] ; [#uses=1 type=float]
  ret float %p_0, !dbg !97                        ; [debug line = 43:1]
}

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=1]
define weak i32 @_ssdm_op_SpecMemCore(...) {
entry:
  ret i32 0
}

; [#uses=2]
define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

; [#uses=5]
define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

; [#uses=5]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=2]
define weak i1 @_ssdm_op_Read.s_axilite.i1(i1) {
entry:
  ret i1 %0
}

; [#uses=1]
define weak float @_ssdm_op_Read.s_axilite.float(float) {
entry:
  ret float %0
}

; [#uses=2]
define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2) ; [#uses=1 type=i32]
  %empty_6 = trunc i32 %empty to i8               ; [#uses=1 type=i8]
  ret i8 %empty_6
}

; [#uses=0]
declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

; [#uses=0]
declare i16 @_ssdm_op_HSub(...)

; [#uses=0]
declare i16 @_ssdm_op_HMul(...)

; [#uses=0]
declare i16 @_ssdm_op_HDiv(...)

; [#uses=0]
declare i16 @_ssdm_op_HAdd(...)

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [3 x float]* @weights}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"weights", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 2, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"pX", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 1, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"pY", metadata !17, metadata !"float", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 0, i32 0}
!19 = metadata !{metadata !20}
!20 = metadata !{i32 0, i32 0, metadata !21}
!21 = metadata !{metadata !22}
!22 = metadata !{metadata !"pPredict", metadata !17, metadata !"bool", i32 0, i32 0}
!23 = metadata !{metadata !24}
!24 = metadata !{i32 0, i32 0, metadata !25}
!25 = metadata !{metadata !26}
!26 = metadata !{metadata !"pReset", metadata !17, metadata !"bool", i32 0, i32 0}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 31, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"return", metadata !31, metadata !"float", i32 0, i32 31}
!31 = metadata !{metadata !32}
!32 = metadata !{i32 0, i32 1, i32 0}
!33 = metadata !{i32 786689, metadata !34, metadata !"pReset", metadata !35, i32 67108880, metadata !41, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!34 = metadata !{i32 786478, i32 0, metadata !35, metadata !"dummy_algorithm", metadata !"dummy_algorithm", metadata !"_Z15dummy_algorithmPKffbb", metadata !35, i32 16, metadata !36, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !43, i32 16} ; [ DW_TAG_subprogram ]
!35 = metadata !{i32 786473, metadata !"../../../implementation/hls/DummyAlgorithm/include/DummyAlgorithm.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace", null} ; [ DW_TAG_file_type ]
!36 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !37, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!37 = metadata !{metadata !38, metadata !39, metadata !40, metadata !41, metadata !41}
!38 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!39 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !40} ; [ DW_TAG_pointer_type ]
!40 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !38} ; [ DW_TAG_const_type ]
!41 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !42} ; [ DW_TAG_const_type ]
!42 = metadata !{i32 786468, null, metadata !"bool", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ]
!43 = metadata !{metadata !44}
!44 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!45 = metadata !{i32 16, i32 90, metadata !34, null}
!46 = metadata !{i32 786689, metadata !34, metadata !"pPredict", metadata !35, i32 50331664, metadata !41, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!47 = metadata !{i32 16, i32 69, metadata !34, null}
!48 = metadata !{i32 786689, metadata !34, metadata !"pY", metadata !35, i32 33554448, metadata !40, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!49 = metadata !{i32 16, i32 54, metadata !34, null}
!50 = metadata !{i32 786689, metadata !34, metadata !"pX", null, i32 16, metadata !51, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!51 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !40, metadata !52, i32 0, i32 0} ; [ DW_TAG_array_type ]
!52 = metadata !{metadata !53}
!53 = metadata !{i32 786465, i64 0, i64 1}        ; [ DW_TAG_subrange_type ]
!54 = metadata !{i32 16, i32 35, metadata !34, null}
!55 = metadata !{i32 18, i32 1, metadata !56, null}
!56 = metadata !{i32 786443, metadata !34, i32 16, i32 98, metadata !35, i32 3} ; [ DW_TAG_lexical_block ]
!57 = metadata !{i32 19, i32 1, metadata !56, null}
!58 = metadata !{i32 20, i32 1, metadata !56, null}
!59 = metadata !{i32 21, i32 1, metadata !56, null}
!60 = metadata !{i32 23, i32 2, metadata !56, null}
!61 = metadata !{i32 26, i32 2, metadata !62, null}
!62 = metadata !{i32 786443, metadata !63, i32 24, i32 54, metadata !35, i32 6} ; [ DW_TAG_lexical_block ]
!63 = metadata !{i32 786443, metadata !64, i32 24, i32 16, metadata !35, i32 5} ; [ DW_TAG_lexical_block ]
!64 = metadata !{i32 786443, metadata !56, i32 23, i32 14, metadata !35, i32 4} ; [ DW_TAG_lexical_block ]
!65 = metadata !{i32 30, i32 2, metadata !56, null}
!66 = metadata !{i32 8, i32 28, metadata !67, metadata !72}
!67 = metadata !{i32 786443, metadata !68, i32 8, i32 5, metadata !35, i32 1} ; [ DW_TAG_lexical_block ]
!68 = metadata !{i32 786443, metadata !69, i32 6, i32 34, metadata !35, i32 0} ; [ DW_TAG_lexical_block ]
!69 = metadata !{i32 786478, i32 0, metadata !35, metadata !"predict", metadata !"predict", metadata !"_Z7predictPKf", metadata !35, i32 6, metadata !70, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !43, i32 6} ; [ DW_TAG_subprogram ]
!70 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !71, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!71 = metadata !{metadata !38, metadata !39}
!72 = metadata !{i32 31, i32 28, metadata !73, null}
!73 = metadata !{i32 786443, metadata !56, i32 30, i32 17, metadata !35, i32 7} ; [ DW_TAG_lexical_block ]
!74 = metadata !{i32 8, i32 37, metadata !67, metadata !72}
!75 = metadata !{i32 9, i32 9, metadata !76, metadata !72}
!76 = metadata !{i32 786443, metadata !67, i32 8, i32 42, metadata !35, i32 2} ; [ DW_TAG_lexical_block ]
!77 = metadata !{i32 786688, metadata !68, metadata !"sum", metadata !35, i32 7, metadata !38, i32 0, metadata !72} ; [ DW_TAG_auto_variable ]
!78 = metadata !{i32 786688, metadata !67, metadata !"i", metadata !35, i32 8, metadata !79, i32 0, metadata !72} ; [ DW_TAG_auto_variable ]
!79 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!80 = metadata !{i32 11, i32 5, metadata !68, metadata !72}
!81 = metadata !{i32 13, i32 5, metadata !68, metadata !72}
!82 = metadata !{i32 786688, metadata !73, metadata !"prediction", metadata !35, i32 31, metadata !38, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!83 = metadata !{i32 35, i32 2, metadata !84, null}
!84 = metadata !{i32 786443, metadata !85, i32 33, i32 61, metadata !35, i32 9} ; [ DW_TAG_lexical_block ]
!85 = metadata !{i32 786443, metadata !73, i32 33, i32 24, metadata !35, i32 8} ; [ DW_TAG_lexical_block ]
!86 = metadata !{i32 37, i32 9, metadata !73, null}
!87 = metadata !{i32 39, i32 9, metadata !73, null}
!88 = metadata !{i32 8, i32 28, metadata !67, metadata !89}
!89 = metadata !{i32 41, i32 16, metadata !90, null}
!90 = metadata !{i32 786443, metadata !56, i32 40, i32 9, metadata !35, i32 10} ; [ DW_TAG_lexical_block ]
!91 = metadata !{i32 8, i32 37, metadata !67, metadata !89}
!92 = metadata !{i32 9, i32 9, metadata !76, metadata !89}
!93 = metadata !{i32 786688, metadata !68, metadata !"sum", metadata !35, i32 7, metadata !38, i32 0, metadata !89} ; [ DW_TAG_auto_variable ]
!94 = metadata !{i32 786688, metadata !67, metadata !"i", metadata !35, i32 8, metadata !79, i32 0, metadata !89} ; [ DW_TAG_auto_variable ]
!95 = metadata !{i32 11, i32 5, metadata !68, metadata !89}
!96 = metadata !{i32 13, i32 5, metadata !68, metadata !89}
!97 = metadata !{i32 43, i32 1, metadata !56, null}
