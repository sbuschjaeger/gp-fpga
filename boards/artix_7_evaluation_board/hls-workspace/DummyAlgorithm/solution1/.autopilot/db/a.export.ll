; ModuleID = '/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace/DummyAlgorithm/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@weights = global [3 x float] zeroinitializer, align 4
@dummy_algorithm_str = internal unnamed_addr constant [16 x i8] c"dummy_algorithm\00"
@RAM_1P_str = internal unnamed_addr constant [7 x i8] c"RAM_1P\00"
@p_str3 = internal unnamed_addr constant [1 x i8] zeroinitializer
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define float @dummy_algorithm([2 x float]* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pReset) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([2 x float]* %pX) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(float %pY) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pPredict) nounwind, !map !19
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pReset) nounwind, !map !23
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !27
  call void (...)* @_ssdm_op_SpecTopModule([16 x i8]* @dummy_algorithm_str) nounwind
  %pReset_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pReset) nounwind
  %pPredict_read = call i1 @_ssdm_op_Read.s_axilite.i1(i1 %pPredict) nounwind
  %pY_read = call float @_ssdm_op_Read.s_axilite.float(float %pY) nounwind
  %empty = call i32 (...)* @_ssdm_op_SpecMemCore([2 x float]* %pX, [1 x i8]* @p_str3, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str3, i32 -1, [1 x i8]* @p_str3, [1 x i8]* @p_str3, [1 x i8]* @p_str3) nounwind
  call void (...)* @_ssdm_op_SpecInterface([2 x float]* %pX, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 2, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(float %pY, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i1 %pPredict, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i1 %pReset, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  br i1 %pReset_read, label %.preheader.0, label %.loopexit

.preheader.0:                                     ; preds = %0
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4
  br label %.loopexit

.loopexit:                                        ; preds = %.preheader.0, %0
  br i1 %pPredict_read, label %.preheader, label %.preheader21

.preheader21:                                     ; preds = %.loopexit, %1
  %sum_i = phi float [ %sum_3, %1 ], [ 0.000000e+00, %.loopexit ]
  %i_i = phi i2 [ %i_1, %1 ], [ 0, %.loopexit ]
  %exitcond_i = icmp eq i2 %i_i, -2
  %empty_4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind
  %i_1 = add i2 %i_i, 1
  br i1 %exitcond_i, label %predict.exit, label %1

; <label>:1                                       ; preds = %.preheader21
  %tmp_2_i = zext i2 %i_i to i64
  %pX_addr_3 = getelementptr [2 x float]* %pX, i64 0, i64 %tmp_2_i
  %pX_load_3 = load float* %pX_addr_3, align 4
  %weights_addr_1 = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp_2_i
  %weights_load_5 = load float* %weights_addr_1, align 4
  %tmp_3_i = fmul float %pX_load_3, %weights_load_5
  %sum_3 = fadd float %sum_i, %tmp_3_i
  br label %.preheader21

predict.exit:                                     ; preds = %.preheader21
  %weights_load_4 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4
  %sum_2 = fadd float %sum_i, %weights_load_4
  %sum_2_to_int = bitcast float %sum_2 to i32
  %tmp_8 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %sum_2_to_int, i32 23, i32 30)
  %tmp_9 = trunc i32 %sum_2_to_int to i23
  %notlhs = icmp ne i8 %tmp_8, -1
  %notrhs = icmp eq i23 %tmp_9, 0
  %tmp_10 = or i1 %notrhs, %notlhs
  %tmp_11 = fcmp oge float %sum_2, 0.000000e+00
  %tmp_12 = and i1 %tmp_10, %tmp_11
  %prediction = select i1 %tmp_12, float 1.000000e+00, float -1.000000e+00
  %tmp_1 = fsub float %pY_read, %prediction
  %pX_addr = getelementptr [2 x float]* %pX, i64 0, i64 0
  %pX_load = load float* %pX_addr, align 4
  %tmp_s = fmul float %tmp_1, %pX_load
  %weights_load_2 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4
  %tmp_3 = fadd float %weights_load_2, %tmp_s
  store float %tmp_3, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4
  %pX_addr_1 = getelementptr [2 x float]* %pX, i64 0, i64 1
  %pX_load_1 = load float* %pX_addr_1, align 4
  %tmp_4_1 = fmul float %tmp_1, %pX_load_1
  %weights_load_3 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4
  %tmp_5_1 = fadd float %weights_load_3, %tmp_4_1
  store float %tmp_5_1, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4
  %tmp_2 = fadd float %weights_load_4, %tmp_1
  store float %tmp_2, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4
  br label %3

.preheader:                                       ; preds = %.loopexit, %2
  %sum_i5 = phi float [ %sum_1, %2 ], [ 0.000000e+00, %.loopexit ]
  %i_i6 = phi i2 [ %i, %2 ], [ 0, %.loopexit ]
  %exitcond_i7 = icmp eq i2 %i_i6, -2
  %empty_5 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind
  %i = add i2 %i_i6, 1
  br i1 %exitcond_i7, label %predict.exit20, label %2

; <label>:2                                       ; preds = %.preheader
  %tmp_2_i8 = zext i2 %i_i6 to i64
  %pX_addr_2 = getelementptr [2 x float]* %pX, i64 0, i64 %tmp_2_i8
  %pX_load_2 = load float* %pX_addr_2, align 4
  %weights_addr = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp_2_i8
  %weights_load_1 = load float* %weights_addr, align 4
  %tmp_3_i1 = fmul float %pX_load_2, %weights_load_1
  %sum_1 = fadd float %sum_i5, %tmp_3_i1
  br label %.preheader

predict.exit20:                                   ; preds = %.preheader
  %weights_load = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4
  %sum = fadd float %sum_i5, %weights_load
  %sum_to_int = bitcast float %sum to i32
  %tmp = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %sum_to_int, i32 23, i32 30)
  %tmp_4 = trunc i32 %sum_to_int to i23
  %notlhs4 = icmp ne i8 %tmp, -1
  %notrhs5 = icmp eq i23 %tmp_4, 0
  %tmp_5 = or i1 %notrhs5, %notlhs4
  %tmp_6 = fcmp oge float %sum, 0.000000e+00
  %tmp_7 = and i1 %tmp_5, %tmp_6
  %tmp_1_i1 = select i1 %tmp_7, float 1.000000e+00, float -1.000000e+00
  br label %3

; <label>:3                                       ; preds = %predict.exit20, %predict.exit
  %p_0 = phi float [ %tmp_1_i1, %predict.exit20 ], [ %pY_read, %predict.exit ]
  ret float %p_0
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecMemCore(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i1 @_ssdm_op_Read.s_axilite.i1(i1) {
entry:
  ret i1 %0
}

define weak float @_ssdm_op_Read.s_axilite.float(float) {
entry:
  ret float %0
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_6 = trunc i32 %empty to i8
  ret i8 %empty_6
}

declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [3 x float]* @weights}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"weights", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 2, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"pX", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 1, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"pY", metadata !17, metadata !"float", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 0, i32 0}
!19 = metadata !{metadata !20}
!20 = metadata !{i32 0, i32 0, metadata !21}
!21 = metadata !{metadata !22}
!22 = metadata !{metadata !"pPredict", metadata !17, metadata !"bool", i32 0, i32 0}
!23 = metadata !{metadata !24}
!24 = metadata !{i32 0, i32 0, metadata !25}
!25 = metadata !{metadata !26}
!26 = metadata !{metadata !"pReset", metadata !17, metadata !"bool", i32 0, i32 0}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 31, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"return", metadata !31, metadata !"float", i32 0, i32 31}
!31 = metadata !{metadata !32}
!32 = metadata !{i32 0, i32 1, i32 0}
