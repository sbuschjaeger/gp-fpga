; ModuleID = '/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace/DummyAlgorithm/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@weights = global [3 x float] zeroinitializer, align 4 ; [#uses=5 type=[3 x float]*]
@dummy_algorithm.str = internal unnamed_addr constant [16 x i8] c"dummy_algorithm\00" ; [#uses=1 type=[16 x i8]*]
@.str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=20 type=[1 x i8]*]
@.str = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1 ; [#uses=5 type=[10 x i8]*]

; [#uses=11]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=0]
define float @dummy_algorithm([2 x float]* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pReset) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([2 x float]* %pX) nounwind, !map !19
  call void (...)* @_ssdm_op_SpecBitsMap(float %pY) nounwind, !map !25
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pPredict) nounwind, !map !31
  call void (...)* @_ssdm_op_SpecBitsMap(i1 %pReset) nounwind, !map !35
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !39
  call void (...)* @_ssdm_op_SpecTopModule([16 x i8]* @dummy_algorithm.str) nounwind
  call void @llvm.dbg.value(metadata !{[2 x float]* %pX}, i64 0, metadata !45), !dbg !58 ; [debug line = 16:35] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !59), !dbg !60 ; [debug line = 16:54] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{i1 %pPredict}, i64 0, metadata !61), !dbg !62 ; [debug line = 16:69] [debug variable = pPredict]
  call void @llvm.dbg.value(metadata !{i1 %pReset}, i64 0, metadata !63), !dbg !64 ; [debug line = 16:90] [debug variable = pReset]
  call void (...)* @_ssdm_op_SpecInterface([2 x float]* %pX, [10 x i8]* @.str, i32 0, i32 0, i32 0, i32 2, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(float %pY, [10 x i8]* @.str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !65 ; [debug line = 18:1]
  call void (...)* @_ssdm_op_SpecInterface(i1 %pPredict, [10 x i8]* @.str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !67 ; [debug line = 19:1]
  call void (...)* @_ssdm_op_SpecInterface(i1 %pReset, [10 x i8]* @.str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !68 ; [debug line = 20:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @.str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1, [1 x i8]* @.str1) nounwind, !dbg !69 ; [debug line = 21:1]
  br i1 %pReset, label %.preheader.0, label %.loopexit, !dbg !70 ; [debug line = 23:2]

.preheader.0:                                     ; preds = %0
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4, !dbg !71 ; [debug line = 26:2]
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4, !dbg !71 ; [debug line = 26:2]
  store float 0.000000e+00, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !71 ; [debug line = 26:2]
  br label %.loopexit

.loopexit:                                        ; preds = %.preheader.0, %0
  br i1 %pPredict, label %.preheader, label %.preheader21, !dbg !75 ; [debug line = 30:2]

.preheader21:                                     ; preds = %2, %.loopexit
  %sum.i = phi float [ %sum.3, %2 ], [ 0.000000e+00, %.loopexit ] ; [#uses=2 type=float]
  %i.i = phi i2 [ %i.1, %2 ], [ 0, %.loopexit ]   ; [#uses=3 type=i2]
  %exitcond.i = icmp eq i2 %i.i, -2, !dbg !76     ; [#uses=1 type=i1] [debug line = 8:28@31:28]
  %1 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond.i, label %predict.exit, label %2, !dbg !76 ; [debug line = 8:28@31:28]

; <label>:2                                       ; preds = %.preheader21
  %tmp.2.i = zext i2 %i.i to i64, !dbg !84        ; [#uses=2 type=i64] [debug line = 9:9@31:28]
  %pX.addr.3 = getelementptr [2 x float]* %pX, i64 0, i64 %tmp.2.i, !dbg !84 ; [#uses=1 type=float*] [debug line = 9:9@31:28]
  %pX.load.3 = load float* %pX.addr.3, align 4, !dbg !84 ; [#uses=1 type=float] [debug line = 9:9@31:28]
  %weights.addr.1 = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp.2.i, !dbg !84 ; [#uses=1 type=float*] [debug line = 9:9@31:28]
  %weights.load.5 = load float* %weights.addr.1, align 4, !dbg !84 ; [#uses=1 type=float] [debug line = 9:9@31:28]
  %tmp.3.i = fmul float %pX.load.3, %weights.load.5, !dbg !84 ; [#uses=1 type=float] [debug line = 9:9@31:28]
  %sum.3 = fadd float %sum.i, %tmp.3.i, !dbg !84  ; [#uses=1 type=float] [debug line = 9:9@31:28]
  call void @llvm.dbg.value(metadata !{float %sum.3}, i64 0, metadata !86) nounwind, !dbg !84 ; [debug line = 9:9@31:28] [debug variable = sum]
  %i.1 = add i2 %i.i, 1, !dbg !87                 ; [#uses=1 type=i2] [debug line = 8:37@31:28]
  call void @llvm.dbg.value(metadata !{i2 %i.1}, i64 0, metadata !88) nounwind, !dbg !87 ; [debug line = 8:37@31:28] [debug variable = i]
  br label %.preheader21, !dbg !87                ; [debug line = 8:37@31:28]

predict.exit:                                     ; preds = %.preheader21
  %sum.i.lcssa = phi float [ %sum.i, %.preheader21 ] ; [#uses=1 type=float]
  %weights.load.4 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !90 ; [#uses=2 type=float] [debug line = 11:5@31:28]
  %sum.2 = fadd float %sum.i.lcssa, %weights.load.4, !dbg !90 ; [#uses=2 type=float] [debug line = 11:5@31:28]
  call void @llvm.dbg.value(metadata !{float %sum.2}, i64 0, metadata !86) nounwind, !dbg !90 ; [debug line = 11:5@31:28] [debug variable = sum]
  %sum.2_to_int = bitcast float %sum.2 to i32     ; [#uses=2 type=i32]
  %tmp.8 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %sum.2_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp.9 = trunc i32 %sum.2_to_int to i23         ; [#uses=1 type=i23]
  %notlhs = icmp ne i8 %tmp.8, -1                 ; [#uses=1 type=i1]
  %notrhs = icmp eq i23 %tmp.9, 0                 ; [#uses=1 type=i1]
  %tmp.10 = or i1 %notrhs, %notlhs                ; [#uses=1 type=i1]
  %tmp.11 = fcmp oge float %sum.2, 0.000000e+00, !dbg !91 ; [#uses=1 type=i1] [debug line = 13:5@31:28]
  %tmp.12 = and i1 %tmp.10, %tmp.11, !dbg !91     ; [#uses=1 type=i1] [debug line = 13:5@31:28]
  %prediction = select i1 %tmp.12, float 1.000000e+00, float -1.000000e+00, !dbg !91 ; [#uses=1 type=float] [debug line = 13:5@31:28]
  call void @llvm.dbg.value(metadata !{float %prediction}, i64 0, metadata !92), !dbg !82 ; [debug line = 31:28] [debug variable = prediction]
  %tmp.1 = fsub float %pY, %prediction, !dbg !93  ; [#uses=3 type=float] [debug line = 35:2]
  %pX.addr = getelementptr [2 x float]* %pX, i64 0, i64 0, !dbg !93 ; [#uses=1 type=float*] [debug line = 35:2]
  %pX.load = load float* %pX.addr, align 4, !dbg !93 ; [#uses=1 type=float] [debug line = 35:2]
  %tmp. = fmul float %tmp.1, %pX.load, !dbg !93   ; [#uses=1 type=float] [debug line = 35:2]
  %weights.load.2 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4, !dbg !93 ; [#uses=1 type=float] [debug line = 35:2]
  %tmp.3 = fadd float %weights.load.2, %tmp., !dbg !93 ; [#uses=1 type=float] [debug line = 35:2]
  store float %tmp.3, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 0), align 4, !dbg !93 ; [debug line = 35:2]
  %pX.addr.1 = getelementptr [2 x float]* %pX, i64 0, i64 1, !dbg !93 ; [#uses=1 type=float*] [debug line = 35:2]
  %pX.load.1 = load float* %pX.addr.1, align 4, !dbg !93 ; [#uses=1 type=float] [debug line = 35:2]
  %tmp.4.1 = fmul float %tmp.1, %pX.load.1, !dbg !93 ; [#uses=1 type=float] [debug line = 35:2]
  %weights.load.3 = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4, !dbg !93 ; [#uses=1 type=float] [debug line = 35:2]
  %tmp.5.1 = fadd float %weights.load.3, %tmp.4.1, !dbg !93 ; [#uses=1 type=float] [debug line = 35:2]
  store float %tmp.5.1, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 1), align 4, !dbg !93 ; [debug line = 35:2]
  %tmp.2 = fadd float %weights.load.4, %tmp.1, !dbg !96 ; [#uses=1 type=float] [debug line = 37:9]
  store float %tmp.2, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !96 ; [debug line = 37:9]
  br label %5, !dbg !97                           ; [debug line = 39:9]

.preheader:                                       ; preds = %4, %.loopexit
  %sum.i5 = phi float [ %sum.1, %4 ], [ 0.000000e+00, %.loopexit ] ; [#uses=2 type=float]
  %i.i6 = phi i2 [ %i, %4 ], [ 0, %.loopexit ]    ; [#uses=3 type=i2]
  %exitcond.i7 = icmp eq i2 %i.i6, -2, !dbg !98   ; [#uses=1 type=i1] [debug line = 8:28@41:16]
  %3 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond.i7, label %predict.exit20, label %4, !dbg !98 ; [debug line = 8:28@41:16]

; <label>:4                                       ; preds = %.preheader
  %tmp.2.i8 = zext i2 %i.i6 to i64, !dbg !101     ; [#uses=2 type=i64] [debug line = 9:9@41:16]
  %pX.addr.2 = getelementptr [2 x float]* %pX, i64 0, i64 %tmp.2.i8, !dbg !101 ; [#uses=1 type=float*] [debug line = 9:9@41:16]
  %pX.load.2 = load float* %pX.addr.2, align 4, !dbg !101 ; [#uses=1 type=float] [debug line = 9:9@41:16]
  %weights.addr = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp.2.i8, !dbg !101 ; [#uses=1 type=float*] [debug line = 9:9@41:16]
  %weights.load.1 = load float* %weights.addr, align 4, !dbg !101 ; [#uses=1 type=float] [debug line = 9:9@41:16]
  %tmp.3.i1 = fmul float %pX.load.2, %weights.load.1, !dbg !101 ; [#uses=1 type=float] [debug line = 9:9@41:16]
  %sum.1 = fadd float %sum.i5, %tmp.3.i1, !dbg !101 ; [#uses=1 type=float] [debug line = 9:9@41:16]
  call void @llvm.dbg.value(metadata !{float %sum.1}, i64 0, metadata !102) nounwind, !dbg !101 ; [debug line = 9:9@41:16] [debug variable = sum]
  %i = add i2 %i.i6, 1, !dbg !103                 ; [#uses=1 type=i2] [debug line = 8:37@41:16]
  call void @llvm.dbg.value(metadata !{i2 %i}, i64 0, metadata !104) nounwind, !dbg !103 ; [debug line = 8:37@41:16] [debug variable = i]
  br label %.preheader, !dbg !103                 ; [debug line = 8:37@41:16]

predict.exit20:                                   ; preds = %.preheader
  %sum.i5.lcssa = phi float [ %sum.i5, %.preheader ] ; [#uses=1 type=float]
  %weights.load = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !105 ; [#uses=1 type=float] [debug line = 11:5@41:16]
  %sum = fadd float %sum.i5.lcssa, %weights.load, !dbg !105 ; [#uses=2 type=float] [debug line = 11:5@41:16]
  call void @llvm.dbg.value(metadata !{float %sum}, i64 0, metadata !102) nounwind, !dbg !105 ; [debug line = 11:5@41:16] [debug variable = sum]
  %sum_to_int = bitcast float %sum to i32         ; [#uses=2 type=i32]
  %tmp = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %sum_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp.4 = trunc i32 %sum_to_int to i23           ; [#uses=1 type=i23]
  %notlhs4 = icmp ne i8 %tmp, -1                  ; [#uses=1 type=i1]
  %notrhs5 = icmp eq i23 %tmp.4, 0                ; [#uses=1 type=i1]
  %tmp.5 = or i1 %notrhs5, %notlhs4               ; [#uses=1 type=i1]
  %tmp.6 = fcmp oge float %sum, 0.000000e+00, !dbg !106 ; [#uses=1 type=i1] [debug line = 13:5@41:16]
  %tmp.7 = and i1 %tmp.5, %tmp.6, !dbg !106       ; [#uses=1 type=i1] [debug line = 13:5@41:16]
  %tmp.1.i1 = select i1 %tmp.7, float 1.000000e+00, float -1.000000e+00, !dbg !106 ; [#uses=1 type=float] [debug line = 13:5@41:16]
  br label %5, !dbg !99                           ; [debug line = 41:16]

; <label>:5                                       ; preds = %predict.exit20, %predict.exit
  %.0 = phi float [ %tmp.1.i1, %predict.exit20 ], [ %pY, %predict.exit ] ; [#uses=1 type=float]
  ret float %.0, !dbg !107                        ; [debug line = 43:1]
}

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=2]
declare i32 @_ssdm_op_SpecLoopTripCount(...)

; [#uses=5]
declare void @_ssdm_op_SpecInterface(...) nounwind

; [#uses=5]
declare void @_ssdm_op_SpecBitsMap(...)

; [#uses=2]
declare i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}
!llvm.dbg.cu = !{!7}

!0 = metadata !{metadata !1, [3 x float]* @weights}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"weights", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 2, i32 1}
!7 = metadata !{i32 786449, i32 0, i32 4, metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace/DummyAlgorithm/solution1/.autopilot/db/DummyAlgorithm.pragma.2.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, null, null, null, metadata !8} ; [ DW_TAG_compile_unit ]
!8 = metadata !{metadata !9}
!9 = metadata !{metadata !10, metadata !16}
!10 = metadata !{i32 786484, i32 0, null, metadata !"weights", metadata !"weights", metadata !"", metadata !11, i32 4, metadata !12, i32 0, i32 1, [3 x float]* @weights} ; [ DW_TAG_variable ]
!11 = metadata !{i32 786473, metadata !"../../../implementation/hls/DummyAlgorithm/include/DummyAlgorithm.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace", null} ; [ DW_TAG_file_type ]
!12 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 96, i64 32, i32 0, i32 0, metadata !13, metadata !14, i32 0, i32 0} ; [ DW_TAG_array_type ]
!13 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!14 = metadata !{metadata !15}
!15 = metadata !{i32 786465, i64 0, i64 2}        ; [ DW_TAG_subrange_type ]
!16 = metadata !{i32 786484, i32 0, null, metadata !"signgam", metadata !"signgam", metadata !"", metadata !17, i32 149, metadata !18, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!17 = metadata !{i32 786473, metadata !"/usr/include/math.h", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace", null} ; [ DW_TAG_file_type ]
!18 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!19 = metadata !{metadata !20}
!20 = metadata !{i32 0, i32 31, metadata !21}
!21 = metadata !{metadata !22}
!22 = metadata !{metadata !"pX", metadata !23, metadata !"float", i32 0, i32 31}
!23 = metadata !{metadata !24}
!24 = metadata !{i32 0, i32 1, i32 1}
!25 = metadata !{metadata !26}
!26 = metadata !{i32 0, i32 31, metadata !27}
!27 = metadata !{metadata !28}
!28 = metadata !{metadata !"pY", metadata !29, metadata !"float", i32 0, i32 31}
!29 = metadata !{metadata !30}
!30 = metadata !{i32 0, i32 0, i32 0}
!31 = metadata !{metadata !32}
!32 = metadata !{i32 0, i32 0, metadata !33}
!33 = metadata !{metadata !34}
!34 = metadata !{metadata !"pPredict", metadata !29, metadata !"bool", i32 0, i32 0}
!35 = metadata !{metadata !36}
!36 = metadata !{i32 0, i32 0, metadata !37}
!37 = metadata !{metadata !38}
!38 = metadata !{metadata !"pReset", metadata !29, metadata !"bool", i32 0, i32 0}
!39 = metadata !{metadata !40}
!40 = metadata !{i32 0, i32 31, metadata !41}
!41 = metadata !{metadata !42}
!42 = metadata !{metadata !"return", metadata !43, metadata !"float", i32 0, i32 31}
!43 = metadata !{metadata !44}
!44 = metadata !{i32 0, i32 1, i32 0}
!45 = metadata !{i32 786689, metadata !46, metadata !"pX", null, i32 16, metadata !55, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!46 = metadata !{i32 786478, i32 0, metadata !11, metadata !"dummy_algorithm", metadata !"dummy_algorithm", metadata !"_Z15dummy_algorithmPKffbb", metadata !11, i32 16, metadata !47, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !53, i32 16} ; [ DW_TAG_subprogram ]
!47 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !48, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!48 = metadata !{metadata !13, metadata !49, metadata !50, metadata !51, metadata !51}
!49 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !50} ; [ DW_TAG_pointer_type ]
!50 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !13} ; [ DW_TAG_const_type ]
!51 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !52} ; [ DW_TAG_const_type ]
!52 = metadata !{i32 786468, null, metadata !"bool", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ]
!53 = metadata !{metadata !54}
!54 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!55 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !50, metadata !56, i32 0, i32 0} ; [ DW_TAG_array_type ]
!56 = metadata !{metadata !57}
!57 = metadata !{i32 786465, i64 0, i64 1}        ; [ DW_TAG_subrange_type ]
!58 = metadata !{i32 16, i32 35, metadata !46, null}
!59 = metadata !{i32 786689, metadata !46, metadata !"pY", metadata !11, i32 33554448, metadata !50, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!60 = metadata !{i32 16, i32 54, metadata !46, null}
!61 = metadata !{i32 786689, metadata !46, metadata !"pPredict", metadata !11, i32 50331664, metadata !51, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!62 = metadata !{i32 16, i32 69, metadata !46, null}
!63 = metadata !{i32 786689, metadata !46, metadata !"pReset", metadata !11, i32 67108880, metadata !51, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!64 = metadata !{i32 16, i32 90, metadata !46, null}
!65 = metadata !{i32 18, i32 1, metadata !66, null}
!66 = metadata !{i32 786443, metadata !46, i32 16, i32 98, metadata !11, i32 3} ; [ DW_TAG_lexical_block ]
!67 = metadata !{i32 19, i32 1, metadata !66, null}
!68 = metadata !{i32 20, i32 1, metadata !66, null}
!69 = metadata !{i32 21, i32 1, metadata !66, null}
!70 = metadata !{i32 23, i32 2, metadata !66, null}
!71 = metadata !{i32 26, i32 2, metadata !72, null}
!72 = metadata !{i32 786443, metadata !73, i32 24, i32 54, metadata !11, i32 6} ; [ DW_TAG_lexical_block ]
!73 = metadata !{i32 786443, metadata !74, i32 24, i32 16, metadata !11, i32 5} ; [ DW_TAG_lexical_block ]
!74 = metadata !{i32 786443, metadata !66, i32 23, i32 14, metadata !11, i32 4} ; [ DW_TAG_lexical_block ]
!75 = metadata !{i32 30, i32 2, metadata !66, null}
!76 = metadata !{i32 8, i32 28, metadata !77, metadata !82}
!77 = metadata !{i32 786443, metadata !78, i32 8, i32 5, metadata !11, i32 1} ; [ DW_TAG_lexical_block ]
!78 = metadata !{i32 786443, metadata !79, i32 6, i32 34, metadata !11, i32 0} ; [ DW_TAG_lexical_block ]
!79 = metadata !{i32 786478, i32 0, metadata !11, metadata !"predict", metadata !"predict", metadata !"_Z7predictPKf", metadata !11, i32 6, metadata !80, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !53, i32 6} ; [ DW_TAG_subprogram ]
!80 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !81, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!81 = metadata !{metadata !13, metadata !49}
!82 = metadata !{i32 31, i32 28, metadata !83, null}
!83 = metadata !{i32 786443, metadata !66, i32 30, i32 17, metadata !11, i32 7} ; [ DW_TAG_lexical_block ]
!84 = metadata !{i32 9, i32 9, metadata !85, metadata !82}
!85 = metadata !{i32 786443, metadata !77, i32 8, i32 42, metadata !11, i32 2} ; [ DW_TAG_lexical_block ]
!86 = metadata !{i32 786688, metadata !78, metadata !"sum", metadata !11, i32 7, metadata !13, i32 0, metadata !82} ; [ DW_TAG_auto_variable ]
!87 = metadata !{i32 8, i32 37, metadata !77, metadata !82}
!88 = metadata !{i32 786688, metadata !77, metadata !"i", metadata !11, i32 8, metadata !89, i32 0, metadata !82} ; [ DW_TAG_auto_variable ]
!89 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!90 = metadata !{i32 11, i32 5, metadata !78, metadata !82}
!91 = metadata !{i32 13, i32 5, metadata !78, metadata !82}
!92 = metadata !{i32 786688, metadata !83, metadata !"prediction", metadata !11, i32 31, metadata !13, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!93 = metadata !{i32 35, i32 2, metadata !94, null}
!94 = metadata !{i32 786443, metadata !95, i32 33, i32 61, metadata !11, i32 9} ; [ DW_TAG_lexical_block ]
!95 = metadata !{i32 786443, metadata !83, i32 33, i32 24, metadata !11, i32 8} ; [ DW_TAG_lexical_block ]
!96 = metadata !{i32 37, i32 9, metadata !83, null}
!97 = metadata !{i32 39, i32 9, metadata !83, null}
!98 = metadata !{i32 8, i32 28, metadata !77, metadata !99}
!99 = metadata !{i32 41, i32 16, metadata !100, null}
!100 = metadata !{i32 786443, metadata !66, i32 40, i32 9, metadata !11, i32 10} ; [ DW_TAG_lexical_block ]
!101 = metadata !{i32 9, i32 9, metadata !85, metadata !99}
!102 = metadata !{i32 786688, metadata !78, metadata !"sum", metadata !11, i32 7, metadata !13, i32 0, metadata !99} ; [ DW_TAG_auto_variable ]
!103 = metadata !{i32 8, i32 37, metadata !77, metadata !99}
!104 = metadata !{i32 786688, metadata !77, metadata !"i", metadata !11, i32 8, metadata !89, i32 0, metadata !99} ; [ DW_TAG_auto_variable ]
!105 = metadata !{i32 11, i32 5, metadata !78, metadata !99}
!106 = metadata !{i32 13, i32 5, metadata !78, metadata !99}
!107 = metadata !{i32 43, i32 1, metadata !66, null}
