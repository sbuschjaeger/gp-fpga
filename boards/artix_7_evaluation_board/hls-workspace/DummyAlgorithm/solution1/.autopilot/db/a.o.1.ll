; ModuleID = '/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace/DummyAlgorithm/solution1/.autopilot/db/a.g.1.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@weights = global [3 x float] zeroinitializer, align 4 ; [#uses=4 type=[3 x float]*]
@dummy_algorithm.str = internal unnamed_addr constant [16 x i8] c"dummy_algorithm\00" ; [#uses=1 type=[16 x i8]*]
@.str3 = private unnamed_addr constant [15 x i8] c"UPDATE_WEIGHTS\00", align 1 ; [#uses=1 type=[15 x i8]*]
@.str2 = private unnamed_addr constant [6 x i8] c"RESET\00", align 1 ; [#uses=1 type=[6 x i8]*]
@.str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=1 type=[1 x i8]*]
@.str = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1 ; [#uses=1 type=[10 x i8]*]

; [#uses=2]
define internal fastcc float @predict(float* %pX) nounwind uwtable {
  call void @llvm.dbg.value(metadata !{float* %pX}, i64 0, metadata !28), !dbg !29 ; [debug line = 6:27] [debug variable = pX]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX, i32 2) nounwind, !dbg !30 ; [debug line = 6:35]
  br label %1, !dbg !32                           ; [debug line = 8:28]

; <label>:1                                       ; preds = %2, %0
  %sum = phi float [ 0.000000e+00, %0 ], [ %sum.2, %2 ] ; [#uses=2 type=float]
  %i = phi i32 [ 0, %0 ], [ %i.1, %2 ]            ; [#uses=3 type=i32]
  %exitcond = icmp eq i32 %i, 2, !dbg !32         ; [#uses=1 type=i1] [debug line = 8:28]
  br i1 %exitcond, label %3, label %2, !dbg !32   ; [debug line = 8:28]

; <label>:2                                       ; preds = %1
  %tmp.2 = zext i32 %i to i64, !dbg !34           ; [#uses=2 type=i64] [debug line = 9:9]
  %pX.addr = getelementptr inbounds float* %pX, i64 %tmp.2, !dbg !34 ; [#uses=1 type=float*] [debug line = 9:9]
  %pX.load = load float* %pX.addr, align 4, !dbg !34 ; [#uses=2 type=float] [debug line = 9:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pX.load) nounwind
  %weights.addr = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp.2, !dbg !34 ; [#uses=1 type=float*] [debug line = 9:9]
  %weights.load.1 = load float* %weights.addr, align 4, !dbg !34 ; [#uses=2 type=float] [debug line = 9:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %weights.load.1) nounwind
  %tmp.3 = fmul float %pX.load, %weights.load.1, !dbg !34 ; [#uses=1 type=float] [debug line = 9:9]
  %sum.2 = fadd float %sum, %tmp.3, !dbg !34      ; [#uses=1 type=float] [debug line = 9:9]
  call void @llvm.dbg.value(metadata !{float %sum.2}, i64 0, metadata !36), !dbg !34 ; [debug line = 9:9] [debug variable = sum]
  %i.1 = add i32 %i, 1, !dbg !37                  ; [#uses=1 type=i32] [debug line = 8:37]
  call void @llvm.dbg.value(metadata !{i32 %i.1}, i64 0, metadata !38), !dbg !37 ; [debug line = 8:37] [debug variable = i]
  br label %1, !dbg !37                           ; [debug line = 8:37]

; <label>:3                                       ; preds = %1
  %sum.0.lcssa = phi float [ %sum, %1 ]           ; [#uses=1 type=float]
  %weights.load = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !40 ; [#uses=2 type=float] [debug line = 11:5]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %weights.load) nounwind
  %sum.1 = fadd float %sum.0.lcssa, %weights.load, !dbg !40 ; [#uses=1 type=float] [debug line = 11:5]
  call void @llvm.dbg.value(metadata !{float %sum.1}, i64 0, metadata !36), !dbg !40 ; [debug line = 11:5] [debug variable = sum]
  %tmp = fcmp oge float %sum.1, 0.000000e+00, !dbg !41 ; [#uses=1 type=i1] [debug line = 13:5]
  %tmp.1 = select i1 %tmp, float 1.000000e+00, float -1.000000e+00, !dbg !41 ; [#uses=1 type=float] [debug line = 13:5]
  ret float %tmp.1, !dbg !41                      ; [debug line = 13:5]
}

; [#uses=11]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=0]
define float @dummy_algorithm(float* %pX, float %pY, i1 zeroext %pPredict, i1 zeroext %pReset) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap(float 0.000000e+00) nounwind, !map !42
  call void (...)* @_ssdm_op_SpecTopModule([16 x i8]* @dummy_algorithm.str) nounwind
  call void @llvm.dbg.value(metadata !{float* %pX}, i64 0, metadata !48), !dbg !49 ; [debug line = 16:35] [debug variable = pX]
  call void @llvm.dbg.value(metadata !{float %pY}, i64 0, metadata !50), !dbg !51 ; [debug line = 16:54] [debug variable = pY]
  call void @llvm.dbg.value(metadata !{i1 %pPredict}, i64 0, metadata !52), !dbg !53 ; [debug line = 16:69] [debug variable = pPredict]
  call void @llvm.dbg.value(metadata !{i1 %pReset}, i64 0, metadata !54), !dbg !55 ; [debug line = 16:90] [debug variable = pReset]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %pX, i32 2) nounwind, !dbg !56 ; [debug line = 16:99]
  call void (...)* @_ssdm_op_SpecInterface(float* %pX, i8* getelementptr inbounds ([10 x i8]* @.str, i64 0, i64 0), i32 0, i32 0, i32 0, i32 2, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !58 ; [debug line = 17:1]
  %tmp = fpext float %pY to double, !dbg !59      ; [#uses=1 type=double] [debug line = 18:1]
  call void (...)* @_ssdm_op_SpecInterface(double %tmp, i8* getelementptr inbounds ([10 x i8]* @.str, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !59 ; [debug line = 18:1]
  %tmp.6 = zext i1 %pPredict to i32, !dbg !60     ; [#uses=1 type=i32] [debug line = 19:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 %tmp.6, i8* getelementptr inbounds ([10 x i8]* @.str, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !60 ; [debug line = 19:1]
  %tmp.7 = zext i1 %pReset to i32, !dbg !61       ; [#uses=1 type=i32] [debug line = 20:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 %tmp.7, i8* getelementptr inbounds ([10 x i8]* @.str, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !61 ; [debug line = 20:1]
  call void (...)* @_ssdm_op_SpecInterface(i32 0, i8* getelementptr inbounds ([10 x i8]* @.str, i64 0, i64 0), i32 0, i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0), i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !62 ; [debug line = 21:1]
  br i1 %pReset, label %.preheader.preheader, label %.loopexit, !dbg !63 ; [debug line = 23:2]

.preheader.preheader:                             ; preds = %0
  br label %.preheader, !dbg !64                  ; [debug line = 24:39]

.preheader:                                       ; preds = %1, %.preheader.preheader
  %i = phi i32 [ %i.2, %1 ], [ 0, %.preheader.preheader ] ; [#uses=3 type=i32]
  %exitcond1 = icmp eq i32 %i, 3, !dbg !64        ; [#uses=1 type=i1] [debug line = 24:39]
  br i1 %exitcond1, label %.loopexit.loopexit, label %1, !dbg !64 ; [debug line = 24:39]

; <label>:1                                       ; preds = %.preheader
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([6 x i8]* @.str2, i64 0, i64 0)) nounwind, !dbg !67 ; [debug line = 24:55]
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([6 x i8]* @.str2, i64 0, i64 0)) nounwind, !dbg !67 ; [#uses=1 type=i32] [debug line = 24:55]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !69 ; [debug line = 25:1]
  %tmp.8 = zext i32 %i to i64, !dbg !70           ; [#uses=1 type=i64] [debug line = 26:2]
  %weights.addr = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp.8, !dbg !70 ; [#uses=1 type=float*] [debug line = 26:2]
  store float 0.000000e+00, float* %weights.addr, align 4, !dbg !70 ; [debug line = 26:2]
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([6 x i8]* @.str2, i64 0, i64 0), i32 %rbegin) nounwind, !dbg !71 ; [#uses=0 type=i32] [debug line = 27:9]
  %i.2 = add i32 %i, 1, !dbg !72                  ; [#uses=1 type=i32] [debug line = 24:49]
  call void @llvm.dbg.value(metadata !{i32 %i.2}, i64 0, metadata !73), !dbg !72 ; [debug line = 24:49] [debug variable = i]
  br label %.preheader, !dbg !72                  ; [debug line = 24:49]

.loopexit.loopexit:                               ; preds = %.preheader
  br label %.loopexit

.loopexit:                                        ; preds = %.loopexit.loopexit, %0
  br i1 %pPredict, label %6, label %2, !dbg !74   ; [debug line = 30:2]

; <label>:2                                       ; preds = %.loopexit
  %prediction = call fastcc float @predict(float* %pX), !dbg !75 ; [#uses=1 type=float] [debug line = 31:28]
  call void @llvm.dbg.value(metadata !{float %prediction}, i64 0, metadata !77), !dbg !75 ; [debug line = 31:28] [debug variable = prediction]
  %tmp.11 = fsub float %pY, %prediction, !dbg !78 ; [#uses=2 type=float] [debug line = 35:2]
  br label %3, !dbg !81                           ; [debug line = 33:47]

; <label>:3                                       ; preds = %4, %2
  %i1 = phi i32 [ 0, %2 ], [ %i.3, %4 ]           ; [#uses=3 type=i32]
  %exitcond = icmp eq i32 %i1, 2, !dbg !81        ; [#uses=1 type=i1] [debug line = 33:47]
  br i1 %exitcond, label %5, label %4, !dbg !81   ; [debug line = 33:47]

; <label>:4                                       ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopName(i8* getelementptr inbounds ([15 x i8]* @.str3, i64 0, i64 0)) nounwind, !dbg !82 ; [debug line = 33:62]
  %rbegin2 = call i32 (...)* @_ssdm_op_SpecRegionBegin(i8* getelementptr inbounds ([15 x i8]* @.str3, i64 0, i64 0)) nounwind, !dbg !82 ; [#uses=1 type=i32] [debug line = 33:62]
  call void (...)* @_ssdm_Unroll(i32 0, i32 0, i32 0, i8* getelementptr inbounds ([1 x i8]* @.str1, i64 0, i64 0)) nounwind, !dbg !83 ; [debug line = 34:1]
  %tmp.13 = zext i32 %i1 to i64, !dbg !78         ; [#uses=2 type=i64] [debug line = 35:2]
  %pX.addr = getelementptr inbounds float* %pX, i64 %tmp.13, !dbg !78 ; [#uses=1 type=float*] [debug line = 35:2]
  %pX.load = load float* %pX.addr, align 4, !dbg !78 ; [#uses=2 type=float] [debug line = 35:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %pX.load) nounwind
  %tmp.14 = fmul float %tmp.11, %pX.load, !dbg !78 ; [#uses=1 type=float] [debug line = 35:2]
  %weights.addr.1 = getelementptr inbounds [3 x float]* @weights, i64 0, i64 %tmp.13, !dbg !78 ; [#uses=2 type=float*] [debug line = 35:2]
  %weights.load.2 = load float* %weights.addr.1, align 4, !dbg !78 ; [#uses=2 type=float] [debug line = 35:2]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %weights.load.2) nounwind
  %tmp.15 = fadd float %weights.load.2, %tmp.14, !dbg !78 ; [#uses=1 type=float] [debug line = 35:2]
  store float %tmp.15, float* %weights.addr.1, align 4, !dbg !78 ; [debug line = 35:2]
  %rend3 = call i32 (...)* @_ssdm_op_SpecRegionEnd(i8* getelementptr inbounds ([15 x i8]* @.str3, i64 0, i64 0), i32 %rbegin2) nounwind, !dbg !84 ; [#uses=0 type=i32] [debug line = 36:9]
  %i.3 = add i32 %i1, 1, !dbg !85                 ; [#uses=1 type=i32] [debug line = 33:56]
  call void @llvm.dbg.value(metadata !{i32 %i.3}, i64 0, metadata !86), !dbg !85 ; [debug line = 33:56] [debug variable = i]
  br label %3, !dbg !85                           ; [debug line = 33:56]

; <label>:5                                       ; preds = %3
  %weights.load = load float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !87 ; [#uses=2 type=float] [debug line = 37:9]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %weights.load) nounwind
  %tmp.12 = fadd float %weights.load, %tmp.11, !dbg !87 ; [#uses=1 type=float] [debug line = 37:9]
  store float %tmp.12, float* getelementptr inbounds ([3 x float]* @weights, i64 0, i64 2), align 4, !dbg !87 ; [debug line = 37:9]
  br label %7, !dbg !88                           ; [debug line = 39:9]

; <label>:6                                       ; preds = %.loopexit
  %tmp.10 = call fastcc float @predict(float* %pX), !dbg !89 ; [#uses=1 type=float] [debug line = 41:16]
  br label %7, !dbg !89                           ; [debug line = 41:16]

; <label>:7                                       ; preds = %6, %5
  %.0 = phi float [ %tmp.10, %6 ], [ %pY, %5 ]    ; [#uses=1 type=float]
  ret float %.0, !dbg !91                         ; [debug line = 43:1]
}

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=2]
declare i32 @_ssdm_op_SpecRegionEnd(...)

; [#uses=2]
declare i32 @_ssdm_op_SpecRegionBegin(...)

; [#uses=2]
declare void @_ssdm_op_SpecLoopName(...) nounwind

; [#uses=5]
declare void @_ssdm_op_SpecInterface(...) nounwind

; [#uses=1]
declare void @_ssdm_op_SpecBitsMap(...)

; [#uses=2]
declare void @_ssdm_Unroll(...) nounwind

; [#uses=6]
declare void @_ssdm_SpecKeepArrayLoad(...)

; [#uses=2]
declare void @_ssdm_SpecArrayDimSize(...) nounwind

!llvm.dbg.cu = !{!0}
!hls.encrypted.func = !{}

!0 = metadata !{i32 786449, i32 0, i32 4, metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace/DummyAlgorithm/solution1/.autopilot/db/DummyAlgorithm.pragma.2.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, metadata !1, metadata !1, metadata !3, metadata !19} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !5, metadata !14}
!5 = metadata !{i32 786478, i32 0, metadata !6, metadata !"predict", metadata !"predict", metadata !"_Z7predictPKf", metadata !6, i32 6, metadata !7, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, float (float*)* @predict, null, null, metadata !12, i32 6} ; [ DW_TAG_subprogram ]
!6 = metadata !{i32 786473, metadata !"../../../implementation/hls/DummyAlgorithm/include/DummyAlgorithm.cpp", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace", null} ; [ DW_TAG_file_type ]
!7 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !8, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!8 = metadata !{metadata !9, metadata !10}
!9 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!10 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !11} ; [ DW_TAG_pointer_type ]
!11 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !9} ; [ DW_TAG_const_type ]
!12 = metadata !{metadata !13}
!13 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!14 = metadata !{i32 786478, i32 0, metadata !6, metadata !"dummy_algorithm", metadata !"dummy_algorithm", metadata !"_Z15dummy_algorithmPKffbb", metadata !6, i32 16, metadata !15, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, float (float*, float, i1, i1)* @dummy_algorithm, null, null, metadata !12, i32 16} ; [ DW_TAG_subprogram ]
!15 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !16, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!16 = metadata !{metadata !9, metadata !10, metadata !11, metadata !17, metadata !17}
!17 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !18} ; [ DW_TAG_const_type ]
!18 = metadata !{i32 786468, null, metadata !"bool", null, i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ]
!19 = metadata !{metadata !20}
!20 = metadata !{metadata !21, metadata !25}
!21 = metadata !{i32 786484, i32 0, null, metadata !"weights", metadata !"weights", metadata !"", metadata !6, i32 4, metadata !22, i32 0, i32 1, [3 x float]* @weights} ; [ DW_TAG_variable ]
!22 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 96, i64 32, i32 0, i32 0, metadata !9, metadata !23, i32 0, i32 0} ; [ DW_TAG_array_type ]
!23 = metadata !{metadata !24}
!24 = metadata !{i32 786465, i64 0, i64 2}        ; [ DW_TAG_subrange_type ]
!25 = metadata !{i32 786484, i32 0, null, metadata !"signgam", metadata !"signgam", metadata !"", metadata !26, i32 149, metadata !27, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!26 = metadata !{i32 786473, metadata !"/usr/include/math.h", metadata !"/home/buschjae/projects/masterarbeit/GP-FPGA/boards/artix_7_evaluation_board/hls-workspace", null} ; [ DW_TAG_file_type ]
!27 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!28 = metadata !{i32 786689, metadata !5, metadata !"pX", metadata !6, i32 16777222, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!29 = metadata !{i32 6, i32 27, metadata !5, null}
!30 = metadata !{i32 6, i32 35, metadata !31, null}
!31 = metadata !{i32 786443, metadata !5, i32 6, i32 34, metadata !6, i32 0} ; [ DW_TAG_lexical_block ]
!32 = metadata !{i32 8, i32 28, metadata !33, null}
!33 = metadata !{i32 786443, metadata !31, i32 8, i32 5, metadata !6, i32 1} ; [ DW_TAG_lexical_block ]
!34 = metadata !{i32 9, i32 9, metadata !35, null}
!35 = metadata !{i32 786443, metadata !33, i32 8, i32 42, metadata !6, i32 2} ; [ DW_TAG_lexical_block ]
!36 = metadata !{i32 786688, metadata !31, metadata !"sum", metadata !6, i32 7, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!37 = metadata !{i32 8, i32 37, metadata !33, null}
!38 = metadata !{i32 786688, metadata !33, metadata !"i", metadata !6, i32 8, metadata !39, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!39 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!40 = metadata !{i32 11, i32 5, metadata !31, null}
!41 = metadata !{i32 13, i32 5, metadata !31, null}
!42 = metadata !{metadata !43}
!43 = metadata !{i32 0, i32 31, metadata !44}
!44 = metadata !{metadata !45}
!45 = metadata !{metadata !"return", metadata !46, metadata !"float", i32 0, i32 31}
!46 = metadata !{metadata !47}
!47 = metadata !{i32 0, i32 1, i32 0}
!48 = metadata !{i32 786689, metadata !14, metadata !"pX", metadata !6, i32 16777232, metadata !10, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!49 = metadata !{i32 16, i32 35, metadata !14, null}
!50 = metadata !{i32 786689, metadata !14, metadata !"pY", metadata !6, i32 33554448, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!51 = metadata !{i32 16, i32 54, metadata !14, null}
!52 = metadata !{i32 786689, metadata !14, metadata !"pPredict", metadata !6, i32 50331664, metadata !17, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!53 = metadata !{i32 16, i32 69, metadata !14, null}
!54 = metadata !{i32 786689, metadata !14, metadata !"pReset", metadata !6, i32 67108880, metadata !17, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!55 = metadata !{i32 16, i32 90, metadata !14, null}
!56 = metadata !{i32 16, i32 99, metadata !57, null}
!57 = metadata !{i32 786443, metadata !14, i32 16, i32 98, metadata !6, i32 3} ; [ DW_TAG_lexical_block ]
!58 = metadata !{i32 17, i32 1, metadata !57, null}
!59 = metadata !{i32 18, i32 1, metadata !57, null}
!60 = metadata !{i32 19, i32 1, metadata !57, null}
!61 = metadata !{i32 20, i32 1, metadata !57, null}
!62 = metadata !{i32 21, i32 1, metadata !57, null}
!63 = metadata !{i32 23, i32 2, metadata !57, null}
!64 = metadata !{i32 24, i32 39, metadata !65, null}
!65 = metadata !{i32 786443, metadata !66, i32 24, i32 16, metadata !6, i32 5} ; [ DW_TAG_lexical_block ]
!66 = metadata !{i32 786443, metadata !57, i32 23, i32 14, metadata !6, i32 4} ; [ DW_TAG_lexical_block ]
!67 = metadata !{i32 24, i32 55, metadata !68, null}
!68 = metadata !{i32 786443, metadata !65, i32 24, i32 54, metadata !6, i32 6} ; [ DW_TAG_lexical_block ]
!69 = metadata !{i32 25, i32 1, metadata !68, null}
!70 = metadata !{i32 26, i32 2, metadata !68, null}
!71 = metadata !{i32 27, i32 9, metadata !68, null}
!72 = metadata !{i32 24, i32 49, metadata !65, null}
!73 = metadata !{i32 786688, metadata !65, metadata !"i", metadata !6, i32 24, metadata !39, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!74 = metadata !{i32 30, i32 2, metadata !57, null}
!75 = metadata !{i32 31, i32 28, metadata !76, null}
!76 = metadata !{i32 786443, metadata !57, i32 30, i32 17, metadata !6, i32 7} ; [ DW_TAG_lexical_block ]
!77 = metadata !{i32 786688, metadata !76, metadata !"prediction", metadata !6, i32 31, metadata !9, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!78 = metadata !{i32 35, i32 2, metadata !79, null}
!79 = metadata !{i32 786443, metadata !80, i32 33, i32 61, metadata !6, i32 9} ; [ DW_TAG_lexical_block ]
!80 = metadata !{i32 786443, metadata !76, i32 33, i32 24, metadata !6, i32 8} ; [ DW_TAG_lexical_block ]
!81 = metadata !{i32 33, i32 47, metadata !80, null}
!82 = metadata !{i32 33, i32 62, metadata !79, null}
!83 = metadata !{i32 34, i32 1, metadata !79, null}
!84 = metadata !{i32 36, i32 9, metadata !79, null}
!85 = metadata !{i32 33, i32 56, metadata !80, null}
!86 = metadata !{i32 786688, metadata !80, metadata !"i", metadata !6, i32 33, metadata !39, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!87 = metadata !{i32 37, i32 9, metadata !76, null}
!88 = metadata !{i32 39, i32 9, metadata !76, null}
!89 = metadata !{i32 41, i32 16, metadata !90, null}
!90 = metadata !{i32 786443, metadata !57, i32 40, i32 9, metadata !6, i32 10} ; [ DW_TAG_lexical_block ]
!91 = metadata !{i32 43, i32 1, metadata !57, null}
