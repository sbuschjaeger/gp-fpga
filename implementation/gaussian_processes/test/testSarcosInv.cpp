#include <iostream>
#include <fstream>
#include <random>

#include "Helper.h"
#include "ProjectionGP.h"
#include "ApproxProjectionGP.h"
#include "FullGP.h"

// Load file for training
std::vector<double> examplesTrain;
std::vector<double> labelsTrain;

std::vector<double> examplesTest;
std::vector<double> labelsTest;

unsigned int const DIM = 21;
unsigned int const NTrain = 44484;
unsigned int const NTest = 4449;
double const mean = 13.6538; // we are going to predict the first pose
double const testVariance = 414.27069;

unsigned int numNodes;

#ifdef __arm__
    #define RAPL_NR_DOMAIN 1
#endif

inline void readCSV(std::string const &pPath,
                    std::vector<double> &pExamples,
                    std::vector<double> &pLabels) {
    std::string line;
    std::ifstream file(pPath);

    if (file.is_open()) {
        while ( std::getline(file,line)) {
            if ( line.size() > 0) {
                std::stringstream ss(line);
                std::string entry;  

                int cnt = 0;
                while( std::getline(ss, entry,',') ) {
                    if (entry.size() > 0) {
                        if (cnt == 21) {
                            pLabels.push_back(atof(entry.c_str()));
                        } else if (cnt < 21) {
                            pExamples.push_back(atof(entry.c_str()));
                        }
                    }
                    ++cnt;
                }
            }
        }
        file.close();
    }
}

inline void writeStatistics(
        unsigned int const numBV, 
        double const sigma, 
        double const gamma,
       	double const pLength,
        double &avgMse, 
        double avgThroughput, 
        double totalRuntime, 
        double avgConsumption[][RAPL_NR_DOMAIN], 
        std::ofstream &pOut) {

    std::stringstream ss;
    ss << numBV << "," << sigma << "," << gamma << "," << pLength << "," << avgMse << "," << avgThroughput;

#ifndef __arm__
    for (unsigned int i = 0; i < numNodes; ++i) {
        if(is_supported_domain(RAPL_PKG)){
            avgConsumption[i][RAPL_PKG] /= 10;
            ss << "," << avgConsumption[i][RAPL_PKG];
        }

        if(is_supported_domain(RAPL_PP0)) {
            avgConsumption[i][RAPL_PP0] /= 10;
            ss << "," << avgConsumption[i][RAPL_PP0];
        }

        if(is_supported_domain(RAPL_PP1)){
            avgConsumption[i][RAPL_PP1] /= 10; 
            ss << "," << avgConsumption[i][RAPL_PP1];
        }

        if(is_supported_domain(RAPL_DRAM)){
            avgConsumption[i][RAPL_DRAM] /= 10;
            ss << "," << avgConsumption[i][RAPL_DRAM];
        };
    }
#endif

    ss << "," << totalRuntime << std :: endl;
    pOut << ss.str();
    pOut.flush();
}

inline void performFullGP(
        std::ofstream &pOut,
        int const numBV, 
        double const sigma, 
        double const gamma, 
        double const pLength,
        std::string const &pPowerSaveAt) {

    ConstantMean cMean(mean);
    RBFKernel kernel(gamma, pLength);

    double avgMse = 0.0;
    double avgThroughput = 0.0;
    double avgConsumption[numNodes][RAPL_NR_DOMAIN];
    double avgRuntime = 0.0;

    double mse = 0.0;
    double throughput = 0.0;
    double consumption[numNodes][RAPL_NR_DOMAIN];
    double startConsumption[numNodes][RAPL_NR_DOMAIN];
    double endConsumption[numNodes][RAPL_NR_DOMAIN];
    for (unsigned int i = 0; i < numNodes; ++i) {
        for (unsigned int j = 0; j < RAPL_NR_DOMAIN; ++j) {
            consumption[i][j] = 0.0;
            avgConsumption[i][j] = 0.0;
        }
    }

    std :: cout << "\n\nStart measurments for FULL GP " 
                << " config numBV = " << numBV << " sigma = " << sigma
                << " gamma= " << gamma << " length = " << pLength
                << std :: endl;
    for (unsigned int i = 0; i < 10; ++i) {
        GP::FullGP alg(numBV, DIM, sigma, kernel, cMean);

        unsigned int indices[numBV];
        std::chrono::high_resolution_clock::time_point t3,t4;

        std::uniform_int_distribution<> dist(0,NTrain);
        std::mt19937 rng; 
        rng.seed(0);

        for (unsigned int i = 0; i < numBV; ++i) {
            bool isUnique = false;
            unsigned int index;
            while(!isUnique) {
                isUnique = true;
                index = dist(rng);

                for (unsigned int j = 0; j < i; ++j) {
                    if (index == indices[j]) {
                        isUnique = false;
                        break;
                    }
                }
            }
            indices[i] = index;
        }
    #ifdef __linux__  
        std::string cmd("powerstat -d 1 1 1200 >> " + pPowerSaveAt  + "_power.csv &");
        int status = system(cmd.c_str());
        // TODO: DO SOMETHING ABOUT STATUS
    #endif

    #ifndef __arm__
        helper::getEnergyConsumption(numNodes, startConsumption);
    #endif

        t3 = std::chrono::high_resolution_clock::now();
        for (unsigned int i = 0; i < numBV; ++i) {
            alg.train(&examplesTrain[indices[i]*DIM], labelsTrain[indices[i]]);
        }
        t4 = std::chrono::high_resolution_clock::now();
    #ifndef __arm__
        helper::getEnergyConsumption(numNodes, endConsumption);
    #endif

    #ifdef __linux__ 
        status = system("kill -INT $(ps -e | grep powerstat | awk '{ print $1 }')");
    #endif

        double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( t4 - t3 ).count();

        for (unsigned int i = 0; i < NTest; ++i) {
            double pred = alg.predict(&examplesTest[i*DIM]);

            mse += ( (pred - labelsTest[i]) * (pred - labelsTest[i]) ); 
        }

        mse = (mse/NTest)/testVariance;
        throughput = NTrain / runtime;

        avgMse = avgMse + mse;
        avgThroughput = avgThroughput + throughput;
        avgRuntime = avgRuntime + runtime;

    #ifndef __arm__
        for (unsigned int i = 0; i < numNodes; ++i) {
            if(is_supported_domain(RAPL_PKG)){
                consumption[i][RAPL_PKG] += (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG]) / (runtime / 1000.0);
                avgConsumption[i][RAPL_PKG] += consumption[i][RAPL_PKG];
            }

            if(is_supported_domain(RAPL_PP0)) {
                consumption[i][RAPL_PP0] += (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0]) / (runtime / 1000.0);
                avgConsumption[i][RAPL_PP0] += consumption[i][RAPL_PP0];
            }

            if(is_supported_domain(RAPL_PP1)){
                consumption[i][RAPL_PP1] += (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1]) / (runtime / 1000.0);
                avgConsumption[i][RAPL_PP1] += consumption[i][RAPL_PP1];
            }

            if(is_supported_domain(RAPL_DRAM)){
                consumption[i][RAPL_DRAM] += (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM]) / (runtime / 1000.0);
                avgConsumption[i][RAPL_DRAM] += consumption[i][RAPL_DRAM];
            }
        }
    #endif
    }

    avgMse /= 10;
    avgThroughput /= 10;
    avgRuntime /= 10;

    std :: cout << "Test run " << " [AMSE - runtime - throughput - energy]: " 
                << avgMse << "\t" << avgRuntime << " ms \t" 
                << avgThroughput << " #elem/ms \t"
                // TODO USING CONSTANT 0 HERE, MIGHT BE WRONG ON OTHER SYSTEMS
#ifndef __arm__
                << avgConsumption[0][RAPL_PKG] / (avgRuntime / 1000.0) << " W"  
#endif
                << std :: endl;

    writeStatistics(numBV, sigma, gamma, pLength, avgMse, avgThroughput, avgRuntime, avgConsumption, pOut);
} 

template <class T>
inline void performProjectionGPRun(
        std::ofstream &pOut,
        unsigned int const numBV, 
        double const sigma, 
        double const gamma, 
        double const pLength,
        std::string const &pPowerSaveAt) {

    ConstantMean cMean(mean);
    RBFKernel kernel(gamma, pLength);

    double mse = 0.0;
    double throughput = 0.0;
    double consumption[numNodes][RAPL_NR_DOMAIN];
    double startConsumption[numNodes][RAPL_NR_DOMAIN];
    double endConsumption[numNodes][RAPL_NR_DOMAIN];
    for (unsigned int i = 0; i < numNodes; ++i) {
        for (unsigned int j = 0; j < RAPL_NR_DOMAIN; ++j) {
            consumption[i][j] = 0.0;
        }
    }

    std::string name = "";
    if (std::is_same<T, GP::ProjectionGP>::value) {
        name = "ProjectionGP";
    } else if (std::is_same<T, GP::ApproxProjectionGP>::value) {
        name = "ApproxProjectionGP";
    }

    std :: cout << "\n\nStart measurments for " 
                << name << " config"
                << " numBV = " << numBV << " sigma = " << sigma
                << " gamma= " << gamma << " length = " << pLength <<
                std :: endl;
    
    T alg(numBV, kernel, cMean, DIM, sigma);
    std::chrono::high_resolution_clock::time_point t3,t4;
#ifdef __linux__  
    std::string cmd("powerstat -d 1 1 1200 >> " + pPowerSaveAt  + "_power.csv &");
    int status = system(cmd.c_str());
    // TODO: DO SOMETHING ABOUT STATUS
#endif

#ifndef __arm__
    helper::getEnergyConsumption(numNodes, startConsumption);
#endif

    t3 = std::chrono::high_resolution_clock::now();
    for (unsigned int i = 0; i < NTrain; ++i) {
        alg.train(&examplesTrain[i*DIM], labelsTrain[i]);
    }
    t4 = std::chrono::high_resolution_clock::now();
#ifndef __arm__
    helper::getEnergyConsumption(numNodes, endConsumption);
#endif

#ifdef __linux__ 
    status = system("kill -INT $(ps -e | grep powerstat | awk '{ print $1 }')");
    // TODO: DO SOMETHING ABOUT STATUS
#endif

    double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( t4 - t3 ).count();
    
    for (unsigned int i = 0; i < NTest; ++i) {
        double pred = alg.predict(&examplesTest[i*DIM]);

        mse += ( (pred - labelsTest[i]) * (pred - labelsTest[i]) ); 
    }

    mse = (mse/ NTest)/ testVariance;
    throughput = NTrain / runtime;

#ifndef __arm__
    for (unsigned int i = 0; i < numNodes; ++i) {
        if(is_supported_domain(RAPL_PKG)){
            consumption[i][RAPL_PKG] += (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG]) / (runtime / 1000.0);
        }

        if(is_supported_domain(RAPL_PP0)) {
            consumption[i][RAPL_PP0] += (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0]) / (runtime / 1000.0);
        }

        if(is_supported_domain(RAPL_PP1)){
            consumption[i][RAPL_PP1] += (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1]) / (runtime / 1000.0);
        }

        if(is_supported_domain(RAPL_DRAM)){
            consumption[i][RAPL_DRAM] += (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM]) / (runtime / 1000.0);
        }
    }
#endif

    std :: cout << "Test run " << " [SMSE - runtime - throughput - energy]: " 
                << mse << "\t" << runtime << " ms \t" 
                << throughput << " #elem/ms \t"
                // TODO USING CONSTANT 0 HERE, MIGHT BE WRONG ON OTHER SYSTEMS
#ifndef __arm__
                << (endConsumption[0][RAPL_PKG] - startConsumption[0][RAPL_PKG]) / (runtime / 1000.0) << " W"  
#endif
                << std :: endl;

    writeStatistics(numBV, sigma, gamma, pLength, mse, throughput, runtime, consumption, pOut);
} 

int main(int argc, char* argv[]) {
    if (argc < 3) {
        std :: cout << "Please specify path to uj location data" << std :: endl;
        return -1;
    }    

    std::string pathToTrain(argv[1]);
    std::string pathToTest(argv[2]);
    std::string folderToSave(argv[3]);

#ifndef __arm__
    if (0 != init_rapl()) {
        std :: cout << "Init of rapl lib failed. Librapl needs root."
                    << "Also 'modeprobe cpuid' and 'modeprobe msr' might help" 
                    << std :: endl;
        terminate_rapl();
        return -1;
    }
    numNodes = get_num_rapl_nodes_pkg();
#endif
    std :: cout << "*** STARTING LOCATION DATA Test *** " << std :: endl;
    std :: cout << std :: endl;
    std :: cout << "Current time: " << helper::getCurrentTime() << std ::endl;

    std :: cout << "Reading file: " << pathToTrain << std :: endl;
    readCSV(pathToTrain, examplesTrain, labelsTrain);
    readCSV(pathToTest, examplesTest, labelsTest);

    if (examplesTrain.size() / DIM != NTrain || examplesTest.size() / DIM != NTest) {
        std :: cout << "examplesTrain:" << examplesTrain.size() << std :: endl;
        std :: cout << "examplesTest:" << examplesTest.size() << std :: endl;

        std :: cout << "ERROR: DIM / NTrain / NTest does not match expected size" << std :: endl;
        std :: cout << "NTrain: " << labelsTrain.size() << std :: endl;
        std :: cout << "NTest: " << labelsTest.size() << std :: endl;
        return -1;
    }

    double minMaxValues[DIM][2];
    helper::getMinMaxValues(&examplesTrain[0], NTrain, DIM, minMaxValues);
    helper::normalizeData(&examplesTrain[0], NTrain, DIM, minMaxValues);
    helper::normalizeData(&examplesTest[0], NTest, DIM, minMaxValues);

    std::ofstream outFile(folderToSave + "/projection_gp.csv");
    std::stringstream header;
    header << "NUMBV, SIGMA, GAMMA, AVG_SMSE, AVG_THROUGHPUT[#elem/ms]";

#ifndef __arm__
    for (unsigned int i = 0; i < numNodes; ++i) {
        if(is_supported_domain(RAPL_PKG)){
            header << "," << "RAPL_PKG_" << i;
        }

        if(is_supported_domain(RAPL_PP0)) {
            header << "," << "RAPL_PP0_" << i;
        }

        if(is_supported_domain(RAPL_PP1)) {
            header << "," << "RAPL_PP1_" << i;
        }

        if(is_supported_domain(RAPL_DRAM)) {
            header << "," << "RAPL_DRAM_" << i;
        };
    }
#endif

    header << ", TOTAL_RUNTIME[s]";
    outFile << header.str() << std :: endl;

    int const numBVTest[] = {50, 100, 200, 300, 400, 500, 750, 1000};
    double const sigmaTest[] = {0.5,5,10};
    double const gammaTest[] = {0.005, 0.05, 0.5, 2};
    double const lengthTest[] = {0.01, 0.5, 1, 5};

    for (unsigned int i = 0; i < 8; ++i) {
        for (unsigned int j = 0; j < 1; ++j) {
            for (unsigned int k = 0; k < 1; ++k) {
            	for (unsigned int l = 0; l < 1; ++l) {
		            std::stringstream ss;
		            ss << folderToSave << "/projection_gp" << numBVTest[i] << "_" << sigmaTest[j] << "_" << gammaTest[k] << "_" << lengthTest[l];

		            performProjectionGPRun<GP::ProjectionGP>(
		                    outFile, 
		                    numBVTest[i], 
		                    sigmaTest[j], 
		                    gammaTest[k], 
		                    lengthTest[l],
		                    ss.str()
		            );
		            ss.clear();
		            ss.str("");
            	}
            }
        }
    }

    outFile.close();
    outFile.clear();

    outFile.open(folderToSave + "/approx_projection_gp.csv");
    outFile << header.str() << std :: endl;

    for (unsigned int i = 0; i < 8; ++i) {
        for (unsigned int j = 0; j < 1; ++j) {
            for (unsigned int k = 0; k < 1; ++k) {
            	for (unsigned int l = 0; l < 1; ++l) {
	                std::stringstream ss;
	                ss << folderToSave << "/approx_projection_gp" << numBVTest[i] << "_" << sigmaTest[j] << "_" << gammaTest[k] << "_" << lengthTest[l];

	                performProjectionGPRun<GP::ApproxProjectionGP>(
	                        outFile, 
	                        numBVTest[i], 
	                        sigmaTest[j], 
	                        gammaTest[k], 
	                        lengthTest[l],
	                        ss.str()
	                );
	                ss.clear();
	                ss.str("");
	            }
            }
        }
    }
    outFile.close();
    outFile.clear();

    outFile.open(folderToSave + "/full_gp.csv");
    outFile << header.str() << std :: endl;

    for (unsigned int i = 0; i < 8; ++i) {
        for (unsigned int j = 0; j < 1; ++j) {
            for (unsigned int k = 0; k < 1; ++k) {
            	for (unsigned int l = 0; l < 1; ++l) {
	                std::stringstream ss;
	                ss << folderToSave << "/full_gp" << numBVTest[i] << "_" << sigmaTest[j] << "_" << gammaTest[k] << "_" << lengthTest[l];
                    performFullGP(
                            outFile, 
                            numBVTest[i], 
                            sigmaTest[j], 
                            gammaTest[k],
                            lengthTest[l], 
                            ss.str() 
                    );
	                ss.clear();
	                ss.str("");
	            }
            }
        }
    }
    outFile.close();
    outFile.clear();

#ifndef __arm__
    terminate_rapl();
#endif
}

