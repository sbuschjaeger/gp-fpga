#include <iostream>
#include <random>

#include "ProjectionGP.h"
#include "ApproxProjectionGP.h"

int main(int argc, char * argv[]) {
	const unsigned int maxDim = 300;
	const unsigned int stepsize = 5;
    const unsigned int DIM = 21;
    //const unsigned int dims[24] = {8, 16, 21, 32, 50, 64, 76, 96, 100, 110, 120, 128, 132, 150, 200, 225, 240, 256, 300, 320, 350, 400,750,1000};
    const unsigned int numbvs[1] ={ 100};
    const unsigned int NTrain = 10000;
    const double mean = 5.5;
    const double length = 1;
    const double gamma = 0.5;
    const double sigma = 2;

#ifndef __arm__
    if (0 != init_rapl()) {
        std :: cout << "Init of rapl lib failed. Librapl needs root."
                    << "Also 'modeprobe cpuid' and 'modeprobe msr' might help" 
                    << std :: endl;
        terminate_rapl();
        return -1;
    }
    unsigned int numNodes = get_num_rapl_nodes_pkg();
#endif

    //std::ofstream projectionGPOut("projection_gp.csv");
    std::ofstream approxOut("approx_gp.csv");
    //std::ofstream phasedOut("phased_gp.csv");

    std :: stringstream header;
    header << "NAME" << "," << "NUMBV" << "," << "DIM" << "," << "THROUGHPUT";
#ifndef __arm__
    for (unsigned int i = 0; i < numNodes; ++i) {
        if(is_supported_domain(RAPL_PKG)){
            header << "," << "RAPL_PKG_" << i;
        }

        if(is_supported_domain(RAPL_PP0)) {
            header << "," << "RAPL_PP0_" << i;
        }

        if(is_supported_domain(RAPL_PP1)) {
            header << "," << "RAPL_PP1_" << i;
        }

        if(is_supported_domain(RAPL_DRAM)) {
            header << "," << "RAPL_DRAM_" << i;
        };
    }
#endif

    //projectionGPOut << header.str() << std :: endl;
    approxOut << header.str() << std :: endl;
    //phasedOut << header.str() << std :: endl;

#ifndef __arm__
    double startConsumption[numNodes][RAPL_NR_DOMAIN];
    double endConsumption[numNodes][RAPL_NR_DOMAIN];
#endif
    for (unsigned int k = 0; k < 1; ++k) {
        for (unsigned int l = stepsize; l < maxDim; l += stepsize) {
            const unsigned int numBV = l;
            std :: cout << "Testing: " << "NUMBV = " << numBV << " DIM = " << DIM << std :: endl;
            double coeff[DIM];

            std::vector<double> XTrain;
            std::vector<double> YTrain;
            std::vector<double> XTest;
            std::vector<double> YTest;

            std::random_device rd;
            std::mt19937 gen(rd());
            std::normal_distribution<> d(mean,sigma);

            for (unsigned int i = 0; i < DIM; ++i) {
                coeff[i] = d(gen);
            }

            for (unsigned int i = 0; i < NTrain; ++i) {
                double sum = 0.0;
                for (unsigned int j = 0; j < DIM; ++j) {
                    XTrain.push_back( d(gen) );
                    sum += coeff[j]*XTrain[i*j];
                }
                YTrain.push_back(sum);
            }

            // TODO: COMPUTE ACTUAL MEAN
            ConstantMean cMean(0);
            RBFKernel kernel(gamma,length);
            GP::ApproxProjectionGP agp(numBV, kernel, cMean, DIM, 5);
            //GP::ProjectionGP gp(numbvs[k], kernel, cMean, DIM, 5);
            std::chrono::high_resolution_clock::time_point t3,t4;

/*            unsigned int i = 0;
            GP::PhasedProjectionGP pgp(numbvs[k], kernel, cMean, DIM, 5);
            for (; i < numbvs[k]; ++i) {
                pgp.train(&XTrain[i*DIM], YTrain[i]);
            }

#ifndef __arm__
            helper::getEnergyConsumption(numNodes, startConsumption);
#endif
            t3 = std::chrono::high_resolution_clock::now();
            for (; i < NTrain; ++i) {
                pgp.train(&XTrain[i*DIM], YTrain[i]);
            }
            t4 = std::chrono::high_resolution_clock::now();

#ifndef __arm__
            helper::getEnergyConsumption(numNodes, endConsumption);
#endif
            double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( t4 - t3 ).count();
            double throughput = NTrain / runtime;

            phasedOut << "phased" << "," << numbvs[k] << "," << DIM << "," << throughput;
#ifndef __arm__
            for (unsigned int i = 0; i < numNodes; ++i) {
                if(is_supported_domain(RAPL_PKG)){
                    phasedOut << "," << (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_PP0)) {
                    phasedOut << "," << (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_PP1)){
                    phasedOut << "," << (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_DRAM)){
                    phasedOut << "," << (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM]) / (runtime / 1000.0);
                };
            }
#endif
            phasedOut << std :: endl;

            i = 0;
            for (; i < numbvs[k]; ++i) {
                agp.train(&XTrain[i*DIM], YTrain[i]);
            }
*/
#ifndef __arm__
            helper::getEnergyConsumption(numNodes, startConsumption);
#endif
  
            unsigned int i = 0;
            t3 = std::chrono::high_resolution_clock::now();
            for (; i < NTrain; ++i) {
                agp.train(&XTrain[i*DIM], YTrain[i]);
            }
            t4 = std::chrono::high_resolution_clock::now();
#ifndef __arm__
            helper::getEnergyConsumption(numNodes, endConsumption);
#endif

            double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( t4 - t3 ).count();
            double throughput = NTrain / runtime;

            approxOut << "approx" << "," << numbvs[k] << "," << DIM << "," << throughput;
#ifndef __arm__
            for (unsigned int i = 0; i < numNodes; ++i) {
                if(is_supported_domain(RAPL_PKG)){
                    approxOut << "," << (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_PP0)) {
                    approxOut << "," << (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_PP1)){
                    approxOut << "," << (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_DRAM)){
                    approxOut << "," << (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM]) / (runtime / 1000.0);
                };
            }
#endif
            approxOut << std :: endl;
/*
            unsigned int i = 0;
            for (; i < numbvs[k]; ++i) {
                gp.train(&XTrain[i*DIM], YTrain[i]);
            }

#ifndef __arm__
            helper::getEnergyConsumption(numNodes, startConsumption);
#endif            
            t3 = std::chrono::high_resolution_clock::now();
            for (; i < NTrain; ++i) {
                gp.train(&XTrain[i*DIM], YTrain[i]);
            }
            t4 = std::chrono::high_resolution_clock::now();
#ifndef __arm__
            helper::getEnergyConsumption(numNodes, endConsumption);
#endif

            double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( t4 - t3 ).count();
            double throughput = NTrain / runtime;

            //projectionGPOut << "projection" << "," << numbvs[k] << "," << DIM << "," << throughput;
#ifndef __arm__
            for (unsigned int i = 0; i < numNodes; ++i) {
                if(is_supported_domain(RAPL_PKG)){
                    projectionGPOut << "," << (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_PP0)) {
                    projectionGPOut << "," << (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_PP1)){
                    projectionGPOut << "," << (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1]) / (runtime / 1000.0);
                }

                if(is_supported_domain(RAPL_DRAM)){
                    projectionGPOut << "," << (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM]) / (runtime / 1000.0);
                };
            }
#endif
            projectionGPOut << std :: endl;
            */
        }
    }

}

