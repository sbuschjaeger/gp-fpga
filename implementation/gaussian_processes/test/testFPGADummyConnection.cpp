#include <iostream>
#include <random>
#include <chrono>
#include <cstdlib>

#include <boost/asio.hpp>
#include <boost/static_assert.hpp>

#include "../include/Helper.h"

#include "../../hls/DummyAlgorithm/include/DummyAlgorithm_config.h"

unsigned int const payloadLength = (dim + 1)*sizeof(float)+1;

char* buildDataPackage(float const * const pX, float const pY, bool pPredict) {
    char *data = new char[payloadLength];

    // First byte of data is protocol specific. We just look at the LSB
    // If this is set to 0, we are in train mode
    // If this is set to 1, we are in prediction mode
    data[0] = pPredict;

    unsigned int cnt = 1;
    for (unsigned int i = 0; i < dim; ++i) {
        float f = pX[i];
        unsigned char const * xAsChar = reinterpret_cast<unsigned char const *>(&f);
        for (unsigned int j = 0; j < sizeof(float); ++j) {
            data[cnt++] = xAsChar[j];
        }
    }

    if (!pPredict) {
        float f = pY;
        unsigned char const * yAsChar = reinterpret_cast<unsigned char const *>(&f);
        for (unsigned int j = 0; j < sizeof(float); ++j) {
            data[cnt++] = yAsChar[j];
        }
    }

    return data;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std :: cout << "First parameter needs to be the ip address!" << std :: endl;
        return 0;
    }

    if (argc < 3) {
        std :: cout << "Second parameter needs to be ne size of trainign data in MB!" << std :: endl;
        return 0;
    }

    std :: string address(argv[1]);
    const unsigned int NTrainMB = atoi(argv[2]);
    std :: cout << "Payload size: " << payloadLength << std :: endl;
    std :: cout << "Target system: " << address << std :: endl;
    std :: cout << "Training size: " << NTrainMB << " MB" << std :: endl;

    // Just make sure this runs on "normal" system, 
    // where one byte is 8 bit long and one float is 4 byte
    BOOST_STATIC_ASSERT(CHAR_BIT == 8);
    BOOST_STATIC_ASSERT(sizeof(float) * CHAR_BIT == 32);

    const unsigned int NTrainExamples = (NTrainMB*1e6)/payloadLength + 1; 
    const unsigned int NTestExamples = 1;

    std::vector<float> XTrain;
    std::vector<float> YTrain;
    std::vector<float> XTest;
    std::vector<float> YTest;
    
    std :: cout << "Generating " << NTrainMB << " MB of training data" << std :: endl;
    helper::generateData<float>( dim, NTrainExamples, NTestExamples, XTrain, XTest, YTrain, YTest );
    std :: cout << "Generating done." << std :: endl;

    boost::asio::ip::tcp::socket *s;
    boost::asio::io_service io_service;

    boost::asio::ip::tcp::resolver resolver(io_service);
    boost::asio::ip::tcp::resolver::query query(boost::asio::ip::tcp::v4(), address, "5858");
    // boost::asio::ip::tcp::resolver::query query(boost::asio::ip::tcp::v4(), "129.217.30.162", "5858");
    boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);

    std :: cout << "Connecting to FPGA" << std :: endl;
    s = new boost::asio::ip::tcp::socket(io_service);
    boost::asio::connect((*s), iterator);

    std :: cout << "Start training? Press any key to continue" << std :: endl;
    helper::getch();

    std::chrono::high_resolution_clock::time_point start, end;
    unsigned int cnt = 0;
    start = std::chrono::high_resolution_clock::now();
    double runtime;

    for (unsigned int  i = 0; i < NTrainExamples; ++i) {
        char *data = buildDataPackage(&XTrain[i*dim],YTrain[i],false);

        /*std :: cout << "sending data" << std :: endl;
        for (unsigned int j = 0; j < payloadLength; ++j) {
            std :: cout << std :: hex << (int)data[j];
            std :: cout << " ";
        }
        std :: cout << std :: endl;*/

        // Send the data
        boost::asio::write((*s), boost::asio::buffer(data, payloadLength));
        delete [] data;
        ++cnt;
        end = std::chrono::high_resolution_clock::now();
        runtime = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();
        
        if (runtime >= 1000) {
            std :: cout << "Throughput: " << cnt / runtime * 1000.0 << " #elem/s - " 
                        << (cnt / runtime * 1000.0) * payloadLength / 1e6 << " MB/s" << std :: endl;
            cnt = 0;
            start = std::chrono::high_resolution_clock::now();
        }

        /*// Wait for FPGA to process package
        char reply;
        unsigned int replyLength = boost::asio::read((*s), boost::asio::buffer(&reply, 1));

        if (replyLength != 1) {
            std :: cout << "Length mismatch of FPGA reply" << std :: endl;
        } else {
            // check if and only if first bit is set to 1 (rest 0)
            // thus reply == 0000 0001
            if (reply != 1) {
                std :: cout << "FPGA Error " << reply << " occured" << std :: endl;
            } else {
                std :: cout << "Received valid FPGA reply" << std :: endl;
            }
        }*/

    }

    std :: cout << "Done" << std :: endl;

    //std :: cout << "Now starting prediction" << std :: endl;
    for (unsigned int i = 0; i < NTestExamples; ++i) {
        char *data = buildDataPackage(&XTest[i*dim],0,true);

        // Send the data
        boost::asio::write((*s), boost::asio::buffer(data, payloadLength));
        delete[] data;

        // Wait for FPGA to process package
        //char reply[5];
        //unsigned int replyLength = boost::asio::read((*s), boost::asio::buffer(&reply, 5));

        /*if (replyLength != 5) {
            std :: cout << "Length mismatch of FPGA reply" << std :: endl;
        } else {
            // check if and only if first bit is set to 1 (rest 0)
            // thus reply == 0000 0001
            if (reply[0] != 1) {
                   std :: cout << "FPGA Error " << reply << " occured" << std :: endl;
            }

            float *ret = reinterpret_cast<float*>(&reply[1]);
            std :: cout << "Prediction / Label: " << *ret << " / " << YTest[i] << std :: endl;
        }*/
    }

    std :: cout << "Packages sent: " << NTrainExamples + NTestExamples << std :: endl;
}