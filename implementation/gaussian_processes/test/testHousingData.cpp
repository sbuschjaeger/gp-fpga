#include <iostream>
#include <fstream>
#include <random>

#include "Helper.h"
#include "ProjectionGP.h"
#include "ApproxProjectionGP.h"
#include "FullGP.h"

// Load file for training
std::vector<double> examples;
std::vector<double> labels;
std::vector<unsigned int> indicesBegin;
std::vector<unsigned int> indicesEnd;
unsigned int const DIM = 13;
unsigned int N;
double const mean = 66.87798;

unsigned int numNodes = 1;

#ifdef __arm__
    #define RAPL_NR_DOMAIN 1
#endif

inline void performXValRun(
	OnlineAlgorithm &pAlgo, 
	unsigned int const *pIndices,
	unsigned int const numBV, 
	unsigned int const k,
	double *mse, 
	double *throughput, 
	double &totalRuntime,
	double &avgMse, 
	double &avgThroughput, 
	double avgConsumption[][RAPL_NR_DOMAIN],
	std::string const &pSaveAt) {

	std::chrono::high_resolution_clock::time_point t3,t4;
    double startConsumption[numNodes][RAPL_NR_DOMAIN];
    double endConsumption[numNodes][RAPL_NR_DOMAIN];

#ifdef __linux__  
    std::string cmd("powerstat -d 1 1 1200 >> " + pSaveAt  + "_power.csv &");
	int status = system(cmd.c_str());
    // TODO: DO SOMETHING ABOUT STATUS
#endif

#ifndef __arm__
	helper::getEnergyConsumption(numNodes, startConsumption);
#endif

    t3 = std::chrono::high_resolution_clock::now();
    for (unsigned int j = 0; j < 10; ++j) {
        if (j != k) {
        	if (pIndices == NULL) {
        		for (unsigned int i = indicesBegin[j]; i < indicesEnd[j]; ++i) {
                	pAlgo.train(&examples[i*DIM], labels[i]);
                }
            } else {
            	for (unsigned int i = 0; i < numBV; ++i) {
                	pAlgo.train(&examples[pIndices[i]*DIM], labels[pIndices[i]]);
                }
            }	
        }
    }
    t4 = std::chrono::high_resolution_clock::now();
#ifndef __arm__
	helper::getEnergyConsumption(numNodes, endConsumption);
#endif

#ifdef __linux__ 
	status = system("kill -INT $(ps -e | grep powerstat | awk '{ print $1 }')");
    // TODO: DO SOMETHING ABOUT STATUS
#endif

    double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( t4 - t3 ).count();
    totalRuntime += runtime;

    
    double mean = 0.0;
    unsigned int cnt = 0;
    for (unsigned int i = indicesBegin[k]; i < indicesEnd[k]; ++i) {
        mean += examples[i*DIM];
        ++cnt;
    }
    mean /= cnt;

    double testVariance = 0.0;

    for (unsigned int i = indicesBegin[k]; i < indicesEnd[k]; ++i) {
        double pred = pAlgo.predict(&examples[i*DIM]);
        double label = labels[i];
        mse[k] += (pred - label) * (pred - label);
        testVariance += (mean - label) * (mean - label);
    }
    
    testVariance /= cnt;
    mse[k] = (mse[k] / cnt) / testVariance;
    throughput[k] += (N - (indicesEnd[k] - indicesBegin[k])) / runtime;

#ifndef __arm__
    for (unsigned int i = 0; i < numNodes; ++i) {
        if(is_supported_domain(RAPL_PKG)){
            avgConsumption[i][RAPL_PKG] += (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG]) / (runtime / 1000.0);
        }

        if(is_supported_domain(RAPL_PP0)) {
            avgConsumption[i][RAPL_PP0] += (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0]) / (runtime / 1000.0);
        }

        if(is_supported_domain(RAPL_PP1)){
            avgConsumption[i][RAPL_PP1] += (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1]) / (runtime / 1000.0);
        }

        if(is_supported_domain(RAPL_DRAM)){
            avgConsumption[i][RAPL_DRAM] += (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM]) / (runtime / 1000.0);
        }
    }
#endif

    std :: cout << "Cross-validation run " << k << " [AMSE - runtime - throughput - energy]: " 
                << mse[k] << "\t" << runtime << " ms \t" 
                << throughput[k] << " #elem/ms \t"
                // TODO USING CONSTANT 0 HERE, MIGHT BE WRONG ON OTHER SYSTEMS
#ifndef __arm__
                << (endConsumption[0][RAPL_PKG] - startConsumption[0][RAPL_PKG]) / (runtime / 1000.0) << " W"  
#endif
                << std :: endl;

    avgMse += mse[k];
    avgThroughput += throughput[k];
}

inline void writeStatistics(
		unsigned int const numBV, 
		double const sigma, 
		double const gamma,
		double &avgMse, 
		double avgThroughput, 
		double totalRuntime, 
		double avgConsumption[][RAPL_NR_DOMAIN], 
		std::ofstream &pOut) {

	avgMse /= 10;
    avgThroughput /= 10;
    totalRuntime /= 10;
    std::stringstream ss;
    ss << numBV << "," << sigma << "," << gamma << "," << avgMse << "," << avgThroughput;

#ifndef __arm__
    for (unsigned int i = 0; i < numNodes; ++i) {
		if(is_supported_domain(RAPL_PKG)){
            avgConsumption[i][RAPL_PKG] /= 10;
            ss << "," << avgConsumption[i][RAPL_PKG];
        }

        if(is_supported_domain(RAPL_PP0)) {
            avgConsumption[i][RAPL_PP0] /= 10;
            ss << "," << avgConsumption[i][RAPL_PP0];
        }

        if(is_supported_domain(RAPL_PP1)){
            avgConsumption[i][RAPL_PP1] /= 10; 
            ss << "," << avgConsumption[i][RAPL_PP1];
        }

        if(is_supported_domain(RAPL_DRAM)){
            avgConsumption[i][RAPL_DRAM] /= 10;
            ss << "," << avgConsumption[i][RAPL_DRAM];
        };
    }
#endif

    ss << "," << totalRuntime << std :: endl;
    pOut << ss.str();
    pOut.flush();
}

inline void performFullGP(
		std::ofstream &pOut,
		int const numBV, 
		double const sigma, 
		double const gamma, 
        double const length,
		double const mean,
		std::string const &pPowerSaveAt) {

	ConstantMean cMean(mean);
    RBFKernel kernel(gamma,length);

    double mse[10];
    double throughput[10];
    for (unsigned int i = 0; i < 10; ++i) {
        mse[i] = 0.0;
        throughput[i] = 0.0;
    }
    double avgMse = 0.0;
    double avgThroughput = 0.0;
    double totalRuntime = 0.0;

    double avgConsumption[numNodes][RAPL_NR_DOMAIN];
    for (unsigned int i = 0; i < numNodes; ++i) {
        for (unsigned int j = 0; j < RAPL_NR_DOMAIN; ++j) {
            avgConsumption[i][j] = 0.0;
        }
    }

 	std :: cout << "\n\nStart measurments FULL GP for config " 
    		 	<< "numBV = " << numBV << " sigma = " << sigma
    		 	<< " gamma= " << gamma << std :: endl;

    for (unsigned int k = 0; k < 10; ++k) {
    	if (numBV == -1) {
    		GP::FullGP alg(N - (indicesEnd[k] - indicesBegin[k]), DIM,sigma, kernel, cMean);
	        performXValRun(alg, NULL, numBV, k, mse, throughput, totalRuntime, avgMse,  avgThroughput, avgConsumption, pPowerSaveAt);
    	} else {
    		GP::FullGP alg(numBV, DIM,sigma, kernel, cMean);
	    	unsigned int indices[numBV];

	        std::uniform_int_distribution<> dist(0,N);
	        std::mt19937 rng; 
	        rng.seed(0);

	        for (unsigned int i = 0; i < numBV; ++i) {
	            bool isUnique = false;
	            unsigned int index;
	            while(!isUnique) {
	                isUnique = true;
	                index = dist(rng);

	                if (index > indicesBegin[k] && index < indicesEnd[k]) {
	                    isUnique = false;
	                    continue;
	                }

	                for (unsigned int j = 0; j < i; ++j) {
	                    // NOTE: THIS WORKS BECAUSE WE MAKE A LINEAR SPLIT X-VAL
	                    if (index == indices[j]) {
	                        isUnique = false;
	                        break;
	                    }
	                }
	            }
	            indices[i] = index;
	        }
	        performXValRun(alg, indices, numBV, k, mse, throughput, totalRuntime, avgMse,  avgThroughput, avgConsumption, pPowerSaveAt);
    	}
    }

    writeStatistics(numBV, sigma, gamma, avgMse, avgThroughput, totalRuntime, avgConsumption, pOut);
} 

template <class T>
inline void performProjectionGPRun(
		std::ofstream &pOut,
		unsigned int const numBV, 
		double const sigma, 
		double const gamma, 
        double const length,
		double const mean,
		std::string const &pPowerSaveAt) {

	ConstantMean cMean(mean);
    RBFKernel kernel(gamma,length);

    double mse[10];
    double throughput[10];
    for (unsigned int i = 0; i < 10; ++i) {
        mse[i] = 0.0;
        throughput[i] = 0.0;
    }
    double avgMse = 0.0;
    double avgThroughput = 0.0;
    double totalRuntime = 0.0;

    double avgConsumption[numNodes][RAPL_NR_DOMAIN];
    for (unsigned int i = 0; i < numNodes; ++i) {
        for (unsigned int j = 0; j < RAPL_NR_DOMAIN; ++j) {
            avgConsumption[i][j] = 0.0;
        }
    }

    std::string name = "";
 	if (std::is_same<T, GP::ProjectionGP>::value) {
 		name = "ProjectionGP";
 	} else if (std::is_same<T, GP::ApproxProjectionGP>::value) {
 		name = "ApproxProjectionGP";
 	}

  	std :: cout << "\n\nStart measurments for " 
 				<< name << " config"
    		 	<< "numBV = " << numBV << " sigma = " << sigma
    		 	<< " gamma= " << gamma << std :: endl;

    for (unsigned int k = 0; k < 10; ++k) {
        T alg(numBV, kernel, cMean, DIM, sigma);
        performXValRun(alg, NULL, numBV, k, mse, throughput, totalRuntime, avgMse,  avgThroughput, avgConsumption, pPowerSaveAt);
    }

    writeStatistics(numBV, sigma, gamma, avgMse, avgThroughput, totalRuntime, avgConsumption, pOut);
} 

int main(int argc, char * argv[]) {
    if (argc < 2) {
        std :: cout << "Please specify path to housing regression data" << std :: endl;
        return -1;
    }    

    std::string pathToFile(argv[1]);
    std::string folderToSave(argv[2]);

#ifndef __arm__
    if (0 != init_rapl()) {
        std :: cout << "Init of rapl lib failed. Librapl needs root."
                    << "Also 'modeprobe cpuid' and 'modeprobe msr' might help." 
                    << std :: endl;
        terminate_rapl();
        return -1;
    }
    numNodes = get_num_rapl_nodes_pkg();
#endif

    std :: cout << "*** STARTING HOUSING DATA Test *** " << std :: endl;
    std :: cout << std :: endl;
    std :: cout << "Current time: " << helper::getCurrentTime() << std ::endl;

    std :: cout << "Reading file: " << pathToFile << std :: endl;
    helper::readCSV(pathToFile, examples, labels, N, DIM);
    
    helper::calcXVal(10, N, indicesBegin, indicesEnd);
	helper::normalizeData(&examples[0], N, DIM);

	std::ofstream outFile(folderToSave + "/projection_gp.csv");
	std::stringstream header;
	header << "NUMBV, SIGMA, GAMMA, AVG_AMSE, AVG_THROUGHPUT[#elem/ms]";

#ifndef __arm__
	for (unsigned int i = 0; i < numNodes; ++i) {
		if(is_supported_domain(RAPL_PKG)){
            header << "," << "RAPL_PKG_" << i;
        }

        if(is_supported_domain(RAPL_PP0)) {
        	header << "," << "RAPL_PP0_" << i;
        }

        if(is_supported_domain(RAPL_PP1)) {
        	header << "," << "RAPL_PP1_" << i;
        }

        if(is_supported_domain(RAPL_DRAM)) {
        	header << "," << "RAPL_DRAM_" << i;
        };
    }
#endif

    header << ", TOTAL_RUNTIME[s]";
    outFile << header.str() << std :: endl;

    int const numBVTest[] = {50,100,200,300,400};
    double const sigmaTest[] = {0.5,1,5,10};
    double const gammaTest[] = {0.005, 0.05, 0.5, 2};
    double const lengthTest[] = {0.01, 0.5, 1, 5};

    for (unsigned int i = 0; i < 5; ++i) {
	    for (unsigned int j = 0; j < 1; ++j) {
            for (unsigned int k = 0; k < 1; ++k) {
                for (unsigned int l = 0; l < 1; ++l) {
    				std::stringstream ss;
    	        	ss << folderToSave << "/projection_gp" << numBVTest[i] << "_" << sigmaTest[j] << "_" << gammaTest[k];

    				performProjectionGPRun<GP::ProjectionGP>(
    						outFile, 
    						numBVTest[i], 
    						sigmaTest[j], 
    						gammaTest[k], 
                            lengthTest[l],
    						mean,
    						ss.str()
    				);
    				ss.clear();
    				ss.str("");
                }
	        }
	    }
	}

	outFile.close();
	outFile.clear();

	outFile.open(folderToSave + "/approx_projection_gp.csv");
	outFile << header.str() << std :: endl;

	for (unsigned int i = 0; i < 5; ++i) {
	    for (unsigned int j = 0; j < 1; ++j) {
	        for (unsigned int k = 0; k < 1; ++k) {
                for (unsigned int l = 0; l < 1; ++l) {
    	        	std::stringstream ss;
    	        	ss << folderToSave << "/approx_projection_gp" << numBVTest[i] << "_" << sigmaTest[j] << "_" << gammaTest[k];

    				performProjectionGPRun<GP::ApproxProjectionGP>(
    						outFile, 
    						numBVTest[i], 
    						sigmaTest[j], 
    						gammaTest[k], 
                            lengthTest[l],
    						mean,
    						ss.str()
    				);
    				ss.clear();
    				ss.str("");
                }
	        }
	    }
	}
	outFile.close();
	outFile.clear();

	outFile.open(folderToSave + "/full_gp.csv");
	outFile << header.str() << std :: endl;

	for (unsigned int i = 0; i < 6; ++i) {
	    for (unsigned int j = 0; j < 1; ++j) {
	        for (unsigned int k = 0; k < 1; ++k) {
                for (unsigned int l = 0; l < 1; ++l) {
    	        	std::stringstream ss;
    	        	ss << folderToSave << "/full_gp" << numBVTest[i] << "_" << sigmaTest[j] << "_" << gammaTest[k];

    	        	if (i == 5) {
    	        		//Perform full gp
    	        		performFullGP(
    						outFile, 
    						-1, 
    						sigmaTest[j], 
    						gammaTest[k], 
                            lengthTest[l],
    						mean,
    						ss.str()
    					);
    	        	} else {
    		        	performFullGP(
    							outFile, 
    							numBVTest[i], 
    							sigmaTest[j], 
    							gammaTest[k], 
                                lengthTest[l],
    							mean,
    							ss.str()
    					);
    		        }

    				ss.clear();
    				ss.str("");
                }
	        }
	    }
	}
	outFile.close();
	outFile.clear();

#ifndef __arm__
	terminate_rapl();
#endif
}

