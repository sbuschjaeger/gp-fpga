#include <iostream>
#include <chrono>
#include <omp.h>
#include <thread>

#include "Helper.h"

int load(unsigned int pLimit) {
    int num = 1, primes = 0;
    #pragma omp parallel for schedule(dynamic) reduction(+ : primes)
        for (num = 1; num <= pLimit; num++) { 
            int i = 2; 
            while(i <= num) { 
                if(num % i == 0)
                    break;
                i++; 
            }
            if(i == num){
                primes++;
            }
        }   

    return primes;
}

int main(int argc, char* argv[]) {
    if (0 != init_rapl()) {
        std :: cout << "Init of rapl lib failed. Librapl needs root. "
                    << "Also 'modeprobe cpuid' and 'modeprobe msr' might help" 
                    << std :: endl;
        terminate_rapl();
        return -1;
    }
    unsigned int numNodes = get_num_rapl_nodes_pkg();
    
    std::chrono::high_resolution_clock::time_point tStart,tEnd;

    double startConsumption[numNodes][RAPL_NR_DOMAIN];
    double endConsumption[numNodes][RAPL_NR_DOMAIN];
    /*helper::getEnergyConsumption(numNodes, startConsumption);

    std :: cout << "**** Starting energy consumption test on all CPUs **** " << std :: endl;
    std :: cout << std :: endl;
    std :: cout << "Current time: " << helper::getCurrentTime() << std ::endl;
    tStart = std::chrono::high_resolution_clock::now();
    int primes = load(200);
    
    tEnd = std::chrono::high_resolution_clock::now();
    helper::getEnergyConsumption(numNodes, endConsumption);
    double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( tEnd - tStart ).count();

     for (unsigned int i = 0; i < numNodes; ++i) {
        if(is_supported_domain(RAPL_PKG)){
            std :: cout << "Total PKG energy [node " << i << "]: " 
                        << (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG])
                        << " J - " 
                        << (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG])  / (runtime / 1000.0) 
                        << " W" << std :: endl;
        }

        if(is_supported_domain(RAPL_PP0)) {
            std :: cout << "Total PP0 energy [node " << i << "]: " 
                        << (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0])
                        << " J - " 
                        << (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0])  / (runtime / 1000.0)  
                        << " W" << std :: endl;
        }

        if(is_supported_domain(RAPL_PP1)){
            std :: cout << "Total PP1 energy [node " << i << "]: " 
                        << (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1])
                        << " J - " 
                        << (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1])  / (runtime / 1000.0)  
                        << " W" << std :: endl;
        }

        if(is_supported_domain(RAPL_DRAM)){
            std :: cout << "Total DRAM energy [node " << i << "]: " 
                        << (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM])
                        << " J - " 
                        << (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM])  / (runtime / 1000.0)  
                        << " W" << std :: endl;
        }
    }

    std :: cout << "\nOverall runtime: " << runtime / 1000.0 << " seconds" << std :: endl;
*/
    std :: cout << "Get IDLE consumption" << std :: endl;
    tStart = std::chrono::high_resolution_clock::now();
    helper::getEnergyConsumption(numNodes, startConsumption);
    std::this_thread::sleep_for(std::chrono::seconds(5));
    
    tEnd = std::chrono::high_resolution_clock::now();
    helper::getEnergyConsumption(numNodes, endConsumption);
    double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( tEnd - tStart ).count();

    for (unsigned int i = 0; i < numNodes; ++i) {
        if(is_supported_domain(RAPL_PKG)){
            std :: cout << "Total PKG energy [node " << i << "]: " 
                        << (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG])
                        << " J - " 
                        << (endConsumption[i][RAPL_PKG] - startConsumption[i][RAPL_PKG])  / (runtime / 1000.0) 
                        << " W" << std :: endl;
        }

        if(is_supported_domain(RAPL_PP0)) {
            std :: cout << "Total PP0 energy [node " << i << "]: " 
                        << (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0])
                        << " J - " 
                        << (endConsumption[i][RAPL_PP0] - startConsumption[i][RAPL_PP0])  / (runtime / 1000.0)  
                        << " W" << std :: endl;
        }

        if(is_supported_domain(RAPL_PP1)){
            std :: cout << "Total PP1 energy [node " << i << "]: " 
                        << (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1])
                        << " J - " 
                        << (endConsumption[i][RAPL_PP1] - startConsumption[i][RAPL_PP1])  / (runtime / 1000.0)  
                        << " W" << std :: endl;
        }

        if(is_supported_domain(RAPL_DRAM)){
            std :: cout << "Total DRAM energy [node " << i << "]: " 
                        << (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM])
                        << " J - " 
                        << (endConsumption[i][RAPL_DRAM] - startConsumption[i][RAPL_DRAM])  / (runtime / 1000.0)  
                        << " W" << std :: endl;
        }
    }


    std :: cout << "*** END Energy Test *** " << std :: endl;
    std :: cout << std :: endl;

    terminate_rapl();
}

