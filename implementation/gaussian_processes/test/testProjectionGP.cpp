#include <iostream>

#include "Helper.h"
#include "ProjectionGP.h"

int main(int argc, char * argv[]) {
    const unsigned int DIM = 256;
    const unsigned int NTrain = 100000;
    const unsigned int NTest = 5000;
    
    std::vector<double> XTrain;
    std::vector<double> YTrain;
    std::vector<double> XTest;
    std::vector<double> YTest;

    helper::generateData(DIM,NTrain,NTest,XTrain,YTrain,XTest,YTest);

    const double length = 1;
    const double gamma = 0.5;
    const unsigned int numBV = 64;
    
    ConstantMean cMean(0);
    RBFKernel kernel(gamma,length);
    GP::ProjectionGP gp(numBV, kernel, cMean, DIM, 5);

    for (unsigned int i = 0; i < NTrain; ++i) {
        gp.train(&XTrain[i*DIM], YTrain[i]);
    }

    double error = 0;
    for (unsigned int i = 0; i < NTest; ++i) {
        double p = gp.predict(&XTest[i*DIM]);
        error += std::abs(p - YTest[i]);
    }

    std :: cout << "Error: " << error << std :: endl;
}

