#include <iostream>
#include <fstream>
#include <random>
#include <chrono>

#include <boost/asio.hpp>
#include <boost/static_assert.hpp>

#include "Helper.h"
#include "FPGA_GP.h"

int main(int argc, char* argv[]) {
    unsigned int N;
    unsigned int const dim = 21;

    std::vector<double> examples;
    std::vector<double> labels;

    helper::readCSV("../../../experimente/data/sarcos_inv_train.csv", examples, labels, N, dim);
    N = 500;

    GP::FPGA_GP gp("192.168.128.50", "5858", dim);

    std::chrono::high_resolution_clock::time_point t1,t2;
    t1 = std::chrono::high_resolution_clock::now();
    
    for (unsigned int  i = 0; i < N; ++i) {
        try {
            gp.train(&examples[i*dim],labels[i]);
        } catch (GP::FPGA_GP_Exception &e) {
            std :: cout << e.what() << std :: endl;
        }
    }
    t2 = std::chrono::high_resolution_clock::now();
    double runtime = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();

    std :: cout << "Training is over. Runtime for " << N << " examples was: " << runtime << std :: endl;
    std :: cout << "Throughput was: " << (double) (N/runtime)*1000 << std :: endl;

    // TODO Gather Predictions here
}