#include <iostream>
#include <random>

#include <boost/asio.hpp>
#include <boost/static_assert.hpp>

#include "../include/Helper.h"

#include "../../hls/DummyAlgorithm/include/DummyAlgorithm_config.h"

#define alpha 0.1

float weights[dim + 1] = {0};
unsigned int const payloadLength = (dim + 1)*sizeof(float)+1;

float predict(float const pX[dim]) {
    float sum = 0;
    for (unsigned int i = 0; i < dim; ++i) {
        sum += pX[i]*weights[i];
    }
    sum += weights[dim]; 
    
    return sum >= 0 ? 1 : -1;
}

void train(float const pX[dim], float const pY) {
    float prediction = predict(pX);
    
    for (unsigned int i = 0; i < dim; ++i) {
        weights[i] += alpha*(pY - prediction)*pX[i];
    }
    
    weights[dim] += alpha*(pY - prediction);
}

int main(int argc, char* argv[]) {
    // Just make sure this runs on "normal" system, 
    // where one byte is 8 bit long and one float is 4 byte
    BOOST_STATIC_ASSERT(CHAR_BIT == 8);
    BOOST_STATIC_ASSERT(sizeof(float) * CHAR_BIT == 32);

    boost::asio::io_service io_service;
    boost::asio::ip::tcp::acceptor acceptor(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 5858));
    boost::asio::ip::tcp::socket socket(io_service);
    acceptor.accept(socket);

    bool done = false;
    while( !done ) {
        char data[payloadLength];
        unsigned int dataLength = boost::asio::read(socket, boost::asio::buffer(&data, payloadLength));

        if (payloadLength != dataLength) {
            std :: cout << "Received ill-sized package, ignoring!" << std :: endl;
        } else {
            char header = data[0];
            
            if (header == 1) {
                done = true;
            } else {
                //float x[dim];
                float *x;
                float y;

                x = reinterpret_cast<float *> (&data[1]);
                y = *(reinterpret_cast<float *> (&data[payloadLength - sizeof(float)]));
                train(x,y);
            }
        }
    }
}