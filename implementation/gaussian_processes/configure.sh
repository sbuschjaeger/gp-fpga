#!/bin/bash

if [[ -z "$S1" && $1 == "clean" ]] ; then
	echo "Cleaning up"
	rm -R build;
else
	echo "Creating build files"
	
	if [ ! -d "build" ]; then
		mkdir build;
	fi

	cd build;
	cmake -DCMAKE_BUILD_TYPE=Debug ..;
fi;