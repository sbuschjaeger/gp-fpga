#ifndef FPGA_GP_H
#define FPGA_GP_H

#include <iostream>

#include <boost/asio.hpp>
#include <boost/static_assert.hpp>
#include "OnlineAlgorithm.h"

namespace GP {

/**
 * This class implements the connection between a host PC and the FPGA.
 * A simple TCP connection between the FPGA and host PC is established. 
 * Each training example (x,y) is send as a single TCP package looking like this. 
 * Every float needs 4 bytes. Thus they are deconstructed in the following way:
 * XXXX XXXX | x_11 x_12 x_13 x_14 | x_21 x_22 x_23 x_24 | .... | y_1 y_2 y_3 y_4
 * 
 * The first byte denotes a simple configuration field. For details see the code.
 * The maximum size of a package is given by payloadLength
 */
class FPGA_GP : public OnlineAlgorithm {
private:
	// The socket
	boost::asio::ip::tcp::socket *s;

	// The dimension of the observations
	unsigned int const dim;

	// The total payload length
	unsigned int const payloadLength;

	/**
	 * @brief Constructs a data package for given example (x,y)
	 * @details Constructs a data package for given example (x,y) and sets the configuration
	 *          field according to the pPredict flag. 
	 * 
	 * @param pX The observation as simple vector with dim entries
	 * @param pY The label. If pPredict == true, this parameter is ignored
	 * @param pPredict If true, the FPGA will perform a prediction. If false, it will perform training
	 * @return A pointer to the corresponding data package.
	 */
	char* buildDataPackage(double const * const pX, double const pY, bool pPredict) const;	

public:
	/**
	 * @brief Standard c'tor.
	 * @details Standard c'tor.
	 * 
	 * @param pAddress The ip address of the FPGA as string.
	 * @param pPort The port of the FPGA as string
	 * @param pDim The dimension of the examples which are going to be send to the FPGA.
	 */
	FPGA_GP(std::string const &pAddress, std::string const &pPort, unsigned int pDim);

	/**
	 * @brief Standard d'tor.
	 * @details Standard d'tor.
	 */
	~FPGA_GP();

	/**
	 * @brief Sends the given observation pX and label to the FPGA for training.
	 * @details Creates the appropriate data package for the given observation and label and sends this to the FPGA.
	 *          This call blocks until a reply is received from the FPGA. If an invalid reply is received, an
	 *          exception is thrown.
	 * 
	 * @param pX The oberservation
	 * @param pY The label
	 */
	virtual void train(double const * const pX, double const pY);

	/**
	 * @brief Sends the given observation pX and label to the FPGA for prediction.
	 * @details Creates the appropriate data package for the given observation and sends this to the FPGA.
	 *          This call blocks until a reply is received from the FPGA. If an invalid reply is received, an
	 *          exception is thrown.
	 * 
	 * @param pX The oberservation
	 */
	virtual double predict(double const * const pX);
};

/**
 * A simple exception class indicating exceptions during connection with the FPGA.
 */
class FPGA_GP_Exception: public std::exception {
	private:
		// The exception code given by the FPGA. 
		// Please look at the FPGA source code for more information on these exception codes
		unsigned int code;

	public:
		/**
		 * @brief Standard C'tor
		 * @details Standard C'tor
		 * 
		 * @param pCode The exception code.
		 */
		FPGA_GP_Exception(unsigned int pCode) : code(pCode) {};
		
		/**
		 * @brief Gives a string representation of this exception
		 * @details Gives a string representation of this exception
		 * 
		 * @return The string representation of this exception
		 */
		virtual const char* what() const throw() {
			std :: stringstream ss;
			ss << "The FPGA returned an error " << code;
			return ss.str().c_str();
		}
};

};

#endif
