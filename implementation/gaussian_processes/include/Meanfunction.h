#ifndef MEAN_FUNCTION_H
#define MEAN_FUNCTION_H

/**
 * A simple interface for implementing mean functions
 */
class MeanFunction {
private:
public:
	/**
	 * @brief Standard virtual d'tor
	 * @details Standard virtual d'tor
	 */
	virtual ~MeanFunction() {};
	
	/**
	 * @brief The mean function to be computed
	 * @details The mean function to be computed
	 * 
	 * @param pX1 The obversavtion for which the mean function should be computed
	 * @param pDim The dimension of the given observation
	 * 
	 * @return The mean value at the given observation
	 */
	virtual double mean(double const * const pX1, unsigned int const pDim) const = 0;
};

/**
 * A simple constant mean class returning the same constant value for every
 * observation.
 * 
 */
class ConstantMean : public MeanFunction {
private:
	// The constant mean
	double constant;

public:
	/**
	 * @brief Standard c'tor
	 * @details Standard c'tor
	 * 
	 * @param pConstant The constant mean
	 */
    ConstantMean(double pConstant) : constant(pConstant) {}

    /**
     * @brief Standard d'tor
     * @details Standard d'tor
     */
    ~ConstantMean() {};

    /**
     * @brief The constant mean function returning a constant value for every possible observation
     * @details The constant mean function returning a constant value for every possible observation
     * 
     * @param pX1 The observation
     * @param pDim The dimension of the observation
     * 
     * @return The constant value given in the c'tor regardles of pX1
     */
    virtual inline double mean(double const * const pX1, unsigned int const pDim) const {
        return constant;
    }
};
#endif