#include "ApproxProjectionGP.h"

namespace GP {

ApproxProjectionGP::ApproxProjectionGP(	unsigned int pNumBV,
                            Kernel const  &pInputKernel,
                            MeanFunction const &pMeanFunction,
                            unsigned int pDim,
                            double pSigma2N
                          ) :
                            numBV(pNumBV),
                            bvMax(pNumBV + 1),
                            bvCnt(0),
                            inputKernel(pInputKernel),
                            meanFunction(pMeanFunction),
                            basisVectors(new double const*[pNumBV + 1]),
                            dim(pDim),
                            C(new double[(pNumBV + 1)*(pNumBV + 1)]),
                            Q(new double[(pNumBV + 1)*(pNumBV + 1)]),
                            e(new double[pNumBV + 1]),
                            k(new double[pNumBV]),
                            s(new double[pNumBV + 1]),
                            alpha(new double[pNumBV + 1]),
                            sigma2_n(pSigma2N)
{

    for (unsigned int i = 0; i < numBV; ++i) {
        alpha[i] = 0.0;
        e[i] = 0.0;
        s[i] = 0.0;
        k[i] = 0.0;
    }

    e[numBV] = 0.0;
    s[numBV] = 0.0;
    alpha[numBV] = 0.0;

    for (unsigned int i = 0; i < bvMax; ++i) {
        for (unsigned int j = 0; j < bvMax; ++j) {
           if (i == j) {
                C[i*bvMax + j] = pSigma2N;
                Q[i*bvMax + j] = 1.0;
            } else {
                C[i*bvMax + j] = 0.0;
                Q[i*bvMax + j] = 0.0;
            }
        }
    }
}

ApproxProjectionGP::~ApproxProjectionGP() {
    delete[] basisVectors;
    delete[] C;
    delete[] Q;
    delete[] e;
    delete[] k;
    delete[] s;
    delete[] alpha;
}

inline void ApproxProjectionGP :: swapRowAndColumn(double * const pM, unsigned int const rowA , unsigned int const rowB) {
    for (unsigned int i = 0; i < bvMax; ++i) {
        //switch row elements
        double temp = pM[rowA*bvMax+i];
        pM[rowA*bvMax+i] = pM[rowB*bvMax+i];
        pM[rowB*bvMax+i] = temp;
    }

    for (unsigned int i = 0; i < bvMax; ++i) {
        // switch column elements
        double temp = pM[i*bvMax+rowA];
        pM[i*bvMax+rowA] = pM[i*bvMax+rowB];
        pM[i*bvMax+rowB] = temp;
    }
}
void ApproxProjectionGP :: deleteBV(unsigned int const pIndex) {
    /* Copy element to last position in alpha and C/Q */

    // Swap alpha
    double temp = alpha[pIndex];
    alpha[pIndex] = alpha[numBV];
    alpha[numBV] = temp;

    swapRowAndColumn(C, pIndex, numBV);
    swapRowAndColumn(Q, pIndex, numBV);

    double const * bv = basisVectors[pIndex];
    basisVectors[pIndex] = basisVectors[numBV];
    basisVectors[numBV] = bv;

    double alphaStar = alpha[numBV];
    double cStar = C[numBV*bvMax+numBV];
    double qStar = Q[numBV*bvMax+numBV];

    for (unsigned int i = 0; i < numBV; ++i) {
        alpha[i] -= (C[numBV*bvMax+i]+Q[numBV*bvMax+i])*alphaStar/(cStar+qStar);
    }
    alpha[numBV] = 0;

    for(unsigned int i = 0; i < numBV; ++i) {
        for (unsigned int j = 0; j < numBV; ++j) {
            double temp = Q[numBV*bvMax+i]*Q[numBV*bvMax+j]*1.0/qStar;

            C[i*bvMax+j] += temp;
            C[i*bvMax+j] -= (C[numBV*bvMax+i]*C[numBV*bvMax+j] +
                             C[numBV*bvMax+i]*Q[numBV*bvMax+j] +
                             Q[numBV*bvMax+i]*C[numBV*bvMax+j] +
                             Q[numBV*bvMax+i]*Q[numBV*bvMax+j]) * 1.0/ (cStar+qStar);

            Q[i*bvMax+j] -= temp;
        }
    }

    for (unsigned int i = 0; i <= numBV; ++i) {
        Q[i*bvMax+numBV] = 0;
        Q[numBV*bvMax+i] = 0;
        C[i*bvMax+numBV] = 0;
        C[numBV*bvMax+i] = 0;
    }
}

unsigned int ApproxProjectionGP :: getMinKLApprox() const {
    double scores[bvMax];
    for (unsigned int i = 0; i < bvMax; ++i) {
        scores[i] = alpha[i]*alpha[i] / ( Q[i*bvMax+i] + C[i*bvMax+i] ); 
    }

    int index = 0;
    double minScore = scores[0];
    for (unsigned int i = 1; i < bvMax; ++i) {
        if (scores[i] < minScore) {
            minScore = scores[i];
            index = i;
        }
    }

    return index;
}

void ApproxProjectionGP :: train_full_bv_set(double const * const pX, double const pY) {
    // Compute kernel to every basis vector
    // k = ( k(x1, pX), k(x2, pX), ...,  k(xN, pX))
    for (unsigned int i = 0; i < numBV; ++i) {
        k[i] = inputKernel.K(basisVectors[i], pX, dim);
    }
    double kStar = inputKernel.K(pX,pX,dim);

    // Compute mean prediction using alpha
    double m = 0.0;
    for (unsigned int i = 0; i < numBV; ++i) {
        m += k[i]*alpha[i];
    }
    m += meanFunction.mean(pX, dim);

    // Compute variance and update s accordingly
    double sigma2 = kStar;
    for (unsigned int i = 0; i < numBV; ++i) {
        double sum = 0.0;
        for (unsigned int j = 0; j < numBV; ++j) {
            sum += C[i*bvMax+j]*k[j];
        }

        s[i] = sum;
        sigma2 += sum*k[i];
    }
    s[numBV] = 1;

    double q = (pY - m) / (sigma2 + sigma2_n);
    double r = - 1.0/(sigma2_n + sigma2);
    double gamma = kStar;

    for (unsigned int i = 0; i < numBV; ++i) {
        double sum = 0.0;
        for (unsigned int j = 0; j < numBV; ++j) {
            sum += Q[i*bvMax+j]*k[j];
        }

        e[i] = sum;
        //gamma -= sum*k[i];
        alpha[i] += s[i]*q;
    }

    for (unsigned int i = 0; i < numBV; ++i) {
        gamma -= e[i]*k[i];
    }

    alpha[numBV] += s[numBV]*q;

    // Update Q = Q + t*t^T*gamma^-1 with t = e - u
    for (unsigned int i = 0; i <= numBV; ++i) {
        for (unsigned int j = 0; j <= numBV; ++j) {
            // Update C = C + s*s^T*r
            C[i*bvMax+j] += s[i]*s[j]*r;

            double ti = i == numBV ? -1 : e[i];
            double tj = j == numBV ? -1 : e[j];

            Q[i*bvMax+j] += (ti)*(tj)*1.0/gamma;
        }
    }

    basisVectors[numBV] = pX;
    unsigned int index = getMinKLApprox();
    deleteBV( index );
}

void ApproxProjectionGP :: train(double const * const pX, double const pY) {
    if (bvCnt == numBV) {
        train_full_bv_set(pX,pY);
    } else {
        basisVectors[bvCnt++] = pX;
    }
}

double ApproxProjectionGP::predict(double const * const pX) {
    double sum = 0.0;
    for (unsigned int i = 0; i < numBV; ++i) {
        sum += inputKernel.K(basisVectors[i],pX, dim)*alpha[i];
    }

    return sum+meanFunction.mean(pX,dim);
}

}
