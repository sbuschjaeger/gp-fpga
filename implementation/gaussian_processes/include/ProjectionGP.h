#ifndef PROJECTION_GP_H
#define PROJECTION_GP_H

#include <algorithm>

#include "OnlineAlgorithm.h"
#include "MatrixUtils.h"
#include "Helper.h"
#include "Kernel.h"
#include "Meanfunction.h"

namespace GP {

/**
 * This class computes ProjectionGP based on the thesis:
 * 		- "Gaussian Processes - Iterative Sparse Approximations" 
 * 		  by Lehel Csato (http://www.cs.ubbcluj.ro/~csatol/publications/thesis.pdf)
 * 		- "Sparse Online Gaussian Processes" 
 * 		  by Lehel Csato and Manfred Opper (http://core.ac.uk/download/pdf/8529.pdf)
 * 
 * Note: This implementation does not use the novelty parameter gamma_{t} presented in
 * 		 the thsis. Every example can be a basis vector. 
 * 		 
 * Vectors are supposed to be saved as simple arrays with size numBV or bvMax. Details see
 * in the *.cpp file.
 * 
 * Matrices are supposed to be saved as simple array with the following layout:
 *  [x11 x12 ... x1DIM
 *   x21 x22 ... x2DIM
 *    		 ...
 *   xN1 xN2 ... xNDIM ] --> [x11 x12 ... x1DIM x21 x22 ... x2DIM ... xN1 xN2 ... xNDIM] 
 * 	Details on the sizes N and DIM can be found in the *.cpp file.
 */

class ProjectionGP : public OnlineAlgorithm {
private:

	// Number of basis vector
	unsigned int const numBV;

	// Number of basis vectors + 1 
	 unsigned int const bvMax;

	// Current number of basis vectors used
	// Increments during the algorithm until it reaches numBV
	unsigned int bvCnt;

	// Kernel function
	Kernel const &inputKernel;

	// Mean function
	MeanFunction const &meanFunction;

	// Basis vectors. Note: We save the basis vectors as pointer, thus the
	// calling function has to make sure that the memory is still valid
	double const * * const basisVectors;

	// Dimension of basis vectors / oberservation
	unsigned int const dim;

	// Covariance Matrix of GP. 
	// Note: 	This is a (bvMax x bvMax) Matrix, where the last column and last row 
	// 			is temporarily used. Therefore the last row / column contains only 
	// 			0 after an update.  This matrix is symmetric
	double * const C;

	// Inverse Kernel Matrix. 
	// Note: 	This is a (bvMax x bvMax) Matrix, where the last column and last row 
	// 			is temporarily used. Therefore the last row / column contains only 
	// 			0 after an update. This matrixis symmetric
	double * const Q;

	// Temporary vector with bvMax entries. Contains the direction in which Q needs to
	// be updated in the current step.
	double * const e;

	// Temp vector of kernel functions between new input x* and basis vectors 
	// {x1,x2,...,xNumBV) ==> k = [k(x1,x*) k(x2,x*) ... k(xNumBV, x*)] 
	double * const k;

	// Temp vector with bvMax entries. Contains the direction in which C needs to be
	// updated in the current step.
	double * const s;

	// Parameter vector of prediction
	double * const alpha;

	// Noise parameter of GP
	const double sigma2_n;

	/**
	 * @brief	Deletes the basis vector at the given position
	 * @details Deletes the basis vector at the given position from
	 * 			the set of basis vectors by:
	 * 				- swapping the last basis vector and the basis vector at the given position 
	 * 				- computing the KL minimal projection
	 * 				- setting the last entries (row / column) in alpha, Q and C to 0
	 * @param pIndex the index of the basis vector to be deleted. Between 0 and numBV (inclusive)
	 */
	void deleteBV(unsigned int pIndex);

	/**
	 * @brief Returns the index i of that basis vector x_i with the minimal KL-divergence 
	 * 
	 * @details Returns the index i of that basis vector x_i which contributes the
	 * least information into C and alpha, meaning that the KL-divergence is
	 * minimized between C and alpha containing x_i and containing zero at
	 * that point.
	 * Since the KL-divergence is hard to compute, an approximation is used here.
	 * @return That index i which should be deleted from the set of basis vectors
	 */
	unsigned int getMinKLApprox();

	/**
	 * @brief Returns the index i of that basis vector x_i with the minimal KL-divergence 
	 * 
	 * @details Returns the index i of that basis vector x_i which contributes the
	 * least information into C and alpha, meaning that the KL-divergence is
	 * minimized between C and alpha containing x_i and containing zero at
	 * that point. This function uses the real, non approximated KL-divergence.
	 * @return That index i which should be deleted from the set of basis vectors
	 */
	unsigned int getMinKL();

	/**
	 * @brief Swaps the row i and column i with row and column j in the given matrix
	 * @details Swaps the given row and column i with the given row and column in the given
	 * 			matrix pM. pM is expected to be saved in an one dimensional array as 
	 * 			described before.
	 * 
	 * @param pM The matrix
	 * @param i The first row/column
	 * @param j The second row/column
	 */
	inline void swapRowAndColumn(double * const pM, unsigned int i , unsigned int j);
public:
	/**
	 * @brief Standard c'tor of this class
	 * @details Standard c'tor of this class
	 * 
	 * @param pNumBV The number of basis vectors
	 * @param pInputKernel The kernel to be used
	 * @param pMeanFunction The mean function to me used
	 * @param pDim The dimension of the given examples.
	 * @param pSigma2N The noise parameter to be used
	 */
	ProjectionGP(	unsigned int pNumBV,
					Kernel const &pInputKernel,
					MeanFunction const &pMeanFunction,
					unsigned int pDim,
					double pSigma2N
				);

	/**
	 * @brief Standard d'tor.
	 * @details Standard d'tor.
	 */
	~ProjectionGP();

	/**
	 * @brief Updates the model with the given observation / label.
	 * @details Updates the model parameters and the set of basis vectors according to the given observation.
	 * 
	 * @param pX The observation
	 * @param pY The label
	 */
	virtual void train(double const * const pX, double const pY);

	/**
	 * @brief Performs a prediction for the given observation.
	 * @details Performs a prediction for the given observation.
	 * 
	 * @param pX The given observation
	 * @return The predicted label
	 */
	virtual double predict(double const * const pX);
};

}

#endif