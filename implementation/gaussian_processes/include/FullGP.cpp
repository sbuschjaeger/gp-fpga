#include "FullGP.h"

namespace GP {
FullGP::FullGP(	const unsigned int pN, 
			const unsigned int pDim, 
			double pSigma,
			Kernel const &pInputKernel,
			MeanFunction const &pMeanFunction
		) :
			dim(pDim),
			N(pN),
			sigma(pSigma),
			inputKernel(pInputKernel),
			meanFunction(pMeanFunction),
			examples(new double const*[pN]),
			Y(new double[pN]),
			cnt(0),
			K(new double[pN*pN]),
			KInv(new double[pN*pN]),
			alpha(new double[pN])
			{  }


FullGP::~FullGP() {
	// Cleanup temporary arrays
	delete [] examples;
	delete [] Y;
	delete [] K;
	delete [] KInv;
	delete [] alpha;
}

void FullGP::train(double const * const pX, double const pY) {
	if (cnt < N) {
		examples[cnt] = pX;
		Y[cnt++] = pY - meanFunction.mean(pX,dim);

		if (cnt == N) {
			// Compute the kernel matrix
			for (unsigned int i = 0; i < N; ++i) {
				for (unsigned int j = i; j < N; ++j) {
					double tSigma = 0.0;
					if (i == j) {
						tSigma = sigma;
					}

					K[i*N+j] = inputKernel.K(examples[i], examples[j], dim) + tSigma;
					K[j*N+i] = K[i*N+j];
				}
			}

			// Invert the kernel matrix and calculate alpha
			mat::invertWithGauss(KInv, K, N);
			mat::mult(alpha, Y, KInv, N, N);
		}
	}
}

double FullGP::predict(double const * const pX) {
	// Just do the standard prediction
	double sum = 0.0;
	for (unsigned int i = 0; i < N; ++i) {
		sum += inputKernel.K(examples[i],pX,dim)*alpha[i];
	}

	return sum + meanFunction.mean(pX,dim);
}
}