#ifndef FULL_GP_H
#define FULL_GP_H

#include <algorithm>

#include "OnlineAlgorithm.h"
#include "MatrixUtils.h"
#include "Helper.h"
#include "Kernel.h"
#include "Meanfunction.h"

namespace GP {

/**
 * A simple implementation of a full gaussian process without any parameter optimization.
 * The mean is given by the mean function.
 *
 * Note: The full GP is not a real online algorithm, but for the sake of a nice interface 
 * we will use the OnlineAlgorithm interface here. This does induce some overhead, since
 * the train() function can be just called once for every data point.
 * 
 * Vectors are supposed to be saved as simple arrays with size numBV or bvMax. Details see
 * in the *.cpp file.
 * 
 * Matrices are supposed to be saved as simple array with the following layout:
 *  [x11 x12 ... x1DIM
 *   x21 x22 ... x2DIM
 *    		 ...
 *   xN1 xN2 ... xNDIM ] --> [x11 x12 ... x1DIM x21 x22 ... x2DIM ... xN1 xN2 ... xNDIM] 
 * 	Details on the sizes N and DIM can be found in the *.cpp file.
 */
class FullGP : public OnlineAlgorithm {
private:
	/* Constant values */

	// Dimension of exmaples
	const unsigned int dim;

	// Number of examples
	const unsigned int N;
	
	double const sigma;

	// The kernel function used by this FGP.
	Kernel const &inputKernel;

	// The mean function used by this FGP
	MeanFunction const &meanFunction;

	/* Pointer to the exmaple matrix as array.
	 */
	double const * * const examples;

	// The assosiated arrays for the given examples in the example matrix
	// Y[0] corresponds to  the first row in examples
	// Y[1] corresponds to  the seconds row in examples etc.
	double * const Y;
	
	/* Temporary values */
	unsigned int cnt;

	// The kernel matrix as single dimensional array, thus an array with N*N entries 
	double * const K;

	// The inverse kernel matrix as single dimensional array, thus an array with N*N entries
	double * const KInv;

	// The alpha vector used for prediction in this FGP. N entries
	double * const alpha;



public:
	/**
	 * @brief Standard c'tor
	 * @details Standard c'tor
	 * 
	 * @param pN The number of examples 
	 * @param pDim The dimension of examples
	 * @param pSigma The noise parameter
	 * @param pInputKernel The input kernel
	 * @param pMeanFunction The mean function
	 */
	FullGP(		unsigned int const pN, 
				unsigned int const pDim, 
				double const pSigma,
				Kernel const &pInputKernel,
				MeanFunction const &pMeanFunction
			);

	/**
	 * @brief Standard d'tor
	 * @details Standard d'tor
	 */
	~FullGP();

	/**
	 * @brief Updates the model with the given observation / label.
	 * @details Updates the model parameters and the set of basis vectors according to the given observation.
	 * 
	 * @param pX The observation
	 * @param pY The label
	 */
	virtual void train(double const * const pX, double const pY);

	/**
	 * @brief Performs a prediction for the given observation.
	 * @details Performs a prediction for the given observation.
	 * 
	 * @param pX The given observation
	 * @return The predicted label
	 */
	virtual double predict(double const * const pX);
};

}

#endif