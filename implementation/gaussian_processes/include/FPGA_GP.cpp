#include "FPGA_GP.h"

namespace GP {

FPGA_GP::FPGA_GP(std::string const &pAddress, std::string const &pPort, unsigned int pDim)
: dim(pDim), payloadLength((pDim + 1)*sizeof(float) + 1) {
	// Just make sure this runs on "normal" system, 
	// where one byte is 8 bit long and one float is 4 byte
	BOOST_STATIC_ASSERT(CHAR_BIT == 8);
	BOOST_STATIC_ASSERT(sizeof(float) * CHAR_BIT == 32);

    boost::asio::io_service io_service;

    boost::asio::ip::tcp::resolver resolver(io_service);
    boost::asio::ip::tcp::resolver::query query(boost::asio::ip::tcp::v4(), pAddress, pPort);
    boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);

    s = new boost::asio::ip::tcp::socket(io_service);
    boost::asio::connect((*s), iterator);
}

FPGA_GP::~FPGA_GP() {
	delete s;
}

char* FPGA_GP::buildDataPackage(double const * const pX, double const pY, bool pPredict) const {
	char *data = new char[payloadLength];

	// First byte of data is protocol specific. We just look at the LSB
	// If this is set to 0, we are in train mode
	// If this is set to 1, we are in prediction mode
	data[0] = pPredict;

	unsigned int cnt = 1;
	for (unsigned int i = 0; i < dim; ++i) {
		float f = pX[i];
		unsigned char const * xAsChar = reinterpret_cast<unsigned char const *>(&f);
		for (unsigned int j = 0; j < sizeof(float); ++j) {
			data[cnt++] = xAsChar[j];
		}
	}

	if (!pPredict) {
		float f = pY;
		unsigned char const * yAsChar = reinterpret_cast<unsigned char const *>(&f);
		for (unsigned int j = 0; j < sizeof(float); ++j) {
			data[cnt++] = yAsChar[j];
		}
	}

	return data;
}

void FPGA_GP::train(double const * const pX, double const pY) {
	char *data = buildDataPackage(pX,pY,false);

	std :: cout << "sending data" << std :: endl;
	/*for (unsigned int i = 0; i < payloadLength; ++i) {
		std :: cout << std :: hex << (unsigned int) data[i];
		std :: cout << " ";
	}
	std :: cout << std :: endl;*/

	// Send the data
	boost::asio::write((*s), boost::asio::buffer(data, payloadLength));
    delete [] data;

	// Wait for FPGA to process package
	char reply;
    unsigned int replyLength = boost::asio::read((*s), boost::asio::buffer(&reply, 1));

    if (replyLength != 1) {
    	// ERROR
    	throw FPGA_GP_Exception((unsigned int) reply);
    } else {
    	// check if and only if first bit is set to 1 (rest 0)
    	// thus reply == 0000 0001
    	if (reply != 1) {
    		throw FPGA_GP_Exception((unsigned int) reply);
    	}
    }

}

double FPGA_GP::predict(double const * const pX) {
	char *data = buildDataPackage(pX,0,true);

	// Send the data
	boost::asio::write((*s), boost::asio::buffer(data, payloadLength));
	delete[] data;

	// Wait for FPGA to process package
	char reply[5];
    unsigned int replyLength = boost::asio::read((*s), boost::asio::buffer(&reply, 5));

    if (replyLength != 5) {
    	// ERROR
    	throw FPGA_GP_Exception((unsigned int) reply[0]);
    } else {
    	// check if and only if first bit is set to 1 (rest 0)
    	// thus reply == 0000 0001
    	if (reply[0] != 1) {
    		throw FPGA_GP_Exception((unsigned int) reply[0]);
    	}

    	float *ret = reinterpret_cast<float*>(&reply[1]);
    	return (double)(*ret);
    }
}

}