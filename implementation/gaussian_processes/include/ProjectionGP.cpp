#include "ProjectionGP.h"

namespace GP {

ProjectionGP::ProjectionGP(	unsigned int pNumBV,
                            Kernel const  &pInputKernel,
                            MeanFunction const &pMeanFunction,
                            unsigned int pDim,
                            double pSigma2N
                          ) :
                            numBV(pNumBV),
                            bvMax(pNumBV + 1),
                            bvCnt(0),
                            inputKernel(pInputKernel),
                            meanFunction(pMeanFunction),
                            basisVectors(new double const*[pNumBV + 1]),
                            dim(pDim),
                            C(new double[(pNumBV + 1)*(pNumBV + 1)]),
                            Q(new double[(pNumBV + 1)*(pNumBV + 1)]),
                            e(new double[pNumBV + 1]),
                            k(new double[pNumBV]),
                            s(new double[pNumBV + 1]),
                            alpha(new double[pNumBV + 1]),
                            sigma2_n(pSigma2N)
{

    for (unsigned int i = 0; i < numBV; ++i) {
        alpha[i] = 0.0;
        e[i] = 0.0;
        s[i] = 0.0;
        k[i] = 0.0;
    }

    e[numBV] = 0.0;
    s[numBV] = 0.0;
    alpha[numBV] = 0.0;

    for (unsigned int i = 0; i < bvMax; ++i) {
        for (unsigned int j = 0; j < bvMax; ++j) {
            C[i*bvMax + j] = 0.0;
            Q[i*bvMax + j] = 0.0;
        }
    }
}

ProjectionGP::~ProjectionGP() {
    delete[] basisVectors;
    delete[] C;
    delete[] Q;
    delete[] e;
    delete[] k;
    delete[] s;
    delete[] alpha;
}

inline void ProjectionGP :: swapRowAndColumn(double * const pM, unsigned int rowA , unsigned int rowB) {
    for (unsigned int i = 0; i < bvMax; ++i) {
        //switch row elements
        double temp = pM[rowA*bvMax+i];
        pM[rowA*bvMax+i] = pM[rowB*bvMax+i];
        pM[rowB*bvMax+i] = temp;
    }

    for (unsigned int i = 0; i < bvMax; ++i) {
        // switch column elements
        double temp = pM[i*bvMax+rowA];
        pM[i*bvMax+rowA] = pM[i*bvMax+rowB];
        pM[i*bvMax+rowB] = temp;
    }
}

void ProjectionGP :: deleteBV(unsigned int pIndex) {
    /* Copy element to last position in alpha and C/Q */

    unsigned int tD = bvCnt - 1;

    // Swap alpha
    double temp = alpha[pIndex];
    alpha[pIndex] = alpha[tD];
    alpha[tD] = temp;

    swapRowAndColumn(C, pIndex, tD);
    swapRowAndColumn(Q, pIndex, tD);

    double const * bv = basisVectors[pIndex];
    basisVectors[pIndex] = basisVectors[tD];
    basisVectors[tD] = bv;

    double alphaStar = alpha[tD];
    double cStar = C[tD*bvMax+tD];
    double qStar = Q[tD*bvMax+tD];

    for (unsigned int i = 0; i < tD; ++i) {
        alpha[i] -= (C[tD*bvMax+i]+Q[tD*bvMax+i])*alphaStar/(cStar+qStar);
    }
    alpha[tD] = 0;

    for(unsigned int i = 0; i < tD; ++i) {
        for (unsigned int j = 0; j < tD; ++j) {
            double temp = Q[tD*bvMax+i]*Q[tD*bvMax+j]*1.0/qStar;

            C[i*bvMax+j] += temp;
            C[i*bvMax+j] -= (C[tD*bvMax+i]*C[tD*bvMax+j] +
                             C[tD*bvMax+i]*Q[tD*bvMax+j] +
                             Q[tD*bvMax+i]*C[tD*bvMax+j] +
                             Q[tD*bvMax+i]*Q[tD*bvMax+j]) * 1.0/ (cStar+qStar);

            Q[i*bvMax+j] -= temp;

            // double temp = Q[i*dMax+tD]*Q[j*dMax+tD]*1.0/qStar;

            // C[i*dMax+j] += temp;
            // C[i*dMax+j] -= (C[i*dMax+tD]*C[j*dMax+tD] +
            //                 C[i*dMax+tD]*Q[j*dMax+tD] +
            //                 Q[i*dMax+tD]*C[j*dMax+tD] +
            //                 Q[i*dMax+tD]*Q[j*dMax+tD]) * 1.0/ (cStar+qStar);

            // Q[i*dMax+j] -= temp;
        }
    }

    for (unsigned int i = 0; i <= tD; ++i) {
        Q[i*bvMax+tD] = 0;
        Q[tD*bvMax+i] = 0;
        C[i*bvMax+tD] = 0;
        C[tD*bvMax+i] = 0;
    }
}

/*unsigned int ProjectionGP :: getMinKL() {
    double *S = new double[bvMax*bvMax];

    mat::invertWithGauss(S, C, bvMax);
    mat::add(S, S, Q, bvMax, bvMax);
    mat::invertWithGauss(S, S, bvMax);

    int index = 0;
    double minScore = alpha[0]*alpha[0] / ( Q[0*bvMax+0] + C[0*bvMax+0] );
    minScore -= S[0*bvMax+0] / Q[0*bvMax+0];
    minScore += std::log(C[0*bvMax+0] / Q[0*bvMax+0] + 1);

    for (unsigned int i = 1; i < bvMax; ++i) {
        double tScore = alpha[i]*alpha[i] / ( Q[i*bvMax+i] + C[i*bvMax+i] );
        tScore -= S[i*bvMax+i] / Q[i*bvMax+i];
        tScore += std::log(C[i*bvMax+i] / Q[i*bvMax+i] + 1);

        if (tScore < minScore) {
            minScore = tScore;
            index = i;
        }
    }

    delete[] S;
    return index;
}*/

unsigned int ProjectionGP :: getMinKLApprox() {
    int index = 0;
    double minScore = alpha[0]*alpha[0] / ( Q[0*bvMax+0] + C[0*bvMax+0] );

    for (unsigned int i = 1; i < bvMax; ++i) {
        double tScore = alpha[i]*alpha[i] / ( Q[i*bvMax+i] + C[i*bvMax+i] );

        if (tScore < minScore) {
            minScore = tScore;
            index = i;
        }
    }

    return index;
}


void ProjectionGP :: train(double const * const pX, double const pY) {
    // Compute kernel to every basis vector
    // k = ( k(x1, pX), k(x2, pX), ...,  k(xN, pX))
    for (unsigned int i = 0; i < bvCnt; ++i) {
        k[i] = inputKernel.K(basisVectors[i], pX, dim);
    }
    double kStar = inputKernel.K(pX,pX, dim);

    // Compute mean prediction using alpha
    double m = 0.0;
    for (unsigned int i = 0; i < bvCnt; ++i) {
        m += k[i]*alpha[i];
    }

    m += meanFunction.mean(pX, dim);

    // Compute variance and update s accordingly
    double sigma2 = kStar;
    for (unsigned int i = 0; i < bvCnt; ++i) {
        double sum = 0.0;
        for (unsigned int j = 0; j < bvCnt; ++j) {
            sum += C[i*bvMax+j]*k[j];
        }

        s[i] = sum;
        sigma2 += sum*k[i];
    }
    s[bvCnt] = 1;

    double q = (pY - m) / (sigma2 + sigma2_n);
    double r = - 1.0/(sigma2_n + sigma2);
    double gamma = kStar;

    for (unsigned int i = 0; i < bvCnt; ++i) {
        double sum = 0.0;
        for (unsigned int j = 0; j < bvCnt; ++j) {
            sum += Q[i*bvMax+j]*k[j];
        }
        e[i] = sum;
        gamma -= sum*k[i];

        alpha[i] += s[i]*q;
    }
    alpha[bvCnt] += s[bvCnt]*q;

    // Update Q = Q + t*t^T*gamma^-1 with t = e - u
    for (unsigned int i = 0; i < bvCnt + 1; ++i) {
        for (unsigned int j = 0; j < bvCnt + 1; ++j) {

            // Update C = C + s*s^T*r
            C[i*bvMax+j] += s[i]*s[j]*r;

            double ti = i == bvCnt ? -1 : e[i];
            double tj = j == bvCnt ? -1 : e[j];

            Q[i*bvMax+j] += (ti)*(tj)*1.0/gamma;
        }
    }

    basisVectors[bvCnt] = pX;
    ++bvCnt;

    if (bvCnt >= bvMax) {
        // std :: cout << "alpha: ";
        // for (unsigned int i = 0; i < bvCnt; ++i) {
        //     std :: cout << alpha[i] << " ";
        // }
        // std :: cout << std :: endl;
        // helper::getch();

        unsigned int index = getMinKLApprox();
        deleteBV(index);
        --bvCnt;
    }
}

double ProjectionGP::predict(double const * const pX) {
    double sum = 0.0;
    for (unsigned int i = 0; i < bvCnt; ++i) {
        sum += inputKernel.K(basisVectors[i],pX,dim)*alpha[i];
    }

    return sum+meanFunction.mean(pX,dim);
}

}
