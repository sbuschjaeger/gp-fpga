#ifndef MATRIX_UTILS_H
#define MATRIX_UTILS_H

#include <vector>
#include <iostream>
#include <iomanip>
#include <future>
#include <cmath>
#include <atomic>
#include <thread>

namespace mat {

/**** Add functions ****/

inline void add(double * const pOut, double const * const pA, double const * const pB, unsigned int const numRows, unsigned int const numCols) {
    for (unsigned int i = 0; i < numRows; ++i) {
        for (unsigned int j = 0; j < numCols; ++j) {
            pOut[i*numCols + j] = pA[i*numCols + j] + pB[i*numCols + j];
        }
    }
}

/**** Multiplication functions ****/

inline double scalar_product(double const * const pVec1, double const * const pVec2, unsigned int pDim) {
    double sum = 0.0;

    for (unsigned int i = 0; i < pDim; ++i) {
        sum += pVec1[i]*pVec2[i];
    }

    return sum;
}

inline void mult_inplace(double * const pA, double pScalar, unsigned int const numRows, unsigned int const numCols) {
    for (unsigned int i = 0; i < numRows; ++i) {
        for (unsigned int j = 0; j < numCols; ++j) {
            pA[i*numCols+j] = pScalar * pA[i*numRows+j];
        }
    }
}

inline void mult(double * const pOut, double const * const pIn,  double pScalar, unsigned int const numRows, unsigned int const numCols) {
    for (unsigned int i = 0; i < numRows; ++i) {
        for (unsigned int j = 0; j < numCols; ++j) {
            pOut[i*numCols+j] = pScalar * pIn[i*numCols+j];
        }
    }
}

inline void mult(double * const pOut, double const * const pVec, double const * const pMat, unsigned int const pN, unsigned int const numColsMat) {
    for (unsigned int i = 0; i < numColsMat; ++i) {
        double sum = 0.0;
        for (unsigned int j = 0; j < pN; ++j) {
            sum += pVec[j]*pMat[j*numColsMat+i];
        }
        pOut[i] = sum;
    }
}

inline void mult(double * const pOut, double const * const pA, double const * const pB, unsigned int const numRowsA, unsigned int const numColsA, const unsigned int numColsB) {
    for (unsigned int row = 0; row < numRowsA; ++row) {
        for (unsigned int col = 0; col < numColsB; ++col)  {
            double sum = 0.0;
            for (unsigned int i = 0; i < numColsA; ++i) {
                sum += pA[row*numColsA + i] * pB[i*numColsB + col];
            }
            pOut[row * numColsB + col] = sum;
        }
    }
}

inline void multTransposed(double * const pOut, double const * const pA, double const * const pB, unsigned int const numRowsA, unsigned int const numColsA, const unsigned int numRowsB) {
    for (unsigned int rowA = 0; rowA < numRowsA; ++rowA) {
        for (unsigned int rowB = 0; rowB < numRowsB; ++rowB)  {
            double sum = 0.0;
            for (unsigned int i = 0; i < numColsA; ++i) {
                sum += pA[rowA*numColsA + i] * pB[rowB*numColsA + i];//pB[col*numColsB + i];
            }
            //std :: cout << sum << " ";
            pOut[rowA * numRowsB + rowB] = sum;
        }
        //std :: cout << std :: endl;
    }
}

/**** Helper functions ****/

inline void transpose(double * const pOut, double const * const pA, unsigned int const numRows, unsigned int const numCols) {
    for (unsigned int i = 0; i < numRows; ++i) {
        for (unsigned int j = 0;j < numCols; ++j) {
            pOut[j*numRows+i] = pA[i*numCols+j];
        }
    }
}

inline void print(double const * const pA, unsigned int const numRows, unsigned int const numCols, std::ostream &pOut) {
    for (unsigned int i = 0; i < numRows; ++i) {
        for (unsigned int j = 0; j < numCols; ++j) {
            pOut << std::setw(15) << pA[i*numCols+j] << " ";
        }
        pOut << std :: endl;
    }
}

inline void eye(double * const pOut, unsigned int const N) {
    for (unsigned int i = 0; i < N; ++i) {
        pOut[i*N+i] = 1.0;
    }
}

inline void compareApproximateMatrix(bool &pEquality, double const * const pA, double const * const pB, unsigned int const numRows, unsigned int const numCols, double const pEpsilon) {
    for (unsigned int i = 0; i < numRows; ++i) {
        for (unsigned int j = 0; j < numCols; ++j) {
            if (std::abs(pA[i*numCols+j] - pB[i*numCols+j]) > pEpsilon) {
                /* DEBUG */
                std :: cout << "pA: " << pA[i*numCols+j] << std :: endl;
                std :: cout << "pB: " << pB[i*numCols+j] << std :: endl;
                pEquality = false;
                return;
            }
        }
    }

    pEquality = true;
}

inline void frobeniusnorm(double &pOut, double const * const pM, unsigned int const numRows, unsigned int const numCols) {
    pOut = 0;

    for (unsigned int i = 0; i < numRows; ++i)  {
        for (unsigned int j = 0; j < numCols; ++j) {
            pOut += pM[i*numCols+j] * pM[i*numCols+j];
        }
    }

    pOut = std::sqrt(pOut);
}

/**** Inversion functions ****/

inline void cholesky(double * const pOut, double const * const pIn, unsigned int const N) {
    for (unsigned int j = 0; j < N; ++j) {
        double sum = 0.0;

        for (unsigned int k = 0; k < j; ++k) {
            sum += pOut[j*N+k] * pOut[j*N+k];
        }

        pOut[j*N+j] = std :: sqrt(pIn[j*N+j] - sum);

        //#pragma parallel

        for (unsigned int i = j + 1; i < N; ++i) {
            double sum = 0.0;

            for (unsigned int k = 0; k < j; ++k) {
                sum += pOut[i*N+k] * pOut[j*N+k];
            }

            pOut[i*N+j] = 1.0 / pOut[j*N+j] * (pIn[i*N+j] - sum);
        }
    }

}

// inline void invertApprox(double * const pOut, double const * const pIn, unsigned int const  N, double const epsilon) {
    
//     //TODO: Make less allocs here!        
//     double * psi = new double[N*N]();
    
//     double * temp1 = new double[N*N]();
//     double * temp2 = new double[N*N]();
//     double * temp3 = new double[N*N]();

//     double * eye = new double[N*N]();
//     mat::eye(eye,N);

//     double * xsi = new double[N*N]();

//     /*mat::transpose(temp1, pIn, N, N);
//     double norm;
//     mat::frobeniusnorm(norm, pIn, N, N);*/

//     //mat::mult(pOut, pIn, 1.0, N, N);

//     std :: cout << "POUT: " << std :: endl;
//     mat::print(pOut, N, N, std :: cout);

//     std :: cout << "PIN: " << std :: endl;
//     mat::print(pIn, N, N, std :: cout);

//     for (unsigned int n = 0; n < 20; ++n) {
//         mat::mult(psi, pIn, pOut, N, N, N);

//         std :: cout << "pOut: " << std :: endl;
//         mat::print(pOut, N, N, std :: cout);

//         mat::mult(temp1,eye, -8.0, N, N);

//         mat::add(temp2, temp1, psi, N, N);

//         mat::mult(temp1, psi, temp2, N, N, N);
//         mat::mult(temp2, eye, 22.0, N, N);
//         mat::add(temp3, temp1, temp2, N, N);
//         mat::mult(temp2, psi, temp3, N, N, N);

//         mat::mult(temp1, eye, -28.0, N, N);
//         mat::add(temp3, temp1, temp2, N, N);

//         mat::mult(temp1,psi,temp3, N, N, N);
//         mat::mult(temp2, eye, 17.0, N,N);
//         mat::add(xsi, temp1, temp2, N, N);

//         // KappaN = PsiN * XsiN
//         mat::mult(temp1, psi, xsi, N, N, N);

//         //actual update
//         mat::mult(temp2, eye, -12.0, N,N);
//         mat::add(temp3, temp2, temp1, N, N);

//         mat::mult(temp2, temp1, temp3, N,N,N);
//         mat::mult(temp1, eye, 48.0, N,N);
//         mat::add(temp3, temp1, temp2, N,N);

//         mat::mult(temp2, xsi, temp3, N, N, N);
//         mat::mult(temp1, pOut, temp2, N,N, N);

//         mat::mult(pOut, temp1, 1.0/64.0, N, N);
//     }

//     delete psi;
//     delete temp1;
//     delete temp2;
//     delete temp3;
//     delete xsi;
// }    
// 
inline void invertWithGauss(int * const pOut, int const * const pIn, unsigned int const N) {
    int *matrix = new int[2*N*N]();

    // Copy values
    for (unsigned int i = 0; i < N; ++i) {
        for (unsigned int j = 0; j < N; ++j) {
            matrix[i*2*N+j] = pIn[i*N+j];
        }
    }

    // Init right side
    for (unsigned int i = 0; i < N; ++i) {
        matrix[2*N*i+i+N] = 1.0;
    }

    for (unsigned int i = 0; i < N; ++i) {
        for (unsigned int j = 0; j < N; ++j) {
            if (i != j) {
                int ratio = matrix[j*2*N+i] / matrix[i*2*N+i];

                for (unsigned int k = 0; k < 2*N; ++k) {
                    matrix[j*2*N+k] -= ratio * matrix[i*2*N+k];
                }
            }
        }
    }

    for (unsigned int i = 0; i < N; ++i) {
        for (unsigned int j = N; j < 2*N; ++j) {
            matrix[i*2*N+j] /= matrix[i*2*N+i];
        }
    }

    for (unsigned int i = 0; i < N; ++i) {
        for (unsigned int j = 0; j < N; ++j) {
            pOut[i*N+j] = matrix[i*2*N+N+j];
        }
    }

    delete[] matrix;
}

inline void invertWithGauss(double * const pOut, double const * const pIn, unsigned int const N){
    double *matrix = new double[2*N*N]();

    // Copy values
    for (unsigned int i = 0; i < N; ++i) {
        for (unsigned int j = 0; j < N; ++j) {
            matrix[i*2*N+j] = pIn[i*N+j];
        }
    }

    // Init right side
    for (unsigned int i = 0; i < N; ++i) {
        matrix[2*N*i+i+N] = 1.0;
    }

    for (unsigned int i = 0; i < N; ++i) {
        for (unsigned int j = 0; j < N; ++j) {
            if (i != j) {
                double ratio = matrix[j*2*N+i] / matrix[i*2*N+i];

                for (unsigned int k = 0; k < 2*N; ++k) {
                    matrix[j*2*N+k] -= ratio * matrix[i*2*N+k];
                }
            }
        }
    }

    for (unsigned int i = 0; i < N; ++i) {
        for (unsigned int j = N; j < 2*N; ++j) {
            matrix[i*2*N+j] /= matrix[i*2*N+i];
        }
    }

    for (unsigned int i = 0; i < N; ++i) {
        for (unsigned int j = 0; j < N; ++j) {
            pOut[i*N+j] = matrix[i*2*N+N+j];
        }
    }

    delete[] matrix;
}    

inline void invertSquare(double * const pOut, double const * const pIn, unsigned int const  N){
    double *L = new double[N*N]();
    cholesky(L, pIn, N);

    for (int j = N - 1; j >= 0; --j) {
        for (int i = j; i >= 0; --i) {
            double sum = 0.0;
            for (unsigned int k = i+1; k < N; ++k) {
                sum += L[k*N+i]*pOut[k*N+j];
            }

            if (i == j) {
                pOut[i*N+j] = (1.0/L[i*N+i] - sum) / L[i*N+i];
            } else {
                pOut[i*N+j] = - sum / L[i*N+i];
            }

            pOut[j*N+i] = pOut[i*N+j];
        }
    }

    delete[] L;
}

}
#endif
