#ifndef ONLINE_ALGORITHM_H
#define ONLINE_ALGORITHM_H

/**
 * A simple interface for implementing online algorithm for training and prediction
 */
class OnlineAlgorithm {
private:
public:
	/**
	 * @brief Standard virtual d'tor
	 * @details Standard virtual d'tor
	 */
	virtual ~OnlineAlgorithm() {};

	/**
	 * @brief The training function of the implemented online algorithm
	 * @details The training function of the implemented online algorithm.
	 * 			This function takes the given example pX and label pY and
	 * 			updates the underlaying model
	 * 
	 * @param pX The observation
	 * @param pY The label
	 */
	virtual void train(double const * const pX, double const pY) = 0;

	/**
	 * @brief Performs a prediction for the given observation.
	 * @details Performs a prediction for the given observation.
	 * 
	 * @param pX The given observation
	 * @return The predicted label
	 */
	virtual double predict(double const * const pX) = 0;

};

#endif