#ifndef KERNEL_H
#define KERNEL_H

/**
 * A simple interface for implementing kernel functions
 */
class Kernel {
private:
public:
    /**
     * @brief Standard virtual d'tor
     * @details Standard virtual d'tor
     */
	virtual ~Kernel() {};
	
	/**
	 * @brief The kernel function to be computed.
     * @details The kernel function to be computed.
     * 
	 * @param  pX1  The first example
	 * @param  pX2  The second example
	 * @param  pDim The dimension of both example
	 * @return      The kernel function value between pX1 and pX2
	 */
	virtual double K(double const * const pX1, double const * const pX2, unsigned int const pDim) const = 0;
};

/**
 * Standard RBF kernel with scaling parameter gamma and length.
 */
class RBFKernel : public Kernel {
private:
	// scaling parameters
	double gamma;
    double length;

public:
	/**
	 * Standard c'tor.
	 * @param gamma Scaling parameter gamma
	 */
    /**
     * @brief Standard c'tor
     * @details Standard c'tor
     * 
     * @param pGamma The gamma parameter
     * @param pLength The length parameter
     */
    RBFKernel(double pGamma, double pLength) : gamma(pGamma), length(pLength) {
    }

    /**
     * @brief Standard d'tor
     * @details Standard d'tor
     */
    ~RBFKernel() {};

    /**
     * @brief RBF kernel function
     * @details RBF kernel function
     * 
     * @param pX1 First example 
     * @param  pX2  Second example
     * @param  pDim Dimension of both examples
     * @return      The RBF kernel function value between pX1 and pX2 scaled wit gamma
     */
    virtual inline double K(double const * const pX1, double const * const pX2, unsigned int const pDim) const {
        double sum = 0.0;
        
        // Compute ||pX1 - pX2||^2
        for (unsigned int i = 0; i < pDim; ++i) {
            sum += (pX1[i] - pX2[i])*(pX1[i] - pX2[i]);
        }
        
        // Rescaling + actual RBF function
        return length * std :: exp( - gamma * sum );
    }
};
#endif