#ifndef DUMMY_ALGORITHM_H
#define DUMMY_ALGORITHM_H

#include <cmath>
#include "DummyAlgorithm_config.h"

#define PRAGMA_SUB(x) _Pragma (#x)
#define DO_PRAGMA(x) PRAGMA_SUB(x)

#define alpha 1

/**
 * @brief Main function of this simple dummy algorithm.
 * @details Main function of this simple dummy algorithm. This algorithm 
 * 			is a two class online perceptron with learning rate alpha.
 *          If pPredict == false is given, then the perceptron will update its model
 *          parameter according to the example tuple (pX,pY). 
 *          If pPredict == true is given, a prediction for pX is performed and returned.
 *          In this case, pY is ignored.
 * 			
 * 			If pInit = true, the model parameter are reset to 0.
 * 			
 * @param pX The next observation
 * @param pY The label. Only used if pPredict = false
 * @param pPredict A flag to indicate wether the perceptron should predict or train the given observation
 * @param pInit A flag to indicate wether the model paramter should be resetted or not.
 * @return If pPredict = true, then the predicted label is returned. If pPredict = false, then pY is returned.
 */
float dummy_algorithm(float const pX[dim], float const pY, bool const pPredict, bool const pInit);
	
/**
 * @brief A simple helper function to perform the prediction
 * @details A simple helper function to perform the prediction. This function is not exposed to the outside 
 * 			in the RTL design.
 * 
 * @param pX The observation for which a prediction should be generated
 * @return The predicted label.
 */
float predict(float const pX[dim]);

#endif
