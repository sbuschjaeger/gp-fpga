#include "DummyAlgorithm.h"

// (global) model parameters
float weights[dim + 1] = {0};

float predict(float const pX[dim]) {
    float sum = 0;
    for (unsigned int i = 0; i < dim; ++i) {
        sum += pX[i]*weights[i];
    }
    sum += weights[dim]; 
    
    return sum >= 0 ? 1 : -1;
}

float dummy_algorithm(float const pX[dim], float const pY, bool const pPredict, bool const pReset) {
DO_PRAGMA(HLS INTERFACE s_axilite port=pX depth=dim);
#pragma HLS INTERFACE s_axilite port=pY
#pragma HLS INTERFACE s_axilite port=pPredict
#pragma HLS INTERFACE s_axilite port=pReset
#pragma HLS INTERFACE s_axilite port=return

	if (pReset) {
        RESET: for (unsigned int i = 0; i <= dim; ++i) {
#pragma HLS UNROLL
            weights[i] = 0;
        }
	}

	if (!pPredict) {
        float prediction = predict(pX);

        UPDATE_WEIGHTS:for (unsigned int i = 0; i < dim; ++i) {
#pragma HLS UNROLL
            weights[i] += alpha*(pY - prediction)*pX[i];
        }
        weights[dim] += alpha*(pY - prediction);

        return pY;
	} else {
        return predict(pX);
	}
}
