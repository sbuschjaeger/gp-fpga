#include <iostream>

#include "DummyAlgorithm.h"

int main(int argc, char* argv[]) {
    const unsigned int NTrain = 32;
    const unsigned int NTest = 4;
    
    float XTrain[] = { 0, 0,
    				   0, 1,
    				   1, 0,
    				   1, 1,
    				   0, 0,
    				   0, 1,
    				   1, 0,
    				   1, 1,
    				   0, 0,
    				   0, 1,
    				   1, 0,
    				   1, 1,
    				   0, 0,
    				   0, 1,
    				   1, 0,
    				   1, 1,
    				   0, 0,
    				   0, 1,
    				   1, 0,
    				   1, 1,
    				   0, 0,
    				   0, 1,
    				   1, 0,
    				   1, 1,
    				   0, 0,
    				   0, 1,
    				   1, 0,
    				   1, 1,
    				   0, 0,
    				   0, 1,
    				   1, 0,
    				   1, 1 };
    float YTrain[] = {  1, 1, 1, -1,
                        1, 1, 1, -1,
                        1, 1, 1, -1,
                        1, 1, 1, -1,
                   		1, 1, 1, -1,
                        1, 1, 1, -1,
                        1, 1, 1, -1,
                       	1, 1, 1, -1};

    float XTest[] = { 0, 0 ,
    				  0, 1,
    				  1, 0 ,
    				  1, 1};
    float YTest[] = {1, 1, 1, -1};

    for (unsigned int i = 0; i < NTrain; ++i) {
    	dummy_algorithm(&XTrain[i*dim], YTrain[i], false, false);
    }

    for (unsigned int i = 0; i < NTest; ++i) {
    	float prediction = dummy_algorithm(&XTest[i*dim], 0, true, false);
    	std :: cout << "Prediction / Label: " << prediction << " / " << YTest[i] << std :: endl;
    }
}
