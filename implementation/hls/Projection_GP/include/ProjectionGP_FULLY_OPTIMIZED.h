#ifndef PROJECTION_GP_FULLY_OPTIMIZED_H
#define PROJECTION_GP_FULLY_OPTIMIZED_H

#include "ProjectionGP_config.h"

#ifdef GP_TEST
namespace fully_optimized {
#endif

float K(float const pX1[], float const pX2[]);

void swapRowAndColumn(float pM[], unsigned int rowA , unsigned int rowB);

void deleteBV(unsigned int pIndex);

unsigned int getMinKLApprox();

float projection_gp(float const pX[dim], float const pY, bool const pPredict, bool const pInit);

void copyBV(float const pX[dim], unsigned int pIndex);

#ifdef GP_TEST
}
#endif

#endif
