#ifndef PROJECTION_GP_CONFIG_H
#define PROJECTION_GP_CONFIG_H

#define PRAGMA_SUB(x) _Pragma (#x)
#define DO_PRAGMA(x) PRAGMA_SUB(x)

#include <cmath>

/* Configuration parameters */
#define numBV 40
#define bvMax (numBV+1)

#define dim 13
#define sigma2_n 1.0
#define gamma_kernel 0.5
#define gamma_length 1
#define mean 66.87798

#endif
