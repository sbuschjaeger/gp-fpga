#include "ProjectionGP_FULLY_OPTIMIZED.h"

#ifdef GP_TEST
namespace fully_optimized {
#endif

float C[bvMax * bvMax] = {0};
float Q[bvMax * bvMax] = {0};
float e[bvMax] = {0};
float k[numBV] = {0};
float s[bvMax] = {0};
float alpha[bvMax] = {0};
float basisVectors[bvMax*dim] = {0};
unsigned int bvCnt = 0;

float K(float const pX1[dim], float const pX2[dim]) {
    float sum = 0.0;
    for (unsigned int i = 0; i < dim; ++i) {
#pragma HLS PIPELINE
    	float val = (pX1[i] - pX2[i]);
        sum += val*val;
    }

    return std::exp((float) - gamma_kernel * sum);
}

void swapRowAndColumn(float pM[bvMax * bvMax], unsigned int rowA , unsigned int rowB) {
    for (unsigned int i = 0; i < bvMax; ++i) {
        //switch row elements
        float temp = pM[rowA*bvMax+i];
        pM[rowA*bvMax+i] = pM[rowB*bvMax+i];
        pM[rowB*bvMax+i] = temp;
    }

    for (unsigned int i = 0; i < bvMax; ++i) {
        // switch column elements
        float temp = pM[i*bvMax+rowA];
        pM[i*bvMax+rowA] = pM[i*bvMax+rowB];
        pM[i*bvMax+rowB] = temp;
    }
}

void deleteBV( unsigned int pIndex) {
    /* Copy element to last position in alpha and C/Q */

    // Swap alpha
    float temp = alpha[pIndex];
    alpha[pIndex] = alpha[numBV];
    alpha[numBV] = temp;

    swapRowAndColumn(C, pIndex, numBV);
    swapRowAndColumn(Q, pIndex, numBV);

    DELETE_BV_SWAP_LOOP:for (unsigned int i = 0; i < dim; ++i) {
#pragma HLS PIPELINE
        temp = basisVectors[pIndex*dim+i];
        basisVectors[pIndex*dim+i] = basisVectors[numBV*dim+i];
        basisVectors[numBV*dim+i] = temp;
    }

    unsigned int tA = numBV*bvMax+numBV;
    float alphaStar = alpha[numBV];
    float cStar = C[tA];
    float qStar = Q[tA];
    temp = alphaStar/(cStar+qStar);

    DELETE_BV_ALPHA:for (unsigned int i = 0; i < numBV; ++i) {
//#pragma UNROLL
    	unsigned int a = numBV*bvMax+i;
        alpha[i] -= (C[a]+Q[a])*temp;
    }
    alpha[numBV] = 0;

    DELETE_BV_C_OUTER:for(unsigned int i = 0; i < numBV; ++i) {
//#pragma HLS PIPELINE
    	DELETE_BV_C_INNER:for (unsigned int j = 0; j < numBV; ++j) {
//#pragma HLS UNROLL
        	unsigned int a = numBV*bvMax+i;
        	unsigned int b = numBV*bvMax+j;
        	unsigned int c = i*bvMax+j;
            float temp = Q[a]*Q[b]*1.0/qStar;

            //C[i*bvMax+j] += temp;
            C[c] += temp -
            				(C[a]*C[b] +
                             C[a]*Q[b] +
                             Q[a]*C[b] +
                             Q[a]*Q[b]) / (cStar+qStar);

            Q[c] -= temp;
        }
    }

    DELETE_BV_UNSET_C_Q:for (unsigned int i = 0; i <= numBV; ++i) {
//#pragma HLS UNROLL
    	unsigned int a = i*bvMax+numBV;
    	unsigned int b = numBV*bvMax+i;

    	Q[a] = 0;
        Q[b] = 0;
        C[a] = 0;
        C[b] = 0;
    }
}

unsigned int getMinKLApprox() {
    int index = 0;
    float minScore = alpha[0]*alpha[0] / ( Q[0*bvMax+0] + C[0*bvMax+0] );

    for (unsigned int i = 1; i < bvMax; ++i) {
        float tScore = alpha[i]*alpha[i] / ( Q[i*bvMax+i] + C[i*bvMax+i] );

        if (tScore < minScore) {
            minScore = tScore;
            index = i;
        }
    }

    return index;
}

void train_full_bv_set( float const pX[dim], float const pY) {
	float m = mean;
	// Compute kernel to every basis vector
    // k = ( k(x1, pX), k(x2, pX), ...,  k(xN, pX))
    CALC_K:for (unsigned int i = 0; i < numBV; ++i) {
//#pragma HLS UNROLL
        k[i] = K(&basisVectors[i*dim], pX);
        m += k[i]*alpha[i];
    }
    // NOTE: BECAUSE EXPONENTIAL KERNEL IS 1 FOR K(x,x)
    float kStar = 1.0;
    // Compute mean prediction using alpha

    // Compute variance and update s accordingly
    float sigma2 = kStar;
    CALC_SIGMA_OUTER:for (unsigned int i = 0; i < numBV; ++i) {
//#pragma HLS PIPELINE
    	s[i] = 0.0;
    	e[i] = 0.0;
        CALC_SIGMA_INNER:for (unsigned int j = 0; j < numBV; ++j) {
//#pragma HLS UNROLL
        	s[i] += C[i*bvMax+j]*k[j];
        	e[i] += Q[i*bvMax+j]*k[j];
        }
    }

    CALC_S: for (unsigned int i = 0; i < numBV; ++i) {
//#pragma HLS UNROLL
    	sigma2 += s[i]*k[i];
    }
    s[numBV] = 1;

    float q = (pY - m) / (sigma2 + sigma2_n);
    float r = - 1.0/(sigma2_n + sigma2);
    float gamma = kStar;

    UPDATE_ALPHA: for (unsigned int i = 0; i < numBV; ++i) {
//#pragma HLS UNROLL
    	gamma -= e[i]*k[i];
    	alpha[i] += s[i]*q;
    }

//    UPDATE_GAMMA:for (unsigned int i = 0; i < numBV; ++i) {
//    	gamma -= e[i]*k[i];
//    }

    alpha[numBV] += s[numBV]*q;

    // Update Q = Q + t*t^T*gamma^-1 with t = e - u
    UPDATE_C_OUTER:for (unsigned int i = 0; i <= numBV; ++i) {
//#pragma HLS PIPELINE
    	UPDATE_C_INNER:for (unsigned int j = 0; j <= numBV; ++j) {
//#pragma HLS UNROLL
    		// Update C = C + s*s^T*r
            C[i*bvMax+j] += s[i]*s[j]*r;
        }
    }

    UPDATE_Q_OUTER:for (unsigned int i = 0; i <= numBV; ++i) {
//#pragma HLS PIPELINE
    	UPDATE_Q_INNER:for (unsigned int j = 0; j <= numBV; ++j) {
//#pragma HLS UNROLL
    		float ti = i == numBV ? -1 : e[i];
			float tj = j == numBV ? -1 : e[j];

			Q[i*bvMax+j] += (ti)*(tj)*1.0/gamma;
    	}
    }

//	UPDATE_Q_OUTER:for (unsigned int i = 0; i < numBV; ++i) {
//		UPDATE_Q_INNER:for (unsigned int j = 0; j < numBV; ++j) {
////#pragma HLS UNROLL
//			Q[i*bvMax+j] += e[i]*e[j]*1.0/gamma;
//		}
//	}
//
//    UPDATE_Q_NUMBV:for (unsigned int i = 0; i < numBV; ++i) {
////#pragma HLS UNROLL
//    	//j == numBV
//    	Q[i*bvMax+numBV] += -e[i]*1.0/gamma;
//
//    	//i == numBV
//    	Q[numBV*bvMax+i] += -e[i]*1.0/gamma;
//    }
//    Q[numBV*bvMax+numBV] += 1.0/gamma;

    copyBV(pX,numBV);

	unsigned int index = getMinKLApprox();
	deleteBV( index ) ;
}

void copyBV(float const pX[dim], unsigned int pIndex) {
	COPY_BV:for (unsigned int i = 0; i < dim; ++i) {
//#pragma HLS PIPELINE
		basisVectors[bvCnt*dim + i] = pX[i];
	}
}

float projection_gp(float const pX[dim], float const pY, bool const pPredict, bool const pReset) {
DO_PRAGMA(HLS INTERFACE s_axilite port=pX depth=dim);
#pragma HLS INTERFACE s_axilite port=pY
#pragma HLS INTERFACE s_axilite port=pPredict
#pragma HLS INTERFACE s_axilite port=pReset
#pragma HLS INTERFACE s_axilite port=return

	if (pReset) {
		INIT_OUTER:for (unsigned int i = 0; i < bvMax; ++i) {
//#pragma HLS UNROLL
			INIT_INNER: for (unsigned int j = 0; j < bvMax; ++j) {
//#pragma HLS UNROLL
				if (i == j) {
					Q[i*dim + i] = 1.0;
					C[i*dim + i] = 1.0;
				} else {
					Q[i*dim + i] = 0.0;
					C[i*dim + i] = 0.0;
				}
			}
			alpha[i] = 0.0;
		}
	}

	if (!pPredict) {
		if (bvCnt == numBV) {
			train_full_bv_set(pX,pY);
		} else {
			copyBV(pX,bvCnt);
			++bvCnt;
		}
		return 0;
	} else {
		float sum = mean;
		PREDICTION:for (unsigned int i = 0; i < numBV; ++i) {
//#pragma HLS UNROLL
			sum += K(&basisVectors[i*dim],pX)*alpha[i];
		}

		return sum;
	}
}

#ifdef GP_TEST
}
#endif
