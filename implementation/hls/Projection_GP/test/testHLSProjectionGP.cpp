#include <iostream>

#include "../../../gaussian_processes/include/ApproxProjectionGP.h"
#include "../../../gaussian_processes/include/Kernel.h"
#include "../../../gaussian_processes/include/Meanfunction.h"
#include "ProjectionGP_FULLY_OPTIMIZED.h"

int main(int argc, char* argv[]) {
	double const epsilon = 0.05;
	unsigned int const NTest = 30;
	unsigned int const NTrain = 70;

	std::vector<double> XTrainDouble;
	std::vector<double> XTestDouble;	
	std::vector<double> YTrainDouble;
	std::vector<double> YTestDouble;	
	
    helper::generateData(dim, NTrain, NTest, XTrainDouble, XTestDouble, YTrainDouble, YTestDouble);
	
	std::vector<float> XTrainFloat(XTrainDouble.begin(), XTrainDouble.end());
	std::vector<float> XTestFloat(XTestDouble.begin(), XTestDouble.end());
	std::vector<float> YTrainFloat(YTrainDouble.begin(), YTrainDouble.end());
	std::vector<float> YTestFloat(YTestDouble.begin(), YTestDouble.end());

    std :: cout << "Start training" << std :: endl;

    // Train reference model
    RBFKernel k(gamma_kernel, gamma_length);
    ConstantMean cMean(mean);
    GP::ApproxProjectionGP gp(numBV, k, cMean, dim, sigma2_n);

    for (unsigned int i = 0; i < NTrain; ++i) {
    	gp.train(&XTrainDouble[i*dim], YTrainDouble[i]);
    	if (i == 0){
    		fully_optimized::projection_gp(&XTrainFloat[i*dim], YTrainFloat[i], false, true);
    	} else {
    		fully_optimized::projection_gp(&XTrainFloat[i*dim], YTrainFloat[i], false, false);
    	}   	
    }

    std :: cout << "Start test" << std :: endl;

    for (unsigned int i = 0; i < NTest; ++i) {
        double referencePrediction = gp.predict(&XTestDouble[i*dim]);
        double fullyOptimizedPrediction = fully_optimized::projection_gp(&XTestFloat[i*dim], 0, true, false);

       	if ( std::abs(referencePrediction-fullyOptimizedPrediction) > epsilon ) {
       		std :: cout << "Test FAILED on test item " << i << " while testing fully optimized implementation" << std :: endl;
            std :: cout << "Target was " << fullyOptimizedPrediction << " but reference was " << referencePrediction << std :: endl;
       		return -1;
       	}
    }

    std :: cout << "Test PASSED" << std :: endl;
    return 0;
}
