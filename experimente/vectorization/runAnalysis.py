#!/bin/bash

singleBinaries=("bico" "bico/bin/BICO_Quickstart"
				"kmeans++" "kmeans++/kmeans"
				"svmlight" "svm_light/svm_learn"
				"gzip" "gzip-1.6/gzip"		
				"projectiongp" "../gaussian_processes/test/testProjectionGP")

avgBinaries=("lapack" "lapack-3.6.0/bin"
			 "openssl" "openssl-1.0.2e/test"
			 "gnu coreutils" "coreutils-8.24/src/")

N=${#avgBinaries[*]}
for (( j=0; j<=$(( $N -1 )); j = j + 2 )) 
do

binaries=($(find ${avgBinaries[$((j+1))]} -type f -executable -exec sh -c "file -i '{}' | grep -q 'x-executable; charset=binary'" \; -print))
total=${#binaries[*]}

sumVectorInstructions=0
sumInstructions=0
for (( i=0; i<=$(( $total - 1 )); i++ )) 
do
	#echo "PATH:" ${binaries[$i]}
	noVectorInstructions=`./analyze.pl ${binaries[$i]} | awk -F ':' '{print $2}' | awk '{s+=$0} END {print s'}`
	noInstructions=`objdump -d -j .text ${binaries[$i]} | sed '/</d' | sed '/^\s*$/d' | wc -l`
	vectorization=`echo $noVectorInstructions*100/$noInstructions | bc -l`

	sumVectorInstructions=$(($sumVectorInstructions + $noVectorInstructions))
	sumInstructions=$(($sumInstructions + $noInstructions))
done
echo ${avgBinaries[$j]}
#echo ${binaries[$((i+1))]}
echo `echo $sumVectorInstructions/$total | bc -l`
echo `echo $sumInstructions/$total | bc -l` 
vectorization=`echo $sumVectorInstructions*100/$sumInstructions | bc -l`
echo $vectorization
echo ""

done

total=${#singleBinaries[*]}

for (( i=0; i<=$(( $total -1 )); i=i+2 )) 
do
	noVectorInstructions=`./analyze.pl ${singleBinaries[$((i+1))]} | awk -F ':' '{print $2}' | awk '{s+=$0} END {print s'}`
	noInstructions=`objdump -d -j .text ${singleBinaries[$((i+1))]} | sed '/</d' | sed '/^\s*$/d' | wc -l`
	vectorization=`echo $noVectorInstructions*100/$noInstructions | bc -l`

	echo ${singleBinaries[$i]}
	#echo ${binaries[$((i+1))]}
	echo $noVectorInstructions
	echo $noInstructions 
	echo $vectorization
	echo ""
done
