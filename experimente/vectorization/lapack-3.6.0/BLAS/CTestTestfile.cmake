# CMake generated Testfile for 
# Source directory: /home/pille/projects/masterarbeit/masterarbeit-implementation/implementation/benchmarks/lapack-3.6.0/BLAS
# Build directory: /home/pille/projects/masterarbeit/masterarbeit-implementation/implementation/benchmarks/lapack-3.6.0/BLAS
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(SRC)
SUBDIRS(TESTING)
