#!/usr/bin/env python
import matplotlib.pyplot as plt
import itertools
import numpy as np

def savePlot(pXlabel, pYLabel, pLegend, pFileName, pLocation):
    plt.legend(pLegend, loc=pLocation)
    plt.xlabel(pYLabel)
    plt.ylabel(pXlabel)

    # Remove top and right frame incl. ticks
    ax = plt.gca()
    ax.tick_params(axis='both', direction='out')
    ax.get_xaxis().tick_bottom()   # remove unneeded ticks 
    ax.get_yaxis().tick_left()
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)

    plt.savefig(pFileName,bbox_inches='tight')
    #plt.show(); 

filesOverDim = ["desktop_dim_X_numbv_100.csv", "laptop_dim_X_numbv_100.csv", "pi_dim_X_numbv_100.csv", "fpga_th_dim_X_numbv_100.csv"];
filesOverNumBV = ["desktop_dim_21_numbv_X.csv", "laptop_dim_21_numbv_X.csv", "pi_dim_21_numbv_X.csv", "fpga_th_dim_21_numbv_X.csv"];


plt.figure();
marker = itertools.cycle(('D', 'o', '*', 'p')) 
colors = itertools.cycle(['b','g','r','k'])

for f in filesOverDim[:3]:
    data = np.genfromtxt(f, delimiter = ',', skip_header = 1, usecols=(2,3))

    plt.plot(data[:,0],data[:,1]*1000,marker = marker.next(), markersize=8, markevery=5, color = colors.next())

data = np.genfromtxt(filesOverDim[3], delimiter = ',', skip_header = 1, usecols=(2,3))
plt.plot(data[:,0],data[:,1]*1000,marker = marker.next(), markersize=8, markevery=1, color = colors.next())

savePlot("Durchsatz [#elem/s]","Dimension der Beispiele", ['Desktop', 'Laptop' ,'ARM', 'FPGA (HLS)'],'throughput_over_dim.pdf',(0.8,0.2))

plt.figure();
data = np.genfromtxt(filesOverDim[2], delimiter = ',', skip_header = 1, usecols=(2,3))
plt.plot(data[:,0],data[:,1]*1000,marker = '*', color='r', markersize=8, markevery=5)

data = np.genfromtxt(filesOverDim[3], delimiter = ',', skip_header = 1, usecols=(2,3))
plt.plot(data[:,0],data[:,1]*1000,marker = 'p', color='k', markersize=8, markevery=1)
savePlot("Durchsatz [#elem/s]","Dimension der Beispiele",['ARM', 'FPGA (HLS)'],'throughput_over_dim_pi_fpga_only.pdf',(0.8,0.2))

plt.figure();
marker = itertools.cycle(('D', 'o', '*'))
colors = itertools.cycle(['b','g','r','k'])

for f in filesOverNumBV[:3]:
    data = np.genfromtxt(f, delimiter = ',', skip_header = 4, usecols=(1,3))
    plt.plot(data[:,0],data[:,1]*1000,marker = marker.next(), markersize=8, markevery=2, color = colors.next())

data = np.genfromtxt(filesOverNumBV[3], delimiter = ',', skip_header = 3, usecols=(1,3))
plt.plot(data[:,0],data[:,1]*1000,marker = marker.next(), markersize=8, markevery=1, color = colors.next())

savePlot("Durchsatz [#elem/s]","Anzahl der Basisvektoren", ['Desktop', 'Laptop' ,'ARM', 'FPGA (HLS)'],'throughput_over_numbv.pdf',(0.8,0.2))

plt.figure();
data = np.genfromtxt(filesOverNumBV[2], delimiter = ',', skip_header = 3, usecols=(1,3))
plt.plot(data[:,0],data[:,1]*1000,marker = '*', color='r', markersize=8, markevery=5)

data = np.genfromtxt(filesOverNumBV[3], delimiter = ',', skip_header = 3, usecols=(1,3))
plt.plot(data[:,0],data[:,1]*1000,marker = 'p', color='k', markersize=8, markevery=1)
savePlot("Durchsatz [#elem/s]","Dimension der Beispiele",['ARM', 'FPGA (HLS)'],'throughput_over_numbv_pi_fpga_only.pdf',(0.8,0.2))


desktoIDLEnergy = 3.63
laptopIDLEEnergy = 3.45
piEnergy = 2.2
fpgaEnergy = 2

plt.figure();
marker = itertools.cycle(('D', 'o', '*'))
colors = itertools.cycle(['b','g','r','k'])

data = np.genfromtxt(filesOverDim[0], delimiter = ',', skip_header = 1, usecols=(2,4))
plt.plot(data[:,0],data[:,1] - desktoIDLEnergy,marker = marker.next(), markersize=8, markevery=5, color = colors.next())

data = np.genfromtxt(filesOverDim[1], delimiter = ',', skip_header = 1, usecols=(2,4))
plt.plot(data[:,0],data[:,1] - laptopIDLEEnergy,marker = marker.next(), markersize=8, markevery=5, color = colors.next())

plt.plot(data[:,0],np.repeat(piEnergy, len(data[:,0])),marker = marker.next(), markersize=8, markevery=5, color = colors.next())
# plt.plot(data[:,0],np.repeat(fpgaEnergy, len(data[:,0])),marker = marker.next(), markersize=8, markevery=5, color = colors.next())

plt.yticks([0,2,4,6,8,10,12,14,16,18,20])

savePlot("Energieverbrauch [W]", "Dimension der Beispiele", ['Desktop', 'Laptop', 'ARM'],'energy_over_dim.pdf',(0.8,0.2))

plt.figure();
marker = itertools.cycle(('D', 'o', '*'))
colors = itertools.cycle(['b','g','r','k'])

data = np.genfromtxt(filesOverDim[0], delimiter = ',', skip_header = 1, usecols=(2,4))
plt.plot(data[:,0],data[:,1] - desktoIDLEnergy,marker = marker.next(), markersize=8, markevery=5, color = colors.next())

data = np.genfromtxt(filesOverDim[1], delimiter = ',', skip_header = 1, usecols=(2,4))
plt.plot(data[:,0],data[:,1] - laptopIDLEEnergy,marker = marker.next(), markersize=8, markevery=5, color = colors.next())

plt.plot(data[:,0],np.repeat(piEnergy, len(data[:,0])),marker = marker.next(), markersize=8, markevery=5, color = colors.next())

fpgaPY1 = 1.163
fpgaPX1 = 13
fpgaPY2 = 1.235
fpgaPX2 = 21
m = (fpgaPY2 - fpgaPY1) / (fpgaPX2 - fpgaPX1)
b = fpgaPY1 - m*fpgaPX1
fpgaEnergy = [m*x+b for x in data[:,0]]
plt.plot(data[:,0],fpgaEnergy,marker = marker.next(), markersize=8, markevery=5, color = colors.next())

plt.yticks([0,2,4,6,8,10,12,14,16,18,20])
plt.xlim(xmax=192)
savePlot("Energieverbrauch [W]", "Dimension der Beispiele", ['Desktop', 'Laptop', 'ARM', 'FPGA'],'energy_over_dim_incl_fpga.pdf',(0.8,0.65))


plt.figure();
marker = itertools.cycle(('D', 'o', '*'))
colors = itertools.cycle(['b','g','r','k'])

data = np.genfromtxt(filesOverNumBV[0], delimiter = ',', skip_header = 1, usecols=(1,4))
plt.plot(data[:,0],data[:,1] - desktoIDLEnergy,marker = marker.next(), markersize=8, markevery=2, color = colors.next())

data = np.genfromtxt(filesOverNumBV[1], delimiter = ',', skip_header = 1, usecols=(1,4))
plt.plot(data[:,0],data[:,1] - laptopIDLEEnergy,marker = marker.next(), markersize=8, markevery=2, color = colors.next())

plt.plot(data[:,0],np.repeat(piEnergy, len(data[:,0])),marker = marker.next(), markersize=8, markevery=2, color = colors.next())
# plt.plot(data[:,0],np.repeat(fpgaEnergy, len(data[:,0])),marker = marker.next(), markersize=8, markevery=2, color = colors.next())

plt.yticks([0,2,4,6,8,10,12,14,16,18,20])

savePlot("Energieverbrauch [W]", "Anzahl der Basisvektoren", ['Desktop', 'Laptop', 'ARM'],'energy_over_numbv.pdf',(0.8,0.2))

#### Plotte die Energieeffizienz ###
plt.figure();
marker = itertools.cycle(('D', 'o', '*'))
colors = itertools.cycle(['b','g','r','k'])

data = np.genfromtxt(filesOverDim[0], delimiter = ',', skip_header = 1, usecols=(2,3,4))
plt.plot(data[:,0],((data[:,2] - desktoIDLEnergy) / data[:,1]),marker = marker.next(), markersize=8, markevery=5, color = colors.next())

data = np.genfromtxt(filesOverDim[1], delimiter = ',', skip_header = 1, usecols=(2,3,4))
X = data[:,0]
plt.plot(data[:,0],((data[:,2] - laptopIDLEEnergy) / data[:,1]),marker = marker.next(), markersize=8, markevery=5, color = colors.next())

data = np.genfromtxt(filesOverDim[2], delimiter = ',', skip_header = 1, usecols=(2,3))
Y = np.repeat(piEnergy, len(data[:,0])) / data[:,1]
plt.plot(data[:,0],Y,marker = marker.next(), markersize=8, markevery=5, color = colors.next())

plt.yticks([0,1,2.5,5,7,5,10,15,20])
savePlot("Energieeffizienz [mJ/elem]", "Dimension der Beispiele", ['Desktop', 'Laptop', 'ARM'],'efficiency_over_dim.pdf','upper right')

plt.figure();
marker = itertools.cycle(('D', 'o', '*'))
colors = itertools.cycle(['b','g','r','k'])

data = np.genfromtxt(filesOverDim[0], delimiter = ',', skip_header = 1, usecols=(2,3,4))
plt.plot(data[:,0],((data[:,2] - desktoIDLEnergy) / data[:,1]),marker = marker.next(), markersize=8, markevery=5, color = colors.next())

data = np.genfromtxt(filesOverDim[1], delimiter = ',', skip_header = 1, usecols=(2,3,4))
X = data[:,0]
plt.plot(data[:,0],((data[:,2] - laptopIDLEEnergy) / data[:,1]),marker = marker.next(), markersize=8, markevery=5, color = colors.next())

data = np.genfromtxt(filesOverDim[2], delimiter = ',', skip_header = 1, usecols=(2,3))
Y = np.repeat(piEnergy, len(data[:,0])) / data[:,1]
plt.plot(data[:,0],Y,marker = marker.next(), markersize=8, markevery=5, color = colors.next())

fpgaPY1 = 3.26
fpgaPX1 = 13
fpgaPY2 = 3.6
fpgaPX2 = 21
m = (fpgaPY2 - fpgaPY1) / (fpgaPX2 - fpgaPX1)
b = fpgaPY1 - m*fpgaPX1
fpgaEnergyEfficiency = [m*x+b for x in data[:,0]]
plt.plot(data[:,0],fpgaEnergyEfficiency,marker = marker.next(), markersize=8, markevery=5, color = colors.next())
plt.yticks([0,1,2.5,5,7.5,10,15,20])
plt.xlim(xmax=192)
plt.ylim(ymax=22)
savePlot("Energieeffizienz [mJ/elem]", "Dimension der Beispiele", ['Desktop', 'Laptop', 'ARM', 'FPGA'],'efficiency_over_dim_incl_fpga.pdf','upper right')

plt.figure();
marker = itertools.cycle(('D', 'o', '*'))
colors = itertools.cycle(['b','g','r','k'])

data = np.genfromtxt(filesOverNumBV[0], delimiter = ',', skip_header = 1, usecols=(1,3,4))
plt.plot(data[:,0],((data[:,2] - desktoIDLEnergy) / data[:,1]),marker = marker.next(), markersize=8, markevery=2, color = colors.next())

data = np.genfromtxt(filesOverNumBV[1], delimiter = ',', skip_header = 1, usecols=(1,3,4))
X = data[:,0]
plt.plot(data[:,0],((data[:,2] - laptopIDLEEnergy) / data[:,1]),marker = marker.next(), markersize=8, markevery=2, color = colors.next())

data = np.genfromtxt(filesOverNumBV[2], delimiter = ',', skip_header = 1, usecols=(1,3))
Y = np.repeat(piEnergy, len(data[:,0])) / data[:,1]
plt.plot(data[:,0],Y,marker = marker.next(), markersize=8, markevery=2, color = colors.next())

# data = np.genfromtxt(filesOverNumBV[3], delimiter = ',', skip_header = 1, usecols=(1,3))
# Y = np.repeat(fpgaEnergy, len(data[:,0])) / data[:,1]
# plt.plot(data[:,0],Y,marker = marker.next(), markersize=8, markevery=1, color = colors.next())
# plt.yticks([0,1,2.5,5,7,5,10,15,20])
savePlot("Energieverbrauch [mJ/elem]", "Anzahl der Basisvektoren", ['Desktop', 'Laptop', 'ARM'],'efficiency_over_numbv.pdf', 'upper left')
#savePlot("Energieverbrauch [mJ/eleme]", "Anzahl der Basisvektoren", ['Desktop', 'Laptop', 'ARM'],'efficiency_over_numbv.pdf','upper left')