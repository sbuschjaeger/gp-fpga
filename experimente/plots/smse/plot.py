#!/usr/bin/env python
import matplotlib.pyplot as plt
import itertools

pathes = ["projection_gp.csv","full_gp.csv","approx_projection_gp.csv"]

marker = itertools.cycle(('D', 'o', '*')) 

for p in pathes:
    f = open("housing_" + p,'r');
    next(f);
    
    numBVConfigs = {}
    amseEntries = {}
    for line in f:
        temp = line.split(",");
        if int(temp[0]) == 4294967295:
            x = 455.0
        else:
            x = int(temp[0]);
            
        if not x in numBVConfigs: 
            numBVConfigs[x] = [line];
            amseEntries[x] = [float(temp[3])]
        else:
            numBVConfigs[x] = numBVConfigs[x] + [line];
            amseEntries[x] = amseEntries[x] + [float(temp[3])]
    
    X = []
    Y = []
    for k in amseEntries.keys():
        X.append(k)
        Y.append(min(amseEntries[k]))
    
    X, Y = (list(t) for t in zip(*sorted(zip(X, Y))))    
    plt.plot(X,Y,marker = marker.next(), markersize=8)
    
# Remove top and right frame incl. ticks
ax = plt.gca()
ax.tick_params(axis='both', direction='out')
ax.get_xaxis().tick_bottom()   # remove unneeded ticks 
ax.get_yaxis().tick_left()
ax.spines["right"].set_visible(False)
ax.spines["top"].set_visible(False)
                
plt.legend(['pgp', 'fgp', 'apgp'], loc='upper right')
plt.xlabel("Anzahl der Basisvektoren")
plt.ylabel("SMSE")
plt.savefig('housing_smse.pdf',bbox_inches='tight')
#plt.show()

plt.figure()
for p in pathes:
    f = open("sarcos_" + p,'r');
    next(f);
    
    numBVConfigs = {}
    amseEntries = {}
    for line in f:
        temp = line.split(",");
        x = int(temp[0])
        if not x in numBVConfigs: 
            numBVConfigs[x] = [line];
            amseEntries[x] = [float(temp[4])]
        else:
            numBVConfigs[x] = numBVConfigs[x] + [line];
            amseEntries[x] = amseEntries[x] + [float(temp[4])]
    
    X = []
    Y = []
    for k in amseEntries.keys():
        X.append(k)
        Y.append(min(amseEntries[k]))
    
    X, Y = (list(t) for t in zip(*sorted(zip(X, Y))))    
    plt.plot(X,Y,marker = marker.next(), markersize=8)
    
#Remove top and right frame incl. ticks
ax = plt.gca()
ax.tick_params(axis='both', direction='out')
ax.get_xaxis().tick_bottom()   # remove unneeded ticks 
ax.get_yaxis().tick_left()
ax.spines["right"].set_visible(False)
ax.spines["top"].set_visible(False)    

plt.xlim(0, 1050)
plt.legend(['pgp', 'fgp', 'apgp'], loc='upper right')
plt.xlabel("Anzahl der Basisvektoren")
plt.ylabel("SMSE")
plt.savefig('sarcos_smse.pdf',bbox_inches='tight')
#plt.show()